﻿using System;
using UnityEngine;
using GoogleMobileAds.Api;


public class MS_ADS : MonoBehaviour
{
    public static MS_ADS Current;
    private InterstitialAd interstitial;
    private RewardedAd rewardedAd;
    private BannerView bannerView;
    private bool RewardSuccess;
    public event Action<bool> OnRewardResult = delegate { };

    public bool BannerEnable;
    public string InterstitialId, RewardedId, BanerId;
    public event Action<bool> Reward_Enable = delegate {};

    private void Awake()
    {
        Current = this;
    }

    public void Start()
    {
        MobileAds.Initialize(initStatus => { });
        if (BannerEnable) Banner_Request();
        Interstitial_Request();
        Reward_Request();
    }

    #region Interstitial Ads

    public void Interstitial_Request()
    {
        if (this.interstitial != null)
        {
            this.interstitial.Destroy();
        }
        // Initialize an InterstitialAd.
        this.interstitial = new InterstitialAd(InterstitialId);

        // Called when an ad request has successfully loaded.
        this.interstitial.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        this.interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is shown.
        this.interstitial.OnAdOpening += HandleOnAdOpened;
        // Called when the ad is closed.
        this.interstitial.OnAdClosed += HandleOnAdClosed;
        //// Called when the ad click caused the user to leave the application.
        //this.interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplication;

        //Request
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        this.interstitial.LoadAd(request);

       
    }

    public bool Interstitial_Show()
    {
        if (this.interstitial.IsLoaded())
        {
            this.interstitial.Show();
        }
        else
        {
            Interstitial_Request();
        }
        return interstitial.IsLoaded();
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
        Interstitial_Request();
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }
    #endregion

    #region Reward Ads

    public void Reward_Request()
    {
        this.rewardedAd = new RewardedAd(RewardedId);
        // Called when an ad request has successfully loaded.
        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        // Called when an ad request failed to load.
        this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        // Called when an ad is shown.
        this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        // Called when an ad request failed to show.
        this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        // Called when the user should be rewarded for interacting with the ad.
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        // Called when the ad is closed.
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);

    }

    public void Reward_Show(Action<bool> callback)
    {
        OnRewardResult = callback;
        if (this.rewardedAd.IsLoaded())
        {
            this.rewardedAd.Show();
            
        }
        else
        {
            OnRewardResult(false);
        }
    }

    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdLoaded event received");
        Reward_Enable(true);
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToLoad event received with message: "
                             + args.Message);
        Reward_Enable(false);
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdOpening event received");
        RewardSuccess = false;
        Reward_Enable(false);
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToShow event received with message: "
                             + args.Message);
        Reward_Enable(false);
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdClosed event received");
        Reward_Request();
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardedAdRewarded event received for "
                        + amount.ToString() + " " + type);
        RewardSuccess = true;
        OnRewardResult(RewardSuccess);
    }
    #endregion

    #region Banner

    public void Banner_Request()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }
        //bannerView = new BannerView(BanerId, AdSize.Banner, 0, 50);
        //AdSize adSize = new AdSize(250, 250);

        // Create a 320x50 banner at the Bottom of the screen.
        bannerView = new BannerView(BanerId, AdSize.Banner, AdPosition.Bottom);

        //// Called when an ad request has successfully loaded.
        //this.bannerView.OnAdLoaded += this.HandleOnBanerLoaded;
        //// Called when an ad request failed to load.
        //this.bannerView.OnAdFailedToLoad += this.HandleOnBanerFailedToLoad;
        //// Called when an ad is clicked.
        //this.bannerView.OnAdOpening += this.HandleOnAdOpened;
        //// Called when the user returned from the app after an ad click.
        //this.bannerView.OnAdClosed += this.HandleOnBanerClosed;
        //// Called when the ad click caused the user to leave the application.
        //this.bannerView.OnAdLeavingApplication += this.HandleOnBanerLeavingApplication;

        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
        bannerView.Hide();
    }

    public void Banner_Show()
    {
        bannerView.Show();
    }

    public void Banner_Hide()
    {
        bannerView.Hide();
    }

    //public void DestroyBanner()
    //{
    //    bannerView.Destroy();
    //}

    public void HandleOnBanerLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnBanerFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnBanerOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnBanerClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnBanerLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    #endregion
}
