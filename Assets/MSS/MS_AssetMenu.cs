﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
//using AFramework;

public class MenuItems
{
#if UNITY_EDITOR
	[MenuItem("Tools/Clear PlayerPrefs")]
#endif
	private static void NewMenuOption()
	{
		PlayerPrefs.DeleteAll();
        Debug.Log("Reset Data");
        //Singleton<GameDataManager>.I.ResetData();
    }
}