﻿using UnityEngine;

public class MS_DevControl : MonoBehaviour {
	
	public static MS_DevControl Current;
	public GameObject reporter, PopupDevMode;
	public TMPro.TMP_InputField CoinsInput, LevelInput;
	public bool devMode;
	private int devCount, devCount2;

	void Awake(){
		devCount = 0;
		devCount2 = 0;
		if (Current)
		{
			DestroyImmediate(gameObject);
		}
		else
		{
			Current = this;
		}

    }
	public void GotoDevMode(){
		devCount ++;
	}

	public void GotoDevMode2()
	{
		devCount2++;
	}

	public void ActiveDevMode(){
		if (devCount == 3 && devCount2 == 3) {
			devMode = true;
			GetComponent<MS_FPS> ().enabled = true;
			reporter.SetActive (true);
			PopupDevMode.SetActive(true);
		} else {
			devMode = false;
			GetComponent<MS_FPS> ().enabled = false;
			reporter.SetActive (false);
			PopupDevMode.SetActive(false);
		}
	}

	public void ClosePop()
	{
		PopupDevMode.SetActive(false);
	}

	public void ClearData()
	{
		PlayerPrefs.DeleteAll();
		Debug.Log("Reset Data");
	}

	public void Cheat()
	{
		Debug.Log("Cheating Funcsion");
	}
}
