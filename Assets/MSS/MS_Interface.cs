﻿using UnityEngine;
using System;
using pingak9;

public class MS_Interface : MonoBehaviour {

	public static MS_Interface Current;
    private event Action<bool> OnRewardAvaiable = delegate { };
    [HideInInspector]
    public bool RewardAvaiable;
    [Header("Remote Settings")]
    public int maxRewardPerDay;
    [SerializeField]
    private float StepTimeAds;
    private float lastTimeShowAd;
    public string linkPage, linkMoreAndroid, linkMoreIos, linkPrivacy, Version, UpdateNote;
    //public bool notifyAvaiable;


    void Awake()
    {
#if LEADER
        if (Application.identifier != "com.bluemind.wordcrossfreecrosswordpuzzle")
        {
            Application.Quit();
        }
        Debug.unityLogger.logEnabled = false;
#endif

        Application.targetFrameRate = 60;

        if (Current)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Current = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
#if LEADER
        MS_ADS.Current.Reward_Enable += RewardAds_Enable;
        Invoke("ShowUpdateDialog", 1f);
#endif

    }

    #region MSService ---------------------------------

    private void ShowUpdateDialog()
    {
#if LEADER
        if (String.Compare(Application.version, Version, true) < 0)
        {
            string[] line = UpdateNote.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            var title = "Update Required";
            var message = Application.productName + " " + "new version available" + "!\r\n----------------------------------";
            foreach (string str in line)
            {
                message += "\r\n       " + str;
            }

            NativeDialog.OpenDialog(title, message, "UPDATE NOW", "LATER",
            () =>
            {
#if UNITY_ANDROID
                Application.OpenURL("market://details?id=" + Application.identifier);

#elif UNITY_IOS
                            Application.OpenURL ("https://itunes.apple.com/us/app/id" +"...");
#endif
            },
            () =>
            {
                Debug.Log("Later button pressed");
            });

        }
#endif
    }


    public void OnFanPage()
    {
        Application.OpenURL(linkPage);

    }

    public void OnRate()
    {
#if UNITY_IOS
        if(!UnityEngine.iOS.Device.RequestStoreReview())
            Application.OpenURL ("https://itunes.apple.com/us/app/id" +"...");
#elif UNITY_ANDROID
        Application.OpenURL("market://details?id=" + Application.identifier);
#endif
    }

    public void OnMoreGame()
    {
#if UNITY_IOS
            Application.OpenURL (linkMoreIos);
#elif UNITY_ANDROID
        Application.OpenURL(linkMoreAndroid);
#endif
    }
    public void OnPrivacy()
    {
        Application.OpenURL(linkPrivacy);
    }

    #endregion

    #region Advertisement ---------------------------------

    public void Banner_Show()
    {
#if LEADER
        MS_ADS.Current.Banner_Show();
#endif
    }

    public void Banner_Hide()
    {
#if LEADER
        MS_ADS.Current.Banner_Hide();
#endif
    }

    public void Banner_Reload()
    {
#if LEADER
        MS_ADS.Current.Banner_Request();
#endif
    }


    public bool Interstitial_Show()
    {
#if LEADER
        return MS_ADS.Current.Interstitial_Show();
#endif
	}

    public void Interstitial_Reload()
    {
#if LEADER
        if (Time.time - lastTimeShowAd >= StepTimeAds)
        {
            lastTimeShowAd = Time.time;
            MS_ADS.Current.Interstitial_Request();
        }
#endif
    }


    private void RewardAds_Enable(bool stt)
    {
        RewardAvaiable = stt;
        OnRewardAvaiable(stt);
    }

    public void Reward_Show(Action<bool> callback)
    {
#if LEADER
        MS_ADS.Current.Reward_Show(callback);
#else
            callback(true);
#endif

    }

    public void Reward_Reload()
    {
#if LEADER
        MS_ADS.Current.Reward_Request();
#endif
    }

    public void RegisterAdsAvailableCallback(Action<bool> callback)
    {
        OnRewardAvaiable +=callback;
    }

    public void UnRegisterAdsAvailableCallback(Action<bool> callback)
    {
        OnRewardAvaiable -= callback;
    }

    #endregion

    #region InApp-Purchase ---------------------------------

       public void Purchase_Item(string productId)
	{
#if LEADER
        if (MS_DevControl.Current.devMode)
        {
            PurchaseSuccsseCallBack(productId);
        }
        else
        {
            Debug.Log("Mua IAP");
        }
#else
            PurchaseSuccsseCallBack(productId);
#endif
    }

    public void PurchaseSuccsseCallBack(string productId)
	{
#if LEADER
        //MyIAPManager.Current.PurchaseCallBack(productId);
#endif
        Debug.Log ("purchase susscces Item" + productId);
    }

	public void PurchaseErrorCallBack(string productId)
	{
#if LEADER
        //MyIAPManager.Current.PurchaseFail();
#endif
        Debug.Log ("purchase Error Item" + productId);
    }

    public void RestorePurchase()
    {
#if LEADER
        Debug.Log("RestoreIAP");
#endif
    }
    #endregion
}
