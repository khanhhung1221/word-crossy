﻿using UnityEngine;
using System;
#if NOTIFY
#if UNITY_ANDROID
using Unity.Notifications.Android;
#elif UNITY_IOS
using NotificationSamples.iOS;
#endif
#endif

public class MS_Notify : MonoBehaviour
{
    public static MS_Notify Current;
    //private string Title;
    //private int id;

    void Awake()
    {
        Current = this;
        //apiLevel = int.Parse(SystemInfo.operatingSystem.Substring(SystemInfo.operatingSystem.IndexOf("-") + 1, 3));

    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (PlayerInfo.Instance.NotificationKey == 1)
        {
            if (pauseStatus)
            {
                Notify();
            }
            else
            {
#if NOTIFY
                AndroidNotificationCenter.CancelAllNotifications();
#endif
                EventClickNotify();
            }
        }
           
    }

    private void Notify()
    {
#if NOTIFY
        //Test
        //InitNotify("T1", "Fly with English1", "Empowering You to become the Best!!", 20);

        //Push 2 ngay
        //InitNotify("C1", "WARNING!!!", "Universe 7 is in danger! Come back now!!", 172800);

        //Push buoi toi

        if (DateTime.Now.Hour < 20)
        {
            InitNotify("C2", "Fly with English", "Empowering You to become the Best!!", 72000 - DateTime.Now.Hour * 3600 - DateTime.Now.Minute * 60 - DateTime.Now.Second);
        }
        else
        {
            InitNotify("C2", "Fly with English", "Empowering You to become the Best!!", 158400 - DateTime.Now.Hour * 3600 - DateTime.Now.Minute * 60 - DateTime.Now.Second);
        }

        //Push cuoi tuan
        if ((int)DateTime.Now.DayOfWeek == 0 && DateTime.Now.Hour < 9)
        {
            InitNotify("C3", "Weekend Challenge", "A great place to Learn!!", 32400 - ((int)DateTime.Now.DayOfWeek * 86400 + DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second));
        }
        else
        {
            InitNotify("C3", "Weekend Challenge", "A great place to Learn!!", 637200 - ((int)DateTime.Now.DayOfWeek * 86400 + DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second));
        }

        //Push Daily
        int time1 = 86400;
        if (DateTime.Now.Hour >= 23)
        {
            time1 += 7 * 3600;
        }
        if (DateTime.Now.Hour < 6)
        {
            time1 += (6 - DateTime.Now.Hour) * 3600;
        }
        InitNotify("C4", "Great Reward!", "Come back and get great reward!", time1);

        //Push lucky
        DateTime DT = DateTime.Now.AddHours(4.0);
        int time = 4 * 3600;
        if (DT.Hour >= 23)
        {
            time += 7 * 3600;
        }
        if (DT.Hour < 6)
        {
            time += (6 - DT.Hour) * 3600;
        }
        InitNotify("C5", "Free Lucky Spin!", "Come back and try your luck!", time);
#endif
    }

    private void EventClickNotify()
    {
#if NOTIFY
        var notificationIntentData = AndroidNotificationCenter.GetLastNotificationIntent();
        if (notificationIntentData != null)
        {
            //var notify = notificationIntentData.Notification; 
            string channel = notificationIntentData.Channel;

            if(channel == "C1")
            {
                Debug.Log("Click On C1 Notify");
            }
        }
#endif
    }

    private void EventReciveNotifyOnApp() //Notify on App Running
    {
#if NOTIFY
        AndroidNotificationCenter.NotificationReceivedCallback receivedNotificationHandler =
        delegate (AndroidNotificationIntentData data)
        {
            var msg = "Notification received : " + data.Id + "\n";
            msg += "\n Notification received: ";
            msg += "\n .Title: " + data.Notification.Title;
            msg += "\n .Body: " + data.Notification.Text;
            msg += "\n .Channel: " + data.Channel;
            Debug.Log(msg);
        };
        AndroidNotificationCenter.OnNotificationReceived += receivedNotificationHandler;
#endif
    }

    public void InitNotify(string channelId, string Title, string BodyText, double time, string SmallIcon = "icon_0", string LageIcon = "icon_1")
    {
#if NOTIFY
            //---------- Check Notify Avairable ----------------

        //if (AndroidNotificationCenter.CheckScheduledNotificationStatus(identifier) == NotificationStatus.Scheduled)
        //{
        //    // Replace the currently scheduled notification with a new notification.
        //    AndroidNotificationCenter.UpdateScheduledNotification(1, notification, "channel_id");
        //}
        //else if (AndroidNotificationCenter.CheckScheduledNotificationStatus(identifier) == NotificationStatus.Delivered)
        //{
        //    //Remove the notification from the status bar
        //    AndroidNotificationCenter.CancelNotification(identifier);
        //}
        //else if (AndroidNotificationCenter.CheckScheduledNotificationStatus(identifier) == NotificationStatus.Unknown)
        //{
        //    var identifier1 = AndroidNotificationCenter.SendNotification(notification, "channel_id");
        //}

        var c = new AndroidNotificationChannel()
        {
            Id = channelId,
            Name = "Default Channel",
            Importance = Importance.High,
            Description = "Generic notifications",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(c);
        var notification = new AndroidNotification();
        notification.Title = Title;
        notification.Text = BodyText;
        notification.SmallIcon = SmallIcon;
        notification.LargeIcon = LageIcon;
        notification.FireTime = System.DateTime.Now.AddSeconds(time);
        //Save Notify Data
        notification.IntentData = "data_string";
        var identifier = AndroidNotificationCenter.SendNotification(notification, channelId);   
#endif

    }
}
