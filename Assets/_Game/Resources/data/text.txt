id,en
int,string
1001,<size=50>Combine the word <color=#fffc00>'{0}'</color></size>
1002,<size=50>Good job! Now combine the word <color=#fffc00>'{0}'</color></size>
1003,SUCCESS
1004,LEVEL {0}
1005,NEXT
1006,"If you got stuck, try to use <color=#fffc00>'Shuffle'</color> and <color=#fffc00>'Hint'</color> button."
1007,GOT IT
1008,<size=50>Use this to get a hint of a directed letter</size>
1009,<size=50>Tap a blank tile to reveal the letter.</size>
1010,<size=50>Tap a blank tile to reveal the letter.</size>
1011,Confirm your choice
1012,CONFIRM
1013,CANCEL
1014,You have already collected this word
1015,FREE
1016,CLAIM
1017,Get <color=#ff6000><size=48>{0} Coins</size></color>\nFree for watching a video!
1018,WATCH
1019,BONUS WORD
1020,Bonus words collected: 
1021,Bonus words found in this level
1022,SETTINGS
1023,My Score:
1024,Music
1025,Sound
1026,Notifications
1027,HOW TO PLAY
1028,CONTACT US
1029,Rate
1030,BACK
1031,MAIL
1032,RANK
1033,Re-rank remaining time 
1034,Reward Preview
1035,My Rank:
1036,DAILY REWARD
1037,DAY
1038,TODAY
1039,Map
1040,Coming Soon
1041,Quit
1042,Cancel
1043,Confirm
1044,Are you sure to quit game?
1045,SHOP
1046,Remove Ads
1047,New Ability
1048,"If you claim daily gifts for three days in a row, you will unlock a new item."
1049,"You have unlocked the new item, try to use it."
1050,ON
1051,OFF
1052,DISCONNECT
1053,You earned
1054,CONNECT
1055,One Time Offer
1056,Purchased {0} Coins Successfully!
1057,Please check your network connection and try again later
1058,Purchased Remove Ads Successfully!
1059,Connect to Facebook
1060,Friends can send you coins
1061,Get 200 Free Coins
1062,Sync your progress online
1063,Friends can help you pass level
1064,CONNECT
1065,LOADING
1066,Use this item to reveal five letters from blank tiles.
1067,New item unlocked!
1068,Coins Received
1069,Your friend has sent you coins!
1070,Compensation
1071,"Sorry for issues, here is the compensation."
1072,Sent coins! Reset in {0}
1073,Terms of Service
1074,Privacy Policy
1075,PLAY
1076,"By swiping letter, you agree to our "
1077,Swipe to build a valid word.
1078,Use 'Shuffle' or Hint' if you are stuck.\nYou can also take a screenshot and ask your friends.
1079,Some levels have a\n <color=#fffc00>Coin Word</color> to find! You'll collect coins once you have guessed the word.
1080,How can I improve my score?\n1. Swipe an answer\n2. Combo\n3. Find a bonus word
1081,Use this item to hint any letter you want to know.
1082,The powerful item can show 5 letters from blank tiles. Use it when you get stuck!
1083,"No new message received, invite more friend to play!"
1084,Terms of Service
1085,and
1086,Privacy Policy
1087,[e]270C[/e] Hi! we found a clue over there!
1088,A super gift [e]1F381[/e]  is waiting for you claim it!
1089,"90% players were stuck at this level, can you solve it [e]1F575[/e] ?"
1090,How many [e]2B50[/e] stars [e]2B50[/e] can you catch in todays puzzle?
1091,[e]1F47B[/e] Oops! Someone has exceeded your ranking! Check it out!
1092,Are you there? The daily gift is waiting![e]1F388[/e] [e]1F388[/e]
1093,"I am stuck, help me solve the puzzle![e]1F914[/e] "
1094,Tips: Try use this word [e]1F449[/e] [e]1F449[/e] 
1095,[e]1F3AE[/e] Come and try your luck in puzzle games!
1096,Someone wants to have a compatition with you![e]1F4AA[/e] [e]1F4AA[/e]
1097,Deals
1098,EXTRA
1099,FREE
1100,Some levels have a Coin Word to find!
1101,"Bonus words will be shown here, collect to earn coins!"
1102,RATE US
1103,What do you think of Word Slide?
1104,Your Email
1105,Your Name
1106,Collect bonus words to get extra coins!
1107,2X WITH VIDEO
1108,Score
1109,Re-rank remaining time
1110,Days Left
1111,Hours Left
1112,Congratulations!\nYou have earned <color=#ff6000><size=50>{0} Coins</size></color>.
1113,<size=50>You can remove ads in the shop.</size>
1114,BEST
1115,POPULAR
1116,more
1117,Limited Offer
1118,"Connection failed, pls check your network setting."
1119,BONUS
1120,"Connect to Facebook failed, pls try again later."
1121,Version
1122,GET
1123,WATCH VIDEO
1124,Lv. {0} COMPLETE
1125,Free Coins
1126,Send Message
1127,Message has been sent!
1128,Are you sure to abandon sending message?
1129,You need to input your email
1130,January
1131,February
1132,March
1133,April
1134,May
1135,June
1136,July
1137,August
1138,September
1139,October
1140,November
1141,December
1142,JAN
1143,FEB
1144,MAR
1145,APR
1146,MAY
1147,JUN
1148,JUL
1149,AUG
1150,SEP
1151,OCT
1152,NOV
1153,DEC
1154,Come back tomorrow to play the next daily puzzle!
1155,"No level matches, you need to update to the latest version."
1156,Update
1157,DAILY QUEST
1158,Complete {0} New Level!
1159,Collect {0} Scores!
1160,Watch {0} advertisements!
1161,Find {0} Bonus Words!
1162,Collect {0} Star in Daily Puzzle!
1163,GO
1164,DAILY QUEST COMPLETE
1165,Collect {0} Stars in Daily Puzzle!
1166,Connect to Facebook
1167,Complete {0} Good combos!
1168,<size=50>Play Daily Puzzle and earn rewards.</size>
1169,<size=42>Combine the word '{0}' to collect a butterfly.</size>
1170,<size=50>Great! Collect as many <color=#fffc00>butterflies</color> as you can.</size>
1171,Reach Lv. {0} to unlock Daily Puzzle
1172,<size=42>Unlocked <color=#FF5839>'Daily Puzzle'!</color> Wanna have a try?</size>
1173,GO
1174,Not Now
1175,<size=50>Complete <color=#fffc00>Daily Quest</color> to earn rewards.</size>
1176,Complete {0} Great combos!
1177,Complete {0} Excellent combos!
1178,Complete {0} Amazing combos!
1179,Complete {0} Unbelievable combos!
1180,Complete {0} Outstanding combos!
1181,Unlock one new Chapter!
1182,Unlock
1183,[e]1F579[/e] New puzzles update! Can you solve it? [e]1F579[/e]
1184,"[e]1F579[/e] Emmm, it's the most challengeable level I've ever seen. [e]1F579[/e] "
1185,[e]1F3C6[/e] An anonymous player is leading game! [e]1F3C6[/e]
1186,[e]1F618[/e] Come on! Someone has collected more pictures than you!
1187,[e]1F60E[/e] You've accomplished so much quests! [e]1F60E[/e]
1188,Time To Refresh:
1189,WORLD
1190,DAILY\nPUZZLE
1191,QUEST
1192,CLAIMED
1193,Reach Lv.{0} to unlock Daily Quest.
1194,6X WITH VIDEO
1195,You are doing very well. Keep playing to collect more words.
1196,"If you get stuck, watch a quick video for         <color=#ff6000><size=50>FREE COINS!</size></color>"
1197,Quest Complete! Come claim your reward!
1198,Quest Complete! Claim your reward now!
1199,Purchase {0} Successfully!
1200,New Item unlocked! Use this item to reveal five letters from blank tiles.
1201,<size=50>Play every day to unlock your rewards.</size>
1202,Collect <color=#fffc00>Shields</color>  to pick the card. Shield will appear in random levels.
1203,<size=50>Fill the word to collect  <color=#fffc00>Shield</color></size>
1204,Independence Day Event has begun.
1205,"We attach importance to the privacy of your personal data. In order to keep improving game experience, we will collect date during game. You can view our privacy policy and terms of Service in the links below. Have fun."
1206,AGREE
1207,PRIVACY POLICY
1208,LIVES
1209,Next free life in
1210,LIVES FULL
1211,One life is lost when game failed!
1212,REFILL
1213,TAP TO CONTINUE
1214,A new chanllenge <color=#fffc00>Garden Treasure </color> has begun!
1215,TIME OUT!
1216,CONTINUE
1217,GIVE UP
1218,Complete level before time ends to collect <color=#fffc00>Drop</color>!
1219,"Complete levels in limited time. The less time spent, the more <color=#fffc00>Drop</color> you can collected."
1220,Collect <color=#fffc00>Drop</color> from Puzzles to win rewards!
1221,No 3 letter word in current level.
1222,Congratulations! You have completed all Daily Quests.
1223,"In the current level, there are no 3 letter words. Only 4 or more letter words are valid."
1224,You got a Piggy Bank!
1225,Play levels and earn flowers to save Coins in your Piggy Bank.
1226,The Piggy Bank is full!
1227,There is no space for more coins.Open it to collect your earnings!
1228,Claimed Successfully!
1229,"Complete event levels and collect sunlight. The more you collect, the better reward you can earn. "
1230,"Sorry, there was something wrong with the network. Please try again later."
1231,"Collect all the <color=#fffc00>Sunlight!</color> After collecting one, the new one will appear."
1232,Congratulations! Here is your reward.
1233,New event <color=#fffc00>'Solar Collection' </color> has begun!
1234,"New event is ready, wanna have a try?"
1235,GO
1236,"Half price for Hint, remaining time: 30 min."
1237,DAILY\nSEARCH
1238,Are you sure to exit? Current progress will be lost.
1239,Exit
1240,Keep Playing
1241,Try Again
1242,Catch a Butterfly for watching a quick video!
1243,Complete Lv. {0} to unlock Piggy Bank!
1244,OUT OF LIVES
1245,Congratulations
1246,Get a free HINT for watching a quick video.
1247,<size=42>Swipe the word <color=#fffc00>'{0}'</color></size>
1248,<size=42>Simply find & swipe the words before time ends!</size>
1249,Reach Lv. {0} to unlock Daily Search
1250,<size=42>Unlocked <color=#FF5839>'Daily Search'!</color> Wanna have a try?</size>
1251,<size=42>New mode: Daily Search is waiting for your challenge</size>
1252,Daily Puzzle
1253,"<size=42>Start here on Level 1, and see if you can complete all.</size>"
1254,"<size=42>If you get stuck, try to use the mark pen!</size>"
1255,WELL DONE!
1256,EXCELLENT!
1257,PERFECT!
1258,Pass {0} Levels in Daily Search!
1259,NEW\nEVENT
1260,Daily Search
1261,Tips: Bind Facebook to win Free Coins.
1262,Tips: Login Facebook can save your progress.
1263,Tips: Complete Quest to earn extra Coins.
1264,Tips: Do not forget to claim Quest Reward.
1265,Tips: Get stuck? Try to use Hint items.
1266,Tips: Remember to update game weekly.
1267,Tips: Actively participate Events!
1268,Tips: Complete Levels to unlock New modes!
1269,Tips:  Win tons of Coins in Daily Puzzle.
1270,Tips: Usually use items to accelerate your progress!
1271,Tournament
1272,Rank
1273,Complete a level to enter the Tournament!
1274,Collect Trophies to win amazing rewards!
1275,Reward
1276,Finished
1277,Tournamet has ended. 
1278,"You have completed today's challenge, come back tomorrow!"
1279,OK
1280,You placed {0} of 50\nCollect your prize!
1281,Catch a butterfly directly for watching a quick video!
1282,ONLY
1283,WAS
1284,OPEN
1285,YOU
1286,NEW 
1287,A New Tournament has begun!
1288,Find words to earn <color=#fffc00>Trophies</color> for the Tournament!
1289,Check out your Rank and Prizes!
1290,Tap here to enter a nickname!
1291,CHOOSE NICKNAME
1292,Enter your nickname as you like to appear
1293,Nick name must be 3 or more charaters.
1294,CONGRATULATION
1295,LEVEL {0} COMPLETE
1296,LEVEL   {0}
1297,Pick your daily gift!
1298,RESTORE PURCHASES
1299,RESTORE SUCCESSFULLY
1300,RESTORE FAILED
1301,NO ITEM TO RESTORE
1302,"Now, you can pick one card. \nGood Luck!"
1303,Collect more shields to pick the gift card!
1304,Independence Day Present is in one of the gift cards.Collect Shields and pick the right one.
1305,EVENT COMPLETE!
1306,No thanks
1307,VIP
1308,FREE TRAIL
1309,/Week
1310,BENEFITS
1311,INSTANT REWARDS
1312,Become a VIP member and enjoy bunches of benefits!
1313,Info
1314,Get all of these everyday!
1315,"Subscribe to the VIP member to unlock the following features in game:\n  - You will instantly receive 1200 coins, 3 hint item and 2 flash item after your first subscription!\n  - Collect 150 coins when log in daily\n  - 50% price off on Flash item\n  - Collect 1 flash item when log in daily.\nThe VIP subscription price is {0}/week per week with 3-day free trail.\nWhen the trial period end, Google Play automaticlly charges your account at the amount set for the weekly subscription, and continuing at the subscription interval.\nSubscription can be managed by the user and auto renewal can be cancelled by going to user's Account Settings after purchase. For more details, please refer to the link:\n{1}\nThanks for your supporting and have fun playing!"
1316,VIP activated successfully
1317,Liberty Bell
1318,GIVE UP?
1319,YES
1320,NO
1321,TIME LIMIT
1322,NEW\nEVENT
1323,COLLECTION
1324,SPECIAL
1325,COLLECT
1326,FULL
1327,min
1328,VIP Bonus
1329,Here is your vip bonus
1330,Swipe the letters to find out the words.
1331,New event <color=#fffc00>'Word Find' </color> has begun!
1332,"Find the words in the leves, the more words you find, the higher scores you can earn."
1333,Win up to <color=#ffab38>1000% More</color> coins in Lucky Draw. Come and try!
1334,Spin
1335,For
1336,"Swipe words to earn scores, word with more letters earn higher scores."
1337,"Subscribe to the VIP member to unlock the following features in game:\n  - You will instantly receive 1200 coins, 3 hint item and 2 flash item after your first subscription!\n  - Collect 150 coins when log in daily\n  - 50% price off on Flash item\n  - Collect 1 flash item when log in daily.\nThe VIP subscription price is {0}/week per week with 3-day free trail.\nWhen the trial period end, payment will be charged to iTunes Account at the amount set for the weekly subscription, and continuing at the subscription interval.\nSubscription can be managed by the user and auto renewal can be cancelled by going to user's iTues Account Settings after purchase. For more details, please refer to the link:\n{1}\nThanks for your supporting and have fun playing!"
1338,Feeling lucky? Try Lucky Draw!
1339,Purchased {0} Coins and Remove Ads Successfully!!
,
,
,
,
,
,
2000,Harmony
2001,Sailing
2002,Fruition
2003,Mountain
2004,Blossom
2005,Landscape
2006,Life
2007,Ocean
2008,Journey
2009,Tranquil
2010,Hope
2011,Forest
2012,Flower
2013,Faith
2014,Compassion
2015,Aurora
2016,Courage
2017,Soul
2018,Spirit
2019,Energy
2020,Strength
2021,Clarity
2022,Enlightenment
2023,Kindness
2024,Sunshine
2025,Connection
2026,Glow
2027,Wonder
2028,Leaf
2029,Road
2030,Frond
2031,Ravine
2032,Calm
2033,Life
2034,Ladybug
2035,Peaceful
2036,Glamour
2037,Smooth
2038,Woods
2039,Lake
2040,Boat
2041,Night
2042,Bloom
2043,Path
2044,Temple
2045,Ridge
2046,Bay
2047,Snow
2048,Blossom
2049,Edifice
2050,Nature
2051,Sunny
2052,Cosy
2053,Comfort
2054,Petal
2055,Camp
2056,Foliage
2057,Linger
2058,Seashore
2059,Field
2060,Corolla
2061,Peak
2062,Serenity
2063,Cosy
2064,Sunset
2065,Range
2066,Petal
2067,Grace
,
,
,
,
3000,Valuable Pack
3001,Super Pack
3002,Mega Pack
3003,Supremacy Pack
3004,Starter Kit
3005,Ads Cleaner 
3006,FLash Bundle
3007,Hints Bundle
3008,Trainee Kit 
3009,Exploration Kit
3010,Discovery Kit
3011,Knowledge Kit
3012,Doulbe Coin Offer
3013,Double Coins Offer
3014,Almighty Kit