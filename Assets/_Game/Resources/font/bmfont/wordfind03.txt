info face="Arial" size=32 bold=0 italic=0 charset="" unicode=1 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1 outline=0
common lineHeight=32 base=26 scaleW=512 scaleH=512 pages=1 packed=0 alphaChnl=0 redChnl=4 greenChnl=4 blueChnl=4
page id=0 file="wordfind03_0.png"
chars count=26
char id=65   x=0     y=171   width=72    height=83    xoffset=0     yoffset=0     xadvance=72    page=0  chnl=15
char id=66   x=73    y=254   width=71    height=82    xoffset=0     yoffset=0     xadvance=71    page=0  chnl=15
char id=67   x=299   y=0     width=72    height=84    xoffset=0     yoffset=0     xadvance=72    page=0  chnl=15
char id=68   x=357   y=169   width=73    height=82    xoffset=0     yoffset=0     xadvance=73    page=0  chnl=15
char id=69   x=145   y=254   width=71    height=82    xoffset=0     yoffset=0     xadvance=71    page=0  chnl=15
char id=70   x=73    y=170   width=70    height=83    xoffset=0     yoffset=0     xadvance=70    page=0  chnl=15
char id=71   x=355   y=85    width=76    height=83    xoffset=0     yoffset=0     xadvance=76    page=0  chnl=15
char id=72   x=73    y=85    width=71    height=84    xoffset=0     yoffset=0     xadvance=71    page=0  chnl=15
char id=73   x=288   y=253   width=27    height=82    xoffset=0     yoffset=0     xadvance=27    page=0  chnl=15
char id=74   x=445   y=0     width=65    height=84    xoffset=0     yoffset=0     xadvance=65    page=0  chnl=15
char id=75   x=144   y=170   width=70    height=83    xoffset=0     yoffset=0     xadvance=70    page=0  chnl=15
char id=76   x=215   y=170   width=65    height=83    xoffset=0     yoffset=0     xadvance=65    page=0  chnl=15
char id=77   x=281   y=170   width=75    height=82    xoffset=0     yoffset=0     xadvance=75    page=0  chnl=15
char id=78   x=431   y=169   width=72    height=82    xoffset=0     yoffset=0     xadvance=72    page=0  chnl=15
char id=79   x=432   y=85    width=74    height=83    xoffset=0     yoffset=0     xadvance=74    page=0  chnl=15
char id=80   x=0     y=255   width=72    height=82    xoffset=0     yoffset=0     xadvance=72    page=0  chnl=15
char id=81   x=148   y=0     width=75    height=84    xoffset=0     yoffset=0     xadvance=75    page=0  chnl=15
char id=82   x=372   y=0     width=72    height=84    xoffset=0     yoffset=0     xadvance=72    page=0  chnl=15
char id=83   x=287   y=85    width=67    height=84    xoffset=0     yoffset=0     xadvance=67    page=0  chnl=15
char id=84   x=224   y=0     width=74    height=84    xoffset=0     yoffset=0     xadvance=74    page=0  chnl=15
char id=85   x=0     y=86    width=72    height=84    xoffset=0     yoffset=0     xadvance=72    page=0  chnl=15
char id=86   x=217   y=85    width=69    height=84    xoffset=0     yoffset=0     xadvance=69    page=0  chnl=15
char id=87   x=71    y=0     width=76    height=84    xoffset=0     yoffset=0     xadvance=76    page=0  chnl=15
char id=88   x=145   y=85    width=71    height=84    xoffset=0     yoffset=0     xadvance=71    page=0  chnl=15
char id=89   x=0     y=0     width=70    height=85    xoffset=0     yoffset=0     xadvance=70    page=0  chnl=15
char id=90   x=217   y=254   width=70    height=82    xoffset=0     yoffset=0     xadvance=70    page=0  chnl=15
