using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class AdJustDataManager : MonoBehaviour
{
	private static AdJustDataManager inst;

	public Dictionary<string, string> m_List;

	private bool inited;

	public static AdJustDataManager instance => inst ?? (inst = new AdJustDataManager());

	public AdJustDataManager()
	{
		Init(Resources.Load<TextAsset>("Data/adjust").bytes);
	}

	public void Init(byte[] file, string source_encoding = "UTF-8")
	{
		if (!inited)
		{
			MemoryStream stream = new MemoryStream(file);
			m_List = new Dictionary<string, string>();
			List<string> list = new List<string>();
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							list.Add(array[0]);
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			for (int i = 0; i < list.Count; i += 2)
			{
				m_List.Add(list[i + 1], list[i]);
			}
			inited = true;
		}
	}

	public string GetToken(string key)
	{
		if (m_List.ContainsKey(key))
		{
			return m_List[key];
		}
		return string.Empty;
	}
}
