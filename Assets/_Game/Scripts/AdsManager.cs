using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class AdsManager : MonoBehaviour
{
	public static AdsManager Instance;

    public static UnityAction<bool> ShowRewardVideoEvent;

    public static UnityAction ShowInterstitialAdEvent;

	//public GameObject heyzap_go;

	//public GameObject mopub_go;

	public string InterstitialStateStr = string.Empty;

	public bool isShowBanner;

	public static bool isHideBanner;

	public bool isShowIncentivizedAd;

	public bool IncentivizedAdState
	{
		get
		{
            return MS_Interface.Current.RewardAvaiable;
		}
	}

	private void Awake()
	{
		DontDestroyOnLoad(this);
		Instance = this;
	}

	public void DestroyBannerAD()
	{
		isShowBanner = false;
		MS_Interface.Current.Banner_Reload();
	}

	public void HideBannerAD()
	{
		isShowBanner = false;
		MS_Interface.Current.Banner_Hide();
	}

	public void ShowBannerAD()
	{
		if (PlayerInfo.Instance.TutorialStep == 999 && PlayerInfo.Instance.AdRemove != 1 && WordGlobal.isNeedBanner)
		{
			isShowBanner = true;
			MS_Interface.Current.Banner_Show();
		}
	}

	public bool ShowInterstitialAd(string logEvent)
	{
		if (PlayerInfo.Instance.AdRemove == 1)
		{
			return false;
		}
		bool flag = false;
		flag = MS_Interface.Current.Interstitial_Show();
		WordGlobal.LogEvent(logEvent);
		return flag;
	}

	public void WatchVideoEvent(bool ret)
	{
		ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEvent));
		if (ret)
		{
			UIManager.Instance.isShowPopNow = true;
			CommonTipsScript.Instance.ShowClaimUI((int)PlayerInfo.Instance.systemConfig.WatchAdGold, delegate(object obj)
			{
				Vector3 starPos = (Vector3)obj;
				GoldScript goldScript = UnityEngine.Object.FindObjectOfType<GoldScript>();
				goldScript.CollectGold(starPos, 5, 1f, null);
				goldScript.AddGold((int)PlayerInfo.Instance.systemConfig.WatchAdGold, 1f, 1f, isAdd2PlayerInfo: true, 1);
			}, showClose: false);
		}
	}

	IEnumerator RewardFuncion(bool stt)
    {
		yield return null;
		ShowRewardVideoEvent(stt);
	}

	public bool ShowIncentivizedAd(string eventStr)
	{
		if (!IncentivizedAdState)
		{
			return false;
		}

		PlayerInfo.Instance.SetFirstTapAdStr(eventStr);
		WordGlobal.LogEvent(eventStr);
        ShowRewardVideoEvent = null;
        bool flag = false;
        MS_Interface.Current.Reward_Show(delegate (bool stt)
        {
            StartCoroutine(RewardFuncion(stt));
		});
        flag = true;
		isShowIncentivizedAd = flag;
		return flag;
	}
}
