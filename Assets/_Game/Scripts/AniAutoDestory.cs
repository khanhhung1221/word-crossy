using UnityEngine;

public class AniAutoDestory : MonoBehaviour
{
	private Animator _ani;

	private float _delayTime = 0.2f;

	private void Start()
	{
	}

	private void Update()
	{
		_delayTime -= Time.deltaTime;
		if (_delayTime < 0f && _ani.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
		{
			Object.Destroy(base.gameObject);
		}
	}

	private void Awake()
	{
		_ani = base.transform.GetComponent<Animator>();
	}
}
