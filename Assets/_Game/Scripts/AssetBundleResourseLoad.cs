using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AssetBundleResourseLoad : MonoBehaviour
{
	public delegate void DownLoadFileFinishDelegate();

	private string m_Path;

	private Dictionary<string, AssetBundle> m_AssetBundlesList;

	private Dictionary<string, ABResource> m_ABResourceList;

	private static AssetBundleResourseLoad instance;

	public List<string> m_UpdateFils;

	public int m_MaxFielVersion;

	private float m_UnloadTime = 600f;

	private float m_unloadTime;

	private bool m_DownloadError;

	public static AssetBundleResourseLoad Instance => GetInstance();

	public static event DownLoadFileFinishDelegate DownLoadFileFinishEvent;

	private static AssetBundleResourseLoad GetInstance()
	{
		if ((bool)instance)
		{
			return instance;
		}
		GameObject gameObject = new GameObject("AssetBundleResourseLoad", typeof(AssetBundleResourseLoad));
		return gameObject.GetComponent<AssetBundleResourseLoad>();
	}

	private void Start()
	{
	}

	private void Update()
	{
		m_unloadTime -= Time.deltaTime;
		if (m_unloadTime < 0f)
		{
			m_unloadTime = m_UnloadTime;
			Resources.UnloadUnusedAssets();
		}
	}

	private void Awake()
	{
		if (instance != null)
		{
			Object.DestroyImmediate(base.gameObject);
			return;
		}
		instance = this;
		Object.DontDestroyOnLoad(this);
		m_AssetBundlesList = new Dictionary<string, AssetBundle>();
		m_ABResourceList = new Dictionary<string, ABResource>();
		m_UpdateFils = new List<string>();
		m_unloadTime = m_UnloadTime;
		m_Path = Path.Combine(Application.persistentDataPath, "hotfix");
		if (!Directory.Exists(m_Path))
		{
			Directory.CreateDirectory(m_Path);
		}
	}

	public void UnloadAllAssetBound()
	{
		if (m_ABResourceList != null)
		{
			foreach (KeyValuePair<string, AssetBundle> assetBundles in m_AssetBundlesList)
			{
				assetBundles.Value.Unload(unloadAllLoadedObjects: true);
			}
		}
		if (m_AssetBundlesList != null)
		{
			m_AssetBundlesList.Clear();
		}
		if (m_ABResourceList != null)
		{
			m_ABResourceList.Clear();
		}
	}

	public void LoadAllAssetBound()
	{
		if (!File.Exists(Path.Combine(m_Path, "AssetBundles")))
		{
			Debug.LogError("LoadAllAssetBound  filenot exists ==========================");
			return;
		}
		AssetBundle assetBundle = AssetBundle.LoadFromFile(Path.Combine(m_Path, "AssetBundles"));
		AssetBundleManifest assetBundleManifest = assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
		assetBundle.Unload(unloadAllLoadedObjects: false);
		string[] allAssetBundles = assetBundleManifest.GetAllAssetBundles();
		for (int i = 0; i < allAssetBundles.Length; i++)
		{
			if (m_AssetBundlesList.ContainsKey(allAssetBundles[i]))
			{
				continue;
			}
			AssetBundle assetBundle2 = AssetBundle.LoadFromFile(Path.Combine(m_Path, allAssetBundles[i]));
			if (assetBundle2 != null)
			{
				m_AssetBundlesList.Add(allAssetBundles[i], assetBundle2);
				string[] allAssetNames = assetBundle2.GetAllAssetNames();
				for (int j = 0; j < allAssetNames.Length; j++)
				{
					ABResource aBResource = new ABResource();
					aBResource.name = allAssetNames[j];
					aBResource.abName = allAssetBundles[i];
					m_ABResourceList.Add(aBResource.name.Substring(0, aBResource.name.LastIndexOf(".")), aBResource);
				}
			}
			else
			{
				PlayerInfo.Instance.ResVersion--;
			}
		}
	}

	public Sprite LoadSprite(string resName)
	{
		Sprite result = null;
		string key = resName.ToLower();
		if (m_ABResourceList.ContainsKey(key))
		{
			return m_AssetBundlesList[m_ABResourceList[key].abName].LoadAsset<Sprite>(m_ABResourceList[key].name);
		}
		return result;
	}

	public T LoadImage<T>(string path) where T : Object
	{
		T result = (T)null;
		string key = path.ToLower();
		if (m_ABResourceList.ContainsKey(key))
		{
			return m_AssetBundlesList[m_ABResourceList[key].abName].LoadAsset<T>(m_ABResourceList[key].name);
		}
		return result;
	}

	public GameObject LoadGameObject(string resName)
	{
		return LoadRes<GameObject>(resName);
	}

	public T LoadRes<T>(string resName) where T : Object
	{
		if (WordGlobal.isDebugBuild)
		{
			return (T)null;
		}
		string key = resName.ToLower();
		if (m_ABResourceList.ContainsKey(key))
		{
			return m_AssetBundlesList[m_ABResourceList[key].abName].LoadAsset<T>(m_ABResourceList[key].name);
		}
		return (T)null;
	}

	private void DownLoadFinish()
	{
		LoadAllAssetBound();
		if (!m_DownloadError)
		{
			PlayerInfo.Instance.ResVersion = m_MaxFielVersion;
		}
		if (AssetBundleResourseLoad.DownLoadFileFinishEvent != null)
		{
			AssetBundleResourseLoad.DownLoadFileFinishEvent();
		}
	}

	public void DownloadFile()
	{
		Debug.Log("Download File Sql");
		if (m_UpdateFils.Count == 0)
		{
			DownLoadFinish();
		}
		for (int i = 0; i < m_UpdateFils.Count; i++)
		{
			NetMill.Instance.Download(Path.Combine(WordGlobal.FileServerUrl, m_UpdateFils[i]), Path.Combine(m_Path, Path.GetFileName(m_UpdateFils[i])), m_UpdateFils[i], DownLoadOKHandler, DownloadErrorHandler);
		}
	}

	private void DownLoadOKHandler(object content, object userData)
	{
		string file = (string)userData;
		RemoveUpdateFils(file);
	}

	private void DownloadErrorHandler(object userData, int code, string msg)
	{
		string file = (string)userData;
		RemoveUpdateFils(file);
		m_DownloadError = true;
	}

	private void RemoveUpdateFils(string file)
	{
		m_UpdateFils.Remove(file);
		if (m_UpdateFils.Count == 0)
		{
			DownLoadFinish();
		}
	}

	private void OnDestroy()
	{
		UnloadAllAssetBound();
	}
}
