using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
	public AudioClip Music_Main1;

	public AudioClip Music_Main2;

	public AudioClip Music_LevelComplete;

	public AudioClip SF_WindowOpen;

	public AudioClip SF_WindowClose;

	public AudioClip SF_EnterLevel;

	public AudioClip SF_GetCoin;

	public AudioClip SF_WrongWord;

	public AudioClip SF_LastWord;

	public AudioClip SF_ItemFlash;

	public AudioClip SF_ItemShuffle;

	public AudioClip SF_ItemHint;

	public AudioClip SF_ItemSpecific;

	public AudioClip SF_WordDuplicate;

	public AudioClip SF_GetScore;

	public AudioClip SF_DailyGift;

	public AudioClip SF_WordMatch;

	public AudioClip[] SF_SpellList;

	public AudioClip[] SF_ComboList;

	public AudioClip[] SF_HuoDongList;

	public AudioClip[] SF_PiggyBankList;

	public AudioClip[] SF_HuoDongGardonList;

	public AudioClip[] SF_WordSearchList;

	public AudioClip SF_WordSearchBg;

	public AudioClip[] SF_MainUIGoldList;

	public AudioClip SF_LuckyDrawWin;

	public AudioSource Bg_Audio;

	public GameObject SoundEff_Obj;

	private List<AudioSource> eff_audio_list;

	private static AudioControl _instance;

	private float bg_volume = 1f;

	private float sound_volume = 0.5f;

	public static bool isMusicOpen
	{
		get
		{
			int num = int.Parse(PlayerPrefs.GetString("Player.Music", "1"));
			return num == 1;
		}
		set
		{
			PlayerPrefs.SetString("Player.Music", (value ? 1 : 0).ToString());
		}
	}

	public static bool isSoundEffOpen
	{
		get
		{
			int num = int.Parse(PlayerPrefs.GetString("Player.Sound", "1"));
			return num == 1;
		}
		set
		{
			PlayerPrefs.SetString("Player.Sound", (value ? 1 : 0).ToString());
		}
	}

	private void Awake()
	{
		_instance = this;
		eff_audio_list = new List<AudioSource>();
		SetBgVudio(isMusicOpen);
		SetEffVudio(isSoundEffOpen);
	}

	public static AudioControl GetInstance()
	{
		if ((bool)_instance)
		{
			return _instance;
		}
		GameObject gameObject = ResourceLoadManager.GetInstance().LoadObjByPath("UI/AudioControlUI/AudioController");
		Object.DontDestroyOnLoad(gameObject);
		return gameObject.GetComponent<AudioControl>();
	}

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void PlayBG(AudioClip bg)
	{
		if (!isMusicOpen)
		{
			return;
		}
		if (bg == null)
		{
			StopBG();
		}
		if (Bg_Audio != null && Bg_Audio.clip != null && Bg_Audio.clip.name.Equals(bg.name))
		{
			Bg_Audio.Play();
			return;
		}
		if (Bg_Audio.clip == null || !Bg_Audio.clip.name.Equals(bg.name))
		{
			Bg_Audio.clip = bg;
			Bg_Audio.loop = true;
		}
		Bg_Audio.Play();
	}

	public void StopBG()
	{
		if (isMusicOpen && Bg_Audio != null && Bg_Audio.clip != null)
		{
			Bg_Audio.Stop();
		}
	}

	public AudioSource PlaySoundEff(AudioClip effClip)
	{
		if (!isSoundEffOpen)
		{
			return null;
		}
		AudioSource audioSource = null;
		for (int i = 0; i < eff_audio_list.Count; i++)
		{
			AudioSource audioSource2 = eff_audio_list[i];
			if (audioSource2 != null && !audioSource2.isPlaying)
			{
				audioSource = audioSource2;
				break;
			}
		}
		if (audioSource == null && eff_audio_list != null)
		{
			AudioSource audioSource3 = SoundEff_Obj.AddComponent<AudioSource>();
			audioSource = audioSource3;
			eff_audio_list.Add(audioSource);
		}
		if (audioSource != null)
		{
			audioSource.clip = effClip;
			audioSource.loop = false;
			audioSource.volume = sound_volume;
			audioSource.Play();
		}
		return audioSource;
	}

	public AudioSource PlayComboEff(int comboCount)
	{
		if (SF_ComboList != null && comboCount < SF_ComboList.Length)
		{
			return PlaySoundEff(SF_ComboList[comboCount]);
		}
		return null;
	}

	public AudioSource PlaySpellEff(int spellNum)
	{
		if (SF_SpellList != null && spellNum < SF_SpellList.Length)
		{
			return PlaySoundEff(SF_SpellList[spellNum]);
		}
		return null;
	}

	public void StopAllEffect()
	{
		for (int i = 0; i < eff_audio_list.Count; i++)
		{
			AudioSource audioSource = eff_audio_list[i];
			audioSource.Stop();
		}
	}

	public void StopEffect(AudioClip clip)
	{
		if (clip == null)
		{
			return;
		}
		for (int i = 0; i < eff_audio_list.Count; i++)
		{
			AudioSource audioSource = eff_audio_list[i];
			if (!(audioSource == null) && !(audioSource.clip == null) && audioSource.clip.name.Equals(clip.name))
			{
				audioSource.Stop();
			}
		}
	}

	public void SetEffVudio(bool isOpen)
	{
		if (isOpen)
		{
			SetEffOpen();
		}
		else
		{
			SetEffClose();
		}
	}

	public void SetBgVudio(bool isOpen)
	{
		if (isOpen)
		{
			SetBgOpen();
		}
		else
		{
			SetBgClose();
		}
	}

	private void SetBgClose()
	{
		if (Bg_Audio.volume != 0f)
		{
			isMusicOpen = false;
			SetBgVolume(0f);
			StopBG();
		}
	}

	private void SetBgOpen()
	{
		isMusicOpen = true;
		SetBgVolume(bg_volume);
		PlayBG(Music_Main1);
	}

	private void SetEffClose()
	{
		isSoundEffOpen = false;
		StopAllEffect();
	}

	private void SetEffOpen()
	{
		isSoundEffOpen = true;
	}

	public void SetBgVolume(float v)
	{
		Bg_Audio.volume = v;
	}

	public void OnDestroy()
	{
		_instance = null;
	}
}
