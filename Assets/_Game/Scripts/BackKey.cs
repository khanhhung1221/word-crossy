using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BackKey : MonoBehaviour
{
	[Serializable]
	public class OnClosePanelEvent : UnityEvent
	{
	}

	public bool m_IsDestory = true;

	public bool m_DestoryAni;

	public GameObject CurCloseObj;

	public OnClosePanelEvent OnClosePanel;

	private void OnDisable()
	{
		if (base.gameObject.name != "MainUI" && base.gameObject.name != "LevelMapUI")
		{
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WindowClose);
		}
		WordGlobal.BackKeyObjList.Remove(base.gameObject);
		OnClosePanel.Invoke();
	}

	private void OnEnable()
	{
		if (base.gameObject.name != "MainUI" && base.gameObject.name != "LevelMapUI")
		{
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WindowOpen);
		}
		WordGlobal.BackKeyObjList.Add(base.gameObject);
	}

	public void SetCloseEvent(List<UnityAction> calls)
	{
		int num = 0;
		while (calls != null && num < calls.Count)
		{
			OnClosePanel.AddListener(calls[num]);
			num++;
		}
	}

	private void Update()
	{
	}

	public bool IsTopCanvas()
	{
		Canvas componentInChildren = GetComponentInChildren<Canvas>();
		if (componentInChildren == null)
		{
			return false;
		}
		int sortingOrder = componentInChildren.sortingOrder;
		if (sortingOrder >= 100)
		{
			return false;
		}
		int num = CanvasSortOrderSetting.FindMaxOrderForBackKey();
		return sortingOrder >= num;
	}

	public void BtnCloseClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}
}
