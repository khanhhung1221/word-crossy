//using Facebook.Unity;
using System;
using UnityEngine;

public class BackKeyManager : MonoBehaviour
{
	private BackKeyManager instance;

	private void Start()
	{
	}

	private void Update()
	{
		if (PlayerInfo.Instance.TutorialStep == 999 && !DPCTutorialUI.isDPCTutorialShow && Input.GetKeyUp(KeyCode.Escape) && !WordGlobal.DisableBackKey)
		{
			if (WordGlobal.BackKeyObjList != null && WordGlobal.BackKeyObjList.Count > 1)
			{
				GameObject gameObject = WordGlobal.BackKeyObjList[WordGlobal.BackKeyObjList.Count - 1];
				if (gameObject.name == "Result" || gameObject.name == "LevelMapUI")
				{
					return;
				}
				if (gameObject != null)
				{
					if (WordGlobal.UI_CurrentUI == "ChapterImageUI")
					{
						BtnsCtrlUIScript.Instance.BtnMapClick();
						return;
					}
					if (WordGlobal.UI_CurrentUI == "MailUI")
					{
						BtnsCtrlUIScript.Instance.BtnMailClick();
						return;
					}
					if (!(WordGlobal.UI_CurrentUI == "DailyRewardUI"))
					{
						if (WordGlobal.UI_CurrentUI == "RankUI")
						{
							RankUIScript.Instance.BtnRankClose();
							return;
						}
						if (WordGlobal.UI_CurrentUI == "DailyQuestRewardUI")
						{
							DailyQuestRewardUIScript.Instance.BtnCloseClick();
							return;
						}
						if (WordGlobal.UI_CurrentUI == "TutorialWatchVideo")
						{
							TutorialWatchVideo.BtnClick = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "TutorialHuoDongTournament03")
						{
							TutorialHuoDongTournament03.isHide = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "HuoDongUI")
						{
							HuoDongUI.isHide = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "HuoDongGardonTimeOut")
						{
							HuoDongGardonTimeOut.isBtnGiveUpClick = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "ContactUsUI")
						{
							UIManager.Instance.LoadMainUI("UI/ContactUsConfirmUI", isPop: true);
							return;
						}
						if (WordGlobal.UI_CurrentUI == "HuoDongGardonUI")
						{
							HuoDongGardonUI.isBtnCloseClick = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "HuoDongSunUI")
						{
							HuoDongSunUI.isBtnCloseClick = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "PiggyBankUI")
						{
							PiggyBankUI.isCloseClick = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "HuoDongWordSearch")
						{
							HuoDongWordSearch.isCloseClick = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "HuoDongWordSearchUI")
						{
							HuoDongWordSearchUI.isCloseClick = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "HuoDongWordSearchTutorialUI")
						{
							HuoDongWordSearchTutorialUI.Tutorial3Click = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "HuoDongWordSearchResult" || WordGlobal.UI_CurrentUI == "DPCTutorialUI")
						{
							return;
						}
						if (WordGlobal.UI_CurrentUI == "HuoDongWordFind")
						{
							HuoDongWordFind.isBackKey = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "LuckyDrawUI")
						{
							LuckyDrawUI.isHide = true;
							return;
						}
						if (WordGlobal.UI_CurrentUI == "DailyQuestTutorial")
						{
							if (MainUIScript.Instance != null)
							{
								MainUIScript.Instance.SetDailyQuestShow(isShow: true);
							}
							return;
						}
					}
					if (WordGlobal.UI_CurrentUI == WordGlobal.UI_MAIN)
					{
						if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
						{
							MainUIScript.Instance.BtnCloseClick();
						}
						else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
						{
							MainUIScript.Instance.BackToPuzzleCollect();
						}
						else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
						{
							MainUIScript.Instance.BtnCloseClick();
						}
						else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
						{
							MainUIScript.Instance.BtnCloseClick();
						}
						else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
						{
							MainUIScript.Instance.BtnCloseClick();
						}
						return;
					}
					BackKey component = gameObject.GetComponent<BackKey>();
					if (component.m_IsDestory)
					{
						if (component.CurCloseObj != null)
						{
							if (component.m_DestoryAni)
							{
								WordGlobal.CloseUIAni(component.CurCloseObj.transform);
							}
							else
							{
								UnityEngine.Object.Destroy(component.CurCloseObj);
							}
						}
						else if (component.m_DestoryAni)
						{
							WordGlobal.CloseUIAni(gameObject.transform);
						}
						else
						{
							UnityEngine.Object.Destroy(gameObject);
						}
					}
					else if (component.CurCloseObj != null)
					{
						component.CurCloseObj.SetActive(value: false);
					}
					else
					{
						gameObject.SetActive(value: false);
					}
				}
				else
				{
					WordGlobal.BackKeyObjList.Remove(gameObject);
				}
			}
			else if (WordGlobal.BackKeyObjList.Count <= 1 && WordGlobal.BackKeyObjList.Count <= 1)
			{
				if ((WordGlobal.UI_CurrentUI == WordGlobal.UI_LevelMap || WordGlobal.UI_CurrentUI == string.Empty) && !DailyRewardUI_New.isShow)
				{
					ResourceLoadManager.GetInstance().ShowConfirmTipsUI();
				}
				if (!(WordGlobal.UI_CurrentUI == WordGlobal.UI_MAIN) || WordGlobal.BackKeyObjList.Count != 1)
				{
					return;
				}
				if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
				{
					MainUIScript.Instance.BtnCloseClick();
				}
				else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
				{
					MainUIScript.Instance.BackToPuzzleCollect();
				}
				else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
				{
					if (!MainUIScript.Instance.isShowResult())
					{
						MainUIScript.Instance.BtnCloseClick();
					}
				}
				else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
				{
					MainUIScript.Instance.BtnCloseClick();
				}
				else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
				{
					MainUIScript.Instance.BtnCloseClick();
				}
				return;
			}
		}
		if (LevelMapUIScript.Instance != null)
		{
			if (WordGlobal.BackKeyObjList.Count > 1 && WordGlobal.IsShowLevelMapEff)
			{
				LevelMapUIScript.Instance.SetAllItemEffect(isShow: false);
				WordGlobal.IsShowLevelMapEff = false;
			}
			else if (WordGlobal.BackKeyObjList.Count == 1 && !WordGlobal.IsShowLevelMapEff)
			{
				LevelMapUIScript.Instance.SetAllItemEffect(isShow: true);
				WordGlobal.IsShowLevelMapEff = true;
			}
		}
	}

	public void OnApplicationPause(bool pause)
	{
		//if (!pause)
		//{
		//	if (FB.IsInitialized)
		//	{
		//		FB.ActivateApp();
		//	}
		//	else
		//	{
		//		FB.Init(delegate
		//		{
		//			FB.ActivateApp();
		//		});
		//	}
		//}
		if (pause)
		{
			PlayerPrefs.SetString("UserPauseTime", DateTime.Now.Ticks.ToString());
		}
		else if (PlayerPrefs.GetInt("FirstLoading", 0) == 1)
		{
			PlayerPrefs.SetInt("FirstLoading", 0);
		}
		else if (!(PlayerPrefs.GetString("UserPauseTime") == string.Empty))
		{
			DateTime d = new DateTime(long.Parse(PlayerPrefs.GetString("UserPauseTime")));
			if (!((DateTime.Now - d).TotalSeconds >= 120.0) || PlayerInfo.Instance.AdRemove != 1)
			{
			}
			WordFaceBook.Instance.GetHuoDongData();
		}
	}

	private void Awake()
	{
		if (instance != null)
		{
			UnityEngine.Object.DestroyImmediate(base.gameObject);
			return;
		}
		instance = this;
		UnityEngine.Object.DontDestroyOnLoad(this);
	}
}
