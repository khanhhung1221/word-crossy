using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Bezier : MonoBehaviour
{
	public Transform[] controlPoints;

	public LineRenderer lineRenderer;

	private int layerOrder;

	private int _segmentNum = 50;

	private float CTRL_VALUE_A = 0.2f;

	private float CTRL_VALUE_B = 0.2f;

	private void Start()
	{
		if (!lineRenderer)
		{
			lineRenderer = GetComponent<LineRenderer>();
		}
		lineRenderer.sortingLayerID = layerOrder;
	}

	private void Update()
	{
		DrawCurve();
	}

	private void DrawCurve()
	{
		for (int i = 1; i <= _segmentNum; i++)
		{
			float t = (float)i / (float)_segmentNum;
			int num = 0;
			Vector3 position = CalculateCubicBezierPoint(t, controlPoints[num].position, controlPoints[num + 1].position, controlPoints[num + 2].position);
			lineRenderer.positionCount = i;
			lineRenderer.SetPosition(i - 1, position);
		}
		for (int j = _segmentNum + 1; j <= _segmentNum * 2 + 1; j++)
		{
			float t2 = (float)(j - _segmentNum) / (float)_segmentNum;
			int num2 = 2;
			Vector3 position2 = CalculateCubicBezierPoint(t2, controlPoints[num2].position, controlPoints[num2 + 1].position, controlPoints[num2 + 2].position);
			lineRenderer.positionCount = j;
			lineRenderer.SetPosition(j - 1, position2);
		}
	}

	private Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
	{
		float num = 1f - t;
		float d = t * t;
		float d2 = num * num;
		Vector3 a = d2 * p0;
		a += 2f * num * t * p1;
		a += d * p2;
		return new Vector3(a.x, a.y, 0f);
	}

	public void preparePoints(List<Vector3> pointFList)
	{
		Vector3 vector = pointFList[0];
		float x = vector.x;
		Vector3 vector2 = pointFList[0];
		pointFList.Insert(0, new Vector3(x, vector2.y));
		Vector3 vector3 = pointFList[pointFList.Count - 1];
		float x2 = vector3.x;
		Vector3 vector4 = pointFList[pointFList.Count - 1];
		pointFList.Add(new Vector3(x2, vector4.y));
		Vector3 vector5 = pointFList[pointFList.Count - 1];
		float x3 = vector5.x;
		Vector3 vector6 = pointFList[pointFList.Count - 1];
		pointFList.Add(new Vector3(x3, vector6.y));
		for (int i = 1; i < pointFList.Count - 3; i++)
		{
			Vector3 ctrlPointA = default(Vector3);
			Vector3 ctrlPointB = default(Vector3);
			getCtrlPoint(pointFList, i, out ctrlPointA, out ctrlPointB);
		}
	}

	private void getCtrlPoint(List<Vector3> pointFList, int currentIndex, out Vector3 ctrlPointA, out Vector3 ctrlPointB)
	{
		ctrlPointA = default(Vector3);
		ctrlPointB = default(Vector3);
		Vector3 vector = pointFList[currentIndex];
		float x = vector.x;
		Vector3 vector2 = pointFList[currentIndex + 1];
		float x2 = vector2.x;
		Vector3 vector3 = pointFList[currentIndex - 1];
		ctrlPointA.x = x + (x2 - vector3.x) * CTRL_VALUE_A;
		Vector3 vector4 = pointFList[currentIndex];
		float y = vector4.y;
		Vector3 vector5 = pointFList[currentIndex + 1];
		float y2 = vector5.y;
		Vector3 vector6 = pointFList[currentIndex - 1];
		ctrlPointA.y = y + (y2 - vector6.y) * CTRL_VALUE_A;
		Vector3 vector7 = pointFList[currentIndex + 1];
		float x3 = vector7.x;
		Vector3 vector8 = pointFList[currentIndex + 2];
		float x4 = vector8.x;
		Vector3 vector9 = pointFList[currentIndex];
		ctrlPointB.x = x3 - (x4 - vector9.x) * CTRL_VALUE_B;
		Vector3 vector10 = pointFList[currentIndex + 1];
		float y3 = vector10.y;
		Vector3 vector11 = pointFList[currentIndex + 2];
		float y4 = vector11.y;
		Vector3 vector12 = pointFList[currentIndex];
		ctrlPointB.y = y3 - (y4 - vector12.y) * CTRL_VALUE_B;
	}
}
