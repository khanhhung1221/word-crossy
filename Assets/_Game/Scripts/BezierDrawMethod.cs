using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

[RequireComponent(typeof(UILineTextureRenderer))]
public class BezierDrawMethod : MonoBehaviour
{
	public UILineTextureRenderer lineRenderer;

	private static float CTRL_VALUE_A = 0.2f;

	private static float CTRL_VALUE_B = 0.2f;

	public Transform[] temList;

	private int layerOrder;

	private Transform _myTransfrom;

	private void Start()
	{
		_myTransfrom = base.transform;
		if (!lineRenderer)
		{
			lineRenderer = GetComponent<UILineTextureRenderer>();
		}
		InitData();
	}

	public void InitData()
	{
		temList = new Transform[18];
		List<Vector3> list = new List<Vector3>();
		for (int i = 0; i < _myTransfrom.childCount; i++)
		{
			Transform child = _myTransfrom.GetChild(i);
			temList[i] = child;
			Vector3 position = child.position;
			position.z = 0f;
			list.Add(position);
		}
		PreparePoints(list);
	}

	private void Update()
	{
	}

	private void savePathBezier(List<Vector3> pointFList)
	{
		if (pointFList == null || pointFList.Count == 0)
		{
			return;
		}
		int count = pointFList.Count;
		List<Vector2> list = new List<Vector2>();
		int num = 0;
		for (int i = 1; i < count - 3; i++)
		{
			for (float num2 = 0f; num2 < 1f; num2 += 0.01f)
			{
				num++;
				Vector3 ctrlPointA = default(Vector3);
				Vector3 ctrlPointB = default(Vector3);
				getCtrlPoint(pointFList, i, out ctrlPointA, out ctrlPointB);
				Vector3 point = GetPoint(pointFList[i], ctrlPointA, ctrlPointB, pointFList[i + 1], num2);
				list.Add(point);
			}
		}
		lineRenderer.Points = list.ToArray();
	}

	public void PreparePoints(List<Vector3> pointList)
	{
		if (pointList.Count >= 2)
		{
			Vector3 vector = pointList[0];
			float x = vector.x;
			Vector3 vector2 = pointList[0];
			pointList.Insert(0, new Vector3(x, vector2.y));
			Vector3 vector3 = pointList[pointList.Count - 1];
			float x2 = vector3.x;
			Vector3 vector4 = pointList[pointList.Count - 1];
			pointList.Add(new Vector3(x2, vector4.y));
			Vector3 vector5 = pointList[pointList.Count - 1];
			float x3 = vector5.x;
			Vector3 vector6 = pointList[pointList.Count - 1];
			pointList.Add(new Vector3(x3, vector6.y));
			savePathBezier(pointList);
		}
	}

	private void getCtrlPoint(List<Vector3> pointFList, int currentIndex, out Vector3 ctrlPointA, out Vector3 ctrlPointB)
	{
		ctrlPointA = default(Vector3);
		ctrlPointB = default(Vector3);
		Vector3 vector = pointFList[currentIndex];
		float x = vector.x;
		Vector3 vector2 = pointFList[currentIndex + 1];
		float x2 = vector2.x;
		Vector3 vector3 = pointFList[currentIndex - 1];
		ctrlPointA.x = x + (x2 - vector3.x) * CTRL_VALUE_A;
		Vector3 vector4 = pointFList[currentIndex];
		float y = vector4.y;
		Vector3 vector5 = pointFList[currentIndex + 1];
		float y2 = vector5.y;
		Vector3 vector6 = pointFList[currentIndex - 1];
		ctrlPointA.y = y + (y2 - vector6.y) * CTRL_VALUE_A;
		Vector3 vector7 = pointFList[currentIndex + 1];
		float x3 = vector7.x;
		Vector3 vector8 = pointFList[currentIndex + 2];
		float x4 = vector8.x;
		Vector3 vector9 = pointFList[currentIndex];
		ctrlPointB.x = x3 - (x4 - vector9.x) * CTRL_VALUE_B;
		Vector3 vector10 = pointFList[currentIndex + 1];
		float y3 = vector10.y;
		Vector3 vector11 = pointFList[currentIndex + 2];
		float y4 = vector11.y;
		Vector3 vector12 = pointFList[currentIndex];
		ctrlPointB.y = y3 - (y4 - vector12.y) * CTRL_VALUE_B;
	}

	public Vector3 GetPoint(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
	{
		t = Mathf.Clamp01(t);
		float num = 1f - t;
		return num * num * num * p0 + 3f * num * num * t * p1 + 3f * num * t * t * p2 + t * t * t * p3;
	}
}
