using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BonusUIScript : MonoBehaviour
{
	public Text m_TxtBonusList;

	public GameObject m_ObjBtns;

	public GameObject m_ObjItem;

	public Transform m_TfRoot;

	public Image m_ImgProcess;

	public GameObject m_ObjBtnVideo;

	public GameObject m_ObjBtnClaim;

	public GameObject m_ObjBtnNoVideoClaim;

	public Text m_TxtBonusAwardCount;

	public Text m_TxtProcess;

	private MainUIAniControlScript _effectControl;

	private bool _showBtns = true;

	private void Start()
	{
		_showBtns = true;
	}

	private void Update()
	{
		m_ObjBtnVideo.SetActive(value: false);
		m_ObjBtnClaim.SetActive(value: false);
		m_ObjBtnNoVideoClaim.SetActive(value: true);
	}

	private void Awake()
	{
		WordGlobal.InitLabel(base.transform);
		_effectControl = MainUIScript.Instance.GetComponent<MainUIAniControlScript>();
		m_TxtBonusAwardCount.text = "+" + PlayerInfo.Instance.BonusGoldNumber;
	}

	public void BtnCloseClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void SetContent(List<string> bonusList)
	{
		string empty = string.Empty;
		for (int i = 0; i < bonusList.Count; i++)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(m_ObjItem);
			gameObject.transform.SetParent(m_TfRoot, worldPositionStays: false);
			gameObject.SetActive(value: true);
			gameObject.GetComponent<Text>().text = bonusList[i];
		}
		m_ImgProcess.fillAmount = (float)PlayerInfo.Instance.CollectedBonusCount / (float)PlayerInfo.Instance.MaxBonusCount;
		m_ObjBtns.SetActive(PlayerInfo.Instance.CollectedBonusCount >= PlayerInfo.Instance.MaxBonusCount);
		m_TxtProcess.text = PlayerInfo.Instance.CollectedBonusCount + "/" + PlayerInfo.Instance.MaxBonusCount;
	}

	private void ClaimBonus()
	{
		PlayerInfo.Instance.CollectedBonusCount -= PlayerInfo.Instance.MaxBonusCount;
		PlayerInfo.Instance.GetBonusRewardCount++;
		MainUIScript.Instance.InitBonusEffect();
	}

	public void BtnClaimClick()
	{
		MainUIScript.Instance.AddGold(PlayerInfo.Instance.BonusGoldNumber, 1f);
		MainUIScript.Instance._effectControl.CollectGold(((!AdsManager.Instance.IncentivizedAdState) ? m_ObjBtnNoVideoClaim : m_ObjBtnClaim).transform.position, 5, 1f, null);
		ClaimBonus();
		_showBtns = false;
		base.transform.DOLocalMoveZ(0f, 0.3f).OnComplete(delegate
		{
			UnityEngine.Object.Destroy(base.gameObject);
		});
	}

	public void BtnWatchVideoClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
		if (AdsManager.Instance.ShowIncentivizedAd("BonusUIScript"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEvent));
		}
		ClaimBonus();
	}

	public void WatchVideoEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEvent));
		if (ret)
		{
			CommonTipsScript.Instance.ShowUI(string.Format(WordGlobal.ReadText(1017), PlayerInfo.Instance.BonusGoldNumber + 25), 0, WordGlobal.ReadText(1016), delegate(object obj)
			{
				Vector3 starPos = (Vector3)obj;
				GoldScript goldScript = UnityEngine.Object.FindObjectOfType<GoldScript>();
				goldScript.CollectGold(starPos, 5, 1f, null);
				goldScript.AddGold(PlayerInfo.Instance.BonusGoldNumber + 25, 1f, 1f, isAdd2PlayerInfo: true, 13);
			}, showClose: false);
		}
		else
		{
			PlayerInfo.Instance.AddGold(PlayerInfo.Instance.BonusGoldNumber, 11);
		}
	}

	public void WordClick(Text txt)
	{
		DictionaryUIScript.ShowUI(txt.text);
	}
}
