using UnityEngine;
using UnityEngine.UI;

public class BtnDailyQuest : MonoBehaviour
{
	public GameObject DailyQuestTips;

	public GameObject DailyQuestTipsNum;

	public Text DailyQuestTipsNum_text;

	private void Start()
	{
	}

	private void Update()
	{
		int num = PlayerInfo.Instance.GetDailyQuestSaveData().isCanClaim();
		if (num > 0)
		{
			DailyQuestTipsNum.SetActive(value: true);
			DailyQuestTipsNum_text.text = num.ToString();
			DailyQuestTips.SetActive(value: false);
			return;
		}
		DailyQuestTipsNum.SetActive(value: false);
		if (PlayerInfo.Instance.GetLittleTipsData().dailyQuest_open == 0)
		{
			DailyQuestTips.SetActive(value: true);
		}
		else
		{
			DailyQuestTips.SetActive(value: false);
		}
	}
}
