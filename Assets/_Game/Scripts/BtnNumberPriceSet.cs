using UnityEngine;
using UnityEngine.UI;

public class BtnNumberPriceSet : MonoBehaviour
{
	public GameObject m_ObjPrice;

	public GameObject m_ObjBuffer;

	public GameObject m_ObjNumber;

	public GameObject m_ObjLock;

	public Text m_TxtCount;

	public Text m_TxtPrice;

	public MainBtnsType BtnsType;

	public Animator m_BtnAni;

	private int _number;

	private long _price;

	private int _locked;

	private bool _showCloseTip;

	private void Start()
	{
	}

	private void Awake()
	{
		Init();
	}

	private void Update()
	{
		Init();
	}

	private void Init()
	{
		switch (BtnsType)
		{
		case MainBtnsType.AllFirstLetters:
			_number = PlayerInfo.Instance.FirstTipsCount;
			if (PlayerInfo.Instance.TutorialStep != 999)
			{
				_number = 0;
			}
			_price = PlayerInfo.Instance.systemConfig.GetFirstLetterTipsPrice();
			_locked = PlayerInfo.Instance.FirstTipButtonUnlock;
			break;
		case MainBtnsType.Tips:
			_number = PlayerInfo.Instance.TipsCount;
			_price = PlayerInfo.Instance.systemConfig.GetTipsPrice();
			break;
		case MainBtnsType.TipsClick:
			_number = PlayerInfo.Instance.ClickTipsCount;
			if (PlayerInfo.Instance.TutorialStep != 999)
			{
				_number = 0;
			}
			_price = PlayerInfo.Instance.systemConfig.ClickTipsPrice;
			break;
		}
		m_ObjNumber.SetActive(_number > 0 && !_showCloseTip);
		m_ObjPrice.SetActive(_number <= 0 && !_showCloseTip);
		if (m_ObjLock != null)
		{
			m_ObjLock.SetActive(value: false);
			if (_locked == 0)
			{
				m_ObjNumber.SetActive(value: false);
			}
		}
		m_TxtCount.text = _number.ToString();
		m_TxtPrice.text = _price.ToString();
	}

	public void SetShakeAni(int state)
	{
		switch (state)
		{
		case 1:
			m_BtnAni.SetTrigger("Remind2");
			break;
		case 2:
			m_BtnAni.SetTrigger("RemindExit");
			break;
		}
	}

	public void ShowClostTip(bool show)
	{
		_showCloseTip = show;
	}
}
