using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BtnVideoScript : MonoBehaviour
{
	public static float dp_wait_showTime;

	public static int UI_INOUT;

	public static bool isStop;

	public RectTransform out_rect_tran;

	public RectTransform rect_tran;

	public Image bg_img;

	public Image icon_img;

	public Transform Gold_tran;

	public Transform DengPao_tran;

	public GameObject video_eff_go;

	public Button btn;

	public Sprite bg_video_sp;

	public Sprite bg_dp_sp;

	public Sprite icon_video_sp;

	public Sprite icon_dp_sp;

	public int Cur_Type = -1;

	private int TYPE_VIDEO;

	private int TYPE_DP = 1;

	private int cur_level;

	private int cur_state;

	public float wait_time;

	private float out_x = 150f;

	private float in_x = -10f;

	private float anim_time = 0.5f;

	private bool isClickBtnWatchVideo;

	private int MAXLEVEL_SHOW_DP = 18;

	private int SHOW_DP_TIME = 30;

	public bool isAlwaysVideoBtn;

	private float hide_icon_img_time;

	public static bool isClearWaitTime;

	private int inout_num = -1;

	private float stop_waitTime;

	private void Start()
	{
		bg_img = base.transform.GetComponent<Image>();
		rect_tran = bg_img.rectTransform;
		icon_img = base.transform.Find("Gold/Image").GetComponent<Image>();
		btn = base.transform.GetComponent<Button>();
		RectTransform rectTransform = rect_tran;
		float x = out_x;
		Vector2 anchoredPosition = rect_tran.anchoredPosition;
		rectTransform.anchoredPosition = new Vector2(x, anchoredPosition.y);
		cur_level = PlayerInfo.Instance.CurrentLevel;
		isClickBtnWatchVideo = false;
		SetType(TYPE_VIDEO);
		if (isAlwaysVideoBtn)
		{
			RectTransform rectTransform2 = rect_tran;
			float x2 = in_x;
			Vector2 anchoredPosition2 = rect_tran.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(x2, anchoredPosition2.y);
		}
	}

	public void SetType(int type, bool isReset = false)
	{
		if ((isAlwaysVideoBtn && type == TYPE_DP) || ((PlayerInfo.Instance.CurrentLevel <= MAXLEVEL_SHOW_DP || UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament) && type == TYPE_DP) || (Cur_Type == type && !isReset))
		{
			return;
		}
		Cur_Type = type;
		if (Cur_Type == TYPE_DP)
		{
			WordGlobal.LogEvent("HintAD_Shown");
			rect_tran.DOAnchorPosX(out_x, anim_time).OnComplete(ChangeTypeFinish);
		}
		else if (Cur_Type == TYPE_VIDEO)
		{
			if (isAlwaysVideoBtn)
			{
				ChangeTypeFinish();
			}
			else if (isClickBtnWatchVideo)
			{
				ChangeTypeFinish();
				rect_tran.DOAnchorPosX(in_x, anim_time).OnComplete(delegate
				{
					SetState(1);
				});
				WordGlobal.LogEvent("Adbutton_Shown");
			}
			else
			{
				rect_tran.DOAnchorPosX(out_x, anim_time).OnComplete(ChangeTypeFinish);
			}
		}
	}

	public void ChangeTypeFinish()
	{
		if (Cur_Type == TYPE_VIDEO)
		{
			Gold_tran.gameObject.SetActive(value: true);
			DengPao_tran.gameObject.SetActive(value: false);
			bg_img.sprite = bg_video_sp;
			icon_img.sprite = icon_video_sp;
			btn.onClick.RemoveAllListeners();
			btn.onClick.AddListener(BtnWatchVideo);
		}
		else if (Cur_Type == TYPE_DP)
		{
			Gold_tran.gameObject.SetActive(value: false);
			DengPao_tran.gameObject.SetActive(value: true);
			bg_img.sprite = bg_dp_sp;
			btn.onClick.RemoveAllListeners();
			btn.onClick.AddListener(BtnDengPao);
			rect_tran.DOAnchorPosX(in_x, anim_time);
		}
		SetState(0);
	}

	public void BtnWatchVideo()
	{
		isClickBtnWatchVideo = true;
		hide_icon_img_time = 1f;
		MainUIScript.Instance.BtnWatchVideo();
	}

	public void BtnDengPao()
	{
		MainUIScript.Instance.BtnDengPaoVideo();
		SetType(TYPE_VIDEO, isReset: true);
	}

	public void SetState(int state)
	{
		cur_state = state;
		wait_time = 0f;
		if (cur_state == 0)
		{
			if (isAlwaysVideoBtn)
			{
				RectTransform rectTransform = rect_tran;
				float x = in_x;
				Vector2 anchoredPosition = rect_tran.anchoredPosition;
				rectTransform.anchoredPosition = new Vector2(x, anchoredPosition.y);
			}
			else
			{
				RectTransform rectTransform2 = rect_tran;
				float x2 = out_x;
				Vector2 anchoredPosition2 = rect_tran.anchoredPosition;
				rectTransform2.anchoredPosition = new Vector2(x2, anchoredPosition2.y);
			}
		}
		else if (cur_state != 1)
		{
		}
	}

	public void ClearVideoWaitTime()
	{
		wait_time = 0f;
		isClickBtnWatchVideo = true;
		SetType(TYPE_VIDEO, isReset: true);
	}

	private void Update()
	{
		if (isStop)
		{
			stop_waitTime += Time.deltaTime;
			if (stop_waitTime > 10f)
			{
				isStop = false;
			}
			RectTransform rectTransform = rect_tran;
			float x = out_x;
			Vector2 anchoredPosition = rect_tran.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(x, anchoredPosition.y);
			return;
		}
		stop_waitTime = 0f;
		if (isClearWaitTime)
		{
			ClearVideoWaitTime();
			isClearWaitTime = false;
		}
		if (inout_num != UI_INOUT)
		{
			if (out_rect_tran != null)
			{
				if (UI_INOUT == 1)
				{
					out_rect_tran.DOAnchorPosX(450f, 0.2f);
				}
				else
				{
					out_rect_tran.DOAnchorPosX(0f, 0.5f);
				}
			}
			inout_num = UI_INOUT;
		}
		if (cur_level != PlayerInfo.Instance.CurrentLevel)
		{
			cur_level = PlayerInfo.Instance.CurrentLevel;
			isClickBtnWatchVideo = false;
			SetType(TYPE_VIDEO, isReset: true);
			return;
		}
		if (PlayerInfo.Instance.CurrentLevel > MAXLEVEL_SHOW_DP)
		{
			if (MainUIScript.Instance != null && !MainUIScript.Instance.isShowResult())
			{
				dp_wait_showTime += Time.deltaTime;
			}
			if (dp_wait_showTime > (float)SHOW_DP_TIME)
			{
				SetType(TYPE_DP);
				dp_wait_showTime = 0f;
			}
			if (WordGlobal.WordSelectWrongCount >= 2)
			{
				SetType(TYPE_DP);
				WordGlobal.WordSelectWrongCount = 0;
			}
		}
		if (Cur_Type == TYPE_VIDEO)
		{
			icon_img.gameObject.SetActive(value: true);
			if (isClickBtnWatchVideo)
			{
				RectTransform rectTransform2 = rect_tran;
				float x2 = in_x;
				Vector2 anchoredPosition2 = rect_tran.anchoredPosition;
				rectTransform2.anchoredPosition = new Vector2(x2, anchoredPosition2.y);
			}
			else
			{
				if (isAlwaysVideoBtn)
				{
					return;
				}
				if (MainUIScript.Instance != null && !MainUIScript.Instance.isShowResult())
				{
					wait_time += Time.deltaTime;
				}
				if (cur_state == 0)
				{
					if (wait_time >= 5f)
					{
						rect_tran.DOAnchorPosX(in_x, anim_time).OnComplete(delegate
						{
							SetState(1);
						});
						wait_time = 0f;
					}
				}
				else if (cur_state == 1 && wait_time >= 15f)
				{
					rect_tran.DOAnchorPosX(out_x, anim_time).OnComplete(delegate
					{
						SetState(0);
					});
					wait_time = 0f;
				}
			}
		}
		else if (Cur_Type != TYPE_DP)
		{
		}
	}
}
