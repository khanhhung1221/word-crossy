using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

public class BtnsCtrlUIScript : MonoBehaviour
{
	public GameObject BlackBgBtn;

	public GameObject HideBtnsTrans;

	public Button MapBtn;

	public Button MailBtn;

	public Button DailyRewardBtn;

	public GameObject FaceLoginBtn;

	public Image MapBtnBgImg;

	public Image MailBtnBgImg;

	public Image DailyRewardBtnBgImg;

	public GameObject DailyTipsObj;

	public GameObject MailTipsObj;

	public GameObject DailyQuestBtnObj;

	public GameObject DailyQuestTips;

	public GameObject DailyQuestTipsNum;

	public Text DailyQuestTipsNum_text;

	public Image DailyQuestIcon_img;

	public Text DailyQuestText;

	public static BtnsCtrlUIScript Instance;

	public static bool isShowPopOffer = true;

	public static bool isShowDailyReward = true;

	private int isNeedShowDailyReward = 1;

	private bool _toShowDailyReward = true;

	private float ImgWidth;

	private GameObject TargetObj;

	private float RightMostX;

	private bool toShowDailyReward
	{
		get
		{
			return _toShowDailyReward;
		}
		set
		{
			_toShowDailyReward = value;
		}
	}

	private void Awake()
	{
		Instance = this;
		if (isShowPopOffer)
		{
			PopOfferUIScript.ShowUI();
		}
	}

	private void OnEnable()
	{
		if (isNeedShowDailyReward > 0 && PlayerInfo.Instance.TutorialStep >= 999 && PlayerInfo.Instance.IsGetDailyReward % 10 == 0)
		{
			if (!isShowDailyReward)
			{
				isShowDailyReward = true;
			}
			else
			{
				toShowDailyReward = true;
			}
		}
	}

	private void Start()
	{
		if (PlayerInfo.Instance.TutorialStep >= 999 && PlayerInfo.Instance.IsGetDailyReward != 0)
		{
			int day = DateTime.Now.Day;
			if (day != PlayerInfo.Instance.LastDailyRewardUtcTime)
			{
				PlayerInfo.Instance.IsGetDailyReward = 0;
			}
		}
		ImgWidth = 588f;
		if (PlayerInfo.Instance.LoggedFaceBook == 1)
		{
			FaceLoginBtn.SetActive(value: false);
		}
		else
		{
			Debug.Log("Facebook button Hide");
			//FaceLoginBtn.SetActive(value: true);
		}
	}

	public void SetMailTips(bool isShow)
	{
		if (isShow)
		{
			MailTipsObj.SetActive(value: true);
		}
		else
		{
			MailTipsObj.SetActive(value: false);
		}
	}

	public void SetFBBtn()
	{
		if (PlayerInfo.Instance.LoggedFaceBook == 1)
		{
			FaceLoginBtn.SetActive(value: false);
		}
		else
		{
			FaceLoginBtn.SetActive(value: true);
		}
	}

	private void AutoGetReward()
	{
		if (!(MainUIScript.Instance != null) && PlayerInfo.Instance.IsGetDailyReward % 10 != 1)
		{
			isNeedShowDailyReward = 0;
			BtnDailyRewardClick();
		}
	}

	private void Update()
	{
		bool flag = false;
		if (PlayerInfo.Instance.IsGetDailyReward == 0)
		{
			flag = false;
		}
		if (PlayerInfo.Instance.IsGetDailyReward % 10 == 0)
		{
			DailyTipsObj.SetActive(value: true);
		}
		else
		{
			DailyTipsObj.SetActive(value: false);
		}
		int num = PlayerInfo.Instance.GetDailyQuestSaveData().isCanClaim();
		if (num > 0)
		{
			if (!HuoDongWordSearchUI.isShow)
			{
				if (DPCTutorialTip.isDPCTutorialGoto)
				{
					DPCTutorialTip.isDPCTutorialGoto = false;
				}
				else if (PlayerInfo.Instance.MaxPlayLevel >= 5 && PlayerInfo.Instance.ShowTutorialDailyQuestFinish == 0)
				{
					PlayerInfo.Instance.ShowTutorialDailyQuestFinish = 1;
					UIManager.Instance.LoadMainUI("UI/Main/TutorialDailyQuestFinish", isPop: false, UIManager.MainUIType.Type_Common, 100);
				}
				DailyQuestIcon_img.color = Color.white;
				DailyQuestText.color = Color.white;
				DailyQuestTipsNum.SetActive(value: true);
				DailyQuestTipsNum_text.text = num.ToString();
				DailyQuestTips.SetActive(value: false);
			}
		}
		else
		{
			DailyQuestTipsNum.SetActive(value: false);
			if (PlayerInfo.Instance.MaxPlayLevel < WordGlobal.OpenDailyQuestLevel)
			{
				DailyQuestIcon_img.color = new Color(133f / 255f, 133f / 255f, 133f / 255f);
				DailyQuestText.color = WordGlobal.ChangeStringToColor("#a9a9a9");
				DailyQuestTips.SetActive(value: false);
			}
			else
			{
				DailyQuestIcon_img.color = Color.white;
				DailyQuestText.color = Color.white;
				if (PlayerInfo.Instance.GetLittleTipsData().dailyQuest_open == 0)
				{
					DailyQuestTips.SetActive(value: true);
				}
				else
				{
					DailyQuestTips.SetActive(value: false);
				}
			}
		}
		if (toShowDailyReward && !(MainUIScript.Instance != null))
		{
			if (PlayerInfo.Instance.LastOfferPayValue > 0f)
			{
				toShowDailyReward = false;
				Invoke("AutoGetReward", 0.1f);
			}
			else if (Application.internetReachability == NetworkReachability.NotReachable)
			{
				toShowDailyReward = false;
				Invoke("AutoGetReward", 0.1f);
			}
			else
			{
				toShowDailyReward = false;
				Invoke("AutoGetReward", 0.1f);
			}
		}
	}

	public void DestroyObjAndShowBtns()
	{
		UnityEngine.Object.Destroy(TargetObj);
		HideBtnsTrans.SetActive(value: true);
		MapBtnBgImg.gameObject.SetActive(value: false);
		MailBtnBgImg.gameObject.SetActive(value: false);
		DailyRewardBtnBgImg.gameObject.SetActive(value: false);
	}

	public void BtnMapClick()
	{
		if (TargetObj != null)
		{
			if (TargetObj.name == "ChapterImageUI")
			{
				BtnBlackBgClick();
				return;
			}
			UnityEngine.Object.Destroy(TargetObj);
			GameObject gameObject = UIManager.Instance.LoadMainUI("UI/LevelMap/ChapterImageUI");
			RectTransform rectTransform = gameObject.transform as RectTransform;
			Vector3 anchoredPosition3D = rectTransform.anchoredPosition3D;
			rectTransform.anchoredPosition3D = new Vector3(0f - ImgWidth, anchoredPosition3D.y, anchoredPosition3D.z);
			rectTransform.DOAnchorPosX(0f, WordGlobal.UIMoveSpeed);
			TargetObj = gameObject;
			MapBtnBgImg.gameObject.SetActive(value: true);
			MailBtnBgImg.gameObject.SetActive(value: false);
			DailyRewardBtnBgImg.gameObject.SetActive(value: false);
		}
		else
		{
			BtnClick();
			GameObject gameObject2 = UIManager.Instance.LoadMainUI("UI/LevelMap/ChapterImageUI");
			RectTransform rectTransform2 = gameObject2.transform as RectTransform;
			Vector3 anchoredPosition3D2 = rectTransform2.anchoredPosition3D;
			rectTransform2.anchoredPosition3D = new Vector3(0f - ImgWidth, anchoredPosition3D2.y, anchoredPosition3D2.z);
			rectTransform2.DOAnchorPosX(0f, WordGlobal.UIMoveSpeed);
			TargetObj = gameObject2;
			MapBtnBgImg.gameObject.SetActive(value: true);
		}
	}

	public void BtnMailClick()
	{
		if (TargetObj != null)
		{
			if (TargetObj.name == "MailUI")
			{
				BtnBlackBgClick();
				return;
			}
			UnityEngine.Object.Destroy(TargetObj);
			GameObject gameObject = UIManager.Instance.LoadMainUI("UI/MailUI");
			RectTransform rectTransform = gameObject.transform as RectTransform;
			Vector3 anchoredPosition3D = rectTransform.anchoredPosition3D;
			rectTransform.anchoredPosition3D = new Vector3(0f - ImgWidth, anchoredPosition3D.y, anchoredPosition3D.z);
			rectTransform.DOAnchorPosX(0f, WordGlobal.UIMoveSpeed);
			TargetObj = gameObject;
			MapBtnBgImg.gameObject.SetActive(value: false);
			MailBtnBgImg.gameObject.SetActive(value: true);
			DailyRewardBtnBgImg.gameObject.SetActive(value: false);
		}
		else
		{
			BtnClick();
			GameObject gameObject2 = UIManager.Instance.LoadMainUI("UI/MailUI");
			RectTransform rectTransform2 = gameObject2.transform as RectTransform;
			Vector3 anchoredPosition3D2 = rectTransform2.anchoredPosition3D;
			rectTransform2.anchoredPosition3D = new Vector3(0f - ImgWidth, anchoredPosition3D2.y, anchoredPosition3D2.z);
			rectTransform2.DOAnchorPosX(0f, WordGlobal.UIMoveSpeed);
			TargetObj = gameObject2;
			MailBtnBgImg.gameObject.SetActive(value: true);
		}
	}

	public void BtnDailyRewardClick()
	{
		if (WordGlobal.isShowDailyReward)
		{
			UIManager.Instance.LoadMainUI("UI/DailyRewardUI", isPop: true, UIManager.MainUIType.Type_Common, -1, 1);
		}
	}

	public void BtnBlackBgClick()
	{
		Vector3 localPosition = base.transform.localPosition;
		if (localPosition.x == RightMostX)
		{
			if ((bool)WaitLoadingUI.instance)
			{
				WaitLoadingUI.instance.CloseUI();
			}
			NetMill.Instance.ResetSendQueue();
			Transform transform = base.transform;
			Vector3 localPosition2 = base.transform.localPosition;
			transform.DOLocalMoveX(localPosition2.x - ImgWidth, WordGlobal.UIMoveSpeed);
			BlackBgBtn.SetActive(value: false);
			Transform transform2 = TargetObj.transform;
			Vector3 localPosition3 = TargetObj.transform.localPosition;
			transform2.DOLocalMoveX(localPosition3.x - ImgWidth, WordGlobal.UIMoveSpeed);
			Invoke("DestroyObjAndShowBtns", 0.5f);
		}
	}

	public void BtnClick()
	{
		Vector3 localPosition = base.transform.localPosition;
		RightMostX = localPosition.x + ImgWidth;
		Transform transform = base.transform;
		Vector3 localPosition2 = base.transform.localPosition;
		transform.DOLocalMoveX(localPosition2.x + ImgWidth, WordGlobal.UIMoveSpeed);
		BlackBgBtn.SetActive(value: true);
		HideBtnsTrans.SetActive(value: false);
	}
}
