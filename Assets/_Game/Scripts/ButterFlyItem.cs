using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class ButterFlyItem : MonoBehaviour
{
	public List<GameObject> AnimObjList;

	public List<ParticleSystem> ButterFlyParticl;

	private Color _curColor;

	private int _curState;

	private int[] _stateTime = new int[2]
	{
		3,
		10
	};

	private int _curTimeStay;

	private float frameCount;

	private bool isFade;

	private float alphaValue = 1f;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public bool ParticleDoHide(bool isHide, ParticleSystem ps)
	{
		ParticleSystem.MinMaxGradient startColor = ps.main.startColor;
		Color color = startColor.color;
		if (isHide)
		{
			color.a -= alphaValue * Time.deltaTime;
		}
		else
		{
			color.a += alphaValue * Time.deltaTime;
		}
		startColor.color = color;
		Type type = ps.main.GetType();
		PropertyInfo property = type.GetProperty("startColor");
		property.SetValue(ps.main, startColor, null);
		if (isHide)
		{
			return color.a <= 0f;
		}
		return color.a >= 1f;
	}

	public void ShowButterFly(Color color)
	{
		for (int i = 0; i < AnimObjList.Count; i++)
		{
			AnimObjList[i].SetActive(value: false);
			WordGlobal.ChangePSStartColor(ButterFlyParticl[i], color);
		}
		int index = UnityEngine.Random.Range(0, AnimObjList.Count - 1);
		AnimObjList[index].SetActive(value: true);
	}

	public void ChangeButterState(int state)
	{
		_curState = state;
		for (int i = 0; i < AnimObjList.Count; i++)
		{
			AnimObjList[i].SetActive(value: false);
		}
		AnimObjList[_curState].SetActive(value: true);
	}

	public void SetButterFlyScale(float scale)
	{
		for (int i = 0; i < AnimObjList.Count; i++)
		{
			for (int j = 0; j < AnimObjList[i].transform.childCount; j++)
			{
				AnimObjList[i].transform.GetChild(j).transform.localScale = new Vector3(scale, scale, scale);
			}
		}
	}
}
