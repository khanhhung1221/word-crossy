using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;

public class ButterFlyTV : MonoBehaviour
{
	public static int ButterType;

	public static bool isButterFlyTvFreeDP;

	public ParticleSystem butterFly_stand02;

	public ParticleSystem butterFly_fly;

	public GameObject stand02;

	public GameObject fly;

	public GameObject click_go;

	public GameObject move_go;

	public Animator move_anim;

	public float waitShowTime;

	public float waitOutTime;

	private int waitoutMaxTime = 15;

	private int waitShowMaxTime = 15;

	private int showButterCount;

	private bool isWaitWifiToShow;

	private bool isShowTip;

	private void Start()
	{
		if (ButterType == 0)
		{
			Init();
		}
	}

	public void Init()
	{
		if (ButterType == 0)
		{
			waitoutMaxTime = 15;
		}
		else if (ButterType == 1)
		{
			waitoutMaxTime = 25;
			showButterCount = 2;
		}
		waitShowTime = waitShowMaxTime;
		waitOutTime = 0f;
		if (ButterType == 1 && PlayerInfo.Instance.CurrentLevel == 2 && PlayerInfo.Instance.MaxPlayLevel == 2)
		{
			waitShowTime = 0.01f;
		}
		move_go.SetActive(value: false);
		RectTransform rectTransform = base.transform as RectTransform;
		if (WordGlobal.isMainUIShowBanner)
		{
			RectTransform rectTransform2 = rectTransform;
			Vector2 anchoredPosition = rectTransform.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition.x, WordGlobal.showBanner_bottomupy);
		}
		else
		{
			RectTransform rectTransform3 = rectTransform;
			Vector2 anchoredPosition2 = rectTransform.anchoredPosition;
			rectTransform3.anchoredPosition = new Vector2(anchoredPosition2.x, 0f);
		}
	}

	public void WaitWifi()
	{
		isWaitWifiToShow = true;
	}

	public void OnApplicationPause(bool pause)
	{
		if (!pause)
		{
			butterFly_stand02.gameObject.SetActive(value: false);
			butterFly_stand02.gameObject.SetActive(value: true);
			butterFly_fly.gameObject.SetActive(value: false);
			butterFly_fly.gameObject.SetActive(value: true);
		}
	}

	private void Update()
	{
		if ((ButterType == 0 && PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial < 3) || isShowTip || DPCollectPicUIScript.isShow)
		{
			return;
		}
		if (PlayerInfo.Instance.LastOfferPayValue > 0f || !PlayerInfo.Instance.ShowRewardAD)
		{
			if (base.gameObject.activeInHierarchy)
			{
				base.gameObject.SetActive(value: false);
			}
		}
		else
		{
			if (Application.internetReachability == NetworkReachability.NotReachable)
			{
				if (move_go.activeInHierarchy)
				{
					move_go.SetActive(value: false);
				}
				return;
			}
			if (isWaitWifiToShow)
			{
				move_go.SetActive(value: true);
				isWaitWifiToShow = false;
			}
		}
		if (MainUIScript.Instance != null)
		{
			if (MainUIScript.Instance.isShowResult())
			{
				base.gameObject.SetActive(value: false);
			}
			if (ButterType == 0 && UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
			{
				base.gameObject.SetActive(value: false);
			}
		}
		if (waitShowTime > 0f)
		{
			waitShowTime -= Time.deltaTime;
			if (waitShowTime <= 0f && MainUIScript.Instance != null)
			{
				if (ButterType == 0 && MainUIScript.Instance._curButterFlyTVCount >= 3)
				{
					return;
				}
				ShowButter();
			}
		}
		if (waitOutTime > 0f)
		{
			if (ButterType == 0 && PlayerInfo.Instance.GetPlayerOtherInfoSaveData().ButterFlyDailyPuzzleTutorial == 0 && click_go.activeInHierarchy)
			{
				TutorialButterFlyVideo.isShow = true;
				UIManager.Instance.LoadMainUI("UI/Main/TutorialButterFlyVideo", isPop: true);
				PlayerInfo.Instance.GetPlayerOtherInfoSaveData().SetButterFlyDailyPuzzleTutorial();
			}
			if (!TutorialButterFlyVideo.isShow)
			{
				waitOutTime -= Time.deltaTime;
			}
			if (waitOutTime <= 0f)
			{
				OutButter();
			}
		}
	}

	public void GetButter()
	{
		waitOutTime = 0f;
		move_go.SetActive(value: true);
		move_anim.enabled = false;
		fly.SetActive(value: true);
		stand02.SetActive(value: false);
		click_go.SetActive(value: false);
		if (MainUIScript.Instance != null)
		{
			if (ButterType == 0)
			{
				Vector3 position = MainUIScript.Instance.StarPosObj.transform.position;
				MainUIScript.Instance._curButterFlyTVCount++;
				MainUIScript.Instance.AddButterData();
				(move_go.transform as RectTransform).anchoredPosition = new Vector2(2f, -358f);
				Vector3[] path = WordGlobal.CountCirclePoints(move_go.transform.position, position);
				move_go.transform.DOPath(path, 2f).SetDelay(1f).OnComplete(delegate
				{
					MainUIScript.Instance.AddButterFlyNum();
					move_go.SetActive(value: false);
				});
				butterFly_fly.transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 2f).SetDelay(1f);
			}
			else if (ButterType == 1)
			{
				isButterFlyTvFreeDP = true;
				MainUIScript.Instance.FreeDP(isshoweff: false);
				isButterFlyTvFreeDP = false;
			}
		}
		if (ButterType == 0)
		{
			waitShowTime = waitShowMaxTime;
		}
	}

	public void CommonGetButterMove(int row, int col)
	{
		if (move_go.activeInHierarchy && MainUIScript.Instance != null)
		{
			string n = "item-" + (row - 1).ToString() + "-" + (col - 1).ToString();
			GameObject gameObject = MainUIScript.Instance.m_TfItemRoot.Find(n).gameObject;
			if (gameObject != null)
			{
				Vector3 position = gameObject.transform.position;
				MainUIScript.Instance._curButterFlyTVCount++;
				MainUIScript.Instance.AddButterData();
				Vector3[] path = WordGlobal.CountCirclePoints(move_go.transform.position, position);
				move_go.transform.DOPath(path, 0.7f).OnComplete(delegate
				{
					move_go.SetActive(value: false);
				});
				butterFly_fly.transform.DOScale(new Vector3(1f, 1f, 1f), 0.7f);
			}
		}
	}

	public void OutButter()
	{
		move_go.SetActive(value: true);
		move_anim.enabled = true;
		click_go.SetActive(value: false);
		move_anim.Play("butterfly_out");
		if (ButterType == 0)
		{
			waitShowTime = (float)waitShowMaxTime + 4f;
		}
		else if (ButterType == 1 && showButterCount > 0)
		{
			waitShowTime = (float)waitShowMaxTime + 4f;
		}
	}

	public void ShowButter()
	{
		showButterCount--;
		waitOutTime = waitoutMaxTime;
		move_go.SetActive(value: true);
		move_anim.enabled = true;
		move_anim.Play("butterfly");
		string[] array = new string[7]
		{
			"#FB6734",
			"#FE3C3C",
			"#FFB135",
			"#78EA78",
			"#0BBAF4",
			"#9E69FF",
			"#FC61BC"
		};
		int num = UnityEngine.Random.Range(0, array.Length);
		Color color = WordGlobal.ChangeStringToColor(array[num]);
		WordGlobal.ChangePSStartColor(butterFly_stand02, color);
		WordGlobal.ChangePSStartColor(butterFly_fly, color);
		butterFly_fly.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
	}

	public void onClick()
	{
		isShowTip = true;
		int id = 1242;
		if (ButterType == 1)
		{
			id = 1246;
		}
		else
		{
			TutorialButterFlyVideo.isHide = true;
		}
		CommonTipsScript.Instance.ShowUI(WordGlobal.ReadText(id), 1, WordGlobal.ReadText(1018), delegate
		{
			WatchTV();
		}, showClose: true, Close);
	}

	public void Close()
	{
		isShowTip = false;
	}

	public void WatchTV()
	{
		isShowTip = false;
		string eventStr = string.Empty;
		if (ButterType == 0)
		{
			eventStr = "TapDailyPuzzleButterFlyTV";
		}
		else if (ButterType == 1)
		{
			eventStr = "TapCommonButterFlyTV";
		}
		if (AdsManager.Instance.ShowIncentivizedAd(eventStr))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
		}
	}

	public void RewardADEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
		if (ret)
		{
			GetButter();
		}
	}
}
