using UnityEngine;

public class CameraMovement : MonoBehaviour
{
	private float m_CameraSpeed;

	private Vector3 m_CameraPosition = new Vector3(0f, 0f, -2.5f);

	private GUIStyle m_Style = new GUIStyle();

	private void OnGUI()
	{
		GUI.Label(new Rect(0f, 0f, Screen.width, 100f), new GUIContent("Use LEFT and RIGHT arrow to move with the camera."), m_Style);
	}

	private void Start()
	{
		m_Style.fontSize = 32;
		m_Style.normal.textColor = Color.white;
		m_Style.alignment = TextAnchor.UpperCenter;
	}

	private void Update()
	{
		m_CameraSpeed += (0f - m_CameraSpeed) * 1.5f * Time.deltaTime;
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			m_CameraSpeed = Mathf.Max(m_CameraSpeed - 10f * Time.deltaTime, -10f);
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			m_CameraSpeed = Mathf.Min(m_CameraSpeed + 10f * Time.deltaTime, 10f);
		}
		m_CameraPosition.x = Mathf.Clamp(m_CameraPosition.x + m_CameraSpeed * Time.deltaTime, -34f, 33f);
		base.gameObject.transform.position = m_CameraPosition;
	}
}
