using UnityEngine;
using UnityEngine.UI;

public class CanvasScalerChange : MonoBehaviour
{
	public CanvasScaler cs;

	private void Awake()
	{
		if (cs == null)
		{
			cs = base.gameObject.GetComponent<CanvasScaler>();
		}
		if (cs != null)
		{
			float num = (float)Screen.width / (float)Screen.height;
			if (num > 0.6f)
			{
				cs.matchWidthOrHeight = 1f;
			}
			else
			{
				cs.matchWidthOrHeight = 0f;
			}
		}
	}

	private void Start()
	{
	}
}
