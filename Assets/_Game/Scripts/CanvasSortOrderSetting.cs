using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class CanvasSortOrderSetting : MonoBehaviour
{
	public const int TopSortOrder = 100;

	private Canvas _canvas;

	private void OnEnable()
	{
		_canvas = GetComponent<Canvas>();
		_canvas.sortingOrder = 0;
		_canvas.sortingOrder = FindMaxSortOrder() + 1;
	}

	public void OnDestroy()
	{
	}

	public static int FindMaxSortOrder()
	{
		int num = 0;
		Canvas[] array = Object.FindObjectsOfType<Canvas>();
		if (array != null)
		{
			Canvas[] array2 = array;
			foreach (Canvas canvas in array2)
			{
				if (canvas.sortingOrder > num && canvas.sortingOrder < 99)
				{
					num = canvas.sortingOrder;
				}
			}
		}
		return num;
	}

	public static int FindMaxOrderForBackKey()
	{
		int num = 0;
		Canvas[] array = Object.FindObjectsOfType<Canvas>();
		if (array != null)
		{
			Canvas[] array2 = array;
			foreach (Canvas canvas in array2)
			{
				if (canvas.GetComponent<BackKey>() != null)
				{
					num = canvas.sortingOrder;
				}
				else if (canvas.sortingOrder > num && canvas.sortingOrder < 99 && canvas.transform.parent != null && canvas.transform.parent.GetComponent<BackKey>() != null)
				{
					num = canvas.sortingOrder;
				}
			}
		}
		return num;
	}
}
