using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(LineRenderer))]
public class CatmullDraw : MonoBehaviour
{
	public List<Vector3> controlPoints;

	public List<Vector3> pointsList;

	public LineRenderer lineRenderer;

	private int layerOrder;

	public Vector3 _lastClickPoint;

	private bool isMousePressed;

	private Vector3 mousePos;

	private bool isClear;

	public string m_Word = string.Empty;

	private int _oldPointList;

	public Transform start_tran;

	public Transform end_tran;

	public Image start_img;

	public Image end_img;

	public Canvas canvas;

	public static event UnityAction OnDrawLineEnd;

	public static event UnityAction OnClearLine;

	private void Start()
	{
		SetLineOrder(layerOrder);
		controlPoints = new List<Vector3>();
		_lastClickPoint = default(Vector3);
		pointsList = new List<Vector3>();
	}

	public void SetLineOrder(int order)
	{
		if (!lineRenderer)
		{
			lineRenderer = GetComponent<LineRenderer>();
		}
		lineRenderer.sortingOrder = order;
		canvas.sortingOrder = order;
	}

	private void Update()
	{
		if (MainUIScript.Instance.m_PauseLetterTime > 0f)
		{
			return;
		}
		if (Input.GetMouseButton(0) || Input.touchCount == 1)
		{
			isMousePressed = true;
			isClear = false;
			lineRenderer.positionCount = 0;
			lineRenderer.startColor = MainUIScript.Instance.GetCurrentLineColor();
			lineRenderer.endColor = MainUIScript.Instance.GetCurrentLineColor();
			start_img.color = MainUIScript.Instance.GetCurrentLineColor();
			end_img.color = MainUIScript.Instance.GetCurrentLineColor();
			PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
			pointerEventData.position = Input.mousePosition;
			List<RaycastResult> list = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointerEventData, list);
			if (HuoDongGardonTimeOut.isShowGardonTimeOut)
			{
				ClearLine(null);
				MainUIScript.Instance.ClearSelectWord();
				list.Clear();
			}
			if (list.Count > 0)
			{
				MainUIScript.Instance.m_IsMoveLetter = false;
				for (int i = 0; i < list.Count && !(list[i].gameObject.tag != "MainLetter"); i++)
				{
					if (list[i].gameObject.tag == "MainLetter")
					{
						list[i].gameObject.transform.parent.GetComponent<LetterScript>().AddPoint();
						MainUIScript.Instance.m_IsMoveLetter = true;
						break;
					}
				}
			}
		}
		else
		{
			isMousePressed = false;
			if (HuoDongGardonTimeOut.isShowGardonTimeOut)
			{
				return;
			}
			if (!isClear)
			{
				isClear = true;
				if (CatmullDraw.OnDrawLineEnd != null)
				{
					CatmullDraw.OnDrawLineEnd();
				}
				ClearLine(null);
			}
		}
		if (!isMousePressed)
		{
			return;
		}
		pointsList.Clear();
		if (Input.touchCount == 0)
		{
			return;
		}
		try
		{
			mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		}
		catch (Exception)
		{
			throw;
		}
		mousePos.z = 0f;
		if (controlPoints.Count <= 0)
		{
			return;
		}
		Vector3[] array;
		if (MainUIScript.Instance.m_IsMoveLetter && controlPoints.Count > 1)
		{
			array = new Vector3[controlPoints.Count];
			controlPoints.CopyTo(array);
		}
		else
		{
			array = new Vector3[controlPoints.Count + 1];
			controlPoints.CopyTo(array);
			array[array.Length - 1] = mousePos;
		}
		for (int j = 0; j < array.Length; j++)
		{
			if (j != 0 || !(Mathf.Abs(array[j].x) < 0.1f) || !(Mathf.Abs(array[j].y) < 0.1f))
			{
				pointsList.Add(array[j]);
			}
		}
		lineRenderer.positionCount = controlPoints.Count;
		lineRenderer.SetPosition(controlPoints.Count - 1, controlPoints[controlPoints.Count - 1]);
		savePathCatmullRom(pointsList);
		if (!start_tran.gameObject.activeInHierarchy)
		{
			start_tran.gameObject.SetActive(value: true);
			end_tran.gameObject.SetActive(value: true);
		}
		start_tran.position = lineRenderer.GetPosition(0);
		end_tran.position = lineRenderer.GetPosition(lineRenderer.positionCount - 1);
	}

	public void ClearLine(object obj)
	{
		if (CatmullDraw.OnClearLine != null)
		{
			CatmullDraw.OnClearLine();
		}
		lineRenderer.positionCount = 0;
		controlPoints.Clear();
		start_tran.gameObject.SetActive(value: false);
		end_tran.gameObject.SetActive(value: false);
	}

	public void RemovePoint(int index)
	{
		if (controlPoints.Count > index)
		{
			controlPoints.RemoveAt(index);
		}
	}

	public void AddPoints(Vector3 point)
	{
		Vector3 vector = new Vector3(point.x, point.y, 0f);
		controlPoints.Add(vector);
		if (controlPoints.Count > 1)
		{
			MainUIScript.Instance.m_DrawLine.transform.position = vector;
		}
	}

	public void ClearList()
	{
		controlPoints.Clear();
	}

	private void savePathCatmullRom(List<Vector3> pointFList)
	{
		if (pointFList == null || pointFList.Count == 0)
		{
			return;
		}
		int count = pointFList.Count;
		lineRenderer.positionCount = 0;
		int num = 0;
		for (int i = 0; i < count - 1; i++)
		{
			for (float num2 = 0f; num2 < 1f; num2 += 0.01f)
			{
				num++;
				Vector3 position = WordGlobal.interpolatedPosition(pointFList[Mathf.Max(0, i - 1)], pointFList[i], pointFList[Mathf.Min(i + 1, count - 1)], pointFList[Mathf.Min(i + 2, count - 1)], num2);
				lineRenderer.positionCount = num;
				lineRenderer.SetPosition(num - 1, position);
			}
		}
	}
}
