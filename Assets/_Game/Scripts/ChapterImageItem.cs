using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChapterImageItem : MonoBehaviour
{
	public Text Name;

	public Text Percent;

	public Image Bg, Icon, Fill;

	public GameObject LockedBg;

	public Slider Slider;

	private MapDataCsv curData;

	private List<Color> FillColorList;

	private int curIndex;

	private LayoutElement curElement;

	private float wait_load_bg_time;

	private void Start()
	{
	}

	private void Update()
	{
		//if (wait_load_bg_time > 0f)
		//{
		//	wait_load_bg_time -= Time.deltaTime;
		//	if (wait_load_bg_time <= 0f)
		//	{
  //              InitBg(isAsync: true);
  //              InitIcon(isAsync: false);
  //          }
		//}
	}

	public void SetData(MapDataCsv data, Color fillColor, int index)
	{
		curData = data;
		curIndex = index;
		if (curElement == null)
		{
			curElement = base.transform.GetComponent<LayoutElement>();
		}
		ShowUI();
	}

	public void InitIcon(bool isAsync = false)
	{
		string text = "Image/LevelMap/";
		string LIcon = curData.LIcon;
		if (isAsync)
		{
			string str = ".png";
			string path = WordGlobal.DataPath + text + LIcon + str;
			StartCoroutine(ResourceLoadManager.GetInstance().LoadSpriteAsync(path, Icon));
		}
		else
		{
			Icon.sprite = ResourceLoadManager.GetInstance().LoadSpriteByPath(text + LIcon, ResourceLoadManager.SP_LEVELMAP_TYPE.LICON);
		}
	}

	public void InitBg(bool isAsync = false)
	{
		string text = "Image/LevelMap/";
		string cIcon = curData.CIcon;
		if (isAsync)
		{
			string str = ".png";
			string path = WordGlobal.DataPath + text + cIcon + str;
			StartCoroutine(ResourceLoadManager.GetInstance().LoadSpriteAsync(path, Bg));
		}
		else
		{
			Bg.sprite = ResourceLoadManager.GetInstance().LoadSpriteByPath(text + cIcon, ResourceLoadManager.SP_LEVELMAP_TYPE.CICON);
		}
	}

	public void ShowUI()
	{
		Name.text = curData.GetChapterName();
		if (curIndex < 10)
		{
			//InitBg();
			InitIcon();
		}
		else
		{
			wait_load_bg_time = 0.5f + (float)(curIndex - 10) * 0.1f;
		}
		string chapterStartEndLevel = MapDataManager.instance.GetChapterStartEndLevel(curData.ChapterID);
		int num = PlayerInfo.Instance.MaxPlayLevel - 1;
		if (chapterStartEndLevel != null)
		{
			string[] array = chapterStartEndLevel.Split('_');
			int num2 = int.Parse(array[0]);
			int num3 = int.Parse(array[1]);
			float num4 = 0f;
			num4 = ((num < num2) ? 0f : ((num < num2 || num > num3) ? 1f : ((float)(num - num2 + 1) / (float)(num3 - num2 + 1))));
			LockedBg.SetActive(PlayerInfo.Instance.MaxPlayLevel < num2);
			Percent.gameObject.SetActive(PlayerInfo.Instance.MaxPlayLevel >= num2);
			int num5 = Mathf.CeilToInt(num4 * 100f);
			Percent.text = num5 + "%";
			Fill.fillAmount = (float)num5/100;
		}
	}

	public void OnItemClick()
	{
		LevelMapUIScript.Instance.LocationToTargetLevel(curData.LevelStartNum);
		BtnsCtrlUIScript.Instance.BtnMapClick();
	}
}
