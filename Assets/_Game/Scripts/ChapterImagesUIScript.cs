using System.Collections.Generic;
using UnityEngine;

public class ChapterImagesUIScript : MonoBehaviour
{
	public static ChapterImagesUIScript Instance;

	public GameObject Content;

	private Dictionary<int, ChapterImageItem> ChapterImageList;

	private GameObject ComingSoonItem;

	private List<Color> ItemSliderFillColor;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		ChapterImageList = new Dictionary<int, ChapterImageItem>();
		ItemSliderFillColor = new List<Color>();
		Color item = new Color(1f, 233f / 255f, 44f / 255f, 1f);
		Color item2 = new Color(1f, 0.8235294f, 32f / 255f, 1f);
		Color item3 = new Color(254f / 255f, 181f / 255f, 0.0196078438f, 1f);
		ItemSliderFillColor.Add(item);
		ItemSliderFillColor.Add(item2);
		ItemSliderFillColor.Add(item3);
		ShowUI();
	}

	private void Update()
	{
	}

	public void ShowUI()
	{
		CleanData();
		string path = "UI/LevelMap/ChapterImageItem";
		int bigChapterCount = MapDataManager.instance.GetBigChapterCount();
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		float num = 180 * bigChapterCount;
		float num2 = 0f - num - 180f;
		for (int i = 1; i <= bigChapterCount; i++)
		{
			MapDataCsv bigChapterData = MapDataManager.instance.GetBigChapterData(i);
			GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI(path, Content.transform);
			gameObject.transform.SetSiblingIndex(0);
			gameObject.transform.name = "ChapterImageItem_" + i;
			float y = num2 + (float)(i * 180);
			(gameObject.transform as RectTransform).anchoredPosition = new Vector2(260f, y);
			ChapterImageItem component = gameObject.GetComponent<ChapterImageItem>();
			int index = (i - 1) % 3;
			component.SetData(bigChapterData, ItemSliderFillColor[index], i - 1);
			ChapterImageList.Add(bigChapterData.ChapterID, component);
		}
		RectTransform rectTransform = Content.transform as RectTransform;
		rectTransform.anchoredPosition = Vector2.zero;
		RectTransform rectTransform2 = rectTransform;
		Vector2 sizeDelta = rectTransform.sizeDelta;
		rectTransform2.sizeDelta = new Vector2(sizeDelta.x, num);
		float num3 = Time.realtimeSinceStartup - realtimeSinceStartup;
	}

	public void CleanData()
	{
		List<int> list = new List<int>(ChapterImageList.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			ChapterImageItem chapterImageItem = ChapterImageList[list[i]];
			ChapterImageList.Remove(list[i]);
			Object.Destroy(chapterImageItem.gameObject);
		}
		Object.Destroy(ComingSoonItem);
	}

	public void OnCloseBtnClick()
	{
		Object.Destroy(base.gameObject);
	}

	public void OnDestroy()
	{
		Instance = null;
	}
}
