using System;
using System.Security.Cryptography;
using System.Text;

namespace Common.Cryptography
{
	public class AesEncryption
	{
		private static string key = "xhbynPhiRsrv9sL81wFutqze+gV2rtM=";

		private static string iv = "ySRwIJHkZKkWm0Qt";

		public static byte[] AESEncrypt(string text)
		{
			return AESEncrypt(text, key, iv);
		}

		public static byte[] AESEncrypt(string text, string password, string iv)
		{
			RijndaelManaged rijndaelManaged = new RijndaelManaged();
			rijndaelManaged.Mode = CipherMode.CBC;
			rijndaelManaged.Padding = PaddingMode.PKCS7;
			rijndaelManaged.KeySize = 256;
			rijndaelManaged.BlockSize = 128;
			byte[] bytes = Encoding.UTF8.GetBytes(password);
			byte[] array = new byte[32];
			int num = bytes.Length;
			if (num > array.Length)
			{
				num = array.Length;
			}
			Array.Copy(bytes, array, num);
			rijndaelManaged.Key = array;
			byte[] array2 = rijndaelManaged.IV = Encoding.UTF8.GetBytes(iv);
			ICryptoTransform cryptoTransform = rijndaelManaged.CreateEncryptor();
			byte[] bytes3 = Encoding.UTF8.GetBytes(text);
			return cryptoTransform.TransformFinalBlock(bytes3, 0, bytes3.Length);
		}

		public static string AESDecrypt(byte[] text)
		{
			return AESDecrypt(text, key, iv);
		}

		public static string AESDecrypt(byte[] text, string password, string iv)
		{
			RijndaelManaged rijndaelManaged = new RijndaelManaged();
			rijndaelManaged.Mode = CipherMode.CBC;
			rijndaelManaged.Padding = PaddingMode.PKCS7;
			rijndaelManaged.KeySize = 256;
			rijndaelManaged.BlockSize = 128;
			byte[] bytes = Encoding.UTF8.GetBytes(password);
			byte[] array = new byte[32];
			int num = bytes.Length;
			if (num > array.Length)
			{
				num = array.Length;
			}
			Array.Copy(bytes, array, num);
			rijndaelManaged.Key = array;
			byte[] array2 = rijndaelManaged.IV = Encoding.UTF8.GetBytes(iv);
			ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor();
			byte[] bytes3 = cryptoTransform.TransformFinalBlock(text, 0, text.Length);
			return Encoding.UTF8.GetString(bytes3);
		}
	}
}
