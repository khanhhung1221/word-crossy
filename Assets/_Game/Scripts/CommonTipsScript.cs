using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CommonTipsScript : MonoBehaviour
{
	public static bool isAutoHide;

	public Image m_ImgIcon;

	public Text m_TxtTitle;

	public Text m_TxtButton;

	public GameObject m_ObjCloseBtn;

	public GameObject m_ObjConfirmBtn;

	public Sprite[] m_Icons;

	private UnityAction<object> _callBack;

	private UnityAction _closeAction;

	public static CommonTipsScript Instance => GetInstance();

	private static CommonTipsScript GetInstance()
	{
		GameObject gameObject = UIManager.Instance.LoadMainUI("UI/TipsUI", isPop: true);
		return gameObject.GetComponent<CommonTipsScript>();
	}

	private void Start()
	{
		isAutoHide = false;
	}

	private void Update()
	{
		if (PlayerInfo.Instance.LastOfferPayValue > 0f)
		{
			BtnCloseClick();
		}
		if (isAutoHide)
		{
			BtnCloseClick();
			isAutoHide = false;
		}
	}

	private void Awake()
	{
		base.transform.GetComponent<Image>().enabled = true;
	}

	public void ShowClaimUI(int gold, UnityAction<object> callback, bool showClose = true, UnityAction closeAction = null)
	{
        int iconid = 0;
        string title = string.Format(WordGlobal.ReadText(1112), gold);
        string btnTxt = WordGlobal.ReadText(1016);
        ShowUI(title, iconid, btnTxt, callback, showClose, closeAction);
    }

	public void ShowUI(string title, int iconid, string btnTxt, UnityAction<object> callback, bool showClose = true, UnityAction closeAction = null)
	{
		m_TxtTitle.text = title;
		if (iconid >= 0 && iconid < m_Icons.Length)
		{
			m_ImgIcon.sprite = m_Icons[iconid];
			m_ImgIcon.SetNativeSize();
		}
		m_ObjCloseBtn.SetActive(showClose);
		WordGlobal.DisableBackKey = !showClose;
		m_TxtButton.text = btnTxt;
		_callBack = callback;
		_closeAction = closeAction;
	}

	public void BtnConfirmClick()
	{
		WordGlobal.DisableBackKey = false;
		base.transform.GetComponent<Image>().enabled = false;
		Object.Destroy(base.gameObject);
		if (_callBack != null)
		{
			_callBack(m_ImgIcon.transform.position);
			_callBack = null;
		}
	}

	public void BtnCloseClick()
	{
		WordGlobal.DisableBackKey = false;
		base.transform.GetComponent<Image>().enabled = false;
		if (_closeAction != null)
		{
			_closeAction();
			_closeAction = null;
		}
		Object.Destroy(base.gameObject);
		WordGlobal.LogEvent("Adpop_Close");
	}
}
