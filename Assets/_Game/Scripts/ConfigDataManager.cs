using LitJson;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

public class ConfigDataManager
{
	public delegate void OnCsvFileLoadFinish();

	public static OnCsvFileLoadFinish csvFileLoadFinish;

	public static UnityAction localCsvLoadFinish;

	private static string LocalCsvFilePath = "Data/";

	private static ConfigDataManager instance;

	private Dictionary<string, ManagerCsv> managerCsvs;

	private IList<string> updateFileNames;

	private List<string> csvFileNames;

	private string StarPosCsvName = "Starposmap_index.csv";

	public static ConfigDataManager Instance => instance ?? (instance = new ConfigDataManager());

	private int FisrtUpdate
	{
		get
		{
			return PlayerPrefs.GetInt("CsvFile.FirstUpdate", 1);
		}
		set
		{
			PlayerPrefs.SetInt("CsvFile.FirstUpdate", value);
		}
	}

	private ConfigDataManager()
	{
	}

	public void Init()
	{
		updateFileNames = new List<string>();
		managerCsvs = new Dictionary<string, ManagerCsv>();
		managerCsvs.Add("text.csv", TextLibraryDataManagerCsv.instance);
		managerCsvs.Add("maps.csv", MapDataManager.instance);
		managerCsvs.Add("daily reward.csv", DailyRewardDataManagerCsv.instance);
		managerCsvs.Add("notification.csv", NotificationDataManager.instance);
		managerCsvs.Add("daily quest.csv", DailyQuestDataManagerCsv.instance);
		if (WordGlobal.isNeedHuoDong)
		{
			managerCsvs.Add("event.csv", EventDataManagerCsv.instance);
		}
		if (WordGlobal.isNeedWordSearch)
		{
			managerCsvs.Add("wordsearch.csv", WordSearchDataManagerCsv.instance);
		}
		if (WordGlobal.isNeedWordFind)
		{
			managerCsvs.Add("wordfind.csv", WordFindDataManagerCsv.instance);
		}
		managerCsvs.Add("spin.csv", LuckyDrawDataManagerCsv.instance);
		csvFileNames = new List<string>(managerCsvs.Keys);
		WordJsonDataManager.instance.Init();
		loadLocalCsvFile("text.csv");
	}

	public void setUpdateFiles(JsonData fileNames)
	{
		updateFileNames.Clear();
		for (int i = 0; i < fileNames.Count; i++)
		{
			if (managerCsvs.ContainsKey(UpdateFileNamesToLoacal(fileNames[i].ToString())))
			{
				Debug.Log("Hot update file：" + fileNames[i]);
				updateFileNames.Add(fileNames[i].ToString());
			}
		}
	}

	public void LoadData()
	{
		List<string> list = new List<string>(managerCsvs.Keys);
		Debug.Log("Start hot update file");
		if (updateFileNames != null)
		{
			string path = Application.persistentDataPath + "/Data";
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			foreach (string updateFileName in updateFileNames)
			{
				string text = UpdateFileNamesToLoacal(updateFileName);
				Debug.Log("Start Download：" + text);
				list.Remove(text);
				if (!text.Contains("text"))
				{
					NetMill.Instance.Download(updateFileName, DownloadPath(text), text, InitCsvManager, CsvDownloadErrorHandler);
				}
			}
		}
		foreach (string item in list)
		{
			if (item != "text.csv")
			{
				loadLocalCsvFile(item);
			}
		}
		if (localCsvLoadFinish != null)
		{
			localCsvLoadFinish();
		}
	}

	private string UpdateFileNamesToLoacal(string updateFileName)
	{
		int num = updateFileName.LastIndexOf('/');
		return updateFileName.Substring(num + 1);
	}

	private void loadLocalCsvFile(string fileName)
	{
		ManagerCsv managerCsv = managerCsvs[fileName];
		if (managerCsv == null)
		{
			return;
		}
		byte[] downloadBytes = null;
		if (WordGlobal.isDebugBuild || true)
		{
			Debug.Log("loadLocalCsvFile:" + fileName);
			string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
			downloadBytes = Resources.Load<TextAsset>(LocalCsvFilePath + fileNameWithoutExtension).bytes;
		}
		else
		{
			string path = DownloadPath(fileName);
			if (fileName == "Textlibrary.csv" && FisrtUpdate == 0)
			{
				FisrtUpdate = 1;
				File.Delete(path);
			}
			if (File.Exists(path))
			{
				try
				{
					downloadBytes = File.ReadAllBytes(path);
				}
				catch (Exception message)
				{
					WordGlobal.DebugLog(message);
				}
			}
			else
			{
				string fileNameWithoutExtension2 = Path.GetFileNameWithoutExtension(fileName);
				downloadBytes = Resources.Load<TextAsset>(LocalCsvFilePath + fileNameWithoutExtension2).bytes;
			}
		}
		InitCsvManager(downloadBytes, fileName);
	}

	public bool LoadFinished()
	{
		ICollection<ManagerCsv> values = managerCsvs.Values;
		foreach (ManagerCsv item in values)
		{
			if (!item.LoadFinished())
			{
				return false;
			}
		}
		return true;
	}

	public void ReInitCsv(string fileName, ManagerCsv managerCsv)
	{
		Debug.Log("Reread the data table：" + fileName);
		string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
		byte[] bytes = Resources.Load<TextAsset>(LocalCsvFilePath + fileNameWithoutExtension).bytes;
		try
		{
			managerCsv.inited = false;
			managerCsv.skipNumber = 2;
			managerCsv.Init(bytes, "UTF-8");
		}
		catch (Exception ex)
		{
			Debug.LogError(ex.Message);
			WordGlobal.GameStop = true;
		}
	}

	private void InitCsvManager(object downloadBytes, object userData)
	{
		byte[] file = (byte[])downloadBytes;
		string text = (string)userData;
		ManagerCsv managerCsv = managerCsvs[text];
		Debug.Log("InitCsvManager:" + text);
		try
		{
			managerCsv.inited = false;
			managerCsv.skipNumber = 2;
			managerCsv.Init(file, "UTF-8");
		}
		catch (Exception ex)
		{
			Debug.LogError(ex.Message);
			WordGlobal.GameStop = true;
		}
		csvFileNames.Remove(text);
		if (csvFileNames.Count == 0 && csvFileLoadFinish != null)
		{
			csvFileLoadFinish();
		}
	}

	private void CsvDownloadErrorHandler(object userData, int code, string msg)
	{
		loadLocalCsvFile((string)userData);
		PlayerInfo.Instance.CsvFileVersion--;
		WordGlobal.GameStop = true;
	}

	private void DownloadErrorCallback(object obj)
	{
		WordGlobal.RestartGame();
	}

	private string DownloadPath(string resourceName)
	{
		return Application.persistentDataPath + "/Data/" + resourceName;
	}
}
