using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ConfirmUIScript : MonoBehaviour
{
	public enum ConfirmType
	{
		Type_Quit,
		Type_Update
	}

	public class OnConfirmlEvent : UnityEvent
	{
	}

	public Text ShowText;

	public Text ConfirmBtnText;

	public Text CancelBtnText;

	public OnConfirmlEvent OnConfirmlCall;

	private ConfirmType _curShowType;

	public void ShowUI(ConfirmType type = ConfirmType.Type_Quit, int textId = 1044, int comfirmBtnTextId = 1043, UnityAction call = null)
	{
		_curShowType = type;
		ShowText.text = TextLibraryDataManagerCsv.instance.GetText(textId);
		ConfirmBtnText.text = TextLibraryDataManagerCsv.instance.GetText(comfirmBtnTextId);
		CancelBtnText.text = TextLibraryDataManagerCsv.instance.GetText(1042);
		if (call != null)
		{
			if (OnConfirmlCall == null)
			{
				OnConfirmlCall = new OnConfirmlEvent();
			}
			OnConfirmlCall.AddListener(call);
		}
	}

	public void BtnConfirmClick()
	{
		if (_curShowType == ConfirmType.Type_Quit)
		{
			Application.Quit();
		}
		else
		{
			OnConfirmlCall.Invoke();
		}
	}

	public void BtnCancelClick()
	{
		Object.Destroy(base.gameObject);
	}
}
