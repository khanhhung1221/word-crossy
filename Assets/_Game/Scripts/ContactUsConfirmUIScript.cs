using UnityEngine;

public class ContactUsConfirmUIScript : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
	}

	public void BtnConfirmClick()
	{
		GameObject obj = GameObject.Find("ContactUsUI");
		Object.Destroy(base.gameObject);
		Object.Destroy(obj);
	}

	public void BtnCancelClick()
	{
		Object.Destroy(base.gameObject);
	}
}
