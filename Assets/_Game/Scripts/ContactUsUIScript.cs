using LitJson;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContactUsUIScript : MonoBehaviour
{
	public InputField ifContent;

	public InputField ifMail;

	public InputField ifName;

	public Text MailTips;

	private void Start()
	{
		WordGlobal.ResetTopPos(base.transform as RectTransform);
	}

	private void Update()
	{
		if (ifMail.isFocused)
		{
			MailTips.gameObject.SetActive(value: false);
		}
	}

	public void BtnSendClick()
	{
		if (ifMail.text != string.Empty)
		{
			SendMill();
		}
		else
		{
			MailTips.gameObject.SetActive(value: true);
		}
	}

	public void SendMill()
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		jsonData["title"] = ifContent.text;
		jsonData["email"] = ifMail.text;
		jsonData["name"] = ifName.text;
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["feedback"] = SendCallback;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "feedback", jsonData, dictionary);
	}

	public bool SendCallback(JsonData data)
	{
		ResourceLoadManager.GetInstance().ShowTips(1127);
		Object.Destroy(base.gameObject);
		return true;
	}

	public void BtnCloseClick()
	{
		UIManager.Instance.LoadMainUI("UI/ContactUsConfirmUI", isPop: true);
	}
}
