using UnityEngine;
using UnityEngine.UI;

public class ConvertHardCode : MonoBehaviour
{
	private void Start()
	{
		Check();
	}

	private void Update()
	{
	}

	public void Check()
	{
		Text[] componentsInChildren = base.transform.GetComponentsInChildren<Text>(includeInactive: true);
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			Hardcode component = componentsInChildren[i].GetComponent<Hardcode>();
			if (!(component != null) && WordGlobal.IsInteger(componentsInChildren[i].text) && componentsInChildren[i].text.Length <= 4)
			{
				componentsInChildren[i].text = TextLibraryDataManagerCsv.instance.GetText(int.Parse(componentsInChildren[i].text));
			}
		}
	}
}
