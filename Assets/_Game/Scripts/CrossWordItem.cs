using System;

[Serializable]
public class CrossWordItem
{
	public bool vertical;

	public int Row;

	public int Col;

	public int Score;

	public int Length;

	public string Word;

	public CrossWordItem(string str)
	{
		Word = str;
		vertical = true;
		Row = 0;
		Col = 0;
	}

	public CrossWordItem(int col, int row, bool isvertical, int len, int score)
	{
		Col = col;
		Row = row;
		vertical = isvertical;
		Length = len;
		Score = score;
	}
}
