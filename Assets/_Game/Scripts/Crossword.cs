using System;
using System.Collections.Generic;
using UnityEngine;

public class Crossword
{
	public List<string> _wordList;

	public List<CrossWordItem> _crossWordList;

	public List<CrossWordItem> _currentCrossWordList;

	public List<List<string>> _grid;

	public string _empty = "-";

	public int _maxRow = 15;

	public int _maxCol = 15;

	public int _maxLoops = 2000;

	public bool _MinRCDiff = true;

	public List<CrossWordItem> _fitMoneyPosList = new List<CrossWordItem>();

	public Crossword()
	{
		_crossWordList = new List<CrossWordItem>();
		_wordList = new List<string>();
		_currentCrossWordList = new List<CrossWordItem>();
		_grid = InitGrid(_maxRow, _maxCol);
	}

	public List<List<string>> InitGrid(int row, int col)
	{
		List<List<string>> list = new List<List<string>>();
		for (int i = 0; i < row; i++)
		{
			List<string> list2 = new List<string>();
			for (int j = 0; j < col; j++)
			{
				list2.Add(_empty);
			}
			list.Add(list2);
		}
		return list;
	}

	public bool ComputeCrossWord(List<string> list, float time_permitted = 2f, int spins = 2)
	{
		string empty = string.Empty;
		DateTime now = DateTime.Now;
		Crossword crossword = new Crossword();
		InitList(list);
		double num = 0.0;
		int num2 = 0;
		int num3 = 0;
		int num4 = _maxCol * _maxRow;
		int num5 = _maxCol * _maxRow;
		int num6 = 100;
		bool result = false;
		List<List<string>> list2 = new List<List<string>>();
		while (num2 < 3000)
		{
			DateTime now2 = DateTime.Now;
			num2++;
			crossword._currentCrossWordList = new List<CrossWordItem>();
			crossword._grid = InitGrid(_maxRow, _maxCol);
			crossword.InitList(list);
			for (int i = 0; i < spins; i++)
			{
				foreach (CrossWordItem item in crossword._crossWordList)
				{
					if (crossword._currentCrossWordList.FindIndex((CrossWordItem w) => w.Word == item.Word) < 0)
					{
						crossword.FitAndAdd(item);
					}
				}
			}
			if (crossword._currentCrossWordList.Count != _crossWordList.Count)
			{
				continue;
			}
			crossword.RemoveEmptyGrid();
			if (crossword._grid.Count > crossword._grid[0].Count)
			{
				continue;
			}
			result = true;
			num3++;
			double totalSeconds = (DateTime.Now - now2).TotalSeconds;
			if (totalSeconds > num)
			{
				num = totalSeconds;
			}
			int num7 = crossword._grid.Count * crossword._grid[0].Count;
			if (_MinRCDiff)
			{
				if (crossword._grid[0].Count - crossword._grid.Count > num6 || (crossword._grid[0].Count - crossword._grid.Count == num6 && num7 > num4))
				{
					continue;
				}
			}
			else if (num7 >= num4)
			{
				continue;
			}
			num6 = crossword._grid[0].Count - crossword._grid.Count;
			num4 = num7;
			list2 = new List<List<string>>();
			list2 = crossword._grid;
			_grid = crossword._grid;
			_currentCrossWordList = crossword._currentCrossWordList;
		}
		return result;
	}

	private void AddWordToList(string word, bool isRow)
	{
		int num = 0;
		int num2 = 0;
		if (isRow)
		{
			int num3 = UnityEngine.Random.Range(0, 2);
			num2 = UnityEngine.Random.Range(1, _grid[0].Count + 1 - word.Length);
			if (num3 < 1)
			{
				num = 1;
				for (int i = 0; i < _currentCrossWordList.Count; i++)
				{
					_currentCrossWordList[i].Row += 2;
				}
			}
			else
			{
				num = _grid.Count;
			}
		}
		else
		{
			int num4 = UnityEngine.Random.Range(0, 2);
			num = UnityEngine.Random.Range(1, _grid.Count + 2 - word.Length);
			if (num4 < 1)
			{
				num2 = 1;
				for (int j = 0; j < _currentCrossWordList.Count; j++)
				{
					_currentCrossWordList[j].Col += 2;
				}
			}
			else
			{
				num2 = _grid[0].Count;
			}
		}
		CrossWordItem crossWordItem = new CrossWordItem(word);
		crossWordItem.Col = num2;
		crossWordItem.Row = num;
		crossWordItem.vertical = !isRow;
		_currentCrossWordList.Add(crossWordItem);
		InitGridByWordList();
	}

	private void InitGridByWordList()
	{
		for (int i = 0; i < _currentCrossWordList.Count; i++)
		{
			SetWord(_currentCrossWordList[i]);
		}
	}

	private List<CrossWordItem> SuggestCoord(CrossWordItem word)
	{
		int num = -1;
		int num2 = 0;
		int num3 = 0;
		List<CrossWordItem> list = new List<CrossWordItem>();
		for (int i = 0; i < word.Word.Length; i++)
		{
			string a = word.Word.Substring(i, 1);
			num++;
			num2 = 0;
			for (int j = 0; j < _maxRow; j++)
			{
				num2++;
				num3 = 0;
				List<string> list2 = _grid[j];
				for (int k = 0; k < _maxCol; k++)
				{
					num3++;
					if (a == list2[k])
					{
						if (num2 - num > 0 && num2 - num + word.Word.Length <= _maxRow)
						{
							list.Add(new CrossWordItem(num3, num2 - num, isvertical: true, num3 + (num2 - num), 0));
						}
						if (num3 - num > 0 && num3 - num + word.Word.Length <= _maxCol)
						{
							list.Add(new CrossWordItem(num3 - num, num2, isvertical: false, num2 + (num3 - num), 0));
						}
					}
				}
			}
		}
		return Sort_CoordList(list, word);
	}

	private List<CrossWordItem> Sort_CoordList(List<CrossWordItem> list, CrossWordItem item)
	{
		List<CrossWordItem> list2 = new List<CrossWordItem>();
		for (int i = 0; i < list.Count; i++)
		{
			CrossWordItem crossWordItem = list[i];
			crossWordItem.Score = CheckFitScore(crossWordItem.Col, crossWordItem.Row, crossWordItem.vertical, item);
			if (crossWordItem.Score > 0)
			{
				list2.Add(crossWordItem);
			}
		}
		List<CrossWordItem> list3 = WordGlobal.ShuffleList(list2);
		list3.Sort((CrossWordItem x, CrossWordItem y) => -x.Score.CompareTo(y.Score));
		return list3;
	}

	private int CheckFitScore(int col, int row, bool vertical, CrossWordItem word)
	{
		if (col < 1 || row < 1)
		{
			return 0;
		}
		int num = 1;
		int num2 = 1;
		string empty = string.Empty;
		for (int i = 0; i < word.Word.Length; i++)
		{
			string b = word.Word.Substring(i, 1);
			empty = GetCell(col, row);
			if (empty != _empty && empty != b)
			{
				return 0;
			}
			if (empty == b)
			{
				num2++;
				if (vertical)
				{
					if (!CheckIfCellClear(col + 1, row) && !CheckIfCellClear(col - 1, row))
					{
						num2++;
					}
				}
				else if (!CheckIfCellClear(col, row + 1) && !CheckIfCellClear(col, row - 1))
				{
					num2++;
				}
				if (!GetPosVertical(row, col, vertical))
				{
					return 0;
				}
			}
			if (vertical)
			{
				if (empty != b)
				{
					if (!CheckIfCellClear(col + 1, row))
					{
						return 0;
					}
					if (!CheckIfCellClear(col - 1, row))
					{
						return 0;
					}
				}
				if (num == 1 && !CheckIfCellClear(col, row - 1))
				{
					return 0;
				}
				if (num == word.Word.Length && !CheckIfCellClear(col, row + 1))
				{
					return 0;
				}
			}
			else
			{
				if (empty != b)
				{
					if (!CheckIfCellClear(col, row - 1))
					{
						return 0;
					}
					if (!CheckIfCellClear(col, row + 1))
					{
						return 0;
					}
				}
				if (num == 1 && !CheckIfCellClear(col - 1, row))
				{
					return 0;
				}
				if (num == word.Word.Length && !CheckIfCellClear(col + 1, row))
				{
					return 0;
				}
			}
			if (vertical)
			{
				row++;
			}
			else
			{
				col++;
			}
			num++;
		}
		return num2;
	}

	private bool CheckIfCellClear(int col, int row)
	{
		string cell = GetCell(col, row);
		if (cell == _empty)
		{
			return true;
		}
		return false;
	}

	private string GetCell(int col, int row)
	{
		if (col < 1 || row < 1 || col > _grid[0].Count || row > _grid.Count)
		{
			return _empty;
		}
		return _grid[row - 1][col - 1];
	}

	private void SetCell(int col, int row, string str)
	{
		_grid[row - 1][col - 1] = str;
	}

	public void RemoveEmptyGrid()
	{
		int num = _maxRow;
		int num2 = 0;
		int num3 = _maxRow;
		int num4 = 0;
		for (int i = 0; i < _currentCrossWordList.Count; i++)
		{
			CrossWordItem crossWordItem = _currentCrossWordList[i];
			if (crossWordItem.Row < num)
			{
				num = crossWordItem.Row;
			}
			if (crossWordItem.Col < num3)
			{
				num3 = crossWordItem.Col;
			}
			if (!crossWordItem.vertical)
			{
				if (crossWordItem.Col + crossWordItem.Word.Length > num4)
				{
					num4 = crossWordItem.Col + crossWordItem.Word.Length - 1;
				}
				if (crossWordItem.Row > num2)
				{
					num2 = crossWordItem.Row;
				}
			}
			else
			{
				if (crossWordItem.Col > num4)
				{
					num4 = crossWordItem.Col;
				}
				if (crossWordItem.Row + crossWordItem.Word.Length > num2)
				{
					num2 = crossWordItem.Row + crossWordItem.Word.Length - 1;
				}
			}
		}
		_grid = new List<List<string>>();
		_grid = InitGrid(num2 - num + 1, num4 - num3 + 1);
		for (int j = 0; j < _currentCrossWordList.Count; j++)
		{
			_currentCrossWordList[j].Col -= num3 - 1;
			_currentCrossWordList[j].Row -= num - 1;
			SetWord(_currentCrossWordList[j]);
		}
	}

	public void FitAndAdd(CrossWordItem word)
	{
		int num = 0;
		bool flag = false;
		List<CrossWordItem> list = SuggestCoord(word);
		while (!flag && num < _maxLoops)
		{
			if (_currentCrossWordList.Count == 0)
			{
				int num2 = UnityEngine.Random.Range(0, 2);
				bool flag2 = (num2 > 0) ? true : false;
				int num3 = 1;
				int num4 = 1;
				if (flag2)
				{
					num3 = (_maxCol + 1) / 2;
					num4 = (_maxRow + 1) / 2 - (word.Length + 1) / 2;
				}
				else
				{
					num3 = (_maxCol + 1) / 2 - (word.Length + 1) / 2;
					num4 = (_maxRow + 1) / 2;
				}
				if (CheckFitScore(num3, num4, flag2, word) > 0)
				{
					flag = true;
					SetWord(num3, num4, flag2, word);
				}
			}
			else
			{
				if (num >= list.Count)
				{
					break;
				}
				if (list[num].Score > 0)
				{
					flag = true;
					SetWord(list[num].Col, list[num].Row, list[num].vertical, word);
				}
			}
			num++;
		}
	}

	private void SetWord(CrossWordItem word)
	{
		int num = word.Col;
		int num2 = word.Row;
		for (int i = 0; i < word.Word.Length; i++)
		{
			SetCell(num, num2, word.Word.Substring(i, 1));
			if (word.vertical)
			{
				num2++;
			}
			else
			{
				num++;
			}
		}
	}

	private void SetWord(int col, int row, bool vertical, CrossWordItem word)
	{
		word.Col = col;
		word.Row = row;
		word.vertical = vertical;
		_currentCrossWordList.Add(word);
		for (int i = 0; i < word.Word.Length; i++)
		{
			SetCell(col, row, word.Word.Substring(i, 1));
			if (vertical)
			{
				row++;
			}
			else
			{
				col++;
			}
		}
	}

	private void InitList(List<string> list)
	{
		List<string> list2 = WordGlobal.ShuffleList(list);
		_crossWordList = new List<CrossWordItem>();
		for (int i = 0; i < list2.Count; i++)
		{
			CrossWordItem item = new CrossWordItem(list2[i]);
			_crossWordList.Add(item);
		}
		_crossWordList.Sort((CrossWordItem x, CrossWordItem y) => (x.Word.Length > y.Word.Length) ? (-1) : 1);
	}

	private bool GetPosVertical(int row, int col, bool v)
	{
		bool result = false;
		for (int i = 0; i < _currentCrossWordList.Count; i++)
		{
		}
		for (int j = 0; j < _currentCrossWordList.Count; j++)
		{
			int num = _maxRow;
			int num2 = 0;
			int num3 = _maxRow;
			int num4 = 0;
			CrossWordItem crossWordItem = _currentCrossWordList[j];
			if (crossWordItem.Row < num)
			{
				num = crossWordItem.Row;
			}
			if (crossWordItem.Col < num3)
			{
				num3 = crossWordItem.Col;
			}
			if (!crossWordItem.vertical)
			{
				if (crossWordItem.Col + crossWordItem.Word.Length > num4)
				{
					num4 = crossWordItem.Col + crossWordItem.Word.Length;
				}
				if (crossWordItem.Row > num2)
				{
					num2 = crossWordItem.Row;
				}
			}
			else
			{
				if (crossWordItem.Col > num4)
				{
					num4 = crossWordItem.Col;
				}
				if (crossWordItem.Row + crossWordItem.Word.Length > num2)
				{
					num2 = crossWordItem.Row + crossWordItem.Word.Length;
				}
			}
			if (row >= num && row <= num2 && col >= num3 && col <= num4)
			{
				result = ((_currentCrossWordList[j].vertical != v) ? true : false);
				break;
			}
		}
		return result;
	}

	public void PrintGrid(List<List<string>> grid)
	{
		for (int i = 0; i < grid.Count; i++)
		{
			string text = string.Empty;
			for (int j = 0; j < grid[i].Count; j++)
			{
				text = text + grid[i][j] + "|";
			}
			Debug.Log(text);
		}
	}

	public void AddGridRC(bool isRow, int count, bool isStart)
	{
		if (isRow)
		{
			_grid = InitGrid(_grid.Count + count, _grid[0].Count);
		}
		else
		{
			_grid = InitGrid(_grid.Count, _grid[0].Count + count);
		}
		for (int i = 0; i < _currentCrossWordList.Count; i++)
		{
			if (isStart)
			{
				if (isRow)
				{
					_currentCrossWordList[i].Row += count;
				}
				else
				{
					_currentCrossWordList[i].Col += count;
				}
			}
		}
		for (int j = 0; j < _currentCrossWordList.Count; j++)
		{
			SetWord(_currentCrossWordList[j]);
		}
	}

	public bool FitMoneyWord(string moneyword)
	{
		List<CrossWordItem> list = new List<CrossWordItem>();
		for (int i = 1; i <= _grid.Count; i++)
		{
			for (int j = 1; j <= _grid[i - 1].Count; j++)
			{
				if (CheckMoneyFit(i, j, vertical: true, moneyword))
				{
					CrossWordItem crossWordItem = new CrossWordItem(moneyword);
					crossWordItem.Col = j;
					crossWordItem.Row = i;
					crossWordItem.vertical = true;
					list.Add(crossWordItem);
				}
				if (CheckMoneyFit(i, j, vertical: false, moneyword))
				{
					CrossWordItem crossWordItem2 = new CrossWordItem(moneyword);
					crossWordItem2.Col = j;
					crossWordItem2.Row = i;
					crossWordItem2.vertical = false;
					list.Add(crossWordItem2);
				}
			}
		}
		_fitMoneyPosList = WordGlobal.ShuffleList(list);
		return (_fitMoneyPosList.Count > 0) ? true : false;
	}

	private bool CheckMoneyFit(int row, int col, bool vertical, string word)
	{
		if (col < 1 || row < 1)
		{
			return false;
		}
		for (int i = 0; i < word.Length; i++)
		{
			if (!CheckEmptyFit(row, col))
			{
				return false;
			}
			if (vertical)
			{
				row++;
			}
			else
			{
				col++;
			}
		}
		return true;
	}

	private bool CheckEmptyFit(int row, int col)
	{
		if (row <= _grid.Count && col <= _grid[0].Count && CheckIfCellClear(col, row) && CheckIfCellClear(col - 1, row) && CheckIfCellClear(col + 1, row) && CheckIfCellClear(col, row - 1) && CheckIfCellClear(col, row + 1))
		{
			return true;
		}
		return false;
	}
}
