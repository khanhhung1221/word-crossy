using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

public sealed class CsvReader : IDisposable
{
	private Encoding __encoding;

	private long __rowno;

	private TextReader __reader;

	private static Regex rexCsvSplitter = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

	private static Regex rexRunOnLine = new Regex("^[^\"]*(?:\"[^\"]*\"[^\"]*)*\"[^\"]*$");

	public IEnumerable RowEnumerator
	{
		get
		{
			if (__reader == null)
			{
				throw new ApplicationException("I can't start reading without CSV input.");
			}
			__rowno = 0L;
			UTF8Encoding utf8 = new UTF8Encoding();
			while (true)
			{
				string text;
				string sLine2 = text = __reader.ReadLine();
				if (text == null)
				{
					break;
				}
				sLine2 = utf8.GetString(Encoding.Convert(__encoding, utf8, __encoding.GetBytes(sLine2)));
				while (rexRunOnLine.IsMatch(sLine2))
				{
					string sNextLine = text = __reader.ReadLine();
					if (text == null)
					{
						break;
					}
					sLine2 = sLine2 + "\n" + sNextLine;
				}
				__rowno++;
				string[] values = rexCsvSplitter.Split(sLine2);
				for (int i = 0; i < values.Length; i++)
				{
					values[i] = Csv.Unescape(values[i]);
				}
				yield return values;
			}
			__reader.Close();
		}
	}

	public long RowIndex => __rowno;

	public CsvReader(string fileName, Encoding encoding)
		: this(new FileStream(fileName, FileMode.Open, FileAccess.Read), encoding)
	{
	}

	public CsvReader(Stream stream, Encoding encoding)
	{
		__reader = new StreamReader(stream, encoding);
		__encoding = encoding;
	}

	public void Dispose()
	{
		if (__reader != null)
		{
			__reader.Dispose();
		}
	}
}
