using System;
using UnityEngine;
using UnityEngine.UI;

public class DPCCalendarDayItem : MonoBehaviour
{
	public Image bg_img;

	public Text day_num_text;

	public Image star01;

	public Image star02;

	public Image star03;

	private void Start()
	{
	}

	public void Init(DateTime day, int thisMonth, int get_star_num, DPCCalendarUI cal_ui)
	{
		if (day.Month == thisMonth)
		{
			day_num_text.color = WordGlobal.ChangeStringToColor("#000000");
		}
		else
		{
			day_num_text.color = WordGlobal.ChangeStringToColor("#7D7D7D");
		}
		Color color;
		if (day.Year == cal_ui.now_dt.Year && day.Month == cal_ui.now_dt.Month && day.Day == cal_ui.now_dt.Day)
		{
			day_num_text.color = WordGlobal.ChangeStringToColor("#FFFFFF");
			bg_img.gameObject.SetActive(value: true);
			color = WordGlobal.ChangeStringToColor("#FFC435");
		}
		else
		{
			bg_img.gameObject.SetActive(value: false);
			color = WordGlobal.ChangeStringToColor("#FFC435");
		}
		day_num_text.text = day.Day + string.Empty;
		Color color2 = WordGlobal.ChangeStringToColor("#C4C4C4");
		star01.color = color2;
		star02.color = color2;
		star03.color = color2;
		switch (get_star_num)
		{
		case 3:
			star01.color = color;
			star02.color = color;
			star03.color = color;
			break;
		case 2:
			star01.color = color;
			star02.color = color;
			break;
		case 1:
			star01.color = color;
			break;
		}
	}
}
