using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DPCCalendarUI : MonoBehaviour
{
	public Text Month_text;

	public Transform Days_tran;

	public GameObject DayItem_go;

	private int total_dayNum = 42;

	private List<DPCCalendarDayItem> day_item_list = new List<DPCCalendarDayItem>();

	private int min_month;

	private int max_month;

	private int cur_month;

	public DateTime now_dt;

	private void Start()
	{
		Init();
	}

	public void Init()
	{
		CreateDayItems();
		min_month = WordJsonDataManager.instance.GetDailyMinLevel() / 100;
		max_month = WordJsonDataManager.instance.GetDailyMaxLevel() / 100;
		cur_month = 0;
		int dailyDayId = PlayerInfo.Instance.GetDailyDayId();
		int year = 2000 + dailyDayId / 10000;
		int month = dailyDayId % 10000 / 100;
		int day = dailyDayId % 100;
		now_dt = new DateTime(year, month, day);
		SetCurMonth(now_dt);
	}

	public void SetCurMonth(DateTime month)
	{
		List<DateTime> list = Days(month);
		for (int i = 0; i < list.Count; i++)
		{
			int id = list[i].Year % 100 * 10000 + list[i].Month * 100 + list[i].Day;
			DailyPuzzleDataItem dailyPuzzleItemById = PlayerInfo.Instance.GetDailyPuzzleItemById(id);
			int get_star_num = 0;
			if (dailyPuzzleItemById != null)
			{
				get_star_num = dailyPuzzleItemById.Stars;
			}
			day_item_list[i].Init(list[i], month.Month, get_star_num, this);
		}
		int textId = 1129 + month.Month;
		Month_text.text = TextLibraryDataManagerCsv.instance.GetText(textId);
	}

	public List<DateTime> Days(DateTime month)
	{
		List<DateTime> list = new List<DateTime>();
		DateTime dateTime = new DateTime(month.Year, month.Month, 1);
		DayOfWeek dayOfWeek = dateTime.DayOfWeek;
		int num = (int)dayOfWeek;
		if (num.Equals(0))
		{
			num = 7;
		}
		for (int num2 = num; num2 > 0; num2--)
		{
			list.Add(dateTime.AddDays(-num2));
		}
		for (int i = 0; i < total_dayNum - num; i++)
		{
			list.Add(dateTime.AddDays(i));
		}
		return list;
	}

	public void CreateDayItems()
	{
		day_item_list.Clear();
		for (int i = 0; i < total_dayNum; i++)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(DayItem_go);
			gameObject.name = "day_" + i;
			gameObject.transform.SetParent(Days_tran, worldPositionStays: false);
			RectTransform rectTransform = gameObject.transform as RectTransform;
			float x = -270 + i % 7 * 90;
			float y = 130 - i / 7 * 90;
			rectTransform.anchoredPosition = new Vector2(x, y);
			DPCCalendarDayItem component = gameObject.GetComponent<DPCCalendarDayItem>();
			gameObject.SetActive(value: true);
			day_item_list.Add(component);
		}
	}

	public void LeftClick()
	{
		cur_month--;
		DateTime curMonth = DateTime.Now.AddMonths(cur_month);
		int num = curMonth.Year % 100 * 100 + curMonth.Month;
		if (num < min_month)
		{
			cur_month++;
		}
		else
		{
			SetCurMonth(curMonth);
		}
	}

	public void RightClick()
	{
		
		cur_month++;
		DateTime curMonth = DateTime.Now.AddMonths(cur_month);
		int num = curMonth.Year % 100 * 100 + curMonth.Month;
		if (num > max_month)
		{
			cur_month--;
		}
		else
		{
			SetCurMonth(curMonth);
		}
	}

	public void BtnClose()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}
}
