using UnityEngine;

public class DPCTutorialInfo : MonoBehaviour
{
	[HideInInspector]
	public string Obj_name;

	public string parent_name;

	private void Start()
	{
		Obj_name = base.transform.name;
		parent_name = base.transform.parent.name;
	}
}
