using UnityEngine;
using UnityEngine.UI;

public class DPCTutorialTip : MonoBehaviour
{
	public static bool isDPCTutorialGoto;

	public int Type;

	public Sprite[] icon_sps;

	public Image icon_img;

	public Text name_txt;

	public Text title_text;

	public Text des_text;

	public Text btn_text;

	private void Start()
	{
		if (Type == 0)
		{
			icon_img.sprite = icon_sps[0];
			name_txt.text = TextLibraryDataManagerCsv.instance.GetText(1252);
			title_text.text = TextLibraryDataManagerCsv.instance.GetText(1182);
			des_text.text = TextLibraryDataManagerCsv.instance.GetText(1172);
			btn_text.text = TextLibraryDataManagerCsv.instance.GetText(1173);
		}
		else if (Type == 1)
		{
			icon_img.sprite = icon_sps[1];
			name_txt.text = TextLibraryDataManagerCsv.instance.GetText(1260);
			title_text.text = TextLibraryDataManagerCsv.instance.GetText(1182);
			des_text.text = TextLibraryDataManagerCsv.instance.GetText(1250);
			btn_text.text = TextLibraryDataManagerCsv.instance.GetText(1173);
		}
	}

	public void BtnGoToClick()
	{
		if (Type == 0)
		{
			if ((bool)MainUIScript.Instance)
			{
				BtnsCtrlUIScript.isShowPopOffer = false;
				MainUIScript.Instance.BtnCloseClick();
				UIManager.Instance.ClearPop();
				isDPCTutorialGoto = true;
			}
		}
		else if (Type == 1)
		{
			WordGlobal.isWordSearchTutorialFromMainUI = true;
			if ((bool)MainUIScript.Instance)
			{
				BtnsCtrlUIScript.isShowPopOffer = false;
				MainUIScript.Instance.BtnCloseClick();
				UIManager.Instance.ClearPop();
				isDPCTutorialGoto = true;
			}
		}
		BtnBackClick();
	}

	public void BtnBackClick()
	{
		Object.Destroy(base.gameObject);
	}
}
