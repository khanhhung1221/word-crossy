using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DPCTutorialUI : MonoBehaviour
{
	public static bool isDPCTutorialShow;

	public static int HideState;

	public static CrossWordItem WordItem;

	public static bool isWordItemChange;

	public Transform Main_tran;

	public RectTransform tip_rectTran;

	public Text des_text;

	public Text des_up_text;

	public GameObject Btn_go;

	public Text btn_text;

	public Image[] hide_img;

	public Text[] hide_text;

	public RectTransform jiantou_img;

	private DailyPuzzleGetImageData _curDailyImageData;

	private int Tutorial;

	private List<DPCTutorialInfo> temp_trans = new List<DPCTutorialInfo>();

	private List<Canvas> item_canvas = new List<Canvas>();

	private string color_str = "fffc00";

	private string color_end = "</color>";

	private string tutorial01_word = string.Empty;

	private Color base_color;

	private int lineRender_order;

	private bool isHide;

	private bool isPlayHideAnim;

	private void Start()
	{
		isDPCTutorialShow = true;
		Init();
	}

	public void PlayShowAnim()
	{
		tip_rectTran.localScale = Vector3.zero;
		tip_rectTran.DOScale(1f, 0.5f);
	}

	public void Init(bool isPlayAnim = true)
	{
		if (isPlayAnim)
		{
			PlayShowAnim();
		}
		_curDailyImageData = PlayerInfo.Instance.GetDailyPuzzleImageData();
		Tutorial = _curDailyImageData.Tutorial;
		Btn_go.SetActive(value: false);
		HideState = 0;
		jiantou_img.gameObject.SetActive(value: false);
		des_text.gameObject.SetActive(value: true);
		des_up_text.gameObject.SetActive(value: false);
		if (Tutorial == 0)
		{
			des_text.text = TextLibraryDataManagerCsv.instance.GetText(1168);
			RectTransform rectTransform = tip_rectTran;
			Vector2 anchoredPosition = tip_rectTran.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, 30f);
			RectTransform rectTransform2 = tip_rectTran;
			Vector2 sizeDelta = tip_rectTran.sizeDelta;
			rectTransform2.sizeDelta = new Vector2(sizeDelta.x, 200f);
		}
		else if (Tutorial == 1)
		{
			tutorial01_word = WordItem.Word;
			base_color = WordGlobal.ChangeStringToColor("#" + color_str);
			string text = TextLibraryDataManagerCsv.instance.GetText(1169).Replace("{0}", "<color=#" + color_str + ">" + WordItem.Word + color_end);
			des_text.text = text;
			RectTransform rectTransform3 = tip_rectTran;
			Vector2 anchoredPosition2 = tip_rectTran.anchoredPosition;
			rectTransform3.anchoredPosition = new Vector2(anchoredPosition2.x, 30f + WordGlobal.showBanner_bottomupy);
			RectTransform rectTransform4 = tip_rectTran;
			Vector2 sizeDelta2 = tip_rectTran.sizeDelta;
			rectTransform4.sizeDelta = new Vector2(sizeDelta2.x, 200f);
			if (MainUIScript.Instance != null && MainUIScript.Instance.m_DrawLine != null)
			{
				lineRender_order = MainUIScript.Instance.m_DrawLine.GetComponent<LineRenderer>().sortingOrder;
				MainUIScript.Instance._catmullDraw.SetLineOrder(16);
			}
		}
		else if (Tutorial == 2)
		{
			Btn_go.SetActive(value: true);
			des_text.gameObject.SetActive(value: false);
			des_up_text.gameObject.SetActive(value: true);
			btn_text.text = TextLibraryDataManagerCsv.instance.GetText(1007);
			des_up_text.text = TextLibraryDataManagerCsv.instance.GetText(1170);
			RectTransform rectTransform5 = tip_rectTran;
			Vector2 anchoredPosition3 = tip_rectTran.anchoredPosition;
			rectTransform5.anchoredPosition = new Vector2(anchoredPosition3.x, -300f + WordGlobal.showBanner_bottomupy);
			RectTransform rectTransform6 = tip_rectTran;
			Vector2 sizeDelta3 = tip_rectTran.sizeDelta;
			rectTransform6.sizeDelta = new Vector2(sizeDelta3.x, 270f);
		}
		ShowLogic();
	}

	public void ShowLogic()
	{
		temp_trans.Clear();
		for (int i = 0; i < hide_img.Length; i++)
		{
			Image obj = hide_img[i];
			Color color = hide_img[i].color;
			float r = color.r;
			Color color2 = hide_img[i].color;
			float g = color2.g;
			Color color3 = hide_img[i].color;
			obj.color = new Color(r, g, color3.b, 1f);
		}
		for (int j = 0; j < hide_text.Length; j++)
		{
			Text obj2 = hide_text[j];
			Color color4 = hide_text[j].color;
			float r2 = color4.r;
			Color color5 = hide_text[j].color;
			float g2 = color5.g;
			Color color6 = hide_text[j].color;
			obj2.color = new Color(r2, g2, color6.b, 1f);
		}
		DPCTutorialInfo[] array = null;
		if (Tutorial == 0)
		{
			array = base.transform.parent.GetComponentsInChildren<DPCTutorialInfo>();
			for (int k = 0; k < array.Length; k++)
			{
				if (array[k].Obj_name.Equals("DailyPuzzleBtn"))
				{
					array[k].transform.SetParent(Main_tran, worldPositionStays: true);
					jiantou_img.gameObject.SetActive(value: true);
					jiantou_img.transform.SetParent(array[k].transform);
					jiantou_img.anchoredPosition = Vector2.zero;
					temp_trans.Add(array[k]);
				}
			}
		}
		else if (Tutorial == 1)
		{
			array = base.transform.parent.GetComponentsInChildren<DPCTutorialInfo>(includeInactive: true);
			for (int l = 0; l < array.Length; l++)
			{
				if (array[l].Obj_name.Equals("ItemRoot"))
				{
					array[l].GetComponent<GridLayoutGroup>().enabled = false;
				}
				else if (array[l].Obj_name.Equals("LetterBG"))
				{
					array[l].transform.SetParent(Main_tran);
					array[l].transform.Find("lettersRoot").GetComponent<Canvas>().sortingOrder = 17;
					temp_trans.Add(array[l]);
				}
				else if (array[l].transform.name.Equals("SelectLetterBG"))
				{
					array[l].transform.SetParent(Main_tran);
					temp_trans.Add(array[l]);
				}
			}
			if (!(MainUIScript.Instance != null) || !(MainUIScript.Instance.m_TfItemRoot != null))
			{
				return;
			}
			int num = WordItem.Row - 1;
			int num2 = WordItem.Col - 1;
			bool vertical = WordItem.vertical;
			List<GameObject> list = new List<GameObject>();
			bool flag = true;
			do
			{
				GameObject itemByRC = MainUIScript.Instance.GetItemByRC(num, num2);
				if (itemByRC != null)
				{
					ItemScript component = itemByRC.GetComponent<ItemScript>();
					if (component._imgState == 1 || component._imgState == 2)
					{
						Canvas canvas = itemByRC.AddComponent<Canvas>();
						canvas.overrideSorting = true;
						canvas.sortingOrder = 16;
						item_canvas.Add(canvas);
					}
					else
					{
						flag = false;
					}
				}
				else
				{
					flag = false;
				}
				if (vertical)
				{
					num++;
				}
				else
				{
					num2++;
				}
			}
			while (flag);
		}
		else
		{
			if (Tutorial != 2 || !(MainUIScript.Instance != null) || !(MainUIScript.Instance.m_TfItemRoot != null))
			{
				return;
			}
			int num3 = WordItem.Row - 1;
			int num4 = WordItem.Col - 1;
			bool vertical2 = WordItem.vertical;
			List<GameObject> list2 = new List<GameObject>();
			bool flag2 = true;
			do
			{
				GameObject itemByRC2 = MainUIScript.Instance.GetItemByRC(num3, num4);
				if (itemByRC2 != null)
				{
					ItemScript component2 = itemByRC2.GetComponent<ItemScript>();
					if (component2._imgState == 1 || component2._imgState == 2)
					{
						Canvas canvas2 = itemByRC2.GetComponent<Canvas>();
						if (canvas2 == null)
						{
							canvas2 = itemByRC2.AddComponent<Canvas>();
						}
						canvas2.overrideSorting = true;
						canvas2.sortingOrder = 16;
						item_canvas.Add(canvas2);
					}
					else
					{
						flag2 = false;
					}
				}
				else
				{
					flag2 = false;
				}
				if (vertical2)
				{
					num3++;
				}
				else
				{
					num4++;
				}
			}
			while (flag2);
		}
	}

	private void Update()
	{
		if (HideState > 0)
		{
			isHide = true;
			if (Tutorial == 1)
			{
				isHide = false;
				Hide();
				isPlayHideAnim = true;
				float duration = 0.5f;
				for (int i = 0; i < hide_img.Length; i++)
				{
					if (i == 0)
					{
						hide_img[i].DOFade(0f, duration).OnComplete(delegate
						{
							isPlayHideAnim = false;
							Main_tran.gameObject.SetActive(value: false);
						});
					}
					else
					{
						hide_img[i].DOFade(0f, duration);
					}
				}
				for (int j = 0; j < hide_text.Length; j++)
				{
					hide_text[j].DOFade(0f, duration);
				}
			}
			else
			{
				Hide();
			}
			HideState = 0;
		}
		if (Tutorial == 1 && isPlayHideAnim && hide_text.Length > 0)
		{
			float r = base_color.r;
			float g = base_color.g;
			float b = base_color.b;
			Color color = hide_text[0].color;
			Color color2 = new Color(r, g, b, color.a);
			string text = WordGlobal.ChangeColorToString(color2);
			string text2 = TextLibraryDataManagerCsv.instance.GetText(1169).Replace("{0}", "<color=#" + text + ">" + tutorial01_word + color_end);
			des_text.text = text2;
		}
		if (isWordItemChange && !isPlayHideAnim)
		{
			Init();
			Main_tran.gameObject.SetActive(value: true);
			isWordItemChange = false;
		}
	}

	private void Hide()
	{
		for (int i = 0; i < temp_trans.Count; i++)
		{
			string parent_name = temp_trans[i].parent_name;
			if (temp_trans[i].transform.name.Equals("DailyPuzzleBtn"))
			{
				temp_trans[i].transform.SetParent(LevelMapUIScript.Instance.DownBtnsObj.transform);
			}
			else if (temp_trans[i].transform.name.Equals("LetterBG"))
			{
				temp_trans[i].transform.SetParent(MainUIScript.Instance.transform.Find("Bottom"));
				temp_trans[i].transform.Find("lettersRoot").GetComponent<Canvas>().sortingOrder = 1;
				if (MainUIScript.Instance != null && MainUIScript.Instance.m_DrawLine != null)
				{
					MainUIScript.Instance._catmullDraw.SetLineOrder(lineRender_order);
				}
			}
			else if (temp_trans[i].transform.name.Equals("SelectLetterBG"))
			{
				temp_trans[i].transform.SetParent(MainUIScript.Instance.transform.Find("Bottom"));
			}
			else
			{
				GameObject gameObject = GameObject.Find(parent_name);
				if (!(gameObject == null))
				{
					temp_trans[i].transform.SetParent(gameObject.transform);
				}
			}
		}
		for (int j = 0; j < item_canvas.Count; j++)
		{
			item_canvas[j].sortingOrder = 0;
		}
		jiantou_img.transform.SetParent(Main_tran);
		temp_trans.Clear();
		item_canvas.Clear();
		if (isHide)
		{
			isDPCTutorialShow = false;
			Object.Destroy(base.gameObject);
		}
	}

	public void BtnClick()
	{
		if (Tutorial == 2)
		{
			PlayerInfo.Instance.SetDailyImageDataTutorial(3);
			isHide = true;
			Hide();
		}
	}
}
