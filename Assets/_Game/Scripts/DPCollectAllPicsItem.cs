using UnityEngine;
using UnityEngine.UI;

public class DPCollectAllPicsItem : MonoBehaviour
{
	public static string icon_img_str;

	public Image icon_img;

	public Text day_text;

	private void Start()
	{
	}

	public void Init(Sprite sp, string str)
	{
		icon_img.sprite = sp;
		day_text.text = str;
	}

	public void OnClick()
	{
		icon_img_str = icon_img.sprite.name;
		if (GameObject.Find("DPCollectBigPic") == null)
		{
			UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCollectBigPic", isPop: true, UIManager.MainUIType.Type_Common, 99);
		}
	}
}
