using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DPCollectAllPicsUI : MonoBehaviour
{
	public GameObject picItem_go;

	public RectTransform Content_rectTran;

	public Text title_txt;

	private DailyPuzzleGetImageData _curDailyImageData;

	private void Start()
	{
		_curDailyImageData = PlayerInfo.Instance.GetDailyPuzzleImageData();
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1323);
		Init();
	}

	public void Init()
	{
		List<int> list = _curDailyImageData.ImageList;
		if (list == null)
		{
			list = new List<int>();
		}
		int num = 180;
		int count = list.Count;
		num = ((count % 3 != 0) ? (180 * (count / 3 + 1)) : (180 * count / 3));
		RectTransform content_rectTran = Content_rectTran;
		Vector2 sizeDelta = Content_rectTran.sizeDelta;
		content_rectTran.sizeDelta = new Vector2(sizeDelta.x, num);
		for (int i = 0; i < list.Count; i++)
		{
			int num2 = list[i];
			string str = "bg_puzzle_20" + num2;
			Sprite sp = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyPuzzle/" + str);
			GameObject gameObject = Object.Instantiate(picItem_go);
			gameObject.transform.SetParent(Content_rectTran, worldPositionStays: false);
			RectTransform rectTransform = gameObject.transform as RectTransform;
			float x = 94 + i % 3 * 178;
			float y = -95 - i / 3 * 178;
			rectTransform.anchoredPosition = new Vector2(x, y);
			DPCollectAllPicsItem component = gameObject.GetComponent<DPCollectAllPicsItem>();
			component.Init(sp, num2 + string.Empty);
			gameObject.SetActive(value: true);
		}
	}

	public void BtnCloseClick()
	{
		Object.Destroy(base.gameObject);
	}
}
