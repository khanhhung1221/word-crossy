using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class DPCollectBigPic : MonoBehaviour
{
	public Image icon_img;

	public Transform main_tran;

	private void Start()
	{
		icon_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyPuzzle/" + DPCollectAllPicsItem.icon_img_str);
		main_tran.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack);
	}

	public void Click()
	{
		main_tran.DOScale(Vector3.zero, 0.2f).OnComplete(delegate
		{
			Object.Destroy(base.gameObject);
		});
	}
}
