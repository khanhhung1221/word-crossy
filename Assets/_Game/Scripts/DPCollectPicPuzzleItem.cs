using UnityEngine;
using UnityEngine.UI;

public class DPCollectPicPuzzleItem : MonoBehaviour
{
	public RectTransform rectTran;

	public Image mask_img;

	public Image bg_img;

	public int my_index;

	public Vector2[] CenterPos;

	public Vector2 my_centerPos;

	private DPCollectPicUIScript dpcp;

	private void Start()
	{
	}

	public void Init(int index, DPCollectPicUIScript dpcp)
	{
		this.dpcp = dpcp;
		my_index = index;
		my_centerPos = CenterPos[my_index];
		mask_img.sprite = dpcp.puzzle_zhezhaos[my_index];
		bg_img.sprite = dpcp.cur_bigImg_sp;
		rectTran = (base.transform as RectTransform);
	}

	public void Show(bool isShow)
	{
		base.gameObject.SetActive(isShow);
	}

	private void Update()
	{
	}
}
