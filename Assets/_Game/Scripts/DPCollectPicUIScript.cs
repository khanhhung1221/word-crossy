using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DPCollectPicUIScript : MonoBehaviour
{
	public static bool isShow;

	public static int UI_OPENTYPE;

	public static int NEW_PUZZLEIDX = -1;

	public static bool isCloseUI;

	private int GETCOLD_NUM = 1000;

	private int[] openPuzzleStarNum = new int[9]
	{
		3,
		4,
		4,
		5,
		5,
		6,
		6,
		8,
		9
	};

	public RectTransform All_rectTran;

	public Transform gold_flyTran;

	public Image bg_img;

	public GameObject eff_go;

	private GameObject eff_dajiang_go;

	public GameObject other_eff_go;

	public Vector3[] other_eff_pos;

	public GameObject puzzle_item_go;

	public Image up_line_img;

	public Image up_white_img;

	public Image up_fly_img;

	public DOTweenPath doTweenPath;

	public Text btnCalendar_num_text;

	public Text Money_text;

	public Text des_text;

	public Text coin_text;

	public Image jindu_up_img;

	public Text jindu_text;

	public Text Mon_text;

	public Text Day_text;

	public GameObject gou_go;

	public Transform btnCol_tran;

	public Transform middle_tran;

	public RectTransform DownDay_rectTran;

	public GameObject Down_PlayBtn_go;

	public Text down_play_text;

	public GameObject Down_NextBtn_go;

	public Text down_next_text;

	public Transform MiddlePuzzle_tran;

	public RectTransform[] stars_rectTran;

	public Transform jindu_str_tran;

	public Transform jindu_tran;

	public GoldScript gold_script;

	public RectTransform[] OpenAnimTran;

	public GameObject[] FullPicHideGos;

	public Transform puzzle_itemsParent;

	public Sprite[] puzzle_zhezhaos;

	public GameObject[] eff_PuzzleShow_gos;

	public GameObject eff_PuzzleGet_go;

	public GameObject eff_BigPuzzleGet_go;

	public RectTransform eff_TuoWei_rectTran;

	public RectTransform Pos0;

	public Transform puzzleTempParent_tran;

	private List<DPCollectPicPuzzleItem> puzzle_items = new List<DPCollectPicPuzzleItem>();

	private List<int> PlayerInfo_PuzzleList = new List<int>();

	private int anim_index = -1;

	private int anim_end_num;

	private int anim_cur_num;

	public bool isSucCollPic;

	private int thisOnePuzzle_Idx = -1;

	public string cur_bigImg_name = string.Empty;

	public Sprite cur_bigImg_sp;

	private bool isOutMidFinish;

	private int today_starNum;

	public RectTransform m_RtfTop;

	private int btn_collect;

	private int glod = 1;

	private int lt = 2;

    private int cal = 3;

    private int down_ui = 4;

	public bool isTest;

	public int TestNum;

	private float yujiaz_waitTime;

	private float anim_waitTime;

	private int flyStar_num;

	private Vector3 start_fly_p;

	private Vector3 end_fly_p;

	private bool isCloseMainUI = true;

	private void Start()
	{
		isShow = true;
		All_rectTran.anchoredPosition = Vector2.zero;
		down_play_text.text = TextLibraryDataManagerCsv.instance.GetText(1075);
		down_next_text.text = TextLibraryDataManagerCsv.instance.GetText(1005);
		if (gold_script == null)
		{
			gold_script = WordGlobal.AddGoldUI(m_RtfTop);
			gold_script.m_ShowBanner = true;
			OpenAnimTran[glod] = (gold_script.transform as RectTransform);
			if (gold_script.banner_go != null)
			{
				gold_script.banner_go.GetComponent<Canvas>().sortingOrder = 20;
			}
		}
		if (UI_OPENTYPE == 1)
		{
			bg_img.gameObject.SetActive(value: false);
			HideUI();
			DailyPuzzleDataItem dailyPuzzleItem = PlayerInfo.Instance.GetDailyPuzzleItem();
			today_starNum = dailyPuzzleItem.Stars;
			for (int i = 0; i < stars_rectTran.Length; i++)
			{
				stars_rectTran[i].gameObject.SetActive(value: false);
				stars_rectTran[i].localScale = Vector3.one;
			}
			if (today_starNum > 0)
			{
				for (int j = 0; j < today_starNum && j < stars_rectTran.Length; j++)
				{
					stars_rectTran[j].gameObject.SetActive(value: true);
					if (j == 0)
					{
						stars_rectTran[j].DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f).OnComplete(delegate
						{
							Init();
						});
					}
					else
					{
						stars_rectTran[j].DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
					}
				}
			}
			else
			{
				Init();
			}
		}
		else if (UI_OPENTYPE == 2)
		{
			Init();
			All_rectTran.anchoredPosition = new Vector2(3000f, 3000f);
		}
		else
		{
			Init();
		}
	}

	public void HideUI()
	{
		middle_tran.localScale = Vector3.zero;
		RectTransform obj = OpenAnimTran[btn_collect];
		Vector2 anchoredPosition = OpenAnimTran[btn_collect].anchoredPosition;
		obj.anchoredPosition = new Vector2(60f, anchoredPosition.y);
		RectTransform obj2 = OpenAnimTran[glod];
		Vector2 anchoredPosition2 = OpenAnimTran[glod].anchoredPosition;
		obj2.anchoredPosition = new Vector2(142f, anchoredPosition2.y);
		RectTransform obj3 = OpenAnimTran[lt];
		Vector2 anchoredPosition3 = OpenAnimTran[lt].anchoredPosition;
		obj3.anchoredPosition = new Vector2(-142f, anchoredPosition3.y);
        RectTransform obj4 = OpenAnimTran[cal];
        Vector2 anchoredPosition4 = OpenAnimTran[cal].anchoredPosition;
        obj4.anchoredPosition = new Vector2(60f, anchoredPosition4.y);
        RectTransform obj5 = OpenAnimTran[down_ui];
		Vector2 anchoredPosition5 = OpenAnimTran[down_ui].anchoredPosition;
		obj5.anchoredPosition = new Vector2(anchoredPosition5.x, -400f);
	}

	private void Awake()
	{
		m_RtfTop.localPosition = new Vector3(0f, 0f - WordGlobal.GetSaftTopSize(), 0f);
	}

	public void OpenAnim()
	{
		HideUI();
		float duration = 0.5f;
		isOutMidFinish = false;
		middle_tran.DOScale(Vector3.one, duration).SetEase(Ease.OutBack).OnComplete(delegate
		{
			isOutMidFinish = true;
		});
		RectTransform obj = OpenAnimTran[btn_collect];
		Vector2 anchoredPosition = OpenAnimTran[btn_collect].anchoredPosition;
		obj.anchoredPosition = new Vector2(60, anchoredPosition.y);
		RectTransform obj2 = OpenAnimTran[glod];
		Vector2 anchoredPosition2 = OpenAnimTran[glod].anchoredPosition;
		obj2.anchoredPosition = new Vector2(142f, anchoredPosition2.y);
		RectTransform obj3 = OpenAnimTran[lt];
		Vector2 anchoredPosition3 = OpenAnimTran[lt].anchoredPosition;
		obj3.anchoredPosition = new Vector2(-90f, anchoredPosition3.y);
        RectTransform obj4 = OpenAnimTran[cal];
        Vector2 anchoredPosition4 = OpenAnimTran[cal].anchoredPosition;
        obj4.anchoredPosition = new Vector2(-60f, anchoredPosition4.y);
        RectTransform obj5 = OpenAnimTran[down_ui];
		Vector2 anchoredPosition5 = OpenAnimTran[down_ui].anchoredPosition;
		obj5.anchoredPosition = new Vector2(anchoredPosition5.x, -400f);
        OpenAnimTran[btn_collect].DOAnchorPosX(-60f, duration).SetEase(Ease.OutBack);
        OpenAnimTran[glod].DOAnchorPosX(-142f, duration).SetEase(Ease.OutBack);
		OpenAnimTran[lt].DOAnchorPosX(90f, duration).SetEase(Ease.OutBack);
        OpenAnimTran[cal].DOAnchorPosX(60f, duration).SetEase(Ease.OutBack);
        OpenAnimTran[down_ui].DOAnchorPosY(0f, duration).SetEase(Ease.OutBack);
	}

	public void InitEff()
	{
		if (UI_OPENTYPE != 0)
		{
			if (eff_PuzzleGet_go == null)
			{
				eff_PuzzleGet_go = Object.Instantiate(ResourceLoadManager.GetInstance().LoadObjByPath<GameObject>("UI/DailyPuzzle/Eff/pintuxiaochuxian"));
				eff_PuzzleGet_go.transform.SetParent(other_eff_go.transform);
				(eff_PuzzleGet_go.transform as RectTransform).anchoredPosition = new Vector2(0f, 0f);
				eff_PuzzleGet_go.transform.localScale = Vector3.one;
				eff_PuzzleGet_go.SetActive(value: false);
			}
			if (eff_BigPuzzleGet_go == null)
			{
				eff_BigPuzzleGet_go = Object.Instantiate(ResourceLoadManager.GetInstance().LoadObjByPath<GameObject>("UI/DailyPuzzle/Eff/pintu"));
				eff_BigPuzzleGet_go.transform.SetParent(other_eff_go.transform);
				(eff_BigPuzzleGet_go.transform as RectTransform).anchoredPosition = new Vector2(0f, 0f);
				eff_BigPuzzleGet_go.transform.localScale = Vector3.one;
				eff_BigPuzzleGet_go.SetActive(value: false);
			}
			if (eff_TuoWei_rectTran == null)
			{
				GameObject gameObject = Object.Instantiate(ResourceLoadManager.GetInstance().LoadObjByPath<GameObject>("UI/DailyPuzzle/Eff/pintutuowei"));
				gameObject.transform.SetParent(other_eff_go.transform);
				eff_TuoWei_rectTran = (gameObject.transform as RectTransform);
				eff_TuoWei_rectTran.anchoredPosition = new Vector2(227f, -370f);
				eff_TuoWei_rectTran.localScale = Vector3.one;
				gameObject.SetActive(value: false);
			}
		}
	}

	public void Init()
	{
		ResourceLoadManager.GetInstance().HideNetWaiting();
		OpenAnim();
		InitEff();
		coin_text.text = GETCOLD_NUM + " COINS";
		Image image = up_line_img;
		Color color = up_line_img.color;
		float r = color.r;
		Color color2 = up_line_img.color;
		float g = color2.g;
		Color color3 = up_line_img.color;
		image.color = new Color(r, g, color3.b, 1f);
		up_fly_img.rectTransform.sizeDelta = new Vector2(616f, 616f);
		up_fly_img.gameObject.SetActive(value: false);
		DailyPuzzleMonthDataItem dailyPuzzleMonthItem = PlayerInfo.Instance.GetDailyPuzzleMonthItem();
		cur_bigImg_name = "bg_puzzle_20" + dailyPuzzleMonthItem.ID;
		cur_bigImg_sp = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyPuzzle/" + cur_bigImg_name);
		up_fly_img.sprite = cur_bigImg_sp;
		int num = dailyPuzzleMonthItem.ID % 100;
		int starNum = dailyPuzzleMonthItem.StarNum;
		int textId = 1129 + num;
		Money_text.text = TextLibraryDataManagerCsv.instance.GetText(textId);
		int textId2 = 1141 + num;
		Mon_text.text = TextLibraryDataManagerCsv.instance.GetText(textId2);
		DailyPuzzleDataItem dailyPuzzleItem = PlayerInfo.Instance.GetDailyPuzzleItem();
		int num2 = dailyPuzzleItem.ID % 100;
		today_starNum = dailyPuzzleItem.Stars;
		flyStar_num = 0;
		if (dailyPuzzleMonthItem.PuzzleList == null)
		{
			dailyPuzzleMonthItem.PuzzleList = new List<int>();
		}
		if (NEW_PUZZLEIDX >= 0)
		{
			PlayerInfo_PuzzleList.Clear();
			for (int i = 0; i < dailyPuzzleMonthItem.PuzzleList.Count; i++)
			{
				if (dailyPuzzleMonthItem.PuzzleList[i] != NEW_PUZZLEIDX)
				{
					PlayerInfo_PuzzleList.Add(dailyPuzzleMonthItem.PuzzleList[i]);
				}
			}
		}
		else
		{
			for (int j = 0; j < dailyPuzzleMonthItem.PuzzleList.Count; j++)
			{
				PlayerInfo_PuzzleList.Add(dailyPuzzleMonthItem.PuzzleList[j]);
			}
		}
		bool flag = false;
		if (PlayerInfo_PuzzleList.Count >= 9)
		{
			flag = true;
		}
		if (flag)
		{
			for (int k = 0; k < FullPicHideGos.Length; k++)
			{
				FullPicHideGos[k].SetActive(value: false);
			}
		}
		CreatePuzzleItems();
		if (dailyPuzzleItem.IsFinished == 1)
		{
			gou_go.SetActive(value: true);
		}
		else
		{
			gou_go.SetActive(value: false);
		}
		Down_PlayBtn_go.SetActive(value: false);
		Down_NextBtn_go.SetActive(value: false);
		if (UI_OPENTYPE == 0)
		{
			bg_img.gameObject.SetActive(value: true);
			bg_img.sprite = WordJsonDataManager.instance.GetDailyPuzzleBg();
			for (int l = 0; l < stars_rectTran.Length; l++)
			{
				stars_rectTran[l].gameObject.SetActive(value: false);
				stars_rectTran[l].localScale = Vector3.one;
			}
			int num3 = starNum;
			SetJinDu(num3, isCheckZero: true);
			Down_PlayBtn_go.SetActive(value: true);
			DownDay_rectTran.anchoredPosition = new Vector2(0f, -516f);
		}
		else if (UI_OPENTYPE == 1)
		{
			bg_img.gameObject.SetActive(value: false);
			anim_end_num = starNum;
			anim_cur_num = starNum - today_starNum;
			SetJinDu(anim_cur_num, isCheckZero: true);
			Down_NextBtn_go.SetActive(value: true);
			DownDay_rectTran.anchoredPosition = new Vector2(0f, -900f);
			for (int m = 0; m < stars_rectTran.Length; m++)
			{
				stars_rectTran[m].gameObject.SetActive(value: false);
			}
			for (int n = 0; n < today_starNum && n < stars_rectTran.Length; n++)
			{
				stars_rectTran[n].gameObject.SetActive(value: true);
			}
			if (!dailyPuzzleMonthItem.isGotReward && dailyPuzzleMonthItem.PuzzleList.Count >= 9)
			{
				gold_script._isAddGold = true;
				PlayerInfo.Instance.AddGold(GETCOLD_NUM, 4);
				PlayerInfo.Instance.CHangeDailyMonthItemReward(isGot: true);
			}
			if (today_starNum == 0)
			{
				EndAnim();
			}
			else
			{
				SetAnimIndex(0);
			}
			if (NEW_PUZZLEIDX >= 0)
			{
				SetGetPuzzleIdx(NEW_PUZZLEIDX);
			}
		}
		Day_text.text = num2.ToString();
		btnCalendar_num_text.text = num2.ToString();
	}

	public void GetOnePuzzle()
	{
		List<int> list = new List<int>();
		for (int i = 0; i < 9; i++)
		{
			bool flag = false;
			for (int j = 0; j < PlayerInfo_PuzzleList.Count; j++)
			{
				if (PlayerInfo_PuzzleList[j] == i)
				{
					flag = true;
				}
			}
			if (!flag)
			{
				list.Add(i);
			}
		}
		if (list.Count > 0)
		{
			int index = Random.Range(0, list.Count);
			SetGetPuzzleIdx(list[index]);
		}
		else
		{
			WordGlobal.DebugLog("图片拼图收集齐了~！");
		}
	}

	public void SetGetPuzzleIdx(int get_idx)
	{
		WordGlobal.DebugLog("获取新碎片IDX:" + get_idx);
		thisOnePuzzle_Idx = get_idx;
		if (!PlayerInfo_PuzzleList.Contains(thisOnePuzzle_Idx))
		{
			PlayerInfo_PuzzleList.Add(thisOnePuzzle_Idx);
		}
		if (PlayerInfo_PuzzleList.Count >= 9)
		{
			isSucCollPic = true;
		}
	}

	public void SetAnimIndex(int idx)
	{
		anim_index = idx;
		anim_waitTime = 0f;
	}

	public void EndAnim()
	{
		anim_index = -1;
		DownDay_rectTran.DOAnchorPosY(-516f, 0.5f);
		if (PlayerInfo.Instance.ShowTutorialDailyPuzzleFinish == 0)
		{
			PlayerInfo.Instance.ShowTutorialDailyPuzzleFinish = 1;
			UIManager.Instance.LoadMainUI("UI/Main/TutorialDailyPuzzleFinish", isPop: true);
		}
	}

	public void CreatePuzzleItems()
	{
		puzzle_items.Clear();
		for (int i = 0; i < 9; i++)
		{
			GameObject gameObject = Object.Instantiate(puzzle_item_go);
			gameObject.transform.SetParent(puzzle_itemsParent, worldPositionStays: false);
			DPCollectPicPuzzleItem component = gameObject.GetComponent<DPCollectPicPuzzleItem>();
			component.Init(i, this);
			for (int j = 0; j < PlayerInfo_PuzzleList.Count; j++)
			{
				if (PlayerInfo_PuzzleList[j] == i)
				{
					component.Show(isShow: true);
					break;
				}
			}
			puzzle_items.Add(component);
		}
	}

	public void SetJinDu(int num, bool isCheckZero = false)
	{
		int num2 = 0;
		for (int i = 0; i < openPuzzleStarNum.Length; i++)
		{
			num2 = i;
			if (num > openPuzzleStarNum[i])
			{
				num -= openPuzzleStarNum[i];
				continue;
			}
			break;
		}
		if (isCheckZero && num == openPuzzleStarNum[num2])
		{
			num2++;
			if (num2 < openPuzzleStarNum.Length)
			{
				num = 0;
			}
			else
			{
				num2--;
			}
		}
		jindu_text.text = num + "/" + openPuzzleStarNum[num2];
		jindu_up_img.fillAmount = (float)num / (float)openPuzzleStarNum[num2];
	}

	private void Update()
	{
		if (isTest)
		{
			gold_script.CollectGold(gold_flyTran.position, 5, 1f, null);
			gold_script.AddGold(GETCOLD_NUM, 1f, 2f, isAdd2PlayerInfo: false, 2);
			isTest = false;
		}
		if (UI_OPENTYPE == 2)
		{
			yujiaz_waitTime += Time.deltaTime;
			if (yujiaz_waitTime > 0.5f)
			{
				isCloseMainUI = false;
				BtnClose();
			}
		}
		if (isCloseUI)
		{
			BtnClose();
			isCloseUI = false;
		}
		if (isOutMidFinish)
		{
			UpdateAnim();
		}
	}

	public void UpdateAnim()
	{
		if (UI_OPENTYPE != 1)
		{
			return;
		}
		if (anim_index == 0)
		{
			anim_waitTime += Time.deltaTime;
			if (anim_waitTime > 0.5f)
			{
				SetAnimIndex(1);
			}
		}
		else if (anim_index == 1)
		{
			bool flag = false;
			if (jindu_up_img.fillAmount >= 0.98f)
			{
				flag = true;
			}
			if (flag)
			{
				if (thisOnePuzzle_Idx >= 0)
				{
					SetAnimIndex(100);
					WordGlobal.DebugLog("满星星飞碎片");
				}
				else
				{
					WordGlobal.DebugLog("判断满星了，但是没有获得拼图，需要查一下");
					EndAnim();
				}
				return;
			}
			flyStar_num++;
			if (flyStar_num > today_starNum)
			{
				EndAnim();
				return;
			}
			float duration = 0.4f;
			Vector3[] path = WordGlobal.CountCirclePoints(stars_rectTran[flyStar_num - 1].position, jindu_str_tran.position);
			stars_rectTran[flyStar_num - 1].transform.DOPath(path, duration).OnComplete(delegate
			{
				stars_rectTran[flyStar_num - 1].gameObject.SetActive(value: false);
				SetAnimIndex(3);
			});
			SetAnimIndex(2);
		}
		else
		{
			if (anim_index == 2)
			{
				return;
			}
			if (anim_index == 3)
			{
				anim_cur_num++;
				if (anim_cur_num <= anim_end_num)
				{
					SetJinDu(anim_cur_num);
				}
				if (jindu_up_img.fillAmount < 0.98f)
				{
					jindu_str_tran.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.2f).OnComplete(delegate
					{
						jindu_str_tran.DOScale(Vector3.one, 0.2f);
					});
				}
				SetAnimIndex(0);
			}
			else if (anim_index == 100)
			{
				jindu_tran.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.2f).OnComplete(delegate
				{
					jindu_tran.DOScale(Vector3.one, 0.2f).OnComplete(delegate
					{
						eff_TuoWei_rectTran.anchoredPosition = new Vector2(227f, -370f);
						SetAnimIndex(102);
					});
				});
				SetAnimIndex(101);
			}
			else
			{
				if (anim_index == 101)
				{
					return;
				}
				if (anim_index == 102)
				{
					eff_TuoWei_rectTran.gameObject.SetActive(value: true);
					float duration2 = 1f;
					Vector3[] path2 = WordGlobal.CountCirclePoints(eff_TuoWei_rectTran.position, Pos0.position);
					eff_TuoWei_rectTran.transform.DOPath(path2, duration2).OnComplete(delegate
					{
						eff_PuzzleGet_go.SetActive(value: false);
						eff_PuzzleGet_go.SetActive(value: true);
						eff_TuoWei_rectTran.gameObject.SetActive(value: false);
						SetAnimIndex(104);
					});
					SetAnimIndex(103);
				}
				else
				{
					if (anim_index == 103)
					{
						return;
					}
					if (anim_index == 104)
					{
						puzzle_items[thisOnePuzzle_Idx].Show(isShow: true);
						puzzle_items[thisOnePuzzle_Idx].transform.SetParent(puzzleTempParent_tran);
						puzzle_items[thisOnePuzzle_Idx].rectTran.anchoredPosition = Vector2.zero;
						puzzle_items[thisOnePuzzle_Idx].rectTran.localScale = Vector2.zero;
						puzzle_items[thisOnePuzzle_Idx].rectTran.DOAnchorPos(puzzle_items[thisOnePuzzle_Idx].my_centerPos * 1.2f, 0.5f).SetEase(Ease.OutBounce);
						Vector3 endValue = Vector3.one * 1.2f;
						puzzle_items[thisOnePuzzle_Idx].rectTran.DOScale(endValue, 0.5f).SetEase(Ease.OutBounce).OnComplete(delegate
						{
							eff_PuzzleGet_go.SetActive(value: false);
							puzzle_items[thisOnePuzzle_Idx].rectTran.DOAnchorPos(Vector2.zero, 0.5f).OnComplete(delegate
							{
								if (eff_PuzzleShow_gos[thisOnePuzzle_Idx] == null)
								{
									eff_PuzzleShow_gos[thisOnePuzzle_Idx] = Object.Instantiate(ResourceLoadManager.GetInstance().LoadObjByPath<GameObject>("UI/DailyPuzzle/Eff/pintuxiao" + (thisOnePuzzle_Idx + 1)));
									eff_PuzzleShow_gos[thisOnePuzzle_Idx].transform.SetParent(other_eff_go.transform);
									(eff_PuzzleShow_gos[thisOnePuzzle_Idx].transform as RectTransform).anchoredPosition = other_eff_pos[thisOnePuzzle_Idx];
									eff_PuzzleShow_gos[thisOnePuzzle_Idx].transform.localScale = Vector3.one;
								}
								eff_PuzzleShow_gos[thisOnePuzzle_Idx].SetActive(value: true);
								puzzle_items[thisOnePuzzle_Idx].rectTran.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutElastic).OnComplete(delegate
								{
									puzzle_items[thisOnePuzzle_Idx].transform.SetParent(puzzle_itemsParent);
									SetAnimIndex(106);
								});
							});
						});
						SetAnimIndex(105);
					}
					else
					{
						if (anim_index == 105)
						{
							return;
						}
						if (anim_index == 106)
						{
							if (isSucCollPic)
							{
								SetAnimIndex(200);
							}
							else
							{
								SetAnimIndex(1);
							}
							SetJinDu(anim_cur_num, isCheckZero: true);
						}
						else if (anim_index == 200)
						{
							float duration3 = 1f;
							up_line_img.DOFade(0f, duration3).OnComplete(delegate
							{
								gold_script.AddGold(GETCOLD_NUM, 1f, 0.25f, isAdd2PlayerInfo: false, 3);
								eff_BigPuzzleGet_go.SetActive(value: true);
								if (eff_dajiang_go == null)
								{
									eff_dajiang_go = Object.Instantiate(ResourceLoadManager.GetInstance().LoadObjByPath<GameObject>("UI/DailyPuzzle/Eff/pintudajiang"));
									eff_dajiang_go.transform.SetParent(eff_go.transform);
									(eff_dajiang_go.transform as RectTransform).anchoredPosition = new Vector2(0f, 0f);
									eff_dajiang_go.transform.localScale = Vector3.one;
									eff_dajiang_go.SetActive(value: true);
								}
								eff_go.SetActive(value: true);
							});
							SetAnimIndex(201);
						}
						else if (anim_index == 201)
						{
							anim_waitTime += Time.deltaTime;
							if (anim_waitTime > 2f)
							{
								SetAnimIndex(202);
							}
						}
						else if (anim_index == 202)
						{
							float duration4 = 1f;
							up_fly_img.gameObject.SetActive(value: true);
							up_fly_img.rectTransform.DOSizeDelta(Vector2.zero, duration4);
							Vector3[] path3 = WordGlobal.CountCirclePoints(up_fly_img.transform.position, btnCol_tran.position);
							up_fly_img.transform.DOPath(path3, duration4).OnComplete(delegate
							{
								EndAnim();
								up_fly_img.gameObject.SetActive(value: false);
							});
							SetAnimIndex(203);
						}
						else if (anim_index != 203)
						{
						}
					}
				}
			}
		}
	}

	public void BtnClose()
	{
		if ((bool)MainUIScript.Instance && isCloseMainUI)
		{
			MainUIScript.Instance.BtnCloseClick();
		}
		Object.Destroy(base.gameObject);
	}

	public void BtnSettingClick()
	{
		if (GameObject.Find("SettingUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/SettingUI", isPop: true);
		}
	}

	public void BtnCollectClick()
	{
		if (GameObject.Find("DPCollectAllPicsUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCollectAllPicsUI", isPop: true);
		}
	}

	public void BtnPlayClick()
	{
		if (UI_OPENTYPE == 0)
		{
			if (gou_go.activeInHierarchy)
			{
				ResourceLoadManager.GetInstance().ShowTips(1154);
				return;
			}
			isCloseMainUI = false;
			MainUIScript.Instance.Init();
			Object.Destroy(base.gameObject);
		}
		else if (UI_OPENTYPE == 1)
		{
			if (PlayerInfo.Instance.AdRemove != 1)
			{
				AdsManager.Instance.ShowInterstitialAd("Pop_ForceAd_Dailypuzzle");
			}
			UIManager.CurMainUIType = UIManager.MainUIType.Type_Common;
			if (MainUIScript.Instance != null)
			{
				PlayerInfo.Instance.CurrentLevel = PlayerInfo.Instance.MaxPlayLevel;
				MainUIScript.Instance.Init();
				isCloseMainUI = false;
			}
			else if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.ShowMainUI(PlayerInfo.Instance.MaxPlayLevel);
			}
			BtnClose();
		}
	}

	public void BtnCalendarClick()
	{
		if (GameObject.Find("DPCCalendarUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCCalendarUI", isPop: true);
		}
	}

	public void BtnAddGoldClick()
	{
		if (GameObject.Find("ShopUI") == null)
		{
			if (Application.internetReachability == NetworkReachability.NotReachable)
			{
				ResourceLoadManager.GetInstance().ShowTips(1118);
			}
			else
			{
				UIManager.Instance.LoadMainUI("UI/ShopUI").name = "ShopUI";
			}
		}
	}

	private void OnDestroy()
	{
		isShow = false;
		NEW_PUZZLEIDX = -1;
	}
}
