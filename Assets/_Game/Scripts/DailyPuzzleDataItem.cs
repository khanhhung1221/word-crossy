using System;

[Serializable]
public class DailyPuzzleDataItem
{
	public int ID;

	public int Stars;

	public int IsFinished;

	public DailyPuzzleDataItem(string[] values)
	{
		int num = 0;
		Stars = int.Parse(values[num++]);
		IsFinished = int.Parse(values[num++]);
	}

	public DailyPuzzleDataItem()
	{
		Stars = 0;
		IsFinished = 0;
	}
}
