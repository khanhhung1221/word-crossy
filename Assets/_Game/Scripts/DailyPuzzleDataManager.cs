using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class DailyPuzzleDataManager : MonoBehaviour
{
	private static DailyPuzzleDataManager inst;

	public Dictionary<int, WordListItem> ItemList;

	private bool inited;

	private int skipNumber = 2;

	public static DailyPuzzleDataManager instance => inst ?? (inst = new DailyPuzzleDataManager());

	public DailyPuzzleDataManager()
	{
		byte[] file = File.ReadAllBytes(Application.dataPath + "/_Game/Word/dailypuzzle.csv");
		Init(file);
	}

	public DailyPuzzleDataManager(bool abc)
	{
		byte[] file = File.ReadAllBytes(Application.dataPath + "/_Game/Word/dailypuzzle.csv");
		Init(file);
	}

	public void Init(byte[] file, string source_encoding = "UTF-8")
	{
		if (!inited)
		{
			MemoryStream stream = new MemoryStream(file);
			ItemList = new Dictionary<int, WordListItem>();
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								WordListItem wordListItem = new WordListItem(array, isDaily: true);
								ItemList.Add(wordListItem.Level, wordListItem);
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public WordListItem GetItemByLevel(int level)
	{
		if (ItemList.ContainsKey(level))
		{
			return ItemList[level];
		}
		return new WordListItem();
	}
}
