using System;
using System.Collections.Generic;

[Serializable]
public class DailyPuzzleGetImageData
{
	public List<int> ImageList;

	public int Tutorial;
}
