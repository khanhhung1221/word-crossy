using System;
using System.Collections.Generic;

[Serializable]
public class DailyPuzzleMonthDataItem
{
	public int ID;

	public int StarNum;

	public List<int> PuzzleList;

	public bool isGotReward;

	public DailyPuzzleMonthDataItem(string[] values)
	{
		int num = 0;
		ID = int.Parse(values[num++]);
		StarNum = int.Parse(values[num++]);
		string text = values[num++];
		string[] array = text.Split(',');
		PuzzleList = new List<int>();
		for (int i = 0; i < array.Length; i++)
		{
			PuzzleList.Add(int.Parse(array[i]));
		}
	}

	public DailyPuzzleMonthDataItem()
	{
		PuzzleList = new List<int>();
	}
}
