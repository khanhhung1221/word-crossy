using System;

[Serializable]
public class DailyPuzzleTimeMarkData
{
	public string VersionCode;

	public int DailyPuzzleLevel;
}
