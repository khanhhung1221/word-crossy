using System;

[Serializable]
public class DailyQuestDataCsv
{
	public int ID;

	public string RequireStr;

	public int RequireType;

	public int RequireNum;

	public int DesID;

	public int PayType;

	public int Rate;

	public int RewardNum;

	public int PayToFinish;

	public int require1;

	public int reward1;

	public int require2;

	public int reward2;

	public int QuestNum;

	public DailyQuestDataCsv(string[] values, int valueIndex)
	{
		int num = 0;
		QuestNum = 1;
		ID = int.Parse(values[num++]);
		RequireStr = values[num++];
		DesID = int.Parse(values[num++]);
		PayType = int.Parse(values[num++]);
		Rate = int.Parse(values[num++]);
		RewardNum = int.Parse(values[num++]);
		PayToFinish = int.Parse(values[num++]);
		require1 = int.Parse(values[num++]);
		reward1 = int.Parse(values[num++]);
		require2 = int.Parse(values[num++]);
		reward2 = int.Parse(values[num++]);
		if (require1 > 0)
		{
			QuestNum++;
		}
		if (require2 > 0)
		{
			QuestNum++;
		}
	}

	public DailyQuestDataCsv()
	{
		ID = 0;
	}

	public int GetExclusiveKey()
	{
		string[] array = RequireStr.Split('_');
		return int.Parse(array[0].Substring(0, 1));
	}

	public int GetQuestRequireType()
	{
		string[] array = RequireStr.Split('_');
		RequireType = int.Parse(array[0]);
		return RequireType;
	}

	public int GetQuestRequireNum()
	{
		string[] array = RequireStr.Split('_');
		RequireNum = int.Parse(array[1]);
		return RequireNum;
	}
}
