using System;
using System.Collections.Generic;
using UnityEngine;

public class DailyQuestDataManager
{
	private static DailyQuestDataManager _instance;

	private Dictionary<int, DailyQuestDataCsv> NoPayCommonList;

	private Dictionary<int, DailyQuestDataCsv> NoPayRandomList;

	private Dictionary<int, DailyQuestDataCsv> PayCommonList;

	private Dictionary<int, DailyQuestDataCsv> PayRandomList;

	private System.Random random = new System.Random();

	public DailyQuestDataManager()
	{
		NoPayCommonList = new Dictionary<int, DailyQuestDataCsv>();
		NoPayRandomList = new Dictionary<int, DailyQuestDataCsv>();
		PayCommonList = new Dictionary<int, DailyQuestDataCsv>();
		PayRandomList = new Dictionary<int, DailyQuestDataCsv>();
		Dictionary<int, DailyQuestDataCsv> list = DailyQuestDataManagerCsv.instance.GetList();
		for (int i = 1; i <= list.Count; i++)
		{
			DailyQuestDataCsv dailyQuestDataCsv = DailyQuestDataManagerCsv.instance.GetList()[i];
			if (dailyQuestDataCsv.PayType == 0 && dailyQuestDataCsv.Rate == 100)
			{
				NoPayCommonList.Add(i, dailyQuestDataCsv);
			}
			else if (dailyQuestDataCsv.PayType == 0 && dailyQuestDataCsv.Rate != 100)
			{
				NoPayRandomList.Add(i, dailyQuestDataCsv);
			}
			else if (dailyQuestDataCsv.PayType == 1 && dailyQuestDataCsv.Rate == 100)
			{
				PayCommonList.Add(i, dailyQuestDataCsv);
			}
			else if (dailyQuestDataCsv.PayType == 1 && dailyQuestDataCsv.Rate != 100)
			{
				PayRandomList.Add(i, dailyQuestDataCsv);
			}
		}
	}

	public bool IsExclusive(int key, Dictionary<int, DailyQuestDataCsv> contrastlist)
	{
		foreach (KeyValuePair<int, DailyQuestDataCsv> item in contrastlist)
		{
			if (key == item.Value.GetExclusiveKey())
			{
				return true;
			}
		}
		return false;
	}

	private int GetRandomItem(Dictionary<int, DailyQuestDataCsv> rdList, Dictionary<int, DailyQuestDataCsv> contrastlist)
	{
		Dictionary<int, DailyQuestDataCsv> dictionary = new Dictionary<int, DailyQuestDataCsv>();
		foreach (KeyValuePair<int, DailyQuestDataCsv> rd in rdList)
		{
			bool flag = true;
			if (rd.Value.Rate == 0)
			{
				flag = false;
			}
			if (rd.Value.GetQuestRequireType() == 10)
			{
				if (PlayerInfo.Instance.MaxPlayLevel >= 127)
				{
					flag = false;
				}
			}
			else if (rd.Value.GetQuestRequireType() == 30)
			{
				if (PlayerInfo.Instance.LoggedFaceBook == 1)
				{
					flag = false;
				}
			}
			else if (rd.Value.GetQuestRequireType() == 50)
			{
				flag = isNeedDailyPuzzle();
			}
			else if (rd.Value.GetQuestRequireType() == 60)
			{
				flag = MapDataManager.instance.isNeedQuest60(PlayerInfo.Instance.MaxPlayLevel);
			}
			else if (rd.Value.GetQuestRequireType() == 80)
			{
				if (PlayerInfo.Instance.MaxPlayLevel < 127)
				{
					flag = false;
				}
			}
			else if (rd.Value.GetQuestRequireType() == 90)
			{
				flag = isNeedDailySearch();
			}
			if (rd.Value.GetExclusiveKey() == 4)
			{
				if (rd.Value.GetQuestRequireType() == 40)
				{
					if (PlayerInfo.Instance.MaxPlayLevel > 40)
					{
						flag = false;
					}
				}
				else if (rd.Value.GetQuestRequireType() == 41)
				{
					if (PlayerInfo.Instance.MaxPlayLevel < 40 || PlayerInfo.Instance.MaxPlayLevel > 180)
					{
						flag = false;
					}
				}
				else if (rd.Value.GetQuestRequireType() == 42 && PlayerInfo.Instance.MaxPlayLevel <= 180)
				{
					flag = false;
				}
			}
			if (IsExclusive(rd.Value.GetExclusiveKey(), contrastlist))
			{
				flag = false;
			}
			if (flag)
			{
				dictionary.Add(rd.Key, rd.Value);
			}
		}
		int num = 0;
		foreach (KeyValuePair<int, DailyQuestDataCsv> item in dictionary)
		{
			num += item.Value.Rate;
		}
		int num2 = UnityEngine.Random.Range(0, num);
		DailyQuestDataCsv dailyQuestDataCsv = null;
		foreach (KeyValuePair<int, DailyQuestDataCsv> item2 in dictionary)
		{
			if (num2 - item2.Value.Rate < 0)
			{
				dailyQuestDataCsv = item2.Value;
				break;
			}
			num2 -= item2.Value.Rate;
		}
		return dailyQuestDataCsv?.ID ?? 0;
	}

	private int GetRealRandomItem(Dictionary<int, DailyQuestDataCsv> rdList, Dictionary<int, DailyQuestDataCsv> contrastlist)
	{
		int num = 0;
		int num2 = 0;
		while (num == 0)
		{
			num2++;
			if (num2 >= 200)
			{
				Debug.Log("DailyQuestDataManager 随机任务超过200次，可能是随机不到任务，需要检查此处代码");
				return 0;
			}
			num = GetRandomItem(rdList, contrastlist);
			if (num != 0)
			{
				return num;
			}
		}
		return 0;
	}

	public Dictionary<int, DailyQuestDataCsv> GetCurrentNoPayCommonList()
	{
		Dictionary<int, DailyQuestDataCsv> dictionary = new Dictionary<int, DailyQuestDataCsv>();
		foreach (KeyValuePair<int, DailyQuestDataCsv> noPayCommon in NoPayCommonList)
		{
			dictionary.Add(noPayCommon.Key, noPayCommon.Value);
		}
		if (PlayerInfo.Instance.LoggedFaceBook == 1)
		{
			dictionary.Remove(1);
		}
		if (!isNeedDailyPuzzle())
		{
			dictionary.Remove(3);
		}
		if (!isNeedDailySearch())
		{
			dictionary.Remove(26);
		}
		return dictionary;
	}

	public bool isNeedDailySearch()
	{
		if (PlayerInfo.Instance.MaxPlayLevel <= WordGlobal.OpenDailySearchLevel)
		{
			return false;
		}
		return true;
	}

	public bool isNeedDailyPuzzle()
	{
		if (!WordJsonDataManager.instance.IsOpenDailyPuzzle() || PlayerInfo.Instance.MaxPlayLevel <= WordGlobal.OpenDailyPuzzleLevel || PlayerInfo.Instance.GetDailyDayId() < WordJsonDataManager.instance.GetDailyMinLevel() || PlayerInfo.Instance.GetDailyDayId() > WordJsonDataManager.instance.GetDailyMaxLevel())
		{
			return false;
		}
		return true;
	}

	public Dictionary<int, DailyQuestDataCsv> GetNoPayList()
	{
		Dictionary<int, DailyQuestDataCsv> dictionary = new Dictionary<int, DailyQuestDataCsv>();
		dictionary = GetCurrentNoPayCommonList();
		int count = dictionary.Count;
		int num = 4 - count;
		for (int i = 0; i < num; i++)
		{
			int realRandomItem = GetRealRandomItem(NoPayRandomList, dictionary);
			if (realRandomItem != 0)
			{
				dictionary.Add(realRandomItem, DailyQuestDataManagerCsv.instance.GetItemByID(realRandomItem));
			}
		}
		return dictionary;
	}

	public Dictionary<int, DailyQuestDataCsv> GetCurrentPayCommonList()
	{
		Dictionary<int, DailyQuestDataCsv> dictionary = new Dictionary<int, DailyQuestDataCsv>();
		foreach (KeyValuePair<int, DailyQuestDataCsv> payCommon in PayCommonList)
		{
			dictionary.Add(payCommon.Key, payCommon.Value);
		}
		if (PlayerInfo.Instance.LoggedFaceBook == 1)
		{
			dictionary.Remove(14);
		}
		if (!isNeedDailyPuzzle())
		{
			dictionary.Remove(15);
		}
		if (!isNeedDailySearch())
		{
			dictionary.Remove(27);
		}
		return dictionary;
	}

	public Dictionary<int, DailyQuestDataCsv> GetPayList()
	{
		Dictionary<int, DailyQuestDataCsv> dictionary = new Dictionary<int, DailyQuestDataCsv>();
		dictionary = GetCurrentPayCommonList();
		int count = dictionary.Count;
		int num = 4 - count;
		for (int i = 0; i < num; i++)
		{
			int realRandomItem = GetRealRandomItem(PayRandomList, dictionary);
			if (realRandomItem != 0)
			{
				dictionary.Add(realRandomItem, DailyQuestDataManagerCsv.instance.GetItemByID(realRandomItem));
			}
		}
		return dictionary;
	}

	public static DailyQuestDataManager GetInstance()
	{
		if (_instance == null)
		{
			_instance = new DailyQuestDataManager();
		}
		return _instance;
	}
}
