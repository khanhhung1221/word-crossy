using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class DailyQuestDataManagerCsv : ManagerCsv
{
	private static DailyQuestDataManagerCsv inst;

	private Dictionary<int, DailyQuestDataCsv> DQDataList;

	public static DailyQuestDataManagerCsv instance => inst ?? (inst = new DailyQuestDataManagerCsv());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			int valueIndex = 0;
			MemoryStream stream = new MemoryStream(file);
			DQDataList = new Dictionary<int, DailyQuestDataCsv>();
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								DailyQuestDataCsv dailyQuestDataCsv = new DailyQuestDataCsv(array, valueIndex);
								DQDataList[dailyQuestDataCsv.ID] = dailyQuestDataCsv;
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public DailyQuestDataCsv GetItemByID(int ID)
	{
		if (DQDataList != null && DQDataList.ContainsKey(ID))
		{
			return DQDataList[ID];
		}
		return null;
	}

	public static void Renew()
	{
		inst = new DailyQuestDataManagerCsv();
	}

	public Dictionary<int, DailyQuestDataCsv> GetList()
	{
		return DQDataList;
	}
}
