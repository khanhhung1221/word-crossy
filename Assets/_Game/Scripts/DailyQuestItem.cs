using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DailyQuestItem : MonoBehaviour
{
	public Image QuestIcon;

	public Text QuestText;

	public GameObject GoBtn;

	public GameObject ClaimBtn;

	public GameObject AlreadyGetIcon;

	public Image Progressbar;

	public Text BarNumText;

	public int state;

	private DailyQuestDataCsv DQDC;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void SetData(DailyQuestDataCsv dc, int s)
	{
		DQDC = dc;
		state = s;
		switch (state)
		{
		case 0:
			GoBtn.SetActive(value: true);
			break;
		case 1:
			ClaimBtn.SetActive(value: true);
			break;
		case 2:
			AlreadyGetIcon.SetActive(value: true);
			break;
		}
		int num = dc.GetQuestRequireType();
		if (num == 41 || num == 42)
		{
			num = 40;
		}
		QuestIcon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyQuest/daily_quest_icon" + num);
		DailyQuestSaveData dataByID = PlayerInfo.Instance.GetDailyQuestSaveData().GetDataByID(dc.ID);
		int questRequireNum = dc.GetQuestRequireNum();
		if (dataByID != null)
		{
			questRequireNum = dataByID.GetQuestRequireNum();
		}
		if (dc.GetQuestRequireType() == 30)
		{
			QuestText.text = WordGlobal.ReadText(dc.DesID);
		}
		else
		{
			QuestText.text = string.Format(WordGlobal.ReadText(dc.DesID), questRequireNum);
		}
		switch (dc.ID)
		{
		case 1:
			if (PlayerInfo.Instance.LoggedFaceBook == 1)
			{
				Progressbar.fillAmount = GetAmountFloat(1, 1);
				BarNumText.text = 1 + "/" + 1;
			}
			else
			{
				Progressbar.fillAmount = GetAmountFloat(0, 1);
				BarNumText.text = 0 + "/" + 1;
			}
			break;
		case 2:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().WatchAD_num);
			break;
		case 3:
		case 15:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().CollectDPStar_num);
			break;
		case 4:
		case 16:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().PassLevel_num);
			break;
		case 5:
		case 17:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().CollectScore_num);
			break;
		case 6:
		case 18:
		case 28:
		case 29:
		case 30:
		case 31:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().CollectBonus_num);
			break;
		case 7:
		case 19:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().UnlockChapter_num);
			break;
		case 8:
		case 20:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().Good_num);
			break;
		case 9:
		case 21:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().Great_num);
			break;
		case 10:
		case 22:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().Excellent_num);
			break;
		case 11:
		case 23:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().Amazing_num);
			break;
		case 12:
		case 24:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().Unbelievable_num);
			break;
		case 13:
		case 25:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().Outstanding_num);
			break;
		case 14:
			if (PlayerInfo.Instance.LoggedFaceBook == 1)
			{
				Progressbar.fillAmount = GetAmountFloat(1, 1);
				BarNumText.text = 1 + "/" + 1;
			}
			else
			{
				Progressbar.fillAmount = GetAmountFloat(0, 1);
				BarNumText.text = 0 + "/" + 1;
			}
			break;
		case 26:
		case 27:
			SetTextAndBar(PlayerInfo.Instance.GetDailyQuestSaveData().WordSearch_num);
			break;
		}
	}

	private void SetTextAndBar(int currentNum)
	{
		DailyQuestSaveData dataByID = PlayerInfo.Instance.GetDailyQuestSaveData().GetDataByID(DQDC.ID);
		int questRequireNum = DQDC.GetQuestRequireNum();
		if (dataByID != null)
		{
			questRequireNum = dataByID.GetQuestRequireNum();
		}
		Progressbar.fillAmount = GetAmountFloat(currentNum, questRequireNum);
		BarNumText.text = ((currentNum < questRequireNum) ? currentNum : questRequireNum) + "/" + questRequireNum;
	}

	private float GetAmountFloat(int numerator, int denominator)
	{
		return (float)numerator / (float)denominator;
	}

	public void BtnGoClick()
	{
		GameObject obj = GameObject.Find("DailyQuestUI");
		UnityEngine.Object.Destroy(obj);
		switch (DQDC.GetQuestRequireType())
		{
		case 30:
			if (GameObject.Find("FaceBookLoginUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/FaceBookLoginUI", isPop: true);
			}
			break;
		case 50:
			BtnPuzzleGoClick();
			break;
		case 20:
			BtnWatchVideoClick();
			break;
		case 90:
			BtnWordSearchGoClick();
			break;
		case 10:
		case 40:
		case 60:
		case 70:
		case 71:
		case 72:
		case 73:
		case 74:
		case 75:
		case 80:
			if (GameObject.Find("MainUI") == null)
			{
				LevelMapUIScript.Instance.ShowMainUI(PlayerInfo.Instance.MaxPlayLevel);
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
			{
				UIManager.CurMainUIType = UIManager.MainUIType.Type_Common;
				PlayerInfo.Instance.CurrentLevel = PlayerInfo.Instance.MaxPlayLevel;
				MainUIScript.Instance.Init();
			}
			else if (PlayerInfo.Instance.CurrentLevel != PlayerInfo.Instance.MaxPlayLevel)
			{
				PlayerInfo.Instance.CurrentLevel = PlayerInfo.Instance.MaxPlayLevel;
				MainUIScript.Instance.Init();
			}
			break;
		}
	}

	public void BtnClaimClick()
	{
		GameObject obj = GameObject.Find("DailyQuestUI");
		UnityEngine.Object.Destroy(obj);
		List<DailyQuestSaveData> list = new List<DailyQuestSaveData>();
		list = PlayerInfo.Instance.GetDailyQuestSaveData().Getlist(11);
		DailyQuestSaveData dailyQuestSaveData = null;
		for (int i = 0; i < list.Count; i++)
		{
			if (list[i].ID == DQDC.ID)
			{
				dailyQuestSaveData = list[i];
				dailyQuestSaveData.SetState(2);
			}
		}
		if (dailyQuestSaveData != null)
		{
			Debug.Log("DQDC.ID" + DQDC.ID + "  QuestText.text:" + QuestText.text);
			DailyQuestRewardUIScript.QuestID = dailyQuestSaveData.ID;
			GameObject gameObject = UIManager.Instance.LoadMainUI("UI/DailyQuestRewardUI", isPop: true);
			DailyQuestRewardUIScript component = gameObject.GetComponent<DailyQuestRewardUIScript>();
			Debug.Log("dqsd.ID" + dailyQuestSaveData.ID + " dqsd.GetQuestRewartNum():" + dailyQuestSaveData.GetQuestRewartNum());
			component.SetData(dailyQuestSaveData.GetQuestRewartNum());
		}
		PlayerInfo.Instance.SetDailyQuestSaveData();
		WordFaceBook.Instance.SendDailyQuestInfo();
	}

	public void BtnPuzzleGoClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
		if ((bool)MainUIScript.Instance)
		{
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
			{
				return;
			}
			UnityEngine.Object.DestroyImmediate(MainUIScript.Instance.gameObject);
		}
		WordGlobal.LogEvent("Dailypuzzle_enter");
		if (PlayerInfo.Instance.GetDailyDayId() >= WordJsonDataManager.instance.GetDailyMinLevel() && PlayerInfo.Instance.GetDailyDayId() <= WordJsonDataManager.instance.GetDailyMaxLevel())
		{
			if (PlayerInfo.Instance.GetDailyPuzzleItem().IsFinished == 1)
			{
				DPCollectPicUIScript.UI_OPENTYPE = 0;
				UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCollectPicUI").name = "DPCollectPicUI";
				return;
			}
			if (PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 0)
			{
				if (MainUIScript.Instance != null)
				{
					MainUIScript.Instance.BtnCloseClick();
				}
				else
				{
					LevelMapUIScript.Instance.ShowUI();
				}
				return;
			}
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_EnterLevel);
			if (GameObject.Find("MainUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/MainUI", isPop: false, UIManager.MainUIType.Type_Daily);
				PlayerInfo.Instance.GetLittleTipsData().SetDailyPuzzleOpen();
			}
			LevelMapUIScript.Instance.HideLevelMapUI();
			DPCollectPicUIScript.UI_OPENTYPE = 2;
			UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCollectPicUI").name = "DPCollectPicUI";
		}
		else
		{
			ResourceLoadManager.GetInstance().ShowConfirmTipsUI(ConfirmUIScript.ConfirmType.Type_Update, 1155, 1156, GoToUpdateVersion);
		}
	}

	public void BtnWordSearchGoClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
		if ((bool)MainUIScript.Instance)
		{
			WordGlobal.LevelMapOpenShowUI = "WordSearch";
			BtnsCtrlUIScript.isShowPopOffer = false;
			UIManager.Instance.ClearPop();
			MainUIScript.Instance.BtnCloseClick();
		}
		else if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.ShowDailySearchUI("Dailysearch_enter");
		}
	}

	public void GoToUpdateVersion()
	{
		MS_Interface.Current.OnRate();
	}

	public void BtnWatchVideoClick()
	{
		if (AdsManager.Instance.ShowIncentivizedAd("DailyQuestItem"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEvent));
		}
	}

	public void WatchVideoEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEvent));
		if (ret)
		{
			CommonTipsScript.Instance.ShowUI(string.Format(WordGlobal.ReadText(1017), 25), 0, WordGlobal.ReadText(1016), delegate(object obj)
			{
				Vector3 starPos = (Vector3)obj;
				GoldScript goldScript = UnityEngine.Object.FindObjectOfType<GoldScript>();
				goldScript.CollectGold(starPos, 5, 1f, null);
				goldScript.AddGold(25, 1f, 1f, isAdd2PlayerInfo: true, 4);
			}, showClose: false);
		}
	}
}
