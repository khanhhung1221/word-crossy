using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DailyQuestRewardUIScript : MonoBehaviour
{
	public static int QuestID;

	public GameObject GoldTipsObj;

	public GameObject BtnOBj;

	public Text obj_des01;

	public Text obj_des02;

	public GameObject BtnNext;

	public Image BtnNext_img;

	public Text BtnNext_txt;

	public Color[] colors;

	public Text BtnGoldNumText;

	public Text TipsGoldNumText;

	private int GoldNum;

	private float show_NextBtnTime;

	public static DailyQuestRewardUIScript Instance;

	private string str1306;

	private string str1213;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		WordGlobal.LogEvent("DailyQuest_QuestComplete");
		Invoke("ShowGoldTipsObj", 2f);
		str1306 = WordGlobal.ReadText(1306);
		str1213 = WordGlobal.ReadText(1213);
		obj_des01.text = TextLibraryDataManagerCsv.instance.GetText(1122);
		obj_des02.text = TextLibraryDataManagerCsv.instance.GetText(1123);
		if (AdsManager.Instance.IncentivizedAdState)
		{
			WordGlobal.LogEvent("DailyQuest_ADshow");
			BtnOBj.SetActive(value: true);
			BtnNext.SetActive(value: true);
			BtnNext_txt.text = str1306;
			BtnNext_img.color = colors[0];
			(BtnNext.transform as RectTransform).anchoredPosition = new Vector2(0f, -449f);
		}
		else
		{
			BtnOBj.SetActive(value: false);
			BtnNext.SetActive(value: true);
			BtnNext_txt.text = str1213;
			BtnNext_img.color = colors[1];
			(BtnNext.transform as RectTransform).anchoredPosition = new Vector2(0f, -316f);
		}
	}

	private void BtnAni02(GameObject obj, TweenCallback action, float delayTime = 0f)
	{
		obj.transform.localScale = Vector3.zero;
		obj.SetActive(value: true);
		obj.transform.DOScale(1f, 0.2f).SetDelay(delayTime).OnComplete(action);
	}

	public void ShowGoldTipsObj()
	{
		GoldTipsObj.SetActive(value: true);
	}

	public void SetData(int goldNum)
	{
		GoldNum = goldNum;
		TipsGoldNumText.text = goldNum.ToString();
		BtnGoldNumText.text = (goldNum + 25).ToString();
	}

	private void Update()
	{
		if (AdsManager.Instance.IncentivizedAdState)
		{
			BtnOBj.SetActive(value: true);
			BtnNext_txt.text = str1306;
			BtnNext_img.color = colors[0];
			(BtnNext.transform as RectTransform).anchoredPosition = new Vector2(0f, -449f);
		}
		else
		{
			BtnOBj.SetActive(value: false);
			BtnNext_txt.text = str1213;
			BtnNext_img.color = colors[1];
			(BtnNext.transform as RectTransform).anchoredPosition = new Vector2(0f, -316f);
		}
	}

	public void BtnCloseClick()
	{
		WordGlobal.LogEvent("DailyQuest_Tapclose");
		UnityEngine.Object.Destroy(base.gameObject);
		Vector3 zero = Vector3.zero;
		GoldScript goldScript = UnityEngine.Object.FindObjectOfType<GoldScript>();
		goldScript.CollectGold(zero, 5, 1f, null);
		goldScript.AddGold(GoldNum, 1f, 1f, isAdd2PlayerInfo: true, 5);
		DailyQuestSaveData dataByID = PlayerInfo.Instance.GetDailyQuestSaveData().GetDataByID(QuestID);
		if (dataByID != null)
		{
			if (dataByID.IsHaveNext() && GameObject.Find("DailyQuestUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/DailyQuestUI", isPop: true);
			}
			QuestID = 0;
		}
	}

	public void BtnWatchClick()
	{
		if (AdsManager.Instance.ShowIncentivizedAd("DailyQuest_TapAD"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEvent));
		}
	}

	public void WatchVideoEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEvent));
		UnityEngine.Object.Destroy(base.gameObject);
		if (ret)
		{
			CommonTipsScript.Instance.ShowUI(string.Format(WordGlobal.ReadText(1112), GoldNum + 25), 0, WordGlobal.ReadText(1016), delegate(object obj)
			{
				Vector3 starPos = (Vector3)obj;
				GoldScript goldScript2 = UnityEngine.Object.FindObjectOfType<GoldScript>();
				goldScript2.CollectGold(starPos, 5, 1f, null);
				goldScript2.AddGold(GoldNum + 25, 1f, 1f, isAdd2PlayerInfo: true, 6);
			}, showClose: false);
			return;
		}
		Vector3 zero = Vector3.zero;
		GoldScript goldScript = UnityEngine.Object.FindObjectOfType<GoldScript>();
		goldScript.CollectGold(zero, 5, 1f, null);
		goldScript.AddGold(GoldNum, 1f, 1f, isAdd2PlayerInfo: true, 7);
	}
}
