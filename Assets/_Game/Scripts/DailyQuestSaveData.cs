using System;

[Serializable]
public class DailyQuestSaveData
{
	public int ID;

	public int State;

	public int FinishQuestNum;

	private DailyQuestDataCsv dqdc;

	public void SetData(int id, int state)
	{
		ID = id;
		FinishQuestNum = 0;
		SetState(state);
	}

	public bool IsHaveNext()
	{
		if (FinishQuestNum >= GetData().QuestNum)
		{
			return false;
		}
		return true;
	}

	public void SetState(int state, bool isShowTaster = false)
	{
		if (isShowTaster && State == 0 && state == 1)
		{
			ResourceLoadManager.GetInstance().ShowQuestTips(ID);
		}
		State = state;
		if (State == 2)
		{
			FinishQuestNum++;
			if (FinishQuestNum < GetData().QuestNum)
			{
				SetState(0);
				PlayerInfo.Instance.GetDailyQuestSaveData().CheckTask();
			}
		}
	}

	public DailyQuestDataCsv GetData()
	{
		if (dqdc == null)
		{
			dqdc = DailyQuestDataManagerCsv.instance.GetItemByID(ID);
		}
		return dqdc;
	}

	public int GetRequireType()
	{
		DailyQuestDataCsv data = GetData();
		return data.GetQuestRequireType();
	}

	public int GetQuestRequireNum()
	{
		DailyQuestDataCsv data = GetData();
		int result = data.GetQuestRequireNum();
		if (FinishQuestNum == 1)
		{
			result = data.require1;
		}
		else if (FinishQuestNum == 2)
		{
			result = data.require2;
		}
		return result;
	}

	public int GetQuestRewartNum()
	{
		DailyQuestDataCsv data = GetData();
		int result = data.RewardNum;
		if (FinishQuestNum == 2)
		{
			result = data.reward1;
		}
		else if (FinishQuestNum == 3)
		{
			result = data.reward2;
		}
		return result;
	}

	public void CheckAll()
	{
		switch (GetRequireType())
		{
		case 20:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().WatchAD_num, GetQuestRequireNum());
			break;
		case 50:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().CollectDPStar_num, GetQuestRequireNum());
			break;
		case 10:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().PassLevel_num, GetQuestRequireNum());
			break;
		case 80:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().CollectScore_num, GetQuestRequireNum());
			break;
		case 40:
		case 41:
		case 42:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().CollectBonus_num, GetQuestRequireNum());
			break;
		case 60:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().UnlockChapter_num, GetQuestRequireNum());
			break;
		case 70:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().Good_num, GetQuestRequireNum());
			break;
		case 71:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().Great_num, GetQuestRequireNum());
			break;
		case 72:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().Excellent_num, GetQuestRequireNum());
			break;
		case 73:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().Amazing_num, GetQuestRequireNum());
			break;
		case 74:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().Unbelievable_num, GetQuestRequireNum());
			break;
		case 75:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().Outstanding_num, GetQuestRequireNum());
			break;
		case 90:
			CheckItem(PlayerInfo.Instance.GetDailyQuestSaveData().WordSearch_num, GetQuestRequireNum());
			break;
		}
		PlayerInfo.Instance.SetDailyQuestSaveData();
	}

	private void CheckItem(int currentNum, int maxNum)
	{
		if (State != 2 && currentNum >= maxNum)
		{
			SetState(1, isShowTaster: true);
		}
	}
}
