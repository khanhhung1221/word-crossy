using System;
using System.Collections.Generic;

[Serializable]
public class DailyQuestSaveDataManager
{
	public string dailyLevelTime;

	public List<DailyQuestSaveData> QuestList = new List<DailyQuestSaveData>();

	private static DailyQuestSaveDataManager _instance;

	public int _passLevel_num;

	public int _watchAD_num;

	public int _collectScore_num;

	public int _collectBonus_num;

	public int _collectDPStar_num;

	public int _unlockChapter_num;

	public int _good_num;

	public int _great_num;

	public int _excellent_num;

	public int _amazing_num;

	public int _unbelievable_num;

	public int _outstanding_num;

	public int _wordsearch_num;

	public int PassLevel_num
	{
		get
		{
			return _passLevel_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 6)
			{
				_passLevel_num = 0;
				return;
			}
			_passLevel_num = value;
			CheckTask();
		}
	}

	public int WatchAD_num
	{
		get
		{
			return _watchAD_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_watchAD_num = 0;
				return;
			}
			_watchAD_num = value;
			CheckTask();
		}
	}

	public int CollectScore_num
	{
		get
		{
			return _collectScore_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 6)
			{
				_collectScore_num = 0;
				return;
			}
			_collectScore_num = value;
			CheckTask();
		}
	}

	public int CollectBonus_num
	{
		get
		{
			return _collectBonus_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_collectBonus_num = 0;
				return;
			}
			_collectBonus_num = value;
			CheckTask();
		}
	}

	public int CollectDPStar_num
	{
		get
		{
			return _collectDPStar_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5 || !WordJsonDataManager.instance.IsOpenDailyPuzzle())
			{
				_collectDPStar_num = 0;
				return;
			}
			_collectDPStar_num = value;
			CheckTask();
		}
	}

	public int UnlockChapter_num
	{
		get
		{
			return _unlockChapter_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_unlockChapter_num = 0;
				return;
			}
			_unlockChapter_num = value;
			CheckTask();
		}
	}

	public int Good_num
	{
		get
		{
			return _good_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_good_num = 0;
				return;
			}
			_good_num = value;
			CheckTask();
		}
	}

	public int Great_num
	{
		get
		{
			return _great_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_great_num = 0;
				return;
			}
			_great_num = value;
			CheckTask();
		}
	}

	public int Excellent_num
	{
		get
		{
			return _excellent_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_excellent_num = 0;
				return;
			}
			_excellent_num = value;
			CheckTask();
		}
	}

	public int Amazing_num
	{
		get
		{
			return _amazing_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_amazing_num = 0;
				return;
			}
			_amazing_num = value;
			CheckTask();
		}
	}

	public int Unbelievable_num
	{
		get
		{
			return _unbelievable_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_unbelievable_num = 0;
				return;
			}
			_unbelievable_num = value;
			CheckTask();
		}
	}

	public int Outstanding_num
	{
		get
		{
			return _outstanding_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_outstanding_num = 0;
				return;
			}
			_outstanding_num = value;
			CheckTask();
		}
	}

	public int WordSearch_num
	{
		get
		{
			return _wordsearch_num;
		}
		set
		{
			if (PlayerInfo.Instance.MaxPlayLevel < 5)
			{
				_wordsearch_num = 0;
				return;
			}
			_wordsearch_num = value;
			CheckTask();
		}
	}

	public int isCanClaim()
	{
		int num = 0;
		for (int i = 0; i < QuestList.Count; i++)
		{
			if (QuestList[i].State == 1)
			{
				num++;
			}
		}
		return num;
	}

	public DailyQuestSaveData GetDataByID(int id)
	{
		for (int i = 0; i < QuestList.Count; i++)
		{
			if (QuestList[i].ID == id)
			{
				return QuestList[i];
			}
		}
		return null;
	}

	public List<DailyQuestSaveData> Getlist(int index)
	{
		if (string.IsNullOrEmpty(dailyLevelTime))
		{
			dailyLevelTime = "0";
		}
		DateTime dateTime = new DateTime(long.Parse(dailyLevelTime));
		if (QuestList.Count == 0 || false || dateTime.Day != DateTime.Now.Day)
		{
			ClearData();
			dailyLevelTime = DateTime.Now.Ticks.ToString();
			QuestList.Clear();
			if (PlayerInfo.Instance.LastOfferPayValue > 0f)
			{
				SetData(DailyQuestDataManager.GetInstance().GetPayList());
			}
			else
			{
				SetData(DailyQuestDataManager.GetInstance().GetNoPayList());
			}
		}
		return QuestList;
	}

	public void SetData(Dictionary<int, DailyQuestDataCsv> dataList)
	{
		foreach (KeyValuePair<int, DailyQuestDataCsv> data in dataList)
		{
			DailyQuestSaveData dailyQuestSaveData = new DailyQuestSaveData();
			dailyQuestSaveData.SetData(data.Key, 0);
			QuestList.Add(dailyQuestSaveData);
		}
	}

	public void ChangeFBState()
	{
		for (int i = 0; i < Getlist(1).Count; i++)
		{
			if (Getlist(2)[i].ID == 1 || Getlist(3)[i].ID == 14)
			{
				Getlist(4)[i].SetState(1, isShowTaster: true);
			}
		}
	}

	public void RemoveADQuestInList()
	{
		DailyQuestSaveData dailyQuestSaveData = new DailyQuestSaveData();
		int num = 0;
		while (true)
		{
			if (num < QuestList.Count)
			{
				if (QuestList[num].ID == 2 && QuestList[num].State == 0)
				{
					break;
				}
				num++;
				continue;
			}
			return;
		}
		dailyQuestSaveData = QuestList[num];
		QuestList.Remove(dailyQuestSaveData);
	}

	public void CheckTask()
	{
		for (int i = 0; i < QuestList.Count; i++)
		{
			QuestList[i].CheckAll();
		}
	}

	public bool IsHaveQuestByRequireType(int require_type)
	{
		for (int i = 0; i < QuestList.Count; i++)
		{
			if (QuestList[i].GetRequireType() == require_type && QuestList[i].State == 0)
			{
				return true;
			}
		}
		return false;
	}

	public void ClearData()
	{
		_passLevel_num = 0;
		_watchAD_num = 0;
		_collectScore_num = 0;
		_collectBonus_num = 0;
		_collectDPStar_num = 0;
		_unlockChapter_num = 0;
		_good_num = 0;
		_great_num = 0;
		_amazing_num = 0;
		_excellent_num = 0;
		_unbelievable_num = 0;
		_outstanding_num = 0;
		_wordsearch_num = 0;
	}

	public void RefreshData()
	{
		dailyLevelTime = DateTime.Now.Ticks.ToString();
		QuestList = new List<DailyQuestSaveData>();
		if (PlayerInfo.Instance.LastOfferPayValue > 0f)
		{
			SetData(DailyQuestDataManager.GetInstance().GetPayList());
		}
		else
		{
			SetData(DailyQuestDataManager.GetInstance().GetNoPayList());
		}
		ClearData();
	}
}
