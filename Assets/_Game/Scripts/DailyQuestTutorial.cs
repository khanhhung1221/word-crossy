using UnityEngine;
using UnityEngine.UI;

public class DailyQuestTutorial : MonoBehaviour
{
	public Text text;

	public RectTransform btn_rect;

	public RectTransform img_rect;

	private void Start()
	{
		if (MainUIScript.Instance != null)
		{
			MainUIScript.Instance.SetDailyQuestShow(isShow: false);
		}
		text.text = TextLibraryDataManagerCsv.instance.GetText(1175);
		if (WordGlobal.isMainUIShowBanner)
		{
			RectTransform rectTransform = btn_rect;
			Vector2 anchoredPosition = btn_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, -533f + WordGlobal.showBanner_bottomupy);
			RectTransform rectTransform2 = img_rect;
			Vector2 anchoredPosition2 = img_rect.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, -333f + WordGlobal.showBanner_bottomupy);
		}
		else
		{
			RectTransform rectTransform3 = btn_rect;
			Vector2 anchoredPosition3 = btn_rect.anchoredPosition;
			rectTransform3.anchoredPosition = new Vector2(anchoredPosition3.x, -533f);
			RectTransform rectTransform4 = img_rect;
			Vector2 anchoredPosition4 = img_rect.anchoredPosition;
			rectTransform4.anchoredPosition = new Vector2(anchoredPosition4.x, -333f);
		}
	}

	public void BtnGotItClick()
	{
		Object.Destroy(base.gameObject);
		if (MainUIScript.Instance != null)
		{
            MainUIScript.Instance.SetDailyQuestShow(isShow: true);
            MainUIScript.Instance.BtnDailyQuestClick();
			TutorialDailyQuestFinish.isHide = true;
		}
		else
		{
			WordGlobal.DebugLog("DailyQuestTutorial BtnGotItClick MainUIScript is null");
		}
	}
}
