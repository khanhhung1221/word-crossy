public enum DailyQuestType
{
	CompleteNewLevel = 10,
	WatchAD = 20,
	CollectScore = 80,
	BlindFB = 30,
	CollectBonusWord = 40,
	CollectDPStar = 50,
	UnlockNewChapter = 60,
	CompleteGood = 70,
	CompleteGreat = 71,
	CompleteExcellent = 72,
	CompleteAmazing = 73,
	CompleteUnbelievable = 74,
	CompleteOutstanding = 75,
	WordSearch_num = 90
}
