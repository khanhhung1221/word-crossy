using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DailyQuestUIScript : MonoBehaviour
{
	public Transform RootTrans;

	public Text TimeText;

	public Text Congratulations_txt;

	public RectTransform Bg_rectTran;

	public long TotalTime;

	private IEnumerator CountDown()
	{
		while (TotalTime >= 0)
		{
			TimeText.GetComponent<Text>().text = WordGlobal.ChangeTimeToNomral(TotalTime);
			yield return new WaitForSeconds(1f);
			TotalTime--;
		}
	}

	private void Start()
	{
		PlayerInfo.Instance.GetLittleTipsData().SetDailyQuestOpen();
		PlayerInfo.Instance.SetDailyQuestSaveData();
		DateTime today = DateTime.Today;
		DateTime d = DateTime.Parse(DateTime.Now.ToString());
		long num = (d - today).Ticks / 10000000;
		TotalTime = 86400 - num;
		StartCoroutine(CountDown());
		List<DailyQuestSaveData> list = new List<DailyQuestSaveData>();
		List<DailyQuestItem> list2 = new List<DailyQuestItem>();
		list = PlayerInfo.Instance.GetDailyQuestSaveData().Getlist(12);
		for (int i = 0; i < list.Count; i++)
		{
			GameObject gameObject = UIManager.Instance.LoadMainUI("UI/DailyQuestItem");
			DailyQuestSaveData dailyQuestSaveData = list[i];
			gameObject.transform.SetParent(RootTrans);
			DailyQuestItem component = gameObject.GetComponent<DailyQuestItem>();
			component.SetData(DailyQuestDataManagerCsv.instance.GetItemByID(dailyQuestSaveData.ID), dailyQuestSaveData.State);
			list2.Add(component);
		}
		bool flag = true;
		Congratulations_txt.gameObject.SetActive(value: false);
		for (int j = 0; j < list2.Count; j++)
		{
			if (list2[j].state == 2)
			{
				list2[j].gameObject.SetActive(value: false);
			}
			else
			{
				flag = false;
			}
		}
		Bg_rectTran.sizeDelta = new Vector2(602f, 840f);
		if (flag)
		{
			Bg_rectTran.sizeDelta = new Vector2(602f, 357f);
			Congratulations_txt.gameObject.SetActive(value: true);
		}
	}

	private void SetUI(int currentNum, int maxNum, string claimTime, GameObject goBtn, GameObject claimBtn, GameObject alreadyGetBtn)
	{
		if (currentNum >= maxNum)
		{
			DateTime dateTime = new DateTime(long.Parse(claimTime));
			if (DateTime.Now.Day == dateTime.Day && DateTime.Now.Year == dateTime.Year)
			{
				alreadyGetBtn.SetActive(value: true);
			}
			else
			{
				claimBtn.SetActive(value: true);
			}
		}
		else
		{
			goBtn.SetActive(value: true);
		}
	}

	public void BtnCloseClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void BtnGoClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
		if (GameObject.Find("MainUI") == null)
		{
			LevelMapUIScript.Instance.ShowMainUI(PlayerInfo.Instance.MaxPlayLevel);
		}
	}

	public void BtnPuzzleGoClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
		if ((bool)MainUIScript.Instance)
		{
			UnityEngine.Object.DestroyImmediate(MainUIScript.Instance.gameObject);
		}
		WordGlobal.LogEvent("Dailypuzzle_enter");
		if (PlayerInfo.Instance.GetDailyDayId() >= WordJsonDataManager.instance.GetDailyMinLevel() && PlayerInfo.Instance.GetDailyDayId() <= WordJsonDataManager.instance.GetDailyMaxLevel())
		{
			if (PlayerInfo.Instance.GetDailyPuzzleItem().IsFinished == 1)
			{
				DPCollectPicUIScript.UI_OPENTYPE = 0;
				UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCollectPicUI").name = "DPCollectPicUI";
				return;
			}
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_EnterLevel);
			if (GameObject.Find("MainUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/MainUI", isPop: false, UIManager.MainUIType.Type_Daily);
				PlayerInfo.Instance.GetLittleTipsData().SetDailyPuzzleOpen();
			}
			LevelMapUIScript.Instance.HideLevelMapUI();
			DPCollectPicUIScript.UI_OPENTYPE = 2;
			UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCollectPicUI").name = "DPCollectPicUI";
		}
		else
		{
			ResourceLoadManager.GetInstance().ShowConfirmTipsUI(ConfirmUIScript.ConfirmType.Type_Update, 1155, 1156, GoToUpdateVersion);
		}
	}

	public void GoToUpdateVersion()
	{
		MS_Interface.Current.OnRate();
	}

	public void BtnClaimClick()
	{
		UIManager.Instance.LoadMainUI("UI/DailyQuestRewardUI", isPop: true);
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void BtnLevelClaimClick()
	{
	}

	public void BtnScoreClaimClick()
	{
	}

	public void BtnADClaimClick()
	{
	}

	public void BtnBonusClaimClick()
	{
	}

	public void BtnPuzzleClaimClick()
	{
	}

	private float GetAmountFloat(int numerator, int denominator)
	{
		return (float)numerator / (float)denominator;
	}

	public void BtnWatchVideoClick()
	{
		if (AdsManager.Instance.ShowIncentivizedAd("DailyQuestUIScript"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEvent));
		}
	}

	public void WatchVideoEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEvent));
		if (ret)
		{
			CommonTipsScript.Instance.ShowUI(string.Format(WordGlobal.ReadText(1017), PlayerInfo.Instance.BonusGoldNumber * 2), 0, WordGlobal.ReadText(1016), delegate(object obj)
			{
				Vector3 starPos = (Vector3)obj;
				GoldScript goldScript = UnityEngine.Object.FindObjectOfType<GoldScript>();
				goldScript.CollectGold(starPos, 5, 1f, null);
				goldScript.AddGold(PlayerInfo.Instance.BonusGoldNumber * 2, 1f, 1f, isAdd2PlayerInfo: true, 8);
			}, showClose: false);
		}
	}

	public void LateUpdate()
	{
	}
}
