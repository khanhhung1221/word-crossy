public class DailyRewardData
{
	public int day;

	public int target;

	public string reward;

	public int item_type;

	public int item_id;

	public int item_num;

	public DailyRewardData(string[] values, int valueIndex)
	{
		int num = 0;
		day = int.Parse(values[num++]);
		target = int.Parse(values[num++]);
		reward = values[num++];
	}

	public void Init()
	{
		string[] array = reward.Split('_');
		item_type = int.Parse(array[0]);
		if (item_type == 1)
		{
			item_id = int.Parse(array[1]);
			item_num = int.Parse(array[2]);
		}
		else if (item_type == 3)
		{
			item_num = int.Parse(array[1]);
		}
	}
}
