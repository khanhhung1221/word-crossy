using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class DailyRewardDataManagerCsv : ManagerCsv
{
	private static DailyRewardDataManagerCsv inst;

	private Dictionary<int, DailyRewardData> DataList;

	public static DailyRewardDataManagerCsv instance => inst ?? (inst = new DailyRewardDataManagerCsv());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			int num = 0;
			MemoryStream stream = new MemoryStream(file);
			DataList = new Dictionary<int, DailyRewardData>();
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								DailyRewardData dailyRewardData = new DailyRewardData(array, 0);
								int key = dailyRewardData.target * 10 + dailyRewardData.day;
								DataList.Add(key, dailyRewardData);
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public DailyRewardData GetData(int day, int target)
	{
		int key = target * 10 + day;
		if (DataList.ContainsKey(key))
		{
			return DataList[key];
		}
		return null;
	}
}
