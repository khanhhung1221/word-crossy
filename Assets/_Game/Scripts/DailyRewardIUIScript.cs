using System;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewardIUIScript : MonoBehaviour
{
	public Text TryText;

	public Transform UIRoot;

	private DailyRewardItemInfo drii1;

	private DailyRewardItemInfo drii2;

	public Transform ClaimedTrans;

	public Transform OneItemTrans;

	public Transform TwoItemTrans;

	public Image OneItemBgImg;

	public Image OneItemIconImg;

	public Image OneItemFiftyImg;

	public Image TwoItemBg1Img;

	public Image TwoItemBg2Img;

	public Text OneItemNumText;

	public Transform CenterTrans;

	public static DailyRewardIUIScript Instance;

	private GameObject targetObj;

	private float testY;

	private float distance;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		if (PlayerInfo.Instance.IsGetDailyReward != 0)
		{
			int day = DateTime.Now.Day;
			if (day != PlayerInfo.Instance.LastDailyRewardUtcTime)
			{
				PlayerInfo.Instance.IsGetDailyReward = 0;
			}
		}
		for (int i = 1; i <= 28; i++)
		{
			drii1 = null;
			drii2 = null;
			string empty = string.Empty;
			string[] array = empty.Split(',');
			if (array.Length == 2)
			{
				drii1 = PraseInfo(array, 0);
				drii2 = PraseInfo(array, 1);
			}
			else if (array.Length == 1)
			{
				drii1 = PraseInfo(array, 0);
			}
			GameObject gameObject = UIManager.Instance.LoadMainUI("UI/DailyRewardItem");
			gameObject.transform.SetParent(UIRoot);
			gameObject.transform.SetAsFirstSibling();
			DailyRewardItem component = gameObject.GetComponent<DailyRewardItem>();
			component.SetInfo(drii1, drii2, i);
			if (i == PlayerInfo.Instance.DailyRewardDay)
			{
				targetObj = gameObject;
			}
		}
		Invoke("Move", 0.01f);
	}

	public DailyRewardItemInfo PraseInfo(string[] strArray, int index)
	{
		string[] array = strArray[index].Split('_');
		DailyRewardItemInfo dailyRewardItemInfo = new DailyRewardItemInfo();
		dailyRewardItemInfo.Drt = (DailyRewardType)int.Parse(array[0]);
		if (dailyRewardItemInfo.Drt == DailyRewardType.Buff)
		{
			dailyRewardItemInfo.ItemID = int.Parse(array[1]);
			dailyRewardItemInfo.ItemNum = int.Parse(array[3]);
		}
		else if (dailyRewardItemInfo.Drt == DailyRewardType.Item)
		{
			dailyRewardItemInfo.ItemID = int.Parse(array[1]);
			dailyRewardItemInfo.ItemNum = int.Parse(array[2]);
		}
		else if (dailyRewardItemInfo.Drt == DailyRewardType.Coin)
		{
			dailyRewardItemInfo.ItemNum = int.Parse(array[1]);
		}
		return dailyRewardItemInfo;
	}

	private void Move()
	{
		Vector3 position = targetObj.transform.position;
		float y = position.y;
		Vector3 position2 = CenterTrans.position;
		distance = y - position2.y;
		Transform uIRoot = UIRoot;
		Vector3 position3 = UIRoot.position;
		float x = position3.x;
		Vector3 position4 = UIRoot.position;
		float y2 = position4.y - distance;
		Vector3 position5 = UIRoot.position;
		uIRoot.position = new Vector3(x, y2, position5.z);
	}

	private void Update()
	{
	}

	public void ShowClaimedTrans(DailyRewardItemInfo d1, DailyRewardItemInfo d2)
	{
		if (d2 == null)
		{
			OneItemTrans.gameObject.SetActive(value: true);
			TwoItemTrans.gameObject.SetActive(value: false);
			if (d1.Drt == DailyRewardType.Coin)
			{
				OneItemBgImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/金币堆");
				OneItemIconImg.gameObject.SetActive(value: false);
			}
			else
			{
				OneItemIconImg.gameObject.SetActive(value: true);
				OneItemIconImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/" + d1.ItemID);
			}
			if (d1.Drt == DailyRewardType.Buff)
			{
				OneItemNumText.text = d1.ItemNum + TextLibraryDataManagerCsv.instance.GetText(1327);
				OneItemFiftyImg.gameObject.SetActive(value: true);
			}
			else
			{
				OneItemNumText.text = "X " + d1.ItemNum;
			}
		}
		else
		{
			OneItemTrans.gameObject.SetActive(value: false);
			TwoItemTrans.gameObject.SetActive(value: true);
			ShowUIItemByData(d1, TwoItemBg1Img);
			ShowUIItemByData(d2, TwoItemBg2Img);
		}
		ClaimedTrans.gameObject.SetActive(value: true);
	}

	private void ShowUIItemByData(DailyRewardItemInfo dri, Image img)
	{
		if (dri.Drt == DailyRewardType.Coin)
		{
			img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/coin");
		}
		else
		{
			Transform transform = img.transform.Find("ItemIconImg");
			transform.gameObject.SetActive(value: true);
			Image component = transform.GetComponent<Image>();
			component.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/" + dri.ItemID);
		}
		Text component2 = img.transform.Find("ItemNumText").GetComponent<Text>();
		if (dri.Drt == DailyRewardType.Buff)
		{
			component2.text = dri.ItemNum + " " + TextLibraryDataManagerCsv.instance.GetText(1327);
			Image component3 = img.transform.Find("50%Img").GetComponent<Image>();
			component3.gameObject.SetActive(value: true);
		}
		else
		{
			component2.text = "X " + dri.ItemNum;
		}
	}

	public void BtnBgClick()
	{
		ClaimedTrans.gameObject.SetActive(value: false);
	}

	public void BtnCloseClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}
}
