using System;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewardItem : MonoBehaviour
{
	public Text DayText;

	public Image Item1Img;

	public Image Item2Img;

	public Image ItemBg1Img;

	public Image ItemBg2Img;

	public Image FiftyPer1Img;

	public Image FiftyPer2Img;

	public Text Item1Num;

	public Text Item2Num;

	private DailyRewardItemInfo d1;

	private DailyRewardItemInfo d2;

	public Transform TodayTrans;

	public Transform NotTodayTrans;

	public Transform AlreadyGetTrans;

	private void Start()
	{
	}

	public void SetInfo(DailyRewardItemInfo drii1, DailyRewardItemInfo drii2, int day)
	{
		if (day < PlayerInfo.Instance.DailyRewardDay)
		{
			AlreadyGetTrans.gameObject.SetActive(value: true);
			NotTodayTrans.gameObject.SetActive(value: true);
		}
		else if (day == PlayerInfo.Instance.DailyRewardDay)
		{
			if (PlayerInfo.Instance.IsGetDailyReward == 0)
			{
				Invoke("BtnClaimClick", 0.5f);
				TodayTrans.gameObject.SetActive(value: true);
			}
			else
			{
				NotTodayTrans.gameObject.SetActive(value: true);
			}
		}
		else
		{
			NotTodayTrans.gameObject.SetActive(value: true);
		}
		d1 = drii1;
		d2 = drii2;
		if (drii1 != null)
		{
			if (drii1.Drt == DailyRewardType.Coin)
			{
				ItemBg1Img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/coin");
				Item1Img.gameObject.SetActive(value: false);
			}
			else
			{
				Item1Img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/" + drii1.ItemID);
			}
			Item1Num.text = "X" + drii1.ItemNum.ToString();
			if (drii1.Drt == DailyRewardType.Buff)
			{
				Item1Num.text = drii1.ItemNum.ToString() + TextLibraryDataManagerCsv.instance.GetText(1327);
				FiftyPer1Img.gameObject.SetActive(value: true);
			}
		}
		if (drii2 != null)
		{
			if (drii2.Drt == DailyRewardType.Coin)
			{
				ItemBg2Img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/coin");
			}
			else
			{
				Item2Img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/" + drii2.ItemID);
			}
			Item2Num.text = "X" + drii2.ItemNum.ToString();
			if (drii2.Drt == DailyRewardType.Buff)
			{
				Item2Num.text = drii2.ItemNum.ToString() + TextLibraryDataManagerCsv.instance.GetText(1327);
				FiftyPer2Img.gameObject.SetActive(value: true);
			}
		}
		else
		{
			ItemBg2Img.gameObject.SetActive(value: false);
		}
		DayText.text = day.ToString();
	}

	public void BtnClaimClick()
	{
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_DailyGift);
		if (PlayerInfo.Instance.DailyRewardDay == 3)
		{
		}
		if (d1.Drt == DailyRewardType.Coin)
		{
			PlayerInfo.Instance.AddGold(d1.ItemNum, 5);
		}
		else if (d1.Drt == DailyRewardType.Buff)
		{
			switch (d1.ItemID)
			{
			case 1001:
			{
				if (PlayerInfo.Instance.TipsCountBuffTimeLeft() <= 0)
				{
					PlayerInfo.Instance.TipsCountBuffDateTime = WordGlobal.GetTimeStamp(DateTime.Now);
				}
				DateTime time2 = WordGlobal.ConvertStringToDateTime(PlayerInfo.Instance.TipsCountBuffDateTime).AddSeconds(d1.ItemNum * 60);
				PlayerInfo.Instance.TipsCountBuffDateTime = WordGlobal.GetTimeStamp(time2);
				break;
			}
			case 1002:
			{
				if (PlayerInfo.Instance.ClickTipsCountBuffTimeLeft() <= 0)
				{
					PlayerInfo.Instance.ClickTipsCountBuffDateTime = WordGlobal.GetTimeStamp(DateTime.Now);
				}
				DateTime time3 = WordGlobal.ConvertStringToDateTime(PlayerInfo.Instance.ClickTipsCountBuffDateTime).AddSeconds(d1.ItemNum * 60);
				PlayerInfo.Instance.ClickTipsCountBuffDateTime = WordGlobal.GetTimeStamp(time3);
				break;
			}
			case 1003:
			{
				if (PlayerInfo.Instance.FirstTipsCountBuffTimeLeft() <= 0)
				{
					PlayerInfo.Instance.FirstTipsCountBuffDateTime = WordGlobal.GetTimeStamp(DateTime.Now);
				}
				DateTime time = WordGlobal.ConvertStringToDateTime(PlayerInfo.Instance.FirstTipsCountBuffDateTime).AddSeconds(d1.ItemNum * 60);
				PlayerInfo.Instance.FirstTipsCountBuffDateTime = WordGlobal.GetTimeStamp(time);
				break;
			}
			}
		}
		else if (d1.Drt == DailyRewardType.Item)
		{
			switch (d1.ItemID)
			{
			case 1001:
				PlayerInfo.Instance.TipsCount += d1.ItemNum;
				break;
			case 1002:
				PlayerInfo.Instance.ClickTipsCount += d1.ItemNum;
				break;
			case 1003:
				PlayerInfo.Instance.FirstTipsCount += d1.ItemNum;
				break;
			}
		}
		if (d2 != null)
		{
			if (d2.Drt == DailyRewardType.Coin)
			{
				PlayerInfo.Instance.AddGold(d2.ItemNum, 6);
			}
			else if (d2.Drt == DailyRewardType.Buff)
			{
				switch (d2.ItemID)
				{
				case 1001:
				{
					if (PlayerInfo.Instance.TipsCountBuffTimeLeft() <= 0)
					{
						PlayerInfo.Instance.TipsCountBuffDateTime = WordGlobal.GetTimeStamp(DateTime.Now);
					}
					DateTime time5 = WordGlobal.ConvertStringToDateTime(PlayerInfo.Instance.TipsCountBuffDateTime).AddSeconds(d2.ItemNum * 60);
					PlayerInfo.Instance.TipsCountBuffDateTime = WordGlobal.GetTimeStamp(time5);
					break;
				}
				case 1002:
				{
					if (PlayerInfo.Instance.ClickTipsCountBuffTimeLeft() <= 0)
					{
						PlayerInfo.Instance.ClickTipsCountBuffDateTime = WordGlobal.GetTimeStamp(DateTime.Now);
					}
					DateTime time6 = WordGlobal.ConvertStringToDateTime(PlayerInfo.Instance.ClickTipsCountBuffDateTime).AddSeconds(d2.ItemNum * 60);
					PlayerInfo.Instance.ClickTipsCountBuffDateTime = WordGlobal.GetTimeStamp(time6);
					break;
				}
				case 1003:
				{
					if (PlayerInfo.Instance.FirstTipsCountBuffTimeLeft() <= 0)
					{
						PlayerInfo.Instance.FirstTipsCountBuffDateTime = WordGlobal.GetTimeStamp(DateTime.Now);
					}
					DateTime time4 = WordGlobal.ConvertStringToDateTime(PlayerInfo.Instance.FirstTipsCountBuffDateTime).AddSeconds(d2.ItemNum * 60);
					PlayerInfo.Instance.FirstTipsCountBuffDateTime = WordGlobal.GetTimeStamp(time4);
					break;
				}
				}
			}
			else if (d2.Drt == DailyRewardType.Item)
			{
				switch (d2.ItemID)
				{
				case 1001:
					PlayerInfo.Instance.TipsCount += d2.ItemNum;
					break;
				case 1002:
					PlayerInfo.Instance.ClickTipsCount += d2.ItemNum;
					break;
				case 1003:
					PlayerInfo.Instance.FirstTipsCount += d2.ItemNum;
					break;
				}
			}
		}
		AlreadyGetTrans.gameObject.SetActive(value: true);
		NotTodayTrans.gameObject.SetActive(value: true);
		TodayTrans.gameObject.SetActive(value: false);
		PlayerInfo.Instance.IsGetDailyReward = 1;
		PlayerInfo.Instance.LastDailyRewardUtcTime = DateTime.Now.Day;
		PlayerInfo.Instance.DailyRewardDay++;
		if (PlayerInfo.Instance.DailyRewardDay > 28)
		{
			PlayerInfo.Instance.DailyRewardDay = 1;
		}
		DailyRewardIUIScript.Instance.ShowClaimedTrans(d1, d2);
	}

	private void Update()
	{
	}
}
