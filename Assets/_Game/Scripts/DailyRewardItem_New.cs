using System;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewardItem_New : MonoBehaviour
{
	public Image bg_img;

	public Image icon_bgimg;

	public Image icon_img;

	public Text day_text;

	public Text num_text;

	public Image gou_img;

	private DailyRewardData drd;

	public GameObject half_img;

	private DailyRewardUI_New drui_new;

	private bool isShowNewA;

	private bool isShowGou;

	private float showGou_time = 0.3f;

	private void Start()
	{
	}

	public void Init(DailyRewardData t_drd, DailyRewardUI_New drui_new)
	{
		this.drui_new = drui_new;
		drd = t_drd;
		drd.Init();
		day_text.text = TextLibraryDataManagerCsv.instance.GetText(1037) + drd.day;
		if (drd.item_type == 1)
		{
			icon_bgimg.gameObject.SetActive(value: true);
			string str = drd.item_id + string.Empty;
			if (drd.item_id == 2001)
			{
				str = "1001";
				half_img.SetActive(value: true);
				num_text.text = drd.item_num / 60 + TextLibraryDataManagerCsv.instance.GetText(1327);
			}
			else
			{
				num_text.text = "+" + drd.item_num;
			}
			icon_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/" + str);
			icon_img.rectTransform.sizeDelta = new Vector2(icon_img.sprite.textureRect.width, icon_img.sprite.textureRect.height);
		}
		else if (drd.item_type == 3)
		{
			icon_bgimg.gameObject.SetActive(value: false);
			icon_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/jinbi_dr");
			icon_img.rectTransform.sizeDelta = new Vector2(100f, 100f);
			num_text.text = "+" + drd.item_num;
		}
		Image image = icon_bgimg;
		Color color = icon_bgimg.color;
		float r = color.r;
		Color color2 = icon_bgimg.color;
		float g = color2.g;
		Color color3 = icon_bgimg.color;
		image.color = new Color(r, g, color3.b, 1f);
		Image image2 = icon_img;
		Color color4 = icon_bgimg.color;
		float r2 = color4.r;
		Color color5 = icon_bgimg.color;
		float g2 = color5.g;
		Color color6 = icon_bgimg.color;
		image2.color = new Color(r2, g2, color6.b, 1f);
		gou_img.gameObject.SetActive(value: false);
		if (drd.day < PlayerInfo.Instance.DailyRewardDayNew)
		{
			Image image3 = icon_bgimg;
			Color color7 = icon_bgimg.color;
			float r3 = color7.r;
			Color color8 = icon_bgimg.color;
			float g3 = color8.g;
			Color color9 = icon_bgimg.color;
			image3.color = new Color(r3, g3, color9.b, 0.7f);
			Image image4 = icon_img;
			Color color10 = icon_bgimg.color;
			float r4 = color10.r;
			Color color11 = icon_bgimg.color;
			float g4 = color11.g;
			Color color12 = icon_bgimg.color;
			image4.color = new Color(r4, g4, color12.b, 0.7f);
			bg_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/dailylogin_yilingqu");
			gou_img.gameObject.SetActive(value: true);
		}
		else if (drd.day > PlayerInfo.Instance.DailyRewardDayNew)
		{
			bg_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/dailylogin_weilingqu");
		}
		else if (PlayerInfo.Instance.IsGetDailyReward % 10 == 1)
		{
			bg_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/dailylogin_weilingqu");
		}
		else
		{
			bg_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/dailylogin_now");
		}
		if (PlayerInfo.Instance.IsGetDailyReward % 10 == 1 && PlayerInfo.Instance.DailyRewardDayNew == 1)
		{
			Image image5 = icon_bgimg;
			Color color13 = icon_bgimg.color;
			float r5 = color13.r;
			Color color14 = icon_bgimg.color;
			float g5 = color14.g;
			Color color15 = icon_bgimg.color;
			image5.color = new Color(r5, g5, color15.b, 0.7f);
			Image image6 = icon_img;
			Color color16 = icon_bgimg.color;
			float r6 = color16.r;
			Color color17 = icon_bgimg.color;
			float g6 = color17.g;
			Color color18 = icon_bgimg.color;
			image6.color = new Color(r6, g6, color18.b, 0.7f);
			bg_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/dailylogin_yilingqu");
			gou_img.gameObject.SetActive(value: true);
		}
	}

	public void BtnClick()
	{
		if (drd.day != PlayerInfo.Instance.DailyRewardDayNew || PlayerInfo.Instance.IsGetDailyReward % 10 == 1)
		{
			return;
		}
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_DailyGift);
		if (true)
		{
			if (drd.item_type == 3)
			{
				PlayerInfo.Instance.AddGold(drd.item_num, 7);
			}
			else if (drd.item_type == 1)
			{
				switch (drd.item_id)
				{
				case 1001:
					PlayerInfo.Instance.TipsCount += drd.item_num;
					break;
				case 1002:
					PlayerInfo.Instance.ClickTipsCount += drd.item_num;
					break;
				case 1003:
					PlayerInfo.Instance.FirstTipsCount += drd.item_num;
					break;
				case 2001:
					PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setgHalfPayFirstTipLastTime(drd.item_num);
					break;
				}
			}
		}
		PlayerInfo.Instance.IsGetDailyReward++;
		PlayerInfo.Instance.LastDailyRewardUtcTime = DateTime.Now.Day;
		PlayerInfo.Instance.DailyRewardDayNew++;
		if (PlayerInfo.Instance.DailyRewardDayNew > 7)
		{
			PlayerInfo.Instance.DailyRewardDayNew = 1;
		}
		gou_img.fillAmount = 0f;
		isShowGou = true;
		Init(drd, drui_new);
	}

	private void Update()
	{
		if (!isShowGou)
		{
			return;
		}
		gou_img.fillAmount += 1f / showGou_time * Time.deltaTime;
		if (gou_img.fillAmount >= 0.9f)
		{
			if (isShowNewA)
			{
				isShowNewA = false;
			}
			gou_img.fillAmount = 1f;
			isShowGou = false;
		}
	}
}
