using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewardUI_Box : MonoBehaviour
{
	public Button btn_mono;

	public Image box01_img;

	public Image box02_img;

	public RectTransform rect;

	public Animator anim;

	public GameObject eff_go;

	private string item_str;

	private DailyRewardUI_New dr;

	public GameObject loop_effGo;

	private void Start()
	{
		rect = (base.transform as RectTransform);
		anim = base.gameObject.GetComponent<Animator>();
		loop_effGo.SetActive(value: true);
	}

	public void GetReward(DailyRewardUI_New dr, string itemStr = "")
	{
		item_str = itemStr;
		this.dr = dr;
		if (!string.IsNullOrEmpty(item_str))
		{
			dr.StartAnim(this, itemStr);
			return;
		}
		box01_img.DOFade(0f, 0.2f);
		box02_img.DOFade(0f, 0.2f);
	}

	public void PlayAnim()
	{
		anim.enabled = true;
		loop_effGo.SetActive(value: false);
		anim.Play("BOX01");
		eff_go.SetActive(value: true);
	}
}
