using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewardUI_New : MonoBehaviour
{
	public static bool isShow;

	public Text title_txt;

	public Text des_txt;

	public Text next_txt;

	public DailyRewardUI_Box box01;

	public DailyRewardUI_Box box02;

	public DailyRewardUI_Box box03;

	public RectTransform Item_rect;

	public Image Item_bgImg;

	public Image Item_halfImg;

	public Image Item_iconImg;

	public Text Num_text;

	public RectTransform Collect_rect;

	public RectTransform main_rect;

	public Transform light_tran;

	private int isGetGold;

	private float hide_wait_time;

	private int click_index = -1;

	private void Start()
	{
		isShow = true;
		light_tran.gameObject.SetActive(value: false);
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1036);
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1297);
		next_txt.text = TextLibraryDataManagerCsv.instance.GetText(1325);
		if (PlayerInfo.Instance.IsGetDailyReward != 0)
		{
			int day = DateTime.Now.Day;
			if (day != PlayerInfo.Instance.LastDailyRewardUtcTime)
			{
				PlayerInfo.Instance.IsGetDailyReward = 0;
			}
		}
		if (PlayerInfo.Instance.IsGetDailyReward == 0 && PlayerInfo.Instance.DailyRewardDayNew == 1)
		{
			PlayerInfo.Instance.DailyRewardTarget = 0;
			if (PlayerInfo.Instance.FirstTipButtonUnlock == 1)
			{
				if (PlayerInfo.Instance.LastOfferPayValue > 0f)
				{
					PlayerInfo.Instance.DailyRewardTarget = 2;
				}
				else
				{
					PlayerInfo.Instance.DailyRewardTarget = 1;
				}
			}
			if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HalfPayFirstTipTutorial == 0 && PlayerInfo.Instance.LastOfferPayValue == 0f)
			{
				PlayerInfo.Instance.DailyRewardTarget = 0;
			}
		}
		WordGlobal.LogEvent("DailyReward_Page");
	}

	private void Update()
	{
		if (hide_wait_time > 0f)
		{
			hide_wait_time -= Time.deltaTime;
			if (hide_wait_time <= 0f)
			{
				Hide();
			}
		}
		if (light_tran.gameObject.activeInHierarchy)
		{
			light_tran.Rotate(0f, 0f, -30f * Time.deltaTime);
		}
	}

	public void StartAnim(DailyRewardUI_Box box, string item_str)
	{
		Debug.Log("start Anim");
		StartCoroutine(GetAnim(box, item_str));
	}

	public IEnumerator GetAnim(DailyRewardUI_Box box, string item_str)
	{
		if (!box.Equals(box01))
		{
			box01.gameObject.SetActive(value: false);
		}
		if (!box.Equals(box02))
		{
			box02.gameObject.SetActive(value: false);
		}
		if (!box.Equals(box03))
		{
			box03.gameObject.SetActive(value: false);
		}
		des_txt.enabled = false;
		box.rect.DOAnchorPos(new Vector2(0f, -75f), 0.2f);
		box.rect.DOScale(1f, 0.2f);
		box.btn_mono.enabled = false;
		yield return new WaitForSeconds(0.2f);
		des_txt.enabled = true;
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1245);
		box.PlayAnim();
		yield return new WaitForSeconds(0.8f);
		string[] items = item_str.Split('_');
		if (items.Length > 0)
		{
			int num = int.Parse(items[0]);
			int num2 = int.Parse(items[1]);
			switch (num)
			{
			case 1:
			{
				isGetGold = 0;
				Item_bgImg.gameObject.SetActive(value: true);
				Item_bgImg.enabled = true;
				string str = num2 + string.Empty;
				if (num2 == 2001)
				{
					int num3 = int.Parse(items[2]);
					str = "1001";
					Item_halfImg.gameObject.SetActive(value: true);
					Num_text.text = num3 / 60 + TextLibraryDataManagerCsv.instance.GetText(1327);
				}
				else
				{
					Num_text.text = "+" + 1;
				}
				Item_iconImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/" + str);
				Item_iconImg.rectTransform.sizeDelta = new Vector2(Item_iconImg.sprite.textureRect.width, Item_iconImg.sprite.textureRect.height);
				break;
			}
			case 3:
				isGetGold = num2;
				Item_bgImg.gameObject.SetActive(value: true);
				Item_bgImg.enabled = false;
				Item_iconImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/jinbi_dr");
				Item_iconImg.rectTransform.sizeDelta = new Vector2(100f, 100f);
				Num_text.text = "+" + num2;
				break;
			}
		}
		Item_rect.DOAnchorPosY(70f, 0.2f);
		yield return new WaitForSeconds(0.5f);
		box.gameObject.SetActive(value: false);
		Item_rect.DOAnchorPosY(20f, 0.2f);
		Item_rect.DOScale(1.5f, 0.2f);
		yield return new WaitForSeconds(0.2f);
		light_tran.gameObject.SetActive(value: true);
		Num_text.gameObject.SetActive(value: true);
		Collect_rect.localScale = Vector3.zero;
		Collect_rect.gameObject.SetActive(value: true);
		Collect_rect.DOScale(1f, 0.2f).SetEase(Ease.OutBack);
	}

	public void Hide()
	{
		main_rect.DOScale(1.2f, 0.1f).OnComplete(delegate
		{
			main_rect.DOScale(0f, 0.2f).OnComplete(delegate
			{
				UnityEngine.Object.Destroy(base.gameObject);
			});
		});
		isShow = false;
	}

	public void BtnCollectClick()
	{
		WordGlobal.LogEvent("DailyReward_TapCollect");
		if (isGetGold > 0)
		{
			if (LevelMapUIScript.Instance != null)
			{
				PlayerInfo.Instance.AddGold(-isGetGold, 18);
				LevelMapUIScript.Instance.GoldItem.ResetGold();
				LevelMapUIScript.Instance.GoldItem._isAddGold = true;
				LevelMapUIScript.Instance.GoldItem.AddGold(isGetGold, 1f, 1f, isAdd2PlayerInfo: true, 19);
				LevelMapUIScript.Instance.GoldItem.CollectGold(Vector3.zero, 5, 1f, null);
			}
			hide_wait_time = 0.5f;
		}
		else
		{
			hide_wait_time = 0.4f;
			Item_rect.DOScale(1.7f, 0.1f).OnComplete(delegate
			{
				Item_rect.DOScale(1.5f, 0.2f);
			});
		}
	}

	public void BtnClaimClick(int index)
	{
		if (click_index <= 0 && PlayerInfo.Instance.IsGetDailyReward % 10 != 1)
		{
			click_index = index;
			string itemStr = WordGlobal.GetDailyRewardItem();
			if (PlayerInfo.Instance.DailyRewardDayNew == 1)
			{
				itemStr = WordGlobal.DailyReward_FirstItem;
			}
			PlayerInfo.Instance.AddItemByStr(itemStr, 6);
			LevelMapUIScript.Instance.GoldItem._isAddGold = true;
			PlayerInfo.Instance.IsGetDailyReward++;
			PlayerInfo.Instance.LastDailyRewardUtcTime = DateTime.Now.Day;
			PlayerInfo.Instance.DailyRewardDayNew++;
			if (click_index == 1)
			{
				box01.GetReward(this, itemStr);
				box02.GetReward(null, string.Empty);
				box03.GetReward(null, string.Empty);
			}
			else if (click_index == 2)
			{
				box02.GetReward(this, itemStr);
				box01.GetReward(null, string.Empty);
				box03.GetReward(null, string.Empty);
			}
			else
			{
				box03.GetReward(this, itemStr);
				box02.GetReward(null, string.Empty);
				box01.GetReward(null, string.Empty);
			}
		}
	}

	public void BtnClick01()
	{
		BtnClaimClick(1);
	}

	public void BtnClick02()
	{
		BtnClaimClick(2);
	}

	public void BtnClick03()
	{
		BtnClaimClick(3);
	}
}
