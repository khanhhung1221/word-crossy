public enum DebugType
{
	Console,
	Memory,
	System,
	Screen,
	Quality,
	Environment
}
