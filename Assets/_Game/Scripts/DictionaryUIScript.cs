using LitJson;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DictionaryUIScript : MonoBehaviour
{
	public Text m_txtTitle;

	public Text m_txtText;

	public GameObject m_objLeftBtn;

	public GameObject m_objRightBtn;

	public GameObject m_objDot;

	public GameObject m_objItem;

	public Transform m_tfDotRoot;

	public Transform m_tfItemRoot;

	public ScrollControlScript m_SCS;

	public GameObject m_objLoading;

	public Text m_TxtNet;

	private static string _tmpWord;

	private List<string> _wordList;

	private WordInfo _wordInfo;

	public int mCurrentIndex;

	private bool _isLoading;

	private void Start()
	{
		ScrollControlScript.OnEndMove = (UnityAction<int>)Delegate.Combine(ScrollControlScript.OnEndMove, new UnityAction<int>(OnItemEndMove));
	}

	private void Update()
	{
		if (_isLoading)
		{
			m_objLoading.transform.Rotate(0f, 0f, -360f * Time.deltaTime);
		}
	}

	private void OnItemEndMove(int index)
	{
		m_txtTitle.text = _wordList[index - 1];
		MainUIScript.Instance.m_DictionaryCurrentIndex = index;
	}

	private void SetContent(Text desc, WordInfo info)
	{
		string text = "<size=50>" + info.word + "</size>\n\n";
		for (int i = 0; i < info.Desc.Count; i++)
		{
			text += info.Desc[i];
			if (i < info.Desc.Count - 1)
			{
				text += "\n";
			}
		}
		desc.text = text;
		desc.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
	}

	private void Awake()
	{
		m_objLoading.SetActive(value: false);
		_wordList = new List<string>();
		_wordList.AddRange(_tmpWord.Split(','));
		if (_wordList.Count > 1)
		{
			mCurrentIndex = MainUIScript.Instance.m_DictionaryCurrentIndex;
		}
		else
		{
			mCurrentIndex = 1;
		}
		List<string> list = new List<string>();
		for (int i = 0; i < _wordList.Count; i++)
		{
			if (!PlayerInfo.Instance.WordInfoList.ContainsKey(_wordList[i]))
			{
				list.Add(_wordList[i]);
			}
		}
		if (list.Count > 0)
		{
			m_txtTitle.text = _wordList[mCurrentIndex - 1];
			if (_wordList.Count > 1)
			{
				for (int j = 0; j < _wordList.Count; j++)
				{
					GameObject gameObject = UnityEngine.Object.Instantiate(m_objItem);
					gameObject.SetActive(value: true);
					gameObject.transform.SetParent(m_tfItemRoot, worldPositionStays: false);
				}
			}
			GetWordInfo(WordGlobal.ListToStr(list));
		}
		else
		{
			Init();
		}
	}

	private void Init()
	{
		if (_wordList.Count > 1 && m_tfItemRoot != null)
		{
			if (m_tfItemRoot.childCount > 0)
			{
				for (int i = 0; i < _wordList.Count; i++)
				{
					GameObject gameObject = m_tfItemRoot.GetChild(i).gameObject;
					SetContent(gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>(), PlayerInfo.Instance.WordInfoList[_wordList[i]]);
				}
			}
			else
			{
				for (int j = 0; j < _wordList.Count; j++)
				{
					GameObject gameObject2 = UnityEngine.Object.Instantiate(m_objItem);
					gameObject2.SetActive(value: true);
					gameObject2.transform.SetParent(m_tfItemRoot, worldPositionStays: false);
					SetContent(gameObject2.transform.GetChild(0).GetChild(0).GetComponent<Text>(), PlayerInfo.Instance.WordInfoList[_wordList[j]]);
				}
			}
			m_SCS.m_ItemCount = _wordList.Count;
			m_SCS.m_CurrentIndex = mCurrentIndex;
			m_SCS.Init();
			m_txtTitle.text = _wordList[mCurrentIndex - 1];
		}
		else if (m_txtText != null)
		{
			SetContent(m_txtText, PlayerInfo.Instance.WordInfoList[_tmpWord]);
			m_txtTitle.text = _tmpWord;
		}
	}

	private void InitDots(int count, int index)
	{
		for (int i = 0; i < count; i++)
		{
		}
	}

	public static void ShowUI(string word = null)
	{
		_tmpWord = word;
		if (word.IndexOf(",") >= 0)
		{
			UIManager.Instance.LoadMainUI("UI/Main/DictionaryUI", isPop: true);
		}
		else
		{
			UIManager.Instance.LoadMainUI("UI/Main/DictionaryOneWordUI", isPop: true);
		}
	}

	private void GetWordInfo(string word)
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			WordInfoCallbackEvent(null);
			return;
		}
		_isLoading = true;
		m_objLoading.SetActive(_isLoading);
		JsonData jsonData = new JsonData();
		jsonData["uuid"] = PlayerInfo.Instance.UUID;
		jsonData["word"] = word;
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["queryword"] = WordInfoCallbackEvent;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "queryword", jsonData, dictionary);
	}

	private bool WordInfoCallbackEvent(JsonData jsonData)
	{
		_isLoading = false;
		if (m_objLoading != null)
		{
			m_objLoading.SetActive(_isLoading);
		}
		if (jsonData != null)
		{
			JsonData jsonData2 = jsonData["wordlist"];
			for (int i = 0; i < jsonData2.Count; i++)
			{
				WordInfo wordInfo = new WordInfo();
				wordInfo.title = jsonData2[i]["title"].ToString();
				wordInfo.word = jsonData2[i]["word"].ToString();
				wordInfo.Desc = new List<string>();
				JsonData jsonData3 = jsonData2[i]["text"];
				for (int j = 0; j < jsonData3.Count; j++)
				{
					wordInfo.Desc.Add(jsonData3[j].ToString());
				}
				PlayerInfo.Instance.WordInfoList.Add(wordInfo.title, wordInfo);
			}
			if (jsonData2.Count > 0)
			{
				Init();
			}
		}
		else
		{
			m_TxtNet.gameObject.SetActive(value: true);
			m_TxtNet.text = WordGlobal.ReadText(1230);
		}
		return true;
	}

	private void OnDestroy()
	{
		ScrollControlScript.OnEndMove = (UnityAction<int>)Delegate.Remove(ScrollControlScript.OnEndMove, new UnityAction<int>(OnItemEndMove));
	}
}
