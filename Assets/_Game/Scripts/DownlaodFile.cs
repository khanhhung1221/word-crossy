using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DownlaodFile : MonoBehaviour
{
	private static DownlaodFile instance;

	public Dictionary<string, UnityWebRequest> listRequest = new Dictionary<string, UnityWebRequest>();

	public static DownlaodFile _Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new GameObject("Download").AddComponent<DownlaodFile>();
			}
			return instance;
		}
	}

	public _DownloadHandler StartDownload(string url, string savePath)
	{
		if (listRequest.ContainsKey(url))
		{
			Debug.Log("下载列表已经存在路径=>" + url);
			return null;
		}
		_DownloadHandler downloadHandler = new _DownloadHandler(savePath);
		UnityWebRequest unityWebRequest = UnityWebRequest.Get(url);
		unityWebRequest.chunkedTransfer = true;
		unityWebRequest.disposeDownloadHandlerOnDispose = true;
		unityWebRequest.SetRequestHeader("Range", "bytes=" + downloadHandler.DownedLength + "-");
		unityWebRequest.downloadHandler = downloadHandler;
		unityWebRequest.SendWebRequest();
		listRequest.Add(url, unityWebRequest);
		return downloadHandler;
	}

	public void StopDownload(string url)
	{
		UnityWebRequest value = null;
		if (!listRequest.TryGetValue(url, out value))
		{
			Debug.Log("不存在下载的请求=>" + url);
			return;
		}
		listRequest.Remove(url);
		(value.downloadHandler as _DownloadHandler).OnDispose();
		value.Abort();
		value.Dispose();
	}

	private void Update()
	{
		List<string> list = new List<string>();
		foreach (string key in listRequest.Keys)
		{
			UnityWebRequest unityWebRequest = listRequest[key];
			if (unityWebRequest.isDone)
			{
				Debug.Log(unityWebRequest.responseCode);
				unityWebRequest.Dispose();
				list.Add(key);
			}
		}
		for (int i = 0; i < list.Count; i++)
		{
			listRequest.Remove(list[i]);
		}
		list.Clear();
	}

	private void OnApplicationQuit()
	{
		foreach (string key in listRequest.Keys)
		{
			(listRequest[key].downloadHandler as _DownloadHandler).OnDispose();
			listRequest[key].Dispose();
		}
		listRequest.Clear();
	}
}
