using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour
{
	private struct myLine
	{
		public Vector3 StartPoint;

		public Vector3 EndPoint;
	}

	private LineRenderer line;

	private bool isMousePressed;

	private List<Vector3> pointsList;

	private Vector3 mousePos;

	private void Awake()
	{
		line = base.gameObject.AddComponent<LineRenderer>();
		line.material = new Material(Shader.Find("Particles/Additive"));
		line.SetVertexCount(0);
		line.SetWidth(0.1f, 0.1f);
		line.SetColors(Color.green, Color.green);
		line.useWorldSpace = true;
		isMousePressed = false;
		pointsList = new List<Vector3>();
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			isMousePressed = true;
			line.SetVertexCount(0);
			pointsList.RemoveRange(0, pointsList.Count);
			line.SetColors(Color.green, Color.green);
		}
		else if (Input.GetMouseButtonUp(0))
		{
			isMousePressed = false;
		}
		if (!isMousePressed)
		{
			return;
		}
		mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousePos.z = 0f;
		if (!pointsList.Contains(mousePos))
		{
			pointsList.Add(mousePos);
			line.SetVertexCount(pointsList.Count);
			line.SetPosition(pointsList.Count - 1, pointsList[pointsList.Count - 1]);
			if (isLineCollide())
			{
				isMousePressed = false;
				line.SetColors(Color.red, Color.red);
			}
		}
	}

	private bool isLineCollide()
	{
		if (pointsList.Count < 2)
		{
			return false;
		}
		int num = pointsList.Count - 1;
		myLine[] array = new myLine[num];
		if (num > 1)
		{
			for (int i = 0; i < num; i++)
			{
				array[i].StartPoint = pointsList[i];
				array[i].EndPoint = pointsList[i + 1];
			}
		}
		myLine l = default(myLine);
		for (int j = 0; j < num - 1; j++)
		{
			l.StartPoint = pointsList[pointsList.Count - 2];
			l.EndPoint = pointsList[pointsList.Count - 1];
			if (isLinesIntersect(array[j], l))
			{
				return true;
			}
		}
		return false;
	}

	private bool checkPoints(Vector3 pointA, Vector3 pointB)
	{
		return pointA.x == pointB.x && pointA.y == pointB.y;
	}

	private bool isLinesIntersect(myLine L1, myLine L2)
	{
		if (checkPoints(L1.StartPoint, L2.StartPoint) || checkPoints(L1.StartPoint, L2.EndPoint) || checkPoints(L1.EndPoint, L2.StartPoint) || checkPoints(L1.EndPoint, L2.EndPoint))
		{
			return false;
		}
		return Mathf.Max(L1.StartPoint.x, L1.EndPoint.x) >= Mathf.Min(L2.StartPoint.x, L2.EndPoint.x) && Mathf.Max(L2.StartPoint.x, L2.EndPoint.x) >= Mathf.Min(L1.StartPoint.x, L1.EndPoint.x) && Mathf.Max(L1.StartPoint.y, L1.EndPoint.y) >= Mathf.Min(L2.StartPoint.y, L2.EndPoint.y) && Mathf.Max(L2.StartPoint.y, L2.EndPoint.y) >= Mathf.Min(L1.StartPoint.y, L1.EndPoint.y);
	}
}
