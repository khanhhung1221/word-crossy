using System;
using System.Globalization;
using System.Text;

public class EmojiDeCode
{
	public static string GetEmoji(string paramContent)
	{
		string text = paramContent.Replace("[e]", "\\u").Replace("[/e]", string.Empty);
		char[] array = new char[6]
		{
			'0',
			'0',
			'0',
			'0',
			'0',
			'0'
		};
		StringBuilder stringBuilder = new StringBuilder(2000);
		int length = paramContent.Length;
		for (int i = 0; i < length; i++)
		{
			int num = i;
			try
			{
				if (paramContent[num] != '[')
				{
					goto IL_0128;
				}
				StringBuilder stringBuilder2 = new StringBuilder(20);
				StringBuilder tmps = new StringBuilder(5);
				if (paramContent[num + 1] != 'e' || paramContent[num + 2] != ']')
				{
					goto IL_0128;
				}
				num += 3;
				num = ChangUnicodeToUTF16(paramContent, stringBuilder2, tmps, num);
				if (paramContent[num] == '-')
				{
					num++;
					num = ChangUnicodeToUTF16(paramContent, stringBuilder2, tmps, num);
				}
				if (paramContent[num] != '[' || paramContent[num + 1] != '/' || paramContent[num + 2] != 'e' || paramContent[num + 3] != ']')
				{
					goto IL_0128;
				}
				num += 3;
				i = num;
				stringBuilder.Append(stringBuilder2.ToString());
				goto end_IL_0050;
				IL_0128:
				i = num;
				goto IL_0138;
				end_IL_0050:;
			}
			catch (Exception)
			{
				goto IL_0138;
			}
			continue;
			IL_0138:
			stringBuilder.Append(paramContent[i]);
		}
		return stringBuilder.ToString();
	}

	public static int ChangUnicodeToUTF16(string paramContent, StringBuilder tempSB, StringBuilder tmps, int i)
	{
		for (int j = 0; j < 20; j++)
		{
			if (paramContent[i] == '-')
			{
				break;
			}
			if (paramContent[i] == '[')
			{
				break;
			}
			tmps.Append(paramContent[i]);
			i++;
		}
		tempSB.Append(EmojiCodeToUTF16String(tmps.ToString()));
		return i;
	}

	public static int EmojiToUTF16(int V, bool LowHeight = true)
	{
		int num = V - 65536;
		int num2 = num >> 10;
		int num3 = num & 0x3FF;
		int num4 = 55296;
		int num5 = 56320;
		num4 |= num2;
		num5 |= num3;
		if (LowHeight)
		{
			return (num5 << 16) | num4;
		}
		return (num4 << 16) | num5;
	}

	public static string EmojiCodeToUTF16String(string EmojiCode)
	{
		if (EmojiCode.Length != 4 && EmojiCode.Length != 5)
		{
			throw new ArgumentException("错误的 EmojiCode 16进制数据长度.一般为4位或5位");
		}
		int utf = int.Parse(EmojiCode, NumberStyles.HexNumber);
		return char.ConvertFromUtf32(utf);
	}
}
