public class EmojiList
{
	public static string GrinningFace = "\ud83d\ude00";

	public static string FaceWithTearsOfJoy = "\ud83d\ude02";

	public static string WinkingFace = "\ud83d\ude09";

	public static string CryingFace = "\ud83d\ude22";

	public static string LoudlyCryingFace = "\ud83d\ude2d";

	public static string Skull = "\ud83d\udc80";

	public static string Alien = "\ud83d\udc7d";

	public static string Man = "\ud83d\udc68";

	public static string Woman = "\ud83d\udc69";

	public static string BabyAngel = "\ud83d\udc7c";

	public static string SantaClaus = "\ud83c\udf85";

	public static string MrsClaus = "\ud83e\udd36";

	public static string Mage = "\ud83e\uddd9";

	public static string FlexedBiceps = "\ud83d\udcaa";

	public static string BackhandIndexPointingLeft = "\ud83d\udc48";

	public static string BackhandIndexPointingUp = "\ud83d\udc46";

	public static string VictoryHand = "✌";

	public static string ThumbsUp = "\ud83d\udc4d";

	public static string OncomingFist = "\ud83d\udc4a";

	public static string KissMark = "\ud83d\udc8b";

	public static string RedHeart = "❤";

	public static string Zzz = "\ud83d\udca4";

	public static string Bomb = "\ud83d\udca3";

	public static string GemStone = "\ud83d\udc8e";

	public static string BirthdayCake = "\ud83c\udf82";

	public static string GlobeWithMeridians = "\ud83c\udf10";

	public static string House = "\ud83c\udfe0";

	public static string Castle = "\ud83c\udff0";

	public static string Automobile = "\ud83d\ude97";

	public static string GlowingStar = "\ud83c\udf1f";

	public static string Sparkles = "✨";

	public static string WrappedGift = "\ud83c\udf81";

	public static string Trophy = "\ud83c\udfc6";

	public static string MilitaryMedal = "\ud83c\udf96";

	public static string SportsMedal = "\ud83c\udfc5";

	public static string MoneyBag = "\ud83d\udcb0";

	public static string HeavyDollarSign = "\ud83d\udcb2";
}
