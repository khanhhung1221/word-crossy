using System;
using System.Collections.Generic;

[Serializable]
public class EventDataCsv
{
	public int eventid;

	public int type;

	public int starttime;

	public int endtime;

	public int startlv;

	public int endlv;

	public string require;

	public int requireamount;

	public string item1;

	public string item2;

	public string item3;

	public string item4;

	public string other;

	public List<string> items = new List<string>();

	public EventDataCsv(string[] values, int valueIndex)
	{
		int num = 0;
		items.Clear();
		eventid = int.Parse(values[num++]);
		type = int.Parse(values[num++]);
		starttime = int.Parse(values[num++]);
		endtime = int.Parse(values[num++]);
		startlv = int.Parse(values[num++]);
		endlv = int.Parse(values[num++]);
		require = values[num++];
		requireamount = int.Parse(values[num++]);
		item1 = values[num++];
		item2 = values[num++];
		item3 = values[num++];
		item4 = values[num++];
		other = values[num++];
		AddItem(item1);
		AddItem(item2);
		AddItem(item3);
		AddItem(item4);
	}

	public void AddItem(string item)
	{
		if (item.Contains("_"))
		{
			items.Add(item);
		}
	}
}
