using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class EventDataManagerCsv : ManagerCsv
{
	private static EventDataManagerCsv inst;

	private Dictionary<int, EventDataCsv> Data_dic = new Dictionary<int, EventDataCsv>();

	public static EventDataManagerCsv instance => inst ?? (inst = new EventDataManagerCsv());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			int valueIndex = 0;
			MemoryStream stream = new MemoryStream(file);
			Data_dic = new Dictionary<int, EventDataCsv>();
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								EventDataCsv eventDataCsv = new EventDataCsv(array, valueIndex);
								Data_dic[eventDataCsv.eventid] = eventDataCsv;
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public void ReInit()
	{
		if (Data_dic == null || Data_dic.Count == 0)
		{
			ConfigDataManager.Instance.ReInitCsv("event", this);
		}
	}

	public EventDataCsv GetDataById(int id)
	{
		ReInit();
		if (Data_dic.ContainsKey(id))
		{
			return Data_dic[id];
		}
		return null;
	}

	public EventDataCsv GetDataByType(int type)
	{
		ReInit();
		foreach (int key in Data_dic.Keys)
		{
			EventDataCsv eventDataCsv = Data_dic[key];
			if (eventDataCsv.type == type)
			{
				return eventDataCsv;
			}
		}
		return null;
	}

	public int GetIdByMaxLevel(int level, int type)
	{
		ReInit();
		foreach (int key in Data_dic.Keys)
		{
			EventDataCsv eventDataCsv = Data_dic[key];
			if (eventDataCsv.type == type && level >= eventDataCsv.startlv && level <= eventDataCsv.endlv)
			{
				return eventDataCsv.eventid;
			}
		}
		return 0;
	}

	public int GetMinLevel(int type)
	{
		ReInit();
		int num = 99999;
		if (Data_dic == null)
		{
			return 6;
		}
		foreach (int key in Data_dic.Keys)
		{
			EventDataCsv eventDataCsv = Data_dic[key];
			if ((eventDataCsv.type == type || type == 99) && num > eventDataCsv.startlv)
			{
				num = eventDataCsv.startlv;
			}
		}
		if (num == 99999)
		{
			num = 6;
		}
		return num;
	}
}
