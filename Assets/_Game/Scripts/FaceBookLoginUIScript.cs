using System;
using UnityEngine;

public class FaceBookLoginUIScript : MonoBehaviour
{
	public static FaceBookLoginUIScript Instance;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		if (PlayerInfo.Instance.PopUpFBNum < 3)
		{
			PlayerInfo.Instance.PopUpFBNum++;
		}
		PlayerInfo.Instance.LastPopUpFBUtcTime = DateTime.UtcNow.Day;
	}

	private void Update()
	{
	}

	public void BtnFBClick()
	{
		WordFaceBook.Instance.FaceBookLogin();
	}

	public void BtnCloseClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}
}
