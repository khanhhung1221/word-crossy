using UnityEngine;
using UnityEngine.UI;

public class FirstGetBonusUI : MonoBehaviour
{
	public RectTransform img_rect;

	private int index;

	private void Start()
	{
		if (MainUIScript.Instance != null)
		{
			index = MainUIScript.Instance.m_ObjBtnBonus.transform.GetSiblingIndex();
			MainUIScript.Instance.m_ObjBtnBonus.transform.SetParent(base.transform, worldPositionStays: true);
		}
		if (WordGlobal.isMainUIShowBanner)
		{
			RectTransform rectTransform = img_rect;
			Vector2 anchoredPosition = img_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, -265f + WordGlobal.showBanner_bottomupy);
		}
		else
		{
			RectTransform rectTransform2 = img_rect;
			Vector2 anchoredPosition2 = img_rect.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, -265f);
		}
	}

	private void OnDestroy()
	{
		if (MainUIScript.Instance != null)
		{
			MainUIScript.Instance.m_ObjBtnBonus.transform.SetParent(MainUIScript.Instance.transform, worldPositionStays: true);
			MainUIScript.Instance.m_ObjBtnBonus.transform.SetSiblingIndex(index);
			MainUIScript.Instance.m_ObjBtnBonus.GetComponent<Image>().enabled = true;
			MainUIScript.Instance.m_ObjBtnBonus.GetComponent<Button>().enabled = true;
			MainUIScript.Instance.m_ObjBtnBonus.GetComponent<Animator>().enabled = true;
		}
	}
}
