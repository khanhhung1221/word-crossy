using UnityEngine;

public class GameObjectAutoDestory : MonoBehaviour
{
	public float m_Time = 0.5f;

	private void Start()
	{
	}

	private void Update()
	{
		m_Time -= Time.deltaTime;
		if (m_Time < 0f)
		{
			Object.Destroy(base.gameObject);
		}
	}
}
