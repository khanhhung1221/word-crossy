using LitJson;
using System.Collections.Generic;

public class GameSaveData
{
	public List<string> Words;

	public string CurrentTipsWord;

	public List<List<string>> Grid;

	public string GridStr;

	public List<string> BonusList;

	public int Level;

	public int CurrentScore;

	public int ComboScore;

	public string ButterFlyPos;

	public string ButterFlyWord;

	public int CurButterScore;

	public int CurStarNums;

	public int IsDailyPuzzleFinished;

	public int ButterFlyTVCount;

	public string HDSunPos;

	public int CurSunScore;

	public List<string> DictList;

	public GameSaveData()
	{
		CurrentTipsWord = string.Empty;
		Words = new List<string>();
		Grid = new List<List<string>>();
		BonusList = new List<string>();
		CurrentScore = 0;
		Level = 0;
		ComboScore = 0;
		ButterFlyPos = string.Empty;
		ButterFlyWord = string.Empty;
		CurButterScore = 0;
		CurStarNums = 0;
		ButterFlyTVCount = 0;
		IsDailyPuzzleFinished = 0;
		DictList = new List<string>();
	}

	public void InitGrid()
	{
		List<object> list = JsonMapper.Deserialize(GridStr) as List<object>;
		Grid = new List<List<string>>();
		if (list == null)
		{
			return;
		}
		for (int i = 0; i < list.Count; i++)
		{
			List<object> list2 = (List<object>)list[i];
			List<string> list3 = new List<string>();
			for (int j = 0; j < list2.Count; j++)
			{
				list3.Add(list2[j].ToString());
			}
			Grid.Add(list3);
		}
	}

	public bool CheckGrid()
	{
		if (Grid != null)
		{
			for (int i = 0; i < Grid.Count; i++)
			{
				for (int j = 0; j < Grid[i].Count; j++)
				{
					if (Grid[i][j] != "-" && Grid[i][j] != string.Empty)
					{
						return true;
					}
				}
			}
		}
		return false;
	}
}
