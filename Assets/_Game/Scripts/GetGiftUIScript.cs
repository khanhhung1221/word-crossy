using UnityEngine;
using UnityEngine.UI;

public class GetGiftUIScript : MonoBehaviour
{
	public Image GiftBgImg;

	public Image GiftIconImg;

	public Text GiftNumText;

	private MailItemInfo mailII;

	public void BtnDoneClick()
	{
		Object.Destroy(base.gameObject);
	}

	public void SetInfo(MailItemInfo mi)
	{
		mailII = mi;
		GiftNumText.text = "X" + mi.ItemNum.ToString();
		switch (mi.ItemID)
		{
		case 1001:
		case 1002:
		case 1003:
			GiftIconImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/" + mi.ItemID);
			GiftIconImg.gameObject.SetActive(value: true);
			break;
		case 1004:
			GiftBgImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/coin");
			GiftIconImg.gameObject.SetActive(value: false);
			break;
		}
	}

	private void Start()
	{
	}

	private void Update()
	{
	}
}
