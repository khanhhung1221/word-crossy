using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class GetGoldEff : MonoBehaviour
{
	public Vector3 end_pos;

	private float wait_move;

	private float moveTime;

	private float delay_time;

	private UnityAction callback;

	private float destory_time;

	private void Start()
	{
	}

	public void Init(Vector3 end_pos, float delay_time = 0f, float moveTime = 0.5f, UnityAction callback = null)
	{
		this.end_pos = end_pos;
		this.moveTime = moveTime;
		this.delay_time = delay_time;
		this.callback = callback;
		if (delay_time <= 0f)
		{
			StartAnim();
		}
	}

	public void StartAnim()
	{
		wait_move = 0.01f;
	}

	private void Update()
	{
		if (delay_time > 0f)
		{
			delay_time -= Time.deltaTime;
			if (delay_time <= 0f)
			{
				StartAnim();
			}
		}
		if (wait_move > 0f)
		{
			wait_move -= Time.deltaTime;
			if (wait_move <= 0f)
			{
				Vector3[] path = WordGlobal.CountCirclePoints(base.transform.position, end_pos);
				base.transform.DOPath(path, moveTime).OnComplete(delegate
				{
					if (callback != null)
					{
						callback();
					}
					Object.Destroy(base.gameObject);
				});
			}
		}
	}
}
