using UnityEngine;
using UnityEngine.UI;

public class GoldItem : MonoBehaviour
{
	public GameObject anim_go;

	public Canvas canvas;

	public Image img;

	private float DelayTime;

	private bool isEnd;

	private void Start()
	{
	}

	public void Init(float delay_time, bool isEnd)
	{
		this.isEnd = isEnd;
		anim_go.SetActive(value: false);
		DelayTime = delay_time;
	}

	public void OnlyImg(int canvasnum)
	{
		img.enabled = true;
		canvas.sortingOrder = canvasnum;
		anim_go.SetActive(value: false);
	}

	private void Update()
	{
		if (!(DelayTime > 0f))
		{
			return;
		}
		DelayTime -= Time.deltaTime;
		if (DelayTime <= 0f)
		{
			if (isEnd)
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_MainUIGoldList[1]);
			}
			else
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_MainUIGoldList[0]);
			}
			anim_go.SetActive(value: true);
		}
	}
}
