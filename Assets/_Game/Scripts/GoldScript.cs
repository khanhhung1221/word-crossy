using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class GoldScript : MonoBehaviour
{
	public Text m_TxtGold;

	public GameObject m_ObjGold;

	public bool m_ShowBanner;

	private long _gold;

	public bool _isAddGold;

	private List<string> m_ListComm;

	private List<string> m_ListToday;

	private string m_Sprite200 = "shop_cuxiao_word200%";

	private string m_Sprite300 = "shop_cuxiao_word300%";

	private int addNUmPerFps = 1;

	private string _path = "Image/Shop/";

	public GameObject banner_go;

	private void Start()
	{
		m_ObjGold = base.transform.Find("GoldIcon").gameObject;
		PlayerInfo.GetOfferEvent += GetPopOfferEvent;
		PayManager.PaymentVerifyEvnet += PayManager_PaymentVerifyEvnet;
	}

	private void OnDestroy()
	{
		PayManager.PaymentVerifyEvnet -= PayManager_PaymentVerifyEvnet;
		PlayerInfo.GetOfferEvent -= GetPopOfferEvent;
	}

	private void PayManager_PaymentVerifyEvnet(Product arg0)
	{
		ShowBanner();
	}

	public void BtnLoadShop()
	{
		if (!(GameObject.Find("ShopUI") == null))
		{
			return;
		}
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			ResourceLoadManager.GetInstance().ShowTips(1118);
			return;
		}
		UIManager.Instance.LoadMainUI("UI/ShopUI").name = "ShopUI";
		if (MainUIScript.Instance != null && WordGlobal.isMainUIShowBanner)
		{
			AdsManager.Instance.HideBannerAD();
		}
	}

	public void ShowBanner()
	{
		if (!m_ShowBanner)
		{
			return;
		}
		if (PlayerInfo.Instance.CurrentOfferItem != null)
		{
			Transform transform = base.transform.Find("GoldBanner");
			if (transform == null)
			{
				banner_go = ResourceLoadManager.GetInstance().LoadObjByPath("UI/OfferBanner");
			}
			else
			{
				banner_go = transform.gameObject;
			}
			try
			{
				banner_go.transform.SetParent(base.transform, worldPositionStays: false);
				banner_go.name = "GoldBanner";
				banner_go.SetActive(value: true);
				if (PlayerInfo.Instance.CurrentOfferItem.Value == "200")
				{
					banner_go.transform.Find("200").GetComponent<Image>().sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>(_path + m_Sprite200);
				}
				else if (PlayerInfo.Instance.CurrentOfferItem.Value == "300")
				{
					banner_go.transform.Find("200").GetComponent<Image>().sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>(_path + m_Sprite300);
				}
				banner_go.transform.Find("300").GetComponent<Image>().sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>(_path + GetOfferImgName());
			}
			catch (Exception)
			{
				throw;
			}
		}
		else
		{
			GameObject gameObject = GameObject.Find("GoldBanner");
			if (gameObject != null)
			{
				UnityEngine.Object.Destroy(gameObject);
			}
		}
	}

	private void Awake()
	{
		_gold = PlayerInfo.Instance.Gold;
		m_TxtGold.text = PlayerInfo.Instance.Gold.ToString();
		m_ListComm = new List<string>();
		m_ListComm.Add("shop_cuxiao_wordLIMITED");
		m_ListComm.Add("shop_cuxiao_wordSALE");
		m_ListComm.Add("shop_cuxiao_wordSPECIAL");
		m_ListToday = new List<string>();
		m_ListToday.Add("shop_cuxiao_wordHOT-TODAY");
		m_ListToday.Add("shop_cuxiao_wordTODAY-ONLY");
		ShowBanner();
	}

	private string GetOfferImgName()
	{
		string @string = PlayerPrefs.GetString("Gold.OfferImg", string.Empty);
		if (@string != string.Empty)
		{
			return @string;
		}
		List<string> list = new List<string>();
		if ((PlayerInfo.Instance.OfferEndTime - DateTime.Now).TotalHours > 24.0)
		{
			list.AddRange(m_ListToday);
		}
		list.AddRange(m_ListComm);
		@string = list[UnityEngine.Random.Range(0, list.Count)];
		PlayerPrefs.SetString("Gold.OfferImg", @string);
		return @string;
	}

	private void Update()
	{
		if (_isAddGold)
		{
			m_TxtGold.text = _gold.ToString();
		}
		else
		{
			m_TxtGold.text = PlayerInfo.Instance.Gold.ToString();
		}
	}

	public void ResetGold()
	{
		_gold = PlayerInfo.Instance.Gold;
		m_TxtGold.text = PlayerInfo.Instance.Gold.ToString();
		ShowBanner();
	}

	public void AddGold(int count, float delayTime, float animTotalTime = 1f, bool isAdd2PlayerInfo = true, int dyindex = 0)
	{
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_GetCoin);
		if (!_isAddGold)
		{
			_gold = PlayerInfo.Instance.Gold;
		}
		_isAddGold = true;
        m_TxtGold.transform.DOScale(1.3f, 0.2f);
        m_TxtGold.color = new Color(0.9921569f, 251f / 255f, 31f / 85f);
		if (animTotalTime == 0f)
		{
			addNUmPerFps = 1;
			StartCoroutine(AddGoldAni(count, delayTime));
			return;
		}
		float num = 30f;
		addNUmPerFps = (int)((float)count / animTotalTime / num);
		if (addNUmPerFps < 3)
		{
			addNUmPerFps = 3;
		}
		StartCoroutine(AddGoldAni01(count, delayTime, isAdd2PlayerInfo, dyindex));
	}

	private IEnumerator AddGoldAni(int count, float delayTime)
	{
		PlayerInfo.Instance.AddGold(count, 12);
		yield return new WaitForSeconds(delayTime);
		float waitTime = 1f / (float)count;
		if (waitTime > 0.5f)
		{
		}
		for (int i = 0; i < count; i++)
		{
			_gold++;
			m_TxtGold.text = _gold.ToString();
			yield return new WaitForSeconds(1f / (float)count);
		}
		m_TxtGold.DOColor(new Color(1f, 1f, 1f), 0.2f);
        m_TxtGold.transform.DOScale(1f, 0.2f);
        _isAddGold = false;
	}

	private IEnumerator AddGoldAni01(int count, float delayTime, bool isAdd2PlayerInfo = true, int dyindex = 0)
	{
		if (isAdd2PlayerInfo)
		{
			PlayerInfo.Instance.AddGold(count, 1300 + dyindex);
		}
		yield return new WaitForSeconds(delayTime);
		long total_gold = PlayerInfo.Instance.Gold;
		float start_time = Time.realtimeSinceStartup;
		do
		{
			_gold += addNUmPerFps;
			if (_gold >= total_gold)
			{
				_gold = total_gold;
			}
			m_TxtGold.text = _gold.ToString();
			yield return new WaitForSeconds(Time.deltaTime);
		}
		while (_gold < total_gold);
		float num = Time.realtimeSinceStartup - start_time;
		m_TxtGold.DOColor(new Color(1f, 1f, 1f), 0.2f);
        m_TxtGold.transform.DOScale(1f, 0.2f);
        _isAddGold = false;
	}

	public void CollectGold(Vector3 starPos, int count, float moveTime, UnityAction callback)
	{
		CollectGoldNew(starPos, 1, moveTime, callback);
	}

	public void CollectGoldNew(Vector3 starPos, int count, float moveTime, UnityAction callback)
	{
		base.transform.DOComplete(withCallbacks: true);
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_GetCoin);
		for (int i = 0; i < count; i++)
		{
			GameObject gameObject = ResourceLoadManager.GetInstance().LoadObjByPath("UI/Main/GetGoldEff");
			gameObject.transform.SetParent(UIManager.Instance.transform, worldPositionStays: false);
			gameObject.transform.position = starPos;
			GetGoldEff component = gameObject.GetComponent<GetGoldEff>();
			if (i < count - 1)
			{
				component.Init(m_ObjGold.transform.position, (float)i * 0.2f, moveTime);
			}
			else
			{
				component.Init(m_ObjGold.transform.position, (float)i * 0.2f, moveTime, callback);
			}
		}
	}

	private void OnEnable()
	{
		ShowBanner();
	}

	private void GetPopOfferEvent()
	{
		ShowBanner();
	}
}
