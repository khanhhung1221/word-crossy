using System.Collections.Generic;
using UnityEngine;

public class GuideLineDraw : MonoBehaviour
{
	public static bool isNeedGuideLine;

	private static int guideLetterShowState;

	public List<Vector3> pointsList;

	public LineRenderer lineRenderer;

	private int layerOrder;

	public bool isShow;

	public List<GuidePointInfo> gpis = new List<GuidePointInfo>();

	public int cur_guideLetterShowState;

	private float wrong_count_cd;

	private int SHOW_WAIT_TIME = 30;

	private int MAXLEVEL_SHOW = 18;

	private int WRONG_CD = 20;

	public bool isTest;

	public string test_str = string.Empty;

	private static float wait_show_time;

	public static int GuideLetterShowState
	{
		get
		{
			return guideLetterShowState;
		}
		set
		{
			if (isNeedGuideLine)
			{
				guideLetterShowState = value;
				wait_show_time = 0f;
			}
			BtnVideoScript.dp_wait_showTime = 0f;
		}
	}

	private void Start()
	{
		if (!lineRenderer)
		{
			lineRenderer = GetComponent<LineRenderer>();
		}
		lineRenderer.sortingLayerID = layerOrder;
		pointsList = new List<Vector3>();
		GuideLetterShowState = 0;
		WordGlobal.WordSelectWrongCount = 0;
		HideLine();
		wrong_count_cd = WRONG_CD;
	}

	public void ShowLine()
	{
		WordGlobal.WordSelectWrongCount = 0;
		int num = 100;
		string text = string.Empty;
		List<CrossWordItem> unFinishedWordList = MainUIScript.Instance.GetUnFinishedWordList();
		for (int i = 0; i < unFinishedWordList.Count; i++)
		{
			string word = unFinishedWordList[i].Word;
			if (word.Length < num)
			{
				num = word.Length;
				text = word;
			}
		}
		if (string.IsNullOrEmpty(text))
		{
			Debug.LogError("引导选词出现错误");
		}
		if (text.Length > 3)
		{
			GuideLetterShowState = 2;
		}
		else
		{
			SetWord(text);
		}
	}

	public void SetWord(string word)
	{
		gpis.Clear();
		for (int i = 0; i < MainUIScript.Instance.m_TfLettersRoot.childCount; i++)
		{
			Transform child = MainUIScript.Instance.m_TfLettersRoot.GetChild(i);
			GuidePointInfo guidePointInfo = new GuidePointInfo();
			guidePointInfo.letter_pos = child.position;
			if (child.name.Contains("_"))
			{
				string[] array = child.name.Split('_');
				guidePointInfo.letter_id = int.Parse(array[0]);
				guidePointInfo.letter_str = array[1][0];
				gpis.Add(guidePointInfo);
			}
		}
		List<Vector3> list = new List<Vector3>();
		char[] array2 = word.ToUpper().ToCharArray();
		for (int j = 0; j < array2.Length; j++)
		{
			for (int k = 0; k < gpis.Count; k++)
			{
				if (!gpis[k].isUse && gpis[k].letter_str.Equals(array2[j]))
				{
					gpis[k].isUse = true;
					list.Add(gpis[k].letter_pos);
					break;
				}
			}
		}
		SetPoints(list);
	}

	public void SetPoints(List<Vector3> points)
	{
		isShow = true;
		lineRenderer.enabled = true;
		pointsList = points;
	}

	public void HideLine()
	{
		if (isShow)
		{
			isShow = false;
			wrong_count_cd = 0f;
			lineRenderer.enabled = false;
		}
	}

	public void ClearLine()
	{
		pointsList.Clear();
		lineRenderer.positionCount = 0;
	}

	private void Update()
	{
		if (isTest)
		{
			SetWord(test_str);
			isTest = false;
		}
		if (PlayerInfo.Instance.CurrentLevel > MAXLEVEL_SHOW || PlayerInfo.Instance.CurrentLevel <= 4 || UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament || (MainUIScript.Instance != null && MainUIScript.Instance._cellTipsClick))
		{
			return;
		}
		if (!WordGlobal.IsGuideCheck)
		{
			wrong_count_cd = WRONG_CD;
			wait_show_time = 0f;
			return;
		}
		UpdateShowTime();
		UpdateShowState();
		if (isShow)
		{
			UpdateLineShow();
		}
	}

	public void UpdateShowState()
	{
		if (cur_guideLetterShowState != GuideLetterShowState)
		{
			cur_guideLetterShowState = GuideLetterShowState;
			if (GuideLetterShowState == 1)
			{
				ShowLine();
			}
			else if (GuideLetterShowState == 2)
			{
				HideLine();
			}
		}
		wrong_count_cd += Time.deltaTime;
		if (wrong_count_cd >= (float)WRONG_CD)
		{
			if (WordGlobal.WordSelectWrongCount >= 2)
			{
				GuideLetterShowState = 1;
				WordGlobal.WordSelectWrongCount = 0;
			}
		}
		else
		{
			WordGlobal.WordSelectWrongCount = 0;
		}
	}

	public void UpdateShowTime()
	{
		if (!isShow)
		{
			wait_show_time += Time.deltaTime;
			if (wrong_count_cd >= (float)WRONG_CD && wait_show_time > (float)SHOW_WAIT_TIME)
			{
				GuideLetterShowState = 1;
			}
		}
	}

	public void UpdateLineShow()
	{
		if (pointsList == null || pointsList.Count == 0)
		{
			return;
		}
		int count = pointsList.Count;
		lineRenderer.positionCount = 0;
		int num = 0;
		for (int i = 0; i < count - 1; i++)
		{
			for (float num2 = 0f; num2 < 1f; num2 += 0.01f)
			{
				num++;
				Vector3 position = WordGlobal.interpolatedPosition(pointsList[Mathf.Max(0, i - 1)], pointsList[i], pointsList[Mathf.Min(i + 1, count - 1)], pointsList[Mathf.Min(i + 2, count - 1)], num2);
				lineRenderer.positionCount = num;
				lineRenderer.SetPosition(num - 1, position);
			}
		}
	}
}
