using UnityEngine;

public class GuidePointInfo
{
	public int letter_id;

	public char letter_str;

	public Vector3 letter_pos;

	public bool isUse;
}
