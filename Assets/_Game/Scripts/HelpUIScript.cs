using UnityEngine;

public class HelpUIScript : MonoBehaviour
{
	public GameObject[] MapArray;

	public GameObject[] BrightSpotsArray;

	public GameObject LeftBtnObj;

	public GameObject RightBtnObj;

	public GameObject LeftImgObj;

	public GameObject RightImgObj;

	private int CurrentIndex;

	public static HelpUIScript Instance;

	public void SetCurrentIndex(int index)
	{
		CurrentIndex = index;
	}

	private void Awake()
	{
		Instance = this;
	}

	public void BtnCloseClick()
	{
		Object.Destroy(base.gameObject);
	}

	public void BtnLeftClick()
	{
		CurrentIndex--;
		if (CurrentIndex <= 0)
		{
			CurrentIndex = 0;
		}
		ChangeMapByIndex();
		ChangeSpotsByIndex();
		ChangeBtnByIndex();
	}

	public void BtnRightClick()
	{
		CurrentIndex++;
		if (CurrentIndex >= 4)
		{
			CurrentIndex = 4;
		}
		ChangeMapByIndex();
		ChangeSpotsByIndex();
		ChangeBtnByIndex();
	}

	private void ChangeMapByIndex()
	{
		for (int i = 0; i < MapArray.Length; i++)
		{
			if (i == CurrentIndex)
			{
				MapArray[i].SetActive(value: true);
			}
			else
			{
				MapArray[i].SetActive(value: false);
			}
		}
	}

	public void ChangeSpotsByIndex()
	{
		for (int i = 0; i < BrightSpotsArray.Length; i++)
		{
			if (i == CurrentIndex)
			{
				BrightSpotsArray[i].SetActive(value: true);
			}
			else
			{
				BrightSpotsArray[i].SetActive(value: false);
			}
		}
	}

	private void ChangeBtnByIndex()
	{
		switch (CurrentIndex)
		{
		case 0:
			LeftBtnObj.SetActive(value: false);
			RightBtnObj.SetActive(value: true);
			LeftImgObj.SetActive(value: true);
			RightImgObj.SetActive(value: false);
			break;
		case 1:
		case 2:
		case 3:
			LeftBtnObj.SetActive(value: true);
			RightBtnObj.SetActive(value: true);
			LeftImgObj.SetActive(value: false);
			RightImgObj.SetActive(value: false);
			break;
		case 4:
			LeftBtnObj.SetActive(value: true);
			RightBtnObj.SetActive(value: false);
			LeftImgObj.SetActive(value: false);
			RightImgObj.SetActive(value: true);
			break;
		}
	}

	private void Start()
	{
	}

	private void Update()
	{
	}
}
