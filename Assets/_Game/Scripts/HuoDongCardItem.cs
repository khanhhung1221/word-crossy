using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongCardItem : MonoBehaviour
{
	public RectTransform pos_rect;

	public GameObject up_go;

	public Image up_img;

	public GameObject back_go;

	public GameObject back_desgo;

	public GameObject back_collectBtn;

	public Text clime_txt;

	public Text claimed_txt;

	public HuoDongRewardItem item;

	public GameObject huanrao_go;

	public GameObject BigRewardDes;

	public Text BigReward_txt;

	public int getState;

	public Animator anim;

	private Vector2 base_pos;

	private int idx;

	private HuoDongUI hdui;

	private RectTransform rectTran;

	private string open_str;

	private int anim_idx;

	private float wait_time;

	private void Start()
	{
	}

	public void Init(HuoDongUI hdui, int idx)
	{
		this.hdui = hdui;
		this.idx = idx;
		rectTran = (base.transform as RectTransform);
		base_pos = (base.transform as RectTransform).anchoredPosition;
		up_img.sprite = hdui.card_sps[idx];
		clime_txt.text = TextLibraryDataManagerCsv.instance.GetText(1016);
		claimed_txt.text = TextLibraryDataManagerCsv.instance.GetText(1192);
		open_str = string.Empty;
		string value = PlayerInfo.Instance.GetHuoDongSaveData().card_gotItem[idx];
		huanrao_go.SetActive(value: false);
		BigRewardDes.SetActive(value: false);
		if (string.IsNullOrEmpty(value))
		{
			up_go.SetActive(value: true);
			back_go.SetActive(value: false);
			return;
		}
		getState = 1;
		up_go.SetActive(value: false);
		back_go.SetActive(value: true);
		back_desgo.SetActive(value: true);
		back_collectBtn.SetActive(value: false);
		anim.enabled = false;
		SetItem(value);
	}

	public void SetItem(string item_str)
	{
		string[] array = item_str.Split('_');
		int num = int.Parse(array[0]);
		int num2 = 0;
		switch (num)
		{
		case 1:
		{
			int num3 = int.Parse(array[1]);
			num2 = int.Parse(array[2]);
			switch (num3)
			{
			case 1001:
				item.img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_lightbulb");
				item.img.rectTransform.sizeDelta = new Vector2(104f, 104f);
				break;
			case 1002:
				item.img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_finger");
				item.img.rectTransform.sizeDelta = new Vector2(104f, 104f);
				break;
			case 1003:
				item.img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_lightning");
				item.img.rectTransform.sizeDelta = new Vector2(104f, 104f);
				break;
			}
			break;
		}
		case 3:
			num2 = int.Parse(array[1]);
			item.img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/jinbi_dr");
			item.img.rectTransform.sizeDelta = new Vector2(80f, 80f);
			break;
		}
		item.num.text = "+" + num2;
	}

	public void HuXi(int idx)
	{
		if (this.idx == idx)
		{
			anim.enabled = true;
			anim.Play("Card_huxi");
		}
	}

	public void OnClick()
	{
		if (getState != 0 || !hdui.isCanClickCard)
		{
			return;
		}
		open_str = PlayerInfo.Instance.GetHuoDongSaveData().OpenCard(idx);
		if (!string.IsNullOrEmpty(open_str))
		{
			anim_idx = 1;
			anim.Play("Card_stand");
			anim.enabled = false;
			if (open_str.Equals("BigReward"))
			{
				item.img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/HuoDong/Christmas_rewardItem");
				item.img.rectTransform.sizeDelta = new Vector2(100f, 131f);
				item.num.enabled = false;
			}
			else
			{
				SetItem(open_str);
			}
			BigRewardDes.SetActive(value: false);
			back_desgo.SetActive(value: false);
			back_collectBtn.SetActive(value: true);
			hdui.card_bg.SetActive(value: true);
			back_collectBtn.SetActive(value: false);
			hdui.card_bg.transform.SetAsLastSibling();
			rectTran.SetAsLastSibling();
			if (open_str.Equals("BigReward"))
			{
				BigRewardDes.transform.SetAsLastSibling();
			}
			hdui.isCanClickCard = false;
			hdui.isCanHide = false;
		}
		else
		{
			ResourceLoadManager.GetInstance().ShowTips(WordGlobal.ReadText(1303));
		}
	}

	public void OnCollect()
	{
		Debug.Log("Collect");
		rectTran.DOAnchorPos(base_pos, 0.5f);
		pos_rect.DOScale(0.25f, 0.5f);
		anim.enabled = false;
		hdui.isCanClickCard = true;
		hdui.isCanHide = true;
		back_desgo.SetActive(value: true);
		back_collectBtn.SetActive(value: false);
		hdui.card_bg.SetActive(value: false);
		huanrao_go.SetActive(value: false);
		BigRewardDes.SetActive(value: false);
		if (open_str.Equals("BigReward") && PlayerInfo.Instance.GetHuoDongSaveData().GetRewardState == 0)
		{
			hdui.showRewardUITime = 0.1f;
		}
	}

	private void Update()
	{
		if (anim_idx == 1)
		{
			rectTran.DOAnchorPos(Vector2.zero, 0.3f);
			pos_rect.DOScale(1f, 0.3f).OnComplete(delegate
			{
				anim_idx = 2;
			});
			anim_idx = 100;
		}
		else if (anim_idx == 2)
		{
			anim.enabled = true;
			anim.Play("Card_get");
			anim_idx = 3;
		}
		else if (anim_idx == 3)
		{
			wait_time += Time.deltaTime;
			if (wait_time >= 1f)
			{
				wait_time = 0f;
				anim_idx = 100;
				huanrao_go.SetActive(value: true);
				back_collectBtn.SetActive(value: true);
				if (open_str.Equals("BigReward"))
				{
					BigRewardDes.SetActive(value: true);
					BigReward_txt.text = TextLibraryDataManagerCsv.instance.GetText(1317);
				}
			}
		}
		else if (anim_idx != 100)
		{
		}
	}
}
