using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HuoDongGardonAddTime : MonoBehaviour
{
	public static float last_time;

	public Text title_text;

	public GameObject But_go;

	public Text PowerNumText;

	public Text PowerTimeText;

	public Text PowerDesText;

	public Text BtnDesText;

	public Text BtnGoldNumText;

	public GameObject bgnTV_go;

	public RectTransform bg_rect;

	public GameObject Full_go;

	public Text PowerFullNumText;

	public Text PowerFullText;

	public Text BtnCloseText;

	private float update_waitTime;

	private HuoDongGardonSaveData hgsd;

	private void Start()
	{
		update_waitTime = 1f;
		hgsd = PlayerInfo.Instance.GetHuoDongGardonSaveData();
		PowerDesText.text = TextLibraryDataManagerCsv.instance.GetText(1209);
		PowerFullText.text = TextLibraryDataManagerCsv.instance.GetText(1211);
		BtnDesText.text = TextLibraryDataManagerCsv.instance.GetText(1212);
		BtnGoldNumText.text = hgsd.GetaddPowerNeedNum() + string.Empty;
		BtnCloseText.text = TextLibraryDataManagerCsv.instance.GetText(1042);
		ShowBtnTv();
	}

	public void ShowBtnTv()
	{
		if (Full_go.activeInHierarchy)
		{
			bgnTV_go.SetActive(value: false);
			RectTransform rectTransform = bg_rect;
			Vector2 sizeDelta = bg_rect.sizeDelta;
			rectTransform.sizeDelta = new Vector2(sizeDelta.x, 458f);
		}
		else if (AdsManager.Instance.IncentivizedAdState)
		{
			bgnTV_go.SetActive(value: true);
			RectTransform rectTransform2 = bg_rect;
			Vector2 sizeDelta2 = bg_rect.sizeDelta;
			rectTransform2.sizeDelta = new Vector2(sizeDelta2.x, 530f);
		}
		else
		{
			bgnTV_go.SetActive(value: false);
			RectTransform rectTransform3 = bg_rect;
			Vector2 sizeDelta3 = bg_rect.sizeDelta;
			rectTransform3.sizeDelta = new Vector2(sizeDelta3.x, 458f);
		}
	}

	private void Update()
	{
		update_waitTime += Time.deltaTime;
		if (update_waitTime >= 1f)
		{
			if (hgsd.isFull())
			{
				SetFull();
			}
			else
			{
				if (hgsd.PowerNum <= 0)
				{
					title_text.text = TextLibraryDataManagerCsv.instance.GetText(1244);
				}
				else
				{
					title_text.text = TextLibraryDataManagerCsv.instance.GetText(1208);
				}
				PowerNumText.text = hgsd.PowerNum + string.Empty;
				PowerTimeText.text = hgsd.GetNextPowerTimeLeft();
				Full_go.SetActive(value: false);
				But_go.SetActive(value: true);
				if (hgsd.isFull())
				{
					SetFull();
				}
			}
			float num = WordGlobal.GetHuoDongGardonTimeLeft();
			if (num < 0f)
			{
				num = 0f;
				BtnCloseClick();
			}
			update_waitTime = 0f;
		}
		ShowBtnTv();
	}

	public void SetFull()
	{
		title_text.text = TextLibraryDataManagerCsv.instance.GetText(1210);
		Full_go.SetActive(value: true);
		But_go.SetActive(value: false);
		PowerFullNumText.text = hgsd.PowerNum + string.Empty;
	}

	public void BtnCloseClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void BtnBuyClick()
	{
		if (hgsd.isFull())
		{
			BtnCloseClick();
		}
		else if (PlayerInfo.Instance.DecGold(hgsd.GetaddPowerNeedNum()))
		{
			HuoDongGardonUI.RefreshGold = true;
			hgsd.SetFullPower(isBuy: true);
			BtnCloseClick();
			WordGlobal.LogEvent("HuoDongGardon_BuyPower");
		}
	}

	public void BtnTvClick()
	{
		if (AdsManager.Instance.ShowIncentivizedAd("HuoDongGardon_AddPowerWatch"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
		}
	}

	public void RewardADEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
		if (ret)
		{
			hgsd.AddPower(1, isChangeLasetAddPowerTime: false);
		}
	}
}
