using UnityEngine;
using UnityEngine.UI;

public class HuoDongGardonBackLevel : MonoBehaviour
{
	public static bool isHide;

	public Text giveUp_txt;

	public Text yes_txt;

	public Text no_txt;

	private void Start()
	{
		isHide = false;
		giveUp_txt.text = TextLibraryDataManagerCsv.instance.GetText(1318);
		yes_txt.text = TextLibraryDataManagerCsv.instance.GetText(1319);
		no_txt.text = TextLibraryDataManagerCsv.instance.GetText(1320);
	}

	private void Update()
	{
		if (isHide)
		{
			BtnNo();
			isHide = false;
		}
	}

	public void BtnYes()
	{
		HuoDongGardonUI.OpenType = 3;
		if (GameObject.Find("HuoDongGardonUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
		}
		BtnNo();
	}

	public void BtnNo()
	{
		Object.Destroy(base.gameObject);
	}
}
