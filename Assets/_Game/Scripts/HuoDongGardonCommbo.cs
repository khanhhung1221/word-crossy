using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongGardonCommbo : MonoBehaviour
{
	public static int TextID;

	public Text txt;

	public Image bg;

	public RectTransform rect;

	private void Start()
	{
		txt.text = TextLibraryDataManagerCsv.instance.GetText(TextID);
		rect.transform.localScale = Vector3.zero;
		rect.transform.DOScale(Vector3.one, 1f).SetEase(Ease.OutBack).OnComplete(delegate
		{
			txt.DOFade(0f, 0.5f).SetDelay(0.5f);
			bg.DOFade(0f, 0.5f).SetDelay(0.5f);
			rect.DOAnchorPosY(200f, 0.5f).SetDelay(0.5f).OnComplete(delegate
			{
				Object.Destroy(base.gameObject);
			});
		});
	}
}
