using DG.Tweening;
using UnityEngine;

public class HuoDongGardonEndUI : MonoBehaviour
{
	public RectTransform main_rect;

	private void Start()
	{
		main_rect.localScale = Vector3.zero;
		main_rect.DOScale(Vector3.one, 1f).SetEase(Ease.OutBack);
	}

	public void BtnClose()
	{
		Object.Destroy(base.gameObject);
	}

	private void OnDestroy()
	{
		HuoDongGardonUI.isBtnCloseClick = true;
	}
}
