using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongGardonFlower : MonoBehaviour
{
	public Sprite[] bg_sps;

	public Texture2D[] flower_texs;

	public Image bg_img;

	public SkeletonGraphic flower_img;

	public GameObject flower_go;

	private HuoDongGardonUI huodondgardonui;

	private int index;

	private void Start()
	{
	}

	public void Init(int idx, int type, HuoDongGardonUI hdgui)
	{
		index = idx;
		huodondgardonui = hdgui;
		if (index / 4 % 2 == 0)
		{
			if (index % 2 == 0)
			{
				bg_img.sprite = bg_sps[0];
			}
			else
			{
				bg_img.sprite = bg_sps[1];
			}
		}
		else if (index % 2 == 0)
		{
			bg_img.sprite = bg_sps[1];
		}
		else
		{
			bg_img.sprite = bg_sps[0];
		}
		if (index < PlayerInfo.Instance.GetHuoDongGardonSaveData().GetRewardNeedNum(1))
		{
			flower_img.OverrideTexture = flower_texs[0];
		}
		else if (index < PlayerInfo.Instance.GetHuoDongGardonSaveData().GetRewardNeedNum(2))
		{
			flower_img.OverrideTexture = flower_texs[1];
		}
		else if (index < PlayerInfo.Instance.GetHuoDongGardonSaveData().GetRewardNeedNum(3))
		{
			flower_img.OverrideTexture = flower_texs[2];
		}
		int num = index + 1;
		if ((float)num <= huodondgardonui.flowerOpenNum)
		{
			Open();
		}
		else if (num <= huodondgardonui.flowerWillOpenEndNum)
		{
			int num2 = 0;
			float num3 = num2 - (huodondgardonui.flowerWillOpenEndNum - num) - 1;
			OpenAnim(num3 * 0.3f);
		}
		else
		{
			Close();
		}
	}

	public void Close()
	{
		flower_go.SetActive(value: false);
	}

	public void Open()
	{
		PlayStandAnim();
		flower_go.SetActive(value: true);
	}

	public void PlayStandAnim(int trackIdx = 0)
	{
		switch (Random.Range(0, 3))
		{
		case 1:
			flower_img.AnimationState.SetAnimation(trackIdx, "play", loop: true);
			break;
		case 2:
			flower_img.AnimationState.SetAnimation(trackIdx, "play1", loop: true);
			break;
		default:
			flower_img.AnimationState.SetAnimation(trackIdx, "play2", loop: true);
			break;
		}
	}

	public void OpenAnim(float delay_time)
	{
		TrackEntry trackEntry = flower_img.AnimationState.SetAnimation(0, "shengzhang", loop: false);
		trackEntry.Delay = delay_time;
		trackEntry.Complete += OpenAnimEnd;
		flower_go.SetActive(value: true);
	}

	public void OpenAnimEnd(TrackEntry trackEntry)
	{
		PlayStandAnim();
	}

	public void PlayFire()
	{
		TrackEntry trackEntry = flower_img.AnimationState.SetAnimation(0, "fire", loop: false);
		trackEntry.Complete += PlayFireEnd;
	}

	private void PlayFireEnd(TrackEntry trackEntry)
	{
		PlayStandAnim();
	}
}
