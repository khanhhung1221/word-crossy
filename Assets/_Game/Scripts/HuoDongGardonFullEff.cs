using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongGardonFullEff : MonoBehaviour
{
	public static int ShowType;

	public static bool Hide;

	public Image img;

	public int curShowType;

	private void Start()
	{
	}

	private void Update()
	{
		if (curShowType != ShowType)
		{
			curShowType = ShowType;
			if (curShowType == 1)
			{
				img.color = new Color(0f, 0f, 0f, 0.6f);
			}
			else if (curShowType == 2)
			{
				img.color = new Color(1f, 1f, 1f, 0f);
				img.DOFade(0.8f, 0.3f);
			}
			else if (curShowType == 3)
			{
				img.DOFade(0f, 0.1f).OnComplete(delegate
				{
					Object.Destroy(base.gameObject);
				});
			}
		}
		if (Hide)
		{
			img.DOKill();
			Object.Destroy(base.gameObject);
			Hide = false;
		}
	}
}
