using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongGardonGetGold : MonoBehaviour
{
	public static int GoldNum;

	public static string IconStr = string.Empty;

	public static HuoDongGardonUI hdgui;

	public Transform ItemTran;

	public Image icon;

	public Text num;

	public Text title_txt;

	public Text claim_txt;

	private void Start()
	{
		ItemTran.localScale = Vector2.zero;
		ItemTran.DOScale(1f, 0.3f);
		num.text = "+" + GoldNum;
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1232);
		claim_txt.text = TextLibraryDataManagerCsv.instance.GetText(1016);
	}

	private void OnDestroy()
	{
		if (hdgui != null)
		{
			hdgui.GiveGold(GoldNum, ItemTran.position);
			hdgui = null;
		}
	}
}
