using DG.Tweening;
using Spine.Unity;
using UnityEngine;

public class HuoDongGardonMainFlower : MonoBehaviour
{
	public int State;

	public RectTransform rectTran;

	public SkeletonGraphic flower_img;

	public GameObject flower_eff_fly;

	public GameObject flower_eff_shouji;

	public GameObject flower_eff_tx;

	public Vector3 position;

	private int c_type = -1;

	private HuoDongGardonMainUI hdgm;

	private float delay_showTime;

	private int anim_index;

	private float anim_waitTime;

	private Vector3 endPos;

	private int count_index;

	private void Start()
	{
		rectTran = (base.transform as RectTransform);
	}

	public void SetColor(int color_type, HuoDongGardonMainUI hdgm)
	{
		this.hdgm = hdgm;
		anim_index = 0;
		c_type = color_type;
		base.transform.DOKill();
		delay_showTime = 2.5f;
		base.transform.localScale = Vector3.one;
		flower_img.gameObject.SetActive(value: false);
		flower_eff_fly.SetActive(value: false);
		flower_eff_shouji.SetActive(value: false);
		flower_eff_tx.SetActive(value: false);
	}

	public void ShowAnim(Vector3 end_pos, int count_index)
	{
		anim_index = 1;
		endPos = end_pos;
		this.count_index = count_index;
	}

	public void SetState(int state, bool isInit = false)
	{
		if (isInit)
		{
			State = state;
			base.gameObject.SetActive(value: true);
			if (flower_img != null)
			{
				flower_img.AnimationState.SetAnimation(0, "huanyuan", loop: false);
			}
		}
		else if (State != state)
		{
			State = state;
			if (State == 0)
			{
				flower_img.AnimationState.SetAnimation(0, "huanyuan", loop: false);
				base.gameObject.SetActive(value: true);
			}
			else if (State == 1)
			{
				flower_img.AnimationState.SetAnimation(0, "xiaoshi", loop: false);
			}
		}
	}

	public void Update()
	{
		position = base.transform.position;
		if (delay_showTime > 0f)
		{
			delay_showTime -= Time.deltaTime;
			if (delay_showTime <= 0f)
			{
				flower_img.gameObject.SetActive(value: true);
			}
		}
		if (anim_index == 1)
		{
			flower_eff_fly.SetActive(value: true);
			Vector3[] path = WordGlobal.CountCirclePoints(base.transform.position, endPos);
			base.transform.DOPath(path, 0.8f).OnComplete(delegate
			{
				anim_index = 3;
				anim_waitTime = (float)(count_index + 1) * 0.5f;
			});
			anim_index = 2;
			anim_waitTime = 0.2f;
		}
		else if (anim_index == 2)
		{
			if (anim_waitTime > 0f)
			{
				anim_waitTime -= Time.deltaTime;
				if (anim_waitTime <= 0f)
				{
					flower_eff_tx.SetActive(value: true);
					base.transform.DOScale(1.6f, 0.3f);
					hdgm.ShowFullEffWhite();
				}
			}
		}
		else if (anim_index == 3)
		{
			if (!(anim_waitTime > 0f))
			{
				return;
			}
			anim_waitTime -= Time.deltaTime;
			if (anim_waitTime <= 0f)
			{
				base.transform.DOScale(1f, 0.3f);
				if (count_index >= HuoDongGardonUI.ShuiDiFlyPos.Count)
				{
					count_index = HuoDongGardonUI.ShuiDiFlyPos.Count - 1;
				}
				Vector3 end_pos = HuoDongGardonUI.ShuiDiFlyPos[count_index];
				Vector3[] path2 = WordGlobal.CountCirclePoints(base.transform.position, end_pos);
				base.transform.DOPath(path2, 0.5f).OnComplete(delegate
				{
					flower_img.gameObject.SetActive(value: false);
					flower_eff_fly.SetActive(value: false);
					flower_eff_shouji.SetActive(value: true);
					flower_eff_tx.SetActive(value: false);
					anim_waitTime = 1f;
					anim_index = 5;
					HuoDongGardonUI.isAddWillOpenNum = true;
					AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_HuoDongGardonList[0]);
				});
				anim_index = 4;
			}
		}
		else if (anim_index == 5 && anim_waitTime > 0f)
		{
			anim_waitTime -= Time.deltaTime;
			if (anim_waitTime <= 0f)
			{
				flower_img.gameObject.SetActive(value: false);
			}
		}
	}
}
