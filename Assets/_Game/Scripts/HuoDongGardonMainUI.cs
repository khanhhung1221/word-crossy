using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongGardonMainUI : MonoBehaviour
{
	public Image round_img;

	public Text time_txt;

	public HuoDongGardonMainFlower[] flowers;

	public Transform[] flowerFlyPos;

	public Transform[] flowerFlyPos02;

	public Transform[] flowerFlyPos03;

	public Vector2[] flowerStandPos;

	public GameObject TimeShow_go;

	public Text TimeShow_des;

	public RectTransform TimeShow_timeRect;

	public Text TimeShow_timetxt;

	public Image TimeShow_timeBg;

	public RectTransform AddTime_rect;

	public MaskableGraphic[] addTime_mg;

	public Text addTime_text;

	private float rest_time;

	private float rest_endtime;

	private float rest_addSpeed;

	private float total_time;

	private HuoDongGardonSaveData hgds;

	public bool isTVAddTime;

	public Canvas canvas;

	public Transform round_tran;

	public static bool OnDestoryNeedCurPower;

	private bool isResult;

	private bool isShowFullEffWhite;

	public bool isTest;

	private void Start()
	{
	}

	private void OnDestroy()
	{
		if (!OnDestoryNeedCurPower)
		{
		}
	}

	public void Init()
	{
		OnDestoryNeedCurPower = true;
		hgds = PlayerInfo.Instance.GetHuoDongGardonSaveData();
		canvas.sortingOrder = 1;
		isTVAddTime = false;
		total_time = hgds.GetCurLevelTime();
		rest_time = total_time;
		time_txt.text = ((int)rest_time).ToString();
		round_img.fillAmount = 1f;
		int color_type = 0;
		if (hgds.HuoDongItemAmount < hgds.GetRewardNeedNum(1))
		{
			color_type = 0;
			round_img.color = WordGlobal.ChangeStringToColor("#0667f9");
		}
		else if (hgds.HuoDongItemAmount < hgds.GetRewardNeedNum(2))
		{
			color_type = 1;
			round_img.color = WordGlobal.ChangeStringToColor("#0667f9");
		}
		else if (hgds.HuoDongItemAmount < hgds.GetRewardNeedNum(3))
		{
			color_type = 2;
			round_img.color = WordGlobal.ChangeStringToColor("#0667f9");
		}
		for (int i = 0; i < flowers.Length; i++)
		{
			flowers[i].SetColor(color_type, this);
			flowers[i].SetState(0, isInit: true);
			(flowers[i].transform as RectTransform).anchoredPosition = flowerStandPos[i];
		}
		base.gameObject.SetActive(value: true);
		isResult = false;
		InitShowUIInTime();
		isShowFullEffWhite = false;
	}

	public void ShowTutorial()
	{
		if ((!(MainUIScript.Instance != null) || !MainUIScript.Instance.isUInAnimGardon) && hgds.Tutorial02 == 0 && PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HuoDongGardonTutorial01 <= 1)
		{
			if (GameObject.Find("HuoDongHelpUI") == null)
			{
				HuoDongHelpUI.des_txtid = 1218;
				HuoDongHelpUI.isHuoDongGardonTutorialPause = true;
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongHelpUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
			}
			hgds.SetTutorial(2);
		}
	}

	public void ShowFullEffWhite()
	{
		if (!isShowFullEffWhite)
		{
			WordGlobal.ShowHuoDongGardonMainFullEff(2);
			isShowFullEffWhite = true;
		}
	}

	public void InitShowUIInTime()
	{
		TimeShow_go.SetActive(value: false);
		TimeShow_des.text = TextLibraryDataManagerCsv.instance.GetText(1321);
		TimeShow_timetxt.text = rest_time + string.Empty;
		TimeShow_timeRect.localScale = Vector3.zero;
		TimeShow_des.transform.localScale = Vector3.zero;
		TimeShow_timeRect.anchoredPosition = new Vector2(0f, 200f);
		Text timeShow_timetxt = TimeShow_timetxt;
		Color color = TimeShow_timetxt.color;
		float r = color.r;
		Color color2 = TimeShow_timetxt.color;
		float g = color2.g;
		Color color3 = TimeShow_timetxt.color;
		timeShow_timetxt.color = new Color(r, g, color3.b, 1f);
		Text timeShow_des = TimeShow_des;
		Color color4 = TimeShow_des.color;
		float r2 = color4.r;
		Color color5 = TimeShow_des.color;
		float g2 = color5.g;
		Color color6 = TimeShow_des.color;
		timeShow_des.color = new Color(r2, g2, color6.b, 1f);
		TimeShow_timeBg.color = Color.white;
	}

	public float ShowUIInTime(int type)
	{
		TimeShow_go.SetActive(value: true);
		float anim_time = 0f;
		float result = 0f;
		switch (type)
		{
		case 0:
			anim_time = 0.5f;
			TimeShow_timeRect.DOScale(Vector3.one, anim_time).SetEase(Ease.OutBack);
			TimeShow_des.transform.DOScale(Vector3.one, anim_time).SetEase(Ease.OutBack);
			result = anim_time;
			break;
		case 1:
			anim_time = 0.3f;
			TimeShow_des.DOFade(0f, anim_time);
			TimeShow_timeRect.DOAnchorPosY(20f, anim_time).OnComplete(delegate
			{
				TimeShow_timetxt.DOFade(0f, anim_time);
				TimeShow_timeBg.DOFade(0f, anim_time);
			});
			result = anim_time * 2f;
			break;
		}
		return result;
	}

	public void AddTime(float add_time, bool isTVAdd = false)
	{
		int num = (int)add_time;
		if (rest_time + add_time > total_time)
		{
			add_time = total_time - rest_time;
		}
		rest_endtime = rest_time + add_time;
		rest_addSpeed = add_time;
		isResult = false;
		if (isTVAdd)
		{
			isTVAddTime = true;
		}
		AddTime_rect.DOKill();
		AddTime_rect.gameObject.SetActive(value: true);
		RectTransform addTime_rect = AddTime_rect;
		Vector2 anchoredPosition = AddTime_rect.anchoredPosition;
		addTime_rect.anchoredPosition = new Vector2(anchoredPosition.x, 300f);
		for (int i = 0; i < addTime_mg.Length; i++)
		{
			MaskableGraphic obj = addTime_mg[i];
			Color color = addTime_mg[i].color;
			float r = color.r;
			Color color2 = addTime_mg[i].color;
			float g = color2.g;
			Color color3 = addTime_mg[i].color;
			obj.color = new Color(r, g, color3.b, 1f);
		}
		float duration = 2f;
		AddTime_rect.DOAnchorPosY(400f, duration);
		for (int j = 0; j < addTime_mg.Length; j++)
		{
			addTime_mg[j].DOFade(0f, 0.3f).SetDelay(0.5f);
		}
		addTime_text.text = num + string.Empty;
	}

	public void SetResult()
	{
		isResult = true;
	}

	public IEnumerator Result(bool isFail = false)
	{
		if (isFail)
		{
			if (MainUIScript.Instance._effectControl.isLetterFly)
			{
				yield return new WaitForSeconds(1f);
			}
			if (!MainUIScript.Instance.isShowResult())
			{
				Debug.Log("显示失败结算");
				CommonTipsScript.isAutoHide = true;
				if (GameObject.Find("HuoDongGardonTimeOut") == null)
				{
					UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonTimeOut", isPop: true, UIManager.MainUIType.Type_Common, 0);
				}
				HuoDongGardonBackLevel.isHide = true;
			}
			yield break;
		}
		int AddNum = 0;
		int count = 0;
		for (int i = 0; i < flowers.Length; i++)
		{
			if (flowers[i].State == 0)
			{
				count++;
				AddNum++;
			}
		}
		int count_index3 = 0;
		switch (count)
		{
		case 3:
			for (int k = 0; k < flowers.Length; k++)
			{
				if (flowers[k].State == 0)
				{
					flowers[k].ShowAnim(flowerFlyPos03[k].position, count_index3);
					count_index3++;
				}
			}
			break;
		case 2:
			count_index3 = 0;
			for (int j = 1; j < flowers.Length; j++)
			{
				if (flowers[j].State == 0)
				{
					flowers[j].ShowAnim(flowerFlyPos02[j - 1].position, count_index3);
					count_index3++;
				}
			}
			break;
		default:
			count_index3 = 0;
			flowers[2].ShowAnim(flowerFlyPos[0].position, count_index3);
			break;
		}
		WordGlobal.ShowHuoDongGardonMainFullEff(1);
		canvas.sortingOrder = 21;
		hgds.AddItem(AddNum);
		WordFaceBook.Instance.UpdateUserdataMill();
		yield return new WaitForSeconds(1f);
		if (WordGlobal.GetHuoDongGardonTimeLeft() <= 0)
		{
			if (MainUIScript.Instance != null)
			{
				MainUIScript.Instance.BtnCloseClick();
			}
			HuoDongGardonFullEff.Hide = true;
		}
		else
		{
			HuoDongGardonUI.OpenType = 1;
			if (GameObject.Find("HuoDongGardonUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
			}
		}
	}

	public void FlyShuiDi(int idx, Vector3 pos)
	{
	}

	private void Update()
	{
		if (isTest)
		{
			AddTime(2f);
			isTest = false;
		}
		UpdateTime();
		ShowTutorial();
	}

	private void UpdateTime()
	{
		if (isResult || HuoDongHelpUI.isHuoDongGardonTutorialPause || (MainUIScript.Instance != null && MainUIScript.Instance.isUInAnimGardon))
		{
			return;
		}
		if (rest_endtime > rest_time)
		{
			rest_time += Time.deltaTime * rest_addSpeed;
			if (rest_time >= rest_endtime)
			{
				rest_time = rest_endtime;
				rest_endtime = 0f;
			}
			time_txt.text = ((int)rest_time).ToString();
			float fillAmount = rest_time / total_time;
			round_img.fillAmount = fillAmount;
		}
		else if (rest_time > 0f)
		{
			rest_time -= Time.deltaTime;
			if (rest_time <= 0f)
			{
				rest_time = 0f;
				SetResult();
				StartCoroutine(Result(isFail: true));
				time_txt.text = ((int)rest_time).ToString();
			}
			else
			{
				time_txt.text = ((int)(rest_time + 1f)).ToString();
			}
			float fillAmount2 = rest_time / total_time;
			round_img.fillAmount = fillAmount2;
		}
		UpdateFlowerState();
	}

	public void UpdateFlowerState()
	{
		for (int i = 0; i < flowers.Length; i++)
		{
			if (rest_time < total_time - total_time * (float)(i + 1) / 3f)
			{
				flowers[i].SetState(1);
			}
			else
			{
				flowers[i].SetState(0);
			}
		}
	}
}
