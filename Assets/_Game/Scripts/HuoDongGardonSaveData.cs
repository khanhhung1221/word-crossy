using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HuoDongGardonSaveData
{
	public int HuoDongID;

	public int HuoDongItemAmount;

	public int HuoDongGardon_getLevelCount;

	public int PowerNum;

	public double LastAddPowerTime;

	public List<int> GetRewardNum = new List<int>();

	public int BuyPowerNum;

	public bool isCloseHuoDong;

	public int ItemWillAddNum;

	public int Tutorial01;

	public int Tutorial02;

	public int Tutorial03;

	public int TutorialOpen;

	public int FirstOpen;

	public int FirstPlay;

	private int power_maxnum = 3;

	private int addPowerSecond = 1200;

	private int addPowerNeedNum = 200;

	private int timeout_addTime = 15;

	private int timeout_needGoldNum = 50;

	private int Reward01_NeedNum = -1;

	private int Reward02_NeedNum = -1;

	private int Reward03_NeedNum = -1;

	private EventDataCsv edc;

	private int[] allLevels = new int[12]
	{
		50000,
		50099,
		50100,
		50199,
		50200,
		50299,
		50300,
		50359,
		50360,
		50409,
		50410,
		50449
	};

	private int[] level_weight = new int[6]
	{
		5,
		15,
		30,
		60,
		60,
		20
	};

	private int weight_all;

	public int Timeout_addTime
	{
		get
		{
			return timeout_addTime;
		}
		set
		{
			timeout_addTime = value;
		}
	}

	public int Timeout_needGoldNum
	{
		get
		{
			return timeout_needGoldNum;
		}
		set
		{
			timeout_needGoldNum = value;
		}
	}

	public void InitHuoDongID()
	{
		HuoDongID = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 2);
		GetEventData();
		HuoDongItemAmount = 0;
		BuyPowerNum = 0;
		GetRewardNum.Clear();
		isCloseHuoDong = false;
		SetFullPower();
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
	}

	public void SetFirstOpen()
	{
		FirstOpen = 1;
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
	}

	public void SetFirstPlay()
	{
		FirstPlay = 1;
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
	}

	public void SetTutorialOpen()
	{
		TutorialOpen = 1;
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
	}

	public int GetEveryRewardNeedNum(int idx)
	{
		string[] array = GetEventData().other.Split('_');
		return int.Parse(array[idx]);
	}

	public int GetRewardNeedNum(int type)
	{
		if (Reward01_NeedNum < 0)
		{
			string[] array = GetEventData().other.Split('_');
			Reward01_NeedNum = int.Parse(array[0]);
			Reward02_NeedNum = int.Parse(array[1]) + Reward01_NeedNum;
			Reward03_NeedNum = int.Parse(array[2]) + Reward02_NeedNum;
		}
		switch (type)
		{
		case 1:
			return Reward01_NeedNum;
		case 2:
			return Reward02_NeedNum;
		default:
			return Reward03_NeedNum;
		}
	}

	public void SetCloseHuoDong()
	{
		isCloseHuoDong = true;
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
	}

	public EventDataCsv GetEventData()
	{
		if (edc == null)
		{
			edc = EventDataManagerCsv.instance.GetDataByType(2);
		}
		return edc;
	}

	public int GetRewardGoldNum(int idx)
	{
		int result = 0;
		if (idx == 0)
		{
			string[] array = GetEventData().item1.Split('_');
			int num = int.Parse(array[0]);
			if (num == 3)
			{
				int num2 = int.Parse(array[1]);
				result = num2;
			}
		}
		if (idx == 1)
		{
			string[] array2 = GetEventData().item2.Split('_');
			int num3 = int.Parse(array2[0]);
			if (num3 == 3)
			{
				int num4 = int.Parse(array2[1]);
				result = num4;
			}
		}
		if (idx == 2)
		{
			string[] array3 = GetEventData().item3.Split('_');
			int num5 = int.Parse(array3[0]);
			if (num5 == 3)
			{
				int num6 = int.Parse(array3[1]);
				result = num6;
			}
		}
		return result;
	}

	public bool GiveReward(int idx, out int gold_num)
	{
		bool result = false;
		gold_num = 0;
		if (idx == 0 && !GetRewardNum.Contains(idx) && HuoDongItemAmount >= GetRewardNeedNum(1))
		{
			PlayerInfo.Instance.AddItemByStr(GetEventData().item1, 1);
			string[] array = GetEventData().item1.Split('_');
			int num = int.Parse(array[0]);
			if (num == 3)
			{
				int num2 = gold_num = int.Parse(array[1]);
			}
			AddRewardNum(idx);
			result = true;
		}
		if (idx == 1 && !GetRewardNum.Contains(idx) && HuoDongItemAmount >= GetRewardNeedNum(2))
		{
			PlayerInfo.Instance.AddItemByStr(GetEventData().item2, 2);
			string[] array2 = GetEventData().item2.Split('_');
			int num3 = int.Parse(array2[0]);
			if (num3 == 3)
			{
				int num4 = gold_num = int.Parse(array2[1]);
			}
			AddRewardNum(idx);
			result = true;
		}
		if (idx == 2 && !GetRewardNum.Contains(idx) && HuoDongItemAmount >= GetRewardNeedNum(3))
		{
			PlayerInfo.Instance.AddItemByStr(GetEventData().item3, 3);
			string[] array3 = GetEventData().item3.Split('_');
			int num5 = int.Parse(array3[0]);
			if (num5 == 3)
			{
				int num6 = gold_num = int.Parse(array3[1]);
			}
			AddRewardNum(idx);
			result = true;
		}
		return result;
	}

	public void AddRewardNum(int idx)
	{
		GetRewardNum.Add(idx);
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
	}

	public void SetTutorial(int type)
	{
		switch (type)
		{
		case 1:
			Tutorial01 = 1;
			break;
		case 2:
			Tutorial02 = 1;
			break;
		case 3:
			Tutorial03 = 1;
			break;
		}
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
	}

	public int GetaddPowerNeedNum()
	{
		return addPowerNeedNum;
	}

	public void CheckPowerTime()
	{
		if (LastAddPowerTime == 0)
		{
			SetLastAddPTime(0L);
			return;
		}
		double num = WordGlobal.ConvertDateTimeToSecond(DateTime.Now);
		double num2 = (num - LastAddPowerTime) / addPowerSecond;
		double lastAddPTime = (num - LastAddPowerTime) % addPowerSecond;
		if (num2 > 0)
		{
			if (PowerNum + num2 >= power_maxnum)
			{
				SetLastAddPTime(0L);
			}
			else
			{
				SetLastAddPTime(lastAddPTime);
			}
		}
		AddPower((int)num2, isChangeLasetAddPowerTime: false);
	}

	public bool isFull()
	{
		if (PowerNum == power_maxnum)
		{
			return true;
		}
		return false;
	}

	public bool isFullItem()
	{
		if (HuoDongItemAmount >= GetRewardNeedNum(3))
		{
			return true;
		}
		return false;
	}

	public string GetNextPowerTimeLeft()
	{
		if (PowerNum == power_maxnum)
		{
			return TextLibraryDataManagerCsv.instance.GetText(1326);
		}
		double num = LastAddPowerTime + addPowerSecond;
		double num2 = WordGlobal.ConvertDateTimeToSecond(DateTime.Now);
		if (num2 >= num)
		{
			AddPower(1);
			num = LastAddPowerTime + addPowerSecond;
		}
		double time = num - num2;
		return WordGlobal.ChangeTimeToNomral(time, isNeedDay: false);
	}

	public void SetFullPower(bool isBuy = false)
	{
		if (isBuy)
		{
			BuyPowerNum++;
		}
		SetPowerNum(power_maxnum);
		SetLastAddPTime(0);
	}

	public void SetLastAddPTime(double shengyutime = 0L)
	{
		double num = WordGlobal.ConvertDateTimeToSecond(DateTime.Now);
		if (num > LastAddPowerTime)
		{
			LastAddPowerTime = num - shengyutime;
		}
	}

	public void AddPower(int num, bool isChangeLasetAddPowerTime = true)
	{
		int powerNum = PowerNum + num;
		if (num > 0 && isChangeLasetAddPowerTime)
		{
			SetLastAddPTime(0L);
		}
		if (num < 0 && PowerNum == power_maxnum)
		{
			if (HuoDongGardonMainUI.OnDestoryNeedCurPower)
			{
				HuoDongGardonMainUI.OnDestoryNeedCurPower = false;
			}
			SetLastAddPTime(0L);
		}
		if (num < 0)
		{
			HuoDongGardonMainUI.OnDestoryNeedCurPower = false;
		}
		SetPowerNum(powerNum);
	}

	public void SetPowerNum(int num)
	{
		PowerNum = num;
		if (PowerNum <= 0)
		{
			PowerNum = 0;
		}
		if (PowerNum >= power_maxnum)
		{
			PowerNum = power_maxnum;
		}
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
	}

	public void AddItem(int num)
	{
		int huoDongItemAmount = HuoDongItemAmount;
		HuoDongItemAmount += num;
		if (HuoDongItemAmount > GetRewardNeedNum(3))
		{
			HuoDongItemAmount = GetRewardNeedNum(3);
		}
		ItemWillAddNum = HuoDongItemAmount - huoDongItemAmount;
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
	}

	public int GetCurLevelTime()
	{
		int flowerType = GetFlowerType();
		int result = 0;
		switch (flowerType)
		{
		case 1:
			result = 60;
			break;
		case 2:
			result = 45;
			break;
		case 3:
			result = 30;
			break;
		}
		return result;
	}

	public int GetFlowerType()
	{
		if (HuoDongItemAmount < GetRewardNeedNum(1))
		{
			return 1;
		}
		if (HuoDongItemAmount < GetRewardNeedNum(2))
		{
			return 2;
		}
		return 3;
	}

	public int GetHuoDongGardonLevelId()
	{
		int num = 0;
		int num2 = 0;
		if (num2 <= 0)
		{
			for (int i = 0; i < level_weight.Length; i++)
			{
				num2 += level_weight[i];
			}
		}
		int num3 = UnityEngine.Random.Range(0, num2);
		int num4 = 0;
		for (int j = 0; j < level_weight.Length; j++)
		{
			if (num3 - level_weight[j] < 0)
			{
				num4 = j;
				break;
			}
			num3 -= level_weight[j];
		}
		int min = allLevels[num4 * 2];
		int max = allLevels[num4 * 2 + 1];
		num = UnityEngine.Random.Range(min, max);
		Debug.Log("花园随机关卡 levelID:" + num + " ran_weight_num:" + num3 + " weight_levelIdx:" + num4);
		PlayerInfo.Instance.SaveHuoDongGardonSaveData();
		return num;
	}
}
