using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HuoDongGardonTimeOut : MonoBehaviour
{
	public static bool isBtnGiveUpClick;

	public static bool isShowGardonTimeOut;

	public RectTransform main_rectTran;

	public Text title_txt;

	public GameObject BtnWatch_go;

	public GameObject BtnWatch_dengpao_go;

	public GameObject BtnWatch_plus_go;

	public RectTransform BtnWatch_timeRect;

	public Text BtnWatch_timeTxt;

	public GameObject BtnGold_go;

	public GameObject BtnGold_dengpao_go;

	public GameObject BtnGold_plus_go;

	public RectTransform BtnGold_timeRect;

	public Text BtnGold_timeTxt;

	public Text BtnGold_goldNumTxt;

	public GameObject BtnGiveUp_go;

	public Text btnWatch_txt;

	public Text btnGold_txt;

	public Text btnGiveUp_txt;

	public GameObject JiaZai_go;

	public Transform JiaZai_yTran;

	private HuoDongGardonSaveData hgsd;

	private bool isLastLetter;

	private int count = 1;

	private void Start()
	{
		isShowGardonTimeOut = true;
		isBtnGiveUpClick = false;
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1215);
		btnWatch_txt.text = TextLibraryDataManagerCsv.instance.GetText(1216);
		btnGold_txt.text = TextLibraryDataManagerCsv.instance.GetText(1216);
		btnGiveUp_txt.text = TextLibraryDataManagerCsv.instance.GetText(1217);
		hgsd = PlayerInfo.Instance.GetHuoDongGardonSaveData();
		hgsd.AddPower(-1);
		BtnWatch_timeTxt.text = hgsd.Timeout_addTime + string.Empty;
		BtnGold_timeTxt.text = hgsd.Timeout_addTime + string.Empty;
		BtnGold_goldNumTxt.text = hgsd.Timeout_needGoldNum + string.Empty;
		if (MainUIScript.Instance.huodong_gardon_main != null)
		{
			if (MainUIScript.Instance.huodong_gardon_main.isTVAddTime || !AdsManager.Instance.IncentivizedAdState || PlayerInfo.Instance.LastOfferPayValue > 0f)
			{
				BtnWatch_go.SetActive(value: false);
			}
			else
			{
				BtnWatch_go.SetActive(value: true);
			}
		}
		MainUIScript.Instance.m_TfItemRoot.gameObject.SetActive(value: false);
		MainUIScript.Instance.m_UILetterRoot.SetActive(value: false);
		MainUIScript.Instance.huodong_gardon_main.gameObject.SetActive(value: false);
		int num = MainUIScript.Instance.NoLetterNum();
		if (num > 1)
		{
			isLastLetter = false;
		}
		else
		{
			isLastLetter = true;
		}
		if (isLastLetter)
		{
			BtnWatch_dengpao_go.SetActive(value: false);
			BtnWatch_plus_go.SetActive(value: false);
			BtnGold_dengpao_go.SetActive(value: false);
			BtnGold_plus_go.SetActive(value: false);
			RectTransform btnWatch_timeRect = BtnWatch_timeRect;
			Vector2 anchoredPosition = BtnWatch_timeRect.anchoredPosition;
			btnWatch_timeRect.anchoredPosition = new Vector2(-174f, anchoredPosition.y);
			RectTransform btnGold_timeRect = BtnGold_timeRect;
			Vector2 anchoredPosition2 = BtnGold_timeRect.anchoredPosition;
			btnGold_timeRect.anchoredPosition = new Vector2(-174f, anchoredPosition2.y);
		}
		else
		{
			BtnWatch_dengpao_go.SetActive(value: true);
			BtnWatch_plus_go.SetActive(value: true);
			BtnGold_dengpao_go.SetActive(value: true);
			BtnGold_plus_go.SetActive(value: true);
			RectTransform btnWatch_timeRect2 = BtnWatch_timeRect;
			Vector2 anchoredPosition3 = BtnWatch_timeRect.anchoredPosition;
			btnWatch_timeRect2.anchoredPosition = new Vector2(-150f, anchoredPosition3.y);
			RectTransform btnGold_timeRect2 = BtnGold_timeRect;
			Vector2 anchoredPosition4 = BtnGold_timeRect.anchoredPosition;
			btnGold_timeRect2.anchoredPosition = new Vector2(-150f, anchoredPosition4.y);
		}
		SetPos();
		if (MainUIScript.Instance != null && MainUIScript.Instance._cellTipsClick)
		{
			MainUIScript.Instance.BtnCellTipsClick();
		}
	}

	public void SetPos()
	{
		if (!BtnWatch_go.activeInHierarchy)
		{
			RectTransform rectTransform = main_rectTran;
			Vector2 sizeDelta = main_rectTran.sizeDelta;
			rectTransform.sizeDelta = new Vector2(sizeDelta.x, 350f);
			RectTransform rectTransform2 = BtnGold_go.transform as RectTransform;
			RectTransform rectTransform3 = rectTransform2;
			Vector2 anchoredPosition = rectTransform2.anchoredPosition;
			rectTransform3.anchoredPosition = new Vector2(anchoredPosition.x, -148f);
			RectTransform rectTransform4 = BtnGiveUp_go.transform as RectTransform;
			RectTransform rectTransform5 = rectTransform4;
			Vector2 anchoredPosition2 = rectTransform4.anchoredPosition;
			rectTransform5.anchoredPosition = new Vector2(anchoredPosition2.x, -258f);
		}
	}

	public void GiveReward(int type)
	{
		switch (type)
		{
		case 0:
			MainUIScript.Instance.huodong_gardon_main.AddTime(hgsd.Timeout_addTime, isTVAdd: true);
			break;
		case 1:
			MainUIScript.Instance.huodong_gardon_main.AddTime(hgsd.Timeout_addTime);
			break;
		}
		if (!isLastLetter)
		{
			MainUIScript.Instance.FreeDP();
		}
		hgsd.AddPower(1, isChangeLasetAddPowerTime: false);
		MainUIScript.Instance.m_TfItemRoot.gameObject.SetActive(value: true);
		MainUIScript.Instance.m_UILetterRoot.SetActive(value: true);
		MainUIScript.Instance.huodong_gardon_main.gameObject.SetActive(value: true);
	}

	public void BtnWatchClick()
	{
		if (AdsManager.Instance.IncentivizedAdState && AdsManager.Instance.ShowIncentivizedAd("HuoDongGardon_TimeOutWatch"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEnd));
		}
	}

	private void WatchVideoEnd(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEnd));
		if (ret)
		{
			GiveReward(0);
			Hide();
		}
	}

	public void BtnGoldClick()
	{
		if (PlayerInfo.Instance.DecGold(hgsd.Timeout_needGoldNum))
		{
			GiveReward(1);
			WordGlobal.LogEvent("HuoDongGardon_TimeOutGoldBuy");
			Hide();
		}
	}

	public void BtnGiveUpClick()
	{
		if (hgsd.isFull())
		{
			hgsd.AddPower(1);
			hgsd.AddPower(-1);
		}
		else
		{
			hgsd.AddPower(1, isChangeLasetAddPowerTime: false);
			hgsd.AddPower(-1);
		}
		if (WordGlobal.GetHuoDongGardonTimeLeft() <= 0)
		{
			Hide();
			if (MainUIScript.Instance != null)
			{
				MainUIScript.Instance.BtnCloseClick();
			}
		}
		else
		{
			Hide(isAnim: true);
		}
	}

	private void Hide(bool isAnim = false)
	{
		if (isAnim)
		{
			main_rectTran.DOScale(Vector3.zero, 0.3f).OnComplete(delegate
			{
				Hide();
				MainUIScript.Instance.HuoDongGardonGiveUp();
			});
			return;
		}
		UnityEngine.Object.Destroy(base.gameObject);
		MainUIScript.Instance.m_TfItemRoot.gameObject.SetActive(value: true);
	}

	private void Update()
	{
		if (MainUIScript.Instance.isShowResult() && count > 0)
		{
			hgsd.AddPower(1, isChangeLasetAddPowerTime: false);
			UnityEngine.Object.Destroy(base.gameObject);
			count--;
		}
		if (!AdsManager.Instance.IncentivizedAdState)
		{
			JiaZai_go.SetActive(value: true);
		}
		else
		{
			JiaZai_go.SetActive(value: false);
		}
		if (JiaZai_go.activeInHierarchy)
		{
			JiaZai_yTran.Rotate(0f, 0f, -360f * Time.deltaTime);
		}
		if (isBtnGiveUpClick)
		{
			BtnGiveUpClick();
			isBtnGiveUpClick = false;
		}
	}

	private void OnDestroy()
	{
		isShowGardonTimeOut = false;
	}
}
