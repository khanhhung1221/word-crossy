using UnityEngine;
using UnityEngine.UI;

public class HuoDongGardonTutorial01 : MonoBehaviour
{
	public static int Type = -1;

	public RectTransform[] Pos_rects;

	public Text des_txt;

	public Text btn_txt;

	public RectTransform move_rect;

	public static bool isHide;

	private void Start()
	{
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1235);
		isHide = false;
		if (PlayerInfo.Instance.LoggedFaceBook == 1)
		{
			RectTransform rectTransform = move_rect;
			Vector2 anchoredPosition = move_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, 106f);
		}
		else
		{
			RectTransform rectTransform2 = move_rect;
			Vector2 anchoredPosition2 = move_rect.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, 0f);
		}
		if (Type == 0)
		{
			des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1214);
			if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongGardon).transform.SetParent(base.transform);
			}
			int huoDongBtnPosIdx = LevelMapUIScript.Instance.GetHuoDongBtnPosIdx(LevelMapUIScript.HuoDongBtnType.HuoDongGardon);
			for (int i = 0; i < Pos_rects.Length; i++)
			{
				RectTransform obj = Pos_rects[i];
				Vector2 anchoredPosition3 = Pos_rects[i].anchoredPosition;
				float x = anchoredPosition3.x;
				Vector2 anchoredPosition4 = Pos_rects[i].anchoredPosition;
				obj.anchoredPosition = new Vector2(x, anchoredPosition4.y - (float)(huoDongBtnPosIdx * 152));
			}
		}
		else if (Type == 1)
		{
			des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1233);
			if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongSun).transform.SetParent(base.transform);
			}
			int huoDongBtnPosIdx2 = LevelMapUIScript.Instance.GetHuoDongBtnPosIdx(LevelMapUIScript.HuoDongBtnType.HuoDongSun);
			for (int j = 0; j < Pos_rects.Length; j++)
			{
				RectTransform obj2 = Pos_rects[j];
				Vector2 anchoredPosition5 = Pos_rects[j].anchoredPosition;
				float x2 = anchoredPosition5.x;
				Vector2 anchoredPosition6 = Pos_rects[j].anchoredPosition;
				obj2.anchoredPosition = new Vector2(x2, anchoredPosition6.y - (float)(huoDongBtnPosIdx2 * 152));
			}
		}
		else if (Type == 2)
		{
			des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1287);
			if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongTournament).transform.SetParent(base.transform);
			}
			int huoDongBtnPosIdx3 = LevelMapUIScript.Instance.GetHuoDongBtnPosIdx(LevelMapUIScript.HuoDongBtnType.HuoDongTournament);
			for (int k = 0; k < Pos_rects.Length; k++)
			{
				RectTransform obj3 = Pos_rects[k];
				Vector2 anchoredPosition7 = Pos_rects[k].anchoredPosition;
				float x3 = anchoredPosition7.x;
				Vector2 anchoredPosition8 = Pos_rects[k].anchoredPosition;
				obj3.anchoredPosition = new Vector2(x3, anchoredPosition8.y - (float)(huoDongBtnPosIdx3 * 152));
			}
		}
		else if (Type == 3)
		{
			des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1331);
			if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongWordFind).transform.SetParent(base.transform);
			}
			int huoDongBtnPosIdx4 = LevelMapUIScript.Instance.GetHuoDongBtnPosIdx(LevelMapUIScript.HuoDongBtnType.HuoDongWordFind);
			for (int l = 0; l < Pos_rects.Length; l++)
			{
				RectTransform obj4 = Pos_rects[l];
				Vector2 anchoredPosition9 = Pos_rects[l].anchoredPosition;
				float x4 = anchoredPosition9.x;
				Vector2 anchoredPosition10 = Pos_rects[l].anchoredPosition;
				obj4.anchoredPosition = new Vector2(x4, anchoredPosition10.y - (float)(huoDongBtnPosIdx4 * 152));
			}
		}
	}

	private void Update()
	{
		if (isHide)
		{
			Object.Destroy(base.gameObject);
			isHide = false;
		}
		if (Type == 0)
		{
			if (WordGlobal.GetHuoDongGardonTimeLeft() <= 0)
			{
				isHide = true;
			}
		}
		else if (Type == 1)
		{
			if (WordGlobal.GetHuoDongSunTimeLeft() <= 0)
			{
				isHide = true;
			}
		}
		else if (Type == 2)
		{
			if (WordGlobal.GetHuoDongTournamentTimeLeft() <= 0)
			{
				isHide = true;
			}
		}
		else if (Type == 3 && WordGlobal.GetHuoDongWordFindTimeLeft() <= 0)
		{
			isHide = true;
		}
	}

	public void BtnGotoClick()
	{
		if (Type == 0)
		{
			if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.BtnHuoDongGardonClick();
			}
		}
		else if (Type == 1)
		{
			if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.BtnHuoDongSunClick();
			}
		}
		else if (Type == 2)
		{
			if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.BtnHuoDongTournamentClick();
			}
		}
		else if (Type == 3 && LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.BtnHuoDongWordFindClick();
		}
		Object.Destroy(base.gameObject);
	}

	private void OnDestroy()
	{
		if (LevelMapUIScript.Instance == null)
		{
			return;
		}
		if (Type == 0)
		{
			Button component = LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongGardon).GetComponent<Button>();
			if (LevelMapUIScript.Instance != null && component != null)
			{
				LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongGardon).transform.SetParent(LevelMapUIScript.Instance.m_RtfHideBtnsTrans);
				component.enabled = true;
			}
		}
		else if (Type == 1)
		{
			Button component2 = LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongSun).GetComponent<Button>();
			if (LevelMapUIScript.Instance != null && component2 != null)
			{
				LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongSun).transform.SetParent(LevelMapUIScript.Instance.m_RtfHideBtnsTrans);
				component2.enabled = true;
			}
		}
		else if (Type == 2)
		{
			Button component3 = LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongTournament).GetComponent<Button>();
			if (LevelMapUIScript.Instance != null && component3 != null)
			{
				LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongTournament).transform.SetParent(LevelMapUIScript.Instance.m_RtfHideBtnsTrans);
				component3.enabled = true;
			}
		}
		else if (Type == 3)
		{
			Button component4 = LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongWordFind).GetComponent<Button>();
			if (LevelMapUIScript.Instance != null && component4 != null)
			{
				LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDongWordFind).transform.SetParent(LevelMapUIScript.Instance.m_RtfHideBtnsTrans);
				component4.enabled = true;
			}
		}
		Type = -1;
	}
}
