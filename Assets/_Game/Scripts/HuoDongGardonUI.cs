using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongGardonUI : MonoBehaviour
{
	public static int OpenType = 0;

	public static bool isBtnCloseClick = false;

	public static List<Vector3> ShuiDiFlyPos = new List<Vector3>();

	public static float isShowOpenBtn = -1f;

	public static bool RefreshGold = false;

	public static bool isFlowerOpenAnim = false;

	public static bool isResetGold = false;

	public static bool StartShowAddTime = false;

	public static int AdCount = 0;

	public Transform flower_parent;

	public Text title_txt;

	public Text timeTxt;

	public Text playTxt;

	public Text playTimeTxt;

	public Text PowerNumTxt;

	public Text PowerTimeTxt;

	public Image add_img;

	public GameObject add_clickGo;

	public GameObject HeartAnim_go;

	public GameObject closeBtn_go;

	public GameObject playAndPowerBtn_go;

	public HuoDongGardonUI_JinDU[] JinDus;

	public RectTransform Top_rect;

	private int tempPowerNum;

	private int huodong_timeleft;

	public float flowerOpenNum;

	public int flowerWillOpenEndNum;

	private float cut_powerNum_time;

	private HuoDongGardonSaveData hgsd;

	private GoldScript gold_script;

	private bool HideNeedShowBanner;

	private float count_time;

	private bool ad_show;

	private float ad_waitTime;

	private bool cutP_showaddtime;

	public static bool isAddWillOpenNum = false;

	private float add_speed;

	private void Start()
	{
		WordGlobal.ResetTopPos(Top_rect);
		isResetGold = false;
		isBtnCloseClick = false;
		PlayerInfo.Instance.GetPlayerOtherInfoSaveData().OpenHuoDongUI(0);
		ResourceLoadManager.GetInstance().HideNetWaiting();
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1220);
		playTxt.text = TextLibraryDataManagerCsv.instance.GetText(1075);
		hgsd = PlayerInfo.Instance.GetHuoDongGardonSaveData();
		playTimeTxt.text = hgsd.GetCurLevelTime() + string.Empty;
		hgsd.CheckPowerTime();
		PowerNumTxt.text = hgsd.PowerNum + string.Empty;
		tempPowerNum = hgsd.PowerNum;
		count_time = 1f;
		gold_script = WordGlobal.AddGoldUI(Top_rect);
		gold_script.m_ShowBanner = true;
		gold_script._isAddGold = true;
		if (gold_script.banner_go != null)
		{
			gold_script.banner_go.GetComponent<Canvas>().sortingOrder = 20;
		}
		isFlowerOpenAnim = false;
		if (OpenType == 0)
		{
			flowerOpenNum = hgsd.HuoDongItemAmount;
		}
		else if (OpenType == 1)
		{
			flowerOpenNum = hgsd.HuoDongItemAmount - hgsd.ItemWillAddNum;
			flowerWillOpenEndNum = (int)flowerOpenNum;
			add_speed = 10f;
			ad_show = true;
		}
		else if (OpenType == 2)
		{
			flowerOpenNum = hgsd.HuoDongItemAmount;
			cut_powerNum_time = 0.2f;
			hgsd.AddPower(1, isChangeLasetAddPowerTime: false);
			if (hgsd.PowerNum <= 1)
			{
				cutP_showaddtime = true;
			}
			ad_waitTime = 0.7f;
		}
		else if (OpenType == 3)
		{
			flowerOpenNum = hgsd.HuoDongItemAmount;
			cut_powerNum_time = 0.2f;
			if (hgsd.PowerNum <= 1)
			{
				cutP_showaddtime = true;
			}
			ad_waitTime = 0.7f;
		}
		if (ad_waitTime > 0f && AdCount > 0)
		{
			ad_waitTime = 0.01f;
		}
		InitShuiDi();
		if (OpenType == 1)
		{
			ShuiDiFlyPos.Clear();
			for (int i = 0; i < JinDus.Length; i++)
			{
				JinDus[i].CountNeedItemNum((int)flowerOpenNum);
			}
			for (int j = 0; j < JinDus.Length; j++)
			{
				for (int k = 0; k < JinDus[j].NeedItemNum; k++)
				{
					if (ShuiDiFlyPos.Count < 3)
					{
						Vector3 position = JinDus[j].icon_rect.position;
						ShuiDiFlyPos.Add(position);
					}
				}
			}
		}
		if (MainUIScript.Instance != null && MainUIScript.Instance.huodong_gardon_main != null)
		{
			MainUIScript.Instance.huodong_gardon_main.SetResult();
		}
		if (hgsd.isFullItem())
		{
			closeBtn_go.SetActive(value: false);
			SetPlayShow(isShow: false);
		}
		else
		{
			if (OpenType == 1)
			{
				isShowOpenBtn = 0f;
				SetPlayShow(isShow: false);
			}
			else
			{
				SetPlayShow(isShow: true);
			}
			closeBtn_go.SetActive(value: false);
		}
		PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHuoDongGardonTutorial01();
		if (hgsd.Tutorial03 == 0 && PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HuoDongGardonTutorial01 <= 1)
		{
			WordGlobal.ShowTutorialNPC(TextLibraryDataManagerCsv.instance.GetText(1220));
			hgsd.SetTutorial(3);
		}
		if (hgsd.FirstOpen == 0)
		{
			WordGlobal.LogEvent("HuoDongGardonFirstOpen");
			hgsd.SetFirstOpen();
		}
		if (StartShowAddTime)
		{
			BtnAddClick();
			StartShowAddTime = false;
		}
		if (AdsManager.Instance.isShowBanner)
		{
			HideNeedShowBanner = true;
			AdsManager.Instance.HideBannerAD();
		}
	}

	public void SetPlayShow(bool isShow)
	{
		if (isShow)
		{
			if (hgsd.isFullItem())
			{
				playAndPowerBtn_go.SetActive(value: false);
			}
			else
			{
				playAndPowerBtn_go.SetActive(value: true);
			}
			if (ad_show)
			{
				ShowAD();
			}
		}
		else
		{
			playAndPowerBtn_go.SetActive(value: false);
		}
	}

	public void InitShuiDi()
	{
		for (int i = 0; i < JinDus.Length; i++)
		{
			JinDus[i].Init(i, hgsd.GetEveryRewardNeedNum(i), hgsd.GetRewardNeedNum(i + 1), this);
		}
		SetCurJinDu(flowerOpenNum, isSetFill: true);
		CheckJinDuShow();
	}

	public void SetCurJinDu(float item_amount, bool isSetFill = false)
	{
		for (int i = 0; i < JinDus.Length; i++)
		{
			JinDus[i].SetItemAmount(item_amount, isSetFill);
		}
	}

	public int GetRewardNumBtIndex(int index)
	{
		return hgsd.GetRewardGoldNum(index);
	}

	public bool CheckReward(int index)
	{
		int gold_num = 0;
		bool flag = hgsd.GiveReward(index, out gold_num);
		if (flag)
		{
			HuoDongGardonGetGold.GoldNum = gold_num;
		}
		return flag;
	}

	public void GiveGold(int gold_num, Vector3 pos)
	{
		gold_script._isAddGold = true;
		gold_script.AddGold(gold_num, 1f, 1f, isAdd2PlayerInfo: false, 10);
		gold_script.CollectGoldNew(pos, 1, 1f, null);
		CheckJinDuShow();
		if (hgsd.isFullItem())
		{
			ShowCongratulates(isAnim: true);
			closeBtn_go.SetActive(value: false);
		}
	}

	public void ShowCongratulates(bool isAnim)
	{
		if (GameObject.Find("HuoDongGardonEndUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonEndUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
		}
	}

	public void CheckJinDuShow()
	{
		for (int i = 0; i < JinDus.Length; i++)
		{
			JinDus[i].CheckJinDuShow();
		}
	}

	private void Update()
	{
		if (isResetGold)
		{
			if (gold_script != null)
			{
				gold_script.ResetGold();
			}
			isResetGold = false;
		}
		count_time += Time.deltaTime;
		if (count_time > 1f)
		{
			huodong_timeleft = WordGlobal.GetHuoDongGardonTimeLeft();
			if (huodong_timeleft < 0)
			{
				huodong_timeleft = 0;
				Hide();
			}
			string text = WordGlobal.ChangeTimeToNomral(huodong_timeleft, isNeedDay: false);
			timeTxt.text = text;
			UpdatePowerTimeTxt();
			count_time = 0f;
		}
		if (HuoDongGardonFullEff.ShowType == 2)
		{
			WordGlobal.ShowHuoDongGardonMainFullEff(3);
		}
		if (isShowOpenBtn > 0f)
		{
			isShowOpenBtn -= Time.deltaTime;
			if (isShowOpenBtn <= 0f)
			{
				if (!isFlowerOpenAnim)
				{
					if (hgsd.isFullItem())
					{
						closeBtn_go.SetActive(value: true);
					}
					SetPlayShow(isShow: true);
				}
				else
				{
					isShowOpenBtn = 0.2f;
				}
			}
		}
		if (tempPowerNum != hgsd.PowerNum)
		{
			PowerNumTxt.text = hgsd.PowerNum + string.Empty;
			UpdatePowerTimeTxt();
			tempPowerNum = hgsd.PowerNum;
		}
		if (cut_powerNum_time > 0f)
		{
			cut_powerNum_time -= Time.deltaTime;
			if (cut_powerNum_time <= 0f)
			{
				HeartAnim_go.SetActive(value: true);
				hgsd.AddPower(-1);
				if (cutP_showaddtime)
				{
					BtnAddClick();
					cutP_showaddtime = false;
				}
			}
		}
		if (ad_waitTime > 0f)
		{
			ad_waitTime -= Time.deltaTime;
			if (ad_waitTime <= 0f)
			{
				ShowAD();
			}
		}
		if (isBtnCloseClick)
		{
			BtnCloseClick();
			isBtnCloseClick = false;
		}
		if (RefreshGold)
		{
			gold_script._isAddGold = false;
			RefreshGold = false;
		}
		AddWillOpenNum();
		UpdateAddJinDuNum();
	}

	public void ShowAD()
	{
		if (AdCount <= 0)
		{
			AdsManager.Instance.ShowInterstitialAd("Pop_ForceAd_Gardon");
			AdCount = 2;
		}
		else
		{
			AdCount--;
		}
	}

	public void AddWillOpenNum()
	{
		if (isAddWillOpenNum)
		{
			flowerWillOpenEndNum++;
			if (flowerWillOpenEndNum >= hgsd.HuoDongItemAmount)
			{
				flowerWillOpenEndNum = hgsd.HuoDongItemAmount;
			}
			isAddWillOpenNum = false;
		}
	}

	public void UpdateAddJinDuNum()
	{
		if (!((float)flowerWillOpenEndNum > flowerOpenNum))
		{
			return;
		}
		flowerOpenNum += Time.deltaTime * add_speed;
		if (flowerOpenNum >= (float)flowerWillOpenEndNum)
		{
			if (flowerWillOpenEndNum >= hgsd.HuoDongItemAmount)
			{
				isShowOpenBtn = 0.2f;
			}
			flowerOpenNum = flowerWillOpenEndNum;
		}
		SetCurJinDu(flowerOpenNum);
	}

	public void UpdatePowerTimeTxt()
	{
		PowerTimeTxt.text = hgsd.GetNextPowerTimeLeft();
		if (hgsd.isFull())
		{
			add_img.gameObject.SetActive(value: false);
			add_clickGo.SetActive(value: false);
		}
		else
		{
			add_img.gameObject.SetActive(value: true);
			add_clickGo.SetActive(value: true);
		}
	}

	public void PlayBtnClick()
	{
		if (hgsd.PowerNum <= 0)
		{
			BtnAddClick();
		}
		else
		{
			ShowHuoDongGardonMainUI();
		}
	}

	private void Hide()
	{
		Object.Destroy(base.gameObject);
	}

	public void BtnCloseHuoDongClick()
	{
		BtnCloseClick();
	}

	public void BtnAddClick()
	{
		if (GameObject.Find("HuoDongGardonAddTime") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonAddTime", isPop: true, UIManager.MainUIType.Type_Common, 99);
		}
		count_time = 1f;
	}

	public void BtnCloseClick()
	{
		if (playAndPowerBtn_go.activeInHierarchy || hgsd.isFullItem())
		{
			if (hgsd.isFullItem() && hgsd != null)
			{
				hgsd.SetCloseHuoDong();
			}
			if (MainUIScript.Instance != null)
			{
				MainUIScript.Instance.BtnCloseClick();
			}
			Hide();
		}
	}

	public void BtnHelpClick()
	{
		if (GameObject.Find("HuoDongGardonHelpUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonHelpUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
		}
	}

	public void ShowHuoDongGardonMainUI()
	{
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_EnterLevel);
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.HideLevelMapUI();
		}
		if (MainUIScript.Instance == null)
		{
			UIManager.Instance.LoadMainUI("UI/MainUI", isPop: false, UIManager.MainUIType.Type_HDGardon);
		}
		else
		{
			MainUIScript.Instance.Init();
		}
		if (hgsd != null && hgsd.FirstPlay == 0)
		{
			WordGlobal.LogEvent("HuoDongGardonFirstPlay");
			hgsd.SetFirstPlay();
		}
		Hide();
	}
}
