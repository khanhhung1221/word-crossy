using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongGardonUI_JinDU : MonoBehaviour
{
	public RectTransform JD_rectTran;

	public Image jd_fill_img;

	public Text num_text;

	public RectTransform gold_rect;

	public RectTransform icon_rect;

	public SkeletonGraphic img;

	public Text rewardGoldNum;

	private Image gold_img;

	private int idx;

	private int maxNum;

	private int totalNum;

	private HuoDongGardonUI hdgu;

	private int cur_animState = -1;

	public int NeedItemNum;

	private float cur_fill;

	public bool isTest;

	public string Test = "open";

	private float waitsound;

	private void Start()
	{
		gold_img = gold_rect.GetComponent<Image>();
	}

	public void Init(int idx, int maxNum, int totalNum, HuoDongGardonUI hdgu)
	{
		this.idx = idx;
		this.maxNum = maxNum;
		this.totalNum = totalNum;
		this.hdgu = hdgu;
		int rewardNumBtIndex = hdgu.GetRewardNumBtIndex(idx);
		rewardGoldNum.text = rewardNumBtIndex + string.Empty;
	}

	public void CheckJinDuShow()
	{
		if (!PlayerInfo.Instance.GetHuoDongGardonSaveData().GetRewardNum.Contains(idx))
		{
			JD_rectTran.gameObject.SetActive(value: true);
		}
		else
		{
			JD_rectTran.gameObject.SetActive(value: false);
		}
	}

	public void SetAnimState(int state, float delay = 0f)
	{
		if (cur_animState != state)
		{
			int num = cur_animState;
			cur_animState = state;
			if (cur_animState == 0)
			{
				SetAnim("start", 0f, null);
			}
			else if (cur_animState == 1)
			{
				waitsound = delay;
				HuoDongGardonUI.isFlowerOpenAnim = true;
				SetAnim("open", delay, PlayAnimEnd);
			}
			else if (cur_animState == 2 && num != 1)
			{
				SetAnim("loop", 0f, null, isLoop: true);
			}
		}
	}

	public void CountNeedItemNum(int itemAmount)
	{
		int num = itemAmount - totalNum;
		if (num >= 0)
		{
			NeedItemNum = 0;
		}
		else
		{
			NeedItemNum = Mathf.Abs(num);
		}
	}

	public void SetItemAmount(float itemAmount, bool isSetFill = false)
	{
		float num = itemAmount - (float)totalNum;
		float num2 = 0f;
		if (num >= 0f)
		{
			num2 = maxNum;
			if (hdgu.CheckReward(idx))
			{
				float delay = 0.5f;
				SetAnimState(1, delay);
			}
			else
			{
				SetAnimState(2);
			}
		}
		else
		{
			num2 = (float)maxNum + num;
			SetAnimState(0);
		}
		if (num2 < 0f)
		{
			num2 = 0f;
		}
		cur_fill = num2 / (float)maxNum;
		num_text.text = (int)num2 + "/" + maxNum;
		jd_fill_img.fillAmount = cur_fill;
	}

	public void PlayAnimEnd(TrackEntry trackEntry)
	{
		HuoDongGardonUI.isFlowerOpenAnim = false;
		HuoDongGardonGetGold.hdgui = hdgu;
		HuoDongGardonGetGold.IconStr = gold_img.sprite.name;
		if (GameObject.Find("HuoDongGardonGetGold") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonGetGold", isPop: true, UIManager.MainUIType.Type_Common, 99);
		}
	}

	public void SetAnim(string anim, float delay, Spine.AnimationState.TrackEntryDelegate animEnd, bool isLoop = false)
	{
		TrackEntry trackEntry = img.AnimationState.SetAnimation(0, anim, isLoop);
		trackEntry.Delay = delay;
		trackEntry.Complete += animEnd;
	}

	private void Update()
	{
		if (isTest)
		{
			SetAnim(Test, 0f, null, isLoop: true);
			isTest = false;
		}
		if (waitsound > 0f)
		{
			waitsound -= Time.deltaTime;
			if (waitsound <= 0f)
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_HuoDongGardonList[1]);
			}
		}
	}
}
