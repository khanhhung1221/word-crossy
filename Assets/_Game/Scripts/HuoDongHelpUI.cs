using UnityEngine;
using UnityEngine.UI;

public class HuoDongHelpUI : MonoBehaviour
{
	public static bool isHuoDongGardonTutorialPause;

	public static int des_txtid;

	public static int des_txtid02;

	public Text des_txt;

	public Text btn_txt;

	private void Start()
	{
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(des_txtid);
		if (des_txtid02 != 0)
		{
			Text text = des_txt;
			text.text = text.text + "\n" + TextLibraryDataManagerCsv.instance.GetText(des_txtid02);
		}
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1007);
		if (isHuoDongGardonTutorialPause)
		{
			base.gameObject.GetComponent<Button>().enabled = false;
		}
	}

	private void OnDestroy()
	{
		isHuoDongGardonTutorialPause = false;
	}
}
