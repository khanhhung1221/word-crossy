using UnityEngine;

public class HuoDongItemSp : MonoBehaviour
{
	private RectTransform rectTran;

	public bool isT;

	public Vector3 pos;

	private void Start()
	{
	}

	public RectTransform GetRectTran()
	{
		if (rectTran == null)
		{
			rectTran = (base.transform as RectTransform);
		}
		return rectTran;
	}

	private void Update()
	{
		if (isT)
		{
			Debug.LogError(base.transform.position);
			base.transform.position = pos;
			isT = false;
		}
	}
}
