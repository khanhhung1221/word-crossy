using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongMainTip : MonoBehaviour
{
	public RectTransform move_tran;

	public Text text;

	public Image img;

	public Sprite[] sps;

	private int type;

	private float out_y = 100f;

	private float in_y = -45f;

	public int cur_state;

	public float wait_time;

	private bool isHide;

	private void Start()
	{
		text.text = TextLibraryDataManagerCsv.instance.GetText(1322);
	}

	public void SetState(int s, float wait_t = 0f)
	{
		cur_state = s;
		if (cur_state == 0)
		{
			Hide();
		}
		else if (cur_state == 1)
		{
			float endValue = in_y - WordGlobal.GetSaftTopSize();
			move_tran.DOAnchorPosY(endValue, 0.5f);
			wait_time = wait_t;
			isHide = false;
		}
		else if (cur_state == 2)
		{
			move_tran.DOAnchorPosY(out_y, 0.5f);
			wait_time = wait_t;
			isHide = false;
		}
	}

	public void Init(bool isRunningInit = false)
	{
		if (WordGlobal.GetHuoDongTimeLeft() > 0 && PlayerInfo.Instance.GetHuoDongSaveData().HuoDongID > 0 && PlayerInfo.Instance.GetHuoDongSaveData().GetRewardState == 0)
		{
			SetState(0);
			return;
		}
		if (WordGlobal.GetHuoDongTournamentTimeLeft() > 0 && PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongID > 0)
		{
			SetState(0);
			return;
		}
		move_tran.gameObject.SetActive(value: true);
		RectTransform rectTransform = move_tran;
		Vector2 anchoredPosition = move_tran.anchoredPosition;
		rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, out_y);
		if (WordGlobal.GetHuoDongGardonTimeLeft() > 0)
		{
			img.sprite = sps[0];
			type = 0;
			SetState(2, 15f);
			if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().IsTodayOpenHuoDong(0))
			{
				SetState(0);
			}
		}
		else if (WordGlobal.GetHuoDongSunTimeLeft() > 0)
		{
			img.sprite = sps[1];
			type = 1;
			SetState(2, 15f);
			if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().IsTodayOpenHuoDong(1))
			{
				SetState(0);
			}
		}
		else
		{
			SetState(0);
		}
	}

	public void Hide()
	{
		move_tran.gameObject.SetActive(value: false);
		isHide = true;
	}

	private void Update()
	{
		if (isHide)
		{
			return;
		}
		if (WordGlobal.GetHuoDongTimeLeft() > 0 && PlayerInfo.Instance.GetHuoDongSaveData().HuoDongID > 0 && PlayerInfo.Instance.GetHuoDongSaveData().GetRewardState == 0)
		{
			SetState(0);
			return;
		}
		if (type == 0)
		{
			if (WordGlobal.GetHuoDongGardonTimeLeft() <= 0)
			{
				SetState(0);
			}
		}
		else if (type == 1 && WordGlobal.GetHuoDongSunTimeLeft() <= 0)
		{
			SetState(0);
		}
		if (!(wait_time > 0f))
		{
			return;
		}
		wait_time -= Time.deltaTime;
		if (wait_time <= 0f)
		{
			if (cur_state == 1)
			{
				SetState(2, 30f);
			}
			else if (cur_state == 2)
			{
				SetState(1, 15f);
			}
		}
	}

	public void OnClick()
	{
		if (type == 0)
		{
			WordGlobal.LevelMapOpenShowUI = "HuoDongGardon";
			if ((bool)MainUIScript.Instance)
			{
				BtnsCtrlUIScript.isShowPopOffer = false;
				UIManager.Instance.ClearPop();
				MainUIScript.Instance.BtnCloseClick();
			}
			WordGlobal.LogEvent("TapEventFlag_Flower");
		}
		else if (type == 1)
		{
			WordGlobal.LevelMapOpenShowUI = "HuoDongSun";
			if ((bool)MainUIScript.Instance)
			{
				BtnsCtrlUIScript.isShowPopOffer = false;
				UIManager.Instance.ClearPop();
				MainUIScript.Instance.BtnCloseClick();
			}
			WordGlobal.LogEvent("TapEventFlag_Sun");
		}
	}
}
