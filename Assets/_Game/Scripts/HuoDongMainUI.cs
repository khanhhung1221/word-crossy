using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongMainUI : MonoBehaviour
{
	public static float wordItemSize;

	public HuoDongItemSp itemsp;

	public GameObject icon_go;

	public Image icon_img;

	public GameObject xuehua01_go;

	public GameObject xuehua02_go;

	public GameObject xuehua03_go;

	public GameObject endMove_go;

	public GameObject eggs_eff_stand;

	public GameObject eggs_eff_tuowei;

	public Animator anim;

	public string Word;

	public CrossWordItem crossWordItem;

	private List<ItemScript> wordItemScripts = new List<ItemScript>();

	private ParticleSystem[] pss;

	private float pss_size = 1f;

	private ItemScript curItemScript;

	private int curItemScript_idx;

	private int CompleteWord_count;

	private float gotoNextWaitFrame;

	private float hide_icon_time;

	public bool isCompleteGetItem;

	public bool isGetItemFlyAnim;

	private bool isUpdateCheck;

	public bool isCheckCompleteWord;

	public float flyUpDelayTime;

	private bool WaitChangePos;

	private bool isNeedEndMove = true;

	private float showXueHua02Time;

	private float hideXueHua01Time;

	public bool isTest;

	private void Start()
	{
	}

	public IEnumerator ShowEff(GameObject go, float show_time)
	{
		go.SetActive(value: true);
		yield return new WaitForSeconds(show_time);
		go.SetActive(value: false);
	}

	public void Show(bool isShow)
	{
		Debug.Log("显示huodongmainUI:" + isShow);
		base.gameObject.SetActive(isShow);
		icon_go.SetActive(isShow);
	}

	public void Init(WordListItem _item)
	{
		wordItemScripts.Clear();
		Word = string.Empty;
		Show(isShow: true);
		Sprite sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/HuoDong/Christmas_snowicon");
		icon_img.sprite = sprite;
		for (int i = 0; i < _item.Words.Count; i++)
		{
			string text = _item.Words[i];
			if (!text.Equals(_item.MoneyWord) && text.Length > Word.Length)
			{
				Word = text;
				crossWordItem = _item.WordList[i];
			}
		}
		for (int j = 0; j < crossWordItem.Word.Length; j++)
		{
			int num = crossWordItem.Row;
			int num2 = crossWordItem.Col;
			if (crossWordItem.vertical)
			{
				num += j;
			}
			else
			{
				num2 += j;
			}
			string text2 = "item-" + (num - 1).ToString() + "-" + (num2 - 1).ToString();
			int index = _item.MaxCol * (num - 1) + num2 - 1;
			GameObject gameObject = MainUIScript.Instance.m_TfItemRoot.GetChild(index).gameObject;
			ItemScript component = gameObject.GetComponent<ItemScript>();
			wordItemScripts.Add(component);
		}
		CompleteWord_count = MainUIScript.Instance._currentWordList.Count;
		curItemScript_idx = PlayerInfo.Instance.GetHuoDongSaveData().HuoDongItemPosIdx;
		gotoNextWaitFrame = 0.2f;
		itemsp.transform.DOKill();
		itemsp.gameObject.SetActive(value: false);
		itemsp.transform.localScale = Vector3.zero;
		isUpdateCheck = false;
		showXueHua02Time = 0.5f;
		xuehua02_go.SetActive(value: false);
		xuehua01_go.SetActive(value: false);
		xuehua03_go.SetActive(value: true);
		anim.Play("Easter_eggs_stand");
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_HuoDongList[0]);
		isCompleteGetItem = false;
		isGetItemFlyAnim = false;
		pss = base.gameObject.GetComponentsInChildren<ParticleSystem>(includeInactive: true);
	}

	public void Check()
	{
		isUpdateCheck = true;
	}

	public void GotoNext(bool isAnim = true)
	{
		Debug.Log("huodongmainui gotoNext isanim:" + isAnim);
		curItemScript_idx++;
		if (curItemScript_idx > wordItemScripts.Count - 1)
		{
			WordGlobal.DebugLog("没有获得活动道具，道具飞走");
			anim.Play("Easter_eggs_get");
			hide_icon_time = 1.6f;
			xuehua01_go.SetActive(value: false);
			xuehua02_go.SetActive(value: false);
			xuehua03_go.SetActive(value: false);
			endMove_go.SetActive(value: false);
			if (isNeedEndMove)
			{
				endMove_go.SetActive(value: true);
				isNeedEndMove = false;
			}
			itemsp.gameObject.SetActive(value: false);
			PlayerInfo.Instance.GetHuoDongSaveData().SetMaxLevel(PlayerInfo.Instance.MaxPlayLevel);
			PlayerInfo.Instance.GetHuoDongSaveData().SetHuoDongItemPosIdx(-1);
			return;
		}
		isNeedEndMove = true;
		itemsp.gameObject.SetActive(value: true);
		curItemScript = wordItemScripts[curItemScript_idx];
		PlayerInfo.Instance.GetHuoDongSaveData().SetHuoDongItemPosIdx(curItemScript_idx - 1);
		if (curItemScript._imgState == 1)
		{
			WordGlobal.DebugLog("GotoNext isAnim:" + isAnim + " isCompleteGetItem:" + isCompleteGetItem);
			if (isAnim)
			{
				if (isCompleteGetItem)
				{
					itemsp.transform.DOKill(complete: true);
				}
				else
				{
					itemsp.transform.DOKill();
				}
				float duration = 1.23f;
				showXueHua02Time = 1.23f;
				hideXueHua01Time = 1f;
				xuehua01_go.SetActive(value: false);
				xuehua01_go.SetActive(value: true);
				anim.Play("Easter_eggs_jump");
				xuehua02_go.SetActive(value: false);
				Vector3[] path = WordGlobal.CountCirclePoints(itemsp.transform.position, curItemScript.transform.position);
				itemsp.transform.DOPath(path, duration).SetDelay(0.1f).OnComplete(delegate
				{
					xuehua01_go.SetActive(value: false);
					endMove_go.SetActive(value: false);
					endMove_go.SetActive(value: true);
					anim.Play("Easter_eggs_stand");
				});
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_HuoDongList[1]);
			}
			else
			{
				Vector3 position = curItemScript.transform.position;
				if (position.y > 6f)
				{
					WaitChangePos = true;
				}
				else
				{
					itemsp.transform.position = curItemScript.transform.position;
				}
			}
			itemsp.GetRectTran().sizeDelta = curItemScript.GetRectTran().sizeDelta;
		}
		else
		{
			GotoNext(isAnim);
		}
	}

	private void Update()
	{
		if (isTest)
		{
			GotoNext();
			isTest = false;
		}
		if (gotoNextWaitFrame > 0f)
		{
			gotoNextWaitFrame -= Time.deltaTime;
			if (gotoNextWaitFrame <= 0f)
			{
				itemsp.transform.DOScale(wordItemSize, 0.5f);
				itemsp.gameObject.SetActive(value: true);
				GotoNext(isAnim: false);
			}
		}
		if (hide_icon_time > 0f)
		{
			hide_icon_time -= Time.deltaTime;
			if (hide_icon_time <= 0f)
			{
				icon_go.SetActive(value: false);
			}
		}
		if (WaitChangePos && curItemScript != null)
		{
			Vector3 position = curItemScript.transform.position;
			if (!(position.y > 6f))
			{
				itemsp.transform.position = curItemScript.transform.position;
				WaitChangePos = false;
			}
		}
		UpdateCheck();
		if (showXueHua02Time > 0f)
		{
			showXueHua02Time -= Time.deltaTime;
			if (showXueHua02Time <= 0f)
			{
				anim.Play("Easter_eggs_stand");
				xuehua02_go.SetActive(value: true);
			}
		}
		if (hideXueHua01Time > 0f)
		{
			hideXueHua01Time -= Time.deltaTime;
			if (hideXueHua01Time <= 0f)
			{
				xuehua01_go.SetActive(value: false);
			}
		}
		if (pss_size != wordItemSize)
		{
			for (int i = 0; i < pss.Length; i++)
			{
				pss[i].transform.localScale = new Vector3(wordItemSize, wordItemSize, wordItemSize);
			}
			pss_size = wordItemSize;
		}
	}

	public void UpdateCheck()
	{
		if (flyUpDelayTime > 0f)
		{
			flyUpDelayTime -= Time.deltaTime;
		}
		else
		{
			if (!isUpdateCheck || crossWordItem == null || !(curItemScript != null))
			{
				return;
			}
			string[] array = curItemScript.gameObject.name.Split('-');
			int row = int.Parse(array[1]) + 1;
			int col = int.Parse(array[2]) + 1;
			if (MainUIScript.Instance.CheckOneWordCompleted(row, col))
			{
				if (PlayerInfo.Instance.GetHuoDongSaveData().GetItemMaxLevel < PlayerInfo.Instance.MaxPlayLevel)
				{
					float duration = 1.23f;
					hideXueHua01Time = 1f;
					xuehua01_go.SetActive(value: false);
					xuehua01_go.SetActive(value: true);
					anim.Play("Easter_eggs_jump");
					StartCoroutine(ShowEff(eggs_eff_tuowei, 1.5f));
					xuehua02_go.SetActive(value: false);
					PlayerInfo.Instance.GetHuoDongSaveData().SetMaxLevel(PlayerInfo.Instance.MaxPlayLevel);
					itemsp.transform.DOKill();
					isCompleteGetItem = true;
					isGetItemFlyAnim = true;
					AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_HuoDongList[2]);
					Debug.Log("============");
					Debug.Log("获得01");
					itemsp.transform.DOMove(MainUIScript.Instance.huodong_flyPos.position, duration).SetDelay(0.2f).OnComplete(delegate
					{
						PlayerInfo.Instance.GetHuoDongSaveData().AddItemAmount();
						PlayerInfo.Instance.GetHuoDongSaveData().SetHuoDongItemPosIdx(-1);
						showXueHua02Time = 0f;
						if (xuehua02_go.activeInHierarchy)
						{
							xuehua02_go.SetActive(value: false);
						}
						endMove_go.SetActive(value: false);
						endMove_go.SetActive(value: true);
						isGetItemFlyAnim = false;
						anim.Play("Easter_eggs_stand");
						if (MainUIScript.Instance != null && MainUIScript.Instance.huodong_effgo != null)
						{
							MainUIScript.Instance.huodong_effgo.SetActive(value: true);
						}
						icon_go.SetActive(value: false);
						itemsp.gameObject.SetActive(value: false);
						Debug.Log("获得02");
						WordGlobal.DebugLog("获得活动道具");
					});
				}
			}
			else
			{
				int num = 0;
				if (curItemScript._imgState == 2)
				{
					num = 1;
				}
				if (MainUIScript.Instance._currentWordList.Count > CompleteWord_count && isCheckCompleteWord)
				{
					num = 1;
					CompleteWord_count = MainUIScript.Instance._currentWordList.Count;
					isCheckCompleteWord = false;
				}
				for (int i = 0; i < num; i++)
				{
					GotoNext();
				}
			}
			isUpdateCheck = false;
		}
	}
}
