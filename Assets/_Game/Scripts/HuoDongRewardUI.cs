using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongRewardUI : MonoBehaviour
{
	public Text title_txt;

	public HuoDongRewardItem[] items;

	public Vector2[] EndPos01;

	public Vector2[] EndPos02;

	public Vector2[] EndPos03;

	public Vector2[] EndPos04;

	private float show_time;

	private HuoDongSaveData hdsd;

	private EventDataCsv edc;

	private void Start()
	{
		show_time = 2f;
		hdsd = PlayerInfo.Instance.GetHuoDongSaveData();
		edc = EventDataManagerCsv.instance.GetDataById(hdsd.HuoDongID);
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1305);
		bool flag = false;
		if (hdsd.GetRewardState == 0)
		{
			flag = true;
		}
		if (!flag)
		{
			WordGlobal.DebugLog("已经领取过了");
			Hide();
		}
		else
		{
			for (int i = 0; i < items.Length; i++)
			{
				items[i].gameObject.SetActive(value: false);
			}
			int count = edc.items.Count;
			Vector2[] array = EndPos01;
			if (count == 1)
			{
				array = EndPos01;
			}
			if (count == 2)
			{
				array = EndPos02;
			}
			if (count == 3)
			{
				array = EndPos03;
			}
			if (count == 4)
			{
				array = EndPos04;
			}
			for (int j = 0; j < edc.items.Count; j++)
			{
				PlayerInfo.Instance.AddItemByStr(edc.items[j], 7);
				string[] array2 = edc.items[j].Split('_');
				int num = int.Parse(array2[0]);
				int num2 = 0;
				switch (num)
				{
				case 1:
				{
					int num3 = int.Parse(array2[1]);
					num2 = int.Parse(array2[2]);
					switch (num3)
					{
					case 1001:
						items[j].img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_lightbulb");
						items[j].img.rectTransform.sizeDelta = new Vector2(104f, 104f);
						break;
					case 1002:
						items[j].img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_finger");
						items[j].img.rectTransform.sizeDelta = new Vector2(104f, 104f);
						break;
					case 1003:
						items[j].img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_lightning");
						items[j].img.rectTransform.sizeDelta = new Vector2(104f, 104f);
						break;
					}
					break;
				}
				case 3:
					num2 = int.Parse(array2[1]);
					items[j].img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/jinbi_common_04");
					items[j].img.rectTransform.sizeDelta = new Vector2(126f, 95f);
					break;
				}
				items[j].num.text = "X" + num2;
				(items[j].transform as RectTransform).DOAnchorPos(array[j], 0.5f).SetDelay(show_time + (float)j * 0.1f).SetEase(Ease.OutBack)
					.OnComplete(delegate
					{
						AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_HuoDongList[3]);
					});
			}
		}
		PlayerInfo.Instance.GetHuoDongSaveData().SetRewardState(1);
		WordGlobal.isUpdateLevelMapHuoDong = true;
	}

	public void Hide()
	{
		Object.Destroy(base.gameObject);
	}

	private void OnDestroy()
	{
		if (MainUIScript.Instance != null)
		{
			MainUIScript.Instance.CheckHuoDongGetItem = true;
		}
		HuoDongUI.isHide = true;
	}

	private void Update()
	{
		if (!(show_time > 0f))
		{
			return;
		}
		show_time -= Time.deltaTime;
		if (show_time <= 0f)
		{
			for (int i = 0; i < edc.items.Count; i++)
			{
				items[i].gameObject.SetActive(value: true);
			}
		}
	}
}
