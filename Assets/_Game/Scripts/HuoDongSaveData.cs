using System;
using UnityEngine;

[Serializable]
public class HuoDongSaveData
{
	public int HuoDongID;

	public int HuoDongItemAmount;

	public float HuoDongItemAmount_AnimNum;

	public int HuoDongItemPosIdx = -1;

	public int GetItemMaxLevel;

	public int GetRewardState;

	public int Tutorial01;

	public int Tutorial02;

	public int NoUseLevel;

	public string[] card_gotItem = new string[9];

	public string last_openCard = string.Empty;

	public int NextHaveLevel;

	public static int PerRewardItemNum = 5;

	private int per_minLv = 3;

	private int per_maxLv = 4;

	private string[] ItemArr = new string[9]
	{
		"3_5",
		"3_10",
		"3_15",
		"3_20",
		"3_30",
		"3_50",
		"1_1001_1",
		"1_1002_1",
		"1_1003_1"
	};

	private int[] WeightArr = new int[9]
	{
		40,
		10,
		10,
		10,
		6,
		2,
		10,
		10,
		2
	};

	public string GetItemStr()
	{
		string empty = string.Empty;
		int num = 0;
		if (num <= 0)
		{
			for (int i = 0; i < WeightArr.Length; i++)
			{
				num += WeightArr[i];
			}
		}
		int num2 = UnityEngine.Random.Range(0, num);
		int num3 = 0;
		for (int j = 0; j < WeightArr.Length; j++)
		{
			if (num2 - WeightArr[j] < 0)
			{
				num3 = j;
				break;
			}
			num2 -= WeightArr[j];
		}
		empty = ItemArr[num3];
		if (empty.Equals(last_openCard))
		{
			Debug.Log("随机到上次物品，放弃掉");
			empty = GetItemStr();
		}
		Debug.Log("item_str:" + empty + " ran_weight_num:" + num2 + " weight_levelIdx:" + num3);
		return empty;
	}

	public string OpenCard(int idx)
	{
		if (HuoDongItemAmount >= PerRewardItemNum)
		{
			HuoDongItemAmount -= PerRewardItemNum;
			string empty = string.Empty;
			int num = 0;
			for (int i = 0; i < card_gotItem.Length; i++)
			{
				if (!string.IsNullOrEmpty(card_gotItem[i]))
				{
					num++;
				}
			}
			float num2 = -1f;
			if (num >= 7)
			{
				num2 = 100f;
			}
			else if (num >= 6)
			{
				num2 = 50f;
			}
			else if (num >= 5)
			{
				num2 = 30f;
			}
			int num3 = UnityEngine.Random.Range(0, 100);
			if ((float)num3 <= num2)
			{
				empty = "BigReward";
			}
			else
			{
				empty = GetItemStr();
				PlayerInfo.Instance.AddItemByStr(empty, 8);
			}
			card_gotItem[idx] = empty;
			last_openCard = empty;
			return empty;
		}
		return null;
	}

	public void SetHuoDongItemPosIdx(int idx)
	{
		HuoDongItemPosIdx = idx;
		PlayerInfo.Instance.SaveHuoDongSaveData();
	}

	public void SetMaxLevel(int level)
	{
		GetItemMaxLevel = level;
		RefreshNextHaveLevel(isSave: false);
		PlayerInfo.Instance.SaveHuoDongSaveData();
	}

	public void SetTutorial01(int v)
	{
		Tutorial01 = v;
		PlayerInfo.Instance.SaveHuoDongSaveData();
	}

	public void SetTutorial02(int v)
	{
		Tutorial02 = v;
		PlayerInfo.Instance.SaveHuoDongSaveData();
	}

	public void SetRewardState(int state)
	{
		GetRewardState = state;
		PlayerInfo.Instance.SaveHuoDongSaveData();
	}

	public void InitHuoDongID()
	{
		HuoDongID = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 1);
		HuoDongItemAmount = 0;
		HuoDongItemAmount_AnimNum = 0f;
		GetRewardState = 0;
		NoUseLevel = 0;
		card_gotItem = new string[9];
		last_openCard = string.Empty;
		HuoDongItemPosIdx = -1;
		GetItemMaxLevel = PlayerInfo.Instance.MaxPlayLevel - 1;
		if (!string.IsNullOrEmpty(PlayerInfo.Instance.GameSaveDatas))
		{
			GameSaveData gameSaveData = JsonUtility.FromJson<GameSaveData>(PlayerInfo.Instance.GameSaveDatas);
			if (gameSaveData != null)
			{
				gameSaveData.InitGrid();
				for (int i = 0; i < gameSaveData.Grid.Count; i++)
				{
					for (int j = 0; j < gameSaveData.Grid[i].Count; j++)
					{
						if (gameSaveData.Grid[i][j] != "-" && gameSaveData.Grid[i][j] != string.Empty && gameSaveData.Grid[i][j] != "|")
						{
							NoUseLevel = gameSaveData.Level;
							break;
						}
					}
				}
			}
		}
		EventDataCsv dataById = EventDataManagerCsv.instance.GetDataById(HuoDongID);
		if (dataById != null)
		{
			if (PlayerInfo.Instance.MaxPlayLevel < dataById.startlv)
			{
				NextHaveLevel = dataById.startlv;
			}
			else if (NoUseLevel != 0)
			{
				RefreshNextHaveLevel(isSave: false);
			}
			else
			{
				NextHaveLevel = PlayerInfo.Instance.MaxPlayLevel;
			}
			WordGlobal.HuoDongRefreshLevelMapIconIndex++;
		}
		PlayerInfo.Instance.SaveHuoDongSaveData();
	}

	public void RefreshNextHaveLevel(bool isSave = true)
	{
		int num = UnityEngine.Random.Range(per_minLv, per_maxLv);
		NextHaveLevel = PlayerInfo.Instance.MaxPlayLevel + num;
		if (isSave)
		{
			PlayerInfo.Instance.SaveHuoDongSaveData();
		}
	}

	public bool IsUseLevel(int level)
	{
		if (level == NoUseLevel)
		{
			return false;
		}
		if (level == NextHaveLevel)
		{
			return true;
		}
		return false;
	}

	public bool isShowIconInLevelMapLevels(int level)
	{
		if (WordGlobal.GetHuoDongTimeLeft() > 0 && HuoDongID > 0 && GetRewardState == 0 && level == NextHaveLevel)
		{
			return true;
		}
		return false;
	}

	public void AddItemAmount()
	{
		HuoDongItemAmount++;
		EventDataCsv dataById = EventDataManagerCsv.instance.GetDataById(HuoDongID);
		if (dataById != null && HuoDongItemAmount > 0 && HuoDongItemAmount % 5 == 0)
		{
			MainUIScript.Instance.SetShowHuoDongUI();
		}
		PlayerInfo.Instance.SaveHuoDongSaveData();
	}
}
