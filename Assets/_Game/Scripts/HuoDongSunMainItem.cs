using DG.Tweening;
using UnityEngine;

public class HuoDongSunMainItem : MonoBehaviour
{
	public int State;

	public RectTransform item_rectTran;

	public int row;

	public int col;

	private HuoDongSunMainUI mainui;

	public ItemScript curItemScript;

	public CrossWordItem crossWordItem;

	private void Start()
	{
	}

	public void Init(int r, int c, CrossWordItem crossWordItem, HuoDongSunMainUI mainui)
	{
		row = r - 1;
		col = c - 1;
		this.mainui = mainui;
		this.crossWordItem = crossWordItem;
		State = 1;
		GameObject gameObject = GameObject.Find("item-" + row + "-" + col);
		curItemScript = gameObject.GetComponent<ItemScript>();
		item_rectTran.transform.position = curItemScript.transform.position;
		item_rectTran.sizeDelta = curItemScript.GetRectTran().sizeDelta;
		item_rectTran.localScale = Vector3.zero;
		item_rectTran.DOScale(1f, 0.2f);
		base.gameObject.SetActive(value: true);
	}

	public bool Check(int row, int col)
	{
		if (State == 1 && this.row == row - 1 && this.col == col - 1)
		{
			AddItem();
			return true;
		}
		return false;
	}

	public void Out()
	{
		item_rectTran.DOScale(0f, 0.2f);
	}

	public void AddItem()
	{
		State = 2;
		mainui.ToAddSunItem();
		float duration = 0.5f;
		Vector3[] path = WordGlobal.CountCirclePoints(item_rectTran.transform.position, MainUIScript.Instance.huodong_flyPos.position);
		item_rectTran.transform.DOPath(path, duration).OnComplete(delegate
		{
			mainui.AddSunScore();
			State = 0;
			base.gameObject.SetActive(value: false);
		});
		item_rectTran.DOSizeDelta(new Vector2(81f, 81f), duration);
	}
}
