using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HuoDongSunMainUI : MonoBehaviour
{
	private GameSaveData gameSaveData;

	private HuoDongSunSaveData hdssd;

	private int curSunScore;

	private List<HuoDongSunMainItem> items = new List<HuoDongSunMainItem>();

	private bool isCheck;

	private float gotoNextWaitFrame = 0.2f;

	public float flyUpDelayTime;

	private bool isUpdateToAddSun;

	public List<int> AddedSunLetter = new List<int>();

	private void Start()
	{
	}

	public void Init(GameSaveData gameSaveD)
	{
		gameSaveData = gameSaveD;
		hdssd = PlayerInfo.Instance.GetHuoDongSunSaveData();
		AddedSunLetter.Clear();
		gotoNextWaitFrame = 0.2f;
		for (int i = 0; i < items.Count; i++)
		{
			items[i].State = 0;
			items[i].gameObject.SetActive(value: false);
		}
		if (PlayerInfo.Instance.GetHuoDongSunSaveData().Tutorial01 == 0 && PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HuoDongSunTutorial02 <= 2)
		{
			if (GameObject.Find("HuoDongTutorial01") == null)
			{
				HuoDongTutorial01.TextId = 1231;
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTutorial01", isPop: true, UIManager.MainUIType.Type_Common, 99);
			}
			PlayerInfo.Instance.GetHuoDongSunSaveData().SetTutorial();
		}
	}

	public void RealInit()
	{
		if (gameSaveData == null)
		{
			curSunScore = 0;
			InitNewSun();
			return;
		}
		curSunScore = gameSaveData.CurSunScore;
		string[] array = gameSaveData.HDSunPos.Split('&');
		for (int i = 0; i < array.Length; i++)
		{
			string[] array2 = array[i].Split('_');
			if (array2.Length == 3)
			{
				int row = int.Parse(array2[0]);
				int col = int.Parse(array2[1]);
				string word_str = array2[2];
				CrossWordItem wordByStr = MainUIScript.Instance.GetWordByStr(word_str);
				if (wordByStr != null)
				{
					SetSun(row, col, wordByStr);
				}
			}
		}
	}

	public void Result()
	{
		if (PlayerInfo.Instance.AdRemove == 1)
		{
			RealResult();
		}
		else if (AdsManager.Instance.ShowInterstitialAd("Pop_ForceAd_Sun"))
		{
			BtnVideoScript.isStop = true;
			AdsManager.ShowInterstitialAdEvent = (UnityAction)Delegate.Combine(AdsManager.ShowInterstitialAdEvent, new UnityAction(RewardADEvent));
		}
		else
		{
			BtnVideoScript.isStop = true;
			RealResult();
		}
	}

	private void RewardADEvent()
	{
		AdsManager.ShowInterstitialAdEvent = (UnityAction)Delegate.Remove(AdsManager.ShowInterstitialAdEvent, new UnityAction(RewardADEvent));
		RealResult();
	}

	public void RealResult()
	{
		curSunScore = 0;
		for (int i = 0; i < items.Count; i++)
		{
			items[i].State = 0;
		}
		if (WordGlobal.GetHuoDongSunTimeLeft() <= 0)
		{
			if (MainUIScript.Instance != null)
			{
				MainUIScript.Instance.BtnCloseClick();
			}
			return;
		}
		HuoDongSunUI.ShowType = 1;
		HuoDongSunUI.isInAnim = true;
		if (GameObject.Find("HuoDongSunUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongSunUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
		}
	}

	public void Check()
	{
		isCheck = true;
	}

	public List<CrossWordItem> GetCrossWordItem(int num)
	{
		List<CrossWordItem> unFinishedWordList = MainUIScript.Instance.GetUnFinishedWordList();
		List<CrossWordItem> list = new List<CrossWordItem>();
		List<CrossWordItem> list2 = new List<CrossWordItem>();
		for (int i = 0; i < unFinishedWordList.Count; i++)
		{
			CrossWordItem crossWordItem = unFinishedWordList[i];
			bool flag = false;
			for (int j = 0; j < items.Count; j++)
			{
				if (items[j].State == 1 && items[j].crossWordItem.Equals(crossWordItem))
				{
					flag = true;
				}
			}
			if (!flag)
			{
				list.Add(crossWordItem);
			}
		}
		if (num > 0)
		{
			if (num > list.Count)
			{
				num = list.Count;
			}
			if (num > 0)
			{
				List<int> list3 = new List<int>();
				for (int k = 0; k < list.Count; k++)
				{
					list3.Add(k);
				}
				RandomArray<int> randomArray = new RandomArray<int>(list3.ToArray());
				int[] disruptedItems = randomArray.GetDisruptedItems();
				for (int l = 0; l < num; l++)
				{
					list2.Add(list[disruptedItems[l]]);
				}
			}
		}
		return list2;
	}

	public void InitNewSun()
	{
		if (MainUIScript.Instance.GetUnFinishedWordList().Count >= hdssd.SunMaxNum)
		{
			int num = 0;
			do
			{
				AddSunItem(hdssd.SunMaxNum - GetUseSunItemCount(), isSetLetter: true);
				num++;
			}
			while (GetUseSunItemCount() < hdssd.SunMaxNum && num < 50);
		}
		else
		{
			Debug.LogError("关卡没有词levelID:" + PlayerInfo.Instance.GetHuoDongSunSaveData().GetCurLevelID());
		}
	}

	public int GetUseSunItemCount()
	{
		int num = 0;
		for (int i = 0; i < items.Count; i++)
		{
			if (items[i].State == 1)
			{
				num++;
			}
		}
		return num;
	}

	public void ToAddSunItem()
	{
		isUpdateToAddSun = true;
	}

	public void UpdateToAddSun()
	{
		if (isUpdateToAddSun)
		{
			int useSunItemCount = GetUseSunItemCount();
			int count = MainUIScript.Instance.GetUnFinishedWordList().Count;
			int num = 0;
			num = ((count <= hdssd.SunMaxNum) ? count : hdssd.SunMaxNum);
			if (useSunItemCount < num)
			{
				AddSunItem(1);
			}
			else
			{
				isUpdateToAddSun = false;
			}
		}
	}

	public void AddSunItem(int num, bool isSetLetter = false)
	{
		List<CrossWordItem> crossWordItem = GetCrossWordItem(num);
		for (int i = 0; i < crossWordItem.Count; i++)
		{
			CrossWordItem crossWordItem2 = crossWordItem[i];
			if (isSetLetter)
			{
				if (isHaveSun(crossWordItem2.Row - 1, crossWordItem2.Col - 1))
				{
					continue;
				}
				MainUIScript.Instance.SetCell(crossWordItem2.Col, crossWordItem2.Row, crossWordItem2.Word.Substring(0, 1), initItem: true, removeGold: true);
			}
			int num2 = crossWordItem2.Col;
			int num3 = crossWordItem2.Row;
			bool flag = false;
			for (int j = 0; j < crossWordItem2.Word.Length; j++)
			{
				if (j > 0)
				{
					if (crossWordItem2.vertical)
					{
						num3++;
					}
					else
					{
						num2++;
					}
				}
				if (isHaveSun(num3 - 1, num2 - 1))
				{
					continue;
				}
				string text = "item-" + (num3 - 1).ToString() + "-" + (num2 - 1).ToString();
				Transform x = MainUIScript.Instance.m_TfItemRoot.Find(text);
				if (x != null)
				{
					int imgState = MainUIScript.Instance.m_TfItemRoot.Find(text).GetComponent<ItemScript>()._imgState;
					if (imgState == 1)
					{
						flag = true;
						break;
					}
				}
				else
				{
					Debug.LogError("tran is null => " + text);
				}
			}
			if (flag)
			{
				SetSun(num3, num2, crossWordItem2);
			}
		}
	}

	public bool isHaveSun(int r, int col)
	{
		int item = r * 100 + col;
		if (AddedSunLetter.Contains(item))
		{
			return true;
		}
		for (int i = 0; i < items.Count; i++)
		{
			if (items[i].State == 1 && items[i].row == r && items[i].col == col)
			{
				return true;
			}
		}
		return false;
	}

	public void SunItemOut()
	{
		for (int i = 0; i < items.Count; i++)
		{
			items[i].Out();
		}
	}

	public void SetSun(int row, int col, CrossWordItem item)
	{
		HuoDongSunMainItem unUseSunItem = GetUnUseSunItem();
		unUseSunItem.Init(row, col, item, this);
	}

	public HuoDongSunMainItem GetUnUseSunItem()
	{
		for (int i = 0; i < items.Count; i++)
		{
			if (items[i].State == 0)
			{
				return items[i];
			}
		}
		GameObject gameObject = UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongSunMainItem");
		gameObject.transform.SetParent(base.transform);
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		gameObject.transform.SetAsLastSibling();
		HuoDongSunMainItem component = gameObject.GetComponent<HuoDongSunMainItem>();
		items.Add(component);
		return component;
	}

	public string GetSunPos()
	{
		string text = string.Empty;
		for (int i = 0; i < items.Count; i++)
		{
			if (items[i].State == 1)
			{
				string text2 = text;
				text = text2 + (items[i].row + 1) + "_" + (items[i].col + 1) + "_" + items[i].crossWordItem.Word + "&";
			}
		}
		return text;
	}

	public int GetSunScore()
	{
		return curSunScore;
	}

	public void AddSunScore()
	{
		curSunScore++;
	}

	public void CheckItem(int row, int col)
	{
		for (int i = 0; i < items.Count; i++)
		{
			items[i].Check(row, col);
		}
	}

	private void Update()
	{
		if (gotoNextWaitFrame > 0f)
		{
			gotoNextWaitFrame -= Time.deltaTime;
			if (gotoNextWaitFrame <= 0f)
			{
				RealInit();
			}
		}
		UpdateToAddSun();
	}
}
