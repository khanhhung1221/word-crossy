using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HuoDongSunSaveData
{
	public int HuoDongID;

	public int HuoDongItemAmount;

	public int HuoDongSun_getLevelCount = -1;

	public bool IsCurLevelComplete = true;

	public int GetRewardCount;

	public int LastLevelId;

	public int Tutorial01;

	public int Tutorial02;

	public int TutorialOpen;

	private EventDataCsv edc;

	private int[] reward_nums;

	private int sunMaxNum = 3;

	private int curLevelGetSun;

	private List<int> NeedGetRewardIndex = new List<int>();

	public int SunMaxNum => sunMaxNum;

	public int CurLevelGetSun
	{
		get
		{
			return curLevelGetSun;
		}
		set
		{
			curLevelGetSun = value;
		}
	}

	public void InitHuoDongID()
	{
		HuoDongID = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 3);
		GetEventData();
		LastLevelId = 0;
		HuoDongItemAmount = 0;
		HuoDongSun_getLevelCount = -1;
		IsCurLevelComplete = true;
		GetRewardCount = 0;
		Init();
		PlayerInfo.Instance.SaveHuoDongSunSaveData();
	}

	public void SetTutorial()
	{
		Tutorial01 = 1;
		PlayerInfo.Instance.SaveHuoDongSunSaveData();
	}

	public void SetTutorial02()
	{
		Tutorial02 = 1;
		PlayerInfo.Instance.SaveHuoDongSunSaveData();
	}

	public void SetTutorialOpen()
	{
		TutorialOpen = 1;
		PlayerInfo.Instance.SaveHuoDongSunSaveData();
	}

	public void Init()
	{
		if (HuoDongID > 0)
		{
			string[] array = GetEventData().other.Split('_');
			reward_nums = new int[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				reward_nums[i] = int.Parse(array[i]);
			}
		}
	}

	public int[] GetRewardItem()
	{
		return reward_nums;
	}

	public int GetItemMaxNum()
	{
		if (reward_nums.Length > 0)
		{
			return reward_nums[reward_nums.Length - 1];
		}
		return 0;
	}

	public void AddItemAmount(int num)
	{
		HuoDongItemAmount += num;
		if (HuoDongItemAmount >= GetItemMaxNum())
		{
			HuoDongItemAmount = GetItemMaxNum();
		}
		IsCurLevelComplete = true;
		CheckReward();
		PlayerInfo.Instance.SaveHuoDongSunSaveData();
	}

	public bool isFull()
	{
		if (HuoDongItemAmount >= GetItemMaxNum())
		{
			return true;
		}
		return false;
	}

	public void CheckReward()
	{
		if (GetRewardCount < reward_nums.Length)
		{
			int num = reward_nums[GetRewardCount];
			if (HuoDongItemAmount >= num)
			{
				string itemStr = GetEventData().items[GetRewardCount];
				PlayerInfo.Instance.AddItemByStr(itemStr, 4);
				NeedGetRewardIndex.Add(GetRewardCount);
				GetRewardCount++;
			}
		}
	}

	public List<int> GetNeedGetRewardIndex()
	{
		return NeedGetRewardIndex;
	}

	public int GetCurLevelID()
	{
		int result = 0;
		GetEventData();
		if (edc != null)
		{
			int min = int.Parse(edc.require);
			int requireamount = edc.requireamount;
			if (IsCurLevelComplete)
			{
				HuoDongSun_getLevelCount++;
				IsCurLevelComplete = false;
			}
			for (int i = 0; i < 10; i++)
			{
				int num = UnityEngine.Random.Range(min, requireamount);
				if (num != LastLevelId)
				{
					LastLevelId = num;
					result = num;
					break;
				}
			}
			PlayerInfo.Instance.SaveHuoDongSunSaveData();
		}
		return result;
	}

	public EventDataCsv GetEventData()
	{
		if (edc == null)
		{
			edc = EventDataManagerCsv.instance.GetDataByType(3);
		}
		return edc;
	}
}
