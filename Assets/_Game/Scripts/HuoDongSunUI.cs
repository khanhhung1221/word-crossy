using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongSunUI : MonoBehaviour
{
	public static bool isBtnCloseClick;

	public static bool isResetGold;

	public static int ShowType;

	public static bool isInAnim;

	public GameObject bg_go;

	public RectTransform main_rect;

	public HuoDongSunUIKeDu[] kedu;

	public Text title_desText;

	public Text time_text;

	public Image Jindu_up;

	public Text SunNum_text;

	public GameObject playBtn_go;

	public Text playBtn_txt;

	public RectTransform top_tran;

	public GoldScript gold_script;

	private HuoDongSunSaveData hdssd;

	private float itemAmount;

	private float jd_width = 558f;

	private float add_speed;

	private bool isInAniming;

	private bool HideNeedShowBanner;

	private float count_time;

	public bool isTest;

	public int TestNum = 1000;

	private void Start()
	{
		WordGlobal.ResetTopPos(top_tran);
		title_desText.text = TextLibraryDataManagerCsv.instance.GetText(1229);
		playBtn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1075);
		hdssd = PlayerInfo.Instance.GetHuoDongSunSaveData();
		Debug.Log(hdssd.GetRewardItem());
		if (hdssd.HuoDongID == 0)
		{
			hdssd.InitHuoDongID();
		}
		PlayerInfo.Instance.GetPlayerOtherInfoSaveData().OpenHuoDongUI(1);
		ResourceLoadManager.GetInstance().HideNetWaiting();
		itemAmount = hdssd.HuoDongItemAmount;
		InitKeDu();
		SetJinDu();
		isResetGold = false;
		if (gold_script == null)
		{
			gold_script = WordGlobal.AddGoldUI(top_tran);
			gold_script._isAddGold = true;
			gold_script.m_ShowBanner = true;
			if (gold_script.banner_go != null)
			{
				gold_script.banner_go.GetComponent<Canvas>().sortingOrder = 20;
			}
		}
		if (ShowType == 1)
		{
			hdssd.AddItemAmount(hdssd.CurLevelGetSun);
			add_speed = hdssd.CurLevelGetSun;
			WordFaceBook.Instance.UpdateUserdataMill();
		}
		if (hdssd.HuoDongItemAmount >= hdssd.GetItemMaxNum())
		{
			playBtn_go.SetActive(value: false);
		}
		else
		{
			playBtn_go.SetActive(value: true);
		}
		count_time = 1f;
		InAnim();
		PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHuoDongSunTutorial02(0);
		if (AdsManager.Instance.isShowBanner)
		{
			HideNeedShowBanner = true;
			AdsManager.Instance.HideBannerAD();
		}
	}

	public void InAnim()
	{
		if (isInAnim)
		{
			isInAniming = true;
			bg_go.SetActive(value: false);
			RectTransform rectTransform = main_rect;
			float x = Screen.width * 2;
			Vector2 anchoredPosition = main_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(x, anchoredPosition.y);
			main_rect.DOAnchorPosX(0f, 0.5f).SetDelay(0.2f).OnComplete(delegate
			{
				isInAniming = false;
			});
		}
		else
		{
			bg_go.SetActive(value: true);
		}
		isInAnim = false;
	}

	public void InitKeDu()
	{
		for (int i = 0; i < hdssd.GetRewardItem().Length; i++)
		{
			int num = hdssd.GetRewardItem()[i];
			float num2 = (float)num / (float)hdssd.GetItemMaxNum();
			string reward = hdssd.GetEventData().items[i];
			if (i < kedu.Length)
			{
				RectTransform rectTransform = kedu[i].transform as RectTransform;
				if (num2 > 0.99f)
				{
					num2 = 0.95f;
				}
				RectTransform rectTransform2 = rectTransform;
				float x = jd_width * num2;
				Vector2 anchoredPosition = rectTransform.anchoredPosition;
				rectTransform2.anchoredPosition = new Vector2(x, anchoredPosition.y);
				kedu[i].Init(i, reward, num, this);
			}
		}
	}

	public void AddGoldEff(int goldNum, Vector3 flyPos)
	{
		if (gold_script != null)
		{
			gold_script._isAddGold = true;
			gold_script.AddGold(goldNum, 1f, 1f, isAdd2PlayerInfo: false, 11);
			gold_script.CollectGoldNew(flyPos, 1, 1f, null);
		}
	}

	public void SetJinDu()
	{
		SunNum_text.text = (int)itemAmount + string.Empty;
		float fillAmount = itemAmount / (float)hdssd.GetItemMaxNum();
		Jindu_up.fillAmount = fillAmount;
		if (ShowType == 1)
		{
			for (int i = 0; i < kedu.Length; i++)
			{
				kedu[i].Check((int)itemAmount);
			}
		}
	}

	private void Update()
	{
		if (isTest)
		{
			AddGoldEff(TestNum, kedu[0].transform.position);
			isTest = false;
		}
		if (isResetGold)
		{
			if (gold_script != null)
			{
				gold_script.ResetGold();
			}
			isResetGold = false;
		}
		if (ShowType == 1 && !isInAniming && itemAmount < (float)hdssd.HuoDongItemAmount)
		{
			itemAmount += Time.deltaTime * add_speed;
			if (itemAmount >= (float)hdssd.HuoDongItemAmount)
			{
				itemAmount = hdssd.HuoDongItemAmount;
			}
			SetJinDu();
		}
		count_time += Time.deltaTime;
		if (count_time > 1f)
		{
			long num = WordGlobal.GetHuoDongSunTimeLeft();
			if (num < 0)
			{
				num = 0L;
				Hide();
			}
			string text = WordGlobal.ChangeTimeToNomral(num, isNeedDay: false);
			time_text.text = text;
			count_time = 0f;
		}
		if (isBtnCloseClick)
		{
			BtnCloseClick();
			isBtnCloseClick = false;
		}
	}

	private void Hide()
	{
		Object.Destroy(base.gameObject);
	}

	public void BtnCloseClick()
	{
		if (MainUIScript.Instance != null)
		{
			MainUIScript.Instance.isHuoDongSunDestroy = true;
			MainUIScript.Instance.BtnCloseClick();
		}
		Hide();
	}

	public void BtnPlayClick()
	{
		ShowHuoDongSunMainUI();
	}

	public void ShowHuoDongSunMainUI()
	{
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_EnterLevel);
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.HideLevelMapUI();
		}
		if (MainUIScript.Instance == null)
		{
			UIManager.Instance.LoadMainUI("UI/MainUI", isPop: false, UIManager.MainUIType.Type_HDSun);
		}
		else
		{
			MainUIScript.Instance.Init();
		}
		Hide();
	}
}
