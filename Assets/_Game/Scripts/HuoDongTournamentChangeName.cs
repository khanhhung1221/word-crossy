using UnityEngine;
using UnityEngine.UI;

public class HuoDongTournamentChangeName : MonoBehaviour
{
	public Text des_txt;

	public Text title_txt;

	public Text btn_txt;

	public InputField input;

	private void Start()
	{
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1291);
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1292);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1279);
	}

	private void Update()
	{
	}

	public void OKClick()
	{
		if (input.text != null)
		{
			if (input.text.Length < 3)
			{
				UIManager.Instance.ShowTip_DesClose(TextLibraryDataManagerCsv.instance.GetText(1293));
				return;
			}
			Debug.Log(input.text.Length + " " + input.text);
			string text = input.text;
			WordFaceBook.Instance.GetTournamentData(text);
			Object.Destroy(base.gameObject);
		}
	}
}
