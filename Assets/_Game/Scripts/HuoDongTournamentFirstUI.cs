using UnityEngine;
using UnityEngine.UI;

public class HuoDongTournamentFirstUI : MonoBehaviour
{
	public Text title_txt;

	public Text time_txt;

	public Text des_txt;

	public Text btn_txt;

	private float wait_time;

	private void Start()
	{
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1271);
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1273);
		btn_txt.text = string.Format(WordGlobal.ReadText(1004), PlayerInfo.Instance.MaxPlayLevel);
		UpdateTime();
	}

	private void Update()
	{
		wait_time += Time.deltaTime;
		if (wait_time >= 1f)
		{
			UpdateTime();
			wait_time = 0f;
		}
	}

	public void UpdateTime()
	{
		int huoDongTournamentTimeLeft = WordGlobal.GetHuoDongTournamentTimeLeft();
		if (huoDongTournamentTimeLeft > 0 && PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongID > 0)
		{
			time_txt.text = WordGlobal.ChangeTimeToNormalSpecial(huoDongTournamentTimeLeft - WordGlobal.HuoDongTournamentFinishtTime, 0, isNeedDay: false);
		}
	}

	public void BtnClick()
	{
		UIManager.CurMainUIType = UIManager.MainUIType.Type_Common;
		if (MainUIScript.Instance != null)
		{
			PlayerInfo.Instance.CurrentLevel = PlayerInfo.Instance.MaxPlayLevel;
			MainUIScript.Instance.Init();
		}
		else if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.ShowMainUI(PlayerInfo.Instance.MaxPlayLevel);
		}
		Object.Destroy(base.gameObject);
	}
}
