using UnityEngine;

public class HuoDongTournamentMainFlyItem : MonoBehaviour
{
	public GameObject img_go;

	public GameObject addEff_go;

	public Animator anim;

	private float wait_time;

	private void Start()
	{
	}

	public void ShowEff(bool isAdd)
	{
		if (isAdd)
		{
			addEff_go.SetActive(value: true);
			anim.enabled = true;
			anim.Play("jiangbei_levelUp");
		}
		else
		{
			MainResultScript.PlayTournamentAnimCount++;
			anim.enabled = false;
			wait_time = 0.1f;
		}
	}

	private void Update()
	{
		if (wait_time > 0f)
		{
			wait_time -= Time.deltaTime;
			if (wait_time <= 0f)
			{
				Object.Destroy(base.gameObject);
			}
		}
	}
}
