using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HuoDongTournamentMainItem : MonoBehaviour
{
	public static bool isAnimEnd;

	public Text num_txt;

	public RectTransform flyPos;

	public RectTransform itemParent;

	public int current_num;

	public GameObject item_go;

	private bool isStartResult;

	private int anim_idx;

	private Vector3 resultFlyPos;

	private float finishWaitTime;

	private void Start()
	{
	}

	public void SendScore()
	{
		if (WordGlobal.GetHuoDongTournamentTimeLeft() <= WordGlobal.HuoDongTournamentFinishtTime || PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardState != 0)
		{
			return;
		}
		if (WordGlobal.UpdateHuoDongTournamentInfoIdx == 2)
		{
			int num = 0;
			if (MainUIScript.Instance != null)
			{
				num = MainUIScript.Instance._bonusList.Count + MainUIScript.Instance._currentWordList.Count;
			}
			PlayerInfo.Instance.GetHuoDongTournamentSaveData().AddItemAmount(num);
		}
		WordFaceBook.Instance.GetTournamentData(string.Empty);
	}

	public void Init()
	{
		isStartResult = false;
		if (MainUIScript.Instance != null)
		{
			current_num = MainUIScript.Instance._bonusList.Count + MainUIScript.Instance._currentWordList.Count;
		}
		Show();
		if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().Tutorial02 == 0 && WordGlobal.GetHuoDongTournamentTimeLeft() > WordGlobal.HuoDongTournamentFinishtTime && !PlayerInfo.Instance.GetPlayerOtherInfoSaveData().isHuoDongTournamentTutorialShow[1])
		{
			if (GameObject.Find("HuoDongTutorial01") == null)
			{
				HuoDongTutorial01.TextId = 1288;
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTutorial01", isPop: true, UIManager.MainUIType.Type_Common, 99);
			}
			PlayerInfo.Instance.GetHuoDongTournamentSaveData().SetTutorial02();
		}
	}

	public void StartResult(Vector3 pos)
	{
		if (!isStartResult)
		{
			itemParent.GetComponent<Canvas>().sortingOrder = 13;
			resultFlyPos = pos;
			anim_idx = 0;
			isStartResult = true;
			isAnimEnd = false;
		}
	}

	public void Show()
	{
		int num = MainUIScript.Instance._bonusList.Count + MainUIScript.Instance._currentWordList.Count;
		if (current_num >= num)
		{
			current_num = num;
		}
		if (current_num <= 0)
		{
			current_num = 0;
		}
		num_txt.text = current_num + string.Empty;
		base.gameObject.SetActive(value: true);
	}

	public void LevelAddCup(Vector3 start_pos, float delay_time = 0f, float showDelay_time = 0f)
	{
		StartCoroutine(FlyCups(start_pos, flyPos.position, AddCup, delay_time, showDelay_time));
	}

	public void AddCup()
	{
		current_num++;
		Show();
	}

	public void ResultFlyCup(Vector3 start_pos, Vector3 end_pos, float delay_time = 0f, int cut_num_per = 1)
	{
		StartCoroutine(CutCup(delay_time, cut_num_per));
		StartCoroutine(FlyCups(start_pos, end_pos, null, delay_time, 0f, 1.5f, isAdd: false));
	}

	public IEnumerator CutCup(float delay_time, int cut_num_per)
	{
		yield return new WaitForSeconds(delay_time);
		current_num -= cut_num_per;
		Show();
	}

	public IEnumerator FlyCups(Vector3 start_pos, Vector3 end_pos, UnityAction end_act = null, float delay_time = 0f, float showDelay_time = 0f, float ScaleEndV = 0.8f, bool isAdd = true)
	{
		yield return new WaitForSeconds(showDelay_time);
		GameObject fly_go = Object.Instantiate(item_go);
		fly_go.transform.SetParent(itemParent);
		fly_go.transform.position = start_pos;
		fly_go.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
		HuoDongTournamentMainFlyItem mfi = fly_go.GetComponent<HuoDongTournamentMainFlyItem>();
		float anim_time = 0.8f;
		if (isAdd)
		{
			mfi.ShowEff(isAdd);
			delay_time += 0.3f;
		}
		else
		{
			mfi.anim.enabled = false;
			fly_go.SetActive(value: false);
			anim_time = 0.4f;
		}
		fly_go.SetActive(value: true);
		ShortcutExtensions.DOPath(path: WordGlobal.CountCirclePoints(fly_go.transform.position, end_pos, 0f, 1), target: fly_go.transform, duration: anim_time).SetDelay(0.2f + delay_time).OnComplete(delegate
		{
			if (end_act != null)
			{
				end_act();
			}
			if (!isAdd)
			{
				mfi.ShowEff(isAdd);
			}
			else
			{
				Object.Destroy(fly_go);
			}
		});
		fly_go.transform.DOScale(ScaleEndV, anim_time).SetDelay(0.2f + delay_time);
	}

	private void Update()
	{
		if (!isStartResult)
		{
			return;
		}
		if (anim_idx == 0)
		{
			int num = current_num / 10 + 1;
			int num2 = current_num / num;
			if (current_num % num != 0)
			{
				num2++;
			}
			for (int i = 0; i < num2; i++)
			{
				float delay_time = (float)i * 0.15f;
				ResultFlyCup(flyPos.position, resultFlyPos, delay_time, num);
			}
			finishWaitTime = 1f;
			anim_idx = 1;
		}
		else
		{
			if (anim_idx != 1 || current_num > 0)
			{
				return;
			}
			finishWaitTime -= Time.deltaTime;
			if (finishWaitTime <= 0f)
			{
				itemParent.GetComponent<Canvas>().sortingOrder = 2;
				MainResultScript.isUpdateHuoDongTournamentInfo = true;
				base.transform.SetParent(MainUIScript.Instance.Top_rectTran);
				if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().Tutorial03 == 0 && WordGlobal.GetHuoDongTournamentTimeLeft() > WordGlobal.HuoDongTournamentFinishtTime && !PlayerInfo.Instance.GetPlayerOtherInfoSaveData().isHuoDongTournamentTutorialShow[2])
				{
					if (GameObject.Find("TutorialHuoDongTournament03") == null)
					{
						UIManager.Instance.LoadMainUI("UI/Main/TutorialHuoDongTournament03", isPop: true);
					}
					PlayerInfo.Instance.GetHuoDongTournamentSaveData().SetTutorial03();
				}
				isStartResult = false;
			}
			isAnimEnd = true;
		}
	}

	public void BtnClick()
	{
		if (!isStartResult && GameObject.Find("HuoDongTournamentUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTournamentUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
		}
	}
}
