using UnityEngine;
using UnityEngine.UI;

public class HuoDongTournamentRewardUI : MonoBehaviour
{
	public Text title_txt;

	public Text rankNum_txt;

	public Text des_txt;

	public Image reward_icon;

	public Text reward_num_txt;

	public Text btn_txt;

	private int gold_num;

	private void Start()
	{
		int myRank = PlayerInfo.Instance.GetHuoDongTournamentSaveData().MyRank;
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1271);
		des_txt.text = string.Format(TextLibraryDataManagerCsv.instance.GetText(1280), myRank);
		rankNum_txt.text = PlayerInfo.Instance.GetHuoDongTournamentSaveData().MyRank + string.Empty;
		gold_num = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardItemByRank(PlayerInfo.Instance.GetHuoDongTournamentSaveData().MyRank);
		reward_num_txt.text = gold_num + string.Empty;
		if (myRank <= 3)
		{
			reward_icon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/HuoDong/Tournament_jinbi_icon0" + myRank);
			return;
		}
		reward_icon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/HuoDong/Tournament_jinbi_icon04");
		reward_icon.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
	}

	public void BtnCollect()
	{
		PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetReward();
		Object.Destroy(base.gameObject);
	}
}
