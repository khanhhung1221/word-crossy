using System;
using UnityEngine;

[Serializable]
public class HuoDongTournamentSaveData
{
	public int HuoDongID;

	public int HuoDongItemAmount;

	public int MyRank;

	public int MyCups;

	public int GetRewardState;

	public int Tutorial01;

	public int Tutorial02;

	public int Tutorial03;

	public int Tutorial04;

	private EventDataCsv edc;

	private int[] reward_nums;

	private int min_levelID = 500;

	private int max_levelID = 4000;

	public int GetCurLevelID()
	{
		int num = UnityEngine.Random.Range(min_levelID, max_levelID);
		Debug.Log("tournament random_lv:" + num);
		return num;
	}

	public void InitHuoDongID()
	{
		HuoDongID = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 4);
		GetEventData();
		MyRank = 0;
		MyCups = 0;
		HuoDongItemAmount = 0;
		Init();
		PlayerInfo.Instance.SaveHuoDongTournamentSaveData();
	}

	public void CheckShowRewardUI()
	{
		int rewardItemByRank = GetRewardItemByRank(MyRank);
		if (rewardItemByRank > 0)
		{
			if (GetRewardState <= 0 && GameObject.Find("HuoDongTournamentRewardUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTournamentRewardUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
			}
		}
		else
		{
			GetRewardState = 1;
			PlayerInfo.Instance.SaveHuoDongTournamentSaveData();
		}
	}

	public void GetReward()
	{
		if (GetRewardState == 0)
		{
			int rewardItemByRank = GetRewardItemByRank(MyRank);
			if (MainUIScript.Instance != null)
			{
				GoldScript component = MainUIScript.Instance.m_GoldItem.GetComponent<GoldScript>();
				component.ResetGold();
				component.CollectGold(Vector3.zero, 5, 1f, null);
				component.AddGold(rewardItemByRank, 1f, 1f, isAdd2PlayerInfo: true, 20);
			}
			else if (LevelMapUIScript.Instance != null && LevelMapUIScript.Instance.GoldItem != null)
			{
				LevelMapUIScript.Instance.GoldItem.ResetGold();
				LevelMapUIScript.Instance.GoldItem.CollectGold(Vector3.zero, 5, 1f, null);
				LevelMapUIScript.Instance.GoldItem.AddGold(rewardItemByRank, 1f, 1f, isAdd2PlayerInfo: true, 21);
			}
			else
			{
				PlayerInfo.Instance.AddGold(rewardItemByRank, 19);
			}
			GetRewardState = 1;
			PlayerInfo.Instance.SaveHuoDongTournamentSaveData();
			WordFaceBook.Instance.GetTournamentData(string.Empty);
		}
	}

	public void SetEndRewardState()
	{
		GetRewardState = 2;
		PlayerInfo.Instance.SaveHuoDongTournamentSaveData();
	}

	public string GetMyRankNum()
	{
		if (MyRank > 0)
		{
			return MyRank + string.Empty;
		}
		return "50+";
	}

	public void SetTutorial()
	{
		Tutorial01 = 1;
		PlayerInfo.Instance.SaveHuoDongTournamentSaveData();
	}

	public void SetTutorial02()
	{
		Tutorial02 = 1;
		PlayerInfo.Instance.GetPlayerOtherInfoSaveData().SetHuoDongTournamentTutorialShow(1);
		PlayerInfo.Instance.SaveHuoDongTournamentSaveData();
	}

	public void SetTutorial03()
	{
		Tutorial03 = 1;
		PlayerInfo.Instance.GetPlayerOtherInfoSaveData().SetHuoDongTournamentTutorialShow(2);
		PlayerInfo.Instance.SaveHuoDongTournamentSaveData();
	}

	public void SetTutorial04()
	{
		Tutorial04 = 1;
		PlayerInfo.Instance.GetPlayerOtherInfoSaveData().SetHuoDongTournamentTutorialShow(3);
		PlayerInfo.Instance.SaveHuoDongTournamentSaveData();
	}

	public void Init()
	{
		if (GetEventData() != null)
		{
			string[] array = GetEventData().other.Split('_');
			reward_nums = new int[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				reward_nums[i] = int.Parse(array[i]);
			}
		}
	}

	public int GetRewardItemByRank(int rank)
	{
		int result = 0;
		int num = rank - 1;
		if (reward_nums == null)
		{
			Init();
		}
		if (num >= 0 && num < reward_nums.Length)
		{
			result = reward_nums[num];
		}
		return result;
	}

	public void AddItemAmount(int num)
	{
		HuoDongItemAmount += num;
		PlayerInfo.Instance.SaveHuoDongTournamentSaveData();
	}

	public EventDataCsv GetEventData()
	{
		if (edc == null)
		{
			edc = EventDataManagerCsv.instance.GetDataById(HuoDongID);
		}
		return edc;
	}
}
