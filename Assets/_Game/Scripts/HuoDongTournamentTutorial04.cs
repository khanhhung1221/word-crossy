using UnityEngine;
using UnityEngine.UI;

public class HuoDongTournamentTutorial04 : MonoBehaviour
{
	public static bool isShow;

	public static bool isHide;

	public static string tran_name = string.Empty;

	public Text des_txt;

	public RectTransform main;

	private GameObject btn_go;

	private void Start()
	{
		isShow = true;
		isHide = false;
		WordGlobal.DisableBackKey = true;
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1290);
		btn_go = GameObject.Find(tran_name);
		if (btn_go != null)
		{
			btn_go.transform.SetParent(base.transform);
			btn_go.transform.SetAsFirstSibling();
			RectTransform rectTransform = btn_go.transform as RectTransform;
			RectTransform rectTransform2 = main;
			Vector2 anchoredPosition = rectTransform.anchoredPosition;
			float x = anchoredPosition.x;
			Vector2 anchoredPosition2 = rectTransform.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(x, anchoredPosition2.y + 100f);
		}
	}

	private void Update()
	{
		if (isHide)
		{
			Object.Destroy(base.gameObject);
			isHide = false;
		}
	}

	private void OnDestroy()
	{
		isShow = false;
		WordGlobal.DisableBackKey = false;
		if (btn_go != null)
		{
			btn_go.GetComponent<HuoDongTournamentUIItem>().enabled = true;
			btn_go.GetComponent<HuoDongTournamentUIItem>().UpdatePos();
		}
	}
}
