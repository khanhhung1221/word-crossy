using UnityEngine;
using UnityEngine.UI;

public class HuoDongTournamentUI : MonoBehaviour
{
	public static bool isWaitNetInfo;

	public RectTransform top_rect;

	public Text title_txt;

	public Text time_txt;

	public Text des_txt;

	public Text rank_txt;

	public GameObject BtnPlay_go;

	public Text BtnPlay_text;

	public GameObject finished_go;

	public Text finished_rankTxt;

	public Text finished_rankNumTxt;

	public GameObject reward_go;

	public Text finished_rewardTxt;

	public Image finished_rewardIconImg;

	public Text finished_rewardNumTxt;

	public RectTransform content_rectTran;

	public RectTransform myItem_rectTran;

	public GameObject item_go;

	public GameObject JiaZai_go;

	public Transform JiaZai_yTran;

	public RectTransform Pos_Top;

	public RectTransform Pos_Down;

	private int my_rank;

	private bool isInit;

	private bool HideNeedShowBanner;

	private float huodongInfo_waitTime;

	private void Start()
	{
		WordGlobal.ResetTopPos(top_rect);
		my_rank = PlayerInfo.Instance.GetHuoDongTournamentSaveData().MyRank;
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1271);
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1274);
		finished_rankTxt.text = TextLibraryDataManagerCsv.instance.GetText(1272);
		finished_rewardTxt.text = TextLibraryDataManagerCsv.instance.GetText(1275);
		BtnPlay_text.text = TextLibraryDataManagerCsv.instance.GetText(1075);
		rank_txt.text = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum() + string.Empty;
		finished_rankNumTxt.text = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum() + string.Empty;
		int rewardItemByRank = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardItemByRank(my_rank);
		if (rewardItemByRank > 0)
		{
			reward_go.SetActive(value: true);
			finished_rewardNumTxt.text = rewardItemByRank + string.Empty;
			if (my_rank <= 3)
			{
				finished_rewardIconImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/HuoDong/Tournament_jinbi_icon0" + my_rank);
			}
			else
			{
				finished_rewardIconImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/HuoDong/Tournament_jinbi_icon04");
				finished_rewardIconImg.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
			}
		}
		else
		{
			reward_go.SetActive(value: false);
		}
		huodongInfo_waitTime = 1f;
		GetInfo();
		if (AdsManager.Instance.isShowBanner)
		{
			HideNeedShowBanner = true;
			AdsManager.Instance.HideBannerAD();
		}
	}

	public void GetInfo()
	{
		isWaitNetInfo = true;
		isInit = false;
		WordFaceBook.Instance.GetTournamentData(string.Empty);
	}

	public void Init()
	{
		isInit = true;
		InitItems();
	}

	public void InitItems()
	{
		for (int i = 0; i < WordGlobal.TournamentInfoList.Count; i++)
		{
			TournamentInfoData tournamentInfoData = WordGlobal.TournamentInfoList[i];
			GameObject gameObject = Object.Instantiate(item_go);
			gameObject.transform.SetParent(content_rectTran);
			gameObject.transform.localScale = Vector3.one;
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.name = "Item_" + tournamentInfoData.rankNum;
			RectTransform rectTransform = gameObject.transform as RectTransform;
			int rankNum = tournamentInfoData.rankNum;
			rectTransform.anchoredPosition = new Vector2(0f, 58 - rankNum * 132);
			gameObject.SetActive(value: true);
			HuoDongTournamentUIItem component = gameObject.GetComponent<HuoDongTournamentUIItem>();
			component.Init(this, tournamentInfoData);
		}
		if (my_rank <= 0)
		{
			TournamentInfoData tournamentInfoData2 = new TournamentInfoData();
			tournamentInfoData2.cupNum = PlayerInfo.Instance.GetHuoDongTournamentSaveData().MyCups;
			tournamentInfoData2.rankNum = PlayerInfo.Instance.GetHuoDongTournamentSaveData().MyRank;
			tournamentInfoData2.name = PlayerInfo.Instance.PlayerName;
			tournamentInfoData2.fbid = PlayerInfo.Instance.FaceBookID;
			if (PlayerOtherInfo.HalfPayAllFirstLettersTimeStamp > 0)
			{
				tournamentInfoData2.Vip = 1;
			}
			if (string.IsNullOrEmpty(tournamentInfoData2.fbid))
			{
				tournamentInfoData2.fbid = "1";
			}
			tournamentInfoData2.uuid = PlayerInfo.Instance.UUID;
			GameObject gameObject2 = Object.Instantiate(item_go);
			gameObject2.transform.SetParent(content_rectTran);
			gameObject2.transform.localScale = Vector3.one;
			gameObject2.transform.localPosition = Vector3.zero;
			gameObject2.transform.name = "Item_" + my_rank;
			RectTransform rectTransform2 = gameObject2.transform as RectTransform;
			rectTransform2.anchoredPosition = new Vector2(0f, -1000f);
			gameObject2.SetActive(value: true);
			HuoDongTournamentUIItem component2 = gameObject2.GetComponent<HuoDongTournamentUIItem>();
			component2.Init(this, tournamentInfoData2);
		}
		RectTransform rectTransform3 = content_rectTran;
		Vector2 sizeDelta = content_rectTran.sizeDelta;
		rectTransform3.sizeDelta = new Vector2(sizeDelta.x, WordGlobal.TournamentInfoList.Count * 132 + 20);
	}

	private void Update()
	{
		if (isWaitNetInfo)
		{
			JiaZai_go.SetActive(value: true);
			if (JiaZai_go.activeInHierarchy)
			{
				JiaZai_yTran.Rotate(0f, 0f, -360f * Time.deltaTime);
			}
		}
		else
		{
			JiaZai_go.SetActive(value: false);
			if (!isInit)
			{
				Init();
			}
		}
		UpdateTime();
	}

	public void UpdateTime()
	{
		huodongInfo_waitTime += Time.deltaTime;
		if (!(huodongInfo_waitTime >= 1f))
		{
			return;
		}
		int huoDongTournamentTimeLeft = WordGlobal.GetHuoDongTournamentTimeLeft();
		if (huoDongTournamentTimeLeft > 0)
		{
			if (huoDongTournamentTimeLeft - WordGlobal.HuoDongTournamentFinishtTime > 0)
			{
				time_txt.text = WordGlobal.ChangeTimeToNormalSpecial(huoDongTournamentTimeLeft - WordGlobal.HuoDongTournamentFinishtTime, 0, isNeedDay: false);
				finished_go.SetActive(value: false);
				BtnPlay_go.SetActive(value: true);
			}
			else
			{
				finished_go.SetActive(value: true);
				BtnPlay_go.SetActive(value: false);
				time_txt.text = TextLibraryDataManagerCsv.instance.GetText(1276);
				string arg = WordGlobal.ChangeTimeToNormalSpecial(huoDongTournamentTimeLeft, 0, isNeedDay: false);
				des_txt.text = string.Format(TextLibraryDataManagerCsv.instance.GetText(1277), arg);
			}
		}
	}

	public void BtnCloseClick()
	{
		Object.Destroy(base.gameObject);
	}

	public void BtnPlayClick()
	{
		if (MainUIScript.Instance != null)
		{
			if (MainUIScript.Instance.isShowResult())
			{
				MainResultScript.isTournamentNext = true;
			}
		}
		else
		{
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_EnterLevel);
			if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.HideLevelMapUI();
			}
			PlayLevel();
		}
		BtnCloseClick();
	}

	public void PlayLevel()
	{
		if (PlayerInfo.Instance.MaxPlayLevel > MapDataManager.instance.GetMaxLevel())
		{
			UIManager.Instance.LoadMainUI("UI/MainUI", isPop: false, UIManager.MainUIType.Type_HDTournament);
		}
		else if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.ShowMainUI(PlayerInfo.Instance.MaxPlayLevel);
		}
	}

	private void OnDestroy()
	{
		if (HideNeedShowBanner)
		{
			AdsManager.Instance.ShowBannerAD();
			HideNeedShowBanner = false;
		}
	}
}
