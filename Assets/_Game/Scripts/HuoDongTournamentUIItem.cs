using UnityEngine;
using UnityEngine.UI;

public class HuoDongTournamentUIItem : MonoBehaviour
{
	public static bool isMyChangeName;

	public Image bg_img;

	public Sprite my_bgSp;

	public GameObject otherNum_go;

	public Text otherNum_num;

	public Image RankNumIcon;

	public Sprite[] rankIcons;

	public Text name_txt;

	public RectTransform jiangbei_bg;

	public Text jiangbei_num_txt;

	public GameObject reward_go;

	public Image reward_icon;

	public Text reward_num;

	public Image head_icon;

	public GameObject vip_go;

	public Image head_up_img;

	private TournamentInfoData tid;

	private HuoDongTournamentUI hdtui;

	public bool isMyRank;

	private RectTransform my_rect;

	public float outPos_y;

	public float inRectPosY;

	public float pos_y;

	public float content_y;

	public float pos_top_y;

	public float pos_down_y;

	private float wait_tutorial;

	private void Start()
	{
	}

	public void Init(HuoDongTournamentUI hdtui, TournamentInfoData tid)
	{
		this.hdtui = hdtui;
		this.tid = tid;
		int myRank = PlayerInfo.Instance.GetHuoDongTournamentSaveData().MyRank;
		isMyRank = false;
		if (tid.Vip == 1)
		{
			vip_go.SetActive(value: true);
			head_up_img.color = WordGlobal.ChangeStringToColor("#fec02a");
		}
		else
		{
			vip_go.SetActive(value: false);
			head_up_img.color = Color.white;
		}
		if (tid.rankNum == myRank)
		{
			bg_img.sprite = my_bgSp;
			isMyRank = true;
			base.transform.name = "Item_" + tid.rankNum + "_my";
		}
		if (tid.rankNum <= 3 && tid.rankNum > 0)
		{
			otherNum_go.SetActive(value: false);
			RankNumIcon.gameObject.SetActive(value: true);
			RankNumIcon.sprite = rankIcons[tid.rankNum - 1];
		}
		else
		{
			otherNum_go.SetActive(value: true);
			RankNumIcon.gameObject.SetActive(value: false);
			if (tid.rankNum <= 0)
			{
				if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongItemAmount <= 0)
				{
					otherNum_go.SetActive(value: false);
				}
				else
				{
					otherNum_num.text = "50+";
				}
			}
			else
			{
				otherNum_num.text = tid.rankNum + string.Empty;
			}
		}
		name_txt.text = tid.name;
		if (tid.cupNum <= 999)
		{
			RectTransform rectTransform = jiangbei_bg;
			Vector2 sizeDelta = jiangbei_bg.sizeDelta;
			rectTransform.sizeDelta = new Vector2(90f, sizeDelta.y);
			RectTransform rectTransform2 = jiangbei_num_txt.rectTransform;
			Vector2 anchoredPosition = jiangbei_num_txt.rectTransform.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(52f, anchoredPosition.y);
		}
		else
		{
			RectTransform rectTransform3 = jiangbei_bg;
			Vector2 sizeDelta2 = jiangbei_bg.sizeDelta;
			rectTransform3.sizeDelta = new Vector2(105f, sizeDelta2.y);
			RectTransform rectTransform4 = jiangbei_num_txt.rectTransform;
			Vector2 anchoredPosition2 = jiangbei_num_txt.rectTransform.anchoredPosition;
			rectTransform4.anchoredPosition = new Vector2(59f, anchoredPosition2.y);
		}
		jiangbei_num_txt.text = tid.cupNum + string.Empty;
		int rewardItemByRank = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardItemByRank(tid.rankNum);
		if (rewardItemByRank > 0)
		{
			reward_go.SetActive(value: true);
			reward_num.text = rewardItemByRank + string.Empty;
			if (tid.rankNum <= 3)
			{
				reward_icon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/HuoDong/Tournament_jinbi_icon0" + tid.rankNum);
			}
			else
			{
				reward_icon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/HuoDong/Tournament_jinbi_icon04");
				reward_icon.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
			}
		}
		else
		{
			reward_go.SetActive(value: false);
		}
		if (tid.fbid.Length > 3)
		{
			StartCoroutine(WordGlobal.LoadFBHeadIcon(tid.fbid, head_icon, isSelf: false));
		}
		else
		{
			int num = int.Parse(tid.fbid) + 1;
			head_icon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/HuoDong/Tournament_head_" + num.ToString("d2"));
		}
		my_rect = (base.transform as RectTransform);
		if (hdtui != null && isMyRank)
		{
			Vector3 position = hdtui.content_rectTran.position;
			float y = position.y;
			Vector3 position2 = hdtui.Pos_Top.position;
			pos_top_y = position2.y;
			Vector3 position3 = hdtui.Pos_Down.position;
			pos_down_y = position3.y;
			Vector3 position4 = base.transform.position;
			pos_y = position4.y;
			outPos_y = y + (pos_down_y - pos_y + 0.5f);
			Vector2 anchoredPosition3 = my_rect.anchoredPosition;
			inRectPosY = anchoredPosition3.y;
			if (!string.IsNullOrEmpty(PlayerInfo.Instance.PlayerName))
			{
				name_txt.text = PlayerInfo.Instance.PlayerName;
			}
			else
			{
				name_txt.text = TextLibraryDataManagerCsv.instance.GetText(1285);
			}
			wait_tutorial = 0.1f;
		}
		UpdatePos();
	}

	private void Update()
	{
		UpdatePos();
		UpdateTutorial();
		if (isMyChangeName && hdtui != null && isMyRank)
		{
			name_txt.text = PlayerInfo.Instance.PlayerName;
			isMyChangeName = false;
		}
	}

	public void UpdateTutorial()
	{
		if (!(wait_tutorial > 0f))
		{
			return;
		}
		wait_tutorial -= Time.deltaTime;
		if (wait_tutorial <= 0f && PlayerInfo.Instance.GetHuoDongTournamentSaveData().Tutorial04 == 0 && WordGlobal.GetHuoDongTournamentTimeLeft() > WordGlobal.HuoDongTournamentFinishtTime && !PlayerInfo.Instance.GetPlayerOtherInfoSaveData().isHuoDongTournamentTutorialShow[3])
		{
			HuoDongTournamentTutorial04.tran_name = base.transform.name;
			if (GameObject.Find("HuoDongTournamentTutorial04") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTournamentTutorial04", isPop: true, UIManager.MainUIType.Type_Common, 99);
			}
			PlayerInfo.Instance.GetHuoDongTournamentSaveData().SetTutorial04();
		}
	}

	public void UpdatePos()
	{
		if (HuoDongTournamentTutorial04.isShow || !(hdtui != null) || !isMyRank)
		{
			return;
		}
		Vector3 position = hdtui.content_rectTran.position;
		if (position.y < outPos_y || tid.rankNum <= 0)
		{
			if (base.transform.parent != hdtui.myItem_rectTran)
			{
				base.transform.SetParent(hdtui.myItem_rectTran);
				my_rect.anchoredPosition3D = new Vector3(0f, 0f, 0f);
				my_rect.localScale = Vector3.one;
			}
		}
		else if (base.transform.parent != hdtui.content_rectTran)
		{
			base.transform.SetParent(hdtui.content_rectTran);
			my_rect.anchoredPosition3D = new Vector3(0f, inRectPosY, 0f);
			my_rect.localScale = Vector3.one;
		}
	}

	public void BtnClick()
	{
		if (hdtui != null && isMyRank)
		{
			if (GameObject.Find("HuoDongTournamentChangeName") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTournamentChangeName", isPop: true, UIManager.MainUIType.Type_Common, 99);
			}
			HuoDongTournamentTutorial04.isHide = true;
		}
	}
}
