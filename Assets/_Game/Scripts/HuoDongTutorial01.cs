using UnityEngine;
using UnityEngine.UI;

public class HuoDongTutorial01 : MonoBehaviour
{
	public static int TextId = 1203;

	public Text des_txt;

	public Text btn_txt;

	private void Start()
	{
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(TextId);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1007);
	}
}
