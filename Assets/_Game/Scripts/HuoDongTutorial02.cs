using UnityEngine;
using UnityEngine.UI;

public class HuoDongTutorial02 : MonoBehaviour
{
	public Text des_txt;

	public Text btn_txt;

	public RectTransform move_rect;

	public static bool isHide;

	private void Start()
	{
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1204);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1235);
		isHide = false;
		if (PlayerInfo.Instance.LoggedFaceBook == 1)
		{
			RectTransform rectTransform = move_rect;
			Vector2 anchoredPosition = move_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, 106f);
		}
		else
		{
			RectTransform rectTransform2 = move_rect;
			Vector2 anchoredPosition2 = move_rect.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, 0f);
		}
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDong).transform.SetParent(base.transform);
		}
	}

	private void Update()
	{
		if (isHide)
		{
			Object.Destroy(base.gameObject);
			isHide = false;
		}
	}

	public void BtnGotoClick()
	{
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.BtnHuoDongClick();
			Object.Destroy(base.gameObject);
		}
	}

	private void OnDestroy()
	{
		Image component = LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDong).GetComponent<Image>();
		Button component2 = LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDong).GetComponent<Button>();
		if (LevelMapUIScript.Instance != null && component != null && component2 != null)
		{
			LevelMapUIScript.Instance.GetHuoDongBtnGo(LevelMapUIScript.HuoDongBtnType.HuoDong).transform.SetParent(LevelMapUIScript.Instance.m_RtfHideBtnsTrans);
			component.enabled = true;
			component2.enabled = true;
		}
	}
}
