using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongUI : MonoBehaviour
{
	public Text Time_text;

	public GameObject PlayBtn_go;

	public Text PlayBtn_text;

	public Text JD_text;

	public Image JD_upImg;

	public GameObject card_bg;

	public Text des_txt;

	public GameObject des01_go;

	public RectTransform JD_lightRectTran;

	public RectTransform CardTran;

	public GameObject cardItem_resGo;

	public List<HuoDongCardItem> card_list = new List<HuoDongCardItem>();

	public Sprite[] card_sps;

	public RectTransform main_rect;

	public bool isCanClickCard = true;

	private float JD_UpWidth = 493f;

	private int huodong_timeleft;

	private HuoDongSaveData hdsd;

	private EventDataCsv edc;

	private string ss1302;

	private string ss1304;

	private bool HideNeedShowBanner;

	public float showRewardUITime;

	private float count_time;

	private float itemAmount_anim_speed = 3f;

	public static bool isHide;

	public bool isCanHide = true;

	private void Start()
	{
		WordGlobal.ResetTopPos(main_rect);
		hdsd = PlayerInfo.Instance.GetHuoDongSaveData();
		edc = EventDataManagerCsv.instance.GetDataById(hdsd.HuoDongID);
		SetJD();
		ss1302 = TextLibraryDataManagerCsv.instance.GetText(1302);
		ss1304 = TextLibraryDataManagerCsv.instance.GetText(1304);
		if (hdsd.HuoDongItemAmount >= HuoDongSaveData.PerRewardItemNum)
		{
			des_txt.text = ss1302;
			PlayBtn_go.SetActive(value: false);
			des01_go.SetActive(value: false);
			des_txt.gameObject.SetActive(value: true);
		}
		else
		{
			des01_go.SetActive(value: true);
			des_txt.gameObject.SetActive(value: false);
			PlayBtn_go.SetActive(value: true);
			PlayBtn_text.text = string.Format(WordGlobal.ReadText(1004), PlayerInfo.Instance.MaxPlayLevel);
		}
		count_time = 1f;
		isHide = false;
		if (PlayerInfo.Instance.GetHuoDongSaveData().Tutorial02 == 0)
		{
			PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHuoDongTutorial02();
		}
		InitCards();
		if (AdsManager.Instance.isShowBanner)
		{
			HideNeedShowBanner = true;
			AdsManager.Instance.HideBannerAD();
		}
	}

	public void InitCards()
	{
		for (int i = 0; i < 9; i++)
		{
			GameObject gameObject = Object.Instantiate(cardItem_resGo);
			gameObject.transform.SetParent(CardTran);
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localScale = Vector3.one;
			gameObject.transform.name = "Card_" + i;
			float x = -150 + i % 3 * 150;
			float y = 332 - i / 3 * 170;
			Vector2 anchoredPosition = new Vector2(x, y);
			(gameObject.transform as RectTransform).anchoredPosition = anchoredPosition;
			gameObject.SetActive(value: true);
			HuoDongCardItem component = gameObject.GetComponent<HuoDongCardItem>();
			component.Init(this, i);
			card_list.Add(component);
		}
	}

	public IEnumerator ShowEff(GameObject go, float show_time)
	{
		go.SetActive(value: true);
		yield return new WaitForSeconds(show_time);
		go.SetActive(value: false);
	}

	public void SetJD(bool isAnim = false)
	{
		JD_text.text = (int)hdsd.HuoDongItemAmount_AnimNum + "/" + HuoDongSaveData.PerRewardItemNum;
		float num = hdsd.HuoDongItemAmount_AnimNum / (float)HuoDongSaveData.PerRewardItemNum;
		if (num >= 1f)
		{
			num = 1f;
		}
		JD_upImg.fillAmount = num;
		float num2 = JD_UpWidth * num;
		RectTransform jD_lightRectTran = JD_lightRectTran;
		float x = num2;
		Vector2 anchoredPosition = JD_lightRectTran.anchoredPosition;
		jD_lightRectTran.anchoredPosition = new Vector2(x, anchoredPosition.y);
	}

	private void Update()
	{
		count_time += Time.deltaTime;
		if (count_time > 1f)
		{
			huodong_timeleft = WordGlobal.GetHuoDongTimeLeft();
			if (huodong_timeleft < 0)
			{
				huodong_timeleft = 0;
				Hide();
			}
			string text = WordGlobal.ChangeTimeToNomral(huodong_timeleft, isNeedDay: false);
			Time_text.text = text;
			count_time = 0f;
		}
		if (hdsd.HuoDongItemAmount_AnimNum < (float)hdsd.HuoDongItemAmount)
		{
			hdsd.HuoDongItemAmount_AnimNum += itemAmount_anim_speed * Time.deltaTime;
			if (hdsd.HuoDongItemAmount_AnimNum >= (float)hdsd.HuoDongItemAmount)
			{
				hdsd.HuoDongItemAmount_AnimNum = hdsd.HuoDongItemAmount;
				PlayerInfo.Instance.SaveHuoDongSaveData();
			}
			SetJD(isAnim: true);
		}
		else if (hdsd.HuoDongItemAmount_AnimNum > (float)hdsd.HuoDongItemAmount)
		{
			hdsd.HuoDongItemAmount_AnimNum -= itemAmount_anim_speed * Time.deltaTime;
			if (hdsd.HuoDongItemAmount_AnimNum <= (float)hdsd.HuoDongItemAmount)
			{
				hdsd.HuoDongItemAmount_AnimNum = hdsd.HuoDongItemAmount;
				PlayerInfo.Instance.SaveHuoDongSaveData();
			}
			SetJD(isAnim: true);
		}
		if (showRewardUITime > 0f)
		{
			showRewardUITime -= Time.deltaTime;
			if (showRewardUITime <= 0f && GameObject.Find("HuoDongRewardUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongRewardUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
			}
		}
		if (hdsd.HuoDongItemAmount >= HuoDongSaveData.PerRewardItemNum)
		{
			des_txt.text = ss1302;
			PlayBtn_go.SetActive(value: false);
			des01_go.SetActive(value: false);
			des_txt.gameObject.SetActive(value: true);
		}
		else
		{
			des01_go.SetActive(value: true);
			des_txt.gameObject.SetActive(value: false);
			PlayBtn_go.SetActive(value: true);
			PlayBtn_text.text = "LEVEL " + PlayerInfo.Instance.MaxPlayLevel;
		}
		if (isHide)
		{
			Hide();
			isHide = false;
		}
	}

	private void Hide()
	{
		if (isCanHide)
		{
			if (LevelMapUIScript.Instance != null && LevelMapUIScript.Instance.GoldItem != null)
			{
				LevelMapUIScript.Instance.GoldItem._isAddGold = false;
			}
			Object.Destroy(base.gameObject);
		}
	}

	public void BtnCloseClick()
	{
		Hide();
	}

	public void BtnHelpClick()
	{
		if (GameObject.Find("HuoDongHelpUI") == null)
		{
			HuoDongHelpUI.des_txtid = 1202;
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongHelpUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
		}
	}

	public void BtnPlayClick()
	{
		if (hdsd.HuoDongItemAmount != edc.requireamount)
		{
			UIManager.CurMainUIType = UIManager.MainUIType.Type_Common;
			if (MainUIScript.Instance != null)
			{
				PlayerInfo.Instance.CurrentLevel = PlayerInfo.Instance.MaxPlayLevel;
				MainUIScript.Instance.Init();
			}
			else if (LevelMapUIScript.Instance != null)
			{
				LevelMapUIScript.Instance.ShowMainUI(PlayerInfo.Instance.MaxPlayLevel);
			}
			Hide();
		}
	}

	private void OnDestroy()
	{
		if (HideNeedShowBanner)
		{
			AdsManager.Instance.ShowBannerAD();
			HideNeedShowBanner = false;
		}
	}
}
