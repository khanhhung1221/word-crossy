using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HuoDongWordFind : MonoBehaviour
{
	public static bool isBackKey;

	public static bool isGameClose;

	public static bool isShow;

	public static int isGamePause;

	public int CurrentUIState;

	public int GameState;

	private HuoDongWordFindSaveData hdwfsd;

	public RectTransform move_rect;

	public RectTransform UI_rect;

	public Text title_txt;

	public Text time_txt;

	public Text itemNum_txt;

	public Text btnPlay_txt;

	public Image jd_img;

	public HuoDongWordFindKeDu[] kedu;

	public GameObject btnPlay_go;

	public GoldScript gold_script;

	public GoldScript goldGame_script;

	public RectTransform top_tran;

	public RectTransform top_uitran;

	private bool isInit;

	public RectTransform Game_rect;

	public RectTransform Content_rect;

	public RectTransform View_rect;

	public GameObject LineItem_resGo;

	public RectTransform selectBg_rect;

	public RectTransform upPos_rect;

	public Text GameAddItemNum_txt;

	public GameObject ready_go;

	public Text ready_txt;

	private Dictionary<char, float> letterWidth;

	public GameObject complete_letterresGo;

	public RectTransform Content_complete;

	public RectTransform top_gametran;

	public GameObject wordGet_effresGo;

	private float showui_x;

	private float showgame_x = -720f;

	private float waitShowTutorial;

	private float jd_width = 530f;

	private float current_itemNum;

	private float ready_waitTime;

	private int ready_index;

	private List<WordFindDataCsv> wordFind_datalist = new List<WordFindDataCsv>();

	private List<HuoDongWordFind_GameLineItem> allLineItems = new List<HuoDongWordFind_GameLineItem>();

	private float content_curY;

	private float content_endY;

	private float content_Speed = 100f;

	private int gameAddNum;

	public string testNum = "ABC";

	public bool isTest;

	private float jd_moveSpeed = 10f;

	private float count_time;

	private float complete_curX;

	private int line_num;

	private float complete_maxXPerline = 650f;

	private List<HuoDongWordFind_GameCompleteLetter> completeLetters = new List<HuoDongWordFind_GameCompleteLetter>();

	private void OnDestroy()
	{
		AdsManager.Instance.HideBannerAD();
	}

	private void Start()
	{
		WordGlobal.LogEvent("Wordfind_open");
		WordGlobal.ResetTopPos(top_tran);
		WordGlobal.ResetTopPos(top_gametran);
		hdwfsd = PlayerInfo.Instance.GetHuoDongWordFindSaveData();
		letterWidth = WordGlobal.GetKeepCalm_Medium2_UpperSizeDic();
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1332);
		btnPlay_txt.text = TextLibraryDataManagerCsv.instance.GetText(1075);
		AdsManager.Instance.ShowBannerAD();
		SetCurrentUIState(0, isMoveAnim: false);
		isShow = true;
		if (PlayerInfo.Instance.TutorialStep == 999 && PlayerInfo.Instance.AdRemove != 1)
		{
			View_rect.offsetMin = new Vector2(0f, 110f);
		}
	}

	public void SetCurrentUIState(int state, bool isMoveAnim = true)
	{
		CurrentUIState = state;
		if (CurrentUIState == 0)
		{
			InitUI();
			if (isMoveAnim)
			{
				move_rect.DOAnchorPosX(showui_x, 0.5f);
			}
			else
			{
				RectTransform rectTransform = move_rect;
				float x = showui_x;
				Vector2 anchoredPosition = move_rect.anchoredPosition;
				rectTransform.anchoredPosition = new Vector2(x, anchoredPosition.y);
			}
			count_time = 1f;
			if (hdwfsd.isFull())
			{
				btnPlay_go.SetActive(value: false);
			}
			else
			{
				btnPlay_go.SetActive(value: true);
			}
			PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHuoDongWordFindTutorial02(0);
		}
		else if (CurrentUIState == 1)
		{
			if (isMoveAnim)
			{
				move_rect.DOAnchorPosX(showgame_x, 0.5f);
			}
			else
			{
				RectTransform rectTransform2 = move_rect;
				float x2 = showgame_x;
				Vector2 anchoredPosition2 = move_rect.anchoredPosition;
				rectTransform2.anchoredPosition = new Vector2(x2, anchoredPosition2.y);
			}
			InitGame();
			if (PlayerInfo.Instance.GetHuoDongWordFindSaveData().Tutorial01 == 0 && PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HuoDongWordFindTutorial02 <= 2)
			{
				waitShowTutorial = 2f;
				PlayerInfo.Instance.GetHuoDongWordFindSaveData().SetTutorial();
			}
		}
	}

	public void InitUI()
	{
		if (isInit)
		{
			return;
		}
		isInit = true;
		if (gold_script == null)
		{
			gold_script = WordGlobal.AddGoldUI(top_tran);
			gold_script._isAddGold = true;
			gold_script.m_ShowBanner = true;
			if (gold_script.banner_go != null)
			{
				gold_script.banner_go.GetComponent<Canvas>().sortingOrder = 13;
			}
		}
		if (goldGame_script == null)
		{
			goldGame_script = WordGlobal.AddGoldUI(top_gametran);
			goldGame_script._isAddGold = false;
			goldGame_script.m_ShowBanner = true;
			if (goldGame_script.banner_go != null)
			{
				goldGame_script.banner_go.GetComponent<Canvas>().sortingOrder = 13;
			}
		}
		InitKeDu();
		SetJinDu(hdwfsd.HuoDongItemAmount);
	}

	public void InitKeDu()
	{
		for (int i = 0; i < hdwfsd.GetRewardItem().Length; i++)
		{
			int num = hdwfsd.GetRewardItem()[i];
			float num2 = (float)num / (float)hdwfsd.GetItemMaxNum();
			string reward = hdwfsd.GetEventData().items[i];
			if (i < kedu.Length)
			{
				RectTransform rectTransform = kedu[i].transform as RectTransform;
				if (num2 > 0.99f)
				{
					num2 = 0.95f;
				}
				RectTransform rectTransform2 = rectTransform;
				float x = jd_width * num2;
				Vector2 anchoredPosition = rectTransform.anchoredPosition;
				rectTransform2.anchoredPosition = new Vector2(x, anchoredPosition.y);
				kedu[i].Init(i, reward, num, this);
			}
		}
	}

	public void SetJinDu(float num)
	{
		current_itemNum = num;
		itemNum_txt.text = (int)current_itemNum + string.Empty;
		float fillAmount = current_itemNum / (float)hdwfsd.GetItemMaxNum();
		jd_img.fillAmount = fillAmount;
		for (int i = 0; i < kedu.Length; i++)
		{
			kedu[i].Check((int)current_itemNum);
		}
	}

	public void SetGameState(int state)
	{
		GameState = state;
		if (GameState == 0)
		{
			ready_go.SetActive(value: false);
			ready_waitTime = 0.5f;
			ready_index = 0;
		}
		else
		{
			ready_go.SetActive(value: false);
			Content_rect.DOKill();
			Content_rect.DOAnchorPosY(content_endY, 40f).SetEase(Ease.Linear).OnComplete(GameComplete);
		}
	}

	public void InitGame()
	{
		wordFind_datalist = WordFindDataManagerCsv.instance.GetDataRandom(25);
		if (allLineItems.Count == 0)
		{
			float num = -50f;
			for (int i = 0; i < wordFind_datalist.Count; i++)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate(LineItem_resGo);
				gameObject.transform.SetParent(Content_rect);
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.transform.localScale = Vector3.one;
				gameObject.transform.name = "Line_" + i;
				float x = 0f;
				float y = num - (float)(i * 100);
				Vector2 anchoredPosition = new Vector2(x, y);
				(gameObject.transform as RectTransform).anchoredPosition = anchoredPosition;
				gameObject.SetActive(value: true);
				HuoDongWordFind_GameLineItem component = gameObject.GetComponent<HuoDongWordFind_GameLineItem>();
				component.Init(this, wordFind_datalist[i]);
				allLineItems.Add(component);
			}
		}
		else
		{
			for (int j = 0; j < allLineItems.Count; j++)
			{
				allLineItems[j].Init(this, wordFind_datalist[j]);
			}
		}
		content_endY = (float)(wordFind_datalist.Count * 100) + View_rect.rect.height - 50f;
		content_curY = 0f;
		Content_rect.DOKill();
		Content_rect.anchoredPosition = Vector2.zero;
		gameAddNum = 0;
		GameAddItemNum_txt.text = gameAddNum + string.Empty;
		for (int k = 0; k < completeLetters.Count; k++)
		{
			completeLetters[k].gameObject.SetActive(value: false);
		}
		RectTransform content_complete = Content_complete;
		Vector2 anchoredPosition2 = Content_complete.anchoredPosition;
		content_complete.anchoredPosition = new Vector2(anchoredPosition2.x, 0f);
		complete_curX = 50f;
		line_num = 1;
		isGamePause = 0;
		SetGameState(0);
	}

	public void GameComplete()
	{
		if (PlayerInfo.Instance.AdRemove == 1)
		{
			RealResult();
		}
		else if (AdsManager.Instance.ShowInterstitialAd("Pop_ForceAd_WordfindComplete"))
		{
			AdsManager.ShowInterstitialAdEvent = (UnityAction)Delegate.Combine(AdsManager.ShowInterstitialAdEvent, new UnityAction(RewardADEvent));
		}
		else
		{
			RealResult();
		}
	}

	private void RewardADEvent()
	{
		AdsManager.ShowInterstitialAdEvent = (UnityAction)Delegate.Remove(AdsManager.ShowInterstitialAdEvent, new UnityAction(RewardADEvent));
		RealResult();
	}

	public void RealResult()
	{
		Debug.Log("RealResult gameAddNum:" + gameAddNum);
		hdwfsd.AddItemAmount(gameAddNum);
		jd_moveSpeed = gameAddNum;
		if (jd_moveSpeed < 10f)
		{
			jd_moveSpeed = 10f;
		}
		SetCurrentUIState(0);
		HuoDongWordSearchClost.isClose = true;
	}

	public void AddGameGetNum(int num)
	{
		gameAddNum += num;
		GameAddItemNum_txt.text = gameAddNum + string.Empty;
	}

	public void AddGoldEff(int goldNum, Vector3 flyPos)
	{
		if (gold_script != null)
		{
			gold_script._isAddGold = true;
			gold_script.AddGold(goldNum, 1f, 1f, isAdd2PlayerInfo: false, 11);
			gold_script.CollectGoldNew(flyPos, 1, 1f, null);
		}
	}

	public void ShowWordEff(HuoDongWordFind_GameLetterItem letter_item)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate(wordGet_effresGo);
		gameObject.transform.SetParent(Content_rect);
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		gameObject.SetActive(value: true);
		HuoDongWordFindWordGetEff component = gameObject.GetComponent<HuoDongWordFindWordGetEff>();
		component.transform.name = letter_item.line.transform.name + "_" + letter_item.transform.name;
		(component.transform as RectTransform).anchoredPosition = (letter_item.line.transform as RectTransform).anchoredPosition + (letter_item.transform as RectTransform).anchoredPosition;
		component.ShowEff(letter_item.letter_pos);
	}

	private void Update()
	{
		if (isTest)
		{
			isTest = false;
		}
		UpdateUIState();
	}

	public void UpdateUIState()
	{
		if (CurrentUIState == 0)
		{
			UpdateUI();
		}
		else if (CurrentUIState == 1)
		{
			UpdateGame();
		}
	}

	public void UpdateUI()
	{
		if (isBackKey)
		{
			BtnUIClose();
			isBackKey = false;
		}
		if ((float)hdwfsd.HuoDongItemAmount > current_itemNum)
		{
			current_itemNum += Time.deltaTime * jd_moveSpeed;
			if (current_itemNum >= (float)hdwfsd.HuoDongItemAmount)
			{
				current_itemNum = hdwfsd.HuoDongItemAmount;
			}
			SetJinDu(current_itemNum);
		}
		count_time += Time.deltaTime;
		if (count_time > 1f)
		{
			long num = WordGlobal.GetHuoDongWordFindTimeLeft();
			if (num < 0)
			{
				num = 0L;
				BtnUIClose();
			}
			string text = WordGlobal.ChangeTimeToNomral(num, isNeedDay: false);
			time_txt.text = text;
			count_time = 0f;
		}
	}

	public void UpdateGame()
	{
		if (isBackKey)
		{
			BtnGameClose();
			isBackKey = false;
		}
		if (isGameClose)
		{
			if (PlayerInfo.Instance.AdRemove != 1)
			{
				AdsManager.Instance.ShowInterstitialAd("Pop_ForceAd_Wordfind");
			}
			SetCurrentUIState(0);
			isGameClose = false;
		}
		if (GameState == 0)
		{
			if (!(ready_waitTime > 0f))
			{
				return;
			}
			ready_waitTime -= Time.deltaTime;
			if (ready_waitTime <= 0f)
			{
				ready_index++;
				if (ready_index == 1)
				{
					ready_go.SetActive(value: true);
					ready_waitTime = 1f;
					ready_txt.text = "READY";
				}
				else if (ready_index == 2)
				{
					ready_waitTime = 1f;
					ready_txt.text = "GO";
				}
				else if (ready_index == 3)
				{
					SetGameState(1);
				}
			}
		}
		else
		{
			if (GameState != 1)
			{
				return;
			}
			if (waitShowTutorial > 0f)
			{
				waitShowTutorial -= Time.deltaTime;
				if (waitShowTutorial <= 0f && GameObject.Find("HuoDongWordFindTutorial") == null)
				{
					isGamePause = 1;
					Content_rect.DOPause();
					UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordFindTutorial", isPop: true, UIManager.MainUIType.Type_Common, 99);
				}
			}
			if (isGamePause == 2)
			{
				Content_rect.DOPlay();
				isGamePause = 0;
			}
		}
	}

	public List<HuoDongWordFind_GameCompleteLetter> AddCompleteWord(string word)
	{
		if (complete_curX > 50f)
		{
			complete_curX += 30f;
		}
		char[] array = word.ToCharArray();
		float num = 0f;
		for (int i = 0; i < array.Length; i++)
		{
			num += letterWidth[array[i]];
		}
		if (complete_curX + num > complete_maxXPerline)
		{
			AddLine();
		}
		List<HuoDongWordFind_GameCompleteLetter> list = new List<HuoDongWordFind_GameCompleteLetter>();
		for (int j = 0; j < array.Length; j++)
		{
			HuoDongWordFind_GameCompleteLetter unuseCompleteLetter = GetUnuseCompleteLetter();
			unuseCompleteLetter.Init(array[j].ToString());
			float x = complete_curX;
			float y = -(line_num - 1) * 75;
			Vector2 anchoredPosition = new Vector2(x, y);
			(unuseCompleteLetter.transform as RectTransform).anchoredPosition = anchoredPosition;
			float num2 = letterWidth[array[j]];
			complete_curX += num2;
			list.Add(unuseCompleteLetter);
		}
		if (line_num >= 3)
		{
			float y2 = (line_num - 2) * 75;
			RectTransform content_complete = Content_complete;
			Vector2 anchoredPosition2 = Content_complete.anchoredPosition;
			content_complete.anchoredPosition = new Vector2(anchoredPosition2.x, y2);
		}
		return list;
	}

	public void AddLine()
	{
		line_num++;
		complete_curX = 50f;
	}

	public HuoDongWordFind_GameCompleteLetter GetUnuseCompleteLetter()
	{
		for (int i = 0; i < completeLetters.Count; i++)
		{
			if (!completeLetters[i].gameObject.activeInHierarchy)
			{
				return completeLetters[i];
			}
		}
		GameObject gameObject = UnityEngine.Object.Instantiate(complete_letterresGo);
		gameObject.transform.SetParent(Content_complete);
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		float x = 0f;
		float y = 0f;
		Vector2 anchoredPosition = new Vector2(x, y);
		(gameObject.transform as RectTransform).anchoredPosition = anchoredPosition;
		gameObject.SetActive(value: true);
		HuoDongWordFind_GameCompleteLetter component = gameObject.GetComponent<HuoDongWordFind_GameCompleteLetter>();
		completeLetters.Add(component);
		return component;
	}

	public void BtnUIClose()
	{
		isShow = false;
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void BtnUIPlay()
	{
		WordGlobal.LogEvent("Wordfind_play");
		SetCurrentUIState(1);
	}

	public void BtnGameClose()
	{
		if (GameObject.Find("HuoDongWordSearchClose") == null)
		{
			HuoDongWordSearchClost.ShowType = 1;
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchClose", isPop: true);
		}
	}

	public void BtnGameSet()
	{
		if (GameObject.Find("SettingUI") == null)
		{
			SettingUIScript.BackType = 3;
			UIManager.Instance.LoadMainUI("UI/SettingUI", isPop: true);
		}
	}

	public void BtnHelpClick()
	{
		if (GameObject.Find("HuoDongHelpUI") == null)
		{
			HuoDongHelpUI.des_txtid = 1336;
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongHelpUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
		}
	}
}
