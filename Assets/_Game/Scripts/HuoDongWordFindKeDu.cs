using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordFindKeDu : MonoBehaviour
{
	public Image rewardImg;

	public Text rewardText;

	public Text needText;

	public GameObject got_go;

	public Image duihao_img;

	private int idx;

	private int itemNeedNum;

	private int reward_type;

	private int reward_num;

	private HuoDongWordFind hdsui;

	private bool isShowReward;

	private void Start()
	{
	}

	public void Init(int index, string reward, int itemNeedNum, HuoDongWordFind hdsui)
	{
		this.hdsui = hdsui;
		idx = index;
		this.itemNeedNum = itemNeedNum;
		string[] array = reward.Split('_');
		reward_type = int.Parse(array[0]);
		if (reward_type == 1)
		{
			rewardImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/" + array[1]);
			rewardImg.rectTransform.sizeDelta = new Vector2(rewardImg.sprite.textureRect.width, rewardImg.sprite.textureRect.height);
			reward_num = int.Parse(array[2]);
			rewardText.text = string.Empty + reward_num;
		}
		else if (reward_type == 3)
		{
			rewardImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/jinbi_dr");
			rewardImg.rectTransform.sizeDelta = new Vector2(76f, 77f);
			reward_num = int.Parse(array[1]);
			rewardText.text = string.Empty + reward_num;
		}
		needText.text = itemNeedNum + string.Empty;
		if (PlayerInfo.Instance.GetHuoDongSunSaveData().HuoDongItemAmount >= itemNeedNum)
		{
			got_go.SetActive(value: true);
			duihao_img.fillAmount = 1f;
		}
	}

	public void Check(int curItemAmount)
	{
		if (!(hdsui == null) && !isShowReward && PlayerInfo.Instance.GetHuoDongWordFindSaveData().GetNeedGetRewardIndex().Contains(idx) && curItemAmount >= itemNeedNum)
		{
			hdsui.AddGoldEff(reward_num, base.transform.position);
			GotAnim();
			PlayerInfo.Instance.GetHuoDongWordFindSaveData().GetNeedGetRewardIndex().Remove(idx);
			isShowReward = true;
		}
	}

	public void GotAnim()
	{
		got_go.SetActive(value: true);
		duihao_img.fillAmount = 0f;
		duihao_img.DOFillAmount(1f, 0.5f);
	}
}
