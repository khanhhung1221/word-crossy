using System;
using System.Collections.Generic;

[Serializable]
public class HuoDongWordFindSaveData
{
	public int HuoDongID;

	public int HuoDongItemAmount;

	public int[] lastLevelIds = new int[25];

	public int GetRewardCount;

	public int Tutorial01;

	public int Tutorial02;

	public int TutorialOpen;

	private EventDataCsv edc;

	private int[] reward_nums;

	private List<int> NeedGetRewardIndex = new List<int>();

	public void InitHuoDongID()
	{
		HuoDongID = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 5);
		GetEventData();
		lastLevelIds = new int[25];
		HuoDongItemAmount = 0;
		GetRewardCount = 0;
		Init();
		PlayerInfo.Instance.SaveHuoDongWordFindSaveData();
	}

	public void SetLaseLevelIds(int[] arr)
	{
		lastLevelIds = arr;
		PlayerInfo.Instance.SaveHuoDongWordFindSaveData();
	}

	public void SetTutorial()
	{
		Tutorial01 = 1;
		PlayerInfo.Instance.SaveHuoDongWordFindSaveData();
	}

	public void SetTutorial02()
	{
		Tutorial02 = 1;
		PlayerInfo.Instance.SaveHuoDongWordFindSaveData();
	}

	public void SetTutorialOpen()
	{
		TutorialOpen = 1;
		PlayerInfo.Instance.SaveHuoDongWordFindSaveData();
	}

	public void Init()
	{
		if (HuoDongID > 0 && GetEventData() != null)
		{
			string[] array = GetEventData().other.Split('_');
			reward_nums = new int[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				reward_nums[i] = int.Parse(array[i]);
			}
		}
	}

	public int[] GetRewardItem()
	{
		return reward_nums;
	}

	public int GetItemMaxNum()
	{
		if (reward_nums.Length > 0)
		{
			return reward_nums[reward_nums.Length - 1];
		}
		return 0;
	}

	public void AddItemAmount(int num)
	{
		HuoDongItemAmount += num;
		if (HuoDongItemAmount >= GetItemMaxNum())
		{
			HuoDongItemAmount = GetItemMaxNum();
		}
		CheckReward();
		PlayerInfo.Instance.SaveHuoDongWordFindSaveData();
	}

	public bool isFull()
	{
		if (HuoDongItemAmount >= GetItemMaxNum())
		{
			return true;
		}
		return false;
	}

	public void CheckReward()
	{
		if (GetRewardCount < reward_nums.Length)
		{
			int num = reward_nums[GetRewardCount];
			if (HuoDongItemAmount >= num)
			{
				string itemStr = GetEventData().items[GetRewardCount];
				PlayerInfo.Instance.AddItemByStr(itemStr, 9);
				NeedGetRewardIndex.Add(GetRewardCount);
				GetRewardCount++;
				WordFaceBook.Instance.UpdateTrackTaskEvent("WordFind_RewardCount", GetRewardCount + string.Empty);
			}
		}
	}

	public List<int> GetNeedGetRewardIndex()
	{
		return NeedGetRewardIndex;
	}

	public EventDataCsv GetEventData()
	{
		if (edc == null)
		{
			edc = EventDataManagerCsv.instance.GetDataByType(5);
		}
		return edc;
	}
}
