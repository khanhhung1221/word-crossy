using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordFindTutorial : MonoBehaviour
{
	public Text title_txt;

	public Text des_txt;

	public Text btn_txt;

	private void Start()
	{
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1027);
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1330);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1007);
	}

	private void OnDestroy()
	{
		HuoDongWordFind.isGamePause = 2;
	}
}
