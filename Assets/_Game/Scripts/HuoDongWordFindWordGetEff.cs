using DG.Tweening;
using UnityEngine;

public class HuoDongWordFindWordGetEff : MonoBehaviour
{
	public GameObject get_effGo;

	public RectTransform get_EffUpGo;

	private void Start()
	{
	}

	public void ShowEff(HuoDongWordFind_GameCompleteLetter up_endPos)
	{
		base.gameObject.SetActive(value: true);
		get_effGo.SetActive(value: false);
		get_effGo.SetActive(value: true);
		get_EffUpGo.gameObject.SetActive(value: true);
		get_EffUpGo.transform.SetParent(base.transform.parent.parent);
		get_EffUpGo.transform.position = base.gameObject.transform.position;
		get_EffUpGo.DOMove(up_endPos.transform.position, 1f).OnComplete(delegate
		{
			up_endPos.ShowText();
			get_EffUpGo.gameObject.SetActive(value: false);
			Object.Destroy(get_EffUpGo.gameObject);
			Object.Destroy(base.gameObject);
		});
	}
}
