using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordFind_GameCompleteLetter : MonoBehaviour
{
	public Text text;

	private float delayTime;

	private void Start()
	{
	}

	public void Init(string letter)
	{
		base.transform.name = letter;
		base.gameObject.SetActive(value: true);
		text.text = letter;
		text.enabled = false;
	}

	public void ShowText()
	{
		text.enabled = true;
	}
}
