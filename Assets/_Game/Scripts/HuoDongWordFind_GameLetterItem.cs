using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HuoDongWordFind_GameLetterItem : MonoBehaviour, IPointerUpHandler, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler, IDragHandler, IEventSystemHandler
{
	public int state = -1;

	public int inWordIdx;

	public Text normal_txt;

	public Text select_txt;

	public Text wrong_txt;

	public HuoDongWordFind_GameLineItem line;

	public HuoDongWordFind_GameCompleteLetter letter_pos;

	public Animator anim;

	public float hideDelay_time;

	private float backToNormalTime;

	private void Start()
	{
	}

	public void Init(HuoDongWordFind_GameLineItem line, char letter, int inWordIdx)
	{
		this.line = line;
		this.inWordIdx = inWordIdx;
		normal_txt.text = letter.ToString();
		select_txt.text = letter.ToString();
		wrong_txt.text = letter.ToString();
		SetState(0);
		base.gameObject.SetActive(value: true);
	}

	public void SetState(int s, bool isLogic = true)
	{
		if (isLogic && state != s)
		{
			switch (s)
			{
			case 0:
				normal_txt.gameObject.SetActive(value: true);
				select_txt.gameObject.SetActive(value: false);
				wrong_txt.gameObject.SetActive(value: false);
				backToNormalTime = 0f;
				break;
			case 1:
				select_txt.gameObject.SetActive(value: true);
				normal_txt.gameObject.SetActive(value: false);
				wrong_txt.gameObject.SetActive(value: false);
				anim.Play("word_touch", 0);
				break;
			case 2:
				wrong_txt.gameObject.SetActive(value: true);
				normal_txt.gameObject.SetActive(value: false);
				select_txt.gameObject.SetActive(value: false);
				anim.Play("word_fail", 0);
				backToNormalTime = 0.5f;
				break;
			case 3:
				select_txt.gameObject.SetActive(value: true);
				normal_txt.gameObject.SetActive(value: false);
				wrong_txt.gameObject.SetActive(value: false);
				break;
			}
		}
		state = s;
	}

	private void Update()
	{
		if (hideDelay_time > 0f)
		{
			hideDelay_time -= Time.deltaTime;
			if (hideDelay_time <= 0f)
			{
				line.word_find.ShowWordEff(this);
				base.gameObject.SetActive(value: false);
			}
		}
		if (backToNormalTime > 0f)
		{
			backToNormalTime -= Time.deltaTime;
			if (backToNormalTime <= 0f)
			{
				SetState(0);
			}
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (state != 3)
		{
			line.SetDownLetter(this);
		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (state != 3)
		{
			line.SetEnterLetter(this);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (state != 3)
		{
			line.SetUpLetter(this);
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (state != 3)
		{
			Vector2 position = eventData.position;
			float x = position.x;
			Vector2 pressPosition = eventData.pressPosition;
			float move_w = (x - pressPosition.x) * 720f / (float)Screen.width;
			line.SetMoveLetter(this, move_w);
		}
	}
}
