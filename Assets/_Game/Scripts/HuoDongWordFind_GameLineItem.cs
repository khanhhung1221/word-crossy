using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordFind_GameLineItem : MonoBehaviour
{
	public GameObject letterItem_resGo;

	public Text answer_txt;

	private HuoDongWordFind_GameLetterItem startItem;

	public HuoDongWordFind word_find;

	private WordFindDataCsv data;

	private List<HuoDongWordFind_GameLetterItem> letterItems = new List<HuoDongWordFind_GameLetterItem>();

	private List<string> all_meanWords = new List<string>();

	private bool isUP;

	private float base_w = 90f;

	private void Start()
	{
	}

	public void Init(HuoDongWordFind word_find, WordFindDataCsv data)
	{
		this.word_find = word_find;
		this.data = data;
		answer_txt.text = data.mean_word;
		answer_txt.gameObject.SetActive(value: true);
		answer_txt.gameObject.SetActive(value: false);
		char[] array = data.word.ToCharArray();
		all_meanWords.Clear();
		string[] array2 = data.mean_word.Split(';');
		for (int i = 0; i < array2.Length; i++)
		{
			all_meanWords.Add(array2[i]);
		}
		if (letterItems.Count == 0)
		{
			for (int j = 0; j < array.Length; j++)
			{
				GameObject gameObject = Object.Instantiate(letterItem_resGo);
				gameObject.transform.SetParent(base.transform);
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.transform.localScale = Vector3.one;
				gameObject.transform.name = "Letter_" + array[j];
				float x = -225 + j * 90;
				float y = 0f;
				Vector2 anchoredPosition = new Vector2(x, y);
				(gameObject.transform as RectTransform).anchoredPosition = anchoredPosition;
				gameObject.SetActive(value: true);
				HuoDongWordFind_GameLetterItem component = gameObject.GetComponent<HuoDongWordFind_GameLetterItem>();
				component.Init(this, array[j], j);
				letterItems.Add(component);
			}
		}
		else
		{
			for (int k = 0; k < letterItems.Count; k++)
			{
				letterItems[k].Init(this, array[k], k);
			}
		}
		answer_txt.transform.SetAsLastSibling();
		base.gameObject.SetActive(value: true);
	}

	public void SetDownLetter(HuoDongWordFind_GameLetterItem hdwsli)
	{
		if (startItem == null)
		{
			startItem = hdwsli;
		}
		isUP = false;
		word_find.selectBg_rect.SetParent(base.transform);
		word_find.selectBg_rect.SetAsFirstSibling();
		word_find.selectBg_rect.gameObject.SetActive(value: true);
		ShowSelectBG(0f);
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordSearchList[4]);
	}

	public void SetEnterLetter(HuoDongWordFind_GameLetterItem hdwsli)
	{
		if (startItem == null)
		{
			startItem = hdwsli;
		}
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordSearchList[0]);
	}

	public void SetMoveLetter(HuoDongWordFind_GameLetterItem hdwsli, float move_w)
	{
		if (startItem == null)
		{
			startItem = hdwsli;
		}
		ShowSelectBG(move_w);
	}

	public void SetUpLetter(HuoDongWordFind_GameLetterItem hdwsli)
	{
		isUP = true;
		CheckEnd();
	}

	public void CheckEnd(bool isCheck = true)
	{
		if (data == null)
		{
			return;
		}
		if (isCheck)
		{
			Vector2 sizeDelta = word_find.selectBg_rect.sizeDelta;
			float x = sizeDelta.x;
			int num = (int)(x / base_w + 0.5f);
			if (startItem.inWordIdx + num > data.word.Length)
			{
				num = data.word.Length - startItem.inWordIdx;
			}
			Debug.Log("startItem.inWordIdx:" + startItem.inWordIdx + " total_letter_num:" + num);
			string text = data.word.Substring(startItem.inWordIdx, num);
			if (text.Length >= 3)
			{
				bool flag = false;
				if (all_meanWords.Contains(text))
				{
					flag = true;
				}
				List<HuoDongWordFind_GameCompleteLetter> list = new List<HuoDongWordFind_GameCompleteLetter>();
				if (flag)
				{
					int num2 = text.Length - 2;
					word_find.AddGameGetNum(num2);
					list = word_find.AddCompleteWord(text);
				}
				Debug.Log("select_word:" + text + " isMean:" + flag);
				int num3 = 0;
				for (int i = startItem.inWordIdx; i < startItem.inWordIdx + num; i++)
				{
					if (flag)
					{
						letterItems[i].letter_pos = list[num3];
						letterItems[i].hideDelay_time = (float)(num3 + 1) * 0.1f;
						letterItems[i].SetState(3);
						num3++;
					}
					else
					{
						letterItems[i].SetState(2);
					}
				}
			}
			else
			{
				for (int j = startItem.inWordIdx; j < startItem.inWordIdx + num; j++)
				{
					letterItems[j].SetState(0);
				}
			}
		}
		word_find.selectBg_rect.gameObject.SetActive(value: false);
		startItem = null;
	}

	public void ShowSelectBG(float width)
	{
		int num = 0;
		for (int i = startItem.inWordIdx + 1; i < letterItems.Count && letterItems[i].state != 3; i++)
		{
			num++;
		}
		float num2 = (float)num * base_w;
		if (width < 0f)
		{
			width = 0f;
		}
		else if (width > num2)
		{
			width = num2;
		}
		float num3 = base_w + width;
		float x = 45f + (float)startItem.inWordIdx * base_w;
		word_find.selectBg_rect.anchoredPosition = new Vector2(x, 0f);
		RectTransform selectBg_rect = word_find.selectBg_rect;
		float x2 = num3;
		Vector2 sizeDelta = word_find.selectBg_rect.sizeDelta;
		selectBg_rect.sizeDelta = new Vector2(x2, sizeDelta.y);
		int num4 = (int)(num3 / base_w + 0.5f);
		if (startItem.inWordIdx + num4 > data.word.Length)
		{
			num4 = data.word.Length - startItem.inWordIdx;
		}
		string text = data.word.Substring(startItem.inWordIdx, num4);
		for (int j = 0; j < data.word.Length; j++)
		{
			if (letterItems[j].state != 3)
			{
				if (j >= startItem.inWordIdx && j < startItem.inWordIdx + num4)
				{
					letterItems[j].SetState(1);
				}
				else
				{
					letterItems[j].SetState(0);
				}
			}
		}
	}

	public void Update()
	{
		if (startItem != null)
		{
			Vector3 position = base.transform.position;
			float y = position.y;
			Vector3 position2 = word_find.upPos_rect.position;
			if (y >= position2.y)
			{
				CheckEnd(isCheck: false);
			}
		}
	}
}
