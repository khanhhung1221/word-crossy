using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HuoDongWordSearch : MonoBehaviour
{
	public static bool isCloseClick;

	public static bool isExit;

	public static int AddTime;

	public static bool isTryAgain;

	public static int AddGoldNum;

	public static bool isShowAddItemEff;

	public GameObject itemRes_go;

	public GameObject lineRes_go;

	public GameObject sucTextRes_go;

	public RectTransform itemRoot_rectTran;

	public RectTransform lineRoot_rectTran;

	public RectTransform sucTextRoot_rectTran;

	public RectTransform guideLine_rectTran;

	public RectTransform words_posRect;

	public RectTransform root_posRect;

	public HuoDongWordSearchWord[] words;

	public GoldScript gold_script;

	public RectTransform gold_flyTran;

	public RectTransform top_tran;

	public RectTransform GuidePos_tran;

	public RectTransform move_tran;

	public RectTransform hide_tran;

	public Transform GuideBtn_tran;

	public GameObject video_go;

	public Text Time_txt;

	public int curTime;

	public Text GuideTxt;

	public Image GuideIcon_img;

	private HuoDongWordSearchLine cur_line;

	private HuoDongWordSearchLine guide_line;

	public HuoDongWordSearchSelectWord selectWord;

	private List<HuoDongWordSearchSucText> sucTextList = new List<HuoDongWordSearchSucText>();

	public Color[] lineColors;

	public Color guideLineColor;

	private int curColorIndex;

	public int Direction;

	private Dictionary<int, HuoDongWordSearchLetterItem> item_dic = new Dictionary<int, HuoDongWordSearchLetterItem>();

	private List<HuoDongWordSearchLetterItem> curSelectList = new List<HuoDongWordSearchLetterItem>();

	private int countSetLetterItems;

	private Dictionary<string, HuoDongWordSearchAddSuc> allWordLetterItem_dic = new Dictionary<string, HuoDongWordSearchAddSuc>();

	private List<string> allWords = new List<string>();

	private List<string> completeWords = new List<string>();

	private List<HuoDongWordSearchLine> lines = new List<HuoDongWordSearchLine>();

	private HuoDongWordSearchLetterItem wordSearchLetterItem_start;

	private HuoDongWordSearchLetterItem wordSearchLetterItem_end;

	private HuoDongWordSearchLetterItem wordSearchLetterItem_SelectEnd;

	private string curSelectWord = string.Empty;

	private int maxRow = 10;

	private int maxCol = 10;

	private int maxTime = 120;

	private int word7Num = 3;

	private int word6Num = 2;

	private int word5Num = 2;

	private int word4Num = 2;

	private int temp_word7Num;

	private int temp_word6Num;

	private int temp_word5Num;

	private int temp_word4Num;

	private float angle22d5 = 22.5f;

	private int addCount;

	private char[] allLetter = new char[26]
	{
		'A',
		'B',
		'C',
		'D',
		'E',
		'F',
		'G',
		'H',
		'I',
		'J',
		'K',
		'L',
		'M',
		'N',
		'O',
		'P',
		'Q',
		'R',
		'S',
		'T',
		'U',
		'V',
		'W',
		'X',
		'Y',
		'Z'
	};

	private int[] RowCol7OutIdx = new int[4]
	{
		44,
		45,
		54,
		55
	};

	private int[] RowCol8OutIdx = new int[12]
	{
		33,
		34,
		35,
		36,
		43,
		46,
		53,
		56,
		63,
		64,
		65,
		66
	};

	private int[] RowCol9OutIdx = new int[20]
	{
		22,
		23,
		24,
		25,
		26,
		27,
		32,
		37,
		42,
		47,
		52,
		57,
		62,
		67,
		72,
		73,
		74,
		75,
		76,
		77
	};

	private List<int> out7Indexs = new List<int>();

	private List<int> out8Indexs = new List<int>();

	private List<int> out9Indexs = new List<int>();

	private bool isUP = true;

	private bool isShowResult;

	private float showReward_time;

	public bool isTest;

	private float upTimeOut = 1f;

	private float waitConsoleTime;

	private bool CheckTutorial;

	private float dpc_tutorial_waitTime;

	private int temp_guideitemAmount = -1;

	private float waitTime;

	private float sound_time;

	private bool isTimeOut;

	private Vector2Int startItem;

	private Vector2Int endItem;

	private float angleOfLine;

	private float lastAngle = 999f;

	private string lastSelectWord = string.Empty;

	private int selectLetterCount = -1;

	private bool isExiting;

	private void Start()
	{
		WordGlobal.ResetTopPos(top_tran);
		if (PlayerInfo.Instance.AdRemove == 1)
		{
			RectTransform rectTransform = root_posRect;
			Vector2 anchoredPosition = root_posRect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, -12f);
			RectTransform rectTransform2 = words_posRect;
			Vector2 anchoredPosition2 = words_posRect.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, 0f);
		}
		else
		{
			RectTransform rectTransform3 = root_posRect;
			Vector2 anchoredPosition3 = root_posRect.anchoredPosition;
			rectTransform3.anchoredPosition = new Vector2(anchoredPosition3.x, 8f);
			RectTransform rectTransform4 = words_posRect;
			Vector2 anchoredPosition4 = words_posRect.anchoredPosition;
			rectTransform4.anchoredPosition = new Vector2(anchoredPosition4.x, 60f);
		}
		AudioControl.GetInstance().PlayBG(AudioControl.GetInstance().SF_WordSearchBg);
		isCloseClick = false;
		AddTime = 0;
		isTryAgain = false;
		isExit = false;
		isExiting = false;
		AddGoldNum = 0;
		Init();
		if (gold_script == null)
		{
			gold_script = WordGlobal.AddGoldUI(top_tran);
			gold_script.m_ShowBanner = true;
		}
	}

	public void Init()
	{
		AdsManager.Instance.ShowBannerAD();
		if (PlayerInfo.Instance.GetWordSearchSaveData().GetCurPlayLevel() >= 20)
		{
			maxTime = 100;
		}
		else
		{
			maxTime = 120;
		}
		InitItems();
		InitLevel();
		InitWords();
		InitTime();
		RectTransform rectTransform = move_tran;
		Vector2 anchoredPosition = move_tran.anchoredPosition;
		rectTransform.anchoredPosition = new Vector2(1000f, anchoredPosition.y);
		move_tran.DOAnchorPosX(0f, 0.5f).OnComplete(delegate
		{
			if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 2)
			{
				CheckTutorial = true;
			}
		});
		hide_tran.gameObject.SetActive(value: true);
		isShowResult = false;
	}

	public void InitTime()
	{
		curTime = maxTime;
		SetTimeText();
	}

	public void InitWords()
	{
		int[] array = new int[allWords.Count];
		for (int i = 0; i < allWords.Count; i++)
		{
			array[i] = i;
		}
		RandomArray<int> randomArray = new RandomArray<int>(array);
		array = randomArray.GetDisruptedItems();
		for (int j = 0; j < words.Length; j++)
		{
			words[j].Init(allWords[array[j]], this);
		}
	}

	public void InitItems()
	{
		if (item_dic.Count > 0)
		{
			return;
		}
		for (int i = 0; i < maxRow; i++)
		{
			for (int j = 0; j < maxCol; j++)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate(itemRes_go);
				gameObject.transform.SetParent(itemRoot_rectTran);
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.transform.localScale = Vector3.one;
				gameObject.transform.name = "Item_" + i + "_" + j;
				HuoDongWordSearchLetterItem component = gameObject.GetComponent<HuoDongWordSearchLetterItem>();
				component.Init(this, i, j);
				item_dic.Add(component.RowCol, component);
			}
		}
	}

	public void InitLevel()
	{
		temp_word7Num = word7Num;
		temp_word6Num = word6Num;
		temp_word5Num = word5Num;
		temp_word4Num = word4Num;
		allWordLetterItem_dic.Clear();
		allWords.Clear();
		completeWords.Clear();
		addCount = 0;
		AddWord();
	}

	public void SetTimeText()
	{
		if (curTime <= 15)
		{
			Time_txt.color = Color.red;
		}
		else
		{
			Time_txt.color = Color.yellow;
		}
		Time_txt.text = WordGlobal.ChangeTimeToNormalSpecial(curTime, 0, isNeedDay: false);
	}

	public void AddWord()
	{
		addCount++;
		if (addCount > 100)
		{
			Debug.LogError("AddWord out " + addCount);
			addCount = 0;
			return;
		}
		if (temp_word7Num > 0)
		{
			string wordByType = WordSearchDataManagerCsv.instance.GetWordByType(7);
			if (AddWordInLevel(wordByType))
			{
				temp_word7Num--;
			}
			AddWord();
			return;
		}
		if (temp_word6Num > 0)
		{
			string wordByType2 = WordSearchDataManagerCsv.instance.GetWordByType(6);
			if (AddWordInLevel(wordByType2))
			{
				temp_word6Num--;
			}
			AddWord();
			return;
		}
		if (temp_word5Num > 0)
		{
			string wordByType3 = WordSearchDataManagerCsv.instance.GetWordByType(5);
			if (AddWordInLevel(wordByType3))
			{
				temp_word5Num--;
			}
			AddWord();
			return;
		}
		if (temp_word4Num > 0)
		{
			string wordByType4 = WordSearchDataManagerCsv.instance.GetWordByType(4);
			if (AddWordInLevel(wordByType4))
			{
				temp_word4Num--;
			}
			AddWord();
			return;
		}
		int num = 9;
		if (num < 9)
		{
			string wordByType5 = WordSearchDataManagerCsv.instance.GetWordByType(4);
			bool flag = AddWordInLevel(wordByType5);
			AddWord();
			return;
		}
		string text = string.Empty;
		foreach (KeyValuePair<string, HuoDongWordSearchAddSuc> item in allWordLetterItem_dic)
		{
			text += item.Key;
		}
		char[] array = allLetter;
		if (PlayerInfo.Instance.GetWordSearchSaveData().GetCurPlayLevel() > 10)
		{
			char[] array2 = text.ToCharArray();
			array = new char[allLetter.Length + array2.Length];
			int num2 = 0;
			for (int i = 0; i < allLetter.Length; i++)
			{
				array[num2] = allLetter[i];
				num2++;
			}
			for (int j = 0; j < array2.Length; j++)
			{
				array[num2] = array2[j];
				num2++;
			}
		}
		else
		{
			array = allLetter;
		}
		RandomArray<char> randomArray = new RandomArray<char>(array);
		array = randomArray.GetDisruptedItems();
		int num3 = -1;
		foreach (KeyValuePair<int, HuoDongWordSearchLetterItem> item2 in item_dic)
		{
			if (string.IsNullOrEmpty(item2.Value.letter))
			{
				num3++;
				if (num3 >= array.Length)
				{
					num3 = 0;
				}
				item2.Value.SetLetter(array[num3].ToString());
			}
		}
	}

	public bool AddWordInLevel(string word)
	{
		countSetLetterItems = 0;
		return SetLetterItems(word);
	}

	public void ShowAddItemEff()
	{
		if (GameObject.Find("HuoDongWordSearchPenGet") == null)
		{
			HuoDongWordSearchPenGet.FlyPosition = GuidePos_tran.position;
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchPenGet", isPop: true, UIManager.MainUIType.Type_Common, 0);
		}
	}

	public bool SetLetterItems(string word)
	{
		countSetLetterItems++;
		if (countSetLetterItems > 100)
		{
			Debug.LogError("SetLetterItems out " + countSetLetterItems);
			countSetLetterItems = 0;
			return false;
		}
		char[] array = word.ToCharArray();
		int num = array.Length;
		List<int> outIndexByLength = GetOutIndexByLength(num);
		int num2 = -1;
		bool flag = true;
		do
		{
			num2 = UnityEngine.Random.Range(0, 100);
			if (!outIndexByLength.Contains(num2))
			{
				flag = false;
			}
		}
		while (flag);
		HuoDongWordSearchLetterItem itemByRowCol = GetItemByRowCol(num2);
		List<HuoDongWordSearchLetterItem> list = new List<HuoDongWordSearchLetterItem>();
		if (itemByRowCol.isCanAddLetter(array[0].ToString()))
		{
			list.Add(itemByRowCol);
			List<HuoDongWordSearchLetterItem> list2 = new List<HuoDongWordSearchLetterItem>();
			List<int> randomDirectionByLength = itemByRowCol.GetRandomDirectionByLength(num);
			bool flag2 = false;
			int dir = 0;
			for (int i = 0; i < randomDirectionByLength.Count; i++)
			{
				int num3 = randomDirectionByLength[i];
				int num4 = itemByRowCol.Row;
				int num5 = itemByRowCol.Col;
				Vector2Int offsetRCByDir = GetOffsetRCByDir(num3);
				bool flag3 = true;
				list2.Clear();
				for (int j = 1; j < num; j++)
				{
					num4 += offsetRCByDir.x;
					num5 += offsetRCByDir.y;
					HuoDongWordSearchLetterItem itemByRC = GetItemByRC(num4, num5);
					if (!itemByRC.isCanAddLetter(array[j].ToString()))
					{
						flag3 = false;
					}
					else
					{
						list2.Add(itemByRC);
					}
				}
				if (flag3)
				{
					dir = num3;
					flag2 = true;
					break;
				}
			}
			if (allWordLetterItem_dic.ContainsKey(word))
			{
				Debug.LogError("same key word:" + word);
				return false;
			}
			if (flag2)
			{
				list.AddRange(list2);
				for (int k = 0; k < list.Count; k++)
				{
					list[k].SetLetter(array[k].ToString());
				}
				HuoDongWordSearchAddSuc huoDongWordSearchAddSuc = new HuoDongWordSearchAddSuc();
				huoDongWordSearchAddSuc.dir = dir;
				huoDongWordSearchAddSuc.sucSelectLetterItems = list;
				if (!allWordLetterItem_dic.ContainsKey(word))
				{
					allWordLetterItem_dic.Add(word, huoDongWordSearchAddSuc);
					allWords.Add(word);
				}
				else
				{
					Debug.LogError("same key word:" + word);
				}
				return true;
			}
			return SetLetterItems(word);
		}
		return SetLetterItems(word);
	}

	public List<int> GetOutIndexByLength(int length)
	{
		switch (length)
		{
		case 7:
			if (out7Indexs.Count > 0)
			{
				return out7Indexs;
			}
			break;
		case 8:
			if (out8Indexs.Count > 0)
			{
				return out8Indexs;
			}
			break;
		case 9:
			if (out9Indexs.Count > 0)
			{
				return out9Indexs;
			}
			break;
		}
		List<int> list = new List<int>();
		if (length <= 6)
		{
			return list;
		}
		if (length >= 7)
		{
			list.AddRange(RowCol7OutIdx);
		}
		if (length >= 8)
		{
			list.AddRange(RowCol8OutIdx);
		}
		if (length >= 9)
		{
			list.AddRange(RowCol9OutIdx);
		}
		switch (length)
		{
		case 7:
			out7Indexs = list;
			break;
		case 8:
			out8Indexs = list;
			break;
		case 9:
			out9Indexs = list;
			break;
		}
		return list;
	}

	public void SetDownLetter(HuoDongWordSearchLetterItem hdwsli)
	{
		if (wordSearchLetterItem_start == null)
		{
			wordSearchLetterItem_start = hdwsli;
		}
		isUP = false;
		wordSearchLetterItem_end = hdwsli;
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordSearchList[4]);
	}

	public void SetEnterLetter(HuoDongWordSearchLetterItem hdwsli)
	{
		if (wordSearchLetterItem_start == null)
		{
			wordSearchLetterItem_start = hdwsli;
		}
		wordSearchLetterItem_end = hdwsli;
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordSearchList[0]);
	}

	public void SetUpLetter(HuoDongWordSearchLetterItem hdwsli)
	{
		isUP = true;
		CheckEnd();
	}

	public void CheckEnd()
	{
		bool flag = false;
		if (allWords.Contains(curSelectWord))
		{
			flag = true;
		}
		if (completeWords.Contains(curSelectWord))
		{
			flag = false;
		}
		if (isTimeOut)
		{
			flag = false;
		}
		if (flag)
		{
			for (int i = 0; i < curSelectList.Count; i++)
			{
				curSelectList[i].SetSucSelect();
			}
			for (int j = 0; j < words.Length; j++)
			{
				words[j].Check(curSelectWord);
			}
			completeWords.Add(curSelectWord);
			if (completeWords.Count >= 3 && PlayerInfo.Instance.GetWordSearchSaveData().Tutorial02 == 0)
			{
				if (GameObject.Find("HuoDongWordSearchTutorialUseItem") == null)
				{
					UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchTutorialUseItem", isPop: true);
				}
				PlayerInfo.Instance.GetWordSearchSaveData().SetTutorial02(1);
			}
			lastSelectWord = string.Empty;
			selectWord.Show(this, curSelectWord, 1);
			SetSucText(curSelectWord, GetLineColor(isSuc: false));
			if (guide_line != null && curSelectWord.Equals(GetGuideLine().word))
			{
				ClearGuide();
			}
			if (completeWords.Count >= words.Length)
			{
				StartCoroutine(ShowResult(isSuc: true));
			}
			Debug.Log("curSelectWord:" + curSelectWord);
			UpdateLine(1);
			if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 2)
			{
				HuoDongWordSearchTutorialUI.isUpdateTutorial = true;
				if (!curSelectWord.Equals(GetGuideLine().word))
				{
					ClearGuide();
				}
				PlayerInfo.Instance.GetWordSearchSaveData().SetTutorial(3);
			}
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordSearchList[2]);
		}
		else
		{
			UpdateLine(2);
			lastSelectWord = string.Empty;
			if (!string.IsNullOrEmpty(curSelectWord))
			{
				selectWord.Show(this, curSelectWord, 2);
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordSearchList[1]);
			}
		}
		ClearSelect();
	}

	private IEnumerator ShowResult(bool isSuc)
	{
		yield return new WaitForSeconds(0.1f);
		if (isSuc)
		{
			Debug.Log("成功结算");
			isShowResult = true;
			yield return new WaitForSeconds(0.5f);
			int getRewardType = PlayerInfo.Instance.GetWordSearchSaveData().AddLevel();
			WordFaceBook.Instance.UpdateTrackTaskEvent("WordSearch", "LevelCompleteWordSearch_" + PlayerInfo.Instance.GetWordSearchSaveData().GetCurPlayLevel());
			if (getRewardType > 0)
			{
				ShowRewardUI(getRewardType);
			}
			else
			{
				ShowRewardUI(0);
			}
			WordFaceBook.Instance.UpdateUserdataMill();
		}
		else
		{
			Debug.Log("失败结算");
			ShowTimeOut();
		}
	}

	public void ShowRewardUI(int type)
	{
		showReward_time = 0.8f;
		move_tran.DOAnchorPosX(1000f, 0.5f).SetDelay(0.5f);
		hide_tran.gameObject.SetActive(value: false);
		HuoDongWordSearchResult.RewardType = type;
	}

	public void ShowTimeOut()
	{
		if (GameObject.Find("HuoDongWordSearchTimeOut") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchTimeOut", isPop: true);
		}
	}

	public void ClearGuide()
	{
		if (guide_line != null)
		{
			GetGuideLine().setState(0);
			for (int i = 0; i < GetGuideLine().hdwsas.sucSelectLetterItems.Count; i++)
			{
				GetGuideLine().hdwsas.sucSelectLetterItems[i].CutSucSelect();
			}
		}
	}

	public void SetSucText(string word, Color color)
	{
		for (int i = 0; i < sucTextList.Count; i++)
		{
			if (sucTextList[i].State == 0)
			{
				sucTextList[i].Init(word, color);
				return;
			}
		}
		GameObject gameObject = UnityEngine.Object.Instantiate(sucTextRes_go);
		gameObject.transform.SetParent(sucTextRoot_rectTran);
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		HuoDongWordSearchSucText component = gameObject.GetComponent<HuoDongWordSearchSucText>();
		component.Init(word, color);
		sucTextList.Add(component);
	}

	public void HelpWord()
	{
		TutorialWordSearchUseItem.isHide = true;
		if (GetGuideLine().State != 1 && !isShowResult)
		{
			bool flag = false;
			WordSearchSaveData wordSearchSaveData = PlayerInfo.Instance.GetWordSearchSaveData();
			if (wordSearchSaveData.GuideItemAmount > 0)
			{
				wordSearchSaveData.AddGuideItemAmount(-1);
				flag = true;
			}
			else if (PlayerInfo.Instance.DecGold(wordSearchSaveData.GuideItemGoldNum))
			{
				flag = true;
			}
			if (flag)
			{
				Help(string.Empty);
			}
		}
	}

	public void Help(string temp_word = "")
	{
		string text = string.Empty;
		if (!string.IsNullOrEmpty(temp_word))
		{
			text = temp_word;
		}
		else
		{
			List<string> list = new List<string>();
			for (int i = 0; i < allWords.Count; i++)
			{
				if (!completeWords.Contains(allWords[i]))
				{
					list.Add(allWords[i]);
				}
			}
			int num = UnityEngine.Random.Range(0, list.Count);
			if (num < list.Count)
			{
				text = list[num];
			}
		}
		if (!string.IsNullOrEmpty(text))
		{
			HuoDongWordSearchAddSuc huoDongWordSearchAddSuc = allWordLetterItem_dic[text];
			GetGuideLine().SetStartEnd(huoDongWordSearchAddSuc.sucSelectLetterItems[0], huoDongWordSearchAddSuc.sucSelectLetterItems[huoDongWordSearchAddSuc.sucSelectLetterItems.Count - 1], text.Length, 1, huoDongWordSearchAddSuc.dir);
			GetGuideLine().transform.SetAsLastSibling();
			GetGuideLine().word = text;
			GetGuideLine().hdwsas = huoDongWordSearchAddSuc;
			for (int j = 0; j < huoDongWordSearchAddSuc.sucSelectLetterItems.Count; j++)
			{
				huoDongWordSearchAddSuc.sucSelectLetterItems[j].SetSelect(j);
				huoDongWordSearchAddSuc.sucSelectLetterItems[j].SetSucSelect();
			}
		}
	}

	public void DebugWord(string word)
	{
		curSelectWord = word;
		CheckEnd();
	}

	public void BtnVideoClick()
	{
		if (AdsManager.Instance.ShowIncentivizedAd("WordSearchVideo"))
		{
			BtnVideoScript.isStop = true;
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardDPEvent));
		}
	}

	private void RewardDPEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardDPEvent));
		if (ret)
		{
			if (GetGuideLine().State == 1)
			{
				isShowAddItemEff = true;
				PlayerInfo.Instance.GetWordSearchSaveData().AddGuideItemAmount(1);
			}
			else
			{
				Help(string.Empty);
			}
		}
	}

	public Color GetLineColor(bool isSuc)
	{
		if (isSuc)
		{
			Color result = lineColors[curColorIndex];
			curColorIndex++;
			if (curColorIndex >= lineColors.Length)
			{
				curColorIndex = 0;
			}
			return result;
		}
		return lineColors[curColorIndex];
	}

	public Vector2Int GetOffsetRCByDir(int Direction)
	{
		int x = 0;
		int y = 0;
		switch (Direction)
		{
		case 0:
			x = 0;
			y = 1;
			break;
		case 1:
			x = -1;
			y = 1;
			break;
		case 2:
			x = -1;
			y = 0;
			break;
		case 3:
			x = -1;
			y = -1;
			break;
		case 4:
			x = 0;
			y = -1;
			break;
		case 5:
			x = 1;
			y = -1;
			break;
		case 6:
			x = 1;
			y = 0;
			break;
		case 7:
			x = 1;
			y = 1;
			break;
		}
		return new Vector2Int(x, y);
	}

	public void ClearSelect()
	{
		ClearAllSelect();
		wordSearchLetterItem_start = null;
		wordSearchLetterItem_end = null;
	}

	public void ClearLines()
	{
		cur_line = null;
		for (int i = 0; i < lines.Count; i++)
		{
			lines[i].transform.SetAsLastSibling();
			lines[i].setState(0);
		}
	}

	public void ResetAll()
	{
		foreach (KeyValuePair<int, HuoDongWordSearchLetterItem> item in item_dic)
		{
			item.Value.Clear();
		}
		Init();
		ClearGuide();
		ClearLines();
		selectWord.Hide();
		if (gold_script != null && AddGoldNum > 0)
		{
			PlayerInfo.Instance.AddGold(-AddGoldNum, 17);
			gold_script.ResetGold();
			gold_script.CollectGold(gold_flyTran.position, 5, 1f, null);
			gold_script.AddGold(AddGoldNum, 1f, 1f, isAdd2PlayerInfo: true, 18);
			AddGoldNum = 0;
		}
	}

	private void Update()
	{
		if (isTest)
		{
			isTest = false;
		}
		if (Input.touchCount == 0)
		{
			if (upTimeOut > 0f)
			{
				waitConsoleTime += Time.deltaTime;
				if (waitConsoleTime > 1f)
				{
					waitConsoleTime = 0f;
				}
				upTimeOut -= Time.deltaTime;
				if (upTimeOut <= 0f && selectWord.endType == 0)
				{
					SetUpLetter(null);
				}
			}
			if (!isUP)
			{
				SetUpLetter(null);
			}
		}
		else
		{
			upTimeOut = 0.1f;
		}
		if (!AdsManager.Instance.IncentivizedAdState)
		{
			if (video_go.activeInHierarchy)
			{
				video_go.SetActive(value: false);
			}
		}
		else if (!video_go.activeInHierarchy)
		{
			video_go.SetActive(value: true);
		}
		if (isCloseClick)
		{
			if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 >= 4)
			{
				BtnCloseClick();
			}
			isCloseClick = false;
		}
		if (isExit)
		{
			Exit();
			isExit = false;
		}
		if (isShowAddItemEff)
		{
			PlayerInfo.Instance.GetWordSearchSaveData().AddGuideItemAmount(-1);
			ShowAddItemEff();
			isShowAddItemEff = false;
		}
		if (AddTime > 0)
		{
			curTime += AddTime;
			AddTime = 0;
		}
		if (isTryAgain)
		{
			ResetAll();
			isTryAgain = false;
		}
		if (showReward_time > 0f)
		{
			showReward_time -= Time.deltaTime;
			if (showReward_time <= 0f && GameObject.Find("HuoDongWordSearchResult") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchResult", isPop: true);
			}
		}
		UpdateLogic();
		UpdateTime();
		UpdateGuideTxt();
		UpdateCheckTutorial();
	}

	public void UpdateCheckTutorial()
	{
		if (!CheckTutorial)
		{
			return;
		}
		dpc_tutorial_waitTime += Time.deltaTime;
		if (!(dpc_tutorial_waitTime > 0f))
		{
			return;
		}
		if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 2 && GameObject.Find("HuoDongWordSearchTutorialUI") == null)
		{
			for (int i = 0; i < words.Length; i++)
			{
				HuoDongWordSearchAddSuc huoDongWordSearchAddSuc = allWordLetterItem_dic[words[i].word];
				if (huoDongWordSearchAddSuc.dir % 2 == 0)
				{
					HuoDongWordSearchTutorialUI.TutorialWord = words[i].word;
					break;
				}
			}
			if (string.IsNullOrEmpty(HuoDongWordSearchTutorialUI.TutorialWord))
			{
				int num = UnityEngine.Random.Range(0, words.Length);
				HuoDongWordSearchWord huoDongWordSearchWord = words[num];
				HuoDongWordSearchTutorialUI.TutorialWord = huoDongWordSearchWord.word;
			}
			Help(HuoDongWordSearchTutorialUI.TutorialWord);
			GameObject gameObject = UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchTutorialUI");
			gameObject.name = "HuoDongWordSearchTutorialUI";
			HuoDongWordSearchTutorialUI component = gameObject.GetComponent<HuoDongWordSearchTutorialUI>();
			HuoDongWordSearchAddSuc huoDongWordSearchAddSuc2 = allWordLetterItem_dic[HuoDongWordSearchTutorialUI.TutorialWord];
			component.SetMovePos(huoDongWordSearchAddSuc2.sucSelectLetterItems[0].transform.position, huoDongWordSearchAddSuc2.sucSelectLetterItems[huoDongWordSearchAddSuc2.sucSelectLetterItems.Count - 1].transform.position);
		}
		CheckTutorial = false;
		dpc_tutorial_waitTime = 0f;
	}

	public void UpdateGuideTxt()
	{
		if (temp_guideitemAmount != PlayerInfo.Instance.GetWordSearchSaveData().GuideItemAmount)
		{
			temp_guideitemAmount = PlayerInfo.Instance.GetWordSearchSaveData().GuideItemAmount;
			if (temp_guideitemAmount > 0)
			{
				RectTransform rectTransform = GuideTxt.rectTransform;
				Vector2 anchoredPosition = GuideTxt.rectTransform.anchoredPosition;
				rectTransform.anchoredPosition = new Vector2(31f, anchoredPosition.y);
				GuideTxt.text = temp_guideitemAmount + string.Empty;
				GuideIcon_img.gameObject.SetActive(value: false);
			}
			else
			{
				RectTransform rectTransform2 = GuideTxt.rectTransform;
				Vector2 anchoredPosition2 = GuideTxt.rectTransform.anchoredPosition;
				rectTransform2.anchoredPosition = new Vector2(-11.5f, anchoredPosition2.y);
				GuideTxt.text = PlayerInfo.Instance.GetWordSearchSaveData().GuideItemGoldNum + string.Empty;
				GuideIcon_img.gameObject.SetActive(value: true);
			}
		}
	}

	public void UpdateTime()
	{
		if (isShowResult)
		{
			return;
		}
		sound_time += Time.deltaTime;
		if (sound_time >= 0.3f)
		{
			if (curTime > 0 && curTime < 15)
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordSearchList[3]);
			}
			sound_time = 0f;
		}
		waitTime += Time.deltaTime;
		if (waitTime > 1f && curTime > 0)
		{
			curTime--;
			isTimeOut = false;
			if (curTime <= 0)
			{
				isTimeOut = true;
				StartCoroutine(ShowResult(isSuc: false));
				curTime = 0;
			}
			SetTimeText();
			waitTime = 0f;
		}
	}

	public void UpdateLogic()
	{
		if (!(wordSearchLetterItem_start != null) || !(wordSearchLetterItem_end != null))
		{
			return;
		}
		startItem = new Vector2Int(wordSearchLetterItem_start.Col, wordSearchLetterItem_start.Row);
		endItem = new Vector2Int(wordSearchLetterItem_end.Col, wordSearchLetterItem_end.Row);
		angleOfLine = Mathf.Atan2(endItem.y - startItem.y, endItem.x - startItem.x) * 180f / (float)Math.PI;
		UpdateDirection(angleOfLine);
		UpdateEndItem();
		if (curSelectList.Count > 0)
		{
			wordSearchLetterItem_SelectEnd = curSelectList[curSelectList.Count - 1];
			curSelectWord = string.Empty;
			for (int i = 0; i < curSelectList.Count; i++)
			{
				curSelectWord += curSelectList[i].letter;
			}
		}
		else
		{
			selectLetterCount = 0;
		}
		if (string.IsNullOrEmpty(lastSelectWord) || !lastSelectWord.Equals(curSelectWord))
		{
			lastSelectWord = curSelectWord;
			selectWord.Show(this, curSelectWord);
		}
		UpdateLine();
		lastAngle = angleOfLine;
	}

	public void UpdateLine(int lineType = 0)
	{
		switch (lineType)
		{
		case 0:
			GetLine().SetStartEnd(wordSearchLetterItem_start, wordSearchLetterItem_SelectEnd, curSelectList.Count);
			GetLine().transform.SetAsLastSibling();
			break;
		case 1:
			GetLine().SetSucLine(wordSearchLetterItem_start, wordSearchLetterItem_SelectEnd, curSelectList.Count);
			break;
		default:
			GetLine().Clear();
			break;
		}
	}

	public HuoDongWordSearchLine GetGuideLine()
	{
		if (guide_line == null)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(lineRes_go);
			gameObject.transform.SetParent(lineRoot_rectTran);
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localScale = Vector3.one;
			gameObject.transform.name = "guide_line";
			guide_line = gameObject.GetComponent<HuoDongWordSearchLine>();
			guide_line.Init(this);
			guide_line.gameObject.SetActive(value: false);
		}
		return guide_line;
	}

	public HuoDongWordSearchLine GetLine()
	{
		if (cur_line == null || cur_line.State == 2)
		{
			for (int i = 0; i < lines.Count; i++)
			{
				if (lines[i].State == 0)
				{
					cur_line = lines[i];
					return cur_line;
				}
			}
			GameObject gameObject = UnityEngine.Object.Instantiate(lineRes_go);
			gameObject.transform.SetParent(lineRoot_rectTran);
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localScale = Vector3.one;
			HuoDongWordSearchLine component = gameObject.GetComponent<HuoDongWordSearchLine>();
			component.Init(this);
			cur_line = component;
			lines.Add(component);
		}
		return cur_line;
	}

	public void UpdateDirection(float angle)
	{
		if (angle <= angle22d5 && angle >= 0f - angle22d5)
		{
			Direction = 0;
		}
		else if (angle <= 0f - angle22d5 && angle >= 0f - angle22d5 - 45f)
		{
			Direction = 1;
		}
		else if (angle <= 0f - angle22d5 - 45f && angle >= 0f - angle22d5 - 90f)
		{
			Direction = 2;
		}
		else if (angle <= 0f - angle22d5 - 90f && angle >= 0f - angle22d5 - 135f)
		{
			Direction = 3;
		}
		else if (angle <= 0f - angle22d5 - 135f || angle >= angle22d5 + 135f)
		{
			Direction = 4;
		}
		else if (angle <= angle22d5 + 135f && angle >= angle22d5 + 90f)
		{
			Direction = 5;
		}
		else if (angle <= angle22d5 + 90f && angle >= angle22d5 + 45f)
		{
			Direction = 6;
		}
		else if (angle <= angle22d5 + 45f && angle >= angle22d5)
		{
			Direction = 7;
		}
	}

	public void UpdateEndItem()
	{
		int num = 0;
		int num2 = 0;
		int num3 = wordSearchLetterItem_start.Row;
		int num4 = wordSearchLetterItem_start.Col;
		int num5 = wordSearchLetterItem_start.Row;
		int num6 = wordSearchLetterItem_start.Col;
		int num7 = 0;
		if (Direction == 0)
		{
			num = 0;
			num2 = 1;
			num7 = 0;
		}
		else if (Direction == 1)
		{
			num = -1;
			num2 = 1;
			num7 = 1;
		}
		else if (Direction == 2)
		{
			num = -1;
			num2 = 0;
			num7 = 2;
		}
		else if (Direction == 3)
		{
			num = -1;
			num2 = -1;
			num7 = 1;
		}
		else if (Direction == 4)
		{
			num = 0;
			num2 = -1;
			num7 = 0;
		}
		else if (Direction == 5)
		{
			num = 1;
			num2 = -1;
			num7 = 1;
		}
		else if (Direction == 6)
		{
			num = 1;
			num2 = 0;
			num7 = 2;
		}
		else if (Direction == 7)
		{
			num = 1;
			num2 = 1;
			num7 = 1;
		}
		switch (num7)
		{
		case 0:
			num6 = wordSearchLetterItem_end.Col;
			break;
		case 1:
		{
			int num8 = num3;
			int num9 = num4;
			for (int i = 0; i < 10; i++)
			{
				if (num8 == wordSearchLetterItem_end.Row || num9 == wordSearchLetterItem_end.Col)
				{
					num5 = num8;
					num6 = num9;
				}
				num8 += num;
				num9 += num2;
				if (num8 < 0 || num9 < 0 || num8 >= maxRow || num9 >= maxCol)
				{
					break;
				}
			}
			break;
		}
		case 2:
			num5 = wordSearchLetterItem_end.Row;
			break;
		}
		ClearAllSelect();
		SelectLetterItem(num3, num4);
		for (int j = 0; j < 20; j++)
		{
			if (num3 == num5 && num4 == num6)
			{
				break;
			}
			if (num3 < 0)
			{
				break;
			}
			if (num4 < 0)
			{
				break;
			}
			if (num3 >= maxRow)
			{
				break;
			}
			if (num4 >= maxCol)
			{
				break;
			}
			num3 += num;
			num4 += num2;
			SelectLetterItem(num3, num4);
		}
	}

	public void ClearAllSelect()
	{
		for (int i = 0; i < curSelectList.Count; i++)
		{
			curSelectList[i].ClearSelect();
		}
		curSelectList.Clear();
	}

	public void SelectLetterItem(int row, int col)
	{
		HuoDongWordSearchLetterItem itemByRC = GetItemByRC(row, col);
		if (itemByRC != null)
		{
			itemByRC.SetSelect(curSelectList.Count);
			curSelectList.Add(itemByRC);
		}
	}

	public HuoDongWordSearchLetterItem GetItemByRC(int row, int col)
	{
		int key = row * 10 + col;
		if (item_dic.ContainsKey(key))
		{
			return item_dic[key];
		}
		return null;
	}

	public HuoDongWordSearchLetterItem GetItemByRowCol(int rowcol)
	{
		if (item_dic.ContainsKey(rowcol))
		{
			return item_dic[rowcol];
		}
		return null;
	}

	public void BtnCloseClick()
	{
		if (!isExiting && GameObject.Find("HuoDongWordSearchClose") == null)
		{
			HuoDongWordSearchClost.ShowType = 0;
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchClose", isPop: true);
		}
	}

	public void Exit()
	{
		isExiting = true;
		move_tran.DOAnchorPosX(1000f, 0.5f).OnComplete(delegate
		{
			UnityEngine.Object.Destroy(base.gameObject);
			isExiting = false;
		});
		hide_tran.gameObject.SetActive(value: false);
		HuoDongWordSearchUI.isShowUI = true;
		if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 2)
		{
			HuoDongWordSearchTutorialUI.HideState = 1;
			PlayerInfo.Instance.GetWordSearchSaveData().SetTutorial(4);
		}
		AdsManager.Instance.ShowInterstitialAd("Pop_ForceAd_WordsearchExit");
		AdsManager.Instance.DestroyBannerAD();
	}

	public void BtnSetClick()
	{
		if (GameObject.Find("SettingUI") == null)
		{
			SettingUIScript.BackType = 1;
			UIManager.Instance.LoadMainUI("UI/SettingUI", isPop: true);
		}
	}
}
