using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordSearchClost : MonoBehaviour
{
	public static int ShowType;

	public static bool isClose;

	public Text des_txt;

	public Text exit_txt;

	public Text keep_txt;

	private void Start()
	{
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1238);
		exit_txt.text = TextLibraryDataManagerCsv.instance.GetText(1239);
		keep_txt.text = TextLibraryDataManagerCsv.instance.GetText(1240);
		isClose = false;
	}

	private void Update()
	{
		if (isClose)
		{
			BtnClose();
			isClose = false;
		}
	}

	public void BtnExit()
	{
		if (ShowType == 0)
		{
			HuoDongWordSearch.isExit = true;
		}
		else if (ShowType == 1)
		{
			HuoDongWordFind.isGameClose = true;
		}
		BtnClose();
	}

	public void BtnClose()
	{
		Object.Destroy(base.gameObject);
	}
}
