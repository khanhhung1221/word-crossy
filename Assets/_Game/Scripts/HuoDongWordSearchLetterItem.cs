using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HuoDongWordSearchLetterItem : MonoBehaviour, IPointerUpHandler, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler, IEventSystemHandler
{
	public Text letter_txt;

	public RectTransform rectTran;

	public HuoDongWordSearch hdws;

	public string letter;

	public int index;

	public int Row;

	public int Col;

	public int RowCol;

	public int isSelected;

	private int[] ItemNumInDirection;

	public bool isTest;

	private void Start()
	{
	}

	public void Init(HuoDongWordSearch hdws, int row, int col)
	{
		this.hdws = hdws;
		Row = row;
		Col = col;
		RowCol = Row * 10 + col;
		base.gameObject.SetActive(value: true);
		SetLetter(string.Empty);
	}

	public void Clear()
	{
		isSelected = 0;
		SetLetter(string.Empty);
	}

	public void SetLetter(string letter)
	{
		this.letter = letter;
		letter_txt.text = letter;
		ClearSelect();
	}

	public void SetSelect(int index)
	{
		letter_txt.color = Color.white;
	}

	public void SetSucSelect()
	{
		isSelected++;
		ClearSelect();
	}

	public void CutSucSelect()
	{
		isSelected--;
		if (isSelected < 0)
		{
			isSelected = 0;
		}
		ClearSelect();
	}

	public bool isCanAddLetter(string letter)
	{
		if (string.IsNullOrEmpty(this.letter) || this.letter.Equals(letter))
		{
			return true;
		}
		return false;
	}

	public void ClearSelect()
	{
		if (isSelected > 0)
		{
			letter_txt.color = Color.white;
		}
		else
		{
			letter_txt.color = Color.black;
		}
	}

	public List<int> GetRandomDirectionByLength(int length)
	{
		if (ItemNumInDirection == null)
		{
			InitItemNumDirection();
		}
		List<int> list = new List<int>();
		for (int i = 0; i < ItemNumInDirection.Length; i++)
		{
			if (ItemNumInDirection[i] >= length)
			{
				list.Add(i);
			}
		}
		List<int> list2 = new List<int>();
		if (list.Count > 1)
		{
			for (int j = 0; j < list.Count; j++)
			{
				int num = list[j];
				if (num % 2 != 0)
				{
					list2.Add(num);
				}
			}
			for (int k = 0; k < list.Count; k++)
			{
				int num2 = list[k];
				if (num2 % 2 == 0)
				{
					list2.Add(num2);
				}
			}
		}
		return list2;
	}

	public void InitItemNumDirection()
	{
		ItemNumInDirection = new int[8];
		for (int i = 0; i < ItemNumInDirection.Length; i++)
		{
			int direction = i;
			int num = Row;
			int num2 = Col;
			Vector2Int offsetRCByDir = hdws.GetOffsetRCByDir(direction);
			int num3 = 1;
			for (int j = 0; j < 10; j++)
			{
				num += offsetRCByDir.x;
				num2 += offsetRCByDir.y;
				if (num2 < 0 || num < 0 || num2 > 9 || num > 9)
				{
					break;
				}
				num3++;
			}
			ItemNumInDirection[i] = num3;
		}
	}

	public void Update()
	{
		if (isTest)
		{
			InitItemNumDirection();
			isTest = false;
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		hdws.SetDownLetter(this);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		hdws.SetEnterLetter(this);
	}

	public void OnPointerExit(PointerEventData eventData)
	{
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		hdws.SetUpLetter(this);
	}
}
