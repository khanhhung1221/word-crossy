using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordSearchLine : MonoBehaviour
{
	public Image img;

	public int State;

	public int Type;

	public int Direction = -1;

	public RectTransform rectTran;

	public HuoDongWordSearchLetterItem start_item;

	public HuoDongWordSearchLetterItem end_item;

	public int letter_cnt;

	public string word;

	public HuoDongWordSearchAddSuc hdwsas;

	private HuoDongWordSearch hdws;

	private void Start()
	{
	}

	public void Init(HuoDongWordSearch hdws)
	{
		this.hdws = hdws;
		Clear();
	}

	public void Clear()
	{
		setState(0);
	}

	public void SetStartEnd(HuoDongWordSearchLetterItem start_item, HuoDongWordSearchLetterItem end_item, int letter_cnt, int type = 0, int dir = -1)
	{
		this.start_item = start_item;
		this.end_item = end_item;
		this.letter_cnt = letter_cnt;
		Type = type;
		Direction = dir;
		setState(1);
	}

	public void SetSucLine(HuoDongWordSearchLetterItem start_item, HuoDongWordSearchLetterItem end_item, int letter_cnt)
	{
		this.start_item = start_item;
		this.end_item = end_item;
		this.letter_cnt = letter_cnt;
		setState(2);
	}

	public void setState(int state)
	{
		State = state;
		if (State == 0)
		{
			img.DOKill();
			base.gameObject.SetActive(value: false);
		}
		else if (State == 1)
		{
			base.gameObject.SetActive(value: true);
			DrawLine();
		}
		else if (State == 2)
		{
			base.gameObject.SetActive(value: true);
			DrawLine();
		}
	}

	public void ImgAnim()
	{
		img.DOFade(0.2f, 0.5f).SetDelay(0.3f).OnComplete(delegate
		{
			img.DOFade(1f, 0.5f).OnComplete(delegate
			{
				ImgAnim();
			});
		});
	}

	public void DrawLine()
	{
		if (Type == 0)
		{
			Direction = hdws.Direction;
			img.color = hdws.GetLineColor(State == 2);
		}
		else if (Type == 1)
		{
			img.color = hdws.guideLineColor;
			img.transform.localScale = Vector3.one;
			ImgAnim();
		}
		rectTran.localEulerAngles = new Vector3(0f, 0f, Direction * 45);
		if (Direction % 2 != 0)
		{
			rectTran.anchoredPosition = start_item.rectTran.anchoredPosition;
			Vector2 sizeDelta = start_item.rectTran.sizeDelta;
			float num = sizeDelta.x + (float)(98 * (letter_cnt - 1));
			Vector2 sizeDelta2 = start_item.rectTran.sizeDelta;
			float num2 = sizeDelta2.x / 2f / num;
			RectTransform rectTransform = rectTran;
			float x = num2;
			Vector2 pivot = rectTran.pivot;
			rectTransform.pivot = new Vector2(x, pivot.y);
			RectTransform rectTransform2 = rectTran;
			float x2 = num;
			Vector2 sizeDelta3 = rectTran.sizeDelta;
			rectTransform2.sizeDelta = new Vector2(x2, sizeDelta3.y);
		}
		else if (Direction == 0 || Direction == 4)
		{
			if (Direction == 0)
			{
				RectTransform rectTransform3 = rectTran;
				Vector2 anchoredPosition = start_item.rectTran.anchoredPosition;
				float x3 = anchoredPosition.x;
				Vector2 sizeDelta4 = start_item.rectTran.sizeDelta;
				float x4 = x3 - sizeDelta4.x / 2f;
				Vector2 anchoredPosition2 = start_item.rectTran.anchoredPosition;
				rectTransform3.anchoredPosition = new Vector2(x4, anchoredPosition2.y);
			}
			else if (Direction == 4)
			{
				RectTransform rectTransform4 = rectTran;
				Vector2 anchoredPosition3 = start_item.rectTran.anchoredPosition;
				float x5 = anchoredPosition3.x;
				Vector2 sizeDelta5 = start_item.rectTran.sizeDelta;
				float x6 = x5 + sizeDelta5.x / 2f;
				Vector2 anchoredPosition4 = start_item.rectTran.anchoredPosition;
				rectTransform4.anchoredPosition = new Vector2(x6, anchoredPosition4.y);
			}
			Vector2 sizeDelta6 = start_item.rectTran.sizeDelta;
			float num3 = sizeDelta6.x + (float)(69 * (letter_cnt - 1));
			float num4 = 0f;
			RectTransform rectTransform5 = rectTran;
			float x7 = num4;
			Vector2 pivot2 = rectTran.pivot;
			rectTransform5.pivot = new Vector2(x7, pivot2.y);
			RectTransform rectTransform6 = rectTran;
			float x8 = num3;
			Vector2 sizeDelta7 = rectTran.sizeDelta;
			rectTransform6.sizeDelta = new Vector2(x8, sizeDelta7.y);
		}
		else if (Direction == 2 || Direction == 6)
		{
			if (Direction == 2)
			{
				RectTransform rectTransform7 = rectTran;
				Vector2 anchoredPosition5 = start_item.rectTran.anchoredPosition;
				float x9 = anchoredPosition5.x;
				Vector2 anchoredPosition6 = start_item.rectTran.anchoredPosition;
				float y = anchoredPosition6.y;
				Vector2 sizeDelta8 = start_item.rectTran.sizeDelta;
				rectTransform7.anchoredPosition = new Vector2(x9, y - sizeDelta8.y / 2f);
			}
			else if (Direction == 6)
			{
				RectTransform rectTransform8 = rectTran;
				Vector2 anchoredPosition7 = start_item.rectTran.anchoredPosition;
				float x10 = anchoredPosition7.x;
				Vector2 anchoredPosition8 = start_item.rectTran.anchoredPosition;
				float y2 = anchoredPosition8.y;
				Vector2 sizeDelta9 = start_item.rectTran.sizeDelta;
				rectTransform8.anchoredPosition = new Vector2(x10, y2 + sizeDelta9.y / 2f);
			}
			Vector2 sizeDelta10 = start_item.rectTran.sizeDelta;
			float num5 = sizeDelta10.x + (float)(69 * (letter_cnt - 1));
			float num6 = 0f;
			RectTransform rectTransform9 = rectTran;
			float x11 = num6;
			Vector2 pivot3 = rectTran.pivot;
			rectTransform9.pivot = new Vector2(x11, pivot3.y);
			RectTransform rectTransform10 = rectTran;
			float x12 = num5;
			Vector2 sizeDelta11 = rectTran.sizeDelta;
			rectTransform10.sizeDelta = new Vector2(x12, sizeDelta11.y);
		}
	}
}
