using DG.Tweening;
using UnityEngine;

public class HuoDongWordSearchPenGet : MonoBehaviour
{
	public static Vector3 FlyPosition;

	public RectTransform pos_rectTran;

	private void Start()
	{
		pos_rectTran.DOMove(FlyPosition, 0.5f).SetDelay(1f).OnComplete(delegate
		{
			PlayerInfo.Instance.GetWordSearchSaveData().AddGuideItemAmount(1);
			Object.Destroy(base.gameObject);
		});
	}
}
