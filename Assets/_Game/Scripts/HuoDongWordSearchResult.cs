using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HuoDongWordSearchResult : MonoBehaviour
{
	public static int RewardType;

	public static bool isCloseClick;

	public HuoDongRewardItem[] items;

	public Image box01_img;

	public Image box02_img;

	public Image box03_img;

	public Sprite box01_sp;

	public Sprite box02_sp;

	public Sprite box03_sp;

	public Image jinDu_img;

	public Text jinDu_txt;

	public GameObject BoxEff_go;

	public RectTransform TV_rect;

	public RectTransform Next_rect;

	public Text next_txt;

	public Vector2[] EndPos01;

	public Vector2[] EndPos02;

	public Vector2[] EndPos03;

	public Vector2[] EndPos04;

	private float jindu_end_fillAmount;

	private int cur_level_num;

	private int total_level_num = 5;

	private WordSearchSaveData wssd;

	private bool btnShowState;

	private float reward_time;

	private void Start()
	{
		next_txt.text = TextLibraryDataManagerCsv.instance.GetText(1213);
		AdsManager.Instance.ShowBannerAD();
		wssd = PlayerInfo.Instance.GetWordSearchSaveData();
		if (wssd.GetCurPlayLevel() % 5 == 0)
		{
			cur_level_num = 5;
		}
		else
		{
			cur_level_num = wssd.GetCurPlayLevel() % 5;
		}
		int num = cur_level_num - 1;
		jinDu_txt.text = num + "/" + total_level_num;
		float fillAmount = (float)num / (float)total_level_num;
		jinDu_img.fillAmount = fillAmount;
		jindu_end_fillAmount = (float)cur_level_num / (float)total_level_num;
		Debug.Log("result wssd.CurPlayLevel:" + wssd.GetCurPlayLevel());
		int num2 = (wssd.GetCurPlayLevel() - 1) / 5;
		if (num2 % 2 == 1)
		{
			box01_img.sprite = box01_sp;
			box02_img.sprite = box02_sp;
			box03_img.sprite = box03_sp;
		}
		UpdateBtns(isSet: true);
	}

	private void Update()
	{
		UpdateBtns();
		UpdateJinDu();
		UpdateReward();
		if (isCloseClick)
		{
			Next(isExit: true);
			isCloseClick = false;
		}
	}

	public void UpdateBtns(bool isSet = false)
	{
		if (AdsManager.Instance.IncentivizedAdState != btnShowState || isSet)
		{
			btnShowState = AdsManager.Instance.IncentivizedAdState;
			if (!AdsManager.Instance.IncentivizedAdState)
			{
				Next_rect.gameObject.SetActive(value: true);
				TV_rect.gameObject.SetActive(value: false);
				Next_rect.anchoredPosition = new Vector2(0f, -331f);
			}
			else
			{
				TV_rect.gameObject.SetActive(value: true);
				Next_rect.anchoredPosition = new Vector2(0f, -449f);
				Next_rect.localScale = Vector3.zero;
				BtnAni02(TV_rect.gameObject, delegate
				{
				}, 1f);
				BtnAni02(Next_rect.gameObject, null, 1.3f);
			}
		}
	}

	private void BtnAni02(GameObject obj, TweenCallback action, float delayTime = 0f)
	{
		obj.transform.localScale = Vector3.zero;
		obj.SetActive(value: true);
		obj.transform.DOScale(1f, 0.2f).SetDelay(delayTime).OnComplete(action);
	}

	public void UpdateJinDu()
	{
		if (!(jinDu_img.fillAmount < jindu_end_fillAmount))
		{
			return;
		}
		jinDu_img.fillAmount += 0.01f;
		if (jinDu_img.fillAmount >= jindu_end_fillAmount)
		{
			jinDu_img.fillAmount = jindu_end_fillAmount;
			jinDu_txt.text = cur_level_num + "/" + total_level_num;
			if (RewardType > 0)
			{
				reward_time = 0.5f;
				BoxEff_go.GetComponent<Animator>().enabled = true;
			}
		}
	}

	public void UpdateReward()
	{
		if (!(reward_time > 0f))
		{
			return;
		}
		reward_time -= Time.deltaTime;
		if (!(reward_time <= 0f))
		{
			return;
		}
		string[] array = null;
		array = ((RewardType % 2 != 0) ? WordSearchSaveData.box01.Split('&') : WordSearchSaveData.box02.Split('&'));
		for (int i = 0; i < items.Length; i++)
		{
			items[i].gameObject.SetActive(value: false);
		}
		int num = array.Length;
		Vector2[] array2 = EndPos01;
		if (num == 1)
		{
			array2 = EndPos01;
		}
		if (num == 2)
		{
			array2 = EndPos02;
		}
		if (num == 3)
		{
			array2 = EndPos03;
		}
		if (num == 4)
		{
			array2 = EndPos04;
		}
		for (int j = 0; j < array.Length; j++)
		{
			Debug.Log("wordsearch reslut reward item_strs " + j + ":" + array[j]);
			string[] array3 = array[j].Split('_');
			int num2 = int.Parse(array3[0]);
			int num3 = 0;
			switch (num2)
			{
			case 1:
			{
				int num4 = int.Parse(array3[1]);
				num3 = int.Parse(array3[2]);
				switch (num4)
				{
				case 1001:
					items[j].img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_lightbulb");
					items[j].img.rectTransform.sizeDelta = new Vector2(104f, 104f);
					break;
				case 1002:
					items[j].img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_finger");
					items[j].img.rectTransform.sizeDelta = new Vector2(104f, 104f);
					break;
				case 1003:
					items[j].img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_lightning");
					items[j].img.rectTransform.sizeDelta = new Vector2(104f, 104f);
					break;
				}
				break;
			}
			case 3:
				num3 = int.Parse(array3[1]);
				items[j].img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/jinbi_common_02");
				items[j].img.rectTransform.sizeDelta = new Vector2(126f, 95f);
				if (RewardType < PlayerInfo.Instance.GetWordSearchSaveData().GetMaxChapterNum())
				{
					HuoDongWordSearch.AddGoldNum = num3;
				}
				else
				{
					HuoDongWordSearchUI.AddGoldNum = num3;
				}
				break;
			case 4:
				num3 = int.Parse(array3[1]);
				items[j].img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/daily_search_marke");
				items[j].img.rectTransform.sizeDelta = new Vector2(78f, 86f);
				break;
			}
			items[j].num.text = string.Empty + num3;
			(items[j].transform as RectTransform).DOAnchorPos(array2[j], 0.5f).SetDelay((float)j * 0.1f).SetEase(Ease.OutBack)
				.OnComplete(delegate
				{
					AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_HuoDongList[3]);
				});
			items[j].gameObject.SetActive(value: true);
		}
	}

	public void WatchRewardVideo()
	{
		if (AdsManager.Instance.ShowIncentivizedAd("LevelCompleteWordSearch_Watch"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
		}
	}

	private void RewardADEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
		if (ret)
		{
			HuoDongWordSearch.isShowAddItemEff = true;
			wssd.AddGuideItemAmount(1);
			Next();
		}
		else
		{
			Next();
		}
	}

	private void Next(bool isExit = false)
	{
		if (RewardType == PlayerInfo.Instance.GetWordSearchSaveData().GetMaxChapterNum() || isExit)
		{
			HuoDongWordSearch.isExit = true;
			HuoDongWordSearch.isShowAddItemEff = false;
		}
		else
		{
			HuoDongWordSearch.isTryAgain = true;
		}
		Hide();
	}

	public void BtnNext()
	{
		if (wssd.GetCurPlayLevel() % 3 == 0)
		{
			AdsManager.Instance.ShowInterstitialAd("Pop_ForceAd_WordsearchComplete");
		}
		Next();
	}

	public void Hide()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}
}
