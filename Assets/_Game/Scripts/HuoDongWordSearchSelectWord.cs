using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordSearchSelectWord : MonoBehaviour
{
	public RectTransform rectTran;

	public Image bg_img;

	public Text text;

	public Animator animator;

	public int endType;

	private float waitEndTime;

	private void Start()
	{
	}

	public void Show(HuoDongWordSearch hdws, string word, int isEnd = 0)
	{
		endType = isEnd;
		text.text = word;
		text.Rebuild(CanvasUpdate.PreRender);
		float num = text.preferredWidth + 60f;
		RectTransform rectTransform = rectTran;
		float x = num;
		Vector2 sizeDelta = rectTran.sizeDelta;
		rectTransform.sizeDelta = new Vector2(x, sizeDelta.y);
		rectTran.gameObject.SetActive(value: true);
		waitEndTime = 0f;
		switch (isEnd)
		{
		case 1:
		{
			RectTransform rectTransform2 = rectTran;
			Vector2 anchoredPosition = rectTran.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(0f, anchoredPosition.y);
			animator.Play("suc");
			break;
		}
		case 2:
			animator.Play("fail");
			waitEndTime = 0.5f;
			break;
		default:
			bg_img.color = hdws.GetLineColor(isSuc: false);
			break;
		}
	}

	public void Hide()
	{
		rectTran.gameObject.SetActive(value: false);
	}

	private void Update()
	{
		if (waitEndTime > 0f)
		{
			waitEndTime -= Time.deltaTime;
			if (waitEndTime <= 0f)
			{
				Hide();
			}
		}
	}
}
