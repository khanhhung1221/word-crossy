using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordSearchSucText : MonoBehaviour
{
	public int State;

	public Text txt;

	private void Start()
	{
	}

	public void Init(string word, Color color)
	{
		txt.rectTransform.anchoredPosition = new Vector2(0f, 0f);
		txt.text = word;
		txt.color = color;
		txt.rectTransform.DOAnchorPosY(200f, 2f);
		txt.DOFade(0f, 1.5f).SetDelay(0.5f).OnComplete(delegate
		{
			State = 0;
			base.gameObject.SetActive(value: false);
		});
		State = 1;
		base.gameObject.SetActive(value: true);
	}
}
