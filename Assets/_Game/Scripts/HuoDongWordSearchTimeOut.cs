using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HuoDongWordSearchTimeOut : MonoBehaviour
{
	public static bool isBtnGiveUpClick;

	public RectTransform main_rectTran;

	public Text title_txt;

	public GameObject BtnWatch_go;

	public RectTransform BtnWatch_timeRect;

	public Text BtnWatch_timeTxt;

	public GameObject BtnGold_go;

	public RectTransform BtnGold_timeRect;

	public Text BtnGold_timeTxt;

	public Text BtnGold_goldNumTxt;

	public GameObject BtnGiveUp_go;

	public Text btnWatch_txt;

	public Text btnGold_txt;

	public Text btnGiveUp_txt;

	public GameObject JiaZai_go;

	public Transform JiaZai_yTran;

	public RectTransform BtnX_rect;

	private WordSearchSaveData hgsd;

	private int count = 1;

	private void Start()
	{
		isBtnGiveUpClick = false;
		title_txt.text = TextLibraryDataManagerCsv.instance.GetText(1215);
		btnWatch_txt.text = TextLibraryDataManagerCsv.instance.GetText(1216);
		btnGold_txt.text = TextLibraryDataManagerCsv.instance.GetText(1216);
		btnGiveUp_txt.text = TextLibraryDataManagerCsv.instance.GetText(1241);
		hgsd = PlayerInfo.Instance.GetWordSearchSaveData();
		BtnWatch_timeTxt.text = hgsd.Timeout_addTime + string.Empty;
		BtnGold_timeTxt.text = hgsd.Timeout_addTime + string.Empty;
		BtnGold_goldNumTxt.text = hgsd.Timeout_needGoldNum + string.Empty;
		BtnWatch_go.SetActive(value: true);
		if (PlayerInfo.Instance.LastOfferPayValue > 0f || !PlayerInfo.Instance.ShowRewardAD)
		{
			BtnWatch_go.SetActive(value: false);
		}
		SetPos();
	}

	public void SetPos()
	{
		if (!BtnWatch_go.activeInHierarchy)
		{
			RectTransform rectTransform = main_rectTran;
			Vector2 sizeDelta = main_rectTran.sizeDelta;
			rectTransform.sizeDelta = new Vector2(sizeDelta.x, 350f);
			RectTransform rectTransform2 = BtnGold_go.transform as RectTransform;
			RectTransform rectTransform3 = rectTransform2;
			Vector2 anchoredPosition = rectTransform2.anchoredPosition;
			rectTransform3.anchoredPosition = new Vector2(anchoredPosition.x, -148f);
			RectTransform rectTransform4 = BtnGiveUp_go.transform as RectTransform;
			RectTransform rectTransform5 = rectTransform4;
			Vector2 anchoredPosition2 = rectTransform4.anchoredPosition;
			rectTransform5.anchoredPosition = new Vector2(anchoredPosition2.x, -258f);
			RectTransform btnX_rect = BtnX_rect;
			Vector2 anchoredPosition3 = BtnX_rect.anchoredPosition;
			float x = anchoredPosition3.x;
			Vector2 anchoredPosition4 = BtnX_rect.anchoredPosition;
			btnX_rect.anchoredPosition = new Vector2(x, anchoredPosition4.y);
		}
	}

	public void GiveReward(int type)
	{
		HuoDongWordSearch.AddTime = hgsd.Timeout_addTime;
	}

	public void BtnWatchClick()
	{
		if (AdsManager.Instance.IncentivizedAdState && AdsManager.Instance.ShowIncentivizedAd("WordSearch_TimeOutWatch"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEnd));
		}
	}

	private void WatchVideoEnd(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(WatchVideoEnd));
		if (ret)
		{
			GiveReward(0);
			Hide();
		}
	}

	public void BtnGoldClick()
	{
		if (PlayerInfo.Instance.DecGold(hgsd.Timeout_needGoldNum))
		{
			GiveReward(1);
			WordGlobal.LogEvent("WordSearch_TimeOutGoldBuy");
			Hide();
		}
	}

	public void BtnGiveUpClick()
	{
		HuoDongWordSearch.isTryAgain = true;
		Hide();
	}

	public void BtnExitClick()
	{
		HuoDongWordSearch.isExit = true;
		Hide();
	}

	private void Hide()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}

	private void Update()
	{
		if (!AdsManager.Instance.IncentivizedAdState)
		{
			JiaZai_go.SetActive(value: true);
		}
		else
		{
			JiaZai_go.SetActive(value: false);
		}
		if (JiaZai_go.activeInHierarchy)
		{
			JiaZai_yTran.Rotate(0f, 0f, -360f * Time.deltaTime);
		}
		if (isBtnGiveUpClick)
		{
			BtnGiveUpClick();
			isBtnGiveUpClick = false;
		}
	}
}
