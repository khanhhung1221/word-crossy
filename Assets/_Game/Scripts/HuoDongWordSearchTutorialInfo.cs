using UnityEngine;

public class HuoDongWordSearchTutorialInfo : MonoBehaviour
{
	[HideInInspector]
	public string Obj_name;

	public string parent_name;

	public Transform parent_tran;

	private void Start()
	{
		Obj_name = base.transform.name;
		parent_name = base.transform.parent.name;
	}

	public void setParent(Transform tran)
	{
		parent_tran = tran;
	}
}
