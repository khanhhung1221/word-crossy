using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordSearchTutorialUI : MonoBehaviour
{
	public static bool isDPCTutorialShow;

	public static int HideState;

	public static string TutorialWord;

	public static bool isUpdateTutorial;

	public static bool Tutorial3Click;

	public Transform Main_tran;

	public RectTransform tip_rectTran;

	public Text des_text;

	public Text des_up_text;

	public GameObject Btn_go;

	public Text btn_text;

	public Image[] hide_img;

	public Text[] hide_text;

	public RectTransform jiantou_img;

	public RectTransform shou_rect;

	public BackKey bk;

	private WordSearchSaveData saveData;

	private int Tutorial;

	private List<HuoDongWordSearchTutorialInfo> temp_trans = new List<HuoDongWordSearchTutorialInfo>();

	private List<Canvas> item_canvas = new List<Canvas>();

	private string color_str = "fffc00";

	private string color_end = "</color>";

	private string tutorial01_word = string.Empty;

	private Color base_color;

	private int lineRender_order;

	private Vector3 start_pos;

	private Vector3 end_pos;

	private int anim_idx;

	private Image shou_img;

	private bool isHide;

	private bool isPlayHideAnim;

	private void Start()
	{
		isDPCTutorialShow = true;
		Init();
	}

	public void PlayShowAnim()
	{
		tip_rectTran.localScale = Vector3.zero;
		tip_rectTran.DOScale(1f, 0.5f);
	}

	public void SetMovePos(Vector3 start_pos, Vector3 end_pos)
	{
		this.start_pos = start_pos;
		this.end_pos = end_pos;
	}

	public void Init(bool isPlayAnim = true)
	{
		if (isPlayAnim)
		{
			PlayShowAnim();
		}
		saveData = PlayerInfo.Instance.GetWordSearchSaveData();
		Tutorial = saveData.Tutorial01;
		Btn_go.SetActive(value: false);
		HideState = 0;
		jiantou_img.gameObject.SetActive(value: false);
		des_text.gameObject.SetActive(value: true);
		des_up_text.gameObject.SetActive(value: false);
		shou_rect.gameObject.SetActive(value: false);
		if (Tutorial == 0)
		{
			des_text.text = TextLibraryDataManagerCsv.instance.GetText(1251);
			RectTransform rectTransform = tip_rectTran;
			Vector2 anchoredPosition = tip_rectTran.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, -330f);
			RectTransform rectTransform2 = tip_rectTran;
			Vector2 sizeDelta = tip_rectTran.sizeDelta;
			rectTransform2.sizeDelta = new Vector2(sizeDelta.x, 200f);
		}
		else if (Tutorial == 1)
		{
			des_text.text = TextLibraryDataManagerCsv.instance.GetText(1253);
			RectTransform rectTransform3 = tip_rectTran;
			Vector2 anchoredPosition2 = tip_rectTran.anchoredPosition;
			rectTransform3.anchoredPosition = new Vector2(anchoredPosition2.x, -330f);
			RectTransform rectTransform4 = tip_rectTran;
			Vector2 sizeDelta2 = tip_rectTran.sizeDelta;
			rectTransform4.sizeDelta = new Vector2(sizeDelta2.x, 200f);
		}
		else if (Tutorial == 2)
		{
			des_text.text = TextLibraryDataManagerCsv.instance.GetText(1247).Replace("{0}", TutorialWord);
			RectTransform rectTransform5 = tip_rectTran;
			Vector2 anchoredPosition3 = tip_rectTran.anchoredPosition;
			rectTransform5.anchoredPosition = new Vector2(anchoredPosition3.x, 440f);
			RectTransform rectTransform6 = tip_rectTran;
			Vector2 sizeDelta3 = tip_rectTran.sizeDelta;
			rectTransform6.sizeDelta = new Vector2(sizeDelta3.x, 150f);
		}
		else if (Tutorial == 3)
		{
			bk.enabled = true;
			Btn_go.SetActive(value: true);
			des_text.gameObject.SetActive(value: false);
			des_up_text.gameObject.SetActive(value: true);
			btn_text.text = TextLibraryDataManagerCsv.instance.GetText(1007);
			des_up_text.text = TextLibraryDataManagerCsv.instance.GetText(1248);
			RectTransform rectTransform7 = tip_rectTran;
			Vector2 anchoredPosition4 = tip_rectTran.anchoredPosition;
			rectTransform7.anchoredPosition = new Vector2(anchoredPosition4.x, 420f);
			RectTransform rectTransform8 = tip_rectTran;
			Vector2 sizeDelta4 = tip_rectTran.sizeDelta;
			rectTransform8.sizeDelta = new Vector2(sizeDelta4.x, 270f);
		}
		ShowLogic();
	}

	public void ShowLogic()
	{
		temp_trans.Clear();
		for (int i = 0; i < hide_img.Length; i++)
		{
			Image obj = hide_img[i];
			Color color = hide_img[i].color;
			float r = color.r;
			Color color2 = hide_img[i].color;
			float g = color2.g;
			Color color3 = hide_img[i].color;
			obj.color = new Color(r, g, color3.b, 1f);
		}
		for (int j = 0; j < hide_text.Length; j++)
		{
			Text obj2 = hide_text[j];
			Color color4 = hide_text[j].color;
			float r2 = color4.r;
			Color color5 = hide_text[j].color;
			float g2 = color5.g;
			Color color6 = hide_text[j].color;
			obj2.color = new Color(r2, g2, color6.b, 1f);
		}
		HuoDongWordSearchTutorialInfo[] array = null;
		if (Tutorial == 0)
		{
			array = base.transform.parent.GetComponentsInChildren<HuoDongWordSearchTutorialInfo>();
			for (int k = 0; k < array.Length; k++)
			{
				if (array[k].Obj_name.Equals("DailySearchBtn"))
				{
					array[k].transform.SetParent(Main_tran, worldPositionStays: true);
					jiantou_img.gameObject.SetActive(value: true);
					jiantou_img.transform.SetParent(array[k].transform);
					jiantou_img.anchoredPosition = Vector2.zero;
					temp_trans.Add(array[k]);
				}
			}
		}
		else if (Tutorial == 1)
		{
			array = base.transform.parent.GetComponentsInChildren<HuoDongWordSearchTutorialInfo>();
			for (int l = 0; l < array.Length; l++)
			{
				if (array[l].Obj_name.Contains("Point"))
				{
					array[l].transform.SetParent(Main_tran, worldPositionStays: true);
					jiantou_img.gameObject.SetActive(value: true);
					jiantou_img.transform.SetParent(array[l].transform);
					jiantou_img.anchoredPosition = Vector2.zero;
					temp_trans.Add(array[l]);
				}
			}
		}
		else if (Tutorial == 2)
		{
			base.gameObject.GetComponent<Image>().enabled = false;
			jiantou_img.gameObject.SetActive(value: false);
			shou_rect.gameObject.SetActive(value: true);
			shou_img = shou_rect.GetComponent<Image>();
			Image image = shou_img;
			Color color7 = shou_img.color;
			float r3 = color7.r;
			Color color8 = shou_img.color;
			float g3 = color8.g;
			Color color9 = shou_img.color;
			image.color = new Color(r3, g3, color9.b, 0f);
			anim_idx = 1;
		}
		else if (Tutorial == 3)
		{
			base.gameObject.GetComponent<Image>().enabled = true;
		}
	}

	public void UpdateJianTouAnim()
	{
		if (anim_idx > 0)
		{
			if (anim_idx == 1)
			{
				shou_rect.transform.position = start_pos;
				shou_img.DOFade(1f, 0.5f).OnComplete(delegate
				{
					anim_idx = 2;
				});
			}
			else if (anim_idx == 2)
			{
				shou_rect.DOMove(end_pos, 3f).OnComplete(delegate
				{
					anim_idx = 3;
				});
			}
			else if (anim_idx == 3)
			{
				shou_rect.DOKill();
				shou_img.DOFade(0f, 0.3f).OnComplete(delegate
				{
					anim_idx = 1;
				});
			}
		}
	}

	private void Update()
	{
		if (HideState > 0)
		{
			isHide = true;
			Hide();
			HideState = 0;
		}
		if (Tutorial3Click)
		{
			BtnClick();
			Tutorial3Click = false;
		}
		if (isUpdateTutorial)
		{
			isDPCTutorialShow = true;
			Init();
			isUpdateTutorial = false;
		}
		UpdateJianTouAnim();
	}

	private void Hide()
	{
		for (int i = 0; i < temp_trans.Count; i++)
		{
			string parent_name = temp_trans[i].parent_name;
			if (temp_trans[i].transform.name.Equals("Point_0"))
			{
				HuoDongWordSearchUI.isResetPos = true;
				temp_trans[i].transform.SetParent(temp_trans[i].parent_tran);
				continue;
			}
			GameObject gameObject = GameObject.Find(parent_name);
			if (!(gameObject == null))
			{
				temp_trans[i].transform.SetParent(gameObject.transform);
			}
		}
		for (int j = 0; j < item_canvas.Count; j++)
		{
			item_canvas[j].sortingOrder = 0;
		}
		jiantou_img.transform.SetParent(Main_tran);
		temp_trans.Clear();
		item_canvas.Clear();
		if (isHide)
		{
			isDPCTutorialShow = false;
			Object.Destroy(base.gameObject);
		}
	}

	public void BtnClick()
	{
		if (Tutorial == 3)
		{
			PlayerInfo.Instance.GetWordSearchSaveData().SetTutorial(4);
			isHide = true;
			Hide();
		}
	}
}
