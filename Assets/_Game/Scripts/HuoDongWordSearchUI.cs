using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class HuoDongWordSearchUI : MonoBehaviour
{
	public static bool isUpdateUI;

	public static bool isShowUI;

	public static bool isCloseClick;

	public static bool isResetPos;

	public static bool isShow;

	public static int AddGoldNum;

	public Vector2[] points_pos;

	public Vector2[] points_UpPos;

	public GameObject Point_resGo;

	public GameObject Line_resGo;

	public GameObject Box01_resGo;

	public GameObject Box02_resGo;

	public RectTransform HideRectTran;

	public RectTransform MoveRectTran;

	public RectTransform PointContentTran;

	public Transform top_tran;

	public GoldScript gold_script;

	public RectTransform gold_flyTran;

	private int offset_height = 1082;

	private List<HuoDongWordSearchUIPoint> point_list = new List<HuoDongWordSearchUIPoint>();

	private List<HuoDongWordSearchUIPoint> box_list = new List<HuoDongWordSearchUIPoint>();

	private WordSearchSaveData wssd;

	private bool CheckTutorial;

	private float dpc_tutorial_waitTime;

	private void Start()
	{
		WordGlobal.ResetTopPos(top_tran as RectTransform);
		AudioControl.GetInstance().PlayBG(AudioControl.GetInstance().Music_Main2);
		isUpdateUI = false;
		PlayerInfo.Instance.GetLittleTipsData().SetDailySearchOpen();
		if (LevelMapUIScript.Instance != null && LevelMapUIScript.Instance.GoldItem != null)
		{
			LevelMapUIScript.Instance.GoldItem.gameObject.SetActive(value: false);
		}
		int num = WordGlobal.WordSearchMaxLevel / 10;
		RectTransform pointContentTran = PointContentTran;
		Vector2 sizeDelta = PointContentTran.sizeDelta;
		pointContentTran.sizeDelta = new Vector2(sizeDelta.x, 203 + num * offset_height);
		int num2 = PlayerInfo.Instance.GetWordSearchSaveData().GetCurPlayLevel() / 10;
		RectTransform pointContentTran2 = PointContentTran;
		Vector2 anchoredPosition = PointContentTran.anchoredPosition;
		pointContentTran2.anchoredPosition = new Vector2(anchoredPosition.x, -num2 * offset_height);
		wssd = PlayerInfo.Instance.GetWordSearchSaveData();
		for (int i = 0; i < WordGlobal.WordSearchMaxLevel / 10; i++)
		{
			GameObject gameObject = Object.Instantiate(Line_resGo);
			gameObject.transform.SetParent(PointContentTran);
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localScale = Vector3.one;
			gameObject.transform.name = "Line_" + i;
			Vector2 anchoredPosition2 = new Vector2(0f, i * offset_height);
			(gameObject.transform as RectTransform).anchoredPosition = anchoredPosition2;
			gameObject.SetActive(value: true);
		}
		for (int j = 0; j < wssd.GetMaxChapterNum(); j++)
		{
			GameObject gameObject2 = null;
			if (j % 2 == 0)
			{
				gameObject2 = Object.Instantiate(Box01_resGo);
				gameObject2.transform.SetParent(PointContentTran);
				gameObject2.transform.localPosition = Vector3.zero;
				gameObject2.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
				gameObject2.transform.name = "Box01_" + j;
				Vector2 anchoredPosition3 = new Vector2(-275f, 593 + j / 2 * offset_height);
				(gameObject2.transform as RectTransform).anchoredPosition = anchoredPosition3;
			}
			else
			{
				gameObject2 = Object.Instantiate(Box02_resGo);
				gameObject2.transform.SetParent(PointContentTran);
				gameObject2.transform.localPosition = Vector3.zero;
				gameObject2.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
				gameObject2.transform.name = "Box02_" + j;
				Vector2 anchoredPosition4 = new Vector2(-260f, 1143 + j / 2 * offset_height);
				(gameObject2.transform as RectTransform).anchoredPosition = anchoredPosition4;
			}
			HuoDongWordSearchUIPoint component = gameObject2.GetComponent<HuoDongWordSearchUIPoint>();
			component.Init(j, this, HuoDongWordSearchUIPoint.SEARCHPOINT_TYPE.BOX);
			box_list.Add(component);
		}
		for (int k = 0; k < WordGlobal.WordSearchMaxLevel; k++)
		{
			GameObject gameObject3 = Object.Instantiate(Point_resGo);
			gameObject3.transform.SetParent(PointContentTran);
			gameObject3.transform.localPosition = Vector3.zero;
			gameObject3.transform.localScale = Vector3.one;
			gameObject3.transform.name = "Point_" + k;
			HuoDongWordSearchUIPoint component2 = gameObject3.GetComponent<HuoDongWordSearchUIPoint>();
			component2.Init(k, this, HuoDongWordSearchUIPoint.SEARCHPOINT_TYPE.POINT);
			Vector2 zero = Vector2.zero;
			if (k <= 9)
			{
				zero = points_pos[k];
			}
			else
			{
				int num3 = k % 10;
				int num4 = k / 10;
				float y = points_UpPos[num3].y + (float)((num4 - 1) * offset_height);
				zero = new Vector2(points_UpPos[num3].x, y);
			}
			(component2.transform as RectTransform).anchoredPosition = zero;
			if (k == 0 && PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 1)
			{
				component2.isTutorial = true;
				gameObject3.AddComponent<HuoDongWordSearchTutorialInfo>().setParent(PointContentTran);
				CheckTutorial = true;
			}
			point_list.Add(component2);
		}
		AddGoldNum = 0;
		if (gold_script == null)
		{
			gold_script = WordGlobal.AddGoldUI(top_tran);
			gold_script._isAddGold = true;
			gold_script.m_ShowBanner = true;
			if (gold_script.banner_go != null)
			{
				gold_script.banner_go.GetComponent<Canvas>().sortingOrder = 20;
			}
		}
		for (int l = 0; l < box_list.Count; l++)
		{
			box_list[l].InitBoxGaizi();
		}
		if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 1)
		{
			BtnsCtrlUIScript.isShowPopOffer = false;
			UIManager.Instance.ClearPop();
		}
		isShow = true;
	}

	public void ShowUI()
	{
		AudioControl.GetInstance().PlayBG(AudioControl.GetInstance().Music_Main2);
		int num = PlayerInfo.Instance.GetWordSearchSaveData().GetCurPlayLevel() / 10;
		RectTransform pointContentTran = PointContentTran;
		Vector2 anchoredPosition = PointContentTran.anchoredPosition;
		pointContentTran.anchoredPosition = new Vector2(anchoredPosition.x, -num * offset_height);
		MoveRectTran.DOAnchorPosX(0f, 0.5f);
		HideRectTran.gameObject.SetActive(value: true);
		gold_script._isAddGold = false;
		ResetPos();
		if (gold_script != null && AddGoldNum > 0)
		{
			PlayerInfo.Instance.AddGold(-AddGoldNum, 18);
			gold_script.ResetGold();
			gold_script._isAddGold = true;
			gold_script.CollectGold(gold_flyTran.position, 5, 1f, null);
			gold_script.AddGold(AddGoldNum, 1f, 1f, isAdd2PlayerInfo: true, 19);
			AddGoldNum = 0;
		}
		for (int i = 0; i < box_list.Count; i++)
		{
			box_list[i].InitBoxGaizi();
		}
		if (PlayerInfo.Instance.GetWordSearchSaveData().isAllComplete())
		{
			UIManager.Instance.ShowTip_DesClose(TextLibraryDataManagerCsv.instance.GetText(1278), BtnCloseClick);
		}
	}

	public void ResetPos()
	{
		for (int i = 0; i < points_pos.Length; i++)
		{
			(point_list[i].transform as RectTransform).anchoredPosition = points_pos[i];
		}
	}

	private void Update()
	{
		if (isUpdateUI)
		{
			UpdateUI();
			isUpdateUI = false;
		}
		if (isResetPos)
		{
			ResetPos();
			isResetPos = false;
		}
		if (isShowUI)
		{
			ShowUI();
			isShowUI = false;
		}
		if (isCloseClick)
		{
			if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 >= 4)
			{
				BtnCloseClick();
			}
			isCloseClick = false;
		}
		UpdateCheckTutorial();
	}

	public void UpdateCheckTutorial()
	{
		if (!CheckTutorial)
		{
			return;
		}
		dpc_tutorial_waitTime += Time.deltaTime;
		if (dpc_tutorial_waitTime > 0f)
		{
			if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 1 && GameObject.Find("HuoDongWordSearchTutorialUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchTutorialUI").name = "HuoDongWordSearchTutorialUI";
			}
			CheckTutorial = false;
			dpc_tutorial_waitTime = 0f;
		}
	}

	public void UpdateUI()
	{
		for (int i = 0; i < point_list.Count; i++)
		{
			point_list[i].Init(i, this, HuoDongWordSearchUIPoint.SEARCHPOINT_TYPE.POINT);
		}
		PlayerInfo.Instance.GetWordSearchSaveData().SetBox3();
	}

	public void BtnPlayLevel(float delay = 0f)
	{
		if (GameObject.Find("HuoDongWordSearch") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearch");
		}
		MoveRectTran.DOAnchorPosX(-1000f, 0.5f).SetDelay(delay);
		HideRectTran.gameObject.SetActive(value: false);
	}

	private void Hide()
	{
		if (LevelMapUIScript.Instance != null)
		{
			if (LevelMapUIScript.Instance.GoldItem != null)
			{
				LevelMapUIScript.Instance.GoldItem.gameObject.SetActive(value: true);
			}
			LevelMapUIScript.Instance.CheckDCPTutorial();
			LevelMapUIScript.Instance.GoldItem._isAddGold = false;
			if (PlayerInfo.Instance.GetWordSearchSaveData().GetCurPlayLevel() <= 0)
			{
				LevelMapUIScript.Instance.DailySearchNewIcon.SetActive(value: true);
			}
			else
			{
				LevelMapUIScript.Instance.DailySearchNewIcon.SetActive(value: false);
			}
		}
		isShow = false;
		Object.Destroy(base.gameObject);
	}

	public void BtnCloseClick()
	{
		if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 1)
		{
			HuoDongWordSearchTutorialUI.HideState = 1;
			PlayerInfo.Instance.GetWordSearchSaveData().SetTutorial(2);
		}
		Hide();
	}

	public void BtnSetClick()
	{
		if (GameObject.Find("SettingUI") == null)
		{
			SettingUIScript.BackType = 2;
			UIManager.Instance.LoadMainUI("UI/SettingUI", isPop: true);
		}
	}
}
