using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordSearchUIPoint : MonoBehaviour
{
	public enum SEARCHPOINT_TYPE
	{
		POINT,
		BOX
	}

	public Sprite[] bg_sps;

	public Image bg_img;

	public SEARCHPOINT_TYPE Type;

	public Text pointNumText;

	public int idx;

	private HuoDongWordSearchUI searchUI;

	public int state;

	public GameObject eff_go;

	public bool isTutorial;

	public RectTransform box_gaizi;

	private void Start()
	{
	}

	public void Init(int idx, HuoDongWordSearchUI searchUI, SEARCHPOINT_TYPE type)
	{
		this.idx = idx;
		Type = type;
		this.searchUI = searchUI;
		if (pointNumText != null)
		{
			pointNumText.text = (idx + 1).ToString();
			int curPlayLevel = PlayerInfo.Instance.GetWordSearchSaveData().GetCurPlayLevel();
			eff_go.SetActive(value: false);
			if (curPlayLevel == idx)
			{
				bg_img.sprite = bg_sps[1];
				state = 1;
				eff_go.SetActive(value: true);
			}
			else if (curPlayLevel > idx)
			{
				bg_img.sprite = bg_sps[0];
				state = 2;
			}
			else
			{
				bg_img.sprite = bg_sps[2];
				state = 0;
			}
		}
		base.gameObject.SetActive(value: true);
	}

	public void InitBoxGaizi()
	{
		int num = PlayerInfo.Instance.GetWordSearchSaveData().GetBoxArr[idx];
		if (num == 3)
		{
			box_gaizi.anchoredPosition = new Vector2(150f, -13f);
			RectTransform rectTransform = box_gaizi;
			Vector3 localEulerAngles = box_gaizi.localEulerAngles;
			float x = localEulerAngles.x;
			Vector3 localEulerAngles2 = box_gaizi.localEulerAngles;
			rectTransform.localEulerAngles = new Vector3(x, localEulerAngles2.y, -50f);
		}
	}

	public void OnClick()
	{
		if (isTutorial)
		{
			if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 1)
			{
				HuoDongWordSearchTutorialUI.HideState = 1;
				PlayerInfo.Instance.GetWordSearchSaveData().SetTutorial(2);
			}
			if (state == 1)
			{
				searchUI.BtnPlayLevel(0.1f);
			}
		}
		else if (state == 1)
		{
			searchUI.BtnPlayLevel();
		}
	}
}
