using UnityEngine;
using UnityEngine.UI;

public class HuoDongWordSearchWord : MonoBehaviour
{
	public Text txt;

	public int State;

	public string word;

	private HuoDongWordSearch hdws;

	private void Start()
	{
	}

	public void Init(string str, HuoDongWordSearch hdws)
	{
		this.hdws = hdws;
		if (txt == null)
		{
			txt = base.gameObject.GetComponent<Text>();
		}
		word = str;
		txt.text = str;
		txt.color = Color.white;
	}

	public void Check(string word)
	{
		if (this.word.Equals(word))
		{
			txt.color = Color.black;
		}
	}

	public void Click()
	{
		hdws.HelpWord();
	}
}
