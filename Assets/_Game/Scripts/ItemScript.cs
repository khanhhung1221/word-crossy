using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemScript : MonoBehaviour
{
	public Sprite m_ItemLight;

	public Sprite m_ItemLetter;

	public Sprite m_ItemSpace;

	public GameObject m_ObjImage;

	public GameObject m_ObjLight;

	public GameObject m_ObjBGImg;

	public Text m_TxtLetter;

	public Animator m_ObjAni;

	public ButterFlyItem m_ItemButter;

	public bool m_IsMoveToStar;

	private float _width = 78f;

	public int _imgState;

	private float _butterMoveTimeL = 1f;

	private float _butterMoveTimeS = 0.3f;

	private bool _isButterMoveToNextItem;

	private List<CrossWordItem> my_words = new List<CrossWordItem>();

	private int lettertotalCount = -1;

	private int levelCompleteNum = -1;

	private float waitTime;

	public bool isState2Complete;

	private int curRow = -1;

	private int curCol = -1;

	private RectTransform rectTran;

	public Vector3 m_Scale
	{
		get
		{
			Vector3 one = Vector3.one;
			Vector2 sizeDelta = base.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta;
			return one * sizeDelta.x / _width;
		}
	}

	private void Start()
	{
	}

	public List<CrossWordItem> GetMyWords()
	{
		if (my_words.Count == 0)
		{
			my_words = MainUIScript.Instance.GetWords(getRow() + 1, getCol() + 1);
		}
		return my_words;
	}

	public int isMyWordComplete()
	{
		List<CrossWordItem> myWords = GetMyWords();
		int num = 0;
		if (PlayerInfo.Instance.MaxPlayLevel == 1)
		{
			num = 1;
		}
		for (int i = 0; i < myWords.Count; i++)
		{
			if (MainUIScript.Instance.CheckWordCompleteByCrossWord(myWords[i]))
			{
				num++;
			}
		}
		return num;
	}

	private void LateUpdate()
	{
		if (_imgState == 2 && !isState2Complete && MainUIScript.Instance != null && lettertotalCount != MainUIScript.Instance.LetterState2Count)
		{
			if (levelCompleteNum != MainUIScript.Instance._currentWordList.Count)
			{
				SetImageState(2, isAnim: false);
				levelCompleteNum = MainUIScript.Instance._currentWordList.Count;
			}
			lettertotalCount = MainUIScript.Instance.LetterState2Count;
		}
	}

	public void SetLetter(string letter)
	{
		if (!m_ObjImage.activeSelf)
		{
			m_ObjImage.SetActive(value: true);
		}
		m_TxtLetter.text = letter;
		if (isMyWordComplete() > 0)
		{
			if (MainUIScript.Instance != null)
			{
				m_TxtLetter.color = MainUIScript.Instance.GetCurrentTextColor();
			}
		}
		else
		{
			m_TxtLetter.color = Color.black;
		}
		if (MainUIScript.Instance.huodong_main != null)
		{
			MainUIScript.Instance.huodong_main.Check();
		}
		if (MainUIScript.Instance.huodong_sun_main != null)
		{
			MainUIScript.Instance.huodong_sun_main.Check();
		}
	}

	public void SetImgSize(float x, float y)
	{
		RectTransform component = base.transform.GetChild(0).GetComponent<RectTransform>();
		Vector2 sizeDelta = component.sizeDelta;
		float num = x / sizeDelta.x;
		SetButterScale(num);
		HuoDongMainUI.wordItemSize = num;
		component.sizeDelta = new Vector2(x, y);
	}

	public void SetImageState(int index, bool isAnim = true)
	{
		if (_imgState != index && index == 2 && MainUIScript.Instance != null)
		{
			MainUIScript.Instance.LetterState2Count++;
		}
		_imgState = index;
		Image component = m_ObjImage.GetComponent<Image>();
		component.color = new Color(1f, 1f, 1f, 1f);
		if (MainUIScript.Instance != null)
		{
			component.color = MainUIScript.Instance.GetCurrentDishTypeColor();
		}
		m_ObjLight.SetActive(value: false);
		switch (index)
		{
		case 0:
			component.sprite = null;
			component.color = new Color(0f, 0f, 0f, 0f);
			break;
		case 1:
			component.sprite = m_ItemSpace;
			break;
		case 2:
			if (isMyWordComplete() > 0)
			{
				isState2Complete = true;
				if (MainUIScript.Instance != null)
				{
					m_TxtLetter.color = MainUIScript.Instance.GetCurrentTextColor();
				}
				if (!component.sprite.Equals(m_ItemLetter))
				{
					component.sprite = m_ItemLetter;
					Color color = component.color;
					if (MainUIScript.Instance != null)
					{
						color = MainUIScript.Instance.GetCurrentLineColor();
					}
					if (isAnim)
					{
						component.color = new Color(color.r, color.g, color.b, 0f);
						component.DOFade(1f, 0.1f);
					}
					else
					{
						component.color = color;
					}
				}
				else
				{
					Color color2 = component.color;
					if (MainUIScript.Instance != null)
					{
						color2 = MainUIScript.Instance.GetCurrentLineColor();
					}
					component.color = color2;
				}
			}
			else
			{
				isState2Complete = false;
				m_TxtLetter.color = Color.black;
			}
			break;
		case 3:
			component.sprite = m_ItemSpace;
			m_ObjLight.SetActive(value: true);
			break;
		}
	}

	public void ItemClick()
	{
		if (_imgState == 3)
		{
			MainUIScript.Instance.BtnItemClick(base.gameObject);
			DestroyEffect("acg_blank");
		}
		else if (_imgState == 2)
		{
			MainUIScript.Instance.CheckWordShowDictionaryUI(base.gameObject.name);
		}
	}

	public void FinishNullLetterAnim(string letter, int index)
	{
		m_ObjImage.SetActive(value: true);
		Text txtLetter = m_TxtLetter;
		Color color = m_TxtLetter.color;
		float r = color.r;
		Color color2 = m_TxtLetter.color;
		float g = color2.g;
		Color color3 = m_TxtLetter.color;
		txtLetter.color = new Color(r, g, color3.b, 0f);
		m_TxtLetter.text = letter;
		Vector3 endValue = new Vector3(1.1f, 1.1f, 1.1f);
		float delay = (float)index * 0.1f;
		m_ObjImage.transform.DOScale(endValue, 0.2f).SetDelay(delay).OnComplete(delegate
		{
			m_ObjImage.transform.DOScale(Vector3.one, 0.2f);
		});
		m_TxtLetter.DOFade(1f, 0.4f).SetDelay(delay);
	}

	public void SetItemScale(float width)
	{
		SetImgSize(width, width);
	}

	public void LoadLastWordEffect()
	{
		GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI("FX/acg_TxtQuabFX01", base.transform);
		gameObject.transform.localScale = m_Scale;
		gameObject.name = "acg_TxtQuabFX01";
	}

	public void LoadEffect(string name)
	{
		GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI("FX/" + name, base.transform);
		WordGlobal.ScaleObjAndChild(gameObject.transform, m_Scale);
		gameObject.name = name;
	}

	public void DestroyEffect(string name)
	{
		if ((bool)base.transform.Find(name))
		{
			Object.Destroy(base.transform.Find(name).gameObject);
		}
	}

	public void TryWordFailed()
	{
		m_ObjAni.SetTrigger("Sharp");
	}

	public int getRow()
	{
		if (curRow < 0)
		{
			string[] array = base.gameObject.name.Split('-');
			if (array.Length == 3)
			{
				curRow = int.Parse(array[1]);
				curCol = int.Parse(array[2]);
			}
			else
			{
				Debug.Log(string.Empty + base.gameObject.name);
			}
		}
		return curRow;
	}

	public int getCol()
	{
		if (curCol < 0)
		{
			string[] array = base.gameObject.name.Split('-');
			if (array.Length == 3)
			{
				curRow = int.Parse(array[1]);
				curCol = int.Parse(array[2]);
			}
		}
		return curCol;
	}

	public void AddLetterSuccess(Transform lettersRootTran, string letter, int idx, string word)
	{
		GameObject obj = Object.Instantiate(m_TxtLetter.gameObject);
		obj.transform.SetParent(MainUIScript.Instance.selectLetterAnim_rect, worldPositionStays: false);
		RectTransform component = obj.GetComponent<RectTransform>();
		float num = 45f;
		float num2 = 0f;
		num2 = ((word.Length % 2 != 0) ? ((float)(-word.Length) * num / 2f + num * (float)idx + num / 2f) : ((float)(-word.Length) * num / 2f + num * (float)idx));
		component.anchoredPosition = new Vector2(num2, 0f);
		component.anchorMax = new Vector2(0.5f, 0.5f);
		component.anchorMin = new Vector2(0.5f, 0.5f);
		component.sizeDelta = GetComponent<RectTransform>().sizeDelta;
		Text text = obj.GetComponent<Text>();
		text.text = letter;
		text.color = MainUIScript.Instance.GetCurrentTextColor();
		text.resizeTextForBestFit = false;
		Text text2 = text;
		Vector3 localScale = lettersRootTran.localScale;
		text2.fontSize = (int)(localScale.x * 102f);
		text.rectTransform.sizeDelta = new Vector2(200f, 200f);
		string[] array = base.gameObject.name.Split('-');
		int row = int.Parse(array[1]) + 1;
		int col = int.Parse(array[2]) + 1;
		text.enabled = false;
		text.DOFade(1f, 0.01f).SetDelay(0.3f + 0.02f * (float)idx).OnComplete(delegate
		{
			text.enabled = true;
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
			{
				MainUIScript.Instance.JudgeButterChange(row, col);
			}
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
			{
				MainUIScript.Instance.HuoDongSunItemCheck(row, col);
			}
			if (obj != null)
			{
				obj.transform.DOScale(1.5f, 0.15f).SetDelay(0.02f * (float)idx).OnComplete(delegate
				{
					obj.transform.DOScale(0.6f, 0.15f);
				});
				obj.transform.DOMove(base.transform.position, 0.3f).SetDelay(0.02f * (float)idx).OnComplete(delegate
				{
					SetLetter(letter);
					MainUIScript.Instance.SetCellState(base.gameObject.name, letter);
					if (obj != null)
					{
						Object.Destroy(obj);
					}
				});
			}
		});
	}

	public void LetterInAni(int state)
	{
		switch (state)
		{
		case 1:
			m_ObjAni.SetTrigger("LetterIN");
			break;
		case 2:
			m_ObjAni.SetTrigger("Victory");
			break;
		case 3:
			m_ObjAni.SetTrigger("Sharp");
			break;
		case 4:
			m_ObjAni.SetTrigger("LoopSharp");
			break;
		}
	}

	public void SetHidden(bool hidden)
	{
		m_ObjImage.SetActive(!hidden);
	}

	public void ShowButterFly(Color color, bool isDelay)
	{
		if (!(m_ItemButter == null))
		{
			m_ItemButter.ShowButterFly(color);
			if (!isDelay)
			{
				ShowButterObj();
			}
		}
	}

	public void ShowButterObj()
	{
		if (!(m_ItemButter == null))
		{
			m_ItemButter.gameObject.SetActive(value: true);
		}
	}

	public void HideButterFly()
	{
		if (!(m_ItemButter == null))
		{
			m_ItemButter.gameObject.SetActive(value: false);
			_isButterMoveToNextItem = false;
			m_IsMoveToStar = false;
		}
	}

	public void ButterFlyMove(Vector3 endPos, bool isToNextItem)
	{
		if (!(m_ItemButter == null))
		{
			m_ItemButter.transform.DOKill();
			m_ItemButter.ChangeButterState(2);
			_isButterMoveToNextItem = isToNextItem;
			float duration = (!isToNextItem) ? _butterMoveTimeL : _butterMoveTimeS;
			if (isToNextItem)
			{
				m_ItemButter.transform.DOMove(endPos, duration).OnComplete(delegate
				{
					MainUIScript.Instance.ShowCurButterFly();
					HideButterFly();
				});
				return;
			}
			m_IsMoveToStar = true;
			Vector3[] path = WordGlobal.CountCirclePoints(m_ItemButter.transform.position, endPos);
			m_ItemButter.transform.DOPath(path, duration).OnComplete(delegate
			{
				HideButterFly();
				if (!_isButterMoveToNextItem)
				{
					MainUIScript.Instance.AddButterFlyNum();
				}
			});
		}
	}

	public void SetButterScale(float s)
	{
		if (m_ItemButter != null)
		{
			m_ItemButter.transform.localScale = Vector3.one;
			m_ItemButter.SetButterFlyScale(s);
		}
	}

	public RectTransform GetRectTran()
	{
		if (rectTran == null)
		{
			rectTran = (base.transform as RectTransform);
		}
		return rectTran;
	}
}
