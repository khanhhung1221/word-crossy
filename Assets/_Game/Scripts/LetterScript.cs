using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LetterScript : MonoBehaviour, IPointerUpHandler, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler, IEventSystemHandler
{
	public Image bg_img;

	public GameObject m_DrawLine;

	public Animator m_Ani;

	private CatmullDraw _catmullDraw;

	private Vector3 _endValue;

	private float _aniTime = 0.5f;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void RemovePoint()
	{
		bg_img.color = new Color(0f, 0f, 0f, 0f);
	}

	public void AddPoint()
	{
		bg_img.color = MainUIScript.Instance._StyleColor;
		MainUIScript.Instance.AddLetter(base.gameObject);
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
	}

	public void OnPointerClick(PointerEventData eventData)
	{
	}

	public void LetterRandomAni(Vector3 endValue)
	{
		_endValue = endValue;
		base.transform.DOLocalMove(base.transform.parent.localPosition, _aniTime).OnComplete(AniCompleted);
	}

	private void AniCompleted()
	{
		base.transform.DOLocalMove(_endValue, _aniTime);
	}

	public void OnPointerDown(PointerEventData eventData)
	{
	}

	public void SetAni(int state)
	{
		switch (state)
		{
		case 0:
			m_Ani.SetTrigger("Failed");
			break;
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
	}

	public void OnPointerUp(PointerEventData eventData)
	{
	}
}
