using LitJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class LevToolsUIScript : MonoBehaviour
{
	public GameObject m_ObjItem;

	public Transform m_TfItemRoot;

	public InputField m_TxtLevel;

	public InputField m_TxtMinLevel;

	public InputField m_TxtMaxLevel;

	public Text m_TxtLevelInfo;

	public Toggle m_Toggle;

	public Toggle m_TogCurrentLvl;

	public Toggle m_ToggleDaily;

	private int _level = 1;

	private WordJsonList _wordJsonList;

	public List<List<string>> _grid;

	private string _filePath = "/_Game/Resources/Data/";

	private bool isDaily;

	private List<object> CheckWordIsMeanall_words = new List<object>();

	private void Start()
	{
	}

	private void Awake()
	{
		InitItem();
	}

	private void Update()
	{
	}

	private void InitItem()
	{
		isDaily = m_ToggleDaily.isOn;
		if (isDaily)
		{
			if (File.Exists(Application.dataPath + _filePath + "dailypuzzlelevel.json"))
			{
				_wordJsonList = JsonUtility.FromJson<WordJsonList>(File.ReadAllText(Application.dataPath + _filePath + "dailypuzzlelevel.json"));
			}
			else
			{
				_wordJsonList = new WordJsonList();
			}
		}
		else
		{
			_wordJsonList = JsonUtility.FromJson<WordJsonList>(File.ReadAllText(Application.dataPath + _filePath + "words.json"));
			_wordJsonList.Words.Sort((WordListItem x, WordListItem y) => x.Level.CompareTo(y.Level));
		}
	}

	public void CheckAllWord()
	{
		foreach (int key in WordDataManager.instance.ItemList.Keys)
		{
			CheckItem(WordDataManager.instance.ItemList[key]);
		}
		DailyPuzzleDataManager dailyPuzzleDataManager = new DailyPuzzleDataManager(abc: true);
		foreach (int key2 in dailyPuzzleDataManager.ItemList.Keys)
		{
			CheckItem(dailyPuzzleDataManager.ItemList[key2]);
		}
		Debug.Log("检查结束");
	}

	public void CheckItem(WordListItem item)
	{
		try
		{
			for (int i = 0; i < item.Bonus.Count; i++)
			{
				if (item.Words.Contains(item.Bonus[i]))
				{
					Debug.LogError("Level:" + item.Level + ";bonus:" + item.Bonus[i] + "在单词列表中出现");
				}
			}
		}
		catch (Exception)
		{
			Debug.LogError("Exception Level:" + item.Level);
			throw;
		}
	}

	public void CalWords(int min, int max, float time = 2f)
	{
		InitItem();
		Crossword crossword = new Crossword();
		crossword._MinRCDiff = m_Toggle.isOn;
		List<string> list = new List<string>();
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		for (int i = min; i <= max; i++)
		{
			WordListItem item = new WordListItem();
			if (isDaily)
			{
				item = DailyPuzzleDataManager.instance.GetItemByLevel(i);
				if (item.Level == 0)
				{
					continue;
				}
			}
			else
			{
				item = WordDataManager.instance.GetItemByLevel(i);
			}
			List<string> list2 = new List<string>();
			item.DecryptTxt();
			if (item.Words.Count == 0)
			{
				for (int j = 0; j < item.WordList.Count; j++)
				{
					item.Words.Add(item.WordList[j].Word);
				}
			}
			string text = string.Empty;
			for (int k = 0; k < item.Words.Count; k++)
			{
				if (string.IsNullOrEmpty(item.Words[k]))
				{
					text += "有多余逗号";
				}
				if (item.Words[k].Length > 7)
				{
					text += "单词字母个数超过7个";
				}
			}
			bool flag = false;
			if (string.IsNullOrEmpty(text))
			{
				flag = crossword.ComputeCrossWord(item.Words, time);
			}
			if (!flag)
			{
				Debug.LogError("Level:" + item.Level + ",计算失败！！！！！！！！！！！" + text);
				continue;
			}
			if (item.MoneyWord.Length > 0 && crossword._currentCrossWordList.FindIndex((CrossWordItem xx) => xx.Word == item.MoneyWord) < 0)
			{
				int num = 0;
				while (!crossword.FitMoneyWord(item.MoneyWord))
				{
					switch (num)
					{
					case 0:
						crossword.AddGridRC(isRow: true, 1, isStart: true);
						break;
					case 1:
						crossword.AddGridRC(isRow: false, 1, isStart: false);
						break;
					case 2:
						crossword.AddGridRC(isRow: false, 1, isStart: true);
						break;
					case 3:
						crossword.AddGridRC(isRow: true, 1, isStart: false);
						num = 0;
						break;
					}
					num++;
				}
				crossword._currentCrossWordList.Add(crossword._fitMoneyPosList[0]);
				crossword.RemoveEmptyGrid();
			}
			CheckItem(item);
			item.WordList = crossword._currentCrossWordList;
			item.MaxRow = crossword._grid.Count;
			item.MaxCol = crossword._grid[0].Count;
			if (item.MaxRow >= 11 || item.MaxCol >= 11)
			{
				string item2 = "ROWCOL id:" + i + " row:" + item.MaxRow + " col:" + item.MaxCol;
				list.Add(item2);
			}
			List<string> list3 = new List<string>();
			for (int l = 0; l < item.Words.Count; l++)
			{
				string text2 = item.Words[l];
				for (int m = 0; m < text2.Length; m++)
				{
					string str = text2.Substring(m, 1);
					if (list3.FindAll((string ll) => ll == str).Count < Regex.Matches(text2, str).Count)
					{
						string item3 = str;
						list3.Add(item3);
					}
				}
			}
			if (list3.Count > 7)
			{
				string text3 = string.Empty;
				for (int n = 0; n < list3.Count; n++)
				{
					text3 = text3 + list3[n] + ",";
				}
				string item4 = "LETTER_COUNT id:" + i + " Letters.Count:" + list3.Count + " letters:" + text3;
				list.Add(item4);
			}
			item.EncryptTxt();
			int num2 = _wordJsonList.Words.FindIndex((WordListItem x) => x.Level == item.Level);
			if (num2 >= 0)
			{
				_wordJsonList.Words[num2] = item;
			}
			else
			{
				_wordJsonList.Words.Add(item);
			}
		}
		string str2 = Application.dataPath + _filePath;
		str2 = ((!isDaily) ? (str2 + "words.json") : (str2 + "dailypuzzlelevel.json"));
		if (File.Exists(str2))
		{
			File.Delete(str2);
		}
		Debug.Log(Application.dataPath);
		File.WriteAllText(str2, JsonUtility.ToJson(_wordJsonList));
		float num3 = Time.realtimeSinceStartup - realtimeSinceStartup;
		string text4 = "计算完成！！！！耗时：" + num3 + "ms\n";
		if (list.Count > 0)
		{
			text4 += "不符合的关卡，需重新检查！！\n";
			for (int num4 = 0; num4 < list.Count; num4++)
			{
				text4 = text4 + list[num4] + "\n";
			}
		}
		m_TxtLevelInfo.text = text4;
	}

	public void CheckWordIsMean(int min, int max)
	{
		CheckWordIsMeanall_words.Clear();
		for (int i = min; i <= max; i++)
		{
			WordListItem wordListItem = new WordListItem();
			if (isDaily)
			{
				wordListItem = DailyPuzzleDataManager.instance.GetItemByLevel(i);
				if (wordListItem.Level == 0)
				{
					continue;
				}
			}
			else
			{
				wordListItem = WordDataManager.instance.GetItemByLevel(i);
			}
			List<string> list = new List<string>();
			wordListItem.DecryptTxt();
			if (wordListItem.Words.Count == 0)
			{
				for (int j = 0; j < wordListItem.WordList.Count; j++)
				{
					wordListItem.Words.Add(wordListItem.WordList[j].Word);
				}
			}
			for (int k = 0; k < wordListItem.Words.Count; k++)
			{
				if (!string.IsNullOrEmpty(wordListItem.Words[k]))
				{
					CheckWordIsMeanall_words.Add(wordListItem.Words[k]);
				}
			}
		}
		WordFaceBook.Instance.CheckWordIsReal(CheckWordIsMeanall_words, CheckWordIsRealCallBack);
	}

	public bool CheckWordIsRealCallBack(JsonData data)
	{
		if (data == null)
		{
			return false;
		}
		JsonData jsonData = data["wordlist"];
		for (int i = 0; i < jsonData.Count; i++)
		{
			JsonData jsonData2 = jsonData[i];
			if (int.Parse(jsonData2.ToJson()) == 0)
			{
				Debug.LogError("没有意义的单词:" + CheckWordIsMeanall_words[i]);
			}
		}
		Debug.Log("检查单词意义结束");
		return true;
	}

	public void BtnCheckWordIsMean()
	{
		int num = int.Parse(m_TxtMinLevel.text);
		int max = int.Parse(m_TxtMaxLevel.text);
		if (num < 1)
		{
			num = 1;
		}
		CheckWordIsMean(num, max);
	}

	public void BtnHVSetClick()
	{
		int num = int.Parse(m_TxtMinLevel.text);
		int max = int.Parse(m_TxtMaxLevel.text);
		if (num < 1)
		{
			num = 1;
		}
		CalWords(num, max, 4f);
	}

	public void InitWordList()
	{
		InitItem();
		m_TxtLevel.text = _level.ToString();
		Crossword crossword = new Crossword();
		WordListItem wordListItem = _wordJsonList.Words.Find((WordListItem xx) => xx.Level == _level);
		if (wordListItem == null)
		{
			return;
		}
		wordListItem.DecryptTxt();
		_grid = crossword.InitGrid(wordListItem.MaxRow, wordListItem.MaxCol);
		for (int i = 0; i < wordListItem.WordList.Count; i++)
		{
			SetWord(wordListItem.WordList[i]);
		}
		crossword._grid = _grid;
		float num = 65f;
		float y = 65f;
		if (wordListItem.MaxCol * 75 + 20 > 720)
		{
			if (wordListItem.MaxCol < wordListItem.MaxRow)
			{
				num = (y = 700 / wordListItem.MaxRow - 5);
				m_TfItemRoot.GetComponent<RectTransform>().sizeDelta = new Vector2((float)wordListItem.MaxCol * (num + 5f) + 20f, 720f);
			}
			else
			{
				num = (y = 700 / wordListItem.MaxCol - 5);
				m_TfItemRoot.GetComponent<RectTransform>().sizeDelta = new Vector2(720f, 720f);
			}
		}
		else
		{
			m_TfItemRoot.GetComponent<RectTransform>().sizeDelta = new Vector2((float)wordListItem.MaxCol * (num + 5f) + 20f, 720f);
		}
		m_TfItemRoot.GetComponent<GridLayoutGroup>().cellSize = new Vector2(num, y);
		WordGlobal.ClearChilds(m_TfItemRoot);
		for (int j = 0; j < _grid.Count; j++)
		{
			for (int k = 0; k < _grid[j].Count; k++)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate(m_ObjItem);
				if (_grid[j][k] != "-")
				{
					gameObject.transform.GetChild(0).GetComponent<Text>().text = _grid[j][k];
				}
				else
				{
					gameObject.transform.GetComponent<Image>().enabled = false;
				}
				gameObject.transform.SetParent(m_TfItemRoot, worldPositionStays: false);
				gameObject.SetActive(value: true);
			}
		}
		string str = "Level:" + wordListItem.Level + "\n";
		str += "words：\n";
		for (int l = 0; l < wordListItem.Words.Count; l++)
		{
			str = str + wordListItem.Words[l] + ",";
		}
		str += "\n";
		str = str + "moneyword:" + wordListItem.MoneyWord + "\n";
		str += "bonus:";
		string text = string.Empty;
		for (int m = 0; m < wordListItem.Bonus.Count; m++)
		{
			text = text + wordListItem.Bonus[m] + ",";
		}
		str += text;
		str += "\n Letters:";
		List<string> list = new List<string>();
		for (int n = 0; n < wordListItem.Words.Count; n++)
		{
			string text2 = wordListItem.Words[n];
			for (int num2 = 0; num2 < text2.Length; num2++)
			{
				string SS = text2.Substring(num2, 1);
				if (list.FindAll((string ll) => ll == SS).Count < Regex.Matches(text2, SS).Count)
				{
					string item = SS;
					list.Add(item);
				}
			}
		}
		string text3 = string.Empty;
		for (int num3 = 0; num3 < list.Count; num3++)
		{
			text3 = text3 + list[num3] + ",";
		}
		str += text3;
		str += "\n RowCol:";
		string text4 = str;
		str = text4 + "row:" + _grid[0].Count + " col:" + _grid.Count;
		m_TxtLevelInfo.text = str;
	}

	public void ReEncryptJson()
	{
	}

	private void EditBonus(int lvl)
	{
		int num = _wordJsonList.Words.FindIndex((WordListItem x) => x.Level == lvl);
		if (num < 0)
		{
			return;
		}
		WordListItem wordListItem = _wordJsonList.Words[num];
		try
		{
			wordListItem.DecryptTxt();
		}
		catch
		{
		}
		WordListItem itemcsv = new WordListItem();
		if (isDaily)
		{
			itemcsv = DailyPuzzleDataManager.instance.GetItemByLevel(lvl);
		}
		else
		{
			itemcsv = WordDataManager.instance.GetItemByLevel(lvl);
		}
		wordListItem.Bonus.Clear();
		int x2;
		for (x2 = 0; x2 < itemcsv.Bonus.Count; x2++)
		{
			if (!(itemcsv.Bonus[x2] == string.Empty))
			{
				if (wordListItem.WordList.FindIndex((CrossWordItem b) => b.Word == itemcsv.Bonus[x2]) >= 0)
				{
					Debug.LogError("Level:" + wordListItem.Level + ";bonus:" + itemcsv.Bonus[x2] + "在单词列表中出现");
				}
				else if (!string.IsNullOrEmpty(itemcsv.Bonus[x2]))
				{
					wordListItem.Bonus.Add(itemcsv.Bonus[x2]);
				}
			}
		}
		wordListItem.EncryptTxt();
		_wordJsonList.Words[num] = wordListItem;
	}

	public void BtnEditBonus()
	{
		InitItem();
		if (m_TogCurrentLvl.isOn)
		{
			EditBonus(_level);
		}
		else
		{
			int num = int.Parse(m_TxtMinLevel.text);
			int num2 = int.Parse(m_TxtMaxLevel.text);
			if (num < 1)
			{
				num = 1;
			}
			if (!isDaily)
			{
			}
			for (int i = num; i <= num2; i++)
			{
				EditBonus(i);
			}
		}
		string str = Application.dataPath + _filePath;
		str = ((!isDaily) ? (str + "words.json") : (str + "dailypuzzlelevel.json"));
		if (File.Exists(str))
		{
			File.Delete(str);
		}
		Debug.Log(Application.dataPath);
		File.WriteAllText(str, JsonUtility.ToJson(_wordJsonList));
		InitItem();
		m_TxtLevelInfo.text = "bonus 修改完成！！！！";
	}

	public void CheckBonusError()
	{
		int i;
		for (i = 1; i <= _wordJsonList.Words.Count; i++)
		{
			WordListItem item = _wordJsonList.Words.Find((WordListItem xx) => xx.Level == i);
			if (item == null)
			{
				return;
			}
			item.DecryptTxt();
			int x;
			for (x = 0; x < item.Bonus.Count; x++)
			{
				if (item.WordList.FindIndex((CrossWordItem b) => b.Word == item.Bonus[x]) >= 0)
				{
					Debug.LogError("Level:" + item.Level + ";bonus:" + item.Bonus[x] + "在单词列表中出现");
				}
			}
		}
		Debug.LogError("检查完毕");
	}

	private void SetWord(CrossWordItem word)
	{
		int num = word.Col;
		int num2 = word.Row;
		for (int i = 0; i < word.Word.Length; i++)
		{
			SetCell(num, num2, word.Word.Substring(i, 1));
			if (word.vertical)
			{
				num2++;
			}
			else
			{
				num++;
			}
		}
	}

	public void IsDailyClick()
	{
		isDaily = m_ToggleDaily.isOn;
	}

	private void SetCell(int col, int row, string str)
	{
		_grid[row - 1][col - 1] = str;
	}

	public void BtnNextLevelClick()
	{
		if (isDaily)
		{
			_level = int.Parse(new DateTime(int.Parse("20" + _level.ToString().Substring(0, 2)), int.Parse(_level.ToString().Substring(2, 2)), int.Parse(_level.ToString().Substring(4, 2))).AddDays(1.0).ToString("yyMMdd"));
		}
		else
		{
			_level++;
		}
		InitWordList();
	}

	public void BtnPreLevelClick()
	{
		if (isDaily)
		{
			_level = int.Parse(new DateTime(int.Parse("20" + _level.ToString().Substring(0, 2)), int.Parse(_level.ToString().Substring(2, 2)), int.Parse(_level.ToString().Substring(4, 2))).AddDays(-1.0).ToString("yyMMdd"));
		}
		else
		{
			_level--;
			if (_level < 1)
			{
				_level = 1;
			}
		}
		InitWordList();
	}

	public void ChangeLevel(string txt)
	{
		_level = int.Parse(txt);
		InitWordList();
	}

	public void ExchangeRowCol()
	{
		RowColExchangeList();
		InitWordList();
	}

	public void BtnShowLevel()
	{
		_level = int.Parse(m_TxtLevel.text);
		InitWordList();
	}

	public void BtnReCalClick()
	{
		InitItem();
		_level = int.Parse(m_TxtLevel.text);
		CalWords(_level, _level, 10f);
		InitWordList();
	}

	public void RowColExchangeList()
	{
		WordListItem wordListItem = _wordJsonList.Words.Find((WordListItem xx) => xx.Level == _level);
		for (int i = 0; i < _wordJsonList.Words[_level - 1].WordList.Count; i++)
		{
			bool vertical = !_wordJsonList.Words[_level - 1].WordList[i].vertical;
			_wordJsonList.Words[_level - 1].WordList[i].vertical = vertical;
			int row = _wordJsonList.Words[_level - 1].WordList[i].Row;
			int num = _wordJsonList.Words[_level - 1].MaxCol + 1 - _wordJsonList.Words[_level - 1].WordList[i].Col;
			if (_wordJsonList.Words[_level - 1].WordList[i].vertical)
			{
				_wordJsonList.Words[_level - 1].WordList[i].Row = num - _wordJsonList.Words[_level - 1].WordList[i].Word.Length + 1;
			}
			else
			{
				_wordJsonList.Words[_level - 1].WordList[i].Row = num;
			}
			_wordJsonList.Words[_level - 1].WordList[i].Col = row;
		}
		int maxCol = wordListItem.MaxCol;
		int num2 = wordListItem.MaxCol = wordListItem.MaxRow;
		wordListItem.MaxRow = maxCol;
	}

	public void MinRCDiffClick(bool box)
	{
	}

	public void ReCal()
	{
		int i;
		for (i = 0; i < 9999; i++)
		{
			Crossword crossword = new Crossword();
			WordListItem item = _wordJsonList.Words.Find((WordListItem xx) => xx.Level == i);
			if (item != null && item.Words.Count != 0)
			{
				int num = _wordJsonList.Words.FindIndex((WordListItem x) => x.Level == item.Level);
				try
				{
					item.DecryptTxt();
				}
				catch (Exception)
				{
				}
				item.EncryptTxt();
				if (num >= 0)
				{
					_wordJsonList.Words[num] = item;
				}
				else
				{
					_wordJsonList.Words.Add(item);
				}
			}
		}
		if (File.Exists(Application.dataPath + _filePath + "words.json"))
		{
			File.Delete(Application.dataPath + _filePath + "words.json");
		}
		Debug.Log(Application.dataPath);
		File.WriteAllText(Application.dataPath + _filePath + "words.json", JsonUtility.ToJson(_wordJsonList));
	}

	public void CheckAllLevelRepateWord()
	{
		foreach (KeyValuePair<int, WordListItem> item in WordDataManager.instance.ItemList)
		{
			CheckRepeatWord(item.Key);
		}
		Debug.Log("check end");
	}

	private void CheckRepeatWord(int lvl)
	{
		WordListItem item = WordDataManager.instance.GetItemByLevel(lvl);
		int i;
		for (i = 0; i < item.Words.Count; i++)
		{
			if (item.Bonus.FindIndex((string x) => x == item.Words[i]) >= 0)
			{
				Debug.LogError("Level " + lvl + ",word:" + item.Words[i] + " in bonus");
			}
		}
	}
}
