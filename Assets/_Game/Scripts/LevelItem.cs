using System;
using System.Collections.Generic;
using System.Text;

[Serializable]
public class LevelItem
{
	public int LevelID;

	public int SetID;

	public string Letters;

	public int Row;

	public int Column;

	public string Words;

	public string OtherWords;

	public void SplitWords()
	{
		string[] array = Words.Split('|');
		List<string> list = new List<string>();
		for (int i = 0; i < array.Length; i++)
		{
			string[] array2 = array[i].Split(',');
			if (array2.Length > 2)
			{
				list.Add(array2[2]);
			}
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("\"");
		for (int j = 0; j < list.Count; j++)
		{
			if (j < list.Count - 1)
			{
				stringBuilder.Append(list[j] + ",");
			}
			else
			{
				stringBuilder.Append(list[j]);
			}
		}
		stringBuilder.Append("\"");
		Words = stringBuilder.ToString();
	}

	public void SplitOtherWords()
	{
		string[] array = OtherWords.Split(',');
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("\"");
		for (int i = 0; i < array.Length; i++)
		{
			if (i < array.Length - 1)
			{
				stringBuilder.Append(array[i] + ",");
			}
			else
			{
				stringBuilder.Append(array[i]);
			}
		}
		stringBuilder.Append("\"");
		OtherWords = stringBuilder.ToString();
	}

	public string GetLevelString()
	{
		SplitWords();
		SplitOtherWords();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append(LevelID);
		stringBuilder.Append(",");
		stringBuilder.Append(Words);
		stringBuilder.Append(",");
		stringBuilder.Append(string.Empty);
		stringBuilder.Append(",");
		stringBuilder.Append(OtherWords);
		return stringBuilder.ToString();
	}
}
