using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LevelMapBgScript : MonoBehaviour
{
	public Image LevelMapBg;

	public Image TempImage;

	public static LevelMapBgScript Instance;

	private int curMapIndex = -1;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void ChangeBgByIndex(MapDataCsv data, bool isShowBlur)
	{
		if (curMapIndex != data.MapInListIndex || UIManager.Instance.IsCameraBlurOpen != isShowBlur)
		{
			KillAllTween();
			float duration = 1f;
			string str = "Image/LevelMap/";
			string image = data.Image;
			Sprite sprite = ResourceLoadManager.GetInstance().LoadSpriteByPath(str + image, ResourceLoadManager.SP_LEVELMAP_TYPE.BG);
			if (!isShowBlur)
			{
				if (LevelMapUIScript.Instance.ClearImage.sprite == null)
				{
					LevelMapUIScript.Instance.ClearImage.sprite = sprite;
				}
				else if (UIManager.Instance.IsCameraBlurOpen != isShowBlur)
				{
					LevelMapUIScript.Instance.ClearImage.gameObject.SetActive(value: true);
					LevelMapUIScript.Instance.ClearImage.color = new Color(1f, 1f, 1f, 0f);
					LevelMapUIScript.Instance.ClearImage.sprite = sprite;
					LevelMapUIScript.Instance.ClearImage.DOFade(1f, duration).SetEase(Ease.InQuad);
				}
				else
				{
					LevelMapUIScript.Instance.ClearImage.gameObject.SetActive(value: true);
					LevelMapUIScript.Instance.TempClearImage.gameObject.SetActive(value: true);
					LevelMapUIScript.Instance.ClearImage.color = new Color(1f, 1f, 1f, 0f);
					LevelMapUIScript.Instance.TempClearImage.color = Color.white;
					LevelMapUIScript.Instance.TempClearImage.sprite = LevelMapUIScript.Instance.ClearImage.sprite;
					LevelMapUIScript.Instance.ClearImage.sprite = sprite;
					LevelMapUIScript.Instance.ClearImage.DOFade(1f, duration).SetEase(Ease.InQuad);
					LevelMapUIScript.Instance.TempClearImage.DOFade(0f, duration).SetEase(Ease.InQuad).OnComplete(delegate
					{
						LevelMapUIScript.Instance.TempClearImage.gameObject.SetActive(value: false);
					});
				}
				LevelMapBg.sprite = sprite;
				LevelMapBg.color = new Color(1f, 1f, 1f, 0f);
				LevelMapBg.DOFade(1f, duration).SetEase(Ease.InQuad);
				TempImage.gameObject.SetActive(value: false);
			}
			else if (UIManager.Instance.IsCameraBlurOpen != isShowBlur)
			{
				LevelMapUIScript.Instance.ClearImage.DOFade(0f, duration).SetEase(Ease.InQuad).OnComplete(delegate
				{
					LevelMapUIScript.Instance.ClearImage.gameObject.SetActive(value: false);
				});
				LevelMapBg.sprite = sprite;
				TempImage.gameObject.SetActive(value: false);
			}
			else
			{
				TempImage.gameObject.SetActive(value: true);
				LevelMapBg.color = new Color(1f, 1f, 1f, 0f);
				TempImage.color = Color.white;
				TempImage.sprite = LevelMapBg.sprite;
				LevelMapBg.sprite = sprite;
				LevelMapBg.DOFade(1f, duration).SetEase(Ease.InQuad);
				TempImage.DOFade(0f, duration).SetEase(Ease.InQuad).OnComplete(delegate
				{
					TempImage.gameObject.SetActive(value: false);
				});
			}
			UIManager.Instance.SetCameraBlur(isShowBlur);
			curMapIndex = data.MapInListIndex;
		}
		if (!isShowBlur)
		{
			UIManager.Instance.CreateBgEffect(LevelMapUIScript.Instance.ClearImgBlackBg.transform, data.LevelStartNum);
		}
		else
		{
			UIManager.Instance.CleanBgEffect(LevelMapUIScript.Instance.ClearImgBlackBg.transform);
		}
	}

	public void KillAllTween()
	{
		TempImage.DOComplete();
		LevelMapBg.DOComplete();
		LevelMapUIScript.Instance.ClearImage.DOComplete();
		LevelMapUIScript.Instance.TempClearImage.DOComplete();
	}

	public Sprite GetCurMapBgSprite()
	{
		return LevelMapBg.sprite;
	}

	public void OnDestroy()
	{
		Instance = null;
	}
}
