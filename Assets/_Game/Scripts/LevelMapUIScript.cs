using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelMapUIScript : MonoBehaviour
{
	public enum HuoDongBtnType
	{
		HuoDong,
		HuoDongGardon,
		HuoDongSun,
		HuoDongTournament,
		HuoDongWordFind
	}

	public enum LeftBtnType
	{
		VIP,
		LuckyDraw
	}

	public static LevelMapUIScript Instance;

	public Text ScoreNumText;

	public Image ScoreBgImg;

	public Text GoldNumText;

	public GameObject Content;

	public GameObject StyleParent;

	public GameObject LinkIconParent;

	public GameObject ComingSoonParent;

	private Image ChapterNameObj;

	public GameObject LocationBtn;

	public ScrollRectNew Scroll;

	public Image ClearImage;

	public Image ClearImgBlackBg;

	public Image TempClearImage;

	public GameObject DailyPuzzleNewIcon;

	public GameObject DailySearchNewIcon;

	public GameObject MailBtnObj;

	public GameObject DownBtnsObj;

	public GameObject PiggyBankBtnObj;

	public GameObject PiggyUnlock_go;

	public GameObject PiggyBankFullObj;

	public Text piggybank_full_txt;

	public GameObject BtnSlots_go;

	public GameObject BtnVip_go;

	private GameObject PiggyBankFullEff;

	public GameObject NoDragImg_go;

	public GameObject[] HuoDongBtns;

	private int[] HuoDongBtns_PosIdx;

	public Text HuoDong_text;

	public Text HuoDongGardon_text;

	public Text HuoDongSun_text;

	public Text HuoDongTournament_text;

	public Text HuoDongWordFind_text;

	public GameObject[] LeftBtns;

	public Text WorldBtnText;

	public Text DailyPuzzleBtnText;

	public Image DailyPuzzleBtnIcon;

	public Image DailyPuzzleLock;

	public Text DailySearchBtnText;

	public Image DailySearchBtnIcon;

	public Image DailySearchLock;

	public Image DailyQuestLock;

	public Text QuestBtnText;

	public RectTransform m_RtfHideBtnsTrans;

	public GoldScript GoldItem;

	private RectTransform ContentRect;

	private RectTransform ScrollViewRect;

	private float lastChangeStyleY;

	private List<float> specialItemObjHList;

	private Dictionary<int, LevelStyleItem> ItemList;

	private Dictionary<int, LinkIconItem> LinkIconList;

	private GameObject ComingSoonGo;

	private float scrollValue;

	private float mapH = 2250f;

	public static float CanvasSizeY = -1f;

	private float locationBtnH;

	private float linkIconLeftX = 225f;

	private bool isStartUpMove;

	private bool isStartDownMove;

	private bool isLastFrameMoveUp;

	private int curContentInMapIndex;

	private int curMaxLevelIndex;

	private int mapMaxLevel;

	private LevelSelItem curMaxLevelSel;

	private Vector2 MaxLevelPointPos;

	private float mapStartH = 160f;

	private float mapEndH = 500f;

	private float specialItemH;

	private int specialLevelNum = 6;

	private float scrollViewToBottom;

	private bool isCheckDPCTutorial;

	private bool isCheckDailySearchTutorial;

	private static bool isShowAddGoldAnim;

	private static int addGoldCount;

	private static float addGoldDelay;

	private float maxLevelContentPosY;

	private float ToTargetLevelByAnim_delayTime;

	private float NoDragTime;

	private float PerPointSpeed = 0.35f;

	private List<PointAnimInfo> moveAnim_pointList = new List<PointAnimInfo>();

	public bool isTest;

	public Vector2Int TestNum = new Vector2Int(3330, 3339);

	private float restoreTip_waitTime = 0.5f;

	private float sendUpdateUserData_time = 1f;

	private bool isSendHuoDongMsg = true;

	private float showHuoDongTutorialwTime;

	private float showHuoDongGardonTutorialTime;

	private float showHuoDongSunTutorialTime;

	private float showHuoDongWordFindTutorialTime;

	private float showHuoDongTournamentTutorialTime;

	private float showLuckyDrawTutorialwTime;

	private bool isStartUpdateHuoDongTime;

	private float count_timehuodong = 1f;

	private float check_dpcT_waitTime;

	private bool isUpdateHuoDongBtns;

	private bool isUpdateLeftBtns;

	private int fb_loginstate = 100;

	private float hd_btn_base_y = -278f;

	private float left_btn_base_y = -171f;

	private void Awake()
	{
		m_RtfHideBtnsTrans.localPosition = new Vector3(0f, 0f - WordGlobal.GetSaftTopSize(), 0f);
		Instance = this;
		Vector2 sizeDelta = StyleParent.GetComponent<RectTransform>().sizeDelta;
		mapStartH = Mathf.Abs(sizeDelta.y);
		ScoreNumText.text = PlayerInfo.Instance.Score.ToString();
		mapMaxLevel = MapDataManager.instance.GetMaxLevel();
		ContentRect = Content.GetComponent<RectTransform>();
		ScrollViewRect = Scroll.GetComponent<RectTransform>();
		Vector2 sizeDelta2 = ScrollViewRect.sizeDelta;
		scrollViewToBottom = 0f - sizeDelta2.y;
		ItemList = new Dictionary<int, LevelStyleItem>();
		LinkIconList = new Dictionary<int, LinkIconItem>();
		specialItemObjHList = new List<float>();
		GameObject gameObject = ResourceLoadManager.GetInstance().LoadPrefabObj("UI/LevelMap/style_1");
		Vector2 sizeDelta3 = gameObject.GetComponent<RectTransform>().sizeDelta;
		mapH = sizeDelta3.y;
	}

	private void Start()
	{
		WorldBtnText.text = TextLibraryDataManagerCsv.instance.GetText(1189);
		DailyPuzzleBtnText.text = TextLibraryDataManagerCsv.instance.GetText(1190);
		QuestBtnText.text = TextLibraryDataManagerCsv.instance.GetText(1191);
		DailySearchBtnText.text = TextLibraryDataManagerCsv.instance.GetText(1237);
		piggybank_full_txt.text = TextLibraryDataManagerCsv.instance.GetText(1326);
		Vector2 sizeDelta = LocationBtn.GetComponent<RectTransform>().sizeDelta;
		locationBtnH = sizeDelta.y;
		if (base.transform.parent != null)
		{
			Vector2 sizeDelta2 = base.transform.parent.GetComponent<RectTransform>().sizeDelta;
			CanvasSizeY = sizeDelta2.y;
		}
		ComingSoonGo = ResourceLoadManager.GetInstance().LoadUI("UI/LevelMap/ComingSoonItem", ComingSoonParent.transform);
		RectTransform component = ComingSoonGo.GetComponent<RectTransform>();
		component.anchoredPosition = new Vector2(0f, 40f);
		Vector2 sizeDelta3 = component.sizeDelta;
		mapEndH = sizeDelta3.y - 100f;
		GetSpecialItemHList();
		RectTransform contentRect = ContentRect;
		Vector2 sizeDelta4 = ContentRect.sizeDelta;
		contentRect.sizeDelta = new Vector2(sizeDelta4.x, GetContentSizeY());
		Debug.Log("Cheat Level: ");
        //if (PlayerInfo.Instance.MaxPlayLevel < 30)
        //    PlayerInfo.Instance.MaxPlayLevel = 30;
        int level = (PlayerInfo.Instance.MaxPlayLevel <= mapMaxLevel) ? PlayerInfo.Instance.MaxPlayLevel : mapMaxLevel;

		curMaxLevelIndex = MapDataManager.instance.GetMapData(level).MapInListIndex;
		MaxLevelPointPos = Vector2.zero;
		PlayerInfo.Instance.GetLittleTipsData().Init();
		PlayerInfo.Instance.GetDailyQuestSaveData().CheckTask();
		WordFaceBook.Instance.SendApplicationVersion();
		WordFaceBook.Instance.GetHuoDongData();
		ShowUI();
		//WordFaceBook.Instance.UpdateTrackTaskEvent("Adjust_Adid", Adjust.getAdid());
		//Adjust.getGoogleAdId(GetGoogleAdId);
	}

	public void GetGoogleAdId(string id)
	{
		WordFaceBook.Instance.UpdateTrackTaskEvent("Adjust_GoogleAdId", id);
	}

	public static void MarkToAddGold(int count, float delay = 0f)
	{
		isShowAddGoldAnim = true;
		addGoldCount = count;
		addGoldDelay = delay;
	}

	public float GetContentSizeY()
	{
		return specialItemH + (float)(MapDataManager.instance.GetSmallChapterCount() - specialLevelNum) * mapH + mapStartH + mapEndH;
	}

	public void GetSpecialItemHList()
	{
		for (int i = 0; i < specialLevelNum; i++)
		{
			MapDataCsv mapDataByIndex = MapDataManager.instance.GetMapDataByIndex(i);
			string str = "UI/LevelMap/";
			string str2 = "style_" + mapDataByIndex.Style;
			GameObject gameObject = ResourceLoadManager.GetInstance().LoadPrefabObj(str + str2);
			Vector2 sizeDelta = gameObject.GetComponent<RectTransform>().sizeDelta;
			float y = sizeDelta.y;
			specialItemH += y;
			specialItemObjHList.Add(y);
		}
	}

	public void ShowUI(int targetLevel = 0)
	{
		DailyPuzzleNewIcon.SetActive(value: false);
		DailySearchNewIcon.SetActive(value: false);
		if (PlayerInfo.Instance.MaxPlayLevel > WordGlobal.OpenDailyPuzzleLevel && WordJsonDataManager.instance.IsOpenDailyPuzzle())
		{
			if (PlayerInfo.Instance.GetDailyPuzzleItem().IsFinished == 0)
			{
				DailyPuzzleNewIcon.SetActive(value: true);
			}
			DailyPuzzleBtnIcon.color = Color.white;
			DailyPuzzleBtnText.color = Color.white;
			DailyPuzzleLock.gameObject.SetActive(value: false);
		}
		else
		{
			DailyPuzzleBtnIcon.color = new Color(169f / 255f, 169f / 255f, 169f / 255f);
			DailyPuzzleBtnText.color = WordGlobal.ChangeStringToColor("#a9a9a9");
			DailyPuzzleLock.gameObject.SetActive(value: true);
		}
		if (PlayerInfo.Instance.MaxPlayLevel >= WordGlobal.OpenDailyQuestLevel)
		{
			DailyQuestLock.gameObject.SetActive(value: false);
		}
		else
		{
			DailyQuestLock.gameObject.SetActive(value: true);
		}
		if (PlayerInfo.Instance.MaxPlayLevel > WordGlobal.OpenDailySearchLevel)
		{
			DailySearchBtnIcon.color = Color.white;
			DailySearchBtnText.color = Color.white;
			DailySearchLock.gameObject.SetActive(value: false);
			if (PlayerInfo.Instance.GetWordSearchSaveData().GetCurPlayLevel() <= 0)
			{
				DailySearchNewIcon.SetActive(value: true);
			}
		}
		else
		{
			DailySearchBtnIcon.color = new Color(169f / 255f, 169f / 255f, 169f / 255f);
			DailySearchBtnText.color = WordGlobal.ChangeStringToColor("#a9a9a9");
			DailySearchLock.gameObject.SetActive(value: true);
		}
		DailyPuzzleNewIcon.transform.parent.gameObject.SetActive(WordGlobal.IsShowDailyPuzzleBtn);
		SetNoDragTime(0f);
		int num = mapMaxLevel;
		if (targetLevel == 0)
		{
			targetLevel = ((PlayerInfo.Instance.MaxPlayLevel <= num) ? PlayerInfo.Instance.MaxPlayLevel : num);
		}
		if (GoldItem != null)
		{
			GoldItem._isAddGold = false;
		}
		if (PlayerInfo.Instance.MaxPlayLevel > mapMaxLevel && WordGlobal.LevelEndAddGoldNum > 0 && GoldItem != null)
		{
			PlayerInfo.Instance.AddGold(-WordGlobal.LevelEndAddGoldNum, 18);
			GoldItem.ResetGold();
			GoldItem._isAddGold = true;
			GoldItem.AddGold(WordGlobal.LevelEndAddGoldNum, 1f, 1f, isAdd2PlayerInfo: true, 19);
			WordGlobal.LevelEndAddGoldNum = 0;
		}
		PlayerInfo.Instance.GetPigGoldSaveData().OpenPig();
		ShowPigBtn();
		CleanStyleList();
		List<int> list = new List<int>();
		MapDataCsv mapData = MapDataManager.instance.GetMapData(targetLevel);
		int level = (mapData.LevelStartNum - 2 < 1) ? 1 : (mapData.LevelStartNum - 2);
		MapDataCsv mapData2 = MapDataManager.instance.GetMapData(level);
		int level2 = (mapData2.LevelStartNum - 2 < 1) ? 1 : (mapData2.LevelStartNum - 2);
		MapDataCsv mapData3 = MapDataManager.instance.GetMapData(level2);
		int level3 = (mapData.LevelEndNum + 2 <= num) ? (mapData.LevelEndNum + 2) : num;
		MapDataCsv mapData4 = MapDataManager.instance.GetMapData(level3);
		int level4 = (mapData4.LevelEndNum + 2 <= num) ? (mapData4.LevelEndNum + 2) : num;
		MapDataCsv mapData5 = MapDataManager.instance.GetMapData(level4);
		if (mapData5.MapInListIndex >= 0 && mapData5.MapInListIndex < MapDataManager.instance.GetSmallChapterCount() && !list.Contains(mapData5.MapInListIndex))
		{
			list.Add(mapData5.MapInListIndex);
		}
		if (mapData4.MapInListIndex >= 0 && mapData4.MapInListIndex < MapDataManager.instance.GetSmallChapterCount() && !list.Contains(mapData4.MapInListIndex))
		{
			list.Add(mapData4.MapInListIndex);
		}
		if (mapData.MapInListIndex >= 0 && !list.Contains(mapData.MapInListIndex))
		{
			list.Add(mapData.MapInListIndex);
		}
		if (mapData2.MapInListIndex >= 0 && !list.Contains(mapData2.MapInListIndex))
		{
			list.Add(mapData2.MapInListIndex);
		}
		if (mapData3.MapInListIndex >= 0)
		{
			list.Add(mapData5.MapInListIndex);
		}
		for (int i = 0; i < list.Count; i++)
		{
			ShowStyleIdByIndex(list[i], 1);
		}
		PlayerInfo.Instance.GetDailyQuestSaveData().Getlist(13);
		if (UIManager.CurLevelMapUIType == UIManager.LevelMapUIType.Type_OpenChapter)
		{
			int mapInListIndex = mapData.MapInListIndex;
			if (LinkIconList != null && LinkIconList.ContainsKey(mapInListIndex))
			{
				LinkIconItem linkIconItem = LinkIconList[mapInListIndex];
				linkIconItem.ShowLockIconAnim();
			}
		}
		RefreshMaxLevelPointPos(targetLevel);
		if (UIManager.CurLevelMapUIType == UIManager.LevelMapUIType.Type_OpenChapter)
		{
			Invoke("RefreshCurSelIcon", 2f);
			if (PlayerInfo.Instance.MaxPlayLevel == 7)
			{
				Invoke("ShowOpenChapterTips", 2f);
			}
		}
		else
		{
			RefreshCurSelIcon();
		}
		RefreshContentData(targetLevel);
		LocationBtn.SetActive(value: false);
		isUpdateHuoDongBtns = true;
		isUpdateLeftBtns = true;
		UIManager.CurLevelMapUIType = UIManager.LevelMapUIType.Type_Normal;
		if (!WordGlobal.isWordSearchTutorialFromMainUI)
		{
			if (WordJsonDataManager.instance.IsOpenDailyPuzzle() && PlayerInfo.Instance.MaxPlayLevel > WordGlobal.OpenDailyPuzzleLevel)
			{
				DailyPuzzleGetImageData dailyPuzzleImageData = PlayerInfo.Instance.GetDailyPuzzleImageData();
				if (dailyPuzzleImageData != null && dailyPuzzleImageData.Tutorial == 0)
				{
					isCheckDPCTutorial = true;
				}
			}
		}
		else
		{
			isCheckDPCTutorial = false;
		}
		if (PlayerInfo.Instance.MaxPlayLevel > WordGlobal.OpenDailySearchLevel && PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 0)
		{
			isCheckDailySearchTutorial = true;
		}
		if (isShowAddGoldAnim)
		{
			isShowAddGoldAnim = false;
			GoldItem.AddGold(addGoldCount, addGoldDelay, 1f, isAdd2PlayerInfo: true, 12);
		}
		WordGlobal.isUpdateLevelMapHuoDong = true;
		WordGlobal.isUpdateLevelMapLeftBtn = true;
		WordGlobal.HuoDongRefreshLevelMapIconIndex++;
		if (WordGlobal.isNeedHuoDong)
		{
			if (WordGlobal.GetHuoDongTimeLeft() <= 0)
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDong, isShow: false);
			}
			if (WordGlobal.GetHuoDongGardonTimeLeft() <= 0)
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongGardon, isShow: false);
			}
			if (WordGlobal.GetHuoDongSunTimeLeft() <= 0)
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongSun, isShow: false);
			}
			if (WordGlobal.GetHuoDongTournamentTimeLeft() <= 0 || PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardState == 2)
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongTournament, isShow: false);
			}
			if (WordGlobal.GetHuoDongWordFindTimeLeft() <= 0)
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongWordFind, isShow: false);
			}
		}
		if (!string.IsNullOrEmpty(WordGlobal.LevelMapOpenShowUI))
		{
			if (WordGlobal.LevelMapOpenShowUI.Equals("HuoDongGardon"))
			{
				BtnHuoDongGardonClick();
			}
			else if (WordGlobal.LevelMapOpenShowUI.Equals("HuoDongSun"))
			{
				BtnHuoDongSunClick();
			}
			else if (WordGlobal.LevelMapOpenShowUI.Equals("WordSearch"))
			{
				ShowDailySearchUI("Dailysearch_enter");
			}
			WordGlobal.LevelMapOpenShowUI = string.Empty;
		}
		if (WordGlobal.LevelMapMoveAnim_StartLevel > 0)
		{
			WordGlobal.LevelMapMoveAnim_EndLevel = PlayerInfo.Instance.MaxPlayLevel;
			if (WordGlobal.LevelMapMoveAnim_EndLevel > WordGlobal.LevelMapMoveAnim_StartLevel)
			{
				ToTargetLevelByAnim_delayTime = 0.5f;
				ToTargetLevelByAnim(WordGlobal.LevelMapMoveAnim_StartLevel + 1, WordGlobal.LevelMapMoveAnim_EndLevel);
				WordGlobal.LevelMapMoveAnim_StartLevel = -1;
			}
		}
	}

	public void SetAddGoldState(bool isTrue)
	{
		if (GoldItem != null)
		{
			GoldItem._isAddGold = isTrue;
		}
	}

	public void CheckDCPTutorial()
	{
		if (WordJsonDataManager.instance.IsOpenDailyPuzzle() && PlayerInfo.Instance.MaxPlayLevel > WordGlobal.OpenDailyPuzzleLevel)
		{
			DailyPuzzleGetImageData dailyPuzzleImageData = PlayerInfo.Instance.GetDailyPuzzleImageData();
			if (dailyPuzzleImageData != null && dailyPuzzleImageData.Tutorial == 0)
			{
				isCheckDPCTutorial = true;
			}
		}
	}

	public void ShowPigBtn()
	{
		if (PlayerInfo.Instance.GetPigGoldSaveData().isOpen())
		{
			PiggyBankBtnObj.SetActive(value: true);
			PiggyUnlock_go.SetActive(value: false);
			PiggyBankFullObj.SetActive(PlayerInfo.Instance.GetPigGoldSaveData().isFull());
			if (PiggyBankFullObj.activeInHierarchy && PiggyBankFullEff == null)
			{
				PiggyBankFullEff = ResourceLoadManager.GetInstance().LoadUI("UI/PiggyBank/piggy_tx", PiggyBankFullObj.transform);
				PiggyBankFullEff.transform.localPosition = Vector3.zero;
				PiggyBankFullEff.transform.localScale = Vector3.one;
				(PiggyBankFullEff.transform as RectTransform).anchoredPosition = new Vector2(0f, 10f);
			}
			if (PiggyBankFullEff != null)
			{
				PiggyBankFullEff.SetActive(PlayerInfo.Instance.GetPigGoldSaveData().isFull());
			}
		}
		else
		{
			PiggyUnlock_go.SetActive(value: true);
		}
		WordGlobal.isUpdateLevelMapPigBtn = false;
	}

	public void LeftBtnRefresh()
	{
		//Debug.Log("Hide VIP btn ");
		//ShowLeftBtn(LeftBtnType.VIP, PlayerInfo.Instance.IsShowVip());
		if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().IsNeedLuckyDrawBtn() && PlayerInfo.Instance.GetPlayerOtherInfoSaveData().ShowLuckyDrawTutorial())
		{
			showLuckyDrawTutorialwTime = 0.2f;
		}
		ShowLeftBtn(LeftBtnType.LuckyDraw, PlayerInfo.Instance.GetPlayerOtherInfoSaveData().IsNeedLuckyDrawBtn());
	}

	public void HuoDongRefresh()
	{
		if (WordGlobal.isNeedHuoDong)
		{
			if (WordGlobal.GetHuoDongTimeLeft() > 0)
			{
				HuoDongSaveData huoDongSaveData = PlayerInfo.Instance.GetHuoDongSaveData();
				if (huoDongSaveData.HuoDongID == 0)
				{
					huoDongSaveData.InitHuoDongID();
				}
				if (huoDongSaveData.HuoDongID > 0)
				{
					if (huoDongSaveData.GetRewardState == 0)
					{
						ShowHuoDongBtn(HuoDongBtnType.HuoDong, isShow: true);
						EventDataCsv dataById = EventDataManagerCsv.instance.GetDataById(huoDongSaveData.HuoDongID);
						if (dataById == null || huoDongSaveData.HuoDongItemAmount >= dataById.requireamount)
						{
						}
						if (PlayerInfo.Instance.GetHuoDongSaveData().Tutorial02 == 0)
						{
							showHuoDongTutorialwTime = 0.2f;
							PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHuoDongTutorial02();
						}
					}
					else
					{
						ShowHuoDongBtn(HuoDongBtnType.HuoDong, isShow: false);
					}
				}
				else
				{
					ShowHuoDongBtn(HuoDongBtnType.HuoDong, isShow: false);
				}
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDong, isShow: false);
			}
			if (WordGlobal.GetHuoDongGardonTimeLeft() > 0)
			{
				HuoDongGardonSaveData huoDongGardonSaveData = PlayerInfo.Instance.GetHuoDongGardonSaveData();
				if (huoDongGardonSaveData.HuoDongID == 0)
				{
					huoDongGardonSaveData.InitHuoDongID();
				}
				if (huoDongGardonSaveData.HuoDongID > 0)
				{
					huoDongGardonSaveData.CheckPowerTime();
				}
				if (huoDongGardonSaveData.isCloseHuoDong)
				{
					ShowHuoDongBtn(HuoDongBtnType.HuoDongGardon, isShow: false);
				}
				else
				{
					ShowHuoDongBtn(HuoDongBtnType.HuoDongGardon, isShow: true);
				}
				if (PlayerInfo.Instance.GetHuoDongGardonSaveData().Tutorial01 == 0 && IsHuoDongOpenByLevel(HuoDongBtnType.HuoDongGardon))
				{
					showHuoDongGardonTutorialTime = 0.2f;
					PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHuoDongGardonTutorial01();
				}
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongGardon, isShow: false);
			}
			if (WordGlobal.GetHuoDongSunTimeLeft() > 0)
			{
				HuoDongSunSaveData huoDongSunSaveData = PlayerInfo.Instance.GetHuoDongSunSaveData();
				if (huoDongSunSaveData.HuoDongID == 0)
				{
					huoDongSunSaveData.InitHuoDongID();
				}
				if (huoDongSunSaveData.isFull())
				{
					ShowHuoDongBtn(HuoDongBtnType.HuoDongSun, isShow: false);
				}
				else
				{
					ShowHuoDongBtn(HuoDongBtnType.HuoDongSun, isShow: true);
				}
				if (PlayerInfo.Instance.GetHuoDongSunSaveData().Tutorial02 == 0 && IsHuoDongOpenByLevel(HuoDongBtnType.HuoDongSun))
				{
					showHuoDongSunTutorialTime = 0.2f;
					PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHuoDongSunTutorial02(1);
				}
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongSun, isShow: false);
			}
			if (WordGlobal.GetHuoDongWordFindTimeLeft() > 0)
			{
				HuoDongWordFindSaveData huoDongWordFindSaveData = PlayerInfo.Instance.GetHuoDongWordFindSaveData();
				if (huoDongWordFindSaveData.HuoDongID == 0)
				{
					huoDongWordFindSaveData.InitHuoDongID();
				}
				if (huoDongWordFindSaveData.isFull())
				{
					ShowHuoDongBtn(HuoDongBtnType.HuoDongWordFind, isShow: false);
				}
				else
				{
					ShowHuoDongBtn(HuoDongBtnType.HuoDongWordFind, isShow: true);
				}
				if (PlayerInfo.Instance.GetHuoDongWordFindSaveData().Tutorial02 == 0 && IsHuoDongOpenByLevel(HuoDongBtnType.HuoDongWordFind))
				{
					showHuoDongWordFindTutorialTime = 0.2f;
					PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHuoDongWordFindTutorial02(1);
				}
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongWordFind, isShow: false);
			}
			if (WordGlobal.GetHuoDongTournamentTimeLeft() > 0 && PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardState < 2)
			{
				HuoDongTournamentSaveData huoDongTournamentSaveData = PlayerInfo.Instance.GetHuoDongTournamentSaveData();
				if (huoDongTournamentSaveData.HuoDongID == 0)
				{
					huoDongTournamentSaveData.InitHuoDongID();
				}
				if (huoDongTournamentSaveData.HuoDongID > 0)
				{
					if (WordGlobal.GetHuoDongTournamentTimeLeft() > WordGlobal.HuoDongTournamentFinishtTime)
					{
						ShowHuoDongBtn(HuoDongBtnType.HuoDongTournament, isShow: true);
					}
					else if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongItemAmount > 0)
					{
						ShowHuoDongBtn(HuoDongBtnType.HuoDongTournament, isShow: true);
					}
					else
					{
						ShowHuoDongBtn(HuoDongBtnType.HuoDongTournament, isShow: false);
					}
					if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().Tutorial01 == 0 && WordGlobal.GetHuoDongTournamentTimeLeft() > WordGlobal.HuoDongTournamentFinishtTime && IsHuoDongOpenByLevel(HuoDongBtnType.HuoDongTournament))
					{
						showHuoDongTournamentTutorialTime = 0.3f;
						PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHuoDongTournamentTutorial01(1);
					}
				}
				else
				{
					ShowHuoDongBtn(HuoDongBtnType.HuoDongTournament, isShow: false);
				}
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongTournament, isShow: false);
			}
		}
		else
		{
			ShowHuoDongBtn(HuoDongBtnType.HuoDong, isShow: false);
			ShowHuoDongBtn(HuoDongBtnType.HuoDongGardon, isShow: false);
			ShowHuoDongBtn(HuoDongBtnType.HuoDongSun, isShow: false);
			ShowHuoDongBtn(HuoDongBtnType.HuoDongTournament, isShow: false);
		}
	}

	public void RefreshMaxLevelPointPos(int targetLevel)
	{
		if (targetLevel == PlayerInfo.Instance.MaxPlayLevel)
		{
			MapDataCsv mapData = MapDataManager.instance.GetMapData(PlayerInfo.Instance.MaxPlayLevel);
			MaxLevelPointPos = ItemList[mapData.MapInListIndex].GetPointPos(PlayerInfo.Instance.MaxPlayLevel);
		}
		else if (PlayerInfo.Instance.MaxPlayLevel > mapMaxLevel && targetLevel == mapMaxLevel)
		{
			MapDataCsv mapData2 = MapDataManager.instance.GetMapData(mapMaxLevel);
			MaxLevelPointPos = ItemList[mapData2.MapInListIndex].GetPointPos(mapMaxLevel);
		}
	}

	public void RefreshContentData(int targetLevel)
	{
		RefreshMaxLevelPointPos(targetLevel);
		MapDataCsv mapData = MapDataManager.instance.GetMapData(targetLevel);
		float contentYByLevel = GetContentYByLevel(targetLevel);
		ContentRect.anchoredPosition = new Vector2(0f, contentYByLevel);
		lastChangeStyleY = contentYByLevel;
		if (targetLevel == PlayerInfo.Instance.MaxPlayLevel)
		{
			maxLevelContentPosY = contentYByLevel;
		}
		ChangeMapBg(mapData.MapInListIndex);
	}

	public float GetContentYByLevel(int targetLevel)
	{
		MapDataCsv mapData = MapDataManager.instance.GetMapData(targetLevel);
		float num = 0f;
		if (!ItemList.ContainsKey(mapData.MapInListIndex))
		{
			return 0f;
		}
		Vector2 vector = ItemList[mapData.MapInListIndex].GetPointPos(targetLevel);
		num = 0f - (mapStartH + GetNormalStlyeItemY(mapData.MapInListIndex) + vector.y - CanvasSizeY / 2f + scrollViewToBottom);
		if (num > 0f)
		{
			num = 0f;
		}
		else
		{
			Vector2 sizeDelta = ContentRect.sizeDelta;
			if (sizeDelta.y > CanvasSizeY)
			{
				float num2 = num;
				Vector2 sizeDelta2 = ContentRect.sizeDelta;
				if (num2 < 0f - (sizeDelta2.y - CanvasSizeY))
				{
					Vector2 sizeDelta3 = ContentRect.sizeDelta;
					num = 0f - (sizeDelta3.y - CanvasSizeY);
				}
			}
		}
		return num;
	}

	public float GetNormalStlyeItemY(int index)
	{
		if (ItemList.ContainsKey(index))
		{
			Vector2 anchoredPosition = ItemList[index].GetComponent<RectTransform>().anchoredPosition;
			return anchoredPosition.y;
		}
		return 0f;
	}

	public int GetMapIndexByContentY(float contentCentY)
	{
		return GetIndexByContentY(contentCentY);
	}

	public int GetIndexByContentY(float contentCentY)
	{
		int result = 0;
		List<int> list = new List<int>(ItemList.Keys);
		for (int num = list.Count - 1; num >= 0; num--)
		{
			LevelStyleItem levelStyleItem = ItemList[list[num]];
			if (contentCentY >= levelStyleItem.ItemStartPosY && contentCentY <= levelStyleItem.ItemEndPosY)
			{
				result = list[num];
			}
			if (levelStyleItem.CurMapData.LevelEndNum == MapDataManager.instance.GetMaxLevel() && contentCentY >= levelStyleItem.ItemEndPosY)
			{
				result = levelStyleItem.CurMapData.MapInListIndex;
			}
		}
		return result;
	}

	public void ShowStyleIdByIndex(int index, int addType)
	{
		MapDataCsv mapDataByIndex = MapDataManager.instance.GetMapDataByIndex(index);
		AddItemToList(mapDataByIndex, addType);
	}

	public float GetSpecialItemPosY(int index)
	{
		float num = 0f;
		for (int i = 0; i < index; i++)
		{
			num += specialItemObjHList[i];
		}
		return num;
	}

	public void ShowStyleItem(int level, int addType = 1)
	{
		MapDataCsv mapData = MapDataManager.instance.GetMapData(level);
		AddItemToList(mapData, addType);
	}

	public void AddItemToList(MapDataCsv data, int addType)
	{
		if (!ItemList.ContainsKey(data.MapInListIndex))
		{
			float num = 0f;
			num = ((data.MapInListIndex >= specialLevelNum) ? (specialItemH + (float)(data.MapInListIndex - specialLevelNum) * mapH) : GetSpecialItemPosY(data.MapInListIndex));
			LevelStyleItem value = CreateStyleItem(data, num, addType);
			ItemList.Add(data.MapInListIndex, value);
		}
	}

	public LevelStyleItem CreateStyleItem(MapDataCsv data, float posY, int index = 1)
	{
		string str = "UI/LevelMap/";
		string str2 = "style_" + data.Style;
		GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI(str + str2, StyleParent.transform);
		LevelStyleItem component = gameObject.GetComponent<LevelStyleItem>();
		RectTransform component2 = gameObject.GetComponent<RectTransform>();
		LevelStyleItem levelStyleItem = component;
		Vector2 sizeDelta = component2.sizeDelta;
		levelStyleItem.SetData(data, posY, posY + sizeDelta.y);
		component2.anchoredPosition = new Vector2(0f, posY);
		if (index == 0)
		{
			gameObject.transform.SetSiblingIndex(0);
		}
		int mapInListIndex = data.MapInListIndex;
		int position = data.Position;
		ShowLinkItem(mapInListIndex, posY, position);
		return component;
	}

	public void ShowLinkItem(int curLinkIndex, float linkPosY, int curSChapterStartPos)
	{
		if (!LinkIconList.ContainsKey(curLinkIndex))
		{
			string path = "UI/LevelMap/LinkIconItem";
			GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI(path, LinkIconParent.transform);
			RectTransform component = gameObject.GetComponent<RectTransform>();
			LinkIconItem component2 = gameObject.GetComponent<LinkIconItem>();
			component2.SetData(curLinkIndex, curMaxLevelIndex);
			LinkIconList.Add(curLinkIndex, component2);
			Vector2 sizeDelta = component.sizeDelta;
			float y = linkPosY + sizeDelta.y / 2f;
			float x = 0f;
			switch (curSChapterStartPos)
			{
			case 0:
				x = 0f - linkIconLeftX;
				break;
			case 2:
				x = linkIconLeftX;
				break;
			}
			component.anchoredPosition = new Vector2(x, y);
		}
	}

	public void ShowOpenChapterTips()
	{
		ResourceLoadManager.GetInstance().ShowNoBtnTipsUI(1195);
	}

	public void RefreshCurSelIcon()
	{
		if (ItemList.ContainsKey(curMaxLevelIndex))
		{
			int level = (PlayerInfo.Instance.MaxPlayLevel <= mapMaxLevel) ? PlayerInfo.Instance.MaxPlayLevel : mapMaxLevel;
			Transform pointByLevel = ItemList[curMaxLevelIndex].GetPointByLevel(level);
			PointItem pointItem = ItemList[curMaxLevelIndex].GetPointItem(level);
			if (curMaxLevelSel == null)
			{
				string path = "UI/LevelMap/SelItem";
				GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI(path, pointByLevel);
				curMaxLevelSel = gameObject.GetComponent<LevelSelItem>();
			}
			else
			{
				curMaxLevelSel.transform.SetParent(pointByLevel);
			}
			pointItem.gameObject.SetActive(value: false);
			curMaxLevelSel.RefreshUI();
			curMaxLevelSel.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
		}
	}

	public void RefreshPointItemIcon(int level)
	{
		int mapInListIndex = MapDataManager.instance.GetMapData(level).MapInListIndex;
		if (ItemList.ContainsKey(mapInListIndex))
		{
			PointItem pointItem = ItemList[mapInListIndex].GetPointItem(level);
			pointItem.gameObject.SetActive(value: true);
			pointItem.RefreshLockedIcon();
		}
	}

	public PointItem GetPointItemByLevel(int level)
	{
		int mapInListIndex = MapDataManager.instance.GetMapData(level).MapInListIndex;
		if (ItemList.ContainsKey(mapInListIndex))
		{
			return ItemList[mapInListIndex].GetPointItem(level);
		}
		return null;
	}

	public void LocationToTargetLevel(int targetLevel)
	{
		if (!(NoDragTime > 0f))
		{
			Scroll.StopMovement();
			ShowUI(targetLevel);
		}
	}

	public void SetNoDragTime(float time)
	{
		NoDragTime = time;
		if (time == 0f)
		{
			NoDragImg_go.SetActive(value: false);
		}
		else
		{
			NoDragImg_go.SetActive(value: true);
		}
	}

	public void ToTargetLevelByAnim(int from_level, int to_level)
	{
		if (from_level >= MapDataManager.instance.GetMaxLevel())
		{
			from_level = MapDataManager.instance.GetMaxLevel();
		}
		if (to_level >= MapDataManager.instance.GetMaxLevel())
		{
			to_level = MapDataManager.instance.GetMaxLevel();
		}
		Debug.Log("from_level:" + from_level + " to_level:" + to_level);
		moveAnim_pointList.Clear();
		for (int i = from_level; i <= to_level; i++)
		{
			int num = i;
			int mapInListIndex = MapDataManager.instance.GetMapData(num).MapInListIndex;
			if (!ItemList.ContainsKey(mapInListIndex))
			{
				continue;
			}
			PointItem pointItem = ItemList[mapInListIndex].GetPointItem(num);
			if (pointItem != null)
			{
				if (num == ItemList[mapInListIndex].CurMapData.LevelStartNum && LinkIconList.ContainsKey(mapInListIndex))
				{
					LinkIconList[mapInListIndex].LockedItem();
					moveAnim_pointList.Add(LinkIconList[mapInListIndex]);
				}
				if (num < MapDataManager.instance.GetMaxLevel())
				{
					pointItem.LockedItem();
					moveAnim_pointList.Add(pointItem);
				}
			}
		}
		if (curMaxLevelSel != null)
		{
			curMaxLevelSel.LockedItem();
			moveAnim_pointList.Add(curMaxLevelSel);
		}
		for (int j = 0; j < moveAnim_pointList.Count; j++)
		{
			moveAnim_pointList[j].UnLockItemAnim((float)j * PerPointSpeed + ToTargetLevelByAnim_delayTime, PerPointSpeed);
		}
		float contentYByLevel = GetContentYByLevel(from_level);
		float contentYByLevel2 = GetContentYByLevel(to_level);
		RectTransform contentRect = ContentRect;
		Vector2 anchoredPosition = ContentRect.anchoredPosition;
		contentRect.anchoredPosition = new Vector2(anchoredPosition.x, contentYByLevel);
		int count = moveAnim_pointList.Count;
		float num2 = (float)count * PerPointSpeed + (float)count * 0.15f;
		ContentRect.DOAnchorPosY(contentYByLevel2, num2).SetDelay(ToTargetLevelByAnim_delayTime);
		SetNoDragTime(num2);
	}

	public void ToClearTargetLevelByAnim()
	{
		for (int i = 0; i < moveAnim_pointList.Count; i++)
		{
			moveAnim_pointList[i].UnLockItemNow();
		}
	}

	public void SetAllItemEffect(bool isShow)
	{
		List<int> list = new List<int>(LinkIconList.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			LinkIconItem linkIconItem = LinkIconList[list[i]];
			linkIconItem.SetEffectShow(isShow);
		}
		list.Clear();
		list = new List<int>(ItemList.Keys);
		for (int j = 0; j < list.Count; j++)
		{
			LevelStyleItem levelStyleItem = ItemList[list[j]];
		}
	}

	private void Update()
	{
		if (isTest)
		{
			isTest = false;
		}
		ScoreNumText.text = PlayerInfo.Instance.Score.ToString();
		int length = ScoreNumText.text.Length;
		if (length > 3)
		{
			ScoreBgImg.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(128f + (float)(15 * (length - 3)), 53f);
			ScoreNumText.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(55 + 15 * (length - 3), 53f);
		}
		if (!WordGlobal.IsEditorPlatform)
		{
			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
			{
				Vector2 anchoredPosition = ContentRect.anchoredPosition;
				lastChangeStyleY = anchoredPosition.y;
				float num = lastChangeStyleY;
				Vector2 sizeDelta = ContentRect.sizeDelta;
				if (num < 0f - (sizeDelta.y - mapH / 2f))
				{
					Vector2 sizeDelta2 = ContentRect.sizeDelta;
					lastChangeStyleY = 0f - (sizeDelta2.y - mapH / 2f);
				}
				else if (lastChangeStyleY > 0f)
				{
					lastChangeStyleY = 0f;
				}
			}
		}
		else if (Input.GetMouseButtonDown(0))
		{
			Vector2 anchoredPosition2 = ContentRect.anchoredPosition;
			lastChangeStyleY = anchoredPosition2.y;
			float num2 = lastChangeStyleY;
			Vector2 sizeDelta3 = ContentRect.sizeDelta;
			if (num2 < 0f - (sizeDelta3.y - mapH / 2f))
			{
				Vector2 sizeDelta4 = ContentRect.sizeDelta;
				lastChangeStyleY = 0f - (sizeDelta4.y - mapH / 2f);
			}
			else if (lastChangeStyleY > 0f)
			{
				lastChangeStyleY = 0f;
			}
		}
		if (isStartUpMove || isStartDownMove)
		{
			int styleItemMinKeyNum = GetStyleItemMinKeyNum();
			int styleItemMaxKeyNum = GetStyleItemMaxKeyNum();
			Vector2 anchoredPosition3 = ContentRect.anchoredPosition;
			if (!(Mathf.Abs(anchoredPosition3.y - lastChangeStyleY) >= mapH))
			{
				Vector2 anchoredPosition4 = ContentRect.anchoredPosition;
				if ((!(Mathf.Abs(anchoredPosition4.y - lastChangeStyleY) >= mapH / 2f) || ItemList.Count > 4) && (!isStartDownMove || curContentInMapIndex > styleItemMinKeyNum) && (!isStartUpMove || curContentInMapIndex < styleItemMaxKeyNum))
				{
					goto IL_02c4;
				}
			}
			StyleListChanged(styleItemMinKeyNum, styleItemMaxKeyNum);
		}
		goto IL_02c4;
		IL_02c4:
		UpdateCheckDPCTutorial();
		if (MailBtnObj.activeSelf != WordGlobal.IsShowMailBtn)
		{
			MailBtnObj.SetActive(WordGlobal.IsShowMailBtn);
		}
		if (WordGlobal.isUpdateLevelMapHuoDong)
		{
			HuoDongRefresh();
			isStartUpdateHuoDongTime = true;
			count_timehuodong = 1f;
			WordGlobal.isUpdateLevelMapHuoDong = false;
		}
		if (WordGlobal.isUpdateLevelMapLeftBtn)
		{
			LeftBtnRefresh();
			WordGlobal.isUpdateLevelMapLeftBtn = false;
		}
		if (WordGlobal.isUpdateLevelMapPigBtn)
		{
			ShowPigBtn();
		}
		//if (CodelessIAPStoreListener.Instance.isSendVipInfo)
		//{
		//	WordFaceBook.Instance.SendVipInfo(CodelessIAPStoreListener.Instance.SubscriptionBeginDate_str, CodelessIAPStoreListener.Instance.SubscriptionEndDate_str, CodelessIAPStoreListener.Instance.SubscriptionTransactionID);
		//	WordGlobal.GetMailTime = 1f;
		//	CodelessIAPStoreListener.Instance.isSendVipInfo = false;
		//}
		UpdateHuoDongTime();
		UpdateHuoDongBtns();
		UpdateHuoDongMsg();
		UpdateSendUpdateUserData();
		UpdateRestoreTip();
		if (NoDragTime > 0f)
		{
			NoDragTime -= Time.deltaTime;
			if (NoDragTime <= 0f)
			{
				SetNoDragTime(0f);
			}
		}
		if (WordGlobal.GetMailTime > 0f)
		{
			WordGlobal.GetMailTime -= Time.deltaTime;
			if (WordGlobal.GetMailTime <= 0f)
			{
				PlayerInfo.Instance.GetMailData();
			}
		}
	}

	public void UpdateRestoreTip()
	{
		//if (IAPListener.isShowRestorePurchaseTip > 0 && restoreTip_waitTime > 0f)
		//{
		//	restoreTip_waitTime -= Time.deltaTime;
		//	if (restoreTip_waitTime <= 0f)
		//	{
		//		ResourceLoadManager.GetInstance().ShowTips(WordGlobal.ReadText(IAPListener.isShowRestorePurchaseTip));
		//		IAPListener.isShowRestorePurchaseTip = 0;
		//		restoreTip_waitTime = 0.5f;
		//	}
		//}
	}

	public void UpdateSendUpdateUserData()
	{
		if (sendUpdateUserData_time > 0f)
		{
			sendUpdateUserData_time -= Time.deltaTime;
			if (sendUpdateUserData_time <= 0f && Application.internetReachability != 0)
			{
				Debug.Log("LevelMap Sync client data to the server");
				WordFaceBook.Instance.UpdateUserdataMill();
			}
		}
	}

	public void UpdateHuoDongMsg()
	{
		if (DateTime.Now.Minute == 0 && DateTime.Now.Second == 0)
		{
			if (isSendHuoDongMsg)
			{
				WordFaceBook.Instance.GetHuoDongData();
				isSendHuoDongMsg = false;
			}
		}
		else
		{
			isSendHuoDongMsg = true;
		}
		if (WordGlobal.isCheckHuoDongTournamentReward)
		{
			PlayerInfo.Instance.GetHuoDongTournamentSaveData().CheckShowRewardUI();
			WordGlobal.isCheckHuoDongTournamentReward = false;
		}
		if (WordGlobal.isReGetHuoDongData)
		{
			Debug.LogError("WordGlobal.isReGetHuoDongData");
			WordFaceBook.Instance.GetHuoDongData();
			WordGlobal.isReGetHuoDongData = false;
		}
	}

	public void UpdateHuoDongTime()
	{
		if (!isStartUpdateHuoDongTime || !WordGlobal.isNeedHuoDong)
		{
			return;
		}
		count_timehuodong += Time.deltaTime;
		if (count_timehuodong > 1f)
		{
			int huoDongTimeLeft = WordGlobal.GetHuoDongTimeLeft();
			if (huoDongTimeLeft > 0 && PlayerInfo.Instance.GetHuoDongSaveData().HuoDongID > 0)
			{
				HuoDong_text.text = WordGlobal.ChangeTimeToNormalSpecial(huoDongTimeLeft, 0, isNeedDay: false);
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDong, isShow: false);
			}
			huoDongTimeLeft = WordGlobal.GetHuoDongGardonTimeLeft();
			if (huoDongTimeLeft > 0 && !PlayerInfo.Instance.GetHuoDongGardonSaveData().isCloseHuoDong)
			{
				HuoDongGardon_text.text = WordGlobal.ChangeTimeToNormalSpecial(huoDongTimeLeft, 0, isNeedDay: false);
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongGardon, isShow: false);
			}
			huoDongTimeLeft = WordGlobal.GetHuoDongSunTimeLeft();
			if (huoDongTimeLeft > 0 && !PlayerInfo.Instance.GetHuoDongSunSaveData().isFull())
			{
				HuoDongSun_text.text = WordGlobal.ChangeTimeToNormalSpecial(huoDongTimeLeft, 0, isNeedDay: false);
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongSun, isShow: false);
			}
			huoDongTimeLeft = WordGlobal.GetHuoDongWordFindTimeLeft();
			if (huoDongTimeLeft > 0 && !PlayerInfo.Instance.GetHuoDongWordFindSaveData().isFull())
			{
				HuoDongWordFind_text.text = WordGlobal.ChangeTimeToNormalSpecial(huoDongTimeLeft, 0, isNeedDay: false);
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongWordFind, isShow: false);
			}
			huoDongTimeLeft = WordGlobal.GetHuoDongTournamentTimeLeft();
			if (huoDongTimeLeft > 0 && PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardState < 2)
			{
				if (huoDongTimeLeft - WordGlobal.HuoDongTournamentFinishtTime > 0)
				{
					HuoDongTournament_text.text = WordGlobal.ChangeTimeToNormalSpecial(huoDongTimeLeft - WordGlobal.HuoDongTournamentFinishtTime, 0, isNeedDay: false);
				}
				else
				{
					if (!WordGlobal.isOpenHuoDongTournamentGetRewardUI)
					{
						PlayerInfo.Instance.GetHuoDongTournamentSaveData().CheckShowRewardUI();
						WordGlobal.isOpenHuoDongTournamentGetRewardUI = true;
					}
					HuoDongTournament_text.text = TextLibraryDataManagerCsv.instance.GetText(1276);
				}
				if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongItemAmount == 0)
				{
					HuoDongTournament_text.text = TextLibraryDataManagerCsv.instance.GetText(1286);
				}
			}
			else
			{
				ShowHuoDongBtn(HuoDongBtnType.HuoDongTournament, isShow: false);
			}
			count_timehuodong = 0f;
		}
		if (showHuoDongTutorialwTime > 0f)
		{
			showHuoDongTutorialwTime -= Time.deltaTime;
			if (showHuoDongTutorialwTime <= 0f && GameObject.Find("HuoDongTutorial02") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTutorial02", isPop: true);
			}
		}
		if (showLuckyDrawTutorialwTime > 0f)
		{
			showLuckyDrawTutorialwTime -= Time.deltaTime;
			if (showLuckyDrawTutorialwTime <= 0f && GameObject.Find("LuckyDrawTutorial") == null)
			{
				UIManager.Instance.LoadMainUI("UI/LuckyDraw/LuckyDrawTutorial", isPop: true);
			}
		}
		if (HuoDongGardonTutorial01.Type == -1 && showHuoDongGardonTutorialTime > 0f && WordGlobal.UI_CurrentUI.Equals("LevelMapUI"))
		{
			showHuoDongGardonTutorialTime -= Time.deltaTime;
			if (showHuoDongGardonTutorialTime <= 0f && GameObject.Find("HuoDongGardonTutorial01") == null)
			{
				HuoDongGardonTutorial01.Type = 0;
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonTutorial01", isPop: true, UIManager.MainUIType.Type_Common, -1, 1);
			}
		}
		if (HuoDongGardonTutorial01.Type == -1 && showHuoDongSunTutorialTime > 0f && WordGlobal.UI_CurrentUI.Equals("LevelMapUI"))
		{
			showHuoDongSunTutorialTime -= Time.deltaTime;
			if (showHuoDongSunTutorialTime <= 0f && GameObject.Find("HuoDongGardonTutorial01") == null)
			{
				HuoDongGardonTutorial01.Type = 1;
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonTutorial01", isPop: true, UIManager.MainUIType.Type_Common, -1, 1);
			}
		}
		if (HuoDongGardonTutorial01.Type == -1 && showHuoDongTournamentTutorialTime > 0f && WordGlobal.UI_CurrentUI.Equals("LevelMapUI"))
		{
			showHuoDongTournamentTutorialTime -= Time.deltaTime;
			if (showHuoDongTournamentTutorialTime <= 0f && PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongItemAmount == 0 && GameObject.Find("HuoDongGardonTutorial01") == null)
			{
				HuoDongGardonTutorial01.Type = 2;
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonTutorial01", isPop: true, UIManager.MainUIType.Type_Common, -1, 1);
			}
		}
		if (HuoDongGardonTutorial01.Type == -1 && showHuoDongWordFindTutorialTime > 0f && WordGlobal.UI_CurrentUI.Equals("LevelMapUI"))
		{
			showHuoDongWordFindTutorialTime -= Time.deltaTime;
			if (showHuoDongWordFindTutorialTime <= 0f && GameObject.Find("HuoDongGardonTutorial01") == null)
			{
				HuoDongGardonTutorial01.Type = 3;
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonTutorial01", isPop: true, UIManager.MainUIType.Type_Common, -1, 1);
			}
		}
	}

	public void UpdateCheckDPCTutorial()
	{
		if (isCheckDPCTutorial)
		{
			if (UIManager.Instance.GetPopCount() > 0 || HuoDongWordFind.isShow)
			{
				return;
			}
			check_dpcT_waitTime += Time.deltaTime;
			if (check_dpcT_waitTime >= 0.4f)
			{
				check_dpcT_waitTime = 0f;
				if (GameObject.Find("DPCTutorialUI") == null)
				{
					UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCTutorialUI", isPop: false, UIManager.MainUIType.Type_Common, 100).name = "DPCTutorialUI";
				}
				isCheckDPCTutorial = false;
			}
		}
		else
		{
			if (!isCheckDailySearchTutorial)
			{
				return;
			}
			if (WordGlobal.OpenWordSearch)
			{
				if (UIManager.Instance.GetPopList().Count > 0)
				{
					return;
				}
				check_dpcT_waitTime += Time.deltaTime;
				if (check_dpcT_waitTime >= 0.4f)
				{
					check_dpcT_waitTime = 0f;
					if (GameObject.Find("HuoDongWordSearchTutorialUI") == null)
					{
						UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchTutorialUI", isPop: false, UIManager.MainUIType.Type_Common, 100).name = "HuoDongWordSearchTutorialUI";
					}
					WordGlobal.isWordSearchTutorialFromMainUI = false;
					isCheckDailySearchTutorial = false;
				}
			}
			else
			{
				isCheckDailySearchTutorial = false;
			}
		}
	}

	public int GetStyleItemMaxKeyNum()
	{
		int num = 0;
		foreach (KeyValuePair<int, LevelStyleItem> item in ItemList)
		{
			if (num < item.Key)
			{
				num = item.Key;
			}
		}
		return num;
	}

	public int GetStyleItemMinKeyNum()
	{
		int num = 10000;
		foreach (KeyValuePair<int, LevelStyleItem> item in ItemList)
		{
			if (num > item.Key)
			{
				num = item.Key;
			}
		}
		return num;
	}

	public void StyleListChanged(int minIndex, int maxIndex)
	{
		int num = curContentInMapIndex;
		MapDataCsv mapDataByIndex = MapDataManager.instance.GetMapDataByIndex(num);
		MapDataCsv mapDataByIndex2 = MapDataManager.instance.GetMapDataByIndex(maxIndex);
		MapDataCsv mapDataByIndex3 = MapDataManager.instance.GetMapDataByIndex(minIndex);
		if (isStartUpMove)
		{
			if (num != minIndex && ItemList.Count > 4 && ItemList.ContainsKey(minIndex))
			{
				DestroyStyleItem(minIndex);
				if (num > maxIndex)
				{
					DestroyStyleItem(minIndex + 1);
				}
			}
			if (mapDataByIndex2.LevelEndNum < MapDataManager.instance.GetMaxLevel())
			{
				ShowStyleItem(mapDataByIndex2.LevelEndNum + 1, 0);
				MapDataCsv mapData = MapDataManager.instance.GetMapData(mapDataByIndex2.LevelEndNum + 1);
				if (num > maxIndex && mapData.LevelEndNum < MapDataManager.instance.GetMaxLevel())
				{
					ShowStyleItem(mapData.LevelEndNum + 1, 0);
				}
			}
			if (mapDataByIndex.LevelEndNum == MapDataManager.instance.GetMaxLevel())
			{
				Vector2 sizeDelta = ContentRect.sizeDelta;
				lastChangeStyleY = 0f - (sizeDelta.y - mapH / 2f);
			}
			else
			{
				Vector2 anchoredPosition = ContentRect.anchoredPosition;
				lastChangeStyleY = anchoredPosition.y;
			}
			isStartUpMove = false;
			RefreshCurSelIcon();
		}
		else
		{
			if (!isStartDownMove)
			{
				return;
			}
			if (num != maxIndex && ItemList.Count > 4 && ItemList.ContainsKey(maxIndex))
			{
				DestroyStyleItem(maxIndex);
				if (num < minIndex)
				{
					DestroyStyleItem(maxIndex - 1);
				}
			}
			if (mapDataByIndex3.MapInListIndex >= 1)
			{
				ShowStyleItem(mapDataByIndex3.LevelStartNum - 1);
				MapDataCsv mapData2 = MapDataManager.instance.GetMapData(mapDataByIndex3.LevelStartNum - 1);
				if (num < minIndex && mapData2.MapInListIndex >= 1)
				{
					ShowStyleItem(mapData2.LevelStartNum - 1);
				}
			}
			if (mapDataByIndex.LevelStartNum == 1)
			{
				lastChangeStyleY = 0f;
			}
			else
			{
				Vector2 anchoredPosition2 = ContentRect.anchoredPosition;
				lastChangeStyleY = anchoredPosition2.y;
			}
			isStartDownMove = false;
			RefreshCurSelIcon();
		}
	}

	public void ShowLevelMapUI()
	{
		base.gameObject.SetActive(value: true);
		int level = (PlayerInfo.Instance.MaxPlayLevel <= mapMaxLevel) ? PlayerInfo.Instance.MaxPlayLevel : mapMaxLevel;
		curMaxLevelIndex = MapDataManager.instance.GetMapData(level).MapInListIndex;
		ShowUI();
	}

	public void ChangeMapBg(int index)
	{
		bool flag = false;
		flag = ((index > curMaxLevelIndex) ? true : false);
		MapDataCsv mapDataByIndex = MapDataManager.instance.GetMapDataByIndex(index);
		if (LevelMapBgScript.Instance != null)
		{
			LevelMapBgScript.Instance.ChangeBgByIndex(mapDataByIndex, flag);
		}
		curContentInMapIndex = index;
	}

	public void ShowChapterNameEff()
	{
	}

	public void ChapterNameEff2()
	{
	}

	public void HideLevelMapUI()
	{
		base.gameObject.SetActive(value: false);
		CleanStyleList();
	}

	public void ShowMainUI(int level)
	{
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_EnterLevel);
		PlayerInfo.Instance.CurrentLevel = level;
		if (PlayerInfo.Instance.CurrentLevel == PlayerInfo.Instance.MaxPlayLevel)
		{
			WordGlobal.LevelMapMoveAnim_StartLevel = level;
		}
		HideLevelMapUI();
		UIManager.Instance.LoadMainUI("UI/MainUI");
	}

	public void ShowDailyPuMainUI()
	{
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_EnterLevel);
		HideLevelMapUI();
		if (GameObject.Find("MainUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/MainUI", isPop: false, UIManager.MainUIType.Type_Daily);
		}
		PlayerInfo.Instance.GetLittleTipsData().SetDailyPuzzleOpen();
	}

	public void BtnHuoDongGardonClick()
	{
		HuoDongGardonUI.OpenType = 0;
		HuoDongGardonTutorial01.isHide = true;
		if (GameObject.Find("HuoDongGardonUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
		}
	}

	public void BtnHuoDongSunClick()
	{
		HuoDongSunUI.ShowType = 0;
		HuoDongGardonTutorial01.isHide = true;
		if (GameObject.Find("HuoDongSunUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongSunUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
		}
	}

	public void BtnHuoDongWordFindClick()
	{
		HuoDongGardonTutorial01.isHide = true;
		if (GameObject.Find("HuoDongWordFind") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordFind");
		}
	}

	public void BtnHuoDongTournamentClick()
	{
		HuoDongGardonTutorial01.isHide = true;
		if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongItemAmount > 0)
		{
			if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardState == 1)
			{
				PlayerInfo.Instance.GetHuoDongTournamentSaveData().SetEndRewardState();
			}
			if (GameObject.Find("HuoDongTournamentUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTournamentUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
			}
		}
		else if (GameObject.Find("HuoDongTournamentFirstUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTournamentFirstUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
		}
	}

	public void ShowDailySearchUI(string event_str)
	{
		if (PlayerInfo.Instance.MaxPlayLevel <= WordGlobal.OpenDailySearchLevel)
		{
			string txt = string.Format(WordGlobal.ReadText(1249), WordGlobal.OpenDailySearchLevel + 1 + string.Empty);
			ResourceLoadManager.GetInstance().ShowTips(txt);
			return;
		}
		if (PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 0)
		{
			HuoDongWordSearchTutorialUI.HideState = 1;
			PlayerInfo.Instance.GetWordSearchSaveData().SetTutorial(1);
		}
		if (PlayerInfo.Instance.GetWordSearchSaveData().isAllComplete())
		{
			UIManager.Instance.ShowTip_DesClose(TextLibraryDataManagerCsv.instance.GetText(1278));
			return;
		}
		WordGlobal.LogEvent(event_str);
		if (GameObject.Find("HuoDongWordSearchUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongWordSearchUI");
		}
		ToClearTargetLevelByAnim();
	}

	public void BtnDailySearchClick()
	{
		ShowDailySearchUI("Dailysearch_enter");
	}

	public void BtnVipClick()
	{
		WordGlobal.LogEvent("VIP_Tap_Map");
		if (GameObject.Find("VipUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/Vip/VipUI");
		}
	}

	public void BtnLuckyDrawClick()
	{
		if (GameObject.Find("LuckyDrawUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/LuckyDraw/LuckyDrawUI", isPop: true);
		}
		LuckyDrawTutorial.isHide = true;
	}

	public void BtnSlotsClick()
	{
		MS_Interface.Current.OnMoreGame();
	}

	public void DestroyStyleItem(int key)
	{
		if (ItemList != null && ItemList.ContainsKey(key))
		{
			LevelStyleItem levelStyleItem = ItemList[key];
			ItemList.Remove(key);
			UnityEngine.Object.Destroy(levelStyleItem.gameObject);
		}
		if (LinkIconList != null && LinkIconList.ContainsKey(key))
		{
			LinkIconItem linkIconItem = LinkIconList[key];
			LinkIconList.Remove(key);
			UnityEngine.Object.Destroy(linkIconItem.gameObject);
		}
	}

	public void CleanStyleList()
	{
		if (ItemList != null)
		{
			List<int> list = new List<int>(ItemList.Keys);
			for (int i = 0; i < list.Count; i++)
			{
				DestroyStyleItem(list[i]);
			}
		}
	}

	public void ScrollViewChanged(Vector2 value)
	{
		float num = value.y - scrollValue;
		isStartUpMove = (scrollValue < value.y);
		isStartDownMove = (scrollValue > value.y);
		if (isStartUpMove != isLastFrameMoveUp)
		{
			Vector2 anchoredPosition = ContentRect.anchoredPosition;
			lastChangeStyleY = anchoredPosition.y;
		}
		isLastFrameMoveUp = isStartUpMove;
		scrollValue = value.y;
		float num2 = CanvasSizeY / 2f - scrollViewToBottom - mapStartH;
		Vector2 anchoredPosition2 = ContentRect.anchoredPosition;
		float contentCentY = num2 - anchoredPosition2.y;
		int mapIndexByContentY = GetMapIndexByContentY(contentCentY);
		if (curContentInMapIndex != mapIndexByContentY)
		{
			ChangeMapBg(mapIndexByContentY);
		}
		bool flag = true;
		Vector2 anchoredPosition3 = ContentRect.anchoredPosition;
		if (anchoredPosition3.y < maxLevelContentPosY + (CanvasSizeY + locationBtnH) / 2f)
		{
			Vector2 anchoredPosition4 = ContentRect.anchoredPosition;
			if (anchoredPosition4.y > maxLevelContentPosY - (CanvasSizeY + locationBtnH) / 2f)
			{
				flag = false;
			}
		}
		if (LocationBtn.activeSelf != flag)
		{
			LocationBtn.SetActive(flag);
		}
	}

	public void BtnRankClick()
	{
		UIManager.Instance.LoadMainUI("UI/RankUI");
	}

	public void BtnSettingClick()
	{
		UIManager.Instance.ShowLog();
		if (GameObject.Find("SettingUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/SettingUI", isPop: true);
		}
	}

	public void BtnLocationClick()
	{
		int targetLevel = (PlayerInfo.Instance.MaxPlayLevel <= mapMaxLevel) ? PlayerInfo.Instance.MaxPlayLevel : mapMaxLevel;
		LocationToTargetLevel(targetLevel);
	}

	public void BtnHuoDongClick()
	{
		if (GameObject.Find("HuoDongUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
		}
		HuoDongTutorial02.isHide = true;
	}

	public void BtnPiggyBankClick()
	{
		if (PlayerInfo.Instance.GetPigGoldSaveData().isOpen())
		{
			if (GameObject.Find("PiggyBank") == null)
			{
				PiggyBankUI.isLevelMapOpen = true;
				PlayerInfo.Instance.GetPigGoldSaveData().ShowUI(0, 0);
			}
		}
		else
		{
			string txt = string.Format(WordGlobal.ReadText(1243), PlayerInfo.Instance.GetPigGoldSaveData().openLevel);
			ResourceLoadManager.GetInstance().ShowTips(txt);
		}
	}

	public void BtnAddGoldClick()
	{
		if (GameObject.Find("ShopUI") == null)
		{
			if (Application.internetReachability == NetworkReachability.NotReachable)
			{
				ResourceLoadManager.GetInstance().ShowTips(1118);
			}
			else
			{
				UIManager.Instance.LoadMainUI("UI/ShopUI").name = "ShopUI";
			}
		}
	}

	public void BtnFaceBookClick()
	{
		if (GameObject.Find("FaceBookLoginUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/FaceBookLoginUI", isPop: true);
		}
	}

	public void BtnDailyPuzzleClick()
	{
		if (!WordJsonDataManager.instance.IsOpenDailyPuzzle())
		{
			ResourceLoadManager.GetInstance().ShowTips(1040);
			return;
		}
		if (PlayerInfo.Instance.MaxPlayLevel <= WordGlobal.OpenDailyPuzzleLevel)
		{
			string txt = string.Format(WordGlobal.ReadText(1171), WordGlobal.OpenDailyPuzzleLevel + 1 + string.Empty);
			ResourceLoadManager.GetInstance().ShowTips(txt);
			return;
		}
		WordGlobal.LogEvent("Dailypuzzle_enter");
		Debug.Log(WordJsonDataManager.instance.GetDailyMinLevel() + " " + WordJsonDataManager.instance.GetDailyMaxLevel());
        if (PlayerInfo.Instance.GetDailyDayId() >= WordJsonDataManager.instance.GetDailyMinLevel() && PlayerInfo.Instance.GetDailyDayId() <= WordJsonDataManager.instance.GetDailyMaxLevel())
        {
			if (PlayerInfo.Instance.GetDailyPuzzleItem().IsFinished == 1)
			{
				DPCollectPicUIScript.UI_OPENTYPE = 0;
				UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCollectPicUI").name = "DPCollectPicUI";
			}
			else
			{
				ShowDailyPuMainUI();
			}
		}
		else
		{
			ResourceLoadManager.GetInstance().ShowConfirmTipsUI(ConfirmUIScript.ConfirmType.Type_Update, 1155, 1156, GoToUpdateVersion);
		}
		if (WordJsonDataManager.instance.IsOpenDailyPuzzle() && PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 0)
		{
			DPCTutorialUI.HideState = 1;
			PlayerInfo.Instance.SetDailyImageDataTutorial(1);
		}
	}

	public void BtnDailyQuestClick()
	{
		if (PlayerInfo.Instance.MaxPlayLevel < WordGlobal.OpenDailyQuestLevel)
		{
			string txt = string.Format(WordGlobal.ReadText(1193), WordGlobal.OpenDailyQuestLevel + string.Empty);
			ResourceLoadManager.GetInstance().ShowTips(txt);
			return;
		}
		TutorialDailyQuestFinish.isHide = true;
		if (GameObject.Find("DailyQuestUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/DailyQuestUI", isPop: true);
		}
	}

	public void ShowHuoDongBtn(HuoDongBtnType btn_type, bool isShow)
	{
		if ((!IsHuoDongOpenByLevel(btn_type) && isShow) || GetHuoDongBtnGo(btn_type).activeInHierarchy == isShow)
		{
			return;
		}
		if (isShow)
		{
			switch (btn_type)
			{
			case HuoDongBtnType.HuoDongGardon:
				PlayerInfo.Instance.GetHuoDongGardonSaveData().SetTutorialOpen();
				break;
			case HuoDongBtnType.HuoDongSun:
				PlayerInfo.Instance.GetHuoDongSunSaveData().SetTutorialOpen();
				break;
			case HuoDongBtnType.HuoDongWordFind:
				PlayerInfo.Instance.GetHuoDongWordFindSaveData().SetTutorialOpen();
				break;
			}
		}
		//Debug.Log("hide HuoDong btn");
		//GetHuoDongBtnGo(btn_type).SetActive(isShow);
		isUpdateHuoDongBtns = true;
	}

	public bool IsHuoDongOpenByLevel(HuoDongBtnType btn_type)
	{
		int num = 0;
		switch (btn_type)
		{
		case HuoDongBtnType.HuoDong:
			num = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 1);
			break;
		case HuoDongBtnType.HuoDongGardon:
			num = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 2);
			break;
		case HuoDongBtnType.HuoDongSun:
			num = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 3);
			break;
		case HuoDongBtnType.HuoDongWordFind:
			num = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 5);
			break;
		case HuoDongBtnType.HuoDongTournament:
			num = EventDataManagerCsv.instance.GetIdByMaxLevel(PlayerInfo.Instance.MaxPlayLevel, 4);
			break;
		}
		if (num > 0)
		{
			return true;
		}
		return false;
	}

	private void UpdateHuoDongBtns()
	{
		if (fb_loginstate != PlayerInfo.Instance.LoggedFaceBook)
		{
			if (PlayerInfo.Instance.LoggedFaceBook == 1)
			{
				hd_btn_base_y = -171f;
			}
			else
			{
				hd_btn_base_y = -278f;
			}
			isUpdateHuoDongBtns = true;
			fb_loginstate = PlayerInfo.Instance.LoggedFaceBook;
		}
		if (isUpdateHuoDongBtns)
		{
			float num = 152f;
			int num2 = 0;
			if (HuoDongBtns_PosIdx == null)
			{
				HuoDongBtns_PosIdx = new int[HuoDongBtns.Length];
			}
			for (int i = 0; i < HuoDongBtns.Length; i++)
			{
				if (HuoDongBtns[i].activeInHierarchy)
				{
					RectTransform obj = HuoDongBtns[i].transform as RectTransform;
					Vector2 anchoredPosition = (HuoDongBtns[i].transform as RectTransform).anchoredPosition;
					obj.anchoredPosition = new Vector2(anchoredPosition.x, hd_btn_base_y - (float)num2 * num);
					HuoDongBtns_PosIdx[i] = num2;
					num2++;
				}
			}
			isUpdateHuoDongBtns = false;
		}
		if (isUpdateLeftBtns)
		{
			float num3 = 152f;
			int num4 = 0;
			for (int j = 0; j < LeftBtns.Length; j++)
			{
				if (LeftBtns[j].activeInHierarchy)
				{
					RectTransform obj2 = LeftBtns[j].transform as RectTransform;
					Vector2 anchoredPosition2 = (LeftBtns[j].transform as RectTransform).anchoredPosition;
					obj2.anchoredPosition = new Vector2(anchoredPosition2.x, left_btn_base_y - (float)num4 * num3);
					num4++;
				}
			}
			isUpdateLeftBtns = false;
		}
		if (PlayerInfo.Instance.IsShowVip() != BtnVip_go.activeInHierarchy)
		{
			WordGlobal.isUpdateLevelMapLeftBtn = true;
		}
	}

	public void ShowLeftBtn(LeftBtnType btn_type, bool isShow)
	{
		if ((true || !isShow) && GetLeftBtnGo(btn_type).activeInHierarchy != isShow)
		{
			GetLeftBtnGo(btn_type).SetActive(isShow);
			isUpdateLeftBtns = true;
		}
	}

	public GameObject GetLeftBtnGo(LeftBtnType btn_type)
	{
		switch (btn_type)
		{
		case LeftBtnType.VIP:
			return LeftBtns[0];
		case LeftBtnType.LuckyDraw:
			return LeftBtns[1];
		default:
			return LeftBtns[0];
		}
	}

	public GameObject GetHuoDongBtnGo(HuoDongBtnType btn_type)
	{
		switch (btn_type)
		{
		case HuoDongBtnType.HuoDong:
			return HuoDongBtns[0];
		case HuoDongBtnType.HuoDongGardon:
			return HuoDongBtns[1];
		case HuoDongBtnType.HuoDongSun:
			return HuoDongBtns[2];
		case HuoDongBtnType.HuoDongTournament:
			return HuoDongBtns[3];
		case HuoDongBtnType.HuoDongWordFind:
			return HuoDongBtns[4];
		default:
			return HuoDongBtns[0];
		}
	}

	public int GetHuoDongBtnPosIdx(HuoDongBtnType btn_type)
	{
		switch (btn_type)
		{
		case HuoDongBtnType.HuoDong:
			return HuoDongBtns_PosIdx[0];
		case HuoDongBtnType.HuoDongGardon:
			return HuoDongBtns_PosIdx[1];
		case HuoDongBtnType.HuoDongSun:
			return HuoDongBtns_PosIdx[2];
		case HuoDongBtnType.HuoDongTournament:
			return HuoDongBtns_PosIdx[3];
		case HuoDongBtnType.HuoDongWordFind:
			return HuoDongBtns_PosIdx[4];
		default:
			return HuoDongBtns_PosIdx[0];
		}
	}

	public void GoToUpdateVersion()
	{
		MS_Interface.Current.OnRate();
	}

	public void OnDestroy()
	{
		Instance = null;
		isShowAddGoldAnim = false;
	}
}
