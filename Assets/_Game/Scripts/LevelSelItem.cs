using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelItem : MonoBehaviour, PointAnimInfo
{
	public Text CurLevel;

	public Image HeadIcon;

	public Image hd_iconImg;

	public GameObject vip_go;

	public GameObject lock_go;

	public Image unlockImg;

	public GameObject[] other_gos;

	private int curLevel;

	private int HD_refreshIndex = -1;

	private float delaytoshowEff;

	private GameObject eff_go;

	private void Start()
	{
		if (PlayerInfo.Instance.FBHeadIconSpr == null)
		{
			Debug.Log("Load FB Icon");
			//StartCoroutine(WordGlobal.LoadFBHeadIcon(PlayerInfo.Instance.FaceBookID, HeadIcon));
		}
		else
		{
			HeadIcon.sprite = PlayerInfo.Instance.FBHeadIconSpr;
		}
		if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().getHalfPayAllFirstLetterLastTime() > 0.0)
		{
			vip_go.SetActive(value: true);
		}
		else
		{
			vip_go.SetActive(value: false);
		}
		RefreshUI();
	}

	private void Update()
	{
		if (HD_refreshIndex != WordGlobal.HuoDongRefreshLevelMapIconIndex)
		{
			HD_refreshIndex = WordGlobal.HuoDongRefreshLevelMapIconIndex;
		}
		if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().getHalfPayAllFirstLetterLastTime() > 0.0)
		{
			vip_go.SetActive(value: true);
		}
		else
		{
			vip_go.SetActive(value: false);
		}
		if (delaytoshowEff > 0f)
		{
			delaytoshowEff -= Time.deltaTime;
			if (delaytoshowEff <= 0f)
			{
				eff_go.SetActive(value: true);
			}
		}
	}

	public void RefreshUI()
	{
		curLevel = ((PlayerInfo.Instance.MaxPlayLevel <= MapDataManager.instance.GetMaxLevel()) ? PlayerInfo.Instance.MaxPlayLevel : MapDataManager.instance.GetMaxLevel());
		CurLevel.text = curLevel.ToString();
	}

	public void OnItemClick()
	{
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.ShowMainUI(curLevel);
		}
	}

	public void OnDestroy()
	{
	}

	public void LockedItem()
	{
		lock_go.SetActive(value: true);
		for (int i = 0; i < other_gos.Length; i++)
		{
			other_gos[i].SetActive(value: false);
		}
		unlockImg.gameObject.SetActive(value: false);
		Image image = unlockImg;
		Color color = unlockImg.color;
		float r = color.r;
		Color color2 = unlockImg.color;
		float g = color2.g;
		Color color3 = unlockImg.color;
		image.color = new Color(r, g, color3.b, 0f);
	}

	public void UnLockItemAnim(float delay_time, float time)
	{
		unlockImg.gameObject.SetActive(value: true);
		unlockImg.DOFade(1f, time).SetDelay(delay_time).OnComplete(delegate
		{
			for (int i = 0; i < other_gos.Length; i++)
			{
				other_gos[i].SetActive(value: true);
			}
		});
		string empty = string.Empty;
		empty = "LinkIconItemEff_small";
		delaytoshowEff = delay_time;
		string path = "UI/LevelMap/" + empty;
		eff_go = ResourceLoadManager.GetInstance().LoadUI(path, base.transform);
		eff_go.transform.localPosition = Vector3.zero;
		eff_go.transform.localScale = Vector3.one;
		(eff_go.transform as RectTransform).anchoredPosition = Vector2.zero;
		if (delaytoshowEff > 0f)
		{
			eff_go.SetActive(value: false);
		}
		else
		{
			eff_go.SetActive(value: true);
		}
	}

	public void UnLockItemNow()
	{
		if (!(unlockImg == null))
		{
			unlockImg.gameObject.SetActive(value: true);
			unlockImg.color = Color.white;
			unlockImg.DOKill();
			for (int i = 0; i < other_gos.Length; i++)
			{
				other_gos[i].SetActive(value: true);
			}
			if (eff_go != null)
			{
				eff_go.SetActive(value: false);
			}
			delaytoshowEff = 0f;
		}
	}
}
