using System.Collections.Generic;
using UnityEngine;

public class LevelStyleItem : MonoBehaviour
{
	public GameObject PointsObj;

	public List<PointItem> PointList;

	public MapDataCsv CurMapData;

	public float ItemStartPosY;

	public float ItemEndPosY;

	private Transform _myTransform;

	private void Start()
	{
		_myTransform = base.transform;
	}

	public void InitData()
	{
		PointList = new List<PointItem>();
		for (int i = 0; i < PointsObj.transform.childCount; i++)
		{
			Transform child = PointsObj.transform.GetChild(i);
			GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI("UI/LevelMap/PointItem", child);
			PointItem component = gameObject.GetComponent<PointItem>();
			gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
			gameObject.transform.localRotation = base.transform.localRotation;
			int num = CurMapData.LevelStartNum + i;
			component.SetData(num == PlayerInfo.Instance.MaxPlayLevel, num);
			PointList.Add(component);
		}
	}

	public void SetAllPointEffect(bool isShow)
	{
		for (int i = 0; i < PointList.Count; i++)
		{
			PointList[i].SetCoinShow(isShow);
		}
	}

	public void SetData(MapDataCsv data, float itemStartY, float itemEndY)
	{
		CurMapData = data;
		ItemStartPosY = itemStartY;
		ItemEndPosY = itemEndY;
		InitData();
	}

	public Transform GetFristPoint()
	{
		return PointList[0].transform.parent;
	}

	public Transform GetLastPoint()
	{
		return PointList[PointList.Count - 1].transform.parent;
	}

	public Vector3 GetPointPos(int level)
	{
		Transform pointByLevel = GetPointByLevel(level);
		if (pointByLevel != null)
		{
			return pointByLevel.GetComponent<RectTransform>().anchoredPosition;
		}
		return Vector3.zero;
	}

	public Transform GetPointByLevel(int level)
	{
		int pointIndexByLevel = GetPointIndexByLevel(level);
		if (pointIndexByLevel < 0)
		{
			return null;
		}
		return PointList[pointIndexByLevel].transform.parent;
	}

	public int GetPointIndexByLevel(int level)
	{
		int result = -1;
		if (level < CurMapData.LevelStartNum || level > CurMapData.LevelEndNum)
		{
			WordGlobal.DebugLogError("当前关卡不在此地图中！！！");
		}
		else
		{
			result = level - CurMapData.LevelStartNum;
		}
		return result;
	}

	public PointItem GetPointItem(int level)
	{
		int pointIndexByLevel = GetPointIndexByLevel(level);
		if (pointIndexByLevel < 0)
		{
			return null;
		}
		return PointList[pointIndexByLevel];
	}

	private void Update()
	{
	}
}
