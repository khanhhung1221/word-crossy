using UnityEngine;
using UnityEngine.UI;

public class LinkButton : MonoBehaviour
{
	private Text linkText;

	public Image LineImg;

	private void Awake()
	{
	}

	private void Start()
	{
		linkText = base.transform.GetComponent<Text>();
		SetLine();
	}

	private void SetLine()
	{
		if (!(linkText == null))
		{
			RectTransform rectTransform = LineImg.rectTransform;
			float preferredWidth = linkText.preferredWidth;
			rectTransform.sizeDelta = new Vector2(preferredWidth, 1f);
			rectTransform.anchoredPosition = new Vector3(0f, 6f, 0f);
		}
	}

	public void SetText(string str)
	{
		base.transform.GetComponent<Text>().text = str;
		SetLine();
	}

	public string GetText()
	{
		return linkText.text;
	}
}
