using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LinkIconItem : MonoBehaviour, PointAnimInfo
{
	public GameObject Locked;

	public GameObject IconBg_BigChapter;

	public GameObject IconBg_Small;

	public Image ChapterNameBg;

	public Text ChapterName;

	public Image Icon;

	public Animator CurAnim;

	public int CurSmallChapterIndex;

	private float startPosY = 25f;

	private RectTransform ChapterNameRect;

	private bool IsBigChapter;

	private Vector2 defaultPos;

	private bool isUnlockChapter;

	private float frameCount;

	private float delaytoshowEff;

	private GameObject eff_go;

	private void Start()
	{
	}

	private void Update()
	{
		if (isUnlockChapter)
		{
			frameCount += Time.deltaTime;
			if ((double)frameCount > 1.5)
			{
				isUnlockChapter = false;
				frameCount = 0f;
				Locked.SetActive(value: false);
			}
		}
		if (delaytoshowEff > 0f)
		{
			delaytoshowEff -= Time.deltaTime;
			if (delaytoshowEff <= 0f)
			{
				eff_go.SetActive(value: true);
			}
		}
	}

	public void LockedItem()
	{
		Locked.SetActive(value: true);
	}

	public void UnLockItemAnim(float delayTime, float time)
	{
		Locked.SetActive(value: false);
		string empty = string.Empty;
		delaytoshowEff = delayTime;
		empty = "LinkIconItemEff_big";
		string path = "UI/LevelMap/" + empty;
		eff_go = ResourceLoadManager.GetInstance().LoadUI(path, base.transform);
		eff_go.transform.localPosition = Vector3.zero;
		eff_go.transform.localScale = Vector3.one;
		(eff_go.transform as RectTransform).anchoredPosition = Vector2.zero;
		if (delaytoshowEff > 0f)
		{
			eff_go.SetActive(value: false);
		}
		else
		{
			eff_go.SetActive(value: true);
		}
	}

	public void SetData(int index, int maxLevelIndex)
	{
		CurSmallChapterIndex = index;
		bool active = maxLevelIndex < index;
		Locked.SetActive(active);
		MapDataCsv mapDataByIndex = MapDataManager.instance.GetMapDataByIndex(index);
		ChapterName.text = mapDataByIndex.GetChapterName();
		IsBigChapter = MapDataManager.instance.IndexIsBigChapter(index);
		ChapterNameBg.gameObject.SetActive(IsBigChapter);
		ChapterName.gameObject.SetActive(IsBigChapter);
		ChapterNameRect = ChapterName.GetComponent<RectTransform>();
		IconBg_BigChapter.SetActive(IsBigChapter);
		IconBg_Small.SetActive(!IsBigChapter);
		defaultPos = ChapterName.transform.localPosition;
		string str = "Image/LevelMap/";
		string lIcon = mapDataByIndex.LIcon;
		Icon.sprite = ResourceLoadManager.GetInstance().LoadSpriteByPath(str + lIcon, ResourceLoadManager.SP_LEVELMAP_TYPE.LICON);
	}

	public void ShowLockIconAnim()
	{
		isUnlockChapter = true;
		Locked.SetActive(value: true);
		CurAnim.SetTrigger("open_start");
	}

	public void SetEffectShow(bool isShow)
	{
	}

	public void TweenerKill()
	{
		ChapterName.transform.DOKill();
		ChapterNameBg.gameObject.SetActive(IsBigChapter);
		ChapterName.gameObject.SetActive(IsBigChapter);
		ChapterName.transform.localPosition = defaultPos;
	}

	public void OnItemClick()
	{
		ChapterName.transform.localPosition = defaultPos;
		ChapterName.transform.DOKill();
		ChapterNameBg.gameObject.SetActive(value: true);
		ChapterName.gameObject.SetActive(value: true);
		Vector2 endPos = defaultPos;
		Vector2 vector = new Vector2(0f, endPos.y + startPosY);
		Vector2 v = new Vector2(0f, endPos.y + startPosY / 3f);
		Vector2 vector2 = new Vector2(0f, endPos.y - startPosY / 3f);
		float y = endPos.y + startPosY;
		float num = endPos.y + startPosY / 3f;
		float endValue = endPos.y - startPosY / 3f;
		float time = 0.7f;
		ChapterName.color = Color.white;
		if (IsBigChapter)
		{
			ChapterName.transform.localPosition = v;
			ChapterName.transform.DOLocalMoveY(endValue, time).OnComplete(delegate
			{
				ChapterName.transform.DOLocalMoveY(endPos.y, time).OnComplete(delegate
				{
					ChapterNameBg.gameObject.SetActive(IsBigChapter);
					ChapterName.gameObject.SetActive(IsBigChapter);
					ChapterName.transform.localPosition = defaultPos;
				});
			});
			ChapterName.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
			ChapterName.transform.DOScale(1f, time);
		}
		else
		{
			ChapterName.transform.localPosition = v;
			ChapterName.transform.DOLocalMoveY(endPos.y, time).OnComplete(delegate
			{
				ChapterName.DOFade(0f, time);
				ChapterName.transform.DOLocalMoveY(y, time).OnComplete(delegate
				{
					ChapterNameBg.gameObject.SetActive(IsBigChapter);
					ChapterName.gameObject.SetActive(IsBigChapter);
					ChapterName.transform.localPosition = defaultPos;
				});
			});
		}
	}

	public void UnLockItemNow()
	{
		if (Locked != null)
		{
			Locked.SetActive(value: false);
		}
		if (eff_go != null)
		{
			eff_go.SetActive(value: false);
		}
		delaytoshowEff = 0f;
	}
}
