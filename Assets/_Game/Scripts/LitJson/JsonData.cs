using System;
using System.Collections;
using System.Collections.Generic;

namespace LitJson
{
	public class JsonData : IList, IDictionary, IEnumerable, ICollection
	{
		private IList<JsonData> inst_array;

		private bool inst_boolean;

		private double inst_double;

		private int inst_int;

		private long inst_long;

		private IDictionary<string, JsonData> inst_object;

		private string inst_string;

		private string json;

		private JsonType type;

		private IList<KeyValuePair<string, JsonData>> object_list;

		object IDictionary.this[object key]
		{
			get
			{
				return EnsureDictionary()[key];
			}
			set
			{
				if (!(key is string))
				{
					throw new ArgumentException("The key has to be a string");
				}
				JsonData value2 = ToJsonData(value);
				this[(string)key] = value2;
			}
		}

		object IList.this[int index]
		{
			get
			{
				return EnsureList()[index];
			}
			set
			{
				EnsureList();
				JsonData jsonData2 = this[index] = ToJsonData(value);
			}
		}

		int ICollection.Count => Count;

		bool ICollection.IsSynchronized => EnsureCollection().IsSynchronized;

		object ICollection.SyncRoot => EnsureCollection().SyncRoot;

		bool IDictionary.IsFixedSize => EnsureDictionary().IsFixedSize;

		bool IDictionary.IsReadOnly => EnsureDictionary().IsReadOnly;

		ICollection IDictionary.Keys
		{
			get
			{
				EnsureDictionary();
				IList<string> list = new List<string>();
				foreach (KeyValuePair<string, JsonData> item in object_list)
				{
					list.Add(item.Key);
				}
				return (ICollection)list;
			}
		}

		ICollection IDictionary.Values
		{
			get
			{
				EnsureDictionary();
				IList<JsonData> list = new List<JsonData>();
				foreach (KeyValuePair<string, JsonData> item in object_list)
				{
					list.Add(item.Value);
				}
				return (ICollection)list;
			}
		}

		bool IList.IsFixedSize => EnsureList().IsFixedSize;

		bool IList.IsReadOnly => EnsureList().IsReadOnly;

		public int Count => EnsureCollection().Count;

		public bool IsArray => type == JsonType.Array;

		public bool IsBoolean => type == JsonType.Boolean;

		public bool IsDouble => type == JsonType.Double;

		public bool IsInt => type == JsonType.Int;

		public bool IsLong => type == JsonType.Long;

		public bool IsObject => type == JsonType.Object;

		public bool IsString => type == JsonType.String;

		public JsonData this[string prop_name]
		{
			get
			{
				EnsureDictionary();
				return inst_object[prop_name];
			}
			set
			{
				EnsureDictionary();
				KeyValuePair<string, JsonData> keyValuePair = new KeyValuePair<string, JsonData>(prop_name, value);
				if (inst_object.ContainsKey(prop_name))
				{
					for (int i = 0; i < object_list.Count; i++)
					{
						if (object_list[i].Key == prop_name)
						{
							object_list[i] = keyValuePair;
							break;
						}
					}
				}
				else
				{
					object_list.Add(keyValuePair);
				}
				inst_object[prop_name] = value;
				json = null;
			}
		}

		public JsonData this[int index]
		{
			get
			{
				EnsureCollection();
				if (type == JsonType.Array)
				{
					return inst_array[index];
				}
				return object_list[index].Value;
			}
			set
			{
				EnsureCollection();
				if (type == JsonType.Array)
				{
					inst_array[index] = value;
				}
				else
				{
					KeyValuePair<string, JsonData> keyValuePair = object_list[index];
					KeyValuePair<string, JsonData> value2 = new KeyValuePair<string, JsonData>(keyValuePair.Key, value);
					object_list[index] = value2;
					inst_object[keyValuePair.Key] = value;
				}
				json = null;
			}
		}

		public JsonData()
		{
			type = JsonType.None;
		}

		public JsonData(bool boolean)
		{
			JsonDataBool(boolean);
		}

		public JsonData(double number)
		{
			JsonDataDouble(number);
		}

		public JsonData(int number)
		{
			JsonDataInt(number);
		}

		public JsonData(long number)
		{
			JsonDataLong(number);
		}

		public JsonData(string str)
		{
			JsonDataString(str);
		}

		public JsonData(object obj)
		{
			if (obj is bool)
			{
				JsonDataBool((bool)obj);
				return;
			}
			if (obj is double)
			{
				JsonDataDouble((double)obj);
				return;
			}
			if (obj is int)
			{
				JsonDataInt((int)obj);
				return;
			}
			if (obj is long)
			{
				JsonDataLong((long)obj);
				return;
			}
			if (obj is string)
			{
				JsonDataString((string)obj);
				return;
			}
			if (obj is IList)
			{
				JsonDataList((IList)obj);
				return;
			}
			if (obj is IDictionary)
			{
				JsonDataDictionary((IDictionary)obj);
				return;
			}
			throw new ArgumentException("Unable to wrap the given object with JsonData");
		}

		public JsonType GetJsonType()
		{
			return type;
		}

		private void JsonDataBool(bool boolean)
		{
			type = JsonType.Boolean;
			inst_boolean = boolean;
		}

		private void JsonDataDouble(double number)
		{
			type = JsonType.Double;
			inst_double = number;
		}

		private void JsonDataInt(int number)
		{
			type = JsonType.Int;
			inst_int = number;
		}

		private void JsonDataLong(long number)
		{
			type = JsonType.Long;
			inst_long = number;
		}

		private void JsonDataString(string str)
		{
			type = JsonType.String;
			inst_string = str;
		}

		private void JsonDataList(IList obj)
		{
			type = JsonType.Array;
			List<object> list = obj as List<object>;
			inst_array = new List<JsonData>(list.Count);
			foreach (object item in list)
			{
				inst_array.Add(ToJsonData(item));
			}
		}

		private void JsonDataDictionary(IDictionary obj)
		{
			type = JsonType.Object;
			Dictionary<string, object> dictionary = obj as Dictionary<string, object>;
			inst_object = new Dictionary<string, JsonData>(dictionary.Count);
			object_list = new List<KeyValuePair<string, JsonData>>(dictionary.Count);
			foreach (KeyValuePair<string, object> item2 in dictionary)
			{
				JsonData value = ToJsonData(item2.Value);
				KeyValuePair<string, JsonData> item = new KeyValuePair<string, JsonData>(item2.Key, value);
				inst_object.Add(item);
				object_list.Add(item);
			}
		}

		private object ToObject(object obj)
		{
			if (obj is JsonData)
			{
				JsonData jsonData = obj as JsonData;
				switch (jsonData.type)
				{
				case JsonType.Boolean:
					return jsonData.inst_boolean;
				case JsonType.Double:
					return jsonData.inst_double;
				case JsonType.Int:
					return jsonData.inst_int;
				case JsonType.Long:
					return jsonData.inst_long;
				case JsonType.String:
					return jsonData.inst_string;
				case JsonType.Array:
				{
					List<object> list = new List<object>();
					{
						foreach (JsonData item in jsonData.inst_array)
						{
							list.Add(ToObject(item));
						}
						return list;
					}
				}
				case JsonType.Object:
				{
					Dictionary<string, object> dictionary = new Dictionary<string, object>();
					{
						foreach (KeyValuePair<string, JsonData> item2 in jsonData.inst_object)
						{
							dictionary[item2.Key] = ToObject(item2.Value);
						}
						return dictionary;
					}
				}
				default:
					return null;
				}
			}
			return obj;
		}

		public static implicit operator JsonData(bool data)
		{
			return new JsonData(data);
		}

		public static implicit operator JsonData(double data)
		{
			return new JsonData(data);
		}

		public static implicit operator JsonData(int data)
		{
			return new JsonData(data);
		}

		public static implicit operator JsonData(long data)
		{
			return new JsonData(data);
		}

		public static implicit operator JsonData(string data)
		{
			return new JsonData(data);
		}

		public static explicit operator bool(JsonData data)
		{
			if (data.type != JsonType.Boolean)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold a double");
			}
			return data.inst_boolean;
		}

		public static explicit operator double(JsonData data)
		{
			if (data.type != JsonType.Double)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold a double");
			}
			return data.inst_double;
		}

		public static explicit operator int(JsonData data)
		{
			if (data.type != JsonType.Int)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold an int");
			}
			return data.inst_int;
		}

		public static explicit operator long(JsonData data)
		{
			if (data.type != JsonType.Long)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold an int");
			}
			return data.inst_long;
		}

		public static explicit operator string(JsonData data)
		{
			if (data.type != JsonType.String)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold a string");
			}
			return data.inst_string;
		}

		public int Add(object value)
		{
			JsonData value2 = ToJsonData(value);
			json = null;
			return EnsureList().Add(value2);
		}

		public void Clear()
		{
			json = null;
			if (IsObject)
			{
				((IDictionary)this).Clear();
			}
			else if (IsArray)
			{
				((IList)this).Clear();
			}
		}

		public bool Contains(object key)
		{
			return EnsureDictionary().Contains(key);
		}

		public IDictionaryEnumerator Enumerator()
		{
			return EnsureDictionary().GetEnumerator();
		}

		public bool Equals(JsonData x)
		{
			if (x == null)
			{
				return false;
			}
			if (x.type != type)
			{
				return false;
			}
			switch (type)
			{
			case JsonType.None:
				return true;
			case JsonType.Object:
				return inst_object.Equals(x.inst_object);
			case JsonType.Array:
				return inst_array.Equals(x.inst_array);
			case JsonType.String:
				return inst_string.Equals(x.inst_string);
			case JsonType.Int:
				return inst_int.Equals(x.inst_int);
			case JsonType.Long:
				return inst_long.Equals(x.inst_long);
			case JsonType.Double:
				return inst_double.Equals(x.inst_double);
			case JsonType.Boolean:
				return inst_boolean.Equals(x.inst_boolean);
			default:
				return false;
			}
		}

		public string ToJson()
		{
			object obj = ToObject(this);
			json = MiniJSON.Serialize(obj);
			return json;
		}

		public override string ToString()
		{
			switch (type)
			{
			case JsonType.Array:
				return "JsonData array";
			case JsonType.Boolean:
				return inst_boolean.ToString();
			case JsonType.Double:
				return inst_double.ToString();
			case JsonType.Int:
				return inst_int.ToString();
			case JsonType.Long:
				return inst_long.ToString();
			case JsonType.Object:
				return "JsonData object";
			case JsonType.String:
				return inst_string;
			default:
				return "Uninitialized JsonData";
			}
		}

		public static bool IsNull(JsonData jd)
		{
			if (jd == null || jd.type == JsonType.None)
			{
				return true;
			}
			return false;
		}

		private ICollection EnsureCollection()
		{
			if (type == JsonType.Array)
			{
				return (ICollection)inst_array;
			}
			if (type == JsonType.Object)
			{
				return (ICollection)inst_object;
			}
			throw new InvalidOperationException("The JsonData instance has to be initialized first");
		}

		private IDictionary EnsureDictionary()
		{
			if (type == JsonType.Object)
			{
				return (IDictionary)inst_object;
			}
			if (type != 0)
			{
				throw new InvalidOperationException("Instance of JsonData is not a dictionary");
			}
			type = JsonType.Object;
			inst_object = new Dictionary<string, JsonData>();
			object_list = new List<KeyValuePair<string, JsonData>>();
			return (IDictionary)inst_object;
		}

		private IList EnsureList()
		{
			if (type == JsonType.Array)
			{
				return (IList)inst_array;
			}
			if (type != 0)
			{
				throw new InvalidOperationException("Instance of JsonData is not a list");
			}
			type = JsonType.Array;
			inst_array = new List<JsonData>();
			return (IList)inst_array;
		}

		private JsonData ToJsonData(object obj)
		{
			if (obj == null)
			{
				return null;
			}
			if (obj is JsonData)
			{
				return (JsonData)obj;
			}
			return new JsonData(obj);
		}

		void ICollection.CopyTo(Array array, int index)
		{
			EnsureCollection().CopyTo(array, index);
		}

		void IDictionary.Add(object key, object value)
		{
			JsonData value2 = ToJsonData(value);
			EnsureDictionary().Add(key, value2);
			KeyValuePair<string, JsonData> item = new KeyValuePair<string, JsonData>((string)key, value2);
			object_list.Add(item);
			json = null;
		}

		void IDictionary.Clear()
		{
			EnsureDictionary().Clear();
			object_list.Clear();
			json = null;
		}

		bool IDictionary.Contains(object key)
		{
			return EnsureDictionary().Contains(key);
		}

		IDictionaryEnumerator IDictionary.GetEnumerator()
		{
			EnsureDictionary();
			return new OrderedDictionaryEnumerator(object_list.GetEnumerator());
		}

		void IDictionary.Remove(object key)
		{
			EnsureDictionary().Remove(key);
			for (int i = 0; i < object_list.Count; i++)
			{
				if (object_list[i].Key == (string)key)
				{
					object_list.RemoveAt(i);
					break;
				}
			}
			json = null;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return EnsureCollection().GetEnumerator();
		}

		int IList.Add(object value)
		{
			return Add(value);
		}

		void IList.Clear()
		{
			EnsureList().Clear();
			json = null;
		}

		bool IList.Contains(object value)
		{
			return EnsureList().Contains(value);
		}

		int IList.IndexOf(object value)
		{
			return EnsureList().IndexOf(value);
		}

		void IList.Insert(int index, object value)
		{
			EnsureList().Insert(index, value);
			json = null;
		}

		void IList.Remove(object value)
		{
			EnsureList().Remove(value);
			json = null;
		}

		void IList.RemoveAt(int index)
		{
			EnsureList().RemoveAt(index);
			json = null;
		}
	}
}
