using UnityEngine;

public class LittleTipsAnim : MonoBehaviour
{
	public Animator anim;

	public int type;

	private RectTransform rectTran;

	private float base_y;

	private float up_end_v;

	private bool isAnim = true;

	private int anim_idx;

	private float delay_time;

	private void OnEnable()
	{
		if (anim != null)
		{
			(anim.transform as RectTransform).anchoredPosition = Vector2.zero;
		}
	}

	private void Start()
	{
		rectTran = (base.transform as RectTransform);
		Vector2 anchoredPosition = rectTran.anchoredPosition;
		base_y = anchoredPosition.y;
		up_end_v = base_y + 60f;
		anim_idx = 0;
	}

	private void Update()
	{
	}
}
