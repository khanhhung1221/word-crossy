using System;

[Serializable]
public class LittleTipsSaveData
{
	public int dailyPuzzle_open;

	public int dailyPuzzle_openDay;

	public int dailySearch_open;

	public int dailySearch_openDay;

	public int dailyQuest_open;

	public int dailyQuest_openDay;

	public void Init()
	{
		if (DateTime.Now.Day != dailyPuzzle_openDay)
		{
			dailyPuzzle_open = 0;
		}
		if (DateTime.Now.Day != dailySearch_openDay)
		{
			dailySearch_open = 0;
		}
		if (DateTime.Now.Day != dailyQuest_openDay)
		{
			dailyQuest_open = 0;
		}
	}

	public void SetDailyQuestOpen()
	{
		dailyQuest_open = 1;
		dailyQuest_openDay = DateTime.Now.Day;
		PlayerInfo.Instance.SaveLittleTipsData();
	}

	public void SetDailyPuzzleOpen()
	{
		dailyPuzzle_open = 1;
		dailyPuzzle_openDay = DateTime.Now.Day;
		PlayerInfo.Instance.SaveLittleTipsData();
	}

	public void SetDailySearchOpen()
	{
		dailySearch_open = 1;
		dailySearch_openDay = DateTime.Now.Day;
		PlayerInfo.Instance.SaveLittleTipsData();
	}
}
