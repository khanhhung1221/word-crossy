using UnityEngine;
using UnityEngine.UI;

public class LocalHostScript : MonoBehaviour
{
	public GameObject m_ObjLocal;

	public Text info_txt;

	public string str = "LocalHost";

	private void Start()
	{
	}

	private void Awake()
	{
		m_ObjLocal.SetActive(WordGlobal.isDebugBuild);
		str = "LocalHost ";
		if (AdsManager.Instance != null && WordGlobal.AD_Type == 1)
		{
			string text = str;
			str = text + "\nInsert screen:" + AdsManager.Instance.InterstitialStateStr + "\nincentive:" + AdsManager.Instance.IncentivizedAdState;
		}
		info_txt.text = str;
	}

	private void Update()
	{
		if (AdsManager.Instance != null && WordGlobal.AD_Type == 1)
		{
			str = "LocalHost ";
			string text = str;
			str = text + "\nInsert screen:" + AdsManager.Instance.InterstitialStateStr + "\nincentive:" + AdsManager.Instance.IncentivizedAdState;
			info_txt.text = str;
		}
	}
}
