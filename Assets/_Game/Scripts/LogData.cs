public struct LogData
{
	public string time;

	public string type;

	public string message;

	public string stackTrace;
}
