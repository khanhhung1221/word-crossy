using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class LuckyDrawDataManagerCsv : ManagerCsv
{
	private static LuckyDrawDataManagerCsv inst;

	public List<LuckyDrawDataCsv> all_datas = new List<LuckyDrawDataCsv>();

	public static LuckyDrawDataManagerCsv instance => inst ?? (inst = new LuckyDrawDataManagerCsv());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			int valueIndex = 0;
			MemoryStream stream = new MemoryStream(file);
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								LuckyDrawDataCsv item = new LuckyDrawDataCsv(array, valueIndex);
								all_datas.Add(item);
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}
}
