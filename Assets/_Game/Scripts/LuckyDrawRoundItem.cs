using UnityEngine;
using UnityEngine.UI;

public class LuckyDrawRoundItem : MonoBehaviour
{
	public Text num01_txt;

	public Outline num01_out;

	public Text num02_txt;

	public Color color_5000;

	public Color color_9000;

	private LuckyDrawDataCsv data;

	private void Start()
	{
	}

	public void ShowText(LuckyDrawDataCsv data)
	{
		this.data = data;
		num01_txt.text = data.price.ToString("###,###");
		num02_txt.text = data.price.ToString("###,###");
		if (data.price <= 2000)
		{
			num01_txt.color = Color.white;
			num01_out.enabled = false;
			num02_txt.gameObject.SetActive(value: false);
		}
		else if (data.price <= 5000)
		{
			num01_out.effectColor = color_5000;
			num01_out.enabled = true;
			num02_txt.gameObject.SetActive(value: true);
		}
		else
		{
			num01_txt.color = Color.black;
			num01_out.effectColor = color_9000;
			num01_out.enabled = true;
			num02_txt.gameObject.SetActive(value: false);
		}
		num01_txt.gameObject.SetActive(value: false);
		num02_txt.gameObject.SetActive(value: false);
	}
}
