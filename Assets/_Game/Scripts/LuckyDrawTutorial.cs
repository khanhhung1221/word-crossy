using UnityEngine;
using UnityEngine.UI;

public class LuckyDrawTutorial : MonoBehaviour
{
	public Text des_txt;

	public Text btn_txt;

	public RectTransform move_rect;

	public static bool isHide;

	private void Start()
	{
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1338);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1235);
		isHide = false;
		if (!PlayerInfo.Instance.IsShowVip())
		{
			RectTransform rectTransform = move_rect;
			Vector2 anchoredPosition = move_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, 117f);
		}
		else
		{
			RectTransform rectTransform2 = move_rect;
			Vector2 anchoredPosition2 = move_rect.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, -35f);
		}
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.GetLeftBtnGo(LevelMapUIScript.LeftBtnType.LuckyDraw).transform.SetParent(base.transform);
		}
	}

	private void Update()
	{
		if (isHide)
		{
			Object.Destroy(base.gameObject);
			isHide = false;
		}
	}

	public void BtnGotoClick()
	{
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.BtnLuckyDrawClick();
			Object.Destroy(base.gameObject);
		}
	}

	private void OnDestroy()
	{
		Image component = LevelMapUIScript.Instance.GetLeftBtnGo(LevelMapUIScript.LeftBtnType.LuckyDraw).GetComponent<Image>();
		Button component2 = LevelMapUIScript.Instance.GetLeftBtnGo(LevelMapUIScript.LeftBtnType.LuckyDraw).GetComponent<Button>();
		if (LevelMapUIScript.Instance != null && component != null && component2 != null)
		{
			LevelMapUIScript.Instance.GetLeftBtnGo(LevelMapUIScript.LeftBtnType.LuckyDraw).transform.SetParent(LevelMapUIScript.Instance.m_RtfHideBtnsTrans);
			component.enabled = true;
			component2.enabled = true;
		}
	}
}
