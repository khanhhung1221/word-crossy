using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LuckyDrawUI : MonoBehaviour
{
	public static bool isHide;

	public Transform round_tran;

	public GameObject round_upGo;

	public Image round_upImg;

	public Transform EndEff_tran;

	public GameObject NetWaiting_go;

	public GameObject PlayBtn_go;

	public Text PlayBtn_txt01;

	public Text PlayBtn_txt02;

	public GameObject CloseBtn_go;

	public Text CloseBtn_txt;

	public Text des_txt;

	public GameObject round_Itemgo;

	public Transform round_rect;

	public GoldScript gold_script;

	public RectTransform top_tran;

	public Transform goldFly_pos;

	public Transform jiantou_tran;

	public GameObject[] light_gos;

	public int light_state;

	private List<LuckyDrawRoundItem> item_list = new List<LuckyDrawRoundItem>();

	private bool _isPause = true;

	private int _last;

	private int _cyclesNum = 5;

	public float _duration = 10f;

	public AnimationCurve curve;

	private OfferItem oitem;

	private float startAnim_waitTime;

	private float startAnim_waitMaxTime = 0.1f;

	private bool isStartAnim;

	private int light_count;

	private float showGoldWaitTime;

	private int addGoldNum;

	public bool isTest;

	private int light_state2_type;

	private float light_waitTime;

	private void Start()
	{
		Init();
	}

	public void Init()
	{
		WordGlobal.ResetTopPos(top_tran);
		WordGlobal.LogEvent("LuckDraw_Open");
		round_upGo.SetActive(value: false);
		CloseBtn_go.SetActive(value: false);
		EndEff_tran.gameObject.SetActive(value: false);
		CloseBtn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1043);
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1333);
		PlayBtn_txt01.text = TextLibraryDataManagerCsv.instance.GetText(1334);

		for (int i = 0; i < LuckyDrawDataManagerCsv.instance.all_datas.Count; i++)
		{
			GameObject gameObject = Object.Instantiate(round_Itemgo);
			gameObject.transform.SetParent(round_rect);
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localScale = Vector3.one;
			float z = 105 + i * 30;
			gameObject.transform.localEulerAngles = new Vector3(0f, 0f, z);
			gameObject.SetActive(value: true);
			gameObject.transform.name = "item" + i;
			LuckyDrawRoundItem component = gameObject.GetComponent<LuckyDrawRoundItem>();
			component.ShowText(LuckyDrawDataManagerCsv.instance.all_datas[i]);
			item_list.Add(component);
		}
		if (gold_script == null)
		{
			gold_script = WordGlobal.AddGoldUI(top_tran);
			gold_script._isAddGold = true;
			gold_script.m_ShowBanner = true;
			if (gold_script.banner_go != null)
			{
				gold_script.banner_go.GetComponent<Canvas>().sortingOrder = 13;
			}
		}
		ShowStartAnim();
	}

	public void ShowStartAnim()
	{
		PlayBtn_go.GetComponent<Button>().enabled = false;
		isStartAnim = true;
		round_upImg.gameObject.SetActive(value: true);
		round_upImg.fillAmount = 1f;
		startAnim_waitTime = startAnim_waitMaxTime;
		light_count = 0;
		SetLightState(0);
	}

	public void StartDraw()
	{
		if (_isPause)
		{
			ResetEndEff();
			PlayBtn_go.SetActive(value: false);
			_isPause = false;
			int num = 0;
			int rotNum = GetRotNum();
			int num2 = 15;
			num = -(_cyclesNum * 360 + (rotNum - 1) * 30 + num2);
			Debug.Log("The degree of rotation： " + (num + _last) + "\r\nrandom results： " + rotNum);
			LuckyDrawDataCsv data = LuckyDrawDataManagerCsv.instance.all_datas[rotNum - 1];
            switch (rotNum)
            {
				case 1:
				case 4:
				case 6:
				case 11:
					PlayerInfo.Instance.AddGold(10, 20);
					break;
				case 3:
				case 8:
				case 12:
					PlayerInfo.Instance.AddGold(20, 20);
					break;
				case 2:
				case 9:
					PlayerInfo.Instance.AddGold(30, 20);
					break;

				case 5:
				case 10:
					PlayerInfo.Instance.AddGold(40, 20);
					break;


				case 7:
					PlayerInfo.Instance.AddGold(50, 20);
					break;
			}
			//PlayerInfo.Instance.AddGold(data.price, 20);
			PlayerInfo.Instance.GetPlayerOtherInfoSaveData().SetBoughtLuckyDraw();
			WordFaceBook.Instance.UpdateTrackTaskEvent("LuckyDrawSpin", data.price + string.Empty);
			WordGlobal.LogEvent("LuckDraw_Run");
			jiantou_tran.GetComponent<Animator>().Play("spin_zhizhen");
			SetLightState(3);
			round_tran.DORotate(new Vector3(0f, 0f, num + _last), _duration, RotateMode.FastBeyond360).SetEase(curve).OnComplete(delegate
			{
				_isPause = true;
				_last = 0;
				showGoldWaitTime = 0.5f;
				addGoldNum = data.price;
				Transform transform = round_upGo.transform;
				Vector3 localEulerAngles = round_tran.localEulerAngles;
				transform.localEulerAngles = new Vector3(0f, 0f, localEulerAngles.z % 30f - 30f);
				round_upImg.fillAmount = 0.92f;
				Transform endEff_tran = EndEff_tran;
				Vector3 localEulerAngles2 = round_upGo.transform.localEulerAngles;
				endEff_tran.localEulerAngles = new Vector3(0f, 0f, localEulerAngles2.z + 14f);
				EndEff_tran.gameObject.SetActive(value: true);
				round_upGo.SetActive(value: true);
				CloseBtn_go.SetActive(value: true);
				SetLightState(4);
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_LuckyDrawWin);
				if (NetWaiting_go != null)
				{
					NetWaiting_go.SetActive(value: false);
				}
			});
		}
	}

	public void ResetEndEff()
	{
		round_upGo.SetActive(value: false);
	}

	public int GetRotNum(int type = 0)
	{
		int num = 0;
		List<int> list = new List<int>();
		if (num <= 0)
		{
			for (int i = 0; i < LuckyDrawDataManagerCsv.instance.all_datas.Count; i++)
			{
				switch (type)
				{
				case 0:
					if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().LuckyDrawBuyTimes > 0)
					{
						num += LuckyDrawDataManagerCsv.instance.all_datas[i].weight;
						list.Add(LuckyDrawDataManagerCsv.instance.all_datas[i].weight);
					}
					else
					{
						num += LuckyDrawDataManagerCsv.instance.all_datas[i].first_weight;
						list.Add(LuckyDrawDataManagerCsv.instance.all_datas[i].first_weight);
					}
					break;
				case 1:
					num += LuckyDrawDataManagerCsv.instance.all_datas[i].first_weight;
					list.Add(LuckyDrawDataManagerCsv.instance.all_datas[i].first_weight);
					break;
				case 2:
					num += LuckyDrawDataManagerCsv.instance.all_datas[i].weight;
					list.Add(LuckyDrawDataManagerCsv.instance.all_datas[i].weight);
					break;
				}
			}
		}
		int num2 = Random.Range(0, num);
		int index = 0;
		for (int j = 0; j < list.Count; j++)
		{
			if (num2 - list[j] < 0)
			{
				index = j;
				break;
			}
			num2 -= list[j];
		}
		return LuckyDrawDataManagerCsv.instance.all_datas[index].id;
	}

	public void TestRotNum()
	{
		int num = 10000;
		Dictionary<int, int> dictionary = new Dictionary<int, int>();
		Debug.LogError("====================================FirstWeight====================================");
		for (int i = 0; i < num; i++)
		{
			int rotNum = GetRotNum(1);
			if (dictionary.ContainsKey(rotNum))
			{
				Dictionary<int, int> dictionary2;
				int key;
				(dictionary2 = dictionary)[key = rotNum] = dictionary2[key] + 1;
			}
			else
			{
				dictionary.Add(rotNum, 1);
			}
		}
		foreach (int key3 in dictionary.Keys)
		{
			int price = LuckyDrawDataManagerCsv.instance.all_datas[key3 - 1].price;
			Debug.LogError("key:" + key3 + " price:" + price + " val:" + dictionary[key3]);
		}
		Debug.LogError("====================================NormalWeight====================================");
		dictionary.Clear();
		for (int j = 0; j < num; j++)
		{
			int rotNum2 = GetRotNum(2);
			if (dictionary.ContainsKey(rotNum2))
			{
				Dictionary<int, int> dictionary2;
				int key2;
				(dictionary2 = dictionary)[key2 = rotNum2] = dictionary2[key2] + 1;
			}
			else
			{
				dictionary.Add(rotNum2, 1);
			}
		}
		foreach (int key4 in dictionary.Keys)
		{
			int price2 = LuckyDrawDataManagerCsv.instance.all_datas[key4 - 1].price;
			Debug.LogError("key:" + key4 + " price:" + price2 + " val:" + dictionary[key4]);
		}
	}

	private void Update()
	{
		if (isTest)
		{
			TestRotNum();
			isTest = false;
		}
		if (isHide)
		{
			BtnCloseClick();
			isHide = false;
		}
		if (startAnim_waitTime > 0f)
		{
			startAnim_waitTime -= Time.deltaTime;
			if (startAnim_waitTime <= 0f)
			{
				if (round_upImg.fillAmount > 0f)
				{
					round_upImg.fillAmount -= 0.0833333358f;
					startAnim_waitTime = startAnim_waitMaxTime;
					light_gos[light_count].SetActive(value: true);
					light_count++;
					if (light_count > 11)
					{
						light_count = 11;
					}
				}
				else
				{
					round_upImg.gameObject.SetActive(value: false);
					round_upImg.fillAmount = 0f;
					isStartAnim = false;
					SetLightState(2);
					PlayBtn_go.GetComponent<Button>().enabled = true;
				}
			}
		}
		if (showGoldWaitTime > 0f)
		{
			showGoldWaitTime -= Time.deltaTime;
			if (showGoldWaitTime <= 0f)
			{
				gold_script._isAddGold = true;
				gold_script.AddGold(addGoldNum, 1f, 1f, isAdd2PlayerInfo: false, 16);
				gold_script.CollectGoldNew(goldFly_pos.position, 1, 1f, null);
				if (LevelMapUIScript.Instance != null)
				{
					LevelMapUIScript.Instance.SetAddGoldState(isTrue: false);
				}
			}
		}
		UpdateLightState();
	}

	public void SetLightState(int state)
	{
		light_state = state;
		light_waitTime = 0f;
		if (light_state == 0)
		{
			for (int i = 0; i < light_gos.Length; i++)
			{
				light_gos[i].SetActive(value: false);
			}
		}
		else if (light_state == 3)
		{
			for (int j = 0; j < light_gos.Length; j++)
			{
				light_gos[j].SetActive(value: true);
			}
		}
	}

	public void UpdateLightState()
	{
		if (light_state == 4)
		{
			light_waitTime -= Time.deltaTime;
			if (!(light_waitTime <= 0f))
			{
				return;
			}
			light_state2_type++;
			if (light_state2_type % 2 == 0)
			{
				for (int i = 0; i < light_gos.Length; i++)
				{
					if (i % 2 == 0)
					{
						light_gos[i].SetActive(value: false);
					}
					else
					{
						light_gos[i].SetActive(value: true);
					}
				}
			}
			else
			{
				for (int j = 0; j < light_gos.Length; j++)
				{
					if (j % 2 == 0)
					{
						light_gos[j].SetActive(value: true);
					}
					else
					{
						light_gos[j].SetActive(value: false);
					}
				}
			}
			light_waitTime = 0.2f;
		}
		else
		{
			if (light_state != 2)
			{
				return;
			}
			light_waitTime -= Time.deltaTime;
			if (!(light_waitTime <= 0f))
			{
				return;
			}
			light_state2_type++;
			if (light_state2_type % 2 == 1)
			{
				for (int k = 0; k < light_gos.Length; k++)
				{
					light_gos[k].SetActive(value: false);
				}
			}
			else
			{
				for (int l = 0; l < light_gos.Length; l++)
				{
					light_gos[l].SetActive(value: true);
				}
			}
			light_waitTime = 0.3f;
		}
	}

	public void BtnCloseClick()
	{
		if (_isPause)
		{
			Object.Destroy(base.gameObject);
		}
	}

	public void BtnPlayClick()
	{
		if (isStartAnim)
		{
			return;
		}
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			ResourceLoadManager.GetInstance().ShowTips(1118);
			return;
		}

		WordGlobal.DisableBackKey = true;
		//if (NetWaiting_go != null)
		//{
		//	NetWaiting_go.SetActive(value: true);
		//}
		MS_Interface.Current.Reward_Show(delegate (bool result)
		{
			if (result)
            {
				WordGlobal.DisableBackKey = false;
				StartDraw();
            }
            else
            {
				WordGlobal.DisableBackKey = false;
				//if (NetWaiting_go != null)
				//{
				//	NetWaiting_go.SetActive(value: false);
				//}
			}
		});
		
	}

}
