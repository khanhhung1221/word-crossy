using LitJson;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MailUIItem : MonoBehaviour
{
	public Image IconImg;

	public Text TitleText;

	public Text ContentText;

	public Image ItemBgImg;

	public Image ItemIconImg;

	public Text ItemNum;

	private MailItemInfo mailItemInfo;

	public void BtnClaimClick()
	{
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_DailyGift);
		AcceptGift();
		ResourceLoadManager.GetInstance().ShowTips(WordGlobal.ReadText(1228));
		switch (mailItemInfo.ItemID)
		{
		case 1001:
			PlayerInfo.Instance.TipsCount += mailItemInfo.ItemNum;
			break;
		case 1002:
			PlayerInfo.Instance.ClickTipsCount += mailItemInfo.ItemNum;
			break;
		case 1003:
			PlayerInfo.Instance.FirstTipsCount += mailItemInfo.ItemNum;
			break;
		case 1004:
			PlayerInfo.Instance.AddGold(mailItemInfo.ItemNum, 10);
			break;
		}
		WordFaceBook.Instance.UpdateUserdataMill();
		MailUIScript.isRemoveMailID = mailItemInfo.MailID;
		Object.Destroy(base.gameObject);
	}

	public void SetInfo(MailItemInfo mi)
	{
		mailItemInfo = mi;
		if (mi.MailType == 0)
		{
			TitleText.text = TextLibraryDataManagerCsv.instance.GetText(1070);
			ContentText.text = TextLibraryDataManagerCsv.instance.GetText(1071);
		}
		else if (mi.MailType == 2)
		{
			TitleText.text = TextLibraryDataManagerCsv.instance.GetText(1328);
			ContentText.text = TextLibraryDataManagerCsv.instance.GetText(1329);
		}
		else
		{
			TitleText.text = TextLibraryDataManagerCsv.instance.GetText(1068);
			ContentText.text = TextLibraryDataManagerCsv.instance.GetText(1069);
		}
		ItemNum.text = "X" + mi.ItemNum.ToString();
		switch (mi.ItemID)
		{
		case 1001:
		case 1002:
		case 1003:
			ItemIconImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/" + mi.ItemID);
			ItemIconImg.gameObject.SetActive(value: true);
			break;
		case 1004:
			ItemBgImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/jinbi_dr");
			ItemIconImg.gameObject.SetActive(value: false);
			break;
		}
	}

	private void AcceptGift()
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		jsonData["key"] = mailItemInfo.MailID;
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["acceptgift"] = Callback;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "acceptgift", jsonData, dictionary);
	}

	private bool Callback(JsonData data)
	{
		return true;
	}
}
