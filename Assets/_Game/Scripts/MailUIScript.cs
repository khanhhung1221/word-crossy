using LitJson;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MailUIScript : MonoBehaviour
{
	public static string isRemoveMailID;

	public Transform UIRoot;

	public Text TipsText;

	private Dictionary<string, MailItemInfo> _mailItemList;

	private void Start()
	{
		isRemoveMailID = string.Empty;
		_mailItemList = new Dictionary<string, MailItemInfo>();
		GetGiftList();
	}

	private void Update()
	{
		if (!string.IsNullOrEmpty(isRemoveMailID))
		{
			RemoveMail(isRemoveMailID);
			isRemoveMailID = string.Empty;
		}
	}

	private void GetGiftList()
	{
		PlayerInfo.Instance.GetMailListData(CallbackGetGiftList);
		UIManager.Instance.LoadMainUI("UI/WaitLoadingUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
	}

	private bool CallbackGetGiftList(JsonData data)
	{
		if ((bool)WaitLoadingUI.instance)
		{
			WaitLoadingUI.instance.CloseUI();
		}
		JsonData jsonData = data["giftlist"];
		WordGlobal.IsShowMailBtn = (jsonData.Count > 0);
		if (_mailItemList == null)
		{
			_mailItemList = new Dictionary<string, MailItemInfo>();
		}
		for (int i = 0; i < jsonData.Count; i++)
		{
			JsonData jsonData2 = jsonData[i];
			MailItemInfo mailItemInfo = new MailItemInfo();
			mailItemInfo.MailType = (int)jsonData2["type"];
			mailItemInfo.ItemID = (int)jsonData2["toolid"];
			mailItemInfo.ItemNum = (int)jsonData2["toolnum"];
			mailItemInfo.FaceBookID = (string)jsonData2["fbid"];
			mailItemInfo.MailID = (string)jsonData2["key"];
			_mailItemList.Add(mailItemInfo.MailID, mailItemInfo);
			GameObject gameObject = UIManager.Instance.LoadMainUI("UI/MailUIItem");
			gameObject.transform.SetParent(UIRoot);
			MailUIItem component = gameObject.GetComponent<MailUIItem>();
			component.SetInfo(mailItemInfo);
		}
		if (jsonData.Count == 0)
		{
			TipsText.gameObject.SetActive(value: true);
		}
		return true;
	}

	public void RemoveMail(string mailId)
	{
		if (_mailItemList != null && _mailItemList.ContainsKey(mailId))
		{
			_mailItemList.Remove(mailId);
			if (_mailItemList.Count <= 0)
			{
				WordGlobal.IsShowMailBtn = false;
				TipsText.gameObject.SetActive(value: true);
			}
		}
	}

	public void BtnClose()
	{
		Object.Destroy(base.gameObject);
		if ((bool)WaitLoadingUI.instance)
		{
			WaitLoadingUI.instance.CloseUI();
		}
	}
}
