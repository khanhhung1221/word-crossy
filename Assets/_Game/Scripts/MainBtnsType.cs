using System;

[Serializable]
public enum MainBtnsType
{
	AllFirstLetters,
	Tips,
	TipsClick
}
