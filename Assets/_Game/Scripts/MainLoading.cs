//using com.adjust.sdk;
//using Facebook.Unity;
using LitJson;
//using MobileLocalNotifications;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainLoading : MonoBehaviour
{
	public Slider m_Slider;

	public Text m_Txt;

	private AsyncOperation async;

	private int step = 10;

	private int displayStep;

	private int stepcount = 100;

	private int _tmpFileVersion;

	private string[] tips_idxs = new string[10]
	{
		"Tips: Bind Facebook to win Free Coins.",
		"Tips: Login Facebook can save your progress.",
		"Tips: Complete Quest to earn extra Coins.",
		"Tips: Do not forget to claim Quest Reward.",
		"Tips: Get stuck? Try to use Hint items.",
		"Tips: Remember to update game weekly.",
		"Tips: Actively participate Events!",
		"Tips: Complete Levels to unlock New modes!",
		"Tips:  Win tons of Coins in Daily Puzzle.",
		"Tips: Usually use items to accelerate your progress!"
	};

	public int Step
	{
		get
		{
			return step;
		}
		set
		{
			if (step < value)
			{
				step = Mathf.Clamp(value, step, stepcount);
			}
		}
	}

	private void Start()
	{
		//LocalNotificationsManager.InitNotifications();
		//LocalNotificationsManager.ClearLocalNotificationsInPanel();
		//AdjustConfig adjustConfig = new AdjustConfig("cgrlqyyc9r0g", (!WordGlobal.isDebugBuild) ? AdjustEnvironment.Production : AdjustEnvironment.Sandbox, allowSuppressLogLevel: false);
		//adjustConfig.setLogLevel(AdjustLogLevel.Info);
		//adjustConfig.setSendInBackground(sendInBackground: false);
		//adjustConfig.setEventBufferingEnabled(eventBufferingEnabled: false);
		//adjustConfig.setLaunchDeferredDeeplink(launchDeferredDeeplink: false);
		//adjustConfig.setAppSecret(1L, 1200825504L, 1915295001L, 1871715109L, 65864767L);
		//Adjust.start(adjustConfig);

		PlayerInfo.Instance.LastPlayLevel = PlayerInfo.Instance.MaxPlayLevel;
		WordGlobal.Restarting = false;
		PlayerPrefs.SetString("Gold.OfferImg", string.Empty);
		if (PlayerInfo.Instance.FirstLogin < 5)
		{
			PlayerInfo.Instance.FirstLogin++;
		}
		if (PlayerInfo.Instance.FirstLoginDate == string.Empty)
		{
			string firstLoginDate = DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day;
			PlayerInfo.Instance.FirstLoginDate = firstLoginDate;
		}
		if (string.IsNullOrEmpty(PlayerInfo.Instance.GetDailyPuTimeMarkData().VersionCode))
		{
			PlayerInfo.Instance.SetDailyTimeMark();
		}
		else
		{
			bool flag = WordGlobal.CheckVersionNew(Application.version, PlayerInfo.Instance.GetDailyPuTimeMarkData().VersionCode);
			string[] array = Application.version.Split('.');
			int num = 0;
			if (array.Length > 0)
			{
				num = int.Parse(array[array.Length - 1]);
			}
			if (num >= 46)
			{
				string[] array2 = PlayerInfo.Instance.GetDailyPuTimeMarkData().VersionCode.Split('.');
				int num2 = 0;
				if (array2.Length > 0)
				{
					num2 = int.Parse(array2[array2.Length - 1]);
					if (num2 < 46)
					{
						PlayerInfo.Instance.IsGetDailyReward = 0;
						PlayerInfo.Instance.DailyRewardDayNew = 1;
					}
				}
			}
			if (flag || PlayerInfo.Instance.GetDailyDayId() > PlayerInfo.Instance.GetDailyPuTimeMarkData().DailyPuzzleLevel)
			{
				PlayerInfo.Instance.SetDailyTimeMark();
			}
		}
		if (PlayerInfo.Instance.showTestLog > 0)
		{
			PlayerInfo.Instance.ShowTestLog();
		}
		UserLogin();
		PlayerPrefs.SetInt("FirstLoading", 1);
		int num3 = UnityEngine.Random.Range(0, tips_idxs.Length);
		m_Txt.text = tips_idxs[num3];
		WordGlobal.InitLabel(base.transform);
		PlayerInfo.Instance.PopUpFBClickNextLvNum = 0;
		PlayerInfo.Instance.PopUpFBOnceLoginNum = 0;
	}

	private void CsvLoadFinish()
	{
		ConfigDataManager.csvFileLoadFinish = (ConfigDataManager.OnCsvFileLoadFinish)Delegate.Remove(ConfigDataManager.csvFileLoadFinish, new ConfigDataManager.OnCsvFileLoadFinish(CsvLoadFinish));
		Step = 50;
		StartCoroutine(ScrollSlider(50));
		if (PlayerInfo.Instance.FirstOpen == 1)
		{
			PlayerInfo.Instance.AddGold(200L, 8, isSetV: true);
			PlayerInfo.Instance.FirstTipsCount = 0;
			PlayerInfo.Instance.ClickTipsCount = 1;
			PlayerInfo.Instance.TipsCount = 0;
			PlayerInfo.Instance.FirstOpen = 0;
		}
		if (PlayerInfo.Instance.NotificationKey == 1)
		{
			PlayerInfo.Instance.SetRepeatingNotification();
			PlayerInfo.Instance.Set18HourNotification();
		}
		StartCoroutine(loadScene());
	}

	private IEnumerator loadScene()
	{
		async = SceneManager.LoadSceneAsync(WordGlobal.SCENE_MAIN);
		async.allowSceneActivation = false;
		yield return async;
	}

	private void Update()
	{
		m_Slider.value = (float)Step / (float)stepcount;
	}

	private IEnumerator ScrollSlider(int count)
	{
		int step2 = Step;
		for (int i = 0; i < count; i++)
		{
			Step++;
			if (Step == 97)
			{
				async.allowSceneActivation = true;
			}
			yield return new WaitForSeconds(0.1f);
		}
	}

	private void Awake()
	{
		Screen.sleepTimeout = 120;
        //Debug.unityLogger.logEnabled = WordGlobal.isDebugBuild;
        Input.multiTouchEnabled = false;
		if (GameObject.Find("PreLoadNotDestory") == null)
		{
			ResourceLoadManager.GetInstance().LoadObjByPath("UI/PreLoadNotDestory").name = "PreLoadNotDestory";
		}
        if (GameObject.Find("AdsManager") == null)
        {
            ResourceLoadManager.GetInstance().LoadObjByPath("UI/AdsManager").name = "AdsManager";
        }
        //if (!FB.IsInitialized)
        //{
        //    FB.Init(InitCallback, OnHideUnity);
        //}
        //else
        //{
        //    FB.ActivateApp();
        //}
        WordGlobal.Today_DayNum = WordGlobal.GetDateNum(DateTime.Now);
		GameDataInit();
	}

	private void InitCallback()
	{
		//if (FB.IsInitialized)
		//{
		//	FB.ActivateApp();
		//}
		//else
		//{
		//	Debug.Log("Failed to Initialize the Facebook SDK");
		//}
	}

	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown)
		{
			Time.timeScale = 0f;
		}
		else
		{
			Time.timeScale = 1f;
		}
	}

	private void GameDataInit()
	{
		ConfigDataManager.Instance.Init();
		PlayerInfo.Instance.systemConfig = new SystemConfig();
		ResourceLoadManager.GetInstance().PreInitObj("UI/MainUI");
		ResourceLoadManager.GetInstance().PreInitObj("UI/LevelMap/LevelMapUI");
		ResourceLoadManager.GetInstance().PreInitObj("FX/acg_combo01");
		ResourceLoadManager.GetInstance().PreInitObj("FX/acg_combo02");
		DPCollectPicUIScript.UI_OPENTYPE = 2;
		ResourceLoadManager.GetInstance().PreInitObj("UI/DailyPuzzle/DPCollectPicUI");
		for (int i = 1; i <= 9; i++)
		{
			ResourceLoadManager.GetInstance().PreInitObj("UI/DailyPuzzle/Eff/pintuxiao" + i);
		}
		ResourceLoadManager.GetInstance().PreInitObj("UI/DailyPuzzle/Eff/pintudajiang");
		ResourceLoadManager.GetInstance().PreInitObj("UI/DailyPuzzle/Eff/pintuxiaochuxian");
		ResourceLoadManager.GetInstance().PreInitObj("UI/DailyPuzzle/Eff/pintu");
		ResourceLoadManager.GetInstance().PreInitObj("UI/DailyPuzzle/Eff/pintutuowei");
		for (int j = 1; j < 13; j++)
		{
			string path = "UI/LevelMap/style_" + j;
			ResourceLoadManager.GetInstance().PreInitObj(path);
		}
		for (int k = 101; k < 107; k++)
		{
			string path2 = "UI/LevelMap/style_" + k;
			ResourceLoadManager.GetInstance().PreInitObj(path2);
		}
	}

	public void UserLogin()
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["userLogin"] = CallbackEvent;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "login", jsonData, dictionary);
		Invoke("LoginFailed", 1f);
	}

	public void LoginFailed()
	{
		GetBuyOffers();
		StartCoroutine(ScrollSlider(10));
		NetMill.Instance.ResetSendQueue();
	}

	private void GetCsvFiles()
	{
		JsonData jsonData = new JsonData();
		jsonData["platform"] = WordGlobal.Platform;
		jsonData["file_version"] = PlayerInfo.Instance.CsvFileVersion;
		jsonData["client_version"] = Application.version;
		Debug.Log("Hot update file_version:" + PlayerInfo.Instance.CsvFileVersion + " client_version:" + Application.version);
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["checkhotfix"] = CsvCallbackEvent;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "checkhotfix", jsonData, dictionary);
		Invoke("CsvFailed", 1f);
	}

	public void CsvFailed()
	{
		ConfigDataManager.Instance.LoadData();
	}

	private bool CsvCallbackEvent(JsonData data)
	{
		CancelInvoke("CsvFailed");
		if (data != null)
		{
			WordGlobal.DownEndFileVersion = int.Parse(data["max_file_version"].ToString());
			Debug.Log("CsvCallbackEvent WordGlobal.AD_Type:" + WordGlobal.AD_Type + " WordGlobal.DownEndFileVersion:" + WordGlobal.DownEndFileVersion);
			JsonData updateFiles = data["file_list"];
			ConfigDataManager.Instance.setUpdateFiles(updateFiles);
		}
		ConfigDataManager.Instance.LoadData();
		return true;
	}

	public void GetBuyOffers()
	{
		PlayerInfo.Instance.GetBuyOffers();
		PlayerInfo.GetOfferEvent += GetOfferListCallback;
		Invoke("GetOfferFailNext", 2f);
	}

	private void GetOfferFailNext()
	{
		if (PlayerInfo.Instance.OfferListJson != string.Empty)
		{
			JsonData jsonData = JsonMapper.ToObject(PlayerInfo.Instance.OfferListJson);
			PlayerInfo.Instance.OfferList = new List<OfferItem>();
			for (int i = 0; i < jsonData.Count; i++)
			{
				OfferItem item = new OfferItem(jsonData[i]);
				PlayerInfo.Instance.AddOfferToList(item);
			}
		}
		NetMill.Instance.ResetSendQueue();
		GetOfferNext();
	}

	private void GetOfferNext()
	{
		PlayerInfo.GetOfferEvent -= GetOfferListCallback;
		ConfigDataManager.csvFileLoadFinish = (ConfigDataManager.OnCsvFileLoadFinish)Delegate.Combine(ConfigDataManager.csvFileLoadFinish, new ConfigDataManager.OnCsvFileLoadFinish(CsvLoadFinish));
		GetCsvFiles();
		StartCoroutine(ScrollSlider(30));
		GetMailData();
	}

	private void GetOfferListCallback()
	{
		CancelInvoke("GetOfferFailNext");
		GetOfferNext();
	}

	public void GetMailData()
	{
		PlayerInfo.Instance.GetMailListData(GetMailListCallBack);
		Invoke("GetMailFailNext", 2f);
	}

	private void GetMailFailNext()
	{
		NetMill.Instance.ResetSendQueue();
		GetMailNext();
	}

	private bool GetMailListCallBack(JsonData data)
	{
		if (data == null)
		{
			return false;
		}
		JsonData jsonData = data["giftlist"];
		WordGlobal.IsShowMailBtn = (jsonData.Count > 0);
		CancelInvoke("GetMailFailNext");
		GetMailNext();
		return true;
	}

	private void GetMailNext()
	{
		StartCoroutine(ScrollSlider(40));
	}

	private bool CallbackEvent(JsonData data)
	{
		CancelInvoke("LoginFailed");
		int num = 1;
		if (data != null)
		{
			num = (int)data["errcode"];
		}
		switch (num)
		{
		case 0:
		{
			string faceBookName = (string)data["fbname"];
			string faceBookID = (string)data["fbid"];
			PlayerInfo.Instance.FaceBookName = faceBookName;
			PlayerInfo.Instance.FaceBookID = faceBookID;
			PlayerInfo.Instance.UUID = (string)data["uuid"];
			break;
		}
		}
		GetBuyOffers();
		StartCoroutine(ScrollSlider(10));
		return true;
	}
}
