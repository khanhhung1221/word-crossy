using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MainResultScript : MonoBehaviour
{
	public static int PlayTournamentAnimCount;

	public GameObject score_go;

	public RectTransform score_bg;

	public RectTransform score_icon;

	public Text score_txt;

	public GameObject HuoDongTournament_go;

	public Animator HuoDongTournamenr_anim;

	public GameObject HuoDongTournament_flyPos;

	public Text HuoDongTournament_TimeText;

	public Text HuoDongTournament_text;

	public GameObject HuoDongTournament_effGo;

	public GameObject HuoDongTournamentCenter_go;

	public Animator HuoDongTournamenrCenter_anim;

	public GameObject HuoDongTournamentCenter_flyPos;

	public Text HuoDongTournamentCenter_TimeText;

	public Text HuoDongTournamentCenter_text;

	public GameObject HuoDongTournamentCenter_effGo;

	private string btnVideo_type;

	private string btnNext_type;

	public GameObject BtnVideo_go;

	public Text BtnVideo_des01;

	public Text BtnVideo_des02;

	public Text BtnVideo_GoldNumTxt;

	public GameObject BtnNext_go;

	public Text BtnNext_txt;

	public Image BtnNext_img;

	public Color[] btnNext_colors;

	public Transform MainTran;

	public GameObject Box_go;

	public GameObject BoxOpen_go;

	public GameObject BoxEff_go;

	public GameObject JinDu_go;

	public Text JinDu_txt;

	public Image JinDu_img;

	public GameObject guang_go;

	public GameObject congratulation;

	public Text title_txt01;

	public Text title_txt02;

	public RectTransform BtnAA_rect;

	public GameObject m_Gold_Lv18;

	private MainUIAniControlScript _effectControl;

	private bool _showNext = true;

	private int _showBtnType;

	private bool _showWatchVideoBtn;

	private int _tmpLevel;

	private bool _showGet25GoldLevel;

	private bool _stopUpdate;

	private MapDataCsv _tmpMapData;

	private float jindu_end_fillAmount;

	private bool isBoxGetGold;

	private int total_level_num;

	private int cur_level_num;

	private bool isAddJinDu;

	private bool isOldLevel;

	private int showNewBtnType;

	public static bool isTournamentNext;

	public static bool isUpdateHuoDongTournamentInfo;

	private float huodongInfo_waitTime;

	private float HuoDongTournament_effGoshow_time;

	private float showGold_time;

	private bool isBackLevelMap;

	private int tempBtnType;

	private bool isBtnVideoHuXi;

	private float huxi_waitTime;

	public string BtnNext_type
	{
		get
		{
			return btnNext_type;
		}
		set
		{
			btnNext_type = value;
		}
	}

	private void Start()
	{
		AdsManager.Instance.ShowBannerAD();
		PlayerInfo.Instance.SetTmpKeyValue("ShowADPlayCount", PlayerInfo.Instance.GetTmpKeyValue("ShowADPlayCount") + 1);
		ResourceLoadManager.GetInstance().HideNetWaiting();
		JinDu_go.transform.localScale = new Vector3(0f, 1f, 1f);
		BoxEff_go.transform.localScale = Vector3.zero;
		guang_go.transform.localScale = Vector3.zero;
		BoxOpen_go.SetActive(value: false);
		HuoDongTournament_go.SetActive(value: false);
		isUpdateHuoDongTournamentInfo = false;
		if (MainUIScript.Instance != null && MainUIScript.Instance.huodong_tournament_main != null && PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardState != 2)
		{
			huodongInfo_waitTime = 1f;
			WordGlobal.UpdateHuoDongTournamentInfoIdx = 1;
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
			{
				Box_go.SetActive(value: false);
				HuoDongTournamentCenter_go.SetActive(value: false);
				HuoDongTournamentCenter_go.transform.localScale = new Vector3(0f, 1f, 1f);
				MainUIScript.Instance.huodong_tournament_main.transform.SetParent(HuoDongTournamentCenter_go.transform.parent);
				HuoDongTournamentCenter_go.SetActive(value: true);
				HuoDongTournamentCenter_text.text = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum();
			}
			else
			{
				MainUIScript.Instance.huodong_tournament_main.transform.SetParent(HuoDongTournament_go.transform.parent);
				HuoDongTournament_go.SetActive(value: true);
				HuoDongTournament_text.text = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum();
			}
		}
		WordGlobal.ResetTopPos(BtnAA_rect);
		PlayTournamentAnimCount = 0;
		ShowTitle();
	}

	public void SetScore(long score)
	{
		score_go.SetActive(value: true);
		float num = 145 + score.ToString().Length * 20;
		RectTransform rectTransform = score_bg;
		float x = num;
		Vector2 sizeDelta = score_bg.sizeDelta;
		rectTransform.sizeDelta = new Vector2(x, sizeDelta.y);
		score_txt.text = score.ToString();
	}

	public void ShowTitle()
	{
		int num = PlayerInfo.Instance.CurrentLevel - 1;
		title_txt01.text = TextLibraryDataManagerCsv.instance.GetText(1294);
		title_txt02.text = string.Format(TextLibraryDataManagerCsv.instance.GetText(1295), num);
		BtnVideo_des01.text = TextLibraryDataManagerCsv.instance.GetText(1122);
		BtnVideo_des02.text = TextLibraryDataManagerCsv.instance.GetText(1123);
	}

	public void ShowBox()
	{
		isBoxGetGold = false;
		isAddJinDu = false;
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
		{
			HuoDongTournamentCenter_go.SetActive(value: true);
			HuoDongTournamentCenter_go.transform.DOScaleX(1f, 0.2f).SetDelay(0.2f).SetEase(Ease.OutBack);
			return;
		}
		if (isOldLevel)
		{
			BoxOpen_go.SetActive(value: true);
			guang_go.SetActive(value: true);
			BoxEff_go.SetActive(value: false);
			int num = PlayerInfo.Instance.CurrentLevel - 1;
			MapDataCsv mapData = MapDataManager.instance.GetMapData(num);
			total_level_num = mapData.LevelEndNum - mapData.LevelStartNum + 1;
			cur_level_num = total_level_num - (mapData.LevelEndNum - num);
			int num2 = cur_level_num;
			JinDu_txt.text = num2 + "/" + total_level_num;
			float fillAmount = (float)num2 / (float)total_level_num;
			JinDu_img.fillAmount = fillAmount;
			JinDu_go.transform.localScale = new Vector3(1f, 1f, 1f);
			guang_go.transform.localScale = Vector3.one;
			return;
		}
		Box_go.SetActive(value: true);
		BoxEff_go.SetActive(value: true);
		guang_go.SetActive(value: true);
		int num3 = PlayerInfo.Instance.MaxPlayLevel - 1;
		MapDataCsv mapData2 = MapDataManager.instance.GetMapData(num3);
		total_level_num = mapData2.LevelEndNum - mapData2.LevelStartNum + 1;
		cur_level_num = total_level_num - (mapData2.LevelEndNum - num3);
		int num4 = cur_level_num - 1;
		JinDu_txt.text = num4 + "/" + total_level_num;
		float fillAmount2 = (float)num4 / (float)total_level_num;
		JinDu_img.fillAmount = fillAmount2;
		jindu_end_fillAmount = (float)cur_level_num / (float)total_level_num;
		if (cur_level_num == total_level_num)
		{
			isBoxGetGold = true;
			guang_go.SetActive(value: false);
		}
		JinDu_go.transform.DOScaleX(1f, 0.2f).SetDelay(0.2f).SetEase(Ease.OutBack);
		guang_go.transform.DOScale(Vector3.one, 0.2f).SetDelay(0.3f);
		BoxEff_go.transform.DOScale(Vector3.one, 0.2f).SetDelay(0.3f).SetEase(Ease.OutBack)
			.OnComplete(delegate
			{
				isAddJinDu = true;
			});
	}

	private void Update()
	{
		if (_stopUpdate)
		{
			return;
		}
		if (_showBtnType == 1)
		{
			if (AdsManager.Instance.IncentivizedAdState && PlayerInfo.Instance.LastOfferPayValue == 0f)
			{
				if (PlayerInfo.Instance.CurrentLevel - 1 == 6)
				{
					BtnNext_type = "m_ObjNoWatchNextBtn";
					BtnVideo_GoldNumTxt.text = "25";
					showNewBtnType = 1;
				}
				else
				{
					btnVideo_type = "m_ObjWatchBtn";
					BtnNext_type = "m_ObjWatchNextBtn";
					BtnVideo_GoldNumTxt.text = "50";
					showNewBtnType = 2;
				}
				_showWatchVideoBtn = true;
			}
			if (!AdsManager.Instance.IncentivizedAdState || PlayerInfo.Instance.LastOfferPayValue > 0f)
			{
				BtnNext_type = "m_ObjNoWatchNextBtn";
				BtnVideo_GoldNumTxt.text = "25";
				showNewBtnType = 1;
			}
		}
		else if (_showBtnType == 2)
		{
			if (AdsManager.Instance.IncentivizedAdState && PlayerInfo.Instance.LastOfferPayValue == 0f)
			{
				btnVideo_type = "m_ObjGetGoldBtn";
				BtnNext_type = "m_ObjGetGoldVideoNextBtn";
				BtnVideo_GoldNumTxt.text = "25";
				showNewBtnType = 2;
			}
			if (!AdsManager.Instance.IncentivizedAdState || PlayerInfo.Instance.LastOfferPayValue > 0f)
			{
				BtnNext_type = "m_ObjNoGetGoldNextBtn";
				BtnVideo_GoldNumTxt.text = "25";
				showNewBtnType = 1;
			}
		}
		if (MainUIScript.Instance != null && MainUIScript.Instance.huodong_tournament_main != null && MainUIScript.Instance.huodong_tournament_main.current_num > 0)
		{
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
			{
				MainUIScript.Instance.huodong_tournament_main.StartResult(HuoDongTournamentCenter_flyPos.transform.position);
			}
			else
			{
				MainUIScript.Instance.huodong_tournament_main.StartResult(HuoDongTournament_flyPos.transform.position);
			}
		}
		else
		{
			UpdateShowBtnType();
			UpdateJinDu();
			UpdateBtnVideoHuXi();
			UpdateShowGold();
		}
		UpdateHuoDongInfo();
		if (isTournamentNext)
		{
			BtnNoWatchNextLevelGold();
			isTournamentNext = false;
		}
	}

	public void UpdateHuoDongInfo()
	{
		UpdateHuoDongTournamentInfo();
		huodongInfo_waitTime += Time.deltaTime;
		if (huodongInfo_waitTime >= 1f)
		{
			int huoDongTournamentTimeLeft = WordGlobal.GetHuoDongTournamentTimeLeft();
			if (huoDongTournamentTimeLeft > 0)
			{
				if (huoDongTournamentTimeLeft - WordGlobal.HuoDongTournamentFinishtTime > 0)
				{
					if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
					{
						HuoDongTournamentCenter_TimeText.text = WordGlobal.ChangeTimeToNormalSpecial(huoDongTournamentTimeLeft - WordGlobal.HuoDongTournamentFinishtTime, 0, isNeedDay: false);
					}
					else
					{
						HuoDongTournament_TimeText.text = WordGlobal.ChangeTimeToNormalSpecial(huoDongTournamentTimeLeft - WordGlobal.HuoDongTournamentFinishtTime, 0, isNeedDay: false);
					}
				}
				else
				{
					if (!WordGlobal.isOpenHuoDongTournamentGetRewardUI && HuoDongTournamentMainItem.isAnimEnd)
					{
						PlayerInfo.Instance.GetHuoDongTournamentSaveData().CheckShowRewardUI();
						WordGlobal.isOpenHuoDongTournamentGetRewardUI = true;
					}
					if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
					{
						HuoDongTournamentCenter_TimeText.text = TextLibraryDataManagerCsv.instance.GetText(1276);
					}
					else
					{
						HuoDongTournament_TimeText.text = TextLibraryDataManagerCsv.instance.GetText(1276);
					}
				}
			}
		}
		if (PlayTournamentAnimCount > 0)
		{
			if (UIManager.CurMainUIType != UIManager.MainUIType.Type_HDTournament)
			{
				HuoDongTournamenr_anim.Play("jiangbei_levelUp");
			}
			PlayTournamentAnimCount--;
		}
	}

	public void UpdateHuoDongTournamentInfo()
	{
		if (WordGlobal.UpdateHuoDongTournamentInfoIdx > 0)
		{
			if (WordGlobal.UpdateHuoDongTournamentInfoIdx == 1)
			{
				MainUIScript.Instance.huodong_tournament_main.SendScore();
				WordGlobal.UpdateHuoDongTournamentInfoIdx = 100;
			}
			if (WordGlobal.UpdateHuoDongTournamentInfoIdx == 2)
			{
				MainUIScript.Instance.huodong_tournament_main.SendScore();
				if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
				{
					HuoDongTournamentCenter_text.text = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum();
				}
				else
				{
					HuoDongTournament_text.text = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum();
				}
				WordGlobal.UpdateHuoDongTournamentInfoIdx = 0;
			}
		}
		if (isUpdateHuoDongTournamentInfo)
		{
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
			{
				if (!HuoDongTournamentCenter_text.text.Equals(PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum()))
				{
					HuoDongTournamentCenter_effGo.SetActive(value: true);
					HuoDongTournament_effGoshow_time = 0.5f;
				}
				HuoDongTournamentCenter_text.text = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum();
			}
			else
			{
				if (!HuoDongTournament_text.text.Equals(PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum()))
				{
					HuoDongTournament_effGo.SetActive(value: true);
					HuoDongTournament_effGoshow_time = 0.5f;
				}
				HuoDongTournament_text.text = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetMyRankNum();
			}
			isUpdateHuoDongTournamentInfo = false;
		}
		if (HuoDongTournament_effGoshow_time > 0f)
		{
			HuoDongTournament_effGoshow_time -= Time.deltaTime;
			if (HuoDongTournament_effGoshow_time <= 0f)
			{
				HuoDongTournamentCenter_effGo.SetActive(value: false);
				HuoDongTournament_effGo.SetActive(value: false);
			}
		}
	}

	public void UpdateShowGold()
	{
		if (showGold_time > 0f)
		{
			showGold_time -= Time.deltaTime;
			if (showGold_time <= 0f)
			{
				m_Gold_Lv18.SetActive(value: true);
			}
		}
	}

	public void UpdateJinDu()
	{
		if (!isAddJinDu || !(JinDu_img.fillAmount < jindu_end_fillAmount))
		{
			return;
		}
		JinDu_img.fillAmount += Time.deltaTime;
		if (JinDu_img.fillAmount >= jindu_end_fillAmount)
		{
			JinDu_img.fillAmount = jindu_end_fillAmount;
			JinDu_txt.text = cur_level_num + "/" + total_level_num;
			if (isBoxGetGold)
			{
				isBackLevelMap = true;
				showGold_time = 0.5f;
				BoxEff_go.GetComponent<Animator>().enabled = true;
			}
		}
	}

	public void UpdateShowBtnType()
	{
		if (tempBtnType == showNewBtnType)
		{
			return;
		}
		float delayTime = 1f;
		if (isBoxGetGold)
		{
			delayTime = 2f;
		}
		if (showNewBtnType == 1)
		{
			BtnAni02(BtnNext_go, null, delayTime);
			BtnVideo_go.SetActive(value: false);
			(BtnNext_go.transform as RectTransform).anchoredPosition = new Vector2(0f, -316f);
			BtnNext_img.color = btnNext_colors[0];
			BtnNext_txt.fontSize = 50;
			BtnNext_txt.color = Color.white;
		}
		else if (showNewBtnType == 2)
		{
			BtnVideo_go.SetActive(value: true);
			(BtnNext_go.transform as RectTransform).anchoredPosition = new Vector2(0f, -441f);
			BtnNext_img.color = btnNext_colors[1];
			BtnNext_txt.fontSize = 36;
			if (PlayerInfo.Instance.CurrentLevel - 1 < 12)
			{
				BtnVideo_go.SetActive(value: false);
				(BtnNext_go.transform as RectTransform).anchoredPosition = new Vector2(0f, -316f);
				BtnAni02(BtnNext_go, null, delayTime);
				BtnNext_img.color = btnNext_colors[0];
				BtnNext_txt.fontSize = 50;
				BtnNext_txt.color = Color.white;
			}
			else
			{
				BtnAni02(BtnVideo_go, delegate
				{
					huxi_waitTime = 1f;
				}, delayTime);
				BtnAni02(BtnNext_go, null, delayTime);
				BtnNext_txt.text = WordGlobal.ReadText(1306);
				BtnNext_txt.color = WordGlobal.ChangeStringToColor("#3dc1ff");
			}
		}
		tempBtnType = showNewBtnType;
	}

	public void UpdateBtnVideoHuXi()
	{
		if (isBtnVideoHuXi && huxi_waitTime > 0f)
		{
			huxi_waitTime -= Time.deltaTime;
			if (huxi_waitTime <= 0f)
			{
				Vector3 big = new Vector3(1.1f, 1.1f, 1.1f);
				BtnVideo_go.transform.DOScale(big, 0.2f).SetEase(Ease.OutBack).OnComplete(delegate
				{
					BtnVideo_go.transform.DOScale(Vector3.one, 0.1f).OnComplete(delegate
					{
						BtnVideo_go.transform.DOScale(big, 0.2f).SetEase(Ease.OutBack).OnComplete(delegate
						{
							BtnVideo_go.transform.DOScale(Vector3.one, 0.1f).OnComplete(delegate
							{
								huxi_waitTime = UnityEngine.Random.Range(1f, 3f);
							});
						});
					});
				});
			}
		}
	}

	private void Awake()
	{
		WordGlobal.InitLabel(base.transform);
		_tmpLevel = PlayerInfo.Instance.CurrentLevel;
		_tmpMapData = MapDataManager.instance.GetMapData(_tmpLevel);
		if (_tmpLevel <= 54 && !MainUIScript.Instance.m_IsRePlayer)
		{
			WordGlobal.LogEvent("PassLevel" + _tmpLevel);
		}
		else if (_tmpLevel == _tmpMapData.LevelEndNum && !MainUIScript.Instance.m_IsRePlayer)
		{
			WordGlobal.LogEvent("PassLevel" + _tmpLevel);
		}
		m_Gold_Lv18.SetActive(value: false);
		showNewBtnType = 0;
		tempBtnType = 0;
		BtnVideo_go.SetActive(value: false);
		BtnNext_go.SetActive(value: false);
		_effectControl = MainUIScript.Instance.GetComponent<MainUIAniControlScript>();
		isOldLevel = true;
		if (PlayerInfo.Instance.CurrentLevel == PlayerInfo.Instance.MaxPlayLevel)
		{
			isOldLevel = false;
		}
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			PlayerInfo.Instance.CurrentLevel++;
			BtnNext_txt.text = string.Format(WordGlobal.ReadText(1296), _tmpLevel + 1);
		}
		else
		{
			BtnNext_txt.text = TextLibraryDataManagerCsv.instance.GetText(1213);
		}
		PlayerPrefs.Save();
	}

	public void SetContent(int score)
	{
		if (score > 0)
		{
			StartCoroutine(UpScore(score));
		}
		else
		{
			StartCoroutine(UpScore(0));
		}
	}

	public IEnumerator UpScore(int add_score)
	{
		congratulation.SetActive(value: true);
		yield return new WaitForSeconds(0.2f);
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
		{
			title_txt02.gameObject.SetActive(value: false);
		}
		else
		{
			title_txt02.gameObject.SetActive(value: true);
		}
		title_txt02.transform.localScale = new Vector3(1f, 0f, 1f);
		title_txt02.transform.DOScaleY(1f, 0.2f);
		yield return new WaitForSeconds(0.25f);
		SetScore(PlayerInfo.Instance.Score - add_score);
		yield return new WaitForSeconds(0.2f);
		if (add_score != 0)
		{
			float add_time = 0.5f;
			int add_times = 15;
			int cur_score = (int)PlayerInfo.Instance.Score - add_score;
			int end_score = (int)PlayerInfo.Instance.Score;
			int add_per = add_score / add_times;
			if (add_per <= 0)
			{
				add_per = 1;
			}
			for (int i = 0; i < add_times; i++)
			{
				yield return new WaitForSeconds(add_time / (float)add_times);
				cur_score += add_per;
				if (cur_score >= end_score)
				{
					cur_score = end_score;
				}
				SetScore(cur_score);
			}
			if (cur_score <= end_score)
			{
				SetScore(end_score);
			}
		}
		yield return new WaitForSeconds(0.1f);
		ShowNextButton();
		ShowBox();
	}

	private void BtnAni02(GameObject obj, TweenCallback action, float delayTime = 0f)
	{
		obj.transform.localScale = Vector3.zero;
		obj.SetActive(value: true);
		obj.transform.DOScale(1f, 0.2f).SetDelay(delayTime).OnComplete(action);
	}

	private void BtnAni(GameObject obj)
	{
		obj.transform.localScale = Vector3.zero;
		obj.SetActive(value: true);
		obj.transform.DOScale(1f, 0.2f).OnComplete(delegate
		{
			ResourceLoadManager.GetInstance().HideNetWaiting();
		});
	}

	private void ShowNextButton()
	{
		MainUIScript.Instance.SaveGame();
		_showGet25GoldLevel = false;
		if (_tmpLevel == _tmpMapData.LevelEndNum && !MainUIScript.Instance.m_IsRePlayer && UIManager.CurMainUIType != UIManager.MainUIType.Type_HDTournament)
		{
			WordGlobal.LogEvent("Pop_Complete");
			WordGlobal.LogEvent("Lastlevel_Pop_Complete");
			if (AdsManager.Instance.IncentivizedAdState && PlayerInfo.Instance.LastOfferPayValue == 0f)
			{
				WordGlobal.LogEvent("Adbutton_Complete");
				WordGlobal.LogEvent("Lastlevel_Adbutton_Complete");
				btnVideo_type = "m_ObjWatchBtn";
				BtnNext_type = "m_ObjWatchNextBtn";
				BtnVideo_GoldNumTxt.text = "50";
				showNewBtnType = 2;
				_showWatchVideoBtn = true;
			}
			else
			{
				BtnNext_type = "m_ObjNoWatchNextBtn";
				BtnVideo_GoldNumTxt.text = "25";
				showNewBtnType = 1;
			}
			_showBtnType = 1;
			return;
		}
		_showBtnType = 0;
		int num = UnityEngine.Random.Range(0, 100);
		if (_tmpLevel > 9 && !IsHasInterstitialAd() && num < 50 && !MainUIScript.Instance.m_IsRePlayer)
		{
			_showGet25GoldLevel = true;
			WordGlobal.LogEvent("Pop_Complete");
			WordGlobal.LogEvent("Randomlevel_Pop_Complete");
			if (AdsManager.Instance.IncentivizedAdState && PlayerInfo.Instance.LastOfferPayValue == 0f)
			{
				WordGlobal.LogEvent("Adbutton_Complete");
				WordGlobal.LogEvent("Random_Adbutton_Complete");
				btnVideo_type = "m_ObjGetGoldBtn";
				BtnNext_type = "m_ObjGetGoldVideoNextBtn";
				BtnVideo_GoldNumTxt.text = "25";
				showNewBtnType = 2;
				_showWatchVideoBtn = true;
				_showBtnType = 2;
			}
			else
			{
				BtnNext_type = "m_ObjNoGetGoldNextBtn";
				BtnVideo_GoldNumTxt.text = "25";
				showNewBtnType = 1;
			}
		}
		else
		{
			BtnNext_type = "m_ObjNoGetGoldNextBtn";
			BtnVideo_GoldNumTxt.text = "25";
			showNewBtnType = 1;
		}
	}

	private IEnumerator AddScore(int score)
	{
		long tmpScore = PlayerInfo.Instance.Score - score;
		for (int i = 0; i < score; i++)
		{
			tmpScore++;
			yield return new WaitForSeconds(0.5f / (float)score);
		}
		ShowNextButton();
	}

	public void NextLevel()
	{
		if (!MainUIScript.Instance.m_IsRePlayer)
		{
			PlayerInfo.Instance.GetDailyQuestSaveData().PassLevel_num++;
			MapDataCsv mapData = MapDataManager.instance.GetMapData(PlayerInfo.Instance.MaxPlayLevel);
			if (PlayerInfo.Instance.MaxPlayLevel == mapData.LevelStartNum)
			{
				PlayerInfo.Instance.GetDailyQuestSaveData().UnlockChapter_num++;
			}
			if (PlayerInfo.Instance.MaxPlayLevel == 5)
			{
				PlayerInfo.Instance.GetDailyQuestSaveData().RefreshData();
			}
		}
		if (PlayerInfo.Instance.IsAlreadyClickRateBtn == 0 && PlayerInfo.Instance.PopRateUINum < 3 && _tmpLevel == _tmpMapData.LevelEndNum && _tmpLevel >= 18)
		{
			if (WordGlobal.isShowRateUs)
			{
				UIManager.Instance.LoadMainUI("UI/RateUI", isPop: true);
			}
			PlayerInfo.Instance.PopRateUINum++;
		}
		_effectControl.ClearFullEffect();
		MainUIScript.Instance._ComboCount = 0;
		BtnVideoScript.dp_wait_showTime = 0f;
		MainUIScript.Instance.ShowScroeUI(isShow: false);
		try
		{
			if (!_showWatchVideoBtn)
			{
				ShowVideo(_tmpLevel, ShowVideoCallBack);
			}
			else
			{
				ShowVideoCallBack();
			}
		}
		catch (Exception)
		{
		}
		_showWatchVideoBtn = false;
		UnityEngine.Object.Destroy(base.gameObject);
	}

	private void ShowVideoCallBack()
	{
		PlayerInfo.Instance.NoPayPlayLevel++;
		WordGlobal.DisableBackKey = false;
		WordFaceBook.Instance.UpdateUserdataMill();
		PlayerInfo.Instance.GameSaveDatas = string.Empty;
		if (isBackLevelMap)
		{
			MainUIScript.Instance.isCloseBackToLevelMap = true;
		}
		MainUIScript.Instance.Init();
		PlayerInfo.Instance.PopUpFBClickNextLvNum++;
		if (PlayerInfo.Instance.FaceBookID == string.Empty && (PlayerInfo.Instance.LastPopUpFBUtcTime != DateTime.UtcNow.Day || PlayerInfo.Instance.PopUpFBNum < 3) && _tmpLevel == 12)
		{
			PlayerInfo.Instance.PopUpFBOnceLoginNum++;
			PlayerInfo.Instance.PopUpFBClickNextLvNum = 0;
			if (GameObject.Find("FaceBookLoginUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/FaceBookLoginUI", isPop: true);
			}
		}
	}

	public bool IsHasInterstitialAd()
	{
		bool result = false;
		if (PlayerInfo.Instance.AdRemove != 1 && _tmpLevel >= 9 && (PlayerInfo.Instance.GetTmpKeyValue("ShowADCount") == 0 || (PlayerInfo.Instance.GetTmpKeyValue("ShowADCount") > 0 && _tmpLevel <= 54 && PlayerInfo.Instance.GetTmpKeyValue("ShowADPlayCount") >= 3) || (_tmpLevel > 54 && PlayerInfo.Instance.GetTmpKeyValue("ShowADPlayCount") >= 2)))
		{
			result = true;
		}
		return result;
	}

	private bool ShowVideo(int level, UnityAction action)
	{
        if (IsHasInterstitialAd() && AdsManager.Instance.ShowInterstitialAd("Pop_ForceAd_Level"))
        {
            //WaitLoadingUI.ShowUI();
            PlayerInfo.Instance.SetTmpKeyValue("ShowADCount", PlayerInfo.Instance.GetTmpKeyValue("ShowADCount") + 1);
            PlayerInfo.Instance.SetTmpKeyValue("ShowADPlayCount", 0);
			//AdsManager.ShowInterstitialAdEvent = (UnityAction)Delegate.Combine(AdsManager.ShowInterstitialAdEvent, new UnityAction(ShowAdCallBack));
			ShowVideoCallBack();
			return true;
        }
        action();
		return false;
	}

	private void ShowAdCallBack()
	{
		AdsManager.ShowInterstitialAdEvent = (UnityAction)Delegate.Remove(AdsManager.ShowInterstitialAdEvent, new UnityAction(ShowAdCallBack));
		WaitLoadingUI.instance.CloseUI();
		ShowVideoCallBack();
	}

	public void WatchRewardVideo()
	{
		if (AdsManager.Instance.ShowIncentivizedAd("LevelComplete_Watch"))
		{
			_stopUpdate = true;
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
			WordGlobal.LogEvent("LevelComplete_Lastlevel_Watch");
		}
	}

	private void ShowAddGoldAnim(int goldNum, float delayTime = 0f)
	{
		if (_tmpLevel == _tmpMapData.LevelEndNum && !MainUIScript.Instance.m_IsRePlayer)
		{
			LevelMapUIScript.MarkToAddGold(goldNum, delayTime);
			return;
		}
		int maxLevel = MapDataManager.instance.GetMaxLevel();
		if (PlayerInfo.Instance.MaxPlayLevel > maxLevel && !MainUIScript.Instance.m_IsRePlayer)
		{
			WordGlobal.LevelEndAddGoldNum = goldNum;
		}
		MainUIScript.Instance.AddGold(goldNum, delayTime);
	}

	private void RewardADEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
		Debug.LogError("RewardADEvent ret:" + ret);
		if (ret)
		{
			CommonTipsScript.Instance.ShowUI(string.Format(WordGlobal.ReadText(1112), 50), 0, WordGlobal.ReadText(1016), delegate
			{
				ShowAddGoldAnim(50, 1f);
				_effectControl.CollectGold(Vector2.zero, 5, 1f, null);
				NextLevel();
			}, showClose: false);
		}
		else
		{
			WatchVideoNextEvent();
		}
	}

	public void GetGoldWatchVideo()
	{
		if (AdsManager.Instance.ShowIncentivizedAd("LevelComplete_Watch"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(GetGoldRewardADEvent));
			WordGlobal.LogEvent("LevelComplete_Random_Watch");
		}
	}

	private void GetGoldRewardADEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(GetGoldRewardADEvent));
		Debug.LogError("GetGoldRewardADEvent ret:" + ret);
		if (ret)
		{
			CommonTipsScript.Instance.ShowUI(string.Format(WordGlobal.ReadText(1112), 25), 0, WordGlobal.ReadText(1016), delegate
			{
				ShowAddGoldAnim(25, 1f);
				_effectControl.CollectGold(Vector2.zero, 5, 1f, null);
				NextLevel();
			}, showClose: false);
		}
		else
		{
			NextLevel();
		}
	}

	public void WatchVideoNextEvent()
	{
		_effectControl.CollectGold(Vector2.zero, 5, 1f, null);
		ShowAddGoldAnim(25, 1f);
		NextLevel();
	}

	public void BtnWatchNextLevelGold()
	{
		WatchVideoNextEvent();
		WordGlobal.LogEvent("LevelComplete_Next");
	}

	public void BtnNoWatchNextLevelGold()
	{
		_effectControl.CollectGold(Vector2.zero, 5, 1f, null);
		ShowAddGoldAnim(25, 1f);
		NextLevel();
		WordGlobal.LogEvent("LevelComplete_Next");
	}

	public void BtnNextLevelClick()
	{
		NextLevel();
		if (_showGet25GoldLevel)
		{
			WordGlobal.LogEvent("LevelComplete_Next");
		}
	}

	public void BtnVideoClick()
	{
		if (btnVideo_type.Equals("m_ObjWatchBtn"))
		{
			WatchRewardVideo();
		}
		else if (btnVideo_type.Equals("m_ObjGetGoldBtn"))
		{
			GetGoldWatchVideo();
		}
	}

	public void BtnNextClick()
	{
		if (BtnNext_type.Equals("m_ObjWatchNextBtn"))
		{
			BtnWatchNextLevelGold();
		}
		else if (BtnNext_type.Equals("m_ObjNoWatchNextBtn"))
		{
			BtnNoWatchNextLevelGold();
		}
		else if (BtnNext_type.Equals("m_ObjGetGoldVideoNextBtn"))
		{
			BtnNextLevelClick();
		}
		else if (BtnNext_type.Equals("m_ObjNoGetGoldNextBtn"))
		{
			BtnNextLevelClick();
		}
	}

	public void BtnAA()
	{
		MainUIScript.Instance.BtnAAClick();
	}

	public void BtnTournamentClick()
	{
		AdsManager.Instance.DestroyBannerAD();
		if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardState == 1)
		{
			PlayerInfo.Instance.GetHuoDongTournamentSaveData().SetEndRewardState();
		}
		if (GameObject.Find("HuoDongTournamentUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTournamentUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
		}
		TutorialHuoDongTournament03.isHide = true;
	}
}
