using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MainUIAniControlScript : MonoBehaviour
{
	public GameObject[] m_ObjComoboEffect;

	public GameObject m_ObjStar;

	public GameObject m_ObjStarImg;

	public GameObject m_ObjStarResultAni;

	public GameObject m_ObjComoboScore;

	public GameObject m_ObjGoldIcon;

	public Transform m_TfEffectRoot;

	public bool isLetterFly;

	private void Start()
	{
		if (m_ObjStar == null)
		{
			m_ObjStar = base.transform.Find("GoldIcon").gameObject;
		}
	}

	private void Update()
	{
	}

	public void LoadComoboEffect(int count)
	{
		GameObject gameObject = Object.Instantiate(m_ObjComoboEffect[count]);
		gameObject.transform.SetParent(m_TfEffectRoot, worldPositionStays: false);
		RectTransform rectTransform = gameObject.transform as RectTransform;
		//if (WordGlobal.isMainUIShowBanner)
		//{
  //          Debug.
		//	RectTransform rectTransform2 = rectTransform;
		//	Vector2 anchoredPosition = rectTransform.anchoredPosition;
		//	rectTransform2.anchoredPosition = new Vector2(anchoredPosition.x, WordGlobal.showBanner_bottomupy);
		//}
		//else
		{
			RectTransform rectTransform3 = rectTransform;
			Vector2 anchoredPosition2 = rectTransform.anchoredPosition;
			rectTransform3.anchoredPosition = new Vector2(anchoredPosition2.x, 0f);
		}
	}

	private IEnumerator ComboEffectFun(int comboCount, int score)
	{
		WordGlobal.DisableBackKey = true;
		MainUIScript.Instance.m_HideScoreUI = false;
		if (comboCount > m_ObjComoboEffect.Length - 1)
		{
			comboCount = m_ObjComoboEffect.Length - 1;
		}
		LoadComoboEffect(comboCount);
		GameObject comboObj2 = GameObject.Find("acg_combo");
		if (comboCount >= 4)
		{
			if (comboObj2 != null)
			{
				Object.Destroy(comboObj2);
			}
			if (GameObject.Find("acg_combo2") == null)
			{
				GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI("FX/acg_combo02", m_TfEffectRoot);
				gameObject.name = "acg_combo2";
			}
		}
		if (comboObj2 == null && comboCount < 4)
		{
			comboObj2 = ResourceLoadManager.GetInstance().LoadUI("FX/acg_combo01", m_TfEffectRoot);
			comboObj2.name = "acg_combo";
		}
		yield return new WaitForSeconds(0.8f);
		if (!MainUIScript.Instance.m_IsRePlayer)
		{
			MainUIScript.Instance._tmpComboScore -= score;
			MainUIScript.Instance._currentLevelScore += score;
			MainUIScript.Instance.m_HideScoreUI = true;
			WordGlobal.DisableBackKey = false;
		}
		else
		{
			WordGlobal.DisableBackKey = false;
		}
	}

	public void ComboEffect(int comboCount, int score)
	{
		AudioControl.GetInstance().PlayComboEff(comboCount);
		StartCoroutine(ComboEffectFun(comboCount, score));
	}

	private IEnumerator DailyComboEffectFun(int comboCount, int score)
	{
		if (comboCount > m_ObjComoboEffect.Length - 1)
		{
			comboCount = m_ObjComoboEffect.Length - 1;
		}
		LoadComoboEffect(comboCount);
		yield return new WaitForSeconds(0.8f);
	}

	public void DailyComboEffect(int comboCount, int score)
	{
		AudioControl.GetInstance().PlayComboEff(comboCount);
		StartCoroutine(DailyComboEffectFun(comboCount, score));
	}

	public void StopFullScrenCombo()
	{
		GameObject gameObject = GameObject.Find("acg_combo");
		if (gameObject != null)
		{
			Object.Destroy(gameObject);
		}
		GameObject gameObject2 = GameObject.Find("acg_combo2");
		if (gameObject2 != null)
		{
			Object.Destroy(gameObject2);
		}
	}

	public void ResultStarAni()
	{
		m_ObjStarResultAni.SetActive(value: true);
		m_ObjStarResultAni.GetComponent<DOTweenPath>().DORestart();
	}

	public void CollectedStarAni(Vector3 startPos, Vector3 endPos, Vector3 middlePos)
	{
		List<Vector3> list = new List<Vector3>();
		list.Add(startPos);
		list.Add(middlePos);
		list.Add(endPos);
		StartCoroutine(StartStarAni(list));
	}

	private IEnumerator StartStarAni(List<Vector3> pointFList)
	{
		GameObject obj = Object.Instantiate(m_ObjStar);
		obj.transform.SetParent(base.transform.Find("ResultStarPos"), worldPositionStays: false);
		obj.transform.position = pointFList[0];
		obj.GetComponent<Animator>().SetTrigger("starstart");
		yield return new WaitForSeconds(11f / 12f);
		obj.GetComponent<Animator>().SetTrigger("starmove");
		obj.transform.DOPath(CalMovePath(pointFList).ToArray(), 2f);
		yield return new WaitForSeconds(2f);
		obj.GetComponent<Animator>().SetTrigger("starend");
		yield return new WaitForSeconds(1f);
		Object.Destroy(obj);
	}

	private List<Vector3> CalMovePath(List<Vector3> pointFList)
	{
		List<Vector3> list = new List<Vector3>();
		if (pointFList == null || pointFList.Count == 0)
		{
			return list;
		}
		int count = pointFList.Count;
		int num = 0;
		for (int i = 0; i < count - 1; i++)
		{
			for (float num2 = 0f; num2 < 1f; num2 += 0.01f)
			{
				num++;
				Vector3 item = WordGlobal.interpolatedPosition(pointFList[Mathf.Max(0, i - 1)], pointFList[i], pointFList[Mathf.Min(i + 1, count - 1)], pointFList[Mathf.Min(i + 2, count - 1)], num2);
				list.Add(item);
			}
		}
		return list;
	}

	private Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
	{
		float num = 1f - t;
		float d = t * t;
		float d2 = num * num;
		Vector3 a = d2 * p0;
		a += 2f * num * t * p1;
		a += d * p2;
		return new Vector3(a.x, a.y, 0f);
	}

	public void GameVictory()
	{
		ResourceLoadManager.GetInstance().LoadUI("FX/acg_victory", m_TfEffectRoot);
	}

	public void ResultStarAdd(Vector3 startPos, Vector3 endPos, float delay, UnityAction callback)
	{
		GameObject obj = Object.Instantiate(m_ObjStarImg);
		obj.transform.SetParent(base.transform.Find("ResultStarPos"), worldPositionStays: false);
		obj.transform.position = startPos;
		obj.transform.DOMove(endPos, 0.5f).SetDelay(delay).OnComplete(delegate
		{
			Object.Destroy(obj);
			callback();
		});
	}

	public void LetterFly(Vector3 starPos, Vector3 endPos, float flyTime = 1f, UnityAction callback = null, bool isNeedEff = true)
	{
		LetterFlyEff(starPos, endPos, "FX/acg_star02", 0.6f, callback, 0f, isNeedEff);
	}

	public void LetterFlyEff(Vector3 starPos, Vector3 endPos, string eff_path, float flyTime = 1f, UnityAction callback = null, float delayTime = 0f, bool isNeedEff = true)
	{
		GameObject obj = ResourceLoadManager.GetInstance().LoadObjByPath(eff_path);
		isLetterFly = true;
		obj.transform.SetParent(base.transform, worldPositionStays: false);
		obj.transform.position = starPos;
		if (!isNeedEff)
		{
			obj.SetActive(value: false);
		}
		obj.transform.DOMove(endPos, flyTime).SetDelay(delayTime).OnComplete(delegate
		{
			isLetterFly = false;
			Object.Destroy(obj);
			if (callback != null)
			{
				callback();
			}
		});
	}

	public void QuestLetterFly(Vector3 starPos, Vector3 endPos, float flyTime = 1f, UnityAction callback = null)
	{
		LetterFlyEff(starPos, endPos, "FX/acg_star03", flyTime, callback);
	}

	public void CollectGold(Vector3 starPos, int count, float moveTime, UnityAction callback)
	{
		m_ObjGoldIcon.transform.parent.GetComponent<GoldScript>().CollectGold(starPos, count, moveTime, callback);
	}

	public void ClearFullEffect()
	{
		for (int num = m_TfEffectRoot.childCount - 1; num >= 0; num--)
		{
			Object.Destroy(m_TfEffectRoot.GetChild(num));
		}
	}
}
