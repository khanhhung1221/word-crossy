using DG.Tweening;
//using Firebase.Messaging;
using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class MainUIScript : MonoBehaviour
{
	public Image m_ImgBG;

	public GameObject m_ObjItem;

	public RectTransform Top_rectTran;

	public Transform m_TfItemRoot;

	public Transform m_TfLettersRoot;

	public Transform m_TfTipsClickRoot;

	public GameObject m_ObjLettersItem;

	public Text m_TxtCurrentWord;

	public GameObject m_ObjTipsClick;

	public GameObject m_ObjTipsBtns;

	public GameObject m_ObjBtnTipsLight;

	public Text m_TxtConfirm;

	public Transform BottomTran;

	public RectTransform BtnDailyQuest_rect;

	public GameObject m_ObjFirstTipsLock;

	public GameObject m_ObjFirstTipsMask;

	public GameObject tipsHalfPay;

	public Text tipsHalfPay_text;

	public GameObject tipsAllFirstHalfPay;

	public Text tipsAllFirstHalfPay_text;

	public Text m_TxtLevel;

	public Text m_TxtStarCount;

	public GameObject m_GoldItem;

	public Transform m_TfItemTempRoot;

	public GameObject m_ObjTutorial;

	public GameObject m_DrawLine;

	public Transform m_TfGoldIcon;

	public GameObject m_ObjBtnTipsClick;

	public GameObject m_ObjBtnCellTipsClick;

	public GameObject m_ObjBtnCellTipsShow;

	public GameObject m_ObjBtnWatchVideo;

	public GameObject m_ObjBtnBonus;

	public GameObject m_ObjBtnRandomLetter;

	public GameObject m_ObjBtnFirstLetter;

	public GameObject m_ObjBtnAA;

	public RectTransform m_TFScoreUI;

	public GameObject m_ImgCurrentWordBG;

	public RectTransform selectLetterAnim_rect;

	public Image m_ImgWordBG;

	//public Sprite[] m_WordBGs;

	public GameObject m_ObjTS;

	public bool m_IsMoveLetter;

	public float m_PauseLetterTime = 0.5f;

	public GameObject m_ObjBonusFull;

	public GameObject[] m_UIAniLeftObj;

	public GameObject[] m_UIAniRightObj;

	public GameObject[] m_UIAniTopObj;

	public GameObject m_UILetterRoot;

	public GameObject m_ObjButterFly;

	public GameObject m_ObjNo3WordBonus;

	public Text BonusNum_txt;

	private int _minWordLength;

	public int m_DictionaryCurrentIndex;

	public GameObject DailyPuzzleObj;

	public GameObject StarPosObj;

	public Image StarBarFill;

	public List<GameObject> DailyPuzzleStars;

	public List<Text> DailyPuzzleStarNums;

	public Text DailyButterFuNumText;

	public Animator BonusAnim;

	public UIManager.LevelMapUIType CurBackLevelMapType = UIManager.LevelMapUIType.Type_Normal;

	public RectTransform m_RtfTop;

	private float _uiAniLeftPoint = -550f;

	private float _uiAniRightPoint = 550f;

	private float _uiAniTopPoint = 180f;

	private Dictionary<int, float> _uiAniOldPoints;

	public CatmullDraw _catmullDraw;

	private List<List<string>> _grid;

	private List<string> _letters;

	private float _panelHeight = 562f;

	private float _panelWith = 690f;

	private float _itemHeight = 146f;

	private float _itemWith = 146f;

	private float _itemSpacing = 6f;

	private float _letterItemR = 163f;

	private Color _letterColor = new Color(0f, 0f, 0f);

	private WordListItem _item;

	public List<string> _currentWordList = new List<string>();

	public string _currentWord = string.Empty;

	public GameObject m_ObjMask0;

	private CrossWordItem _tipsWord;

	private int _tipsCount;

	public bool _cellTipsClick;

	private int _cellTipsRow;

	private int _cellTipsCol;

	private bool _NextLevel;

	private bool _CheckLeveling;

	public int _currentLevelScore;

	public int _ComboCount;

	private bool _IsLastWord;

	public int _tmpComboScore;

	public List<string> _bonusList = new List<string>();

	private List<string> _bonusTipList = new List<string>();

	private List<string> _collectedMoneyWord;

	private List<Vector3> _letterPosList;

	private List<string> _currentAddLetter = new List<string>();

	public List<string> _tmpCompleteWordList;

	private string _curButterFlyPos = string.Empty;

	private string _curMoveButterPos = string.Empty;

	private CrossWordItem _curButterInWordItem;

	private int _lastButtFlyColorIndex;

	private int _curButterColorIndex;

	private GameObject _lastButterFly;

	private GameObject _curButterFly;

	private string[] _buttColorList = new string[7]
	{
		"#FB6734",
		"#FE3C3C",
		"#FFB135",
		"#78EA78",
		"#0BBAF4",
		"#9E69FF",
		"#FC61BC"
	};

	private int _curGetButterFlyNum;

	private int _curGetStarNum;

	[HideInInspector]
	public int _curButterFlyTVCount;

	private int[] _starButterNumList;

	private bool _isCurDailyLevelFinished;

	private int _dailyMaxStarNum = 3;

	private int _curLightStarIndex;

	private Crossword _cw;

	public MainUIAniControlScript _effectControl;

	private Coroutine _coroutineLastWordEffect;

	public bool m_HideScoreUI = true;

	private int _showScoreUICount;

	private float _showScoreTime = -5f;

	private bool _showResultUI;

	private float _adShakeTimeSet = 20f;

	private float _adShakeTime = 20f;

	public Color _StyleColor = new Color(0.3098f, 0.6705f, 0.996f, 1f);

	private float _aniPlayTime;

	private DateTime _lastOperationDate;

	private long _noOperationTimes = 30L;

	private float _noOperationFrame;

	public GameObject DailyQuestBtnEffect;

	public GameObject DailyQuestBtn;

	public DOTweenAnimation DailyQuestBtn_imgAnim;

	private MapDataCsv _curMapData;

	public RectTransform LevelRectTran;

	[HideInInspector]
	public HuoDongMainUI huodong_main;

	public RectTransform huodong_ItemNum;

	public RectTransform huodong_flyPos;

	public GameObject huodong_effgo;

	public Image huodong_img;

	public Text huodong_ItemNum_text;

	[HideInInspector]
	public HuoDongGardonMainUI huodong_gardon_main;

	[HideInInspector]
	public HuoDongSunMainUI huodong_sun_main;

	[HideInInspector]
	public HuoDongTournamentMainItem huodong_tournament_main;

	private int huodong_ItemAmount;

	private List<string> _dictionaryList = new List<string>();

	public bool isUInAnimGardon;

	private float showLastWordEff_time;

	private CrossWordItem lastWordItem;

	private bool isRePlay;

	public bool isTest;

	public string TestWord;

	private float restoreTip_waitTime = 0.5f;

	private float UpdateHalfPay_waitTime;

	public bool CheckHuoDongGetItem;

	public static bool isShowHuoDongUI;

	public static float showHuoDongUiWaitTime;

	private HuoDongMainTip hdmtip;

	private float dpc_tutorial_waitTime;

	private bool CheckDPCTutorial;

	private Color default_linecolor = Color.white;

	private bool isShowDPCTutorialTip;

	private bool isShowDPCTutorialTip_search;

	private float HalfPayFirstTipTutorial_waitTime;

	public bool isCloseBackToLevelMap;

	private ButterFlyTV bfTV;

	private ButterFlyTV commonButterFlyTV;

	private int[] showButterFlyTVLevels = new int[11]
	{
		2,
		6,
		12,
		18,
		22,
		26,
		32,
		38,
		43,
		48,
		54
	};

	public int LetterState2Count;

	private int _tmpScore;

	private bool _isAddingScore;

	private int score_speed;

	private float add_scoreWaitTime;

	private bool isShowTutorialWatchVideo;

	private bool isTips;

	public bool isFinishNullLetters;

	private bool isFreeDP;

	private bool isFreeDPShowEff = true;

	public bool isHuoDongSunDestroy;

	public bool m_IsRePlayer
	{
		get
		{
			return isRePlay;
		}
		set
		{
			isRePlay = value;
		}
	}

	public GameObject btn_win;

	public static MainUIScript Instance
	{
		get;
		private set;
	}

	private void Start()
	{
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			PlayerInfo.GetOfferEvent += GetPopOfferEvent;
			m_IsRePlayer = (PlayerInfo.Instance.CurrentLevel < PlayerInfo.Instance.MaxPlayLevel);
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			m_IsRePlayer = false;
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
		{
			m_IsRePlayer = false;
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
		{
			m_IsRePlayer = false;
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
		{
			m_IsRePlayer = false;
		}
		SetLastOperationPoint();
		AudioControl.GetInstance().PlayBG(AudioControl.GetInstance().Music_Main1);
		CatmullDraw.OnDrawLineEnd += TryAddWord;
		CatmullDraw.OnClearLine += ClearLetterImg;
		if (m_DrawLine != null)
		{
			_catmullDraw = m_DrawLine.GetComponent<CatmullDraw>();
		}
		if (PlayerInfo.Instance.MaxPlayLevel > 10 && PlayerInfo.Instance.TutorialStep != 999)
		{
			PlayerInfo.Instance.TutorialStep = 999;
		}
		Init();
		Input.multiTouchEnabled = false;
		if (PlayerInfo.Instance.MaxPlayLevel <= 10)
		{
			PlayerInfo.Instance.FirstLogin = 2;
		}
		m_ObjBonusFull.SetActive(PlayerInfo.Instance.CollectedBonusCount >= PlayerInfo.Instance.MaxBonusCount);
		DailyQuestBtnEffect.SetActive(value: false);
		DailyQuestBtn_imgAnim.enabled = false;
		DailyQuestBtn_imgAnim.DOKill();
		//FirebaseMessaging.TokenReceived += OnTokenReceived;
		WordFaceBook.Instance.SendApplicationVersion();
		PlayerInfo.Instance.GetLittleTipsData().Init();

		if (PlayerInfo.Instance.showTestLog == 1) btn_win.SetActive(true);
	}

	//private void OnTokenReceived(object sender, TokenReceivedEventArgs token)
	//{
	//	JsonData jsonData = new JsonData();
	//	jsonData["uuid"] = PlayerInfo.Instance.UUID;
	//	jsonData["token"] = token.Token;
	//	NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "fcmregister", jsonData);
	//}

	private void InitUIAniPar()
	{
		_uiAniOldPoints = new Dictionary<int, float>();
		int num = 0;
		for (int i = 0; i < m_UIAniTopObj.Length; i++)
		{
			Dictionary<int, float> uiAniOldPoints = _uiAniOldPoints;
			int key = num++;
			Vector2 anchoredPosition = m_UIAniTopObj[i].GetComponent<RectTransform>().anchoredPosition;
			uiAniOldPoints.Add(key, anchoredPosition.y);
		}
		for (int j = 0; j < m_UIAniLeftObj.Length; j++)
		{
			Dictionary<int, float> uiAniOldPoints2 = _uiAniOldPoints;
			int key2 = num++;
			Vector3 localPosition = m_UIAniLeftObj[j].transform.localPosition;
			uiAniOldPoints2.Add(key2, localPosition.x);
		}
		for (int k = 0; k < m_UIAniRightObj.Length; k++)
		{
			Dictionary<int, float> uiAniOldPoints3 = _uiAniOldPoints;
			int key3 = num++;
			Vector3 localPosition2 = m_UIAniRightObj[k].transform.localPosition;
			uiAniOldPoints3.Add(key3, localPosition2.x);
		}
	}

	public void UIInAni(int state)
	{
		BtnVideoScript.UI_INOUT = 0;
		int num = 0;
		for (int i = 0; i < m_UIAniTopObj.Length; i++)
		{
			if (state == 0)
			{
				Transform transform = m_UIAniTopObj[i].transform;
				Vector3 localPosition = m_UIAniTopObj[i].transform.localPosition;
				transform.localPosition = new Vector3(localPosition.x, _uiAniTopPoint);
			}
			if (state == 1)
			{
				m_UIAniTopObj[i].GetComponent<RectTransform>().DOAnchorPosY(_uiAniOldPoints[num++], 0.5f);
			}
		}
		for (int j = 0; j < m_UIAniLeftObj.Length; j++)
		{
			if (state == 0)
			{
				Transform transform2 = m_UIAniLeftObj[j].transform;
				float uiAniLeftPoint = _uiAniLeftPoint;
				Vector3 localPosition2 = m_UIAniLeftObj[j].transform.localPosition;
				transform2.localPosition = new Vector3(uiAniLeftPoint, localPosition2.y);
			}
			if (state == 1)
			{
				m_UIAniLeftObj[j].GetComponent<RectTransform>().DOAnchorPosX(_uiAniOldPoints[num++], 0.5f);
			}
		}
		for (int k = 0; k < m_UIAniRightObj.Length; k++)
		{
			if (state == 0)
			{
				Transform transform3 = m_UIAniRightObj[k].transform;
				float uiAniRightPoint = _uiAniRightPoint;
				Vector3 localPosition3 = m_UIAniRightObj[k].transform.localPosition;
				transform3.localPosition = new Vector3(uiAniRightPoint, localPosition3.y);
			}
			if (state == 1)
			{
				m_UIAniRightObj[k].GetComponent<RectTransform>().DOAnchorPosX(_uiAniOldPoints[num++], 0.5f);
			}
		}
		if (state == 0)
		{
			m_UILetterRoot.SetActive(value: false);
			m_UILetterRoot.transform.localScale = Vector3.one;
		}
		if (state == 1)
		{
			m_UILetterRoot.transform.localScale = Vector3.zero;
			m_UILetterRoot.SetActive(value: true);
			m_UILetterRoot.transform.DOScale(1f, 0.5f);
			if (huodong_gardon_main != null)
			{
				huodong_gardon_main.round_tran.localScale = Vector3.zero;
				huodong_gardon_main.round_tran.DOScale(1f, 0.5f);
			}
			for (int l = 0; l < m_TfItemRoot.childCount; l++)
			{
				m_TfItemRoot.GetChild(l).transform.localScale = Vector3.zero;
				m_TfItemRoot.GetChild(l).gameObject.SetActive(value: true);
				m_TfItemRoot.GetChild(l).transform.DOScale(1f, 0.5f).SetDelay(UnityEngine.Random.Range(0f, 0.2f));
			}
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && _curButterFlyPos != string.Empty)
			{
				string[] array = _curButterFlyPos.Split('_');
				int num5 = int.Parse(array[0]);
				int num6 = int.Parse(array[1]);
				ItemScript component = GetItemByRC(num5 - 1, num6 - 1).GetComponent<ItemScript>();
				Color color = WordGlobal.ChangeStringToColor(_buttColorList[_curButterColorIndex]);
				component.ShowButterFly(color, isDelay: false);
			}
		}
	}

	public IEnumerator UIInAniGardon()
	{
		m_UILetterRoot.SetActive(value: false);
		m_UILetterRoot.transform.localScale = Vector3.zero;
		BtnVideoScript.UI_INOUT = 0;
		for (int i = 0; i < m_UIAniTopObj.Length; i++)
		{
			Transform transform = m_UIAniTopObj[i].transform;
			Vector3 localPosition = m_UIAniTopObj[i].transform.localPosition;
			transform.localPosition = new Vector3(localPosition.x, _uiAniTopPoint);
		}
		for (int j = 0; j < m_UIAniLeftObj.Length; j++)
		{
			Transform transform2 = m_UIAniLeftObj[j].transform;
			float uiAniLeftPoint = _uiAniLeftPoint;
			Vector3 localPosition2 = m_UIAniLeftObj[j].transform.localPosition;
			transform2.localPosition = new Vector3(uiAniLeftPoint, localPosition2.y);
		}
		for (int k = 0; k < m_UIAniRightObj.Length; k++)
		{
			Transform transform3 = m_UIAniRightObj[k].transform;
			float uiAniRightPoint = _uiAniRightPoint;
			Vector3 localPosition3 = m_UIAniRightObj[k].transform.localPosition;
			transform3.localPosition = new Vector3(uiAniRightPoint, localPosition3.y);
		}
		for (int l = 0; l < m_TfItemRoot.childCount; l++)
		{
			m_TfItemRoot.GetChild(l).transform.localScale = Vector3.zero;
			m_TfItemRoot.GetChild(l).gameObject.SetActive(value: false);
		}
		if (huodong_gardon_main != null)
		{
			huodong_gardon_main.round_tran.localScale = Vector3.zero;
		}
		isUInAnimGardon = true;
		for (int m = 0; m < m_TfItemRoot.childCount; m++)
		{
			m_TfItemRoot.GetChild(m).gameObject.SetActive(value: true);
			m_TfItemRoot.GetChild(m).transform.DOScale(1f, 0.5f).SetDelay(UnityEngine.Random.Range(0f, 0.2f));
		}
		yield return new WaitForSeconds(0.8f);
		float wait_time = 0f;
		if (huodong_gardon_main != null)
		{
			wait_time = huodong_gardon_main.ShowUIInTime(0) + 0.3f;
		}
		yield return new WaitForSeconds(wait_time);
		int v = 0;
		for (int n = 0; n < m_UIAniTopObj.Length; n++)
		{
			RectTransform component = m_UIAniTopObj[n].GetComponent<RectTransform>();
			Dictionary<int, float> uiAniOldPoints = _uiAniOldPoints;
			int key;
			v = (key = v) + 1;
			component.DOAnchorPosY(uiAniOldPoints[key], 0.5f);
		}
		for (int num = 0; num < m_UIAniLeftObj.Length; num++)
		{
			RectTransform component2 = m_UIAniLeftObj[num].GetComponent<RectTransform>();
			Dictionary<int, float> uiAniOldPoints2 = _uiAniOldPoints;
			int key;
			v = (key = v) + 1;
			component2.DOAnchorPosX(uiAniOldPoints2[key], 0.5f);
		}
		for (int num2 = 0; num2 < m_UIAniRightObj.Length; num2++)
		{
			RectTransform component3 = m_UIAniRightObj[num2].GetComponent<RectTransform>();
			Dictionary<int, float> uiAniOldPoints3 = _uiAniOldPoints;
			int key;
			v = (key = v) + 1;
			component3.DOAnchorPosX(uiAniOldPoints3[key], 0.5f);
		}
		if (huodong_gardon_main != null)
		{
			huodong_gardon_main.ShowUIInTime(1);
		}
		yield return new WaitForSeconds(0.3f);
		m_UILetterRoot.SetActive(value: true);
		m_UILetterRoot.transform.DOScale(1f, 0.5f);
		if (huodong_gardon_main != null)
		{
			huodong_gardon_main.round_tran.DOScale(1f, 0.5f);
		}
		yield return new WaitForSeconds(0.8f);
		isUInAnimGardon = false;
	}

	public void UIOutAni(bool isDailyResult = false, bool isOutItemRoot = true)
	{
		if (UIManager.CurMainUIType != UIManager.MainUIType.Type_Daily && !isDailyResult)
		{
			for (int i = 0; i < m_UIAniTopObj.Length; i++)
			{
				m_UIAniTopObj[i].GetComponent<RectTransform>().DOAnchorPosY(_uiAniTopPoint, 0.2f);
			}
		}
		for (int j = 0; j < m_UIAniLeftObj.Length; j++)
		{
			m_UIAniLeftObj[j].GetComponent<RectTransform>().DOAnchorPosX(_uiAniLeftPoint, 0.2f);
		}
		for (int k = 0; k < m_UIAniRightObj.Length; k++)
		{
			m_UIAniRightObj[k].GetComponent<RectTransform>().DOAnchorPosX(_uiAniRightPoint, 0.2f);
		}
		BtnVideoScript.UI_INOUT = 1;
		m_UILetterRoot.transform.DOScale(0f, 0.2f);
		if (huodong_gardon_main != null)
		{
			huodong_gardon_main.round_tran.DOScale(0f, 0.2f);
		}
		if (huodong_sun_main != null)
		{
			huodong_sun_main.SunItemOut();
		}
		if (isOutItemRoot)
		{
			for (int l = 0; l < m_TfItemRoot.childCount; l++)
			{
				Transform child = m_TfItemRoot.GetChild(l);
				child.DOScale(0f, 0.2f).SetDelay(UnityEngine.Random.Range(0f, 0.2f));
				child.GetComponent<ItemScript>().HideButterFly();
			}
		}
	}

	public void InitBonusEffect()
	{
		m_ObjBonusFull.SetActive(PlayerInfo.Instance.CollectedBonusCount >= PlayerInfo.Instance.MaxBonusCount);
	}

	private void LateUpdate()
	{
		if ((_tmpCompleteWordList.Count != 0 || _NextLevel) && !_CheckLeveling && !_NextLevel && !(_aniPlayTime > 0f))
		{
			CheckLevel();
        }
	}

    public void WinGame()
    {
		StartCoroutine(ShowResult(0));
	}

	private IEnumerator LastWordEffect(CrossWordItem item)
	{
		yield return new WaitForSeconds(1.5f);
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_LastWord);
		int r = item.Row;
		int c = item.Col;
		for (int i = 0; i < item.Word.Length; i++)
		{
			if (GetItemByRC(r - 1, c - 1) == null)
			{
				continue;
			}
			GetItemByRC(r - 1, c - 1).GetComponent<ItemScript>().LoadLastWordEffect();
			yield return new WaitForSeconds(0.35f);
			if (!(GetItemByRC(r - 1, c - 1) == null))
			{
				GetItemByRC(r - 1, c - 1).GetComponent<ItemScript>().DestroyEffect("acg_TxtQuabFX01");
				if (item.vertical)
				{
					r++;
				}
				else
				{
					c++;
				}
				if (_currentWordList.Count >= _item.WordList.Count)
				{
					yield break;
				}
			}
		}
		showLastWordEff_time = 10f;
		lastWordItem = item;
	}

	public void UpdateShowLastWordEff()
	{
		if (!(showLastWordEff_time > 0f))
		{
			return;
		}
		showLastWordEff_time -= Time.deltaTime;
		if (!(showLastWordEff_time <= 0f) || _currentWordList.Count != _item.WordList.Count - 1)
		{
			return;
		}
		int i;
		for (i = 0; i < _item.WordList.Count; i++)
		{
			if (_currentWordList.FindIndex((string x) => x == _item.WordList[i].Word) < 0)
			{
				_coroutineLastWordEffect = StartCoroutine(LastWordEffect(_item.WordList[i]));
			}
		}
	}

	public void SaveGame()
	{
		if (m_IsRePlayer || WordGlobal.Restarting)
		{
			return;
		}
		GameSaveData gameSaveData = new GameSaveData();
		gameSaveData.Words = _currentWordList;
		gameSaveData.GridStr = JsonMapper.Serialize(_grid);
		gameSaveData.InitGrid();
		gameSaveData.BonusList = _bonusList;
		gameSaveData.Level = PlayerInfo.Instance.CurrentLevel;
		gameSaveData.CurrentScore = _currentLevelScore;
		gameSaveData.ComboScore = _tmpComboScore;
		gameSaveData.DictList = _dictionaryList;
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			gameSaveData.Level = PlayerInfo.Instance.GetDailyDayId();
			gameSaveData.ButterFlyPos = _curButterFlyPos;
			if (_curButterInWordItem != null)
			{
				gameSaveData.ButterFlyWord = _curButterInWordItem.Word;
			}
			gameSaveData.CurButterScore = _curGetButterFlyNum;
			gameSaveData.CurStarNums = _curGetStarNum;
			gameSaveData.ButterFlyTVCount = _curButterFlyTVCount;
			gameSaveData.IsDailyPuzzleFinished = (_isCurDailyLevelFinished ? 1 : 0);
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
		{
			gameSaveData.Level = PlayerInfo.Instance.GetHuoDongSunSaveData().GetCurLevelID();
			gameSaveData.HDSunPos = huodong_sun_main.GetSunPos();
			gameSaveData.CurSunScore = huodong_sun_main.GetSunScore();
		}
		if (_tipsWord != null)
		{
			gameSaveData.CurrentTipsWord = _tipsWord.Word;
		}
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			PlayerInfo.Instance.GameSaveDatas = JsonUtility.ToJson(gameSaveData);
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			PlayerInfo.Instance.DailyGameSaveDatas = JsonUtility.ToJson(gameSaveData);
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
		{
			PlayerInfo.Instance.HuoDongSunGameSaveDatas = JsonUtility.ToJson(gameSaveData);
		}
	}

	private void Update()
	{
		if (PlayerInfo.Instance.GetDailyQuestSaveData().isCanClaim() > 0)
		{
			Vector3 localPosition = DailyQuestBtn.transform.localPosition;
			if (!(localPosition.x > 300f) && !_showResultUI && !Instance._effectControl.isLetterFly && !DailyQuestBtnEffect.activeInHierarchy)
			{
				if (DailyQuestBtn_imgAnim != null)
				{
					DailyQuestBtn_imgAnim.transform.localScale = Vector2.one;
					DailyQuestBtn_imgAnim.enabled = true;
					DailyQuestBtn_imgAnim.CreateTween();
					DailyQuestBtn_imgAnim.tween.Play();
				}
				if (PlayerInfo.Instance.MaxPlayLevel >= 5 && PlayerInfo.Instance.ShowTutorialDailyQuestFinish == 0 && UIManager.CurMainUIType != UIManager.MainUIType.Type_HDGardon)
				{
					PlayerInfo.Instance.ShowTutorialDailyQuestFinish = 1;
					UIManager.Instance.LoadMainUI("UI/Main/TutorialDailyQuestFinish", isPop: false, UIManager.MainUIType.Type_Common, 100);
				}
				DailyQuestBtnEffect.SetActive(value: true);
			}
		}
		else
		{
			if (DailyQuestBtn_imgAnim.enabled)
			{
				DailyQuestBtn_imgAnim.enabled = false;
				DailyQuestBtn_imgAnim.DOKill();
			}
			DailyQuestBtnEffect.SetActive(value: false);
		}
		if (isTest)
		{
			FinishNullLetters();
			isTest = false;
		}
		_aniPlayTime -= Time.deltaTime;
		InitUI();
		_showScoreTime -= Time.deltaTime;
		if (_showScoreTime < 0f && _currentWordList.Count < _item.WordList.Count && _showScoreTime > -2f)
		{
			ShowScroeUI(isShow: false);
		}
		m_PauseLetterTime -= Time.deltaTime;
		_noOperationFrame -= Time.deltaTime;
		if (_noOperationFrame < 0f)
		{
			NoOperationCallEvent(null);
		}
		//if (CodelessIAPStoreListener.Instance.isSendVipInfo)
		//{
		//	WordFaceBook.Instance.SendVipInfo(CodelessIAPStoreListener.Instance.SubscriptionBeginDate_str, CodelessIAPStoreListener.Instance.SubscriptionEndDate_str, CodelessIAPStoreListener.Instance.SubscriptionTransactionID);
		//	WordGlobal.GetMailTime = 1f;
		//	CodelessIAPStoreListener.Instance.isSendVipInfo = false;
		//}
		UpdateCheckDPCTutorial();
		UpdateShowHuoDongUI();
		UpdateShowLastWordEff();
		UpdateHuoDongOpenTip();
		UpdateAddScoreAni();
		if (isShowTutorialWatchVideo)
		{
			isShowTutorialWatchVideo = false;
		}
		if (WordGlobal.isNeedHuoDong)
		{
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
			{
				if (huodong_main != null)
				{
					if (huodong_ItemAmount != PlayerInfo.Instance.GetHuoDongSaveData().HuoDongItemAmount)
					{
						huodong_ItemAmount = PlayerInfo.Instance.GetHuoDongSaveData().HuoDongItemAmount;
						huodong_ItemNum_text.text = huodong_ItemAmount.ToString();
					}
					if (CheckHuoDongGetItem && PlayerInfo.Instance.GetHuoDongSaveData().GetRewardState != 0)
					{
						huodong_ItemNum.gameObject.SetActive(value: false);
						LevelRectTran.gameObject.SetActive(value: true);
					}
				}
				if (_showResultUI)
				{
				}
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun && huodong_sun_main != null && huodong_ItemAmount != huodong_sun_main.GetSunScore())
			{
				huodong_ItemAmount = huodong_sun_main.GetSunScore();
				huodong_ItemNum_text.text = huodong_ItemAmount.ToString();
			}
		}
		UpdateHalfPay();
		UpdateRestoreTip();
	}

	public void UpdateRestoreTip()
	{
		//if (IAPListener.isShowRestorePurchaseTip > 0 && restoreTip_waitTime > 0f)
		//{
		//	restoreTip_waitTime -= Time.deltaTime;
		//	if (restoreTip_waitTime <= 0f)
		//	{
		//		ResourceLoadManager.GetInstance().ShowTips(WordGlobal.ReadText(IAPListener.isShowRestorePurchaseTip));
		//		IAPListener.isShowRestorePurchaseTip = 0;
		//		restoreTip_waitTime = 0.5f;
		//	}
		//}
	}

	public void UpdateHalfPay()
	{
		UpdateHalfPay_waitTime += Time.deltaTime;
		if (UpdateHalfPay_waitTime < 1f)
		{
			return;
		}
		UpdateHalfPay_waitTime = 0f;
		double halfPayFirstTipLastTime = PlayerInfo.Instance.GetPlayerOtherInfoSaveData().getHalfPayFirstTipLastTime();
		if (halfPayFirstTipLastTime > 0.0)
		{
			if (!tipsHalfPay.activeInHierarchy)
			{
				tipsHalfPay.SetActive(value: true);
			}
			string text = WordGlobal.ChangeTimeToNormalSpecial((long)halfPayFirstTipLastTime, 0, isNeedDay: false);
			tipsHalfPay_text.text = text;
		}
		else if (tipsHalfPay.activeInHierarchy)
		{
			tipsHalfPay.SetActive(value: false);
		}
	}

	public void SetShowHuoDongUI()
	{
		isShowHuoDongUI = true;
		if (_showResultUI)
		{
			showHuoDongUiWaitTime = 0.8f;
		}
		else
		{
			showHuoDongUiWaitTime = 0.3f;
		}
	}

	public void UpdateHuoDongOpenTip()
	{
		if (WordGlobal.isUpdateMainUIHuoDong && !_showResultUI)
		{
			if (hdmtip != null && hdmtip.cur_state == 0)
			{
				hdmtip.Init(isRunningInit: true);
			}
			WordGlobal.isUpdateMainUIHuoDong = false;
		}
	}

	public void UpdateShowHuoDongUI()
	{
		if (!isShowHuoDongUI || _showResultUI)
		{
			return;
		}
		showHuoDongUiWaitTime -= Time.deltaTime;
		if (showHuoDongUiWaitTime <= 0f)
		{
			int huoDongTimeLeft = WordGlobal.GetHuoDongTimeLeft();
			if (huoDongTimeLeft > 0 && PlayerInfo.Instance.GetHuoDongSaveData().HuoDongID > 0 && PlayerInfo.Instance.GetHuoDongSaveData().GetRewardState == 0 && GameObject.Find("HuoDongUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongUI", isPop: true);
			}
			isShowHuoDongUI = false;
		}
	}

	public void UpdateCheckDPCTutorial()
	{
		if (CheckDPCTutorial)
		{
			dpc_tutorial_waitTime += Time.deltaTime;
			if (dpc_tutorial_waitTime > 0f)
			{
				if ((PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 1 || PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 2) && GameObject.Find("DPCTutorialUI") == null)
				{
					UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCTutorialUI").name = "DPCTutorialUI";
				}
				CheckDPCTutorial = false;
				dpc_tutorial_waitTime = 0f;
			}
		}
		if (HalfPayFirstTipTutorial_waitTime > 0f)
		{
			HalfPayFirstTipTutorial_waitTime -= Time.deltaTime;
			if (HalfPayFirstTipTutorial_waitTime <= 0f)
			{
				UIManager.Instance.LoadMainUI("UI/Main/TutorialTipsHalfPay", isPop: true);
				PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHalfPayFirstTipTutorial();
			}
		}
	}

	private void Awake()
	{
		_dictionaryList = new List<string>();
		m_RtfTop.localPosition = new Vector3(0f, 0f - WordGlobal.GetSaftTopSize(), 0f);
		WordGlobal.InitLabel(base.transform);
		Instance = this;
		_effectControl = GetComponent<MainUIAniControlScript>();
		InitUI();
		InitUIAniPar();
	}

	private IEnumerator InitTutorial(float time = 0f)
	{
		if (PlayerInfo.Instance.TutorialStep == 1)
		{
			time = 0.7f;
		}
		if (PlayerInfo.Instance.TutorialStep == 0 || PlayerInfo.Instance.TutorialStep == 1)
		{
			m_PauseLetterTime = time;
		}
		yield return new WaitForSeconds(time);
		m_ObjTutorial.SetActive(value: true);
		if (PlayerInfo.Instance.TutorialStep == 0)
		{
			m_ObjTutorial.GetComponent<TutorialScript>().SetText(string.Format(WordGlobal.ReadText(1001), _item.WordList[0].Word));
			SetTutorialWord(_item.WordList[0]);
			m_UILetterRoot.GetComponent<Canvas>().overrideSorting = true;
			m_TfLettersRoot.GetComponent<Canvas>().sortingOrder = 22;
			m_ObjTS.SetActive(PlayerInfo.Instance.ShowTermsService == 0);
		}
		else if (PlayerInfo.Instance.TutorialStep == 1)
		{
			m_UILetterRoot.GetComponent<Canvas>().overrideSorting = true;
			m_TfLettersRoot.GetComponent<Canvas>().sortingOrder = 22;
			m_ObjTutorial.GetComponent<TutorialScript>().SetText(string.Format(WordGlobal.ReadText(1002), _item.WordList[1].Word));
			SetTutorialWord(_item.WordList[1]);
		}
	}

	private void SetTutorialWord(CrossWordItem item)
	{
		m_TfItemTempRoot.GetComponent<RectTransform>().sizeDelta = m_TfItemRoot.GetComponent<RectTransform>().sizeDelta;
		WordGlobal.ClearChilds(m_TfItemTempRoot);
		for (int i = 0; i < item.Word.Length; i++)
		{
			int num = item.Row - 1;
			int num2 = item.Col - 1;
			if (item.vertical)
			{
				num += i;
			}
			else
			{
				num2 += i;
			}
			GameObject gameObject = UnityEngine.Object.Instantiate(m_ObjItem);
			gameObject.transform.SetParent(m_TfItemTempRoot, worldPositionStays: false);
			GameObject gameObject2 = GameObject.Find("item-" + num + "-" + num2);
			gameObject.transform.localPosition = gameObject2.transform.localPosition;
			gameObject.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = gameObject2.GetComponent<RectTransform>().sizeDelta;
			if (gameObject2.GetComponent<ItemScript>().m_TxtLetter.text != string.Empty)
			{
				gameObject.GetComponent<ItemScript>().SetLetter(gameObject2.GetComponent<ItemScript>().m_TxtLetter.text);
				gameObject.GetComponent<ItemScript>().SetImageState(2);
			}
			else
			{
				gameObject.GetComponent<ItemScript>().SetImageState(3);
			}
		}
		DrawTutorialLetterLine(item.Word);
	}

	private void InitUI()
	{
		if (_showResultUI)
		{
			return;
		}
		if (PlayerInfo.Instance.TutorialStep == 999)
		{
			if (PlayerInfo.Instance.FirstTipButtonUnlock == 1 || PlayerInfo.Instance.FirstTipsCount >= 1)
			{
				if (UIManager.CurMainUIType != UIManager.MainUIType.Type_Daily || PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial >= 3)
				{
					if (PlayerInfo.Instance.FirstTipButtonUnlock == 0)
					{
						if (UIManager.CurMainUIType != UIManager.MainUIType.Type_HDGardon)
						{
							UIManager.Instance.LoadMainUI("UI/Main/TutorialFirstTips", isPop: true);
						}
						PlayerInfo.Instance.FirstTipButtonUnlock = 1;
					}
					m_ObjBtnFirstLetter.SetActive(value: true);
				}
			}
			else
			{
				m_ObjBtnFirstLetter.SetActive(value: false);
			}
			if (AdsManager.Instance.IncentivizedAdState && PlayerInfo.Instance.LastOfferPayValue == 0f && PlayerInfo.Instance.MaxPlayLevel >= 8 && UIManager.CurMainUIType != UIManager.MainUIType.Type_HDGardon && UIManager.CurMainUIType != UIManager.MainUIType.Type_HDSun)
			{
				m_ObjBtnWatchVideo.SetActive(value: true);
			}
			else
			{
				m_ObjBtnWatchVideo.SetActive(value: false);
			}
			if (WordGlobal.BackKeyObjList.Count <= 0)
			{
			}
		}
		else
		{
			m_ObjBtnFirstLetter.SetActive(value: false);
			m_ObjBtnWatchVideo.SetActive(value: false);
		}
		m_ObjBtnAA.SetActive(_dictionaryList.Count > 0);
	}

	public void SetHuoDongItemShow()
	{
		huodong_ItemNum.gameObject.SetActive(value: true);
		string path = "Image/HuoDong/collection-activities_sun";
		huodong_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>(path);
		huodong_ItemAmount = huodong_sun_main.GetSunScore();
		huodong_ItemNum_text.text = huodong_ItemAmount.ToString();
	}

	public void SetBonusNum()
	{
		if (_bonusList == null || _bonusList.Count == 0)
		{
			BonusNum_txt.gameObject.SetActive(value: false);
			return;
		}
		BonusNum_txt.gameObject.SetActive(value: true);
		BonusNum_txt.text = _bonusList.Count + string.Empty;
	}

	public void InitShowBannerPos()
	{
		if (WordGlobal.isMainUIShowBanner && WordGlobal.isNeedBanner)
		{
			float num = (float)Screen.width / (float)Screen.height;
			float num2 = 0.95f;
			float num3 = 20f;
			WordGlobal.showBanner_bottomupy = 50f;
			if (num >= 0.57f && num <= 0.63f)
			{
				num2 = 0.9f;
				num3 = 45f;
				WordGlobal.showBanner_bottomupy = 100f;
			}
			else if (num > 0.63f)
			{
				num2 = 0.85f;
				num3 = 60f;
				WordGlobal.showBanner_bottomupy = 120f;
			}
			m_TfItemRoot.localScale = new Vector3(num2, num2, num2);
			m_TfTipsClickRoot.localScale = new Vector3(num2, num2, num2);
			RectTransform rectTransform = m_TfItemRoot as RectTransform;
			RectTransform rectTransform2 = rectTransform;
			Vector2 anchoredPosition = rectTransform.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition.x, 257f + num3);
			RectTransform rectTransform3 = m_TfTipsClickRoot as RectTransform;
			RectTransform rectTransform4 = rectTransform3;
			Vector2 anchoredPosition2 = rectTransform3.anchoredPosition;
			rectTransform4.anchoredPosition = new Vector2(anchoredPosition2.x, 257f + num3);
			RectTransform rectTransform5 = BottomTran as RectTransform;
			RectTransform rectTransform6 = rectTransform5;
			Vector2 anchoredPosition3 = rectTransform5.anchoredPosition;
			rectTransform6.anchoredPosition = new Vector2(anchoredPosition3.x, -333f + WordGlobal.showBanner_bottomupy);
			RectTransform rectTransform7 = m_ObjBtnBonus.transform as RectTransform;
			RectTransform rectTransform8 = rectTransform7;
			Vector2 anchoredPosition4 = rectTransform7.anchoredPosition;
			rectTransform8.anchoredPosition = new Vector2(anchoredPosition4.x, -533f + WordGlobal.showBanner_bottomupy);
			RectTransform btnDailyQuest_rect = BtnDailyQuest_rect;
			Vector2 anchoredPosition5 = rectTransform7.anchoredPosition;
			btnDailyQuest_rect.anchoredPosition = new Vector2(anchoredPosition5.x, -533f + WordGlobal.showBanner_bottomupy);
			RectTransform rectTransform9 = m_ObjBtnWatchVideo.transform as RectTransform;
			RectTransform rectTransform10 = rectTransform9;
			Vector2 anchoredPosition6 = rectTransform9.anchoredPosition;
			rectTransform10.anchoredPosition = new Vector2(anchoredPosition6.x, -83f + WordGlobal.showBanner_bottomupy);
		}
		else
		{
			m_TfItemRoot.localScale = Vector3.one;
			m_TfTipsClickRoot.localScale = Vector3.one;
			RectTransform rectTransform11 = m_TfItemRoot as RectTransform;
			RectTransform rectTransform12 = rectTransform11;
			Vector2 anchoredPosition7 = rectTransform11.anchoredPosition;
			rectTransform12.anchoredPosition = new Vector2(anchoredPosition7.x, 257f);
			RectTransform rectTransform13 = m_TfTipsClickRoot as RectTransform;
			RectTransform rectTransform14 = rectTransform13;
			Vector2 anchoredPosition8 = rectTransform13.anchoredPosition;
			rectTransform14.anchoredPosition = new Vector2(anchoredPosition8.x, 257f);
			RectTransform rectTransform15 = BottomTran as RectTransform;
			RectTransform rectTransform16 = rectTransform15;
			Vector2 anchoredPosition9 = rectTransform15.anchoredPosition;
			rectTransform16.anchoredPosition = new Vector2(anchoredPosition9.x, -333f);
			RectTransform rectTransform17 = m_ObjBtnBonus.transform as RectTransform;
			RectTransform rectTransform18 = rectTransform17;
			Vector2 anchoredPosition10 = rectTransform17.anchoredPosition;
			rectTransform18.anchoredPosition = new Vector2(anchoredPosition10.x, -533f);
			RectTransform btnDailyQuest_rect2 = BtnDailyQuest_rect;
			Vector2 anchoredPosition11 = rectTransform17.anchoredPosition;
			btnDailyQuest_rect2.anchoredPosition = new Vector2(anchoredPosition11.x, -533f);
			RectTransform rectTransform19 = m_ObjBtnWatchVideo.transform as RectTransform;
			RectTransform rectTransform20 = rectTransform19;
			Vector2 anchoredPosition12 = rectTransform19.anchoredPosition;
			rectTransform20.anchoredPosition = new Vector2(anchoredPosition12.x, -83f);
		}
	}

	public Color GetCurrentLineColor()
	{
		if (_curMapData != null)
		{
			return _curMapData.getLineColor();
		}
		if (default_linecolor.Equals(Color.white))
		{
			default_linecolor = WordGlobal.ChangeStringToColor("#9049fb");
		}
		return default_linecolor;
	}

	public Color GetCurrentTextColor()
	{
		if (_curMapData != null)
		{
			return _curMapData.getTextColor();
		}
		return Color.white;
	}

	public Color GetCurrentDishTypeColor(bool isLetter = false)
	{
		Debug.Log("Get Color");
		int num = 1;
		if (_curMapData != null)
		{
			num = _curMapData.Dishtype;
		}
		if (num == 1)
		{
			if (isLetter)
			{
				return Color.black;
			}
			return Color.white;
		}
		if (isLetter)
		{
			return Color.white;
		}
		return Color.black;
	}

	public void Init()
	{
		if (PlayerInfo.Instance.TutorialStep != 999 || PlayerInfo.Instance.AdRemove == 1)
		{
			WordGlobal.isMainUIShowBanner = false;
		}
		else
		{
			WordGlobal.isMainUIShowBanner = true;
			AdsManager.Instance.ShowBannerAD();
		}
		InitShowBannerPos();
		LetterState2Count = 0;
		m_DictionaryCurrentIndex = 0;
		_dictionaryList = new List<string>();
		if (PlayerInfo.Instance.NoPayPlayLevel >= 1 && (PlayerInfo.Instance.CurrentOfferItem == null || PlayerInfo.Instance.OfferEndTime < DateTime.Now))
		{
			PlayerInfo.Instance.GetBuyOffers();
		}
		WordGlobal.isUpdateMainUIHuoDong = false;
		_IsLastWord = false;
		showLastWordEff_time = 0f;
		_bonusTipList.Clear();
		CurBackLevelMapType = UIManager.LevelMapUIType.Type_Normal;
		if (PlayerInfo.Instance.MaxPlayLevel >= 5)
		{
			SetDailyQuestShow(isShow: true);
			if (PlayerInfo.Instance.IsAlreadyShowDailyQuestTutorial == 0 && UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
			{
				PlayerInfo.Instance.IsAlreadyShowDailyQuestTutorial = 1;
				if (PlayerInfo.Instance.GetDailyQuestSaveData().isCanClaim() > 0)
				{
					if (PlayerInfo.Instance.ShowTutorialDailyQuestFinish == 0)
					{
						PlayerInfo.Instance.ShowTutorialDailyQuestFinish = 1;
						UIManager.Instance.LoadMainUI("UI/Main/TutorialDailyQuestFinish", isPop: false, UIManager.MainUIType.Type_Common, 100);
					}
				}
				else
				{
					GameObject gameObject = UIManager.Instance.LoadMainUI("UI/DailyQuestTutorial", isPop: true);
					m_ObjBtnTipsClick.transform.SetAsFirstSibling();
					m_ObjBtnRandomLetter.transform.SetAsFirstSibling();
					WordGlobal.InitLabel(gameObject.transform);
				}
			}
		}
		else
		{
			SetDailyQuestShow(isShow: false);
		}
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			m_IsRePlayer = (PlayerInfo.Instance.CurrentLevel < PlayerInfo.Instance.MaxPlayLevel);
			_curMapData = MapDataManager.instance.GetMapData(PlayerInfo.Instance.CurrentLevel);
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			m_IsRePlayer = false;
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
		{
			m_IsRePlayer = false;
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
		{
			m_IsRePlayer = false;
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
		{
			m_IsRePlayer = false;
		}
		if ((UIManager.CurMainUIType == UIManager.MainUIType.Type_Common && PlayerInfo.Instance.MaxPlayLevel > MapDataManager.instance.GetMaxLevel() && PlayerInfo.Instance.CurrentLevel > MapDataManager.instance.GetMaxLevel()) || isCloseBackToLevelMap)
		{
			PlayerInfo.Instance.GameSaveDatas = null;
			BtnCloseClick();
			int huoDongTournamentTimeLeft = WordGlobal.GetHuoDongTournamentTimeLeft();
			if (huoDongTournamentTimeLeft > 0 && PlayerInfo.Instance.MaxPlayLevel > MapDataManager.instance.GetMaxLevel() && huoDongTournamentTimeLeft - WordGlobal.HuoDongTournamentFinishtTime > 0 && PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongID > 0 && PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardState != 2 && GameObject.Find("HuoDongTournamentUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTournamentUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
			}
			isCloseBackToLevelMap = false;
			return;
		}
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && PlayerInfo.Instance.DailyGameSaveDatas != string.Empty)
		{
			GameSaveData gameSaveData = JsonUtility.FromJson<GameSaveData>(PlayerInfo.Instance.DailyGameSaveDatas);
			if (gameSaveData.Level != PlayerInfo.Instance.GetDailyDayId())
			{
				PlayerInfo.Instance.DailyGameSaveDatas = string.Empty;
			}
		}
		if (huodong_effgo != null)
		{
			huodong_effgo.SetActive(value: false);
		}
		m_GoldItem.SetActive(value: true);
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
		{
			m_UILetterRoot.SetActive(value: false);
			m_UILetterRoot.transform.localScale = Vector3.one;
		}
		else
		{
			UIInAni(0);
		}
		m_ObjBtnTipsClick.SetActive(PlayerInfo.Instance.TutorialStep > 1);
		m_ObjBtnCellTipsClick.SetActive(PlayerInfo.Instance.MaxPlayLevel > 3);
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_EnterLevel);
		_showResultUI = false;
		m_ObjMask0.SetActive(value: false);
		m_TfItemRoot.gameObject.SetActive(value: true);
		_NextLevel = true;
		WordGlobal.IsGuideCheck = true;
		m_ObjBtnBonus.SetActive(PlayerInfo.Instance.FirstGetBonus == 1);
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			m_ImgBG.sprite = MapDataManager.instance.GetLevelSprite(PlayerInfo.Instance.CurrentLevel);
			UIManager.Instance.CreateBgEffect(m_ImgBG.transform, PlayerInfo.Instance.CurrentLevel);
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			m_ImgBG.sprite = WordJsonDataManager.instance.GetDailyPuzzleBg();
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
		{
			int flowerType = PlayerInfo.Instance.GetHuoDongGardonSaveData().GetFlowerType();
			string text = "garden_" + flowerType;
			string str = "Image/HuoDong/";
			Sprite sprite = null;
			if (text != null && text != string.Empty)
			{
				sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>(str + text);
			}
			m_ImgBG.sprite = sprite;
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
		{
			string path = "Image/HuoDong/collection activities_bg";
			m_ImgBG.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>(path);
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
		{
			int maxLevel = MapDataManager.instance.GetMaxLevel();
			m_ImgBG.sprite = MapDataManager.instance.GetLevelSprite(maxLevel);
			UIManager.Instance.CreateBgEffect(m_ImgBG.transform, maxLevel);
		}
		_tmpComboScore = 0;
		_CheckLeveling = false;
		SetUIHidden(hidden: false);
		m_TxtLevel.text = string.Format(WordGlobal.ReadText(1004), PlayerInfo.Instance.CurrentLevel);
		m_TxtLevel.transform.parent.gameObject.SetActive(UIManager.CurMainUIType == UIManager.MainUIType.Type_Common);
		DailyPuzzleObj.SetActive(UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily);
		_starButterNumList = new int[3];
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			_item = WordJsonDataManager.instance.GetItemByLevel(PlayerInfo.Instance.CurrentLevel);
			CheckBonus();
			if (PlayerInfo.Instance.CurrentLevel < PlayerInfo.Instance.MaxPlayLevel && PlayerInfo.Instance.Clear3Bonus == 0)
			{
				Clear3Bonus();
			}
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			int dailyDayId = PlayerInfo.Instance.GetDailyDayId();
			_item = WordJsonDataManager.instance.GetDailyItemByLevel(dailyDayId);
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
		{
			int huoDongGardonLevelId = PlayerInfo.Instance.GetHuoDongGardonSaveData().GetHuoDongGardonLevelId();
			_item = WordJsonDataManager.instance.GetItemByLevel(huoDongGardonLevelId);
			CheckBonus();
			if (_item == null)
			{
				Debug.LogError("huodong_gardon_levelid:" + huoDongGardonLevelId);
			}
			for (int i = 0; i < _item.Words.Count; i++)
			{
				Debug.Log("花园活动Words:" + _item.Words[i]);
			}
			for (int j = 0; j < _item.Bonus.Count; j++)
			{
				Debug.Log("花园活动Bonus:" + _item.Bonus[j]);
			}
			if (huodong_gardon_main == null)
			{
				GameObject gameObject2 = UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonMain");
				gameObject2.transform.SetParent(m_UILetterRoot.transform.parent);
				gameObject2.transform.localPosition = Vector3.zero;
				gameObject2.transform.localScale = Vector3.one;
				gameObject2.transform.SetAsLastSibling();
				(gameObject2.transform as RectTransform).anchoredPosition = (m_UILetterRoot.transform as RectTransform).anchoredPosition;
				huodong_gardon_main = gameObject2.GetComponent<HuoDongGardonMainUI>();
			}
			if (huodong_gardon_main != null)
			{
				huodong_gardon_main.Init();
			}
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
		{
			int curLevelID = PlayerInfo.Instance.GetHuoDongSunSaveData().GetCurLevelID();
			_item = WordJsonDataManager.instance.GetItemByLevel(curLevelID);
			CheckBonus();
			if (_item == null)
			{
				Debug.LogError("huodong_sun_levelid:" + curLevelID);
			}
			else
			{
				if (huodong_sun_main == null)
				{
					huodong_sun_main = base.gameObject.AddComponent<HuoDongSunMainUI>();
				}
				for (int k = 0; k < _item.Words.Count; k++)
				{
					Debug.Log("Solar activity Words:" + _item.Words[k]);
				}
				for (int l = 0; l < _item.Bonus.Count; l++)
				{
					Debug.Log("Solar activity Bonus:" + _item.Bonus[l]);
				}
			}
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
		{
			int curLevelID2 = PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetCurLevelID();
			_item = WordJsonDataManager.instance.GetItemByLevel(curLevelID2);
			CheckBonus();
			if (_item == null)
			{
				Debug.LogError("huodong_tournament_levelid:" + curLevelID2);
			}
			else
			{
				for (int m = 0; m < _item.Words.Count; m++)
				{
					Debug.Log("Tournament Words:" + _item.Words[m]);
				}
				for (int n = 0; n < _item.Bonus.Count; n++)
				{
					Debug.Log("Tournament Bonus:" + _item.Bonus[n]);
				}
			}
		}
		_currentWordList = new List<string>();
		_collectedMoneyWord = new List<string>();
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
		{
			_bonusList = new List<string>();
			_bonusList.Clear();
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			_bonusList = PlayerInfo.Instance.GetLevelBonusList(PlayerInfo.Instance.GetDailyDayId());
		}
		else
		{
			_bonusList = PlayerInfo.Instance.GetLevelBonusList(PlayerInfo.Instance.CurrentLevel);
		}
		SetBonusNum();
		setImgCurrentWordBG(isShow: false);
		InitWordPanel();
		_currentAddLetter = new List<string>();
		_tmpCompleteWordList = new List<string>();
		InitLetter();
		_currentLevelScore = 0;
		_tmpScore = 0;
		_ComboCount = 0;
		_tipsWord = null;
		_tipsCount = 0;
		StarBarFill.fillAmount = 0f;
		DailyButterFuNumText.gameObject.SetActive(value: false);
		huodong_ItemNum.gameObject.SetActive(value: false);
		int count = _item.WordList.Count;
		_starButterNumList[0] = Mathf.FloorToInt((float)_item.WordList.Count * 0.2f);
		_starButterNumList[1] = Mathf.FloorToInt((float)_item.WordList.Count * 0.5f);
		_starButterNumList[2] = Mathf.FloorToInt((float)_item.WordList.Count * 0.8f);
		DailyPuzzleStarNums[0].text = _starButterNumList[0].ToString();
		DailyPuzzleStarNums[1].text = _starButterNumList[1].ToString();
		DailyPuzzleStarNums[2].text = _starButterNumList[2].ToString();
		for (int num = 0; num < DailyPuzzleStars.Count; num++)
		{
			DailyPuzzleStars[num].transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
			RectTransform component = DailyPuzzleStars[num].GetComponent<RectTransform>();
			Vector2 sizeDelta = StarBarFill.GetComponent<RectTransform>().sizeDelta;
			float num2 = sizeDelta.x * ((float)_starButterNumList[num] / (float)count);
			RectTransform rectTransform = component;
			float x2 = num2;
			Vector2 anchoredPosition = component.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(x2, anchoredPosition.y);
			DailyPuzzleStarNums[num].gameObject.SetActive(value: true);
		}
		if (PlayerInfo.Instance.FirstGetMoneyWord == 0 && _item.MoneyWord.Length > 0)
		{
			WordGlobal.InitLabel(UIManager.Instance.LoadMainUI("UI/Main/GetGoldTipsUI", isPop: true).transform);
			PlayerInfo.Instance.FirstGetMoneyWord = 1;
		}
		if (PlayerInfo.Instance.NeedTutorialFirstTips)
		{
			PlayerInfo.Instance.NeedTutorialFirstTips = false;
			if (UIManager.CurMainUIType != UIManager.MainUIType.Type_HDGardon)
			{
				UIManager.Instance.LoadMainUI("UI/Main/TutorialFirstTips", isPop: true);
			}
		}
		if (UIManager.CurMainUIType != UIManager.MainUIType.Type_HDGardon && PlayerInfo.Instance.GetPlayerOtherInfoSaveData().getHalfPayFirstTipLastTime() > 0.0 && PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HalfPayFirstTipTutorial == 0)
		{
			if (PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial < 3)
			{
				HalfPayFirstTipTutorial_waitTime = 0.1f;
			}
			else
			{
				UIManager.Instance.LoadMainUI("UI/Main/TutorialTipsHalfPay", isPop: true);
				PlayerInfo.Instance.GetPlayerOtherInfoSaveData().setHalfPayFirstTipTutorial();
			}
		}
		if (!m_IsRePlayer && ((UIManager.CurMainUIType == UIManager.MainUIType.Type_Common && PlayerInfo.Instance.GameSaveDatas != string.Empty) || (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && PlayerInfo.Instance.DailyGameSaveDatas != string.Empty) || (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun && PlayerInfo.Instance.HuoDongSunGameSaveDatas != string.Empty)))
		{
			GameSaveData data = null;
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
			{
				data = JsonUtility.FromJson<GameSaveData>(PlayerInfo.Instance.GameSaveDatas);
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
			{
				data = JsonUtility.FromJson<GameSaveData>(PlayerInfo.Instance.DailyGameSaveDatas);
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
			{
				data = JsonUtility.FromJson<GameSaveData>(PlayerInfo.Instance.HuoDongSunGameSaveDatas);
			}
			try
			{
				data.InitGrid();
				if (((UIManager.CurMainUIType == UIManager.MainUIType.Type_Common && data.Level == PlayerInfo.Instance.CurrentLevel && data.Level == PlayerInfo.Instance.MaxPlayLevel) || (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && data.Level == PlayerInfo.Instance.GetDailyDayId()) || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun) && data.CheckGrid())
				{
					bool flag = true;
					if (data.Grid.Count == _grid.Count && data.Grid[0].Count == _grid[0].Count)
					{
						for (int num3 = 0; num3 < data.Grid.Count; num3++)
						{
							if (!flag)
							{
								break;
							}
							for (int num4 = 0; num4 < data.Grid[num3].Count; num4++)
							{
								if ((data.Grid[num3][num4] != "-" && _grid[num3][num4] == "-") || (data.Grid[num3][num4] == "-" && _grid[num3][num4] != "-") || (data.Grid[num3][num4] != "-" && data.Grid[num3][num4] != string.Empty && data.Grid[num3][num4] != GetCellLetter(num3 + 1, num4 + 1)))
								{
									flag = false;
									break;
								}
							}
						}
					}
					else
					{
						flag = false;
					}
					if (flag)
					{
						_bonusList = data.BonusList;
						SetBonusNum();
						_dictionaryList = data.DictList;
						if (data.CurrentTipsWord != string.Empty)
						{
							_tipsWord = _item.WordList.Find((CrossWordItem x) => x.Word == data.CurrentTipsWord);
						}
						_currentWordList = data.Words;
						_currentLevelScore = (data.CurrentScore += data.ComboScore);
						InitPanelByGrid(data.Grid);
						if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
						{
							_curGetButterFlyNum = data.CurButterScore;
							_curGetStarNum = data.CurStarNums;
							_curButterFlyTVCount = data.ButterFlyTVCount;
							StarBarFill.fillAmount = (float)_curGetButterFlyNum / (float)_item.WordList.Count;
							ChangeDailyStarObjPos();
							DailyButterFuNumText.text = _curGetButterFlyNum.ToString();
							DailyButterFuNumText.gameObject.SetActive(_curGetButterFlyNum > 0);
							if (_curGetStarNum > 0)
							{
								for (int num5 = 0; num5 < _curGetStarNum; num5++)
								{
									DailyPuzzleStars[num5].transform.localScale = Vector3.one;
									DailyPuzzleStarNums[num5].gameObject.SetActive(value: false);
								}
							}
							string[] array = data.ButterFlyPos.Split('_');
							if (array.Length >= 2 && data.ButterFlyWord != string.Empty)
							{
								int row = int.Parse(array[0]);
								int col = int.Parse(array[1]);
								CrossWordItem cItem = _item.WordList.Find((CrossWordItem x) => x.Word == data.ButterFlyWord);
								CreateSaveDataButter(row, col, cItem);
							}
							InitDPButterFlyTV();
						}
						else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
						{
							if (_item != null)
							{
								huodong_sun_main.Init(data);
							}
							SetHuoDongItemShow();
						}
						if (_currentWordList.Count >= _item.WordList.Count - 1)
						{
							CheckLevel();
						}
						_ComboCount = 0;
					}
					else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
					{
						if (_item != null)
						{
							huodong_sun_main.Init(null);
						}
						SetHuoDongItemShow();
					}
				}
			}
			catch (Exception)
			{
			}
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			InitDailyPuzzleData();
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
		{
			if (_item != null && huodong_sun_main != null)
			{
				huodong_sun_main.Init(null);
			}
			SetHuoDongItemShow();
		}
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			LevelRectTran.gameObject.SetActive(value: true);
			InitCommonButtterFlyTV();
			if (hdmtip == null)
			{
				GameObject gameObject3 = UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongMainTip");
				gameObject3.transform.SetParent(base.transform);
				gameObject3.transform.localPosition = Vector3.zero;
				gameObject3.transform.localScale = Vector3.one;
				gameObject3.transform.SetAsLastSibling();
				hdmtip = gameObject3.GetComponent<HuoDongMainTip>();
				hdmtip.Init();
			}
			if (WordGlobal.isNeedHuoDong)
			{
				if (huodong_tournament_main != null)
				{
					huodong_tournament_main.gameObject.SetActive(value: false);
				}
				if (WordGlobal.GetHuoDongTimeLeft() > 0)
				{
					if (PlayerInfo.Instance.GetHuoDongSaveData().HuoDongID == 0)
					{
						PlayerInfo.Instance.GetHuoDongSaveData().InitHuoDongID();
					}
					if (PlayerInfo.Instance.GetHuoDongSaveData().HuoDongID > 0 && PlayerInfo.Instance.GetHuoDongSaveData().GetRewardState == 0)
					{
						EventDataCsv dataById = EventDataManagerCsv.instance.GetDataById(PlayerInfo.Instance.GetHuoDongSaveData().HuoDongID);
						bool flag2 = false;
						if (PlayerInfo.Instance.CurrentLevel >= PlayerInfo.Instance.GetHuoDongSaveData().GetItemMaxLevel && PlayerInfo.Instance.CurrentLevel >= dataById.startlv && PlayerInfo.Instance.GetHuoDongSaveData().IsUseLevel(PlayerInfo.Instance.CurrentLevel))
						{
							flag2 = true;
						}
						Debug.Log("PlayerInfo.Instance.MaxPlayLevel:" + PlayerInfo.Instance.MaxPlayLevel + "PlayerInfo.Instance.CurrentLevel:" + PlayerInfo.Instance.CurrentLevel + " itemmaxlevel:" + PlayerInfo.Instance.GetHuoDongSaveData().GetItemMaxLevel + " hd_data.startlv:" + dataById.startlv);
						if (flag2)
						{
							if (PlayerInfo.Instance.CurrentLevel > PlayerInfo.Instance.GetHuoDongSaveData().GetItemMaxLevel && PlayerInfo.Instance.CurrentLevel != PlayerInfo.Instance.GetHuoDongSaveData().NoUseLevel)
							{
								if (huodong_main == null)
								{
									GameObject gameObject4 = UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongMainUI");
									gameObject4.transform.SetParent(base.transform);
									gameObject4.transform.localPosition = Vector3.zero;
									gameObject4.transform.localScale = Vector3.one;
									gameObject4.transform.SetAsLastSibling();
									huodong_main = gameObject4.GetComponent<HuoDongMainUI>();
								}
								if (huodong_main != null)
								{
									EventDataCsv dataById2 = EventDataManagerCsv.instance.GetDataById(PlayerInfo.Instance.GetHuoDongSaveData().HuoDongID);
									if (PlayerInfo.Instance.GetHuoDongSaveData().HuoDongItemAmount == dataById2.requireamount)
									{
										huodong_main.Show(isShow: false);
									}
									else
									{
										huodong_main.Init(_item);
									}
								}
							}
							if (PlayerInfo.Instance.CurrentLevel > PlayerInfo.Instance.GetHuoDongSaveData().GetItemMaxLevel || PlayerInfo.Instance.MaxPlayLevel <= PlayerInfo.Instance.GetHuoDongSaveData().GetItemMaxLevel)
							{
								huodong_ItemNum.gameObject.SetActive(value: true);
								huodong_ItemAmount = PlayerInfo.Instance.GetHuoDongSaveData().HuoDongItemAmount;
								huodong_ItemNum_text.text = huodong_ItemAmount.ToString();
								LevelRectTran.gameObject.SetActive(value: false);
							}
							if (PlayerInfo.Instance.GetHuoDongSaveData().Tutorial01 == 0 && PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HuoDongTutorial02 <= 1)
							{
								if (GameObject.Find("HuoDongTutorial01") == null)
								{
									HuoDongTutorial01.TextId = 1203;
									UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTutorial01", isPop: true);
								}
								PlayerInfo.Instance.GetHuoDongSaveData().SetTutorial01(1);
							}
						}
						else
						{
							if (huodong_main != null)
							{
								huodong_main.Show(isShow: false);
							}
							huodong_ItemNum.gameObject.SetActive(value: false);
						}
					}
				}
				else if (WordGlobal.GetHuoDongTournamentTimeLeft() > WordGlobal.HuoDongTournamentFinishtTime)
				{
					InitTournament();
				}
			}
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament && WordGlobal.GetHuoDongTournamentTimeLeft() > WordGlobal.HuoDongTournamentFinishtTime)
		{
			InitTournament();
		}
		m_TxtStarCount.text = _currentLevelScore.ToString();
		m_ImgWordBG.color = GetCurrentLineColor();
		m_TxtCurrentWord.color = GetCurrentTextColor();
		m_UILetterRoot.GetComponent<Image>().color = GetCurrentDishTypeColor();
		if (PlayerInfo.Instance.CurrentLevel == 3 && PlayerInfo.Instance.TutorialStep == 2)
		{
			GameObject gameObject5 = UIManager.Instance.LoadMainUI("UI/Main/TutorialLevel3");
			m_ObjBtnTipsClick.transform.SetParent(gameObject5.transform);
			m_ObjBtnRandomLetter.transform.SetParent(gameObject5.transform);
			m_ObjBtnTipsClick.transform.SetAsFirstSibling();
			m_ObjBtnRandomLetter.transform.SetAsFirstSibling();
			WordGlobal.InitLabel(gameObject5.transform);
			PlayerInfo.Instance.TutorialStep = 3;
		}
		if (PlayerInfo.Instance.CurrentLevel == 4 && PlayerInfo.Instance.TutorialStep == 3)
		{
			m_ObjBtnCellTipsClick.GetComponent<BtnNumberPriceSet>().ShowClostTip(show: true);
			GameObject gameObject6 = ResourceLoadManager.GetInstance().LoadUI("UI/Main/TutorialLevel4", base.transform);
			gameObject6.name = "TutorialLevel4";
			WordGlobal.InitLabel(gameObject6.transform);
			m_ObjBtnCellTipsClick.GetComponent<Canvas>().sortingOrder = 18;
			m_ObjBtnCellTipsClick.GetComponent<Canvas>().overrideSorting = true;
		}
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			if (PlayerInfo.Instance.CurrentLevel == 10 && PlayerInfo.Instance.MaxPlayLevel == 10 && PlayerInfo.Instance.AdRemove == 0 && PlayerInfo.Instance.RemoveAdTips == 0)
			{
				PlayerInfo.Instance.RemoveAdTips = 1;
				WordGlobal.InitLabel(UIManager.Instance.LoadMainUI("UI/Main/RemoveAdTips", isPop: true).transform);
				m_GoldItem.GetComponent<Canvas>().overrideSorting = true;
			}
			else
			{
				m_GoldItem.GetComponent<Canvas>().overrideSorting = false;
			}
		}
		_NextLevel = false;
		if (PlayerInfo.Instance.TutorialStep < 2)
		{
			StartCoroutine(InitTutorial(0.5f));
		}
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
		{
			StartCoroutine(UIInAniGardon());
		}
		else
		{
			UIInAni(1);
		}
		if (UIManager.CurMainUIType != UIManager.MainUIType.Type_HDGardon && UIManager.CurMainUIType != UIManager.MainUIType.Type_HDSun && UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			if (WordJsonDataManager.instance.IsOpenDailyPuzzle() && PlayerInfo.Instance.CurrentLevel > WordGlobal.OpenDailyPuzzleLevel && !isShowDPCTutorialTip && PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 0)
			{
				isShowDPCTutorialTip = true;
				GameObject gameObject7 = UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCTutorialTip", isPop: true);
				gameObject7.name = "DPCTutorialTip";
				gameObject7.GetComponent<DPCTutorialTip>().Type = 0;
			}
			if (WordGlobal.OpenWordSearch && PlayerInfo.Instance.CurrentLevel > WordGlobal.OpenDailySearchLevel && !isShowDPCTutorialTip_search && PlayerInfo.Instance.GetWordSearchSaveData().Tutorial01 == 0)
			{
				isShowDPCTutorialTip_search = true;
				GameObject gameObject8 = UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCTutorialTip", isPop: true);
				gameObject8.name = "DPCTutorialTip";
				gameObject8.GetComponent<DPCTutorialTip>().Type = 1;
			}
		}
		ResourceLoadManager.GetInstance().HideNetWaiting();
		if (_bonusList.Count > 0 && _minWordLength > 3 && PlayerInfo.Instance.Clear3Bonus == 0)
		{
			List<string> list = WordGlobal.ShuffleList(_bonusList);
			_bonusList.Clear();
			for (int num6 = 0; num6 < list.Count; num6++)
			{
				if (list[num6].Length > 3)
				{
					_bonusList.Add(list[num6]);
				}
			}
			Clear3Bonus();
			SetBonusNum();
		}
		WordFaceBook.Instance.GetHuoDongData();
	}

	public void InitTournament()
	{
		if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongID == 0)
		{
			PlayerInfo.Instance.GetHuoDongTournamentSaveData().InitHuoDongID();
		}
		if (PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongID <= 0)
		{
			return;
		}
		EventDataCsv dataById = EventDataManagerCsv.instance.GetDataById(PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongID);
		if (PlayerInfo.Instance.CurrentLevel >= dataById.startlv && !m_IsRePlayer)
		{
			if (huodong_tournament_main == null)
			{
				GameObject gameObject = UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongTournamentMainItem");
				gameObject.transform.SetParent(Top_rectTran);
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.transform.localScale = Vector3.one;
				(gameObject.transform as RectTransform).anchoredPosition = Vector2.zero;
				gameObject.transform.SetAsLastSibling();
				huodong_tournament_main = gameObject.GetComponent<HuoDongTournamentMainItem>();
			}
			if (huodong_tournament_main != null)
			{
				huodong_tournament_main.Init();
			}
			LevelRectTran.gameObject.SetActive(value: false);
		}
	}

	public void InitDPButterFlyTV()
	{
		if (UIManager.CurMainUIType != UIManager.MainUIType.Type_Daily || _curButterFlyTVCount >= 3 || !(GameObject.Find("ButterFlyTV") == null))
		{
			return;
		}
		ButterFlyTV.ButterType = 0;
		GameObject gameObject = UnityEngine.Object.Instantiate(ResourceLoadManager.GetInstance().LoadObjByPath<GameObject>("UI/DailyPuzzle/ButterFlyTV"));
		gameObject.name = "ButterFlyTV";
		gameObject.transform.SetParent(base.transform);
		gameObject.transform.localPosition = Vector3.zero;
		(gameObject.transform as RectTransform).anchoredPosition = new Vector2(0f, 0f);
		gameObject.transform.localScale = Vector3.one;
		if (AdsManager.Instance.IncentivizedAdState)
		{
			gameObject.SetActive(value: true);
			return;
		}
		ButterFlyTV component = gameObject.GetComponent<ButterFlyTV>();
		component.move_go.SetActive(value: false);
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			component.WaitWifi();
		}
	}

	public void InitCommonButtterFlyTV()
	{
		if (PlayerInfo.Instance.CurrentLevel <= PlayerInfo.Instance.GetPlayerOtherInfoSaveData().ShowButterFlyTVCommonLevel || PlayerInfo.Instance.LastOfferPayValue > 0f)
		{
			return;
		}
		WordGlobal.showButterFlyTV_per = 0;
		if (PlayerInfo.Instance.MaxPlayLevel <= 54)
		{
			for (int i = 0; i < showButterFlyTVLevels.Length; i++)
			{
				if (PlayerInfo.Instance.CurrentLevel == PlayerInfo.Instance.MaxPlayLevel && PlayerInfo.Instance.MaxPlayLevel == showButterFlyTVLevels[i])
				{
					WordGlobal.showButterFlyTV_per = 100;
				}
			}
		}
		else if (PlayerInfo.Instance.CurrentLevel == PlayerInfo.Instance.MaxPlayLevel && PlayerInfo.Instance.MaxPlayLevel % 6 == 0)
		{
			WordGlobal.showButterFlyTV_per = 100;
		}
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
		{
			PlayerInfo.Instance.GetPlayerOtherInfoSaveData().SetShowButterFlyTVCommonLevel(PlayerInfo.Instance.CurrentLevel);
		}
	}

	private void InitDailyPuzzleData()
	{
		int index = UnityEngine.Random.Range(0, _item.WordList.Count);
		CrossWordItem crossWordItem = _item.WordList[index];
		if (PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 1)
		{
			int num = 99;
			for (int i = 0; i < _item.WordList.Count; i++)
			{
				if (!_item.WordList[i].vertical && num > _item.WordList[i].Row)
				{
					crossWordItem = (DPCTutorialUI.WordItem = _item.WordList[i]);
					num = _item.WordList[i].Row;
				}
			}
			CheckDPCTutorial = true;
		}
		else
		{
			crossWordItem = _item.WordList[index];
			if (PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 2)
			{
				DPCTutorialUI.WordItem = crossWordItem;
				CheckDPCTutorial = true;
			}
		}
		SetCell(crossWordItem.Col, crossWordItem.Row, crossWordItem.Word.Substring(0, 1), initItem: true, removeGold: true);
		int num2 = crossWordItem.Col;
		int num3 = crossWordItem.Row;
		if (crossWordItem.vertical)
		{
			num3++;
		}
		else
		{
			num2++;
		}
		CreateButterFly(num3, num2, crossWordItem, isNewWord: true);
		InitDPButterFlyTV();
	}

	public CrossWordItem GetRandomWord()
	{
		int index = UnityEngine.Random.Range(0, _item.WordList.Count);
		return _item.WordList[index];
	}

	public void CreateSaveDataButter(int row, int col, CrossWordItem cItem)
	{
		GameObject itemByRC = GetItemByRC(row - 1, col - 1);
		if (itemByRC != null)
		{
			ItemScript component = itemByRC.GetComponent<ItemScript>();
			_curButterColorIndex = UnityEngine.Random.Range(0, _buttColorList.Length);
			Color color = WordGlobal.ChangeStringToColor(_buttColorList[_curButterColorIndex]);
			component.ShowButterFly(color, isDelay: false);
			if (PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 1)
			{
				if (DPCTutorialUI.WordItem == null)
				{
					DPCTutorialUI.WordItem = cItem;
				}
				CheckDPCTutorial = true;
			}
			else if (PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 2)
			{
				DPCTutorialUI.WordItem = cItem;
				CheckDPCTutorial = true;
			}
		}
		_curButterInWordItem = cItem;
		_curButterFlyPos = row + "_" + col;
	}

	private void CreateButterFly(int row, int col, CrossWordItem cItem, bool isNewWord)
	{
		WordGlobal.DebugLog("ButterFlyWord :  " + cItem.Word);
		TestWord = cItem.Word;
		_lastButtFlyColorIndex = _curButterColorIndex;
		GameObject itemByRC = GetItemByRC(row - 1, col - 1);
		bool flag = _curButterInWordItem != null && cItem != null && _curButterInWordItem.Word == cItem.Word && !isNewWord;
		if (_curButterFlyPos != string.Empty && _curButterFlyPos != null)
		{
			Vector3 endPos = (!flag) ? StarPosObj.transform.position : itemByRC.transform.position;
			ButterFlyMove(flag, endPos);
		}
		if (itemByRC != null)
		{
			ItemScript component = itemByRC.GetComponent<ItemScript>();
			if (_curButterInWordItem != null && flag)
			{
				_curButterColorIndex = _lastButtFlyColorIndex;
			}
			else
			{
				_curButterColorIndex = UnityEngine.Random.Range(0, _buttColorList.Length);
				if (_curButterColorIndex == _lastButtFlyColorIndex)
				{
					_curButterColorIndex = ((_curButterColorIndex + 1 < _buttColorList.Length) ? (_curButterColorIndex + 1) : 0);
				}
			}
			Color color = WordGlobal.ChangeStringToColor(_buttColorList[_curButterColorIndex]);
			component.ShowButterFly(color, flag);
		}
		_curButterInWordItem = cItem;
		_curButterFlyPos = row + "_" + col;
	}

	public void ButterFlyMove(bool isSameWord, Vector3 endPos)
	{
		GameObject gameObject = null;
		int num = 0;
		int num2 = 0;
		string[] array = null;
		if (_curMoveButterPos != null && _curMoveButterPos != string.Empty && isSameWord)
		{
			array = _curMoveButterPos.Split('_');
			num = int.Parse(array[0]);
			num2 = int.Parse(array[1]);
			gameObject = GetItemByRC(num - 1, num2 - 1);
			if (gameObject != null && gameObject.GetComponent<ItemScript>().m_IsMoveToStar)
			{
				array = _curButterFlyPos.Split('_');
			}
		}
		else
		{
			array = _curButterFlyPos.Split('_');
		}
		num = int.Parse(array[0]);
		num2 = int.Parse(array[1]);
		gameObject = GetItemByRC(num - 1, num2 - 1);
		if (gameObject != null)
		{
			ItemScript component = gameObject.GetComponent<ItemScript>();
			component.ShowButterObj();
			if (isSameWord)
			{
				component.ButterFlyMove(endPos, isToNextItem: true);
			}
			else
			{
				AddButterData();
				component.ButterFlyMove(endPos, isToNextItem: false);
			}
		}
		_curMoveButterPos = num + "_" + num2;
	}

	public void ChangeDailyStarObjPos()
	{
		Vector2 sizeDelta = StarBarFill.GetComponent<RectTransform>().sizeDelta;
		float x = sizeDelta.x;
		float num = x * StarBarFill.fillAmount;
		RectTransform component = StarPosObj.GetComponent<RectTransform>();
		RectTransform rectTransform = component;
		float x2 = num;
		Vector2 anchoredPosition = component.anchoredPosition;
		rectTransform.anchoredPosition = new Vector3(x2, anchoredPosition.y, 0f);
	}

	public void RefreshButterFly(bool isWordComplete)
	{
		if (isWordComplete)
		{
			InitNewButterWord(isWordComplete);
		}
		else
		{
			AddButterFlyByWord(_curButterInWordItem, isWordComplete);
		}
	}

	public void InitNewButterWord(bool isWordComplete)
	{
		List<CrossWordItem> haveCharInUnFinishList = GetHaveCharInUnFinishList();
		if (haveCharInUnFinishList.Count > 0)
		{
			int index = UnityEngine.Random.Range(0, haveCharInUnFinishList.Count);
			CrossWordItem item = haveCharInUnFinishList[index];
			AddButterFlyByWord(item, isWordComplete);
		}
		else
		{
			ButterFlyMove(isSameWord: false, StarPosObj.transform.position);
		}
	}

	public void AddButterFlyByWord(CrossWordItem item, bool isNewWord)
	{
		int col = item.Col;
		int row = item.Row;
		if (PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 1)
		{
			if (DPCTutorialUI.WordItem != null)
			{
				DPCTutorialUI.HideState = 2;
				PlayerInfo.Instance.SetDailyImageDataTutorial(2);
			}
			else if (DPCTutorialUI.WordItem == null)
			{
				DPCTutorialUI.HideState = 3;
			}
		}
		if (PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial == 2)
		{
			DPCTutorialUI.WordItem = item;
			DPCTutorialUI.isWordItemChange = true;
		}
		int num = 0;
		while (true)
		{
			if (num < item.Word.Length)
			{
				if (item.vertical)
				{
					row = item.Row + num;
				}
				else
				{
					col = item.Col + num;
				}
				if (GetCell(col, row) == string.Empty)
				{
					break;
				}
				num++;
				continue;
			}
			return;
		}
		CreateButterFly(row, col, item, isNewWord);
	}

	private void InitWordPanel()
	{
		_cw = new Crossword();
		_grid = _cw.InitGrid(_item.MaxRow, _item.MaxCol);
		for (int i = 0; i < _item.WordList.Count; i++)
		{
			SetWordEmpty(_item.WordList[i]);
		}
		_cw._grid = _grid;
		float num = _itemWith;
		float num2 = _itemHeight;
		if ((float)_item.MaxCol * (_itemWith + _itemSpacing) + 20f > _panelWith)
		{
			num = (num2 = (_panelWith - 20f) / (float)_item.MaxCol - _itemSpacing);
		}
		if ((float)_item.MaxRow * (_itemHeight + _itemSpacing) + 20f > _panelHeight)
		{
			float num3 = (_panelHeight - 20f) / (float)_item.MaxRow - _itemSpacing;
			if (num3 < num)
			{
				num = (num2 = num3);
			}
		}
		m_TfItemRoot.GetComponent<RectTransform>().sizeDelta = new Vector2((float)_item.MaxCol * (num + _itemSpacing) + 20f, (float)_item.MaxRow * (num2 + _itemSpacing) + 20f);
		m_TfTipsClickRoot.GetComponent<RectTransform>().sizeDelta = m_TfItemRoot.GetComponent<RectTransform>().sizeDelta;
		m_TfItemRoot.GetComponent<GridLayoutGroup>().cellSize = new Vector2(num, num2);
		m_TfTipsClickRoot.GetComponent<GridLayoutGroup>().cellSize = new Vector2(num, num2);
		WordGlobal.ClearChilds(m_TfItemRoot);
		m_TfItemRoot.GetComponent<GridLayoutGroup>().enabled = true;
		for (int j = 0; j < _grid.Count; j++)
		{
			for (int k = 0; k < _grid[j].Count; k++)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate(m_ObjItem);
				if (_grid[j][k] == "-")
				{
					gameObject.GetComponent<ItemScript>().SetImageState(0);
				}
				else
				{
					gameObject.GetComponent<ItemScript>().SetImageState(1);
				}
				gameObject.transform.SetParent(m_TfItemRoot, worldPositionStays: false);
				ItemScript component = gameObject.GetComponent<ItemScript>();
				if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
				{
					GameObject gameObject2 = UnityEngine.Object.Instantiate(m_ObjButterFly);
					gameObject2.transform.SetParent(component.m_ObjImage.transform);
					component.m_ItemButter = gameObject2.GetComponent<ButterFlyItem>();
					gameObject2.transform.localPosition = Vector3.zero;
					gameObject2.SetActive(value: false);
				}
				component.SetImgSize(num, num2);
				gameObject.SetActive(value: false);
				gameObject.name = "item-" + j.ToString() + "-" + k.ToString();
			}
		}
		if (m_IsRePlayer || string.IsNullOrEmpty(_item.MoneyWord))
		{
			return;
		}
		CrossWordItem crossWordItem = _item.WordList.Find((CrossWordItem x) => x.Word == _item.MoneyWord);
		int num4 = crossWordItem.Row;
		int num5 = crossWordItem.Col;
		for (int l = 0; l < crossWordItem.Word.Length; l++)
		{
			GameObject letterItem = GetLetterItem(num4, num5);
			GameObject gameObject3 = ResourceLoadManager.GetInstance().LoadUI("UI/Main/GoldItem", letterItem.transform);
			gameObject3.transform.localScale = Vector3.one * (num / _itemWith);
			gameObject3.name = "gold";
			GoldItem component2 = gameObject3.GetComponent<GoldItem>();
			bool isEnd = false;
			if (l == crossWordItem.Word.Length - 1)
			{
				isEnd = true;
			}
			component2.Init(0.5f + (float)l * 0.2f, isEnd);
			if (crossWordItem.vertical)
			{
				num4++;
			}
			else
			{
				num5++;
			}
		}
	}

	private void InitPanelByGrid(List<List<string>> grid)
	{
		for (int i = 0; i < grid.Count; i++)
		{
			for (int j = 0; j < grid[i].Count; j++)
			{
				if (grid[i][j] != "-" && grid[i][j] != string.Empty && grid[i][j] != "|")
				{
					SetCell(j + 1, i + 1, grid[i][j], initItem: true, removeGold: true);
				}
			}
		}
	}

	private void SetWord(CrossWordItem word, bool initItem = false)
	{
		int num = word.Col;
		int num2 = word.Row;
		for (int i = 0; i < word.Word.Length; i++)
		{
			SetCell(num, num2, word.Word.Substring(i, 1), initItem);
			if (word.vertical)
			{
				num2++;
			}
			else
			{
				num++;
			}
		}
	}

	private void SetWordEmpty(CrossWordItem word)
	{
		int num = word.Col;
		int num2 = word.Row;
		for (int i = 0; i < word.Word.Length; i++)
		{
			SetCell(num, num2, string.Empty, initItem: false);
			if (word.vertical)
			{
				num2++;
			}
			else
			{
				num++;
			}
		}
	}

	public void SetCell(int col, int row, string str, bool initItem, bool removeGold = false)
	{
		_grid[row - 1][col - 1] = str;
		if (initItem)
		{
			SetCellState(col, row, str, removeGold);
		}
		if (CheckGridCompleted() && !initItem && str != string.Empty)
		{
            ResourceLoadManager.GetInstance().ShowNetWaiting();
        }
	}

	public void SetCellState(string cellName, string str, bool removeGold = false)
	{
		string[] array = cellName.Split('-');
		_cellTipsRow = int.Parse(array[1]);
		_cellTipsCol = int.Parse(array[2]);
		SetCellState(_cellTipsCol + 1, _cellTipsRow + 1, str, removeGold);
	}

	public void SetCellState(int col, int row, string str, bool removeGold = false)
	{
		string text = str;
		string text2 = "item-" + (row - 1).ToString() + "-" + (col - 1).ToString();
		int index = _item.MaxCol * (row - 1) + col - 1;
		int num = 1;
		GameObject gameObject = m_TfItemRoot.GetChild(index).gameObject;
		int imgState = gameObject.GetComponent<ItemScript>()._imgState;
		if (imgState == 1)
		{
			CheckWordCompleted(row, col);
		}
		if (!(str == "-"))
		{
			num = ((str == string.Empty) ? 1 : 2);
		}
		else
		{
			num = 0;
			text = string.Empty;
		}
		gameObject.GetComponent<ItemScript>().SetImageState(num);
		gameObject.GetComponent<ItemScript>().SetLetter(text);
		if (removeGold && num == 2)
		{
			Transform transform = gameObject.transform.Find("gold");
			if ((bool)transform)
			{
				UnityEngine.Object.Destroy(transform.gameObject);
				_collectedMoneyWord.Add(text);
			}
		}
		if (!_NextLevel && num == 2)
		{
			CheckMoneyWord(str, row, col);
		}
	}

	private string GetCell(int col, int row)
	{
		string empty = string.Empty;
		try
		{
			return _grid[row - 1][col - 1];
		}
		catch (Exception)
		{
			return _cw._empty;
		}
	}

	public bool CheckPointInList(GameObject obj)
	{
		string value = obj.name.Split('_')[1];
		if (_currentWord.IndexOf(value) >= 0)
		{
			return true;
		}
		return false;
	}

	public void AddLetter(GameObject obj)
	{
		SetLastOperationPoint();
		GuideLineDraw.GuideLetterShowState = 2;
		if (PlayerInfo.Instance.TutorialStep < 3)
		{
			m_UILetterRoot.GetComponent<Canvas>().overrideSorting = false;
			m_TfLettersRoot.GetComponent<Canvas>().sortingOrder = 1;
			m_ObjTutorial.SetActive(value: false);
		}
		string text = obj.name.Split('_')[1];
		int num = _currentWord.IndexOf(text);
		if (_currentAddLetter.Count > 1 && _currentAddLetter[_currentAddLetter.Count - 2] == obj.name)
		{
			_catmullDraw.RemovePoint(_currentAddLetter.Count - 1);
			m_TfLettersRoot.Find(_currentAddLetter[_currentAddLetter.Count - 1]).GetComponent<LetterScript>().RemovePoint();
			_currentWord = _currentWord.Remove(_currentWord.Length - 1);
			_currentAddLetter.RemoveAt(_currentAddLetter.Count - 1);
		}
		else
		{
			if (_currentAddLetter.IndexOf(obj.name) >= 0)
			{
				return;
			}
			_catmullDraw.AddPoints(obj.transform.position);
			_currentWord += text;
			_currentAddLetter.Add(obj.name);
		}
		if (_currentWord.Length > 0)
		{
			m_TxtCurrentWord.text = _currentWord;
			m_ImgCurrentWordBG.transform.DOKill();
			setImgCurrentWordBG(isShow: true);
			int num2 = WordGlobal.CalculateLengthOfText(m_TxtCurrentWord, _currentWord) + 30 + 4;
			if (num2 < 65)
			{
				num2 = 65;
			}
			m_ImgCurrentWordBG.GetComponent<RectTransform>().sizeDelta = new Vector2(num2, 70f);
			selectLetterAnim_rect.sizeDelta = new Vector2(num2, 70f);
			AudioControl.GetInstance().PlaySpellEff(_currentWord.Length);
		}
	}

	public void setImgCurrentWordBG(bool isShow)
	{
		m_ImgCurrentWordBG.SetActive(isShow);
	}

	public bool CheckWordCompleteByCrossWord(CrossWordItem word)
	{
		if (_currentWordList.Contains(word.Word))
		{
			return true;
		}
		return false;
	}

	private bool CheckWordComplete(CrossWordItem word, int row, int col)
	{
		int num = word.Row;
		int num2 = word.Col;
		for (int i = 0; i < word.Word.Length; i++)
		{
			if (word.vertical)
			{
				num += i;
			}
			else
			{
				num2 += i;
			}
			if (GetCell(num2, num) == string.Empty && (row != num || col != num2))
			{
				return false;
			}
		}
		return true;
	}

	public void CheckTmpWord(CrossWordItem word)
	{
		int num = word.Row;
		int num2 = word.Col;
		for (int i = 0; i < word.Word.Length; i++)
		{
			if (word.vertical)
			{
				num++;
			}
			else
			{
				num2++;
			}
			if (!(GetCell(num2, num) == string.Empty))
			{
				continue;
			}
			List<CrossWordItem> words = GetWords(num, num2);
			if (words.Count <= 0)
			{
				continue;
			}
			for (int j = 0; j < words.Count; j++)
			{
				if (words[j].Word != word.Word && CheckWordComplete(words[j], num, num2))
				{
					AddTmpCompleteWord(words[j].Word);
				}
			}
		}
	}

	public void CheckLevel()
	{
		int i;
		for (i = 0; i < _tmpCompleteWordList.Count; i++)
		{
			if (_dictionaryList.FindIndex((string x) => x == _tmpCompleteWordList[i]) < 0)
			{
				_dictionaryList.Add(_tmpCompleteWordList[i]);
				m_DictionaryCurrentIndex = _dictionaryList.Count;
			}
		}
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			_CheckLeveling = true;
			int j;
			for (j = 0; j < _tmpCompleteWordList.Count; j++)
			{
				if (_currentWordList.FindIndex((string x) => x == _tmpCompleteWordList[j]) < 0)
				{
					_currentWordList.Add(_tmpCompleteWordList[j]);
				}
			}
			_tmpCompleteWordList.Clear();
			if (_currentWordList.Count >= _item.WordList.Count)
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().Music_LevelComplete);
				float time = 1.5f;
				StartCoroutine(ShowDailyResult(time));
				_CheckLeveling = false;
				return;
			}
			_CheckLeveling = false;
			if (_currentWordList.Count != _item.WordList.Count - 1 || _IsLastWord)
			{
				return;
			}
			int k;
			for (k = 0; k < _item.WordList.Count; k++)
			{
				if (_currentWordList.FindIndex((string x) => x == _item.WordList[k].Word) < 0)
				{
					_IsLastWord = true;
					_coroutineLastWordEffect = StartCoroutine(LastWordEffect(_item.WordList[k]));
				}
			}
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
		{
			_CheckLeveling = true;
			int l;
			for (l = 0; l < _tmpCompleteWordList.Count; l++)
			{
				if (_currentWordList.FindIndex((string x) => x == _tmpCompleteWordList[l]) < 0)
				{
					_currentWordList.Add(_tmpCompleteWordList[l]);
				}
			}
			_tmpCompleteWordList.Clear();
			if (_currentWordList.Count >= _item.WordList.Count)
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().Music_LevelComplete);
				StartCoroutine(ShowHuoDongGardonResult());
				_CheckLeveling = false;
				return;
			}
			_CheckLeveling = false;
			if (_currentWordList.Count != _item.WordList.Count - 1 || _IsLastWord)
			{
				return;
			}
			int m;
			for (m = 0; m < _item.WordList.Count; m++)
			{
				if (_currentWordList.FindIndex((string x) => x == _item.WordList[m].Word) < 0)
				{
					_IsLastWord = true;
					_coroutineLastWordEffect = StartCoroutine(LastWordEffect(_item.WordList[m]));
				}
			}
		}
		else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
		{
			_CheckLeveling = true;
			int n;
			for (n = 0; n < _tmpCompleteWordList.Count; n++)
			{
				if (_currentWordList.FindIndex((string x) => x == _tmpCompleteWordList[n]) < 0)
				{
					_currentWordList.Add(_tmpCompleteWordList[n]);
				}
			}
			_tmpCompleteWordList.Clear();
			if (_currentWordList.Count >= _item.WordList.Count)
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().Music_LevelComplete);
				StartCoroutine(ShowHuoDongSunResult());
				_CheckLeveling = false;
				return;
			}
			_CheckLeveling = false;
			if (_currentWordList.Count != _item.WordList.Count - 1 || _IsLastWord)
			{
				return;
			}
			int i2;
			for (i2 = 0; i2 < _item.WordList.Count; i2++)
			{
				if (_currentWordList.FindIndex((string x) => x == _item.WordList[i2].Word) < 0)
				{
					_IsLastWord = true;
					_coroutineLastWordEffect = StartCoroutine(LastWordEffect(_item.WordList[i2]));
				}
			}
		}
		else
		{
			if (UIManager.CurMainUIType != UIManager.MainUIType.Type_Common && UIManager.CurMainUIType != UIManager.MainUIType.Type_HDTournament)
			{
				return;
			}
			_CheckLeveling = true;
			int num = 0;
			if (!_NextLevel)
			{
				num += _tmpCompleteWordList.Count;
			}
			if (_tmpCompleteWordList.Count > 1)
			{
				for (int num2 = 0; num2 < _tmpCompleteWordList.Count - 1; num2++)
				{
					_ComboCount++;
					if (_ComboCount > 1)
					{
						num = ((_ComboCount >= 8) ? (num + 6) : (num + (_ComboCount - 1)));
					}
				}
			}
			_ComboCount++;
			if (_ComboCount > 1)
			{
				if (_ComboCount < 8)
				{
					_tmpComboScore += _ComboCount - 1;
					_effectControl.ComboEffect(_ComboCount - 2, _ComboCount - 1);
					bool flag = false;
					switch (_ComboCount)
					{
					case 2:
						PlayerInfo.Instance.GetDailyQuestSaveData().Good_num++;
						flag = PlayerInfo.Instance.GetDailyQuestSaveData().IsHaveQuestByRequireType(70);
						break;
					case 3:
						PlayerInfo.Instance.GetDailyQuestSaveData().Great_num++;
						flag = PlayerInfo.Instance.GetDailyQuestSaveData().IsHaveQuestByRequireType(71);
						break;
					case 4:
						PlayerInfo.Instance.GetDailyQuestSaveData().Excellent_num++;
						flag = PlayerInfo.Instance.GetDailyQuestSaveData().IsHaveQuestByRequireType(72);
						break;
					case 5:
						PlayerInfo.Instance.GetDailyQuestSaveData().Amazing_num++;
						flag = PlayerInfo.Instance.GetDailyQuestSaveData().IsHaveQuestByRequireType(73);
						break;
					case 6:
						PlayerInfo.Instance.GetDailyQuestSaveData().Unbelievable_num++;
						flag = PlayerInfo.Instance.GetDailyQuestSaveData().IsHaveQuestByRequireType(74);
						break;
					case 7:
						PlayerInfo.Instance.GetDailyQuestSaveData().Outstanding_num++;
						flag = PlayerInfo.Instance.GetDailyQuestSaveData().IsHaveQuestByRequireType(75);
						break;
					}
					if (flag && DailyQuestBtn.activeInHierarchy)
					{
						_effectControl.QuestLetterFly(m_TxtCurrentWord.transform.position, DailyQuestBtn.transform.position);
					}
				}
				else
				{
					_tmpComboScore += 6;
					_effectControl.ComboEffect(5, 6);
				}
			}
			if (!m_IsRePlayer)
			{
			}
			if (PlayerInfo.Instance.CurrentLevel < PlayerInfo.Instance.MaxPlayLevel)
			{
				num = 0;
			}
			int count = _tmpCompleteWordList.Count;
			int i3;
			for (i3 = 0; i3 < _tmpCompleteWordList.Count; i3++)
			{
				if (_currentWordList.FindIndex((string x) => x == _tmpCompleteWordList[i3]) < 0)
				{
					_currentWordList.Add(_tmpCompleteWordList[i3]);
					if (huodong_tournament_main != null)
					{
						huodong_tournament_main.LevelAddCup(getCenterPosByWord(_tmpCompleteWordList[i3]), 0f, 0.5f);
					}
				}
			}
			_tmpCompleteWordList.Clear();
			if (num >= 1)
			{
				if (_ComboCount > 1 || _currentWordList.Count >= _item.WordList.Count)
				{
					m_HideScoreUI = false;
					_currentLevelScore += num;
				}
				else
				{
					_currentLevelScore += num;
				}
			}
			if (_currentWordList.Count >= _item.WordList.Count)
			{
				if (_coroutineLastWordEffect != null)
				{
					StopCoroutine(_coroutineLastWordEffect);
					showLastWordEff_time = 0f;
					_coroutineLastWordEffect = null;
				}
				for (int num3 = 0; num3 < 10; num3++)
				{
					GameObject gameObject = GameObject.Find("acg_TxtQuabFX01");
					if (gameObject != null)
					{
						UnityEngine.Object.DestroyImmediate(gameObject);
					}
				}
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().Music_LevelComplete);
				float num4 = 1f;
				if (_ComboCount > 1)
				{
					num4 = 2f;
				}
				else if (huodong_main != null && huodong_main.icon_go.activeInHierarchy)
				{
					num4 = 2f;
				}
				else
				{
					if (huodong_tournament_main != null && count > 0)
					{
						num4 += 0.5f;
					}
					if (_currentWordList.Count > 1 && _currentWordList[_currentWordList.Count - 1].Equals(_item.MoneyWord))
					{
						num4 += 1f;
					}
				}
				StartCoroutine(ShowResult(num4));
				_CheckLeveling = false;
				return;
			}
			_CheckLeveling = false;
			if (_currentWordList.Count != _item.WordList.Count - 1 || _IsLastWord)
			{
				return;
			}
			int i4;
			for (i4 = 0; i4 < _item.WordList.Count; i4++)
			{
				if (_currentWordList.FindIndex((string x) => x == _item.WordList[i4].Word) < 0)
				{
					_IsLastWord = true;
					_coroutineLastWordEffect = StartCoroutine(LastWordEffect(_item.WordList[i4]));
				}
			}
		}
	}

	public Vector3 getCenterPosByWord(string word)
	{
		Vector3 result = Vector3.zero;
		CrossWordItem crossWordItem = _item.WordList.Find((CrossWordItem x) => x.Word == word);
		if (crossWordItem != null)
		{
			int num = crossWordItem.Row - 1;
			int num2 = crossWordItem.Col - 1;
			bool vertical = crossWordItem.vertical;
			int num3 = word.Length / 2;
			if (vertical)
			{
				num += num3;
			}
			else
			{
				num2 += num3;
			}
			GameObject itemByRC = GetItemByRC(num, num2);
			if (itemByRC != null)
			{
				result = itemByRC.transform.position;
			}
		}
		return result;
	}

	public void ShowScroeUI(bool isShow = true)
	{
		if (isShow)
		{
			m_TFScoreUI.DOKill();
			m_TFScoreUI.DOAnchorPosX(-39f, 0.3f);
			m_TFScoreUI.gameObject.SetActive(value: true);
			return;
		}
		m_TFScoreUI.DOAnchorPosX(142f, 0.3f);
		if (_showResultUI)
		{
			m_TFScoreUI.gameObject.SetActive(value: false);
		}
	}

	public void AddScore(int score, bool isHide = true)
	{
		AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_GetScore);
		if (isHide)
		{
			if (_showScoreTime < 2f)
			{
				_showScoreTime = 2f;
			}
		}
		else
		{
			_showScoreTime = 3.5f;
		}
		ShowScroeUI();
		Vector2 anchoredPosition = m_TFScoreUI.anchoredPosition;
		if (anchoredPosition.x > 0f)
		{
			add_scoreWaitTime = 0.5f;
		}
		else
		{
			add_scoreWaitTime = 0.1f;
		}
		m_TxtStarCount.color = new Color(0.9921569f, 251f / 255f, 31f / 85f);
		AddScoreAni(score, isHide);
	}

	public void UpdateAddScoreAni()
	{
		if (_tmpScore >= _currentLevelScore)
		{
			return;
		}
		add_scoreWaitTime -= Time.deltaTime;
		if (add_scoreWaitTime < 0f)
		{
			_tmpScore += score_speed;
			if (_tmpScore >= _currentLevelScore)
			{
				_tmpScore = _currentLevelScore;
				m_TxtStarCount.transform.DOKill();
				m_TxtStarCount.transform.localScale = Vector3.one;
				m_TxtStarCount.color = Color.white;
			}
			m_TxtStarCount.text = _tmpScore.ToString();
			add_scoreWaitTime = 0.1f;
		}
	}

	private IEnumerator AddScoreAni()
	{
		_isAddingScore = false;
		while (_tmpScore < _currentLevelScore)
		{
			yield return new WaitForSeconds(0.1f);
			_tmpScore += score_speed;
			if (_tmpScore >= _currentLevelScore)
			{
				_tmpScore = _currentLevelScore;
			}
			m_TxtStarCount.text = _tmpScore.ToString();
		}
		_isAddingScore = false;
		yield return new WaitForSeconds(0.5f);
		m_TxtStarCount.transform.DOScale(2f, 0.2f);
		m_TxtStarCount.color = new Color(0.9921569f, 251f / 255f, 31f / 85f);
		m_TxtStarCount.DOColor(new Color(1f, 1f, 1f), 0.2f);
		m_TxtStarCount.transform.DOScale(1f, 0.2f);
	}

	private void AddScoreAni(int score, bool hide)
	{
		_currentLevelScore += score;
		if (!_isAddingScore)
		{
			int num = _currentLevelScore - _tmpScore;
			score_speed = num / 10;
			if (score_speed <= 0)
			{
				score_speed = 1;
			}
			m_TxtStarCount.transform.DOScale(1.3f, 0.2f);
		}
	}

	private void SetLetterState(int state)
	{
		if (state == 0 && _currentAddLetter.Count > 0)
		{
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WrongWord);
			m_ImgCurrentWordBG.GetComponent<Animator>().SetTrigger("Sharp");
			return;
		}
		for (int i = 0; i < _currentAddLetter.Count; i++)
		{
			m_TfLettersRoot.Find(_currentAddLetter[i]).GetComponent<LetterScript>().SetAni(state);
		}
	}

	public CrossWordItem GetWordByStr(string word_str)
	{
		return _item.WordList.Find((CrossWordItem x) => x.Word == word_str);
	}

	private void SetWordEffectState(string word, int state)
	{
		if (state == 1)
		{
			CrossWordItem crossWordItem = _item.WordList.Find((CrossWordItem x) => x.Word == word);
			int num = crossWordItem.Row;
			int num2 = crossWordItem.Col;
			for (int i = 0; i < word.Length; i++)
			{
				GetLetterItem(num, num2).GetComponent<ItemScript>().AddLetterSuccess(m_TfLettersRoot.transform.Find(_currentAddLetter[i]), word.Substring(i, 1), i, word);
				if (crossWordItem.vertical)
				{
					num++;
				}
				else
				{
					num2++;
				}
			}
			setImgCurrentWordBG(isShow: true);
			m_TxtCurrentWord.text = word;
			Transform transform = m_ImgCurrentWordBG.transform;
			Vector3 position = m_ImgCurrentWordBG.transform.position;
			transform.DOMoveX(position.x + 0.001f, 0.5f).OnComplete(delegate
			{
				m_ImgWordBG.DOFade(0f, 0.2f);
				m_TxtCurrentWord.DOFade(0f, 0.2f).OnComplete(delegate
				{
					if (huodong_main != null)
					{
						huodong_main.Check();
						huodong_main.flyUpDelayTime = 0.3f;
						huodong_main.isCheckCompleteWord = true;
						Debug.Log("huodong_main.isCheckCompleteWord:" + huodong_main.isCheckCompleteWord);
					}
					if (huodong_sun_main != null)
					{
						huodong_sun_main.Check();
						huodong_sun_main.flyUpDelayTime = 0.3f;
					}
					setImgCurrentWordBG(isShow: false);
					Image imgWordBG = m_ImgWordBG;
					Color color = m_ImgWordBG.color;
					float r = color.r;
					Color color2 = m_ImgWordBG.color;
					float g = color2.g;
					Color color3 = m_ImgWordBG.color;
					imgWordBG.color = new Color(r, g, color3.b, 1f);
					Text txtCurrentWord = m_TxtCurrentWord;
					Color color4 = m_TxtCurrentWord.color;
					float r2 = color4.r;
					Color color5 = m_TxtCurrentWord.color;
					float g2 = color5.g;
					Color color6 = m_TxtCurrentWord.color;
					txtCurrentWord.color = new Color(r2, g2, color6.b, 1f);
					m_TxtCurrentWord.text = string.Empty;
				});
			});
			m_ImgCurrentWordBG.GetComponent<Animator>().Play("tanchu");
			return;
		}
		CrossWordItem crossWordItem2 = _item.WordList.Find((CrossWordItem x) => x.Word == word);
		int num3 = crossWordItem2.Row;
		int num4 = crossWordItem2.Col;
		for (int j = 0; j < word.Length; j++)
		{
			if (state == 0)
			{
				GetLetterItem(num3, num4).GetComponent<ItemScript>().TryWordFailed();
			}
			if (crossWordItem2.vertical)
			{
				num3++;
			}
			else
			{
				num4++;
			}
		}
		setImgCurrentWordBG(isShow: false);
	}

	private bool CheckTutorial(string word)
	{
		if (word.Length < 2)
		{
			return false;
		}
		if (PlayerInfo.Instance.TutorialStep < 2)
		{
			if (word.Length >= 2)
			{
				m_ObjTS.SetActive(value: false);
				PlayerInfo.Instance.ShowTermsService = 1;
			}
			if (word != _item.WordList[PlayerInfo.Instance.TutorialStep].Word)
			{
				StartCoroutine(InitTutorial());
				return false;
			}
			PlayerInfo.Instance.TutorialStep++;
		}
		return true;
	}

	public void AddBonusTip(string word)
	{
		if (_bonusTipList.Contains(word))
		{
			ResourceLoadManager.GetInstance().ShowTips(1014);
		}
		else
		{
			_bonusTipList.Add(word);
		}
		BonusAnim.Play("stars_move");
	}

	public void TryAddWord()
	{
		try
		{
			if (!string.IsNullOrEmpty(_currentWord))
			{
				if (_item == null)
				{
					Debug.LogError("_item is null");
				}
				else
				{
					string word = _currentWord;
					if (!string.IsNullOrEmpty(_currentWord) && _currentWord.Length >= 2)
					{
						WordGlobal.WordSelectWrongCount++;
						if (WordGlobal.WordSelectWrongCount >= 1 && m_ObjBtnWatchVideo.activeInHierarchy && !_showResultUI && !DPCTutorialUI.isDPCTutorialShow && PlayerInfo.Instance.ShowTutorialWatchVideo == 0)
						{
							isShowTutorialWatchVideo = true;
						}
					}
					if (word.Length > 0 && _item.Bonus.FindIndex((string x) => x == word) >= 0)
					{
						if (_bonusList.FindIndex((string x) => x == word) >= 0)
						{
							AddBonusTip(word);
							setImgCurrentWordBG(isShow: false);
							m_TxtCurrentWord.text = string.Empty;
							_currentWord = string.Empty;
							AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordDuplicate);
							WordGlobal.WordSelectWrongCount = 0;
							isShowTutorialWatchVideo = false;
							goto IL_046c;
						}
						if (_minWordLength <= 3 || word.Length != 3)
						{
							if (PlayerInfo.Instance.FirstGetBonus == 0)
							{
								bool flag = true;
								if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && PlayerInfo.Instance.GetDailyPuzzleImageData().Tutorial < 3)
								{
									flag = false;
								}
								if (flag && UIManager.CurMainUIType != UIManager.MainUIType.Type_HDGardon)
								{
									WordGlobal.InitLabel(ResourceLoadManager.GetInstance().LoadUI("UI/Main/FirstGetBonusUI", base.transform).transform);
								}
								PlayerInfo.Instance.FirstGetBonus = 1;
								m_ObjBtnBonus.SetActive(value: true);
							}
							AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordMatch);
							WordGlobal.WordSelectWrongCount = 0;
							isShowTutorialWatchVideo = false;
							_bonusList.Add(word);
							_dictionaryList.Add(word);
							SetBonusNum();
							m_DictionaryCurrentIndex = _dictionaryList.Count;
							if (PlayerInfo.Instance.AddBonusWord(PlayerInfo.Instance.CurrentLevel, word))
							{
								PlayerInfo.Instance.CollectedBonusCount++;
								PlayerInfo.Instance.GetDailyQuestSaveData().CollectBonus_num++;
								InitBonusEffect();
								if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common && PlayerInfo.Instance.CurrentLevel == PlayerInfo.Instance.MaxPlayLevel)
								{
									int num = word.Length - 2;
									_currentLevelScore += num;
								}
								setImgCurrentWordBG(isShow: false);
								m_TxtCurrentWord.text = string.Empty;
								_currentWord = string.Empty;
								_effectControl.QuestLetterFly(m_TxtCurrentWord.transform.position, m_ObjBonusFull.transform.position);
								if (huodong_tournament_main != null)
								{
									huodong_tournament_main.LevelAddCup(m_ObjBonusFull.transform.position, 0f, 0.8f);
								}
								if (PlayerInfo.Instance.GetDailyQuestSaveData().IsHaveQuestByRequireType(40) && DailyQuestBtn.activeInHierarchy)
								{
									_effectControl.QuestLetterFly(m_TxtCurrentWord.transform.position, DailyQuestBtn.transform.position);
								}
								if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon && huodong_gardon_main != null)
								{
									huodong_gardon_main.AddTime(word.Length - 1);
								}
							}
							else
							{
								AddBonusTip(word);
								setImgCurrentWordBG(isShow: false);
								m_TxtCurrentWord.text = string.Empty;
								_currentWord = string.Empty;
							}
							goto IL_046c;
						}
						ResourceLoadManager.GetInstance().ShowTips(1221);
						setImgCurrentWordBG(isShow: false);
						m_TxtCurrentWord.text = string.Empty;
						_currentWord = string.Empty;
						_currentAddLetter.Clear();
					}
					else
					{
						_currentWord = string.Empty;
						if (_currentWordList.FindIndex((string x) => x == word) >= 0 || _item.WordList.FindIndex((CrossWordItem x) => x.Word == word) < 0)
						{
							if (word.Length > 2 && _item.WordList.FindIndex((CrossWordItem x) => x.Word == word) < 0)
							{
								_ComboCount = 0;
								_effectControl.StopFullScrenCombo();
							}
							if (_item.WordList.FindIndex((CrossWordItem x) => x.Word == word) < 0)
							{
								if (word.Length > 1)
								{
									SetLetterState(0);
								}
								m_ImgCurrentWordBG.transform.DOScale(1f, 0.6f).OnComplete(delegate
								{
									setImgCurrentWordBG(isShow: false);
									m_TxtCurrentWord.text = string.Empty;
								});
							}
							else
							{
								SetLetterState(1);
								SetWordEffectState(word, 0);
								AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordDuplicate);
								WordGlobal.WordSelectWrongCount = 0;
								isShowTutorialWatchVideo = false;
								if (huodong_main != null)
								{
									huodong_main.isCheckCompleteWord = false;
								}
							}
							_currentAddLetter.Clear();
							CheckTutorial(word);
						}
						else
						{
							setImgCurrentWordBG(isShow: false);
							m_TxtCurrentWord.text = string.Empty;
							if (!CheckTutorial(word))
							{
								_currentAddLetter.Clear();
							}
							else
							{
								AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_WordMatch);
								WordGlobal.WordSelectWrongCount = 0;
								isShowTutorialWatchVideo = false;
								SetLetterState(1);
								AddTmpCompleteWord(word);
								SetWord(_item.WordList.Find((CrossWordItem x) => x.Word == word));
								SetWordEffectState(word, 1);
								_currentAddLetter.Clear();
								CheckTmpWord(_item.WordList.Find((CrossWordItem x) => x.Word == word));
								if (PlayerInfo.Instance.TutorialStep < 2)
								{
									StartCoroutine(InitTutorial());
								}
							}
						}
					}
				}
			}
			goto end_IL_0000;
			IL_046c:
			SetLetterState(1);
			if (huodong_main != null)
			{
				huodong_main.isCheckCompleteWord = false;
			}
			_currentAddLetter.Clear();
			end_IL_0000:;
		}
		catch (Exception arg)
		{
			Debug.LogError("TryAddWord Error:" + arg);
		}
	}

	public void ClearSelectWord()
	{
		_currentAddLetter.Clear();
		setImgCurrentWordBG(isShow: false);
		m_TxtCurrentWord.text = string.Empty;
		_currentWord = string.Empty;
	}

	public void DebugAddWord(string word)
	{
		_currentWord = word;
		List<int> list = new List<int>();
		for (int i = 0; i < word.Length; i++)
		{
			char c = word[i];
			for (int j = 0; j < m_TfLettersRoot.childCount; j++)
			{
				if (!list.Contains(j))
				{
					string name = m_TfLettersRoot.GetChild(j).name;
					if (name.Contains(c.ToString()))
					{
						list.Add(j);
						_currentAddLetter.Add(name);
					}
				}
			}
		}
		TryAddWord();
	}

	private void ClearLetterImg()
	{
		if (!(m_TfLettersRoot != null))
		{
			return;
		}
		for (int i = 0; i < m_TfLettersRoot.childCount; i++)
		{
			if (m_TfLettersRoot.GetChild(i) != null)
			{
				m_TfLettersRoot.GetChild(i).GetComponent<Image>().color = new Color(1f, 1f, 1f, 0f);
				if (m_TfLettersRoot.GetChild(i).GetChild(0) != null)
				{
					m_TfLettersRoot.GetChild(i).GetChild(0).GetComponent<Text>()
						.color = _letterColor;
					}
				}
			}
		}

		public void ReInitLetter()
		{
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_ItemShuffle);
			GuideLineDraw.GuideLetterShowState = 2;
			List<Vector3> list = new List<Vector3>();
			list = WordGlobal.ShuffleList(_letterPosList);
			for (int i = 0; i < m_TfLettersRoot.childCount; i++)
			{
				m_TfLettersRoot.GetChild(i).GetComponent<LetterScript>().LetterRandomAni(list[i]);
			}
		}

		private List<LetterItem> GetLetter()
		{
			List<LetterItem> list = new List<LetterItem>();
			for (int i = 0; i < _item.Words.Count; i++)
			{
				for (int j = 0; j < _item.Words[i].Length; j++)
				{
					string str = _item.Words[i].Substring(j, 1);
					if (list.FindAll((LetterItem ll) => ll.Letter == str).Count < Regex.Matches(_item.Words[i], str).Count)
					{
						LetterItem letterItem = new LetterItem();
						letterItem.ID = list.Count;
						letterItem.Letter = str;
						list.Add(letterItem);
					}
				}
			}
			return list;
		}

		public void InitLetter()
		{
			WordGlobal.ClearChilds(m_TfLettersRoot);
			_letterPosList = new List<Vector3>();
			List<LetterItem> list = WordGlobal.ShuffleList(GetLetter());
			for (int i = 0; i < list.Count; i++)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate(m_ObjLettersItem);
				gameObject.transform.SetParent(m_TfLettersRoot, worldPositionStays: false);
				gameObject.name = list[i].ID + "_" + list[i].Letter;
				gameObject.GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
				Text component = gameObject.transform.GetChild(0).GetComponent<Text>();
				component.text = list[i].Letter;
				component.color = GetCurrentDishTypeColor(isLetter: true);
				gameObject.SetActive(value: true);
				gameObject.transform.localPosition = new Vector3(0f, GetLetterItemR(list.Count), 0f);
				gameObject.transform.RotateAround(m_TfLettersRoot.position, new Vector3(0f, 0f, 1f), 360 * i / list.Count);
				gameObject.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
				gameObject.transform.localScale = GetLetterItemSize(list.Count);
				_letterPosList.Add(gameObject.transform.localPosition);
			}
		}

		private Vector3 GetPos(int count, int index, GameObject obj)
		{
			Vector3 result = default(Vector3);
			if (index == 0)
			{
				result = new Vector3(0f, _letterItemR, 0f);
			}
			obj.transform.Rotate(m_TfLettersRoot.localPosition, 0f, Space.Self);
			return result;
		}

		public void BtnTipsClick()
		{
			if (CheckGridCompleted())
			{
				return;
			}
			SetLastOperationPoint();
			if (isTips || _CheckLeveling)
			{
				return;
			}
			isTips = true;
			GuideLineDraw.GuideLetterShowState = 2;
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
			{
				if (InitTipsWord())
				{
					SetTipsLetter();
				}
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
			{
				SetButterFlyTipsLetter();
			}
			TutorialTipsHalfPay.isHide = true;
			BtnVideoScript.dp_wait_showTime = 0f;
			isTips = false;
		}

		private bool InitTipsWord()
		{
			if (_tipsWord == null || _tipsCount >= _tipsWord.Word.Length)
			{
				string text = (_tipsWord != null) ? _tipsWord.Word : string.Empty;
				List<CrossWordItem> list = new List<CrossWordItem>();
				int i;
				for (i = 0; i < _item.WordList.Count; i++)
				{
					if (_currentWordList.FindIndex((string x) => x == _item.WordList[i].Word) < 0 && _tmpCompleteWordList.FindIndex((string x) => x == _item.WordList[i].Word) < 0)
					{
						list.Add(_item.WordList[i]);
					}
				}
				if (list.Count == 0)
				{
					return false;
				}
				int index = UnityEngine.Random.Range(0, list.Count);
				_tipsWord = list[index];
				_tipsCount = 0;
			}
			return true;
		}

		public int NoLetterNum()
		{
			ItemScript[] componentsInChildren = m_TfItemRoot.GetComponentsInChildren<ItemScript>();
			int num = 0;
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				if (componentsInChildren[i]._imgState == 1)
				{
					num++;
				}
			}
			return num;
		}

		private void SetTipsLetter()
		{
			if (_CheckLeveling || !InitTipsWord())
			{
				return;
			}
			int num = _tipsWord.Row;
			int num2 = _tipsWord.Col;
			if (_tipsWord.vertical)
			{
				num += _tipsCount;
			}
			else
			{
				num2 += _tipsCount;
			}
			if (GetCell(num2, num) != string.Empty)
			{
				_tipsCount++;
				if (_tipsCount >= _tipsWord.Word.Length)
				{
					if (_tipsCount == _tipsWord.Word.Length)
					{
						AddTmpCompleteWord(_tipsWord.Word);
					}
					if (!InitTipsWord())
					{
						return;
					}
				}
				SetTipsLetter();
				return;
			}
			if (isFreeDP)
			{
				PlayerInfo.Instance.TipsCount++;
			}
			if (PlayerInfo.Instance.TipsCount > 0)
			{
				PlayerInfo.Instance.TipsCount--;
			}
			else if (!PlayerInfo.Instance.DecGold(PlayerInfo.Instance.systemConfig.GetTipsPrice()))
			{
				return;
			}
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_ItemHint);
			string letter = _tipsWord.Word.Substring(_tipsCount, 1);
			SetLetter(num2, num, letter, m_ObjBtnTipsClick.transform.position);
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
			{
				HuoDongSunItemCheck(num, num2);
			}
			if (commonButterFlyTV != null && isFreeDP && ButterFlyTV.isButterFlyTvFreeDP)
			{
				commonButterFlyTV.CommonGetButterMove(num, num2);
			}
			_tipsCount++;
			if (_tipsCount >= _tipsWord.Word.Length)
			{
				AddTmpCompleteWord(_tipsWord.Word);
				_tipsWord = null;
				InitTipsWord();
			}
		}

		public void SetButterFlyTipsLetter()
		{
			if (PlayerInfo.Instance.TipsCount > 0)
			{
				PlayerInfo.Instance.TipsCount--;
			}
			else if (!PlayerInfo.Instance.DecGold(PlayerInfo.Instance.systemConfig.GetTipsPrice()))
			{
				return;
			}
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_ItemHint);
			if (_curButterInWordItem != null)
			{
				int num = 0;
				string[] array = _curButterFlyPos.Split('_');
				int num2 = int.Parse(array[0]);
				int num3 = int.Parse(array[1]);
				num = ((!_curButterInWordItem.vertical) ? (num3 - _curButterInWordItem.Col) : (num2 - _curButterInWordItem.Row));
				string letter = _curButterInWordItem.Word.Substring(num, 1);
				SetLetter(num3, num2, letter, m_ObjBtnTipsClick.transform.position);
				if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
				{
					HuoDongSunItemCheck(num2, num3);
				}
			}
		}

		private void SetLetter(int col, int row, string letter, Vector3 startPos)
		{
			SetCell(col, row, letter, initItem: false);
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && _curButterFlyPos != null && _curButterFlyPos != string.Empty)
			{
				JudgeButterChange(row, col);
			}
			if (!isFreeDPShowEff)
			{
				_effectControl.LetterFly(startPos, GetItemByRC(row - 1, col - 1).transform.position, 0.7f, delegate
				{
					SetCell(col, row, letter, initItem: true);
					CheckWordCompleted(row, col);
					GetItemByRC(row - 1, col - 1).GetComponent<ItemScript>().LetterInAni(1);
				}, isNeedEff: false);
				isFreeDPShowEff = true;
			}
			else
			{
				_effectControl.LetterFly(startPos, GetItemByRC(row - 1, col - 1).transform.position, 0.7f, delegate
				{
					SetCell(col, row, letter, initItem: true);
					CheckWordCompleted(row, col);
					GetItemByRC(row - 1, col - 1).GetComponent<ItemScript>().LetterInAni(1);
				});
			}
		}

		public void BtnCellTipsClick()
		{
			if (CheckGridCompleted())
			{
				return;
			}
			SetLastOperationPoint();
			if (PlayerInfo.Instance.TutorialStep == 3)
			{
				m_ObjBtnCellTipsClick.GetComponent<Button>().interactable = false;
				m_ObjBtnTipsLight.SetActive(value: false);
				UnityEngine.Object.Destroy(GameObject.Find("TutorialLevel4"));
			}
			if (PlayerInfo.Instance.ClickTipsCount <= 0 && !PlayerInfo.Instance.TryDecGold(PlayerInfo.Instance.systemConfig.ClickTipsPrice))
			{
				return;
			}
			if (_cellTipsClick)
			{
				if (PlayerInfo.Instance.TutorialStep != 3)
				{
					SetCellNull(1000, 1000, m_TfTipsClickRoot);
					m_ObjTipsClick.SetActive(value: false);
					_cellTipsClick = false;
					m_ObjBtnTipsLight.SetActive(value: false);
					m_ObjBtnCellTipsClick.GetComponent<Canvas>().overrideSorting = false;
					m_ObjBtnCellTipsClick.GetComponent<BtnNumberPriceSet>().ShowClostTip(show: false);
					if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && _curButterFlyPos != string.Empty)
					{
						string[] array = _curButterFlyPos.Split('_');
						ItemScript component = GetItemByRC(int.Parse(array[0]) - 1, int.Parse(array[1]) - 1).GetComponent<ItemScript>();
						ChangeButterParent(component, component.m_ObjImage.transform);
					}
				}
			}
			else
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_ItemSpecific);
				GuideLineDraw.GuideLetterShowState = 2;
				BtnVideoScript.dp_wait_showTime = 0f;
				_cellTipsClick = true;
				m_ObjBtnCellTipsClick.GetComponent<Canvas>().overrideSorting = true;
				m_ObjBtnCellTipsClick.GetComponent<BtnNumberPriceSet>().ShowClostTip(show: true);
				m_TxtConfirm.text = WordGlobal.ReadText((PlayerInfo.Instance.TutorialStep <= 3) ? 1009 : 1010);
				InitTipsClickItem();
				m_ObjTipsClick.SetActive(value: true);
				m_ObjTipsBtns.SetActive(value: false);
				m_ObjBtnTipsLight.SetActive(value: true);
				ResourceLoadManager.GetInstance().LoadUI("FX/acg_tips", m_ObjBtnTipsLight.transform).gameObject.name = "tips";
			}
		}

		public WordListItem get_Item()
		{
			return _item;
		}

		public void BtnCellTipsConfirmClick()
		{
			_cellTipsClick = false;
			m_ObjBtnCellTipsClick.GetComponent<Canvas>().overrideSorting = false;
			m_ObjTipsBtns.transform.GetChild(1).GetComponent<Button>().interactable = true;
			UnityEngine.Object.Destroy(GameObject.Find("TutorialLevel4Confirm"));
			m_ObjTipsClick.SetActive(value: false);
			if (m_ObjBtnTipsLight.transform.childCount > 0)
			{
				UnityEngine.Object.DestroyImmediate(m_ObjBtnTipsLight.transform.GetChild(0).gameObject);
			}
			m_ObjBtnTipsLight.SetActive(value: false);
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && _curButterFlyPos != string.Empty)
			{
				string[] array = _curButterFlyPos.Split('_');
				ItemScript component = GetItemByRC(int.Parse(array[0]) - 1, int.Parse(array[1]) - 1).GetComponent<ItemScript>();
				ChangeButterParent(component, component.m_ObjImage.transform);
			}
			if (PlayerInfo.Instance.TutorialStep == 999)
			{
				if (PlayerInfo.Instance.ClickTipsCount > 0)
				{
					PlayerInfo.Instance.ClickTipsCount--;
				}
				else if (!PlayerInfo.Instance.DecGold(PlayerInfo.Instance.systemConfig.ClickTipsPrice))
				{
					return;
				}
			}
			else
			{
				PlayerInfo.Instance.TutorialStep = 999;
				m_ObjBtnCellTipsClick.GetComponent<Button>().interactable = true;
			}
			string letter = GetCellLetter(_cellTipsRow + 1, _cellTipsCol + 1);
			int r = _cellTipsRow;
			int c = _cellTipsCol;
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && _curButterFlyPos != string.Empty)
			{
				SetCell(c + 1, r + 1, letter, initItem: false);
				JudgeButterChange(r + 1, c + 1);
			}
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
			{
				SetCell(c + 1, r + 1, letter, initItem: false);
				HuoDongSunItemCheck(r + 1, c + 1);
			}
			_effectControl.LetterFly(m_ObjBtnCellTipsClick.transform.position, GetItemByRC(r, c).transform.position, 1f, delegate
			{
				SetCell(c + 1, r + 1, letter, initItem: true);
				GetItemByRC(r, c).GetComponent<ItemScript>().LetterInAni(1);
				bool flag = CheckWordCompleted(r + 1, c + 1);
				WordGlobal.DisableBackKey = false;
			});
			SetCell(_cellTipsCol + 1, _cellTipsRow + 1, string.Empty, initItem: true);
			WordGlobal.DisableBackKey = true;
			SetCell(_cellTipsCol + 1, _cellTipsRow + 1, "|", initItem: false);
		}

		public GameObject GetItemByRC(int row, int col)
		{
			Transform transform = m_TfItemRoot.transform.Find("item-" + row + "-" + col);
			if (transform != null)
			{
				return transform.gameObject;
			}
			return null;
		}

		public void BtnCellTipsCancelClick()
		{
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily && _curButterFlyPos != string.Empty)
			{
				string[] array = _curButterFlyPos.Split('_');
				ItemScript component = GetItemByRC(int.Parse(array[0]) - 1, int.Parse(array[1]) - 1).GetComponent<ItemScript>();
				ChangeButterParent(component, component.m_ObjImage.transform);
			}
			m_ObjBtnCellTipsClick.GetComponent<Canvas>().overrideSorting = false;
			SetCellNull(10000, 10000, m_TfTipsClickRoot);
			m_PauseLetterTime = 0.2f;
			if (m_ObjBtnTipsLight.transform.childCount > 0)
			{
				UnityEngine.Object.DestroyImmediate(m_ObjBtnTipsLight.transform.GetChild(0).gameObject);
			}
			m_ObjTipsClick.SetActive(value: false);
			m_ObjBtnTipsLight.SetActive(value: false);
			_cellTipsClick = false;
		}

		private IEnumerator WaitObjHide()
		{
			m_ObjMask0.SetActive(value: true);
			yield return new WaitForSeconds(0.5f);
			m_ObjMask0.SetActive(value: false);
		}

		public void BtnItemClick(GameObject obj)
		{
			if (_cellTipsClick)
			{
				m_ObjBtnCellTipsClick.GetComponent<BtnNumberPriceSet>().ShowClostTip(show: false);
				m_TxtConfirm.text = WordGlobal.ReadText(1011);
				string[] array = obj.name.Split('-');
				_cellTipsRow = int.Parse(array[1]);
				_cellTipsCol = int.Parse(array[2]);
				SetCellNull(_cellTipsCol, _cellTipsRow, m_TfTipsClickRoot);
				BtnCellTipsConfirmClick();
			}
		}

		private void SetCellHiddenState(int col, int row, bool isHidden = true)
		{
			GetItemByRC(row, col).GetComponent<ItemScript>().SetHidden(isHidden);
		}

		private void SetCellNull(int col, int row, Transform tf)
		{
			for (int i = 0; i < tf.childCount; i++)
			{
				if (tf.GetChild(i).gameObject.name != "item-" + row + "-" + col)
				{
					string[] array = tf.GetChild(i).gameObject.name.Split('-');
					if (tf.GetChild(i).GetComponent<ItemScript>()._imgState == 3)
					{
						int row2 = int.Parse(array[1]);
						int col2 = int.Parse(array[2]);
						SetCellHiddenState(col2, row2, isHidden: false);
					}
					tf.GetChild(i).gameObject.GetComponent<ItemScript>().SetImageState(0);
				}
			}
		}

		public void HuoDongGardonGiveUp()
		{
			StartCoroutine(FinishNullLetters());
		}

		public IEnumerator FinishNullLetters()
		{
			isFinishNullLetters = true;
			for (int num = m_TfItemRoot.childCount - 1; num >= 0; num--)
			{
				ItemScript component = m_TfItemRoot.GetChild(num).GetComponent<ItemScript>();
				if (component._imgState == 1)
				{
					string[] array = component.gameObject.name.Split('-');
					int num2 = int.Parse(array[1]);
					int num3 = int.Parse(array[2]);
					int index = 0;
					string cellLetterAndIndex = GetCellLetterAndIndex(num2 + 1, num3 + 1, out index);
					component.FinishNullLetterAnim(cellLetterAndIndex, index);
				}
			}
			UIOutAni(isDailyResult: false, isOutItemRoot: false);
			yield return new WaitForSeconds(3f);
			UIOutAni();
			yield return new WaitForSeconds(0.5f);
			HuoDongGardonUI.OpenType = 2;
			if (GameObject.Find("HuoDongGardonUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonUI", isPop: true, UIManager.MainUIType.Type_Common, 0);
			}
			isFinishNullLetters = false;
		}

		public bool CheckOneWordCompleted(int row, int col)
		{
			bool result = false;
			List<CrossWordItem> words = GetWords(row, col);
			int num = 0;
			for (int i = 0; i < words.Count; i++)
			{
				if (words[i].vertical)
				{
					num++;
					for (int j = words[i].Row; j < words[i].Row + words[i].Word.Length; j++)
					{
						if (GetCell(col, j) == string.Empty)
						{
							num--;
							break;
						}
					}
					continue;
				}
				num++;
				for (int k = words[i].Col; k < words[i].Col + words[i].Word.Length; k++)
				{
					if (GetCell(k, row) == string.Empty)
					{
						num--;
						break;
					}
				}
			}
			if (num > 0)
			{
				result = true;
			}
			return result;
		}

		public void SetDailyQuestShow(bool isShow)
		{
			if (isShow && (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon || UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun))
			{
				isShow = false;
			}
			DailyQuestBtn.SetActive(isShow);
		}

		public bool CheckWordCompleted(int row, int col)
		{
			bool flag = true;
			List<CrossWordItem> list = GetWords(row, col);
			int i;
			for (i = 0; i < list.Count; i++)
			{
				flag = true;
				if (_tmpCompleteWordList.FindIndex((string x) => x == list[i].Word) < 0)
				{
					if (list[i].vertical)
					{
						for (int j = list[i].Row; j < list[i].Row + list[i].Word.Length; j++)
						{
							if (GetCell(col, j) == string.Empty)
							{
								flag = false;
								break;
							}
						}
					}
					else
					{
						for (int k = list[i].Col; k < list[i].Col + list[i].Word.Length; k++)
						{
							if (GetCell(k, row) == string.Empty)
							{
								flag = false;
								break;
							}
						}
					}
				}
				if (flag && _currentWordList.FindIndex((string x) => x == list[i].Word) < 0 && _tmpCompleteWordList.FindIndex((string x) => x == list[i].Word) < 0)
				{
					AddTmpCompleteWord(list[i].Word);
				}
			}
			return flag;
		}

		private void AddTmpCompleteWord(string word)
		{
			if (_currentWordList.FindIndex((string x) => x == word) < 0)
			{
				_tmpCompleteWordList.Add(word);
			}
		}

		public List<CrossWordItem> GetWords(int r, int c)
		{
			List<CrossWordItem> list = new List<CrossWordItem>();
			for (int i = 0; i < _item.WordList.Count; i++)
			{
				int num = 0;
				int num2 = 0;
				int num3 = 0;
				int num4 = 0;
				num = _item.WordList[i].Row;
				num3 = _item.WordList[i].Col;
				if (_item.WordList[i].vertical)
				{
					num2 = num + _item.WordList[i].Word.Length - 1;
					num4 = num3;
				}
				else
				{
					num2 = num;
					num4 = num3 + _item.WordList[i].Word.Length - 1;
				}
				if (r <= num2 && r >= num && c <= num4 && c >= num3)
				{
					list.Add(_item.WordList[i]);
				}
			}
			return list;
		}

		private string GetCellLetter(int r, int c)
		{
			string result = string.Empty;
			for (int i = 0; i < _item.WordList.Count; i++)
			{
				int num = 0;
				int num2 = 0;
				int num3 = 0;
				int num4 = 0;
				num = _item.WordList[i].Row;
				num3 = _item.WordList[i].Col;
				if (_item.WordList[i].vertical)
				{
					num2 = num + _item.WordList[i].Word.Length - 1;
					num4 = num3;
				}
				else
				{
					num2 = num;
					num4 = num3 + _item.WordList[i].Word.Length - 1;
				}
				if (r <= num2 && r >= num && c <= num4 && c >= num3)
				{
					result = ((!_item.WordList[i].vertical) ? _item.WordList[i].Word.Substring(c - num3, 1) : _item.WordList[i].Word.Substring(r - num, 1));
					break;
				}
			}
			return result;
		}

		public string GetCellLetterAndIndex(int r, int c, out int index)
		{
			string result = string.Empty;
			index = 0;
			for (int i = 0; i < _item.WordList.Count; i++)
			{
				int num = 0;
				int num2 = 0;
				int num3 = 0;
				int num4 = 0;
				num = _item.WordList[i].Row;
				num3 = _item.WordList[i].Col;
				if (_item.WordList[i].vertical)
				{
					num2 = num + _item.WordList[i].Word.Length - 1;
					num4 = num3;
				}
				else
				{
					num2 = num;
					num4 = num3 + _item.WordList[i].Word.Length - 1;
				}
				if (r <= num2 && r >= num && c <= num4 && c >= num3)
				{
					if (_item.WordList[i].vertical)
					{
						result = _item.WordList[i].Word.Substring(r - num, 1);
						index = r - num;
					}
					else
					{
						result = _item.WordList[i].Word.Substring(c - num3, 1);
						index = c - num3;
					}
					break;
				}
			}
			return result;
		}

		private GameObject GetLetterItem(int row, int col, Transform tf = null)
		{
			int num = row - 1;
			int num2 = col - 1;
			Transform transform = ((!(tf == null)) ? tf : m_TfItemRoot).Find("item-" + num + "-" + num2);
			if (transform == null)
			{
				transform = GameObject.Find("item-" + num + "-" + num2).transform;
			}
			return transform.gameObject;
		}

		private void InitTipsClickItem()
		{
			WordGlobal.ClearChilds(m_TfTipsClickRoot);
			for (int i = 0; i < _grid.Count; i++)
			{
				for (int j = 0; j < _grid[i].Count; j++)
				{
					GameObject gameObject = UnityEngine.Object.Instantiate(m_ObjItem);
					ItemScript component = gameObject.GetComponent<ItemScript>();
					ItemScript itemScript = component;
					Vector2 cellSize = m_TfTipsClickRoot.GetComponent<GridLayoutGroup>().cellSize;
					itemScript.SetItemScale(cellSize.x);
					if (_grid[i][j] != string.Empty)
					{
						component.SetImageState(0);
					}
					else
					{
						component.SetImageState(3);
						SetCellHiddenState(j, i);
						if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
						{
							string[] array = _curButterFlyPos.Split('_');
							if (array.Length >= 2 && i + 1 == int.Parse(array[0]) && j + 1 == int.Parse(array[1]))
							{
								ItemScript component2 = GetItemByRC(i, j).GetComponent<ItemScript>();
								ChangeButterParent(component2, component.m_ObjImage.transform);
							}
						}
					}
					gameObject.transform.SetParent(m_TfTipsClickRoot, worldPositionStays: false);
					gameObject.SetActive(value: true);
					gameObject.name = "item-" + i.ToString() + "-" + j.ToString();
				}
			}
			if (string.IsNullOrEmpty(_item.MoneyWord))
			{
				return;
			}
			CrossWordItem crossWordItem = _item.WordList.Find((CrossWordItem x) => x.Word == _item.MoneyWord);
			int num = crossWordItem.Row;
			int num2 = crossWordItem.Col;
			for (int k = 0; k < crossWordItem.Word.Length; k++)
			{
				if (_grid[num - 1][num2 - 1] == string.Empty)
				{
					GameObject letterItem = GetLetterItem(num, num2, m_TfTipsClickRoot);
					GameObject gameObject2 = ResourceLoadManager.GetInstance().LoadUI("UI/Main/GoldItem", letterItem.transform);
					Transform transform = gameObject2.transform;
					Vector3 one = Vector3.one;
					Vector2 cellSize2 = m_TfTipsClickRoot.GetComponent<GridLayoutGroup>().cellSize;
					transform.localScale = one * (cellSize2.x / _itemWith);
					gameObject2.name = "gold";
					GoldItem component3 = gameObject2.GetComponent<GoldItem>();
					component3.OnlyImg(16);
				}
				if (crossWordItem.vertical)
				{
					num++;
				}
				else
				{
					num2++;
				}
			}
		}

		private List<Vector2Int> GetLightingPoints()
		{
			List<Vector2Int> list = new List<Vector2Int>();
			List<CrossWordItem> list2 = WordGlobal.ShuffleList(_item.WordList);
			int i;
			for (i = 0; i < _currentWordList.Count; i++)
			{
				list2.Remove(list2.Find((CrossWordItem x) => x.Word == _currentWordList[i]));
			}
			int j;
			for (j = 0; j < _tmpCompleteWordList.Count; j++)
			{
				list2.Remove(list2.Find((CrossWordItem x) => x.Word == _tmpCompleteWordList[j]));
			}
			while (list.Count < PlayerInfo.Instance.systemConfig.FirstLetterTipsCount && list2.Count > 0)
			{
				List<string> removeItem = new List<string>();
				list2 = WordGlobal.ShuffleList(list2);
				for (int l = 0; l < list2.Count; l++)
				{
					List<Vector2Int> list3 = new List<Vector2Int>();
					if (list2[l].vertical)
					{
						for (int m = 0; m < list2[l].Word.Length; m++)
						{
							if (GetCell(list2[l].Col, list2[l].Row + m) == string.Empty)
							{
								int row = list2[l].Row + m;
								int col = list2[l].Col;
								if (list.FindIndex((Vector2Int x) => x.x == row && x.y == col) < 0)
								{
									list3.Add(new Vector2Int(row, col));
								}
							}
						}
					}
					else
					{
						for (int n = 0; n < list2[l].Word.Length; n++)
						{
							if (GetCell(list2[l].Col + n, list2[l].Row) == string.Empty)
							{
								int row2 = list2[l].Row;
								int col2 = list2[l].Col + n;
								if (list.FindIndex((Vector2Int x) => x.x == row2 && x.y == col2) < 0)
								{
									list3.Add(new Vector2Int(row2, col2));
								}
							}
						}
					}
					if (list3.Count == 0)
					{
						removeItem.Add(list2[l].Word);
						continue;
					}
					if (list.Count < PlayerInfo.Instance.systemConfig.FirstLetterTipsCount)
					{
						list.Add(list3[UnityEngine.Random.Range(0, list3.Count)]);
						continue;
					}
					break;
				}
				int k;
				for (k = 0; k < removeItem.Count; k++)
				{
					list2.Remove(list2.Find((CrossWordItem x) => x.Word == removeItem[k]));
				}
			}
			return list;
		}

		public void BtnAllFirstLetters()
		{
			SetLastOperationPoint();
			if (PlayerInfo.Instance.FirstTipButtonUnlock == 0)
			{
				return;
			}
			if (PlayerInfo.Instance.FirstTipsCount > 0)
			{
				PlayerInfo.Instance.FirstTipsCount--;
			}
			else if (!PlayerInfo.Instance.DecGold(PlayerInfo.Instance.systemConfig.GetFirstLetterTipsPrice()))
			{
				return;
			}
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_ItemFlash);
			GuideLineDraw.GuideLetterShowState = 2;
			BtnVideoScript.dp_wait_showTime = 0f;
			List<Vector2Int> lightingPoints = GetLightingPoints();
			for (int i = 0; i < lightingPoints.Count; i++)
			{
				SetLetter(lightingPoints[i].y, lightingPoints[i].x, GetCellLetter(lightingPoints[i].x, lightingPoints[i].y), m_ObjFirstTipsLock.transform.parent.position);
			}
			for (int j = 0; j < lightingPoints.Count; j++)
			{
				int x = lightingPoints[j].x;
				int y = lightingPoints[j].y;
				if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
				{
					HuoDongSunItemCheck(x, y);
				}
			}
		}

		private GameObject GetLetterItem(string letter)
		{
			for (int i = 0; i < m_TfLettersRoot.childCount; i++)
			{
				if (m_TfLettersRoot.GetChild(i).name.IndexOf(letter) >= 0)
				{
					return m_TfLettersRoot.GetChild(i).gameObject;
				}
			}
			return null;
		}

		private void DrawTutorialLetterLine(string letter)
		{
			List<Vector3> list = new List<Vector3>();
			for (int i = 0; i < letter.Length; i++)
			{
				list.Add(GetLetterItem(letter.Substring(i, 1)).transform.position);
			}
			m_ObjTutorial.GetComponent<TutorialScript>().SetLinePos(list);
		}

		private void OnApplicationPause(bool pause)
		{
			if (pause)
			{
				SaveGame();
			}
		}

		private IEnumerator ShowHuoDongGardonResult()
		{
			_showResultUI = true;
			m_ObjMask0.SetActive(value: true);
			_IsLastWord = false;
			AudioControl.GetInstance().StopEffect(AudioControl.GetInstance().SF_LastWord);
			if (huodong_gardon_main != null)
			{
				huodong_gardon_main.SetResult();
				int num = 0;
				for (int i = 0; i < huodong_gardon_main.flowers.Length; i++)
				{
					if (huodong_gardon_main.flowers[i].State == 0)
					{
						num++;
					}
				}
				switch (num)
				{
				case 3:
					HuoDongGardonCombo(1257);
					break;
				case 2:
					HuoDongGardonCombo(1256);
					break;
				default:
					HuoDongGardonCombo(1255);
					break;
				}
			}
			yield return new WaitForSeconds(2.5f);
			_effectControl.GameVictory();
			for (int j = 0; j < m_TfItemRoot.childCount; j++)
			{
				m_TfItemRoot.GetChild(j).GetComponent<ItemScript>().LetterInAni(2);
			}
			yield return new WaitForSeconds(2f);
			UIOutAni();
			if (huodong_gardon_main != null)
			{
				StartCoroutine(huodong_gardon_main.Result());
			}
		}

		public void HuoDongGardonCombo(int txtid)
		{
			HuoDongGardonCommbo.TextID = txtid;
			GameObject gameObject = UnityEngine.Object.Instantiate(ResourceLoadManager.GetInstance().LoadObjByPath<GameObject>("UI/HuoDong/HuoDongGardonCommbo"));
			gameObject.transform.SetParent(base.transform);
			(gameObject.transform as RectTransform).anchoredPosition = new Vector2(0f, 0f);
			gameObject.transform.localScale = Vector3.one;
		}

		private IEnumerator ShowHuoDongSunResult()
		{
			_showResultUI = true;
			m_ObjMask0.SetActive(value: true);
			_IsLastWord = false;
			AudioControl.GetInstance().StopEffect(AudioControl.GetInstance().SF_LastWord);
			yield return new WaitForSeconds(1f);
			PlayerInfo.Instance.GetHuoDongSunSaveData().CurLevelGetSun = huodong_sun_main.GetSunScore();
			PlayerInfo.Instance.GetHuoDongSunSaveData().IsCurLevelComplete = true;
			SaveGame();
			yield return new WaitForSeconds(0.2f);
			UIOutAni();
			yield return new WaitForSeconds(0.5f);
			m_GoldItem.SetActive(value: false);
			if (huodong_sun_main != null)
			{
				huodong_sun_main.Result();
			}
		}

		private IEnumerator ShowDailyResult(float time)
		{
			_showResultUI = true;
			WordGlobal.IsGuideCheck = false;
			_isCurDailyLevelFinished = true;
			PlayerInfo.Instance.SavePuzzleResultData(_curGetStarNum);
			PlayerInfo.Instance.GetDailyQuestSaveData().CollectDPStar_num = _curGetStarNum;
			SaveGame();
			WordFaceBook.Instance.UpdateUserdataMill();
			m_ObjMask0.SetActive(value: true);
			_IsLastWord = false;
			AudioControl.GetInstance().StopEffect(AudioControl.GetInstance().SF_LastWord);
			yield return new WaitForSeconds(time);
			yield return new WaitForSeconds(0.2f);
			StartCoroutine(ShowPuzzleCollectUI(1));
		}

		public bool isShowResult()
		{
			return _showResultUI;
		}

		private IEnumerator ShowResult(float time)
		{
			Debug.Log("showresult01");
			_showResultUI = true;
			WordGlobal.IsGuideCheck = false;
			SaveGame();
			if (hdmtip != null)
			{
				hdmtip.SetState(0);
			}
			m_ObjMask0.SetActive(value: true);
			_IsLastWord = false;
			AudioControl.GetInstance().StopEffect(AudioControl.GetInstance().SF_LastWord);
			yield return new WaitForSeconds(time);
			yield return new WaitForSeconds(0.2f);
			Debug.Log("showresult02");
			UIOutAni();
			yield return new WaitForSeconds(0.5f);
			bool isShowPigGold = false;
			Debug.Log("showresult03 _currentLevelScore:" + _currentLevelScore);
			if (_currentLevelScore > 0)
			{
				PlayerInfo.Instance.GetPigGoldSaveData().OpenPig(isNeedAddOne: true);
				if (PlayerInfo.Instance.GetPigGoldSaveData().isOpen() && PlayerInfo.Instance.GetPigGoldSaveData().isShowUI())
				{
					isShowPigGold = true;
				}
			}
			GameObject obj = null;
			if (isShowPigGold)
			{
				PiggyBankUI.isNeedResult = true;
				if (!PlayerInfo.Instance.GetPigGoldSaveData().ShowUI(_currentLevelScore, _item.WordList.Count))
				{
					obj = UIManager.Instance.LoadMainUI("UI/Main/Result", isPop: true, UIManager.MainUIType.Type_Common, 99);
					obj.name = "Result";
					ShowScroeUI(isShow: false);
					isShowPigGold = false;
					PiggyBankUI.isNeedResult = false;
				}
			}
			else
			{
				obj = UIManager.Instance.LoadMainUI("UI/Main/Result", isPop: true, UIManager.MainUIType.Type_Common, 99);
				ShowScroeUI(isShow: false);
			}
			Debug.Log("showresult04 isShowPigGold:" + isShowPigGold);
			Text[] temp_childs = Instance.selectLetterAnim_rect.GetComponentsInChildren<Text>();
			for (int i = 0; i < temp_childs.Length; i++)
			{
				if (temp_childs[i] != null)
				{
					UnityEngine.Object.Destroy(temp_childs[i].gameObject);
				}
			}
			_effectControl.StopFullScrenCombo();
			WordGlobal.ClearChilds(m_TfItemRoot);
			SetUIHidden(hidden: true);
			yield return new WaitForSeconds(0.2f);
			AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_GetCoin);
			Debug.Log("showresult05");
			if (_currentLevelScore > 0)
			{
				PlayerInfo.Instance.Score += _currentLevelScore;
				PlayerInfo.Instance.GetDailyQuestSaveData().CollectScore_num += _currentLevelScore;
				if (!isShowPigGold)
				{
					obj.GetComponent<MainResultScript>().SetContent(_currentLevelScore);
					m_TxtStarCount.text = "0";
				}
				_currentLevelScore = 0;
			}
			else if (!isShowPigGold)
			{
				obj.GetComponent<MainResultScript>().SetContent(_currentLevelScore);
			}
		}

		public void SetTxtStarCount(int score)
		{
			m_TxtStarCount.text = score + string.Empty;
		}

		private void SetUIHidden(bool hidden)
		{
		}

		private IEnumerator ShowPuzzleCollectUI(int openType)
		{
			UIOutAni();
			if (WordGlobal.isMainUIShowBanner)
			{
				AdsManager.Instance.HideBannerAD();
			}
			float delayTime = 0f;
			if (openType == 1)
			{
				delayTime = 0.1f;
			}
			yield return new WaitForSeconds(delayTime);
			DailyPuzzleObj.SetActive(value: false);
			m_GoldItem.SetActive(value: false);
			DPCollectPicUIScript.UI_OPENTYPE = openType;
			UIManager.Instance.LoadMainUI("UI/DailyPuzzle/DPCollectPicUI").name = "DPCollectPicUI";
			Instance.SaveGame();
		}

		public void BackToPuzzleCollect()
		{
			SaveGame();
			StartCoroutine(ShowPuzzleCollectUI(0));
		}

		private void OnDestroy()
		{
		}

		public void DestroyGo()
		{
			UnityEngine.Object.Destroy(base.gameObject);
			if (WordGlobal.isMainUIShowBanner)
			{
				AdsManager.Instance.DestroyBannerAD();
			}
			UIManager.Instance.LoadLevelMapUI(UIManager.Instance.Path_LevelMapUI, CurBackLevelMapType);
			ResourceLoadManager.GetInstance().HideNetWaiting();
			SaveGame();
			CatmullDraw.OnDrawLineEnd -= TryAddWord;
			CatmullDraw.OnClearLine -= ClearLetterImg;
			PlayerInfo.GetOfferEvent -= GetPopOfferEvent;
			Instance = null;
		}

		public void BtnBonusClick()
		{
			GuideLineDraw.GuideLetterShowState = 2;
			UIManager.Instance.LoadMainUI("UI/Main/BonusUI", isPop: true, UIManager.MainUIType.Type_Common, 99).GetComponent<BonusUIScript>().SetContent(_bonusList);
		}

		private void CheckMoneyWord(string letter, int row, int col)
		{
			Transform tf = GetLetterItem(row, col).transform.Find("gold");
			if ((bool)tf)
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_GetCoin);
				_collectedMoneyWord.Add(letter);
				tf.gameObject.transform.DOMove(m_TfGoldIcon.position, 1f).OnComplete(delegate
				{
					UnityEngine.Object.Destroy(tf.gameObject);
					AddGold(1);
				});
			}
		}

		public void BtnWatchVideo()
		{
			WordGlobal.DebugLog("激励视频");
			GuideLineDraw.GuideLetterShowState = 2;
			if (AdsManager.Instance.ShowIncentivizedAd("TapAdbutton"))
			{
				BtnVideoScript.isStop = true;
				AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
			}
		}

		private void RewardADEvent(bool ret)
		{
			BtnVideoScript.isStop = false;
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
			if (ret)
			{
				CommonTipsScript.Instance.ShowUI(string.Format(WordGlobal.ReadText(1112), PlayerInfo.Instance.systemConfig.WatchAdGold), 0, WordGlobal.ReadText(1016), delegate(object obj)
				{
					Vector3 starPos = (Vector3)obj;
					GoldScript goldScript = UnityEngine.Object.FindObjectOfType<GoldScript>();
					goldScript.CollectGold(starPos, 5, 1f, null);
					goldScript.AddGold((int)PlayerInfo.Instance.systemConfig.WatchAdGold, 1f, 1f, isAdd2PlayerInfo: true, 14);
				}, showClose: false);
			}
		}

		public void BtnDengPaoVideo()
		{
			WordGlobal.DebugLog("免费灯泡");
			GuideLineDraw.GuideLetterShowState = 2;
			if (AdsManager.Instance.ShowIncentivizedAd("HintAD_Tap"))
			{
				BtnVideoScript.isStop = true;
				AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardDPEvent));
			}
		}

		private void RewardDPEvent(bool ret)
		{
			BtnVideoScript.isStop = false;
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardDPEvent));
			if (ret)
			{
				FreeDP();
				BtnVideoScript.dp_wait_showTime = 0f;
			}
		}

		public void FreeDP(bool isshoweff = true)
		{
			isFreeDP = true;
			isFreeDPShowEff = isshoweff;
			BtnTipsClick();
			isFreeDP = false;
		}

		public void BtnCloseClick()
		{
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
			{
				if (GameObject.Find("DPCollectPicUI") == null)
				{
					if (WordGlobal.LevelMapOpenShowUI == "WordSearch")
					{
						if (base.gameObject != null)
						{
							DestroyGo();
						}
					}
					else
					{
						BackToPuzzleCollect();
					}
				}
				else if (base.gameObject != null)
				{
					DestroyGo();
				}
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
			{
				if (isFinishNullLetters)
				{
					Debug.Log("正在播放结果，不处理逻辑");
				}
				else if (GameObject.Find("HuoDongGardonUI") == null && WordGlobal.GetHuoDongGardonTimeLeft() > 0)
				{
					if ((!(Instance != null) || !Instance.isUInAnimGardon) && GameObject.Find("HuoDongGardonBackLevel") == null)
					{
						UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonBackLevel", isPop: true, UIManager.MainUIType.Type_Common, 99);
					}
				}
				else
				{
					DestroyGo();
				}
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
			{
				if (WordGlobal.GetHuoDongSunTimeLeft() <= 0)
				{
					Instance.isHuoDongSunDestroy = true;
				}
				if (isHuoDongSunDestroy)
				{
					DestroyGo();
					isHuoDongSunDestroy = false;
					return;
				}
				UIOutAni();
				m_GoldItem.SetActive(value: false);
				HuoDongSunUI.ShowType = 0;
				HuoDongSunUI.isInAnim = true;
				SaveGame();
				if (GameObject.Find("HuoDongSunUI") == null)
				{
					UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongSunUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
				}
			}
			else
			{
				DestroyGo();
			}
		}

		public void BtnSettingClick()
		{
			if (GameObject.Find("SettingUI") == null)
			{
				UIManager.Instance.LoadMainUI("UI/SettingUI").name = "SettingUI";
			}
		}

		public void AddGold(int count, float delayTime = 0f)
		{
			if (!m_GoldItem.activeInHierarchy)
			{
				m_GoldItem.SetActive(value: true);
			}
			m_GoldItem.GetComponent<GoldScript>().AddGold(count, delayTime, 1f, isAdd2PlayerInfo: true, 15);
		}

		public void BtnLoadShop()
		{
			if (GameObject.Find("ShopUI") == null)
			{
				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					ResourceLoadManager.GetInstance().ShowTips(1118);
				}
				else
				{
					UIManager.Instance.LoadMainUI("UI/ShopUI").name = "ShopUI";
				}
			}
		}

		public void OpenUrl(string txt)
		{
			Application.OpenURL(txt);
		}

		public void Restart()
		{
			Instance = null;
			DestroyGo();
		}

		private Vector3 GetLetterItemSize(int count)
		{
			Vector3 one = Vector3.one;
			if (count < 6)
			{
				return Vector3.one;
			}
			if (count == 6)
			{
				return 0.8873239f * Vector3.one;
			}
			return 0.7746479f * Vector3.one;
		}

		private float GetLetterItemR(int count)
		{
			float num = 0f;
			if (count < 6)
			{
				return 152f;
			}
			if (count == 6)
			{
				return 160f;
			}
			return 168f;
		}

		public void SetLastOperationPoint()
		{
			_lastOperationDate = DateTime.Now;
			_noOperationFrame = _noOperationTimes;
		}

		public void NoOperationCallEvent(object o)
		{
			double totalSeconds = (DateTime.Now - _lastOperationDate).TotalSeconds;
			if (totalSeconds >= (double)_noOperationTimes && PlayerInfo.Instance.TutorialStep >= 999)
			{
				ShakeItems();
				SetLastOperationPoint();
			}
		}

		public void ShakeItems()
		{
			MainBtnsType mainBtnsType = MainBtnsType.Tips;
			if (PlayerInfo.Instance.TipsCount <= 0 && PlayerInfo.Instance.ClickTipsCount <= 0 && PlayerInfo.Instance.FirstTipsCount <= 0)
			{
				long num = PlayerInfo.Instance.systemConfig.GetTipsPrice();
				Dictionary<MainBtnsType, long> dictionary = new Dictionary<MainBtnsType, long>();
				dictionary.Add(MainBtnsType.Tips, PlayerInfo.Instance.systemConfig.GetTipsPrice());
				dictionary.Add(MainBtnsType.TipsClick, PlayerInfo.Instance.systemConfig.ClickTipsPrice);
				dictionary.Add(MainBtnsType.AllFirstLetters, PlayerInfo.Instance.systemConfig.GetFirstLetterTipsPrice());
				foreach (KeyValuePair<MainBtnsType, long> item in dictionary)
				{
					if (num > item.Value)
					{
						num = item.Value;
						mainBtnsType = item.Key;
					}
				}
			}
			else
			{
				long num2 = PlayerInfo.Instance.systemConfig.GetTipsPrice();
				Dictionary<MainBtnsType, long> dictionary2 = new Dictionary<MainBtnsType, long>();
				if (PlayerInfo.Instance.FirstTipsCount > 0 && PlayerInfo.Instance.FirstTipButtonUnlock == 1)
				{
					dictionary2.Add(MainBtnsType.AllFirstLetters, PlayerInfo.Instance.systemConfig.GetFirstLetterTipsPrice());
				}
				if (PlayerInfo.Instance.TipsCount > 0)
				{
					dictionary2.Add(MainBtnsType.Tips, PlayerInfo.Instance.systemConfig.GetTipsPrice());
				}
				if (PlayerInfo.Instance.ClickTipsCount > 0)
				{
					dictionary2.Add(MainBtnsType.TipsClick, PlayerInfo.Instance.systemConfig.ClickTipsPrice);
				}
				foreach (KeyValuePair<MainBtnsType, long> item2 in dictionary2)
				{
					if (num2 < item2.Value)
					{
						num2 = item2.Value;
						mainBtnsType = item2.Key;
					}
				}
			}
			switch (mainBtnsType)
			{
			case MainBtnsType.Tips:
				m_ObjBtnTipsClick.GetComponent<BtnNumberPriceSet>().SetShakeAni(1);
				break;
			case MainBtnsType.TipsClick:
				m_ObjBtnCellTipsClick.GetComponent<BtnNumberPriceSet>().SetShakeAni(1);
				break;
			case MainBtnsType.AllFirstLetters:
				m_ObjBtnFirstLetter.GetComponent<BtnNumberPriceSet>().SetShakeAni(1);
				break;
			}
		}

		public List<CrossWordItem> GetUnFinishedWordList()
		{
			List<CrossWordItem> list = new List<CrossWordItem>();
			int i;
			for (i = 0; i < _item.WordList.Count; i++)
			{
				if (_currentWordList.FindIndex((string x) => x == _item.WordList[i].Word) < 0 && _tmpCompleteWordList.FindIndex((string x) => x == _item.WordList[i].Word) < 0)
				{
					list.Add(_item.WordList[i]);
				}
			}
			return list;
		}

		public List<CrossWordItem> GetHaveCharInUnFinishList()
		{
			List<CrossWordItem> list = new List<CrossWordItem>();
			List<CrossWordItem> unFinishedWordList = GetUnFinishedWordList();
			for (int i = 0; i < unFinishedWordList.Count; i++)
			{
				CrossWordItem crossWordItem = unFinishedWordList[i];
				for (int j = 0; j < crossWordItem.Word.Length; j++)
				{
					if (crossWordItem.vertical)
					{
						if (GetCell(crossWordItem.Col, crossWordItem.Row + j) != string.Empty)
						{
							list.Add(crossWordItem);
							break;
						}
					}
					else if (GetCell(crossWordItem.Col + j, crossWordItem.Row) != string.Empty)
					{
						list.Add(crossWordItem);
						break;
					}
				}
			}
			return list;
		}

		public void JudgeButterChange(int row, int col)
		{
			string[] array = _curButterFlyPos.Split('_');
			CheckWordCompleted(row, col);
			if (array.Length >= 2 && row == int.Parse(array[0]) && col == int.Parse(array[1]))
			{
				bool isWordComplete = CheckButterFlyWordCompleted(row, col);
				RefreshButterFly(isWordComplete);
			}
		}

		public void HuoDongSunItemCheck(int row, int col)
		{
			if (huodong_sun_main != null)
			{
				int item = (row - 1) * 100 + (col - 1);
				if (!huodong_sun_main.AddedSunLetter.Contains(item))
				{
					huodong_sun_main.AddedSunLetter.Add(item);
				}
				huodong_sun_main.CheckItem(row, col);
			}
		}

		public bool CheckButterFlyWordCompleted(int row, int col)
		{
			bool flag = false;
			List<CrossWordItem> words = GetWords(row, col);
			for (int i = 0; i < words.Count; i++)
			{
				flag = true;
				if (words[i].vertical)
				{
					for (int j = words[i].Row; j < words[i].Row + words[i].Word.Length; j++)
					{
						if (GetCell(col, j) == string.Empty)
						{
							flag = false;
							break;
						}
					}
				}
				else
				{
					for (int k = words[i].Col; k < words[i].Col + words[i].Word.Length; k++)
					{
						if (GetCell(k, row) == string.Empty)
						{
							flag = false;
							break;
						}
					}
				}
				if (flag)
				{
					break;
				}
			}
			return flag;
		}

		public void AddButterFlyNum()
		{
			_curMoveButterPos = string.Empty;
			int count = _item.WordList.Count;
			DailyButterFuNumText.text = _curGetButterFlyNum.ToString();
			DailyButterFuNumText.gameObject.SetActive(_curGetButterFlyNum > 0);
			StarBarFill.fillAmount = (float)_curGetButterFlyNum / (float)count;
			ChangeDailyStarObjPos();
			if (_curLightStarIndex < _dailyMaxStarNum && _curGetButterFlyNum >= _starButterNumList[_curLightStarIndex])
			{
				_curLightStarIndex++;
				DailyPuzzleStarNums[_curLightStarIndex - 1].gameObject.SetActive(value: false);
				DailyPuzzleStars[_curLightStarIndex - 1].transform.DOScale(1f, 0.5f).OnComplete(delegate
				{
					GameObject obj = UnityEngine.Object.Instantiate(DailyPuzzleStars[0]);
					obj.transform.SetParent(DailyPuzzleStars[_curLightStarIndex - 1].transform, worldPositionStays: false);
					obj.transform.localPosition = Vector3.zero;
					obj.transform.DOScale(5f, 0.5f).SetDelay(0.2f).OnComplete(delegate
					{
						UnityEngine.Object.Destroy(obj, 0.5f);
					});
					obj.GetComponent<Image>().DOFade(0f, 0.5f).SetDelay(0.2f);
				});
			}
		}

		public void AddButterData()
		{
			int count = _item.WordList.Count;
			_curGetButterFlyNum++;
			_curGetButterFlyNum = ((_curGetButterFlyNum <= count) ? _curGetButterFlyNum : count);
			if (_curGetStarNum < _dailyMaxStarNum && _curGetButterFlyNum >= _starButterNumList[_curGetStarNum])
			{
				_curGetStarNum++;
				_curGetStarNum = ((_curGetStarNum <= 3) ? _curGetStarNum : 3);
			}
		}

		public void ChangeButterParent(ItemScript orgItem, Transform parent)
		{
			orgItem.m_ItemButter.transform.SetParent(parent, worldPositionStays: false);
			orgItem.m_ItemButter.transform.localPosition = Vector3.zero;
		}

		public void ShowCurButterFly()
		{
			_curMoveButterPos = string.Empty;
			string[] array = _curButterFlyPos.Split('_');
			if (array.Length >= 2)
			{
				GetItemByRC(int.Parse(array[0]) - 1, int.Parse(array[1]) - 1).GetComponent<ItemScript>().ShowButterObj();
			}
		}

		public void BtnDailyQuestClick()
		{
			ShowDailyQuest();
		}

		public void ShowDailyQuest(int p_idx = -1)
		{
			TutorialDailyQuestFinish.isHide = true;
			if (GameObject.Find("DailyQuestUI") == null)
			{
				UIManager instance = UIManager.Instance;
				string path = "UI/DailyQuestUI";
				bool isPop = true;
				instance.LoadMainUI(path, isPop, UIManager.MainUIType.Type_Common, p_idx);
			}
		}

		private bool CheckGridCompleted()
		{
			for (int i = 0; i < _grid.Count; i++)
			{
				for (int j = 0; j < _grid[i].Count; j++)
				{
					if (_grid[i][j] == string.Empty)
					{
						return false;
					}
				}
			}
			return true;
		}

		private void GetPopOfferEvent()
		{
			if (Instance != null)
			{
				PopOfferUIScript.ShowUI();
			}
		}

		private void CheckBonus()
		{
			_minWordLength = 10;
			for (int i = 0; i < _item.WordList.Count; i++)
			{
				if (_item.WordList[i].Word.Length < _minWordLength)
				{
					_minWordLength = _item.WordList[i].Word.Length;
				}
			}
			m_ObjNo3WordBonus.SetActive(_minWordLength > 3);
			if (UIManager.CurMainUIType != UIManager.MainUIType.Type_HDSun && UIManager.CurMainUIType != UIManager.MainUIType.Type_HDGardon && _minWordLength > 3 && PlayerInfo.Instance.FirstGetNoThreeBonus == 0)
			{
				WordGlobal.InitLabel(UIManager.Instance.LoadMainUI("UI/Main/FirstGetNoThreeBonusUI", isPop: true).transform);
				PlayerInfo.Instance.FirstGetNoThreeBonus = 1;
			}
		}

		private void Clear3Bonus()
		{
			List<string> list = new List<string>();
			if (PlayerInfo.Instance.CollectedBonusList != string.Empty)
			{
				CollectedBonus collectedBonus = JsonUtility.FromJson<CollectedBonus>(PlayerInfo.Instance.CollectedBonusList);
				if (collectedBonus != null)
				{
					int lvl;
					for (lvl = 1; lvl <= PlayerInfo.Instance.MaxPlayLevel; lvl++)
					{
						WordListItem itemByLevel = WordJsonDataManager.instance.GetItemByLevel(lvl);
						int num = 10;
						for (int i = 0; i < itemByLevel.WordList.Count; i++)
						{
							if (itemByLevel.WordList[i].Word.Length < num)
							{
								num = itemByLevel.WordList[i].Word.Length;
								if (num == 3)
								{
									break;
								}
							}
						}
						if (num <= 3)
						{
							continue;
						}
						List<BonusWord> list2 = collectedBonus.BonusList.FindAll((BonusWord x) => x.Level == lvl);
						for (int j = 0; j < list2.Count; j++)
						{
							if (list2[j].Word.Length == 3)
							{
								collectedBonus.BonusList.Remove(list2[j]);
							}
						}
						PlayerInfo.Instance.CollectedBonusList = JsonUtility.ToJson(collectedBonus);
					}
				}
			}
			PlayerInfo.Instance.Clear3Bonus = 1;
		}

		public void CheckWordShowDictionaryUI(string name)
		{
			string[] array = name.Split('-');
			int num = int.Parse(array[1]);
			int num2 = int.Parse(array[2]);
			List<CrossWordItem> tmplist = GetWords(num + 1, num2 + 1);
			List<string> list = new List<string>();
			int i;
			for (i = 0; i < tmplist.Count; i++)
			{
				if (_currentWordList.FindIndex((string x) => x == tmplist[i].Word) >= 0)
				{
					list.Add(tmplist[i].Word);
				}
			}
			if (list.Count > 0)
			{
				list.Sort((string x, string y) => y.Length.CompareTo(x.Length));
				DictionaryUIScript.ShowUI(list[0]);
			}
		}

		public void BtnAAClick()
		{
			if (m_DictionaryCurrentIndex == 0)
			{
				m_DictionaryCurrentIndex = _dictionaryList.Count;
			}
			DictionaryUIScript.ShowUI(WordGlobal.ListToStr(_dictionaryList));
		}
	}
