using System;
using UnityEngine;

[Serializable]
public class MapDataCsv
{
	public int ChapterID;

	public string ChapterName;

	private int chapterNameId;

	public string CIcon;

	public int LevelStartNum;

	public int LevelEndNum;

	public string LIcon;

	public string Image;

	public int RewardCoin;

	public int Style;

	public int Position;

	public string Linecolor;

	public string Textcolor;

	public int Dishtype;

	public int MapInListIndex = -1;

	private Color line_color = Color.white;

	private Color text_color = Color.white;

	public MapDataCsv(string[] values, int valueIndex, int indexInList)
	{
		int num = 0;
		ChapterID = int.Parse(values[num++]);
		chapterNameId = int.Parse(values[num++]);
		CIcon = values[num++];
		string[] array = values[num++].Split('_');
		LevelStartNum = int.Parse(array[0]);
		LevelEndNum = int.Parse(array[1]);
		LIcon = values[num++];
		Image = values[num++];
		RewardCoin = int.Parse(values[num++]);
		Style = int.Parse(values[num++]);
		Position = int.Parse(values[num++]);
		Linecolor = values[num++];
		Textcolor = values[num++];
		Dishtype = int.Parse(values[num++]);
		MapInListIndex = indexInList;
	}

	public MapDataCsv()
	{
		ChapterID = 0;
		MapInListIndex = 0;
	}

	public string GetChapterName()
	{
		if (string.IsNullOrEmpty(ChapterName))
		{
			ChapterName = TextLibraryDataManagerCsv.instance.GetText(chapterNameId);
		}
		return ChapterName;
	}

	public Color getLineColor()
	{
		if (line_color.Equals(Color.white))
		{
			string str = "#" + Linecolor;
			if (Linecolor.Contains("#"))
			{
				str = Linecolor;
			}
			line_color = WordGlobal.ChangeStringToColor(str);
		}
		return line_color;
	}

	public Color getTextColor()
	{
		if (text_color.Equals(Color.white))
		{
			string str = "#" + Textcolor;
			if (Textcolor.Contains("#"))
			{
				str = Textcolor;
			}
			text_color = WordGlobal.ChangeStringToColor(str);
		}
		return text_color;
	}

	private int GetMapIndexMapList()
	{
		return LevelEndNum / WordGlobal.SmallChaptherLevelNum - 1;
	}

	public int GetMinToMaxValue()
	{
		return LevelEndNum - LevelStartNum + 1;
	}
}
