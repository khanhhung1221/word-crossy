using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class MapDataManager : ManagerCsv
{
	private static MapDataManager inst;

	private Dictionary<int, MapDataCsv> MapListByLevel;

	private Dictionary<int, Dictionary<int, MapDataCsv>> MapListByChapter;

	private List<MapDataCsv> MapList;

	private List<int> ChapterIndexList;

	public Queue<string> NeedDownLoadFiles = new Queue<string>();

	private static MapDataCsv nullData = new MapDataCsv();

	private int maxLevel;

	public static MapDataManager instance => inst ?? (inst = new MapDataManager());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			int valueIndex = 0;
			MemoryStream stream = new MemoryStream(file);
			MapListByLevel = new Dictionary<int, MapDataCsv>();
			MapListByChapter = new Dictionary<int, Dictionary<int, MapDataCsv>>();
			MapList = new List<MapDataCsv>();
			ChapterIndexList = new List<int>();
			NeedDownLoadFiles = new Queue<string>();
			int num = 0;
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								MapDataCsv mapDataCsv = new MapDataCsv(array, valueIndex, num);
								MapListByLevel[mapDataCsv.LevelEndNum] = mapDataCsv;
								if (maxLevel < mapDataCsv.LevelEndNum)
								{
									maxLevel = mapDataCsv.LevelEndNum;
								}
								if (MapListByChapter.ContainsKey(mapDataCsv.ChapterID))
								{
									Dictionary<int, MapDataCsv> dictionary = MapListByChapter[mapDataCsv.ChapterID];
									if (MapListByChapter[mapDataCsv.ChapterID].ContainsKey(mapDataCsv.LevelEndNum))
									{
										dictionary[mapDataCsv.LevelEndNum] = mapDataCsv;
									}
									else
									{
										dictionary.Add(mapDataCsv.LevelEndNum, mapDataCsv);
									}
								}
								else
								{
									Dictionary<int, MapDataCsv> dictionary2 = new Dictionary<int, MapDataCsv>();
									dictionary2.Add(mapDataCsv.LevelEndNum, mapDataCsv);
									MapListByChapter.Add(mapDataCsv.ChapterID, dictionary2);
									ChapterIndexList.Add(mapDataCsv.MapInListIndex);
								}
								MapList.Add(mapDataCsv);
								num++;
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public void CheckDownLoadFile(int playerMaxLevel = 0)
	{
		List<string> list = new List<string>();
		List<string> list2 = new List<string>();
		NeedDownLoadFiles.Clear();
		for (int i = 0; i < MapList.Count; i++)
		{
			MapDataCsv mapDataCsv = MapList[i];
			if (mapDataCsv.ChapterID <= WordGlobal.LevelMapImgDLMinNum)
			{
				continue;
			}
			bool flag = false;
			if (mapDataCsv.LevelEndNum >= playerMaxLevel)
			{
				flag = true;
			}
			string str = ".png";
			string text = "Image/LevelMap/" + mapDataCsv.CIcon + str;
			string path = WordGlobal.DataPath + text;
			if (!WordGlobal.isFileExist(path))
			{
				if (flag)
				{
					list2.Add(text);
				}
				else
				{
					list.Add(text);
				}
			}
			str = ".png";
			text = "Image/LevelMap/" + mapDataCsv.LIcon + str;
			path = WordGlobal.DataPath + text;
			if (!WordGlobal.isFileExist(path))
			{
				if (flag)
				{
					list2.Add(text);
				}
				else
				{
					list.Add(text);
				}
			}
			str = ".jpg";
			text = "Image/LevelMap/" + mapDataCsv.Image + str;
			path = WordGlobal.DataPath + text;
			if (!WordGlobal.isFileExist(path))
			{
				if (flag)
				{
					list2.Add(text);
				}
				else
				{
					list.Add(text);
				}
			}
		}
		for (int j = 0; j < list2.Count; j++)
		{
			string item = list2[j];
			if (!NeedDownLoadFiles.Contains(item))
			{
				NeedDownLoadFiles.Enqueue(item);
			}
		}
		for (int k = 0; k < list.Count; k++)
		{
			string item2 = list[k];
			if (!NeedDownLoadFiles.Contains(item2))
			{
				NeedDownLoadFiles.Enqueue(item2);
			}
		}
	}

	public MapDataCsv GetMapData(int level)
	{
		if (level > maxLevel)
		{
			level = maxLevel;
		}
		MapDataCsv result = null;
		foreach (KeyValuePair<int, MapDataCsv> item in MapListByLevel)
		{
			MapDataCsv value = item.Value;
			if (level >= value.LevelStartNum && level <= value.LevelEndNum)
			{
				result = value;
			}
		}
		return result;
	}

	public bool isNeedQuest60(int level)
	{
		foreach (KeyValuePair<int, MapDataCsv> item in MapListByLevel)
		{
			MapDataCsv value = item.Value;
			if (level >= value.LevelStartNum && level <= value.LevelEndNum && value.LevelEndNum - level >= 5)
			{
				return true;
			}
		}
		return false;
	}

	public MapDataCsv GetMapDataByIndex(int index)
	{
		MapDataCsv result = null;
		if (index < MapList.Count)
		{
			result = MapList[index];
		}
		return result;
	}

	public int GetMaxLevel()
	{
		return maxLevel;
	}

	public int GetSmallChapterCount()
	{
		return MapListByLevel.Count;
	}

	public int GetBigChapterCount()
	{
		return MapListByChapter.Count;
	}

	public bool IndexIsBigChapter(int index)
	{
		return ChapterIndexList.Contains(index);
	}

	public MapDataCsv GetBigChapterData(int chapterId)
	{
		MapDataCsv result = null;
		for (int i = 0; i < ChapterIndexList.Count; i++)
		{
			MapDataCsv mapDataByIndex = GetMapDataByIndex(ChapterIndexList[i]);
			if (mapDataByIndex.ChapterID == chapterId)
			{
				result = mapDataByIndex;
			}
		}
		return result;
	}

	public string GetChapterStartEndLevel(int chapterId)
	{
		int num = 0;
		int num2 = 0;
		string result = null;
		if (MapListByChapter.ContainsKey(chapterId))
		{
			Dictionary<int, MapDataCsv> dictionary = MapListByChapter[chapterId];
			List<int> list = new List<int>(dictionary.Keys);
			for (int i = 0; i < list.Count; i++)
			{
				MapDataCsv mapDataCsv = dictionary[list[i]];
				if (num <= 0 || num > mapDataCsv.LevelStartNum)
				{
					num = mapDataCsv.LevelStartNum;
				}
				if (num2 <= 0 || num2 < mapDataCsv.LevelEndNum)
				{
					num2 = mapDataCsv.LevelEndNum;
				}
			}
		}
		if (num > 0 && num2 > 0)
		{
			result = num + "_" + num2;
		}
		return result;
	}

	public GameObject GetLevelSpriteEffect(int level)
	{
		if (level > 0 && level <= maxLevel)
		{
			string image = GetMapData(level).Image;
			string str = "FX/BFX_";
			if (image != null && image != string.Empty)
			{
				return ResourceLoadManager.GetInstance().LoadObjByPath(str + image);
			}
		}
		return null;
	}

	public Sprite GetLevelSprite(int level)
	{
		if (level > 0 && level <= maxLevel)
		{
			string image = GetMapData(level).Image;
			string str = "Image/LevelMap/";
			if (image != null && image != string.Empty)
			{
				return ResourceLoadManager.GetInstance().LoadSpriteByPath(str + image, ResourceLoadManager.SP_LEVELMAP_TYPE.BG);
			}
		}
		return null;
	}

	public List<string> GetAllMapStyleName()
	{
		List<string> list = new List<string>();
		for (int i = 0; i < MapList.Count; i++)
		{
			string item = "style_" + MapList[i].Style;
			list.Add(item);
		}
		return list;
	}
}
