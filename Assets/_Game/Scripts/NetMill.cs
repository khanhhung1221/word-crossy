using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class NetMill : MonoBehaviour
{
	public enum RequestType
	{
		AccountServerRequest,
		GameServerRequest
	}

	public delegate bool CommandCallback(JsonData data);

	public delegate void FinishCallback(object custom_data);

	public class CommandCallbackEvent
	{
		public event CommandCallback callbackEvent;

		public bool Invoke(JsonData data)
		{
			return this.callbackEvent(data);
		}
	}

	private struct NetPackage
	{
		public Dictionary<string, CommandCallback> cmdCallbacks;

		public string dataStr;

		public RequestType type;

		public object customData;

		public FinishCallback finishCallback;

		public string url;
	}

	public delegate void DownloadOkHandler(object content, object userData);

	public delegate void DownloadErrorHandler(object userData, int code, string msg);

	private static NetMill instance;

	private const string URL_PREFIX = "http://";

	private const string URL_GAMESERVER_SUFFIX = "";

	private static string gameServerUrl;

	private static string accoountServerHost = "words.lettucegames.com:8089";

	private static Dictionary<string, CommandCallbackEvent> defaultCallbacks = new Dictionary<string, CommandCallbackEvent>();

	private static Queue<NetPackage> sendQueue = new Queue<NetPackage>();

	private static int nextSeq = 0;

	private const int version = 2;

	private static bool sending = false;

	private static Coroutine currentCoroutine;

	public const int NET_ERROR = 0;

	public const int NET_EXCEPT = -1;

	public static NetMill Instance => GetInstance();

	private static NetMill GetInstance()
	{
		if ((bool)instance)
		{
			return instance;
		}
		GameObject gameObject = new GameObject("NetMill", typeof(NetMill));
		return gameObject.GetComponent<NetMill>();
	}

	public static void UpdateGameSeverUrl(string host_port)
	{
		gameServerUrl = "http://" + host_port + string.Empty;
		Debug.Log(gameServerUrl);
	}

	public static string ParseErrorMsg(JsonData data)
	{
		if (data != null && data.IsObject)
		{
			if (((IDictionary)data).Contains((object)"errmsg"))
			{
				return data["errmsg"].ToString();
			}
			if (((IDictionary)data).Contains((object)"errno"))
			{
				return string.Empty;
			}
			return "Unknown";
		}
		return "Unknown";
	}

	public static bool IsError(JsonData data)
	{
		if (data != null && data.IsObject)
		{
			if (((IDictionary)data).Contains((object)"errmsg") || ((IDictionary)data).Contains((object)"errno"))
			{
				return true;
			}
			return false;
		}
		return true;
	}

	private void Awake()
	{
		if (instance != null)
		{
			UnityEngine.Object.DestroyImmediate(base.gameObject);
			return;
		}
		instance = this;
		accoountServerHost = WordGlobal.HttpAccountServer;
		UnityEngine.Object.DontDestroyOnLoad(this);
	}

	public static void ResetSeq()
	{
		nextSeq = 0;
	}

	public static void RegisterDefaultCallback(string name, CommandCallback callback)
	{
		if (!defaultCallbacks.ContainsKey(name))
		{
			defaultCallbacks.Add(name, new CommandCallbackEvent());
		}
		defaultCallbacks[name].callbackEvent += callback;
	}

	public static void UnregisterDefaultCallback(string name, CommandCallback callback)
	{
		if (defaultCallbacks.ContainsKey(name))
		{
			defaultCallbacks[name].callbackEvent -= callback;
		}
	}

	public void Send(RequestType type, string url, JsonData cmd_data, Dictionary<string, CommandCallback> callbacks = null, FinishCallback finishCallback = null, object custom_data = null)
	{
		NetPackage item = default(NetPackage);
		item.cmdCallbacks = callbacks;
		item.customData = custom_data;
		item.finishCallback = finishCallback;
		item.url = url;
		JsonData jsonData = new JsonData();
		switch (type)
		{
		case RequestType.AccountServerRequest:
			jsonData = cmd_data;
			break;
		}
		item.dataStr = jsonData.ToJson();
		item.type = type;
		sendQueue.Enqueue(item);
		if (!sending)
		{
			StartSend();
		}
	}

	private void InvokeCallback(NetPackage pkg, JsonData data, string callback_name)
	{
		if (defaultCallbacks.ContainsKey(callback_name))
		{
			defaultCallbacks[callback_name].Invoke(data);
		}
		else if (callback_name.Equals("EDATA"))
		{
			ServerResponseError(data);
		}
		else if (callback_name.Equals("EWWW"))
		{
			CheckNetwork(data);
		}
		InvokeCallback(pkg, null);
	}

	private void InvokeCallback(NetPackage pkg, JsonData data)
	{
		if (pkg.cmdCallbacks != null)
		{
			foreach (KeyValuePair<string, CommandCallback> cmdCallback in pkg.cmdCallbacks)
			{
				cmdCallback.Value(data);
			}
		}
	}

	private void StartSend()
	{
		if (currentCoroutine != null)
		{
			StopCoroutine(currentCoroutine);
		}
		currentCoroutine = StartCoroutine(SendRequest());
	}

	public void ResetSendQueue()
	{
		if (currentCoroutine != null)
		{
			StopCoroutine(currentCoroutine);
		}
		sendQueue.Clear();
		sending = false;
		ResetSeq();
	}

	public IEnumerator SendRequest()
	{
		sending = true;
		while (sendQueue.Count > 0)
		{
			NetPackage pkg = sendQueue.Peek();
			string gameServerUrl2 = gameServerUrl;
			UnityWebRequest www = new UnityWebRequest();
			new Dictionary<string, string>();
			new List<IMultipartFormSection>();
			switch (pkg.type)
			{
			case RequestType.GameServerRequest:
			{
				string url2 = gameServerUrl;
				www = UnityWebRequest.Put(url2, pkg.dataStr);
				break;
			}
			case RequestType.AccountServerRequest:
			{
				string url2 = accoountServerHost + pkg.url;
				Dictionary<string, string> dictionary = new Dictionary<string, string>();
				dictionary.Add("data", pkg.dataStr);
				dictionary.Add("crc", WordGlobal.GetCrcHash(pkg.dataStr));
				www = UnityWebRequest.Post(url2, dictionary);
				www.chunkedTransfer = false;
				break;
			}
			}
			Debug.Log(pkg.dataStr);
			yield return www.SendWebRequest();
			if (www.isNetworkError)
			{
				WordGlobal.DebugLog("error:" + www.error);
				JsonData jsonData = new JsonData();
				jsonData["error"] = "EWWW";
				jsonData["errmsg"] = www.error;
				sending = false;
				sendQueue.Dequeue();
				InvokeCallback(pkg, jsonData, "EWWW");
				break;
			}
			JsonData jsonData2 = new JsonData();
			try
			{
				switch (pkg.type)
				{
				case RequestType.AccountServerRequest:
					jsonData2 = JsonMapper.ToObject(www.downloadHandler.text);
					break;
				}
			}
			catch
			{
				Debug.LogError(www.downloadHandler.text);
				sending = false;
				InvokeCallback(pkg, null, "EDATA");
				sendQueue.Dequeue();
				break;
			}
			if (jsonData2 == null)
			{
				InvokeCallback(pkg, null, "EDATA");
				sendQueue.Dequeue();
				break;
			}
			InvokeCallback(pkg, jsonData2);
			sendQueue.Dequeue();
		}
		sending = false;
	}

	public void Download(string url, string localPath, object userData, DownloadOkHandler okHandler, DownloadErrorHandler errorHandler)
	{
		StartCoroutine(StartDownload(url, localPath, userData, okHandler, errorHandler));
	}

	public IEnumerator StartDownload(string url, string localPath, object userData, DownloadOkHandler okHandler, DownloadErrorHandler errorHandler)
	{
		WWW www = new WWW(url);
		try
		{
			yield return www;
			try
			{
				if (www.error != null)
				{
					errorHandler?.Invoke(userData, 0, www.error);
				}
				else
				{
					try
					{
						byte[] bytes = www.bytes;
						using (FileStream fileStream = new FileStream(localPath, FileMode.Create))
						{
							try
							{
								Debug.Log("Download end file:" + (string)userData + "  bytes.Length:" + bytes.Length);
								fileStream.Write(bytes, 0, bytes.Length);
							}
							catch (Exception message)
							{
								WordGlobal.DebugLog(message);
							}
						}
						if (WordGlobal.DownEndFileVersion != 0)
						{
							PlayerInfo.Instance.CsvFileVersion = WordGlobal.DownEndFileVersion;
						}
					}
					catch (Exception exception)
					{
						Debug.LogException(exception);
					}
					okHandler?.Invoke(www.bytes, userData);
				}
			}
			catch (Exception ex)
			{
				Debug.LogException(ex);
				errorHandler?.Invoke(userData, -1, ex.ToString());
			}
		}
		finally
		{
			//base._003C_003E__Finally0();
		}
	}

	public bool CheckNetwork(JsonData data)
	{
		return true;
	}

	public bool ServerResponseError(JsonData data)
	{
		return true;
	}
}
