using UnityEngine;

public class NetWaiting : MonoBehaviour
{
	public static float HideTime;

	private void Start()
	{
	}

	private void Update()
	{
		if (HideTime > 0f)
		{
			HideTime -= Time.deltaTime;
			if (HideTime <= 0f)
			{
				Object.Destroy(base.gameObject);
			}
		}
	}
}
