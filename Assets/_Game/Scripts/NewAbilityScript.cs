using UnityEngine;

public class NewAbilityScript : MonoBehaviour
{
	public GameObject BgObj;

	public void SetUI(bool isShowBg)
	{
		if (isShowBg)
		{
			BgObj.SetActive(value: true);
		}
		else
		{
			BgObj.SetActive(value: false);
		}
	}

	public void BtnBgClick()
	{
	}

	private void Start()
	{
	}

	private void Awake()
	{
		WordGlobal.InitLabel(base.transform);
	}

	private void Update()
	{
	}
}
