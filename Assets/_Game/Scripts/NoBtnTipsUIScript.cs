using UnityEngine;
using UnityEngine.UI;

public class NoBtnTipsUIScript : MonoBehaviour
{
	public Text ShowText;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void ShowUI(int textId)
	{
		ShowText.text = TextLibraryDataManagerCsv.instance.GetText(textId);
	}

	public void BtnCloseClick()
	{
		Object.Destroy(base.gameObject);
	}
}
