using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class NotificationDataManager : ManagerCsv
{
	private static NotificationDataManager inst;

	private Dictionary<int, NotificationItem> DataList;

	public static NotificationDataManager instance => inst ?? (inst = new NotificationDataManager());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			MemoryStream stream = new MemoryStream(file);
			DataList = new Dictionary<int, NotificationItem>();
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								NotificationItem notificationItem = new NotificationItem(array);
								DataList.Add(notificationItem.id, notificationItem);
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public NotificationItem Get12Data()
	{
		return DataList[1];
	}

	public List<NotificationItem> Get18DataList()
	{
		List<NotificationItem> list = new List<NotificationItem>();
		foreach (KeyValuePair<int, NotificationItem> data in DataList)
		{
			if (data.Key > 1)
			{
				list.Add(data.Value);
			}
		}
		return list;
	}
}
