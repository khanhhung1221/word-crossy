public class NotificationItem
{
	public int id;

	public string time;

	public string title;

	public string message;

	public string messageID;

	public NotificationItem(string[] values)
	{
		int num = 0;
		id = int.Parse(values[num++]);
		time = values[num++];
		message = values[num++];
		title = values[num++];
		messageID = values[num++];
	}
}
