using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class OfferDataManager : ManagerCsv
{
	private static OfferDataManager inst;

	private Dictionary<int, OfferItem> OfferItemList;

	public static OfferDataManager instance => inst ?? (inst = new OfferDataManager());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			MemoryStream stream = new MemoryStream(file);
			OfferItemList = new Dictionary<int, OfferItem>();
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								OfferItem offerItem = new OfferItem(array);
								OfferItemList.Add(offerItem.ID, offerItem);
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public OfferItem GetItemByID(int id)
	{
		if (OfferItemList.ContainsKey(id))
		{
			return OfferItemList[id];
		}
		return null;
	}

	public List<OfferItem> GetOfferList()
	{
		List<OfferItem> list = new List<OfferItem>();
		foreach (KeyValuePair<int, OfferItem> item in OfferItemList)
		{
			if (PlayerInfo.Instance.BuyInOffers.FindIndex((int x) => x == item.Value.ID) < 0)
			{
				list.Add(item.Value);
			}
		}
		return list;
	}
}
