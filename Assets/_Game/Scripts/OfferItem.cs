using LitJson;
using System;

[Serializable]
public class OfferItem
{
	public int ID;

	public float Price;

	public int Coins;

	public bool AdsOff;

	public string Value;

	public string Productid;

	public string Icon;

	public int Type;

	public int FigerCount;

	public int BulbCount;

	public int LightingCount;

	public int Title;

	public int LastTime;

	public OfferItem(string[] values)
	{
		int num = 0;
		ID = int.Parse(values[num++]);
		Price = float.Parse(values[num++]);
		Icon = values[num++];
		Coins = int.Parse(values[num++]);
		AdsOff = Convert.ToBoolean(int.Parse(values[num++]));
		Value = values[num++];
		Productid = values[num++];
	}

	public OfferItem()
	{
	}

	public OfferItem(JsonData data)
	{
		ID = int.Parse(data["id"].ToString());
		Coins = int.Parse(data["coins"].ToString());
		Value = data["value"].ToString();
		Productid = data["productid"].ToString();
		Price = float.Parse(data["price"].ToString());
		Type = int.Parse(data["type"].ToString());
		Icon = data["icon"].ToString();
		AdsOff = Convert.ToBoolean(int.Parse(data["adsoff"].ToString()));
		//int.TryParse(data["item1"].ToString(), out BulbCount);
		//int.TryParse(data["item2"].ToString(), out FigerCount);
		//int.TryParse(data["item3"].ToString(), out LightingCount);
		//int.TryParse(data["name"].ToString(), out Title);
		//int.TryParse(data["lasttime"].ToString(), out LastTime);
	}
}
