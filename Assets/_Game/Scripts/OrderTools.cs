using UnityEngine;

public class OrderTools : MonoBehaviour
{
	public Canvas base_canvas;

	public int AddOrderNum;

	public int base_order;

	private void Start()
	{
		if (base_canvas != null)
		{
			base_order = base_canvas.sortingOrder;
		}
		int sortingOrder = base_order + AddOrderNum;
		Renderer component = base.gameObject.GetComponent<Renderer>();
		if (component != null)
		{
			component.sortingOrder = sortingOrder;
		}
	}
}
