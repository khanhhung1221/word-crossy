using System;
using System.Security.Cryptography;
using System.Text;

public class PBEWithMD5AndDESEncryption
{
	private static byte[] salt = new byte[8]
	{
		1,
		9,
		7,
		5,
		3,
		2,
		0,
		1
	};

	public static string EncryptTxt(string clearText)
	{
		if (string.IsNullOrEmpty(clearText))
		{
			return clearText;
		}
		PKCSKeyGenerator pKCSKeyGenerator = new PKCSKeyGenerator("papaya word 1.5", salt, 1, 1);
		ICryptoTransform encryptor = pKCSKeyGenerator.Encryptor;
		byte[] inArray = encryptor.TransformFinalBlock(Encoding.UTF8.GetBytes(clearText), 0, clearText.Length);
		return Convert.ToBase64String(inArray);
	}

	public static string DecryptTxt(string cipherText)
	{
		if (string.IsNullOrEmpty(cipherText))
		{
			return cipherText;
		}
		PKCSKeyGenerator pKCSKeyGenerator = new PKCSKeyGenerator("papaya word 1.5", salt, 1, 1);
		ICryptoTransform decryptor = pKCSKeyGenerator.Decryptor;
		byte[] array = Convert.FromBase64String(cipherText);
		byte[] bytes = decryptor.TransformFinalBlock(array, 0, array.Length);
		return Encoding.UTF8.GetString(bytes);
	}
}
