using System;
using System.Security.Cryptography;
using System.Text;

public class PKCSKeyGenerator
{
	private byte[] key = new byte[8];

	private byte[] iv = new byte[8];

	private DESCryptoServiceProvider des = new DESCryptoServiceProvider();

	public byte[] Key => key;

	public byte[] IV => iv;

	public ICryptoTransform Encryptor => des.CreateEncryptor(key, iv);

	public ICryptoTransform Decryptor => des.CreateDecryptor(key, iv);

	public PKCSKeyGenerator()
	{
	}

	public PKCSKeyGenerator(string keystring, byte[] salt, int iterationsMd5, int segments)
	{
		Generate(keystring, salt, iterationsMd5, segments);
	}

	public ICryptoTransform Generate(string keystring, byte[] salt, int iterationsMd5, int segments)
	{
		int num = 16;
		byte[] array = new byte[num * segments];
		byte[] bytes = Encoding.UTF8.GetBytes(keystring);
		byte[] array2 = new byte[bytes.Length + salt.Length];
		Array.Copy(bytes, array2, bytes.Length);
		Array.Copy(salt, 0, array2, bytes.Length, salt.Length);
		MD5 mD = new MD5CryptoServiceProvider();
		byte[] array3 = null;
		byte[] array4 = new byte[num + array2.Length];
		for (int i = 0; i < segments; i++)
		{
			if (i == 0)
			{
				array3 = array2;
			}
			else
			{
				Array.Copy(array3, array4, array3.Length);
				Array.Copy(array2, 0, array4, array3.Length, array2.Length);
				array3 = array4;
			}
			for (int j = 0; j < iterationsMd5; j++)
			{
				array3 = mD.ComputeHash(array3);
			}
			Array.Copy(array3, 0, array, i * num, array3.Length);
		}
		Array.Copy(array, 0, key, 0, 8);
		Array.Copy(array, 8, iv, 0, 8);
		return Encryptor;
	}
}
