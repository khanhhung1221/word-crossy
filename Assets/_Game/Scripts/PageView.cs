using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PageView : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
{
	private ScrollRect rect;

	private float targethorizontal;

	private bool isDrag;

	private List<float> posList = new List<float>();

	private int currentPageIndex = -1;

	public Action<int> OnPageChanged;

	public RectTransform content;

	private bool stopMove = true;

	public float smooting = 4f;

	public float sensitivity;

	private float startTime;

	private float startDragHorizontal;

	public Transform toggleList;

	private void Start()
	{
		rect = base.transform.GetComponent<ScrollRect>();
		RectTransform component = GetComponent<RectTransform>();
		float x = (float)content.transform.childCount * component.rect.width;
		content.sizeDelta = new Vector2(x, component.rect.height);
		float num = content.rect.width - component.rect.width;
		for (int i = 0; i < rect.content.transform.childCount; i++)
		{
			posList.Add(component.rect.width * (float)i / num);
		}
	}

	private void Update()
	{
		if (!isDrag && !stopMove)
		{
			startTime += Time.deltaTime;
			float num = startTime * smooting;
			rect.horizontalNormalizedPosition = Mathf.Lerp(rect.horizontalNormalizedPosition, targethorizontal, num);
			if (num >= 1f)
			{
				stopMove = true;
			}
		}
	}

	public void pageTo(int index)
	{
		if (index >= 0 && index < posList.Count)
		{
			rect.horizontalNormalizedPosition = posList[index];
			SetPageIndex(index);
		}
	}

	private void SetPageIndex(int index)
	{
		if (currentPageIndex != index)
		{
			currentPageIndex = index;
			if (OnPageChanged != null)
			{
				OnPageChanged(index);
			}
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		isDrag = true;
		startDragHorizontal = rect.horizontalNormalizedPosition;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		float horizontalNormalizedPosition = rect.horizontalNormalizedPosition;
		horizontalNormalizedPosition += (horizontalNormalizedPosition - startDragHorizontal) * sensitivity;
		horizontalNormalizedPosition = ((!(horizontalNormalizedPosition < 1f)) ? 1f : horizontalNormalizedPosition);
		horizontalNormalizedPosition = ((!(horizontalNormalizedPosition > 0f)) ? 0f : horizontalNormalizedPosition);
		int num = 0;
		float num2 = Mathf.Abs(posList[num] - horizontalNormalizedPosition);
		for (int i = 1; i < posList.Count; i++)
		{
			float num3 = Mathf.Abs(posList[i] - horizontalNormalizedPosition);
			if (num3 < num2)
			{
				num = i;
				num2 = num3;
			}
		}
		if (num > currentPageIndex)
		{
			num = currentPageIndex + 1;
		}
		else if (num < currentPageIndex)
		{
			num = currentPageIndex - 1;
		}
		SetPageIndex(num);
		GetIndex(num);
		targethorizontal = posList[num];
		isDrag = false;
		startTime = 0f;
		stopMove = false;
	}

	public void GetIndex(int index)
	{
		HelpUIScript.Instance.SetCurrentIndex(index);
		HelpUIScript.Instance.ChangeSpotsByIndex();
	}
}
