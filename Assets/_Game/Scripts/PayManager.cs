using LitJson;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

public class PayManager : MonoBehaviour
{
    public Product m_CurrentProduct;

    public int m_CurrentOfferID;

	private static PayManager instance;

    private CrossPlatformValidator validator;

    private IAPListener iap_listener;

    public static event UnityAction<Product> PaymentVerifyEvnet;

	public static event UnityAction PaymentVerifyFailEvnet;

	public static PayManager GetInstance()
	{
		return instance;
	}

	private void Awake()
	{
		instance = this;
        //validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), UnityChannelTangle.Data(), Application.identifier);
    }


	public void RestoreIap()
	{
        if (iap_listener == null)
        {
            iap_listener = base.gameObject.GetComponent<IAPListener>();
        }
        if (iap_listener != null)
        {
            //iap_listener.RestorePurchases();
        }
    }

	public void PayCompleted(Product product)
	{
        CodelessIAPStoreListener.Instance.StoreController.ConfirmPendingPurchase(product);
        WordGlobal.DisableBackKey = false;
		bool flag = true;
		try
		{
			IPurchaseReceipt[] array = validator.Validate(product.receipt);
			Debug.LogError("Receipt is valid. Contents:");
			IPurchaseReceipt[] array2 = array;
			foreach (IPurchaseReceipt purchaseReceipt in array2)
			{
				Debug.LogError("productReceipt.productI:" + purchaseReceipt.productID);
				Debug.LogError("productReceipt.purchaseDate:" + purchaseReceipt.purchaseDate);
				Debug.LogError("productReceipt.transactionID:" + purchaseReceipt.transactionID);
				GooglePlayReceipt googlePlayReceipt = purchaseReceipt as GooglePlayReceipt;
				if (googlePlayReceipt != null)
				{
					Debug.LogError("productReceipt.purchaseState:" + googlePlayReceipt.purchaseState);
					Debug.LogError("productReceipt.purchaseToken:" + googlePlayReceipt.purchaseToken);
				}
				UnityChannelReceipt unityChannelReceipt = purchaseReceipt as UnityChannelReceipt;
				if (unityChannelReceipt != null)
				{
					Debug.Log("unityChannel.productID:" + unityChannelReceipt.productID);
					Debug.Log("unityChannel.purchaseDat:" + unityChannelReceipt.purchaseDate);
					Debug.Log("unityChannel.transactionID:" + unityChannelReceipt.transactionID);
				}
				AppleInAppPurchaseReceipt appleInAppPurchaseReceipt = purchaseReceipt as AppleInAppPurchaseReceipt;
				if (appleInAppPurchaseReceipt != null)
				{
					Debug.Log("apple.originalTransactionIdentifier:" + appleInAppPurchaseReceipt.originalTransactionIdentifier);
					Debug.Log("apple.subscriptionExpirationDate:" + appleInAppPurchaseReceipt.subscriptionExpirationDate);
					Debug.Log("apple.cancellationDate:" + appleInAppPurchaseReceipt.cancellationDate);
					Debug.Log("apple.quantity:" + appleInAppPurchaseReceipt.quantity);
				}
			}
            //CodelessIAPStoreListener.Instance.SubscriptionInfo();
        }
		catch (IAPSecurityException arg)
		{
			Debug.Log("Invalid receipt, not unlocking content. " + arg);
			flag = false;
		}
		string empty = string.Empty;
		if (!flag)
		{
			m_CurrentOfferID = 0;
			if (PayManager.PaymentVerifyFailEvnet != null)
			{
				PayManager.PaymentVerifyFailEvnet();
			}
			return;
		}
		m_CurrentProduct = product;
		if ((Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer) && m_CurrentOfferID == 0)
		{
			Debug.Log("�\u05b8����� success" + m_CurrentProduct.definition.id);
            //IAPListener.isShowRestorePurchaseTip = 1299;
            OfferItem offerItem = PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.Productid == m_CurrentProduct.definition.id);
			if (offerItem != null)
			{
				m_CurrentOfferID = offerItem.ID;
			}
		}
		OfferItem offerItem2 = PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.ID == m_CurrentOfferID);
		Debug.Log("PayComplete success" + m_CurrentProduct.definition.id + " m_CurrentOfferID:" + m_CurrentOfferID);
		if (m_CurrentOfferID == 0)
		{
			return;
		}
		if (offerItem2.Type == 2)
		{
			PlayerInfo.Instance.SaveBuyOfferItem(m_CurrentOfferID);
		}
		PlayerInfo.Instance.BuyInOffers.Add(m_CurrentOfferID);
		if (offerItem2.Coins > 0)
		{
			PlayerInfo.Instance.AddGold(offerItem2.Coins, 15);
			if (offerItem2.Type == 3)
			{
				ResourceLoadManager.GetInstance().ShowTips(string.Format(WordGlobal.ReadText(1056), offerItem2.Coins));
			}
			WordFaceBook.Instance.UpdateUserdataMill();
		}
		if (offerItem2.AdsOff && PlayerInfo.Instance.AdRemove == 0)
		{
			PlayerInfo.Instance.AdRemove = 1;
			WordFaceBook.Instance.UpdateUserdataMill();
			if (offerItem2.Type == 3)
			{
				ResourceLoadManager.GetInstance().ShowTips(1058);
			}
		}
		if (offerItem2.Type != 3 && offerItem2.Type != 4 && offerItem2.Type != 6)
		{
			if (offerItem2.Type == 5)
			{
				ResourceLoadManager.GetInstance().ShowTips(string.Format(WordGlobal.ReadText(1316), WordGlobal.ReadText(offerItem2.Title)));
			}
			else
			{
				ResourceLoadManager.GetInstance().ShowTips(string.Format(WordGlobal.ReadText(1199), WordGlobal.ReadText(offerItem2.Title)));
			}
			PlayerInfo.Instance.TipsCount += offerItem2.BulbCount;
			PlayerInfo.Instance.FirstTipsCount += offerItem2.LightingCount;
			PlayerInfo.Instance.ClickTipsCount += offerItem2.FigerCount;
		}
		if (offerItem2.Type == 1)
		{
			PlayerInfo.Instance.BuyOfferCount++;
			PlayerInfo.Instance.LasyBuyOfferTime = DateTime.Now.Ticks.ToString();
			PlayerInfo.Instance.CurrentOfferItem = null;
			PlayerInfo.Instance.NoPayPlayLevel = 0;
			if (offerItem2.ID == 1)
			{
				PlayerInfo.Instance.NewPlayerOffer = 1;
			}
		}
		PlayerInfo.Instance.LastOfferPayValue = PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.Productid == m_CurrentProduct.definition.id).Price;
		PlayerInfo.Instance.LastPayUUID = PlayerInfo.Instance.UUID;
		if (offerItem2.Type == 4)
		{
			WordGlobal.isUpdateLevelMapPigBtn = true;
			PiggyBankUI.BuyComplete = true;
			PlayerInfo.Instance.GetPigGoldSaveData().lastBuyProductID = offerItem2.Productid;
			PlayerInfo.Instance.GetPigGoldSaveData().InitData();
			PlayerInfo.Instance.GetPigGoldSaveData().AddBuyTimes();
		}
		if (offerItem2.Type == 5)
		{
			WordGlobal.isUpdateLevelMapLeftBtn = true;
			VipUIScript.isHide = true;
			PlayerInfo.Instance.GetPlayerOtherInfoSaveData().SetBoughtVip();
		}
		if (PayManager.PaymentVerifyEvnet != null)
		{
			PayManager.PaymentVerifyEvnet(m_CurrentProduct);
		}
		if (PlayerInfo.Instance.UUID != "KTGRKHSWG" && (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer))
		{
			//AdjustEvent adjustEvent = new AdjustEvent(AdJustDataManager.instance.GetToken("In_App_Purchase"));
			//adjustEvent.setRevenue((double)m_CurrentProduct.metadata.localizedPrice, m_CurrentProduct.metadata.isoCurrencyCode);
			//Adjust.trackEvent(adjustEvent);
			//FirebaseAnalytics.LogEvent(parameters: new Parameter[3]
			//{
			//	new Parameter(FirebaseAnalytics.ParameterCurrency, m_CurrentProduct.metadata.isoCurrencyCode),
			//	new Parameter(FirebaseAnalytics.ParameterValue, (double)m_CurrentProduct.metadata.localizedPrice),
			//	new Parameter(FirebaseAnalytics.ParameterTransactionId, m_CurrentProduct.transactionID)
			//}, name: FirebaseAnalytics.EventEcommercePurchase);
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["mygame_packagename"] = m_CurrentProduct.definition.storeSpecificId;
			//FB.LogPurchase((float)m_CurrentProduct.metadata.localizedPrice, m_CurrentProduct.metadata.isoCurrencyCode, dictionary);
		}
		HuoDongSunUI.isResetGold = true;
		HuoDongGardonUI.isResetGold = true;
		JsonData jsonData = JsonMapper.ToObject(product.receipt.ToString());
		string a = jsonData["Store"].ToString();
		JsonData jsonData2 = new JsonData();
		JsonData jsonData3 = new JsonData();
		empty = jsonData["TransactionID"].ToString();
		string json = jsonData["Payload"].ToString();
		JsonData jsonData4 = JsonMapper.ToObject(json);
		string data = string.Empty;
		string data2 = string.Empty;
		jsonData3["uuid"] = PlayerInfo.Instance.UUID;
		jsonData3["offerid"] = m_CurrentOfferID.ToString();
		OfferItem offerItem3 = PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.ID.Equals(m_CurrentOfferID));
		if (offerItem3 != null)
		{
			jsonData3["price"] = offerItem3.Price.ToString();
		}
		else
		{
			jsonData3["price"] = product.metadata.localizedPrice.ToString();
		}
		jsonData3["CompleteDailyPuzzle"] = PlayerInfo.Instance.GetDailyPuzzleItem().IsFinished;
		jsonData3["isinMap"] = ((WordGlobal.UI_CurrentUI == WordGlobal.UI_LevelMap) ? 1 : 0);
		jsonData3["type"] = offerItem2.Type;
		jsonData3["gold"] = PlayerInfo.Instance.Gold;
		jsonData3["bulb"] = PlayerInfo.Instance.TipsCount;
		jsonData3["lighting"] = PlayerInfo.Instance.FirstTipsCount;
		jsonData3["finger"] = PlayerInfo.Instance.ClickTipsCount;
		if (WordGlobal.UI_CurrentUI == WordGlobal.UI_MAIN)
		{
			jsonData3["PayLevel"] = ((UIManager.CurMainUIType != UIManager.MainUIType.Type_Common) ? PlayerInfo.Instance.GetDailyDayId() : PlayerInfo.Instance.CurrentLevel);
		}
		else
		{
			jsonData3["PayLevel"] = PlayerInfo.Instance.MaxPlayLevel;
		}
		if (a == "GooglePlay")
		{
			data = jsonData4["json"].ToString();
			data2 = jsonData4["signature"].ToString();
			jsonData3["platform"] = 0;
		}
		else if (a == "fake")
		{
			jsonData3["platform"] = 3;
		}
		else if (a == "Apple")
		{
			jsonData3["platform"] = 1;
		}
		jsonData2["data"] = data;
		jsonData2["signature"] = data2;
		jsonData3["receipt"] = jsonData2.ToJson();
		Dictionary<string, NetMill.CommandCallback> dictionary2 = new Dictionary<string, NetMill.CommandCallback>();
		dictionary2["paymentverify"] = PayMent;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "paymentverify", jsonData3, dictionary2);
		m_CurrentOfferID = 0;
	}

	private bool PayMent(JsonData data)
	{
		string text = data["crc"].ToString();
		string str = data["errcode"].ToString();
		string str2 = data["orderId"].ToString();
		string s = str + "_lettuce-words_" + str2;
		MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
		byte[] array = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(s));
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < array.Length; i++)
		{
			stringBuilder.Append(array[i].ToString("x2"));
		}
		return true;
	}

	public void PurchaseFaild(Product product, PurchaseFailureReason reason)
	{
		WordGlobal.DisableBackKey = false;
        CodelessIAPStoreListener.Instance.StoreController.ConfirmPendingPurchase(product);
        WordGlobal.DebugLogError("PurchaseFaild:" + reason.ToString());
		if (PayManager.PaymentVerifyFailEvnet != null)
		{
			m_CurrentOfferID = 0;
			PayManager.PaymentVerifyFailEvnet();
		}
	}

	public string GetProductPrice(string productID)
	{
		return string.Empty;
	}

	private void OnDestroy()
	{
		if (PlayerInfo.Instance.CurrentLevel > 10)
		{
			PlayerInfo.Instance.FirstLogin = 2;
		}
	}

	private List<OfferItem> GetOffersList()
	{
		List<OfferItem> list = new List<OfferItem>();
		int i;
		for (i = 0; i < PlayerInfo.Instance.OfferList.Count; i++)
		{
			if (list.FindIndex((OfferItem x) => x.Productid == PlayerInfo.Instance.OfferList[i].Productid) < 0)
			{
				list.Add(PlayerInfo.Instance.OfferList[i]);
			}
		}
		return list;
	}

	public void InitProducts()
	{
		StandardPurchasingModule standardPurchasingModule = StandardPurchasingModule.Instance();
		standardPurchasingModule.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
		ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(standardPurchasingModule);
		List<OfferItem> offersList = GetOffersList();
		List<ProductDefinition> list = new List<ProductDefinition>();
		for (int i = 0; i < offersList.Count; i++)
		{
			string productid = offersList[i].Productid;
			List<PayoutDefinition> list2 = new List<PayoutDefinition>();
			if (offersList[i].Coins > 0)
			{
				list2.Add(new PayoutDefinition(PayoutType.Resource, "gold", offersList[i].Coins));
			}
			if (offersList[i].AdsOff)
			{
				list2.Add(new PayoutDefinition(PayoutType.Resource, "removeads", 1.0));
			}
			list.Add(new ProductDefinition(productid, productid, ProductType.Consumable, enabled: true, list2));
			configurationBuilder.AddProduct(productid, ProductType.Consumable, new IDs
			{
				{
					productid,
					"GooglePlay"
				},
				{
					productid,
					"AppleAppStore"
				}
			}, list2);
		}
        UnityPurchasing.Initialize(CodelessIAPStoreListener.Instance, configurationBuilder);
    }
}
