using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PigGoldSaveData
{
	public string productID;

	public int GoldNum;

	public int FullShowTime;

	public int FullDayTime;

	public int BuySuccessTimes;

	public int ShowNextNum;

	public int ThisTimeFullTime;

	public string lastBuyProductID;

	public int openLevel = 14;

	public long Discount_EndTimeSpame;

	public bool isNeedCheckDiscount;

	public string discountProductIDKey = "piggybanksale";

	private int DiscountMaxTime = 172800;

	private float a = 3.5f;

	private float b = 144f;

	private int currentLevelScore;

	private int currentLevelWordNum;

	public bool isFullNextDay;

	private int[] percent01 = new int[3]
	{
		40,
		30,
		30
	};

	private int[] percent02 = new int[3]
	{
		60,
		20,
		20
	};

	private int[] percent03 = new int[3]
	{
		20,
		60,
		20
	};

	private string[] productIDKeys = new string[3]
	{
		"piggybank1",
		"piggybank2",
		"piggybank3"
	};

	private OfferItem oItem;

	public float A => a;

	public float B => b;

	public int CurrentLevelScore
	{
		get
		{
			return currentLevelScore;
		}
		set
		{
			currentLevelScore = value;
		}
	}

	public int CurrentLevelWordNum
	{
		get
		{
			return currentLevelWordNum;
		}
		set
		{
			currentLevelWordNum = value;
		}
	}

	public void OpenPig(bool isNeedAddOne = false)
	{
		if (isNeedAddOne)
		{
			if (PlayerInfo.Instance.MaxPlayLevel + 1 > openLevel && string.IsNullOrEmpty(productID))
			{
				InitData();
			}
		}
		else if (PlayerInfo.Instance.MaxPlayLevel > openLevel && string.IsNullOrEmpty(productID))
		{
			InitData();
		}
		if (FullDayTime != 0)
		{
			int dateNum = GetDateNum(DateTime.Now);
			if (dateNum > FullDayTime)
			{
				FullShowTime = 1;
				ShowNextNum = 1;
				isFullNextDay = true;
				FullDayTime = GetDateNum(DateTime.Now);
				PlayerInfo.Instance.SavePigGoldSaveData();
			}
		}
		if (ThisTimeFullTime != 0)
		{
			int dateNum2 = GetDateNum(DateTime.Now);
			int num = 0;
			if (dateNum2 - ThisTimeFullTime >= 4)
			{
				num = 2;
			}
			else if (dateNum2 - ThisTimeFullTime >= 2)
			{
				num = 1;
			}
			if (num > 0)
			{
				CutOffer(num);
			}
		}
	}

	public void CutOffer(int cut_num)
	{
		string str = productID;
		if (productID.Contains(productIDKeys[0]))
		{
			return;
		}
		if (productID.Contains(productIDKeys[1]))
		{
			productID = productID.Replace(productIDKeys[1], productIDKeys[0]);
		}
		else if (productID.Contains(productIDKeys[2]))
		{
			switch (cut_num)
			{
			case 1:
				productID = productID.Replace(productIDKeys[2], productIDKeys[1]);
				break;
			case 2:
				productID = productID.Replace(productIDKeys[2], productIDKeys[0]);
				break;
			}
		}
		Debug.Log("Cutoffer from:" + str + " to " + productID);
		OfferItem offerItem = GetOfferItem(isChange: true);
		GoldNum = offerItem.Coins;
		ThisTimeFullTime = GetDateNum(DateTime.Now);
		PlayerInfo.Instance.SavePigGoldSaveData();
	}

	public void InitData()
	{
		InitProductID();
		GoldNum = 0;
		FullDayTime = 0;
		FullShowTime = 0;
		ShowNextNum = 0;
		ThisTimeFullTime = 0;
		Discount_EndTimeSpame = 0L;
		isFullNextDay = false;
		isNeedCheckDiscount = false;
		GetOfferItem(isChange: true);
		PlayerInfo.Instance.SavePigGoldSaveData();
	}

	public void InitProductID()
	{
		productID = string.Empty;
		if (PlayerInfo.Instance.MaxPlayLevel < openLevel)
		{
			return;
		}
		List<OfferItem> list = new List<OfferItem>();
		for (int i = 0; i < PlayerInfo.Instance.OfferList.Count; i++)
		{
			OfferItem offerItem = PlayerInfo.Instance.OfferList[i];
			if (offerItem.Type == 4)
			{
				Debug.Log("piggy productID:" + offerItem.Productid);
				list.Add(offerItem);
			}
		}
		if (!string.IsNullOrEmpty(lastBuyProductID))
		{
			int num = 0;
			if (lastBuyProductID.Contains(productIDKeys[0]))
			{
				num = GetRandomIndex(1);
			}
			else if (lastBuyProductID.Contains(productIDKeys[1]))
			{
				num = GetRandomIndex(2);
			}
			else if (lastBuyProductID.Contains(productIDKeys[2]))
			{
				num = GetRandomIndex(3);
			}
			if (num >= list.Count)
			{
				num = 0;
			}
			productID = list[num].Productid;
			return;
		}
		for (int j = 0; j < list.Count; j++)
		{
			if (list[j].Productid.Contains("piggybank1"))
			{
				productID = list[j].Productid;
			}
		}
	}

	public int GetRandomIndex(int type)
	{
		int[] array = null;
		switch (type)
		{
		case 1:
			array = percent01;
			break;
		case 2:
			array = percent02;
			break;
		default:
			array = percent03;
			break;
		}
		int max = 100;
		int num = UnityEngine.Random.Range(0, max);
		int num2 = 0;
		for (int i = 0; i < array.Length; i++)
		{
			num2 += array[i];
			if (num < num2)
			{
				Debug.Log("存钱罐随机几率 random_num:" + num + " 随机类型 type:" + type + " 获得第" + (i + 1) + "梯度");
				return i;
			}
		}
		return 0;
	}

	public void AddBuyTimes()
	{
		BuySuccessTimes++;
		PlayerInfo.Instance.SavePigGoldSaveData();
	}

	public bool isOpen()
	{
		if (WordGlobal.isNeedPigGold)
		{
			if (!string.IsNullOrEmpty(productID) && PlayerInfo.Instance.MaxPlayLevel >= openLevel)
			{
				return true;
			}
			return false;
		}
		return false;
	}

	public bool isFull()
	{
		if (GetOfferItem() != null && GoldNum >= GetOfferItem().Coins)
		{
			return true;
		}
		return false;
	}

	public bool ShowUI(int score, int word_num)
	{
		if (isFull() && score > 0)
		{
			if (ShowNextNum > 0)
			{
				ShowNextNum--;
				PlayerInfo.Instance.SavePigGoldSaveData();
				return false;
			}
			ShowNextNum = 3;
			AddFullShowTime();
			bool flag = false;
			if (isFullNextDay || isNeedCheckDiscount)
			{
				flag = true;
			}
			else if (FullShowTime >= 3)
			{
				flag = true;
			}
			if (flag && Discount_EndTimeSpame == 0)
			{
				SetDiscount_EndTimeSpame();
			}
		}
		if (score > 0)
		{
			CurrentLevelScore = score;
			CurrentLevelWordNum = word_num;
		}
		else
		{
			CurrentLevelScore = 0;
			CurrentLevelWordNum = word_num;
		}
		UIManager.Instance.LoadMainUI("UI/PiggyBank/PiggyBank", isPop: true, UIManager.MainUIType.Type_Common, 99);
		return true;
	}

	public void SetDiscount_EndTimeSpame()
	{
		if (PlayerInfo.Instance.LastOfferPayValue == 0f)
		{
			DateTime time = DateTime.Now.AddSeconds(DiscountMaxTime);
			Discount_EndTimeSpame = WordGlobal.ConvertDateTimeToInt(time) / 1000;
			GetOfferItem(isChange: true);
			PlayerInfo.Instance.SavePigGoldSaveData();
		}
	}

	public int GetDiscountRestSecond()
	{
		return (int)(Discount_EndTimeSpame - WordGlobal.ConvertDateTimeToInt(DateTime.Now) / 1000);
	}

	public string GetDiscountProductID()
	{
		if (!string.IsNullOrEmpty(productID))
		{
			return productID.Substring(0, productID.Length - 10) + discountProductIDKey;
		}
		return productID;
	}

	public void ClearDiscount()
	{
		if (Discount_EndTimeSpame > 0)
		{
			DateTime dateTime = WordGlobal.ConvertTimeStampToDateTime(Discount_EndTimeSpame);
			if (DateTime.Now.Month > dateTime.Month || (DateTime.Now.Month == dateTime.Month && DateTime.Now.Day > dateTime.Day))
			{
				isNeedCheckDiscount = true;
			}
			isFullNextDay = false;
			Discount_EndTimeSpame = 0L;
			GetOfferItem(isChange: true);
			PlayerInfo.Instance.SavePigGoldSaveData();
		}
	}

	public int GetDateNum(DateTime dt)
	{
		return WordGlobal.GetDateNum(dt);
	}

	public void AddFullShowTime()
	{
		if (FullShowTime == 0)
		{
			ShowNextNum = 3;
			FullDayTime = GetDateNum(DateTime.Now);
		}
		FullShowTime++;
		PlayerInfo.Instance.SavePigGoldSaveData();
	}

	public bool isShowUI()
	{
		if (FullShowTime >= 3)
		{
			return false;
		}
		return true;
	}

	public int GetCountGoldNum()
	{
		float num = (float)CurrentLevelScore / (A * (float)CurrentLevelWordNum) * B;
		if (WordGlobal.PiggyBankTempAddGoldNum > 0)
		{
			num = WordGlobal.PiggyBankTempAddGoldNum;
		}
		return (int)num;
	}

	public void AddGoldNum(int add_num)
	{
		GoldNum += add_num;
		if (GoldNum >= GetOfferItem().Coins)
		{
			GoldNum = GetOfferItem().Coins;
			if (ThisTimeFullTime == 0)
			{
				ThisTimeFullTime = GetDateNum(DateTime.Now);
			}
		}
		PlayerInfo.Instance.SavePigGoldSaveData();
	}

	public OfferItem GetOfferItem(bool isChange = false)
	{
		if (oItem == null || isChange)
		{
			if (GetDiscountRestSecond() > 0)
			{
				string disPid = GetDiscountProductID();
				oItem = PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.Productid.Equals(disPid));
			}
			else
			{
				oItem = PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.Productid.Equals(productID));
			}
		}
		return oItem;
	}

	public OfferItem GetMinOfferItem()
	{
		return PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.Productid.Equals(productID));
	}
}
