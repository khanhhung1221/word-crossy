using UnityEngine;

public class PiggyBankGold : MonoBehaviour
{
	public GameObject flyIn;

	public GameObject flyOut;

	public int type;

	public float delayTime;

	private float soundDelayTime;

	private void Start()
	{
	}

	public void SetShow(int tt, float delay)
	{
		type = tt;
		delayTime = delay;
		flyIn.SetActive(value: false);
		flyOut.SetActive(value: false);
	}

	private void Update()
	{
		if (delayTime > 0f)
		{
			delayTime -= Time.deltaTime;
			if (delayTime <= 0f)
			{
				if (type == 0)
				{
					soundDelayTime = 0.8f;
					flyIn.SetActive(value: true);
				}
				else
				{
					soundDelayTime = 0.1f;
					flyOut.SetActive(value: true);
				}
			}
		}
		if (!(soundDelayTime > 0f))
		{
			return;
		}
		soundDelayTime -= Time.deltaTime;
		if (soundDelayTime <= 0f)
		{
			if (type == 0)
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_PiggyBankList[0]);
			}
			else
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_PiggyBankList[1]);
			}
		}
	}
}
