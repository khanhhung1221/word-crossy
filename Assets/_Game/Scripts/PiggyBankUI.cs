using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class PiggyBankUI : MonoBehaviour
{
	public static bool isNeedResult;

	public static bool BuyComplete;

	public static bool isCloseClick;

	public static bool isLevelMapOpen;

	public GameObject jinbi_up;

	public GameObject jinbi_down;

	public Sprite[] title02_sps;

	public Image title02_img;

	public GameObject btnClose_go;

	public GameObject btnOpen_go;

	public GameObject btnNext_go;

	public GameObject btnOk_go;

	public Text btnOpen_txt;

	public GoldScript gold_script;

	public RectTransform top_tran;

	public RectTransform gold_flyPos;

	public GameObject m_ObjMask;

	public SkeletonGraphic Pig_img;

	public Transform piggy_tran;

	public Text des_text;

	public Text des01_text;

	public Text jindu_txt;

	public Image jindu_img;

	public GameObject Discount_go;

	public Text Discount_txt;

	public Text Discount_timeTxt;

	private PigGoldSaveData pgsd;

	private int gold_maxNum;

	private int curGoldNum;

	private int endGoldNum;

	private int needAddGoldNum;

	private float curFillAmount;

	private int addSpeed;

	private int isNeedGoldOutNum;

	private int isNeedGoldInNum;

	private float levelScore;

	private float AutoCloseTime;

	private bool HideNeedShowBanner;

	private float playBrokeTime;

	private float discountWaitTime;

	private bool isCanCloseUI = true;

	private void Start()
	{
		WordGlobal.ResetTopPos(top_tran);
		ResourceLoadManager.GetInstance().HideNetWaiting();
		isCloseClick = false;
		pgsd = PlayerInfo.Instance.GetPigGoldSaveData();
		gold_maxNum = pgsd.GetOfferItem().Coins;
		//Product product = CodelessIAPStoreListener.Instance.GetProduct(pgsd.GetOfferItem().Productid);
		string empty = string.Empty;
		//empty = ((product == null) ? pgsd.GetOfferItem().Price.ToString() : WordGlobal.GetPrice(product.metadata.localizedPriceString));
		if (pgsd.GetDiscountRestSecond() > 0)
		{
			discountWaitTime = 1f;
			btnOpen_txt.text = TextLibraryDataManagerCsv.instance.GetText(1282) + "  " + empty;
		}
		else
		{
			pgsd.ClearDiscount();
			btnOpen_txt.text = TextLibraryDataManagerCsv.instance.GetText(1284) + "  " + empty;
		}
		curGoldNum = pgsd.GoldNum;
		isNeedGoldInNum = 5;
		isNeedGoldOutNum = 0;
		BuyComplete = false;
		btnOpen_go.SetActive(value: false);
		Discount_go.SetActive(value: false);
		btnNext_go.SetActive(value: false);
		jinbi_up.SetActive(value: false);
		jinbi_down.SetActive(value: false);
		levelScore = pgsd.CurrentLevelScore;
		AutoCloseTime = 0f;
		title02_img.sprite = title02_sps[0];
		title02_img.rectTransform.sizeDelta = new Vector2(405f, 69f);
		if (pgsd.isFull())
		{
			title02_img.sprite = title02_sps[1];
			title02_img.rectTransform.sizeDelta = new Vector2(336f, 65f);
			des_text.text = TextLibraryDataManagerCsv.instance.GetText(1226);
			des01_text.text = TextLibraryDataManagerCsv.instance.GetText(1227);
			FullShow();
			isNeedGoldOutNum = 5;
			isNeedGoldInNum = 0;
			Pig_img.AnimationState.SetAnimation(0, "end", loop: true);
		}
		else
		{
			Pig_img.AnimationState.SetAnimation(0, "start", loop: true);
			des_text.text = TextLibraryDataManagerCsv.instance.GetText(1224);
			des01_text.text = string.Format(TextLibraryDataManagerCsv.instance.GetText(1225), gold_maxNum);
			if (pgsd.CurrentLevelScore == 0)
			{
				needAddGoldNum = 0;
			}
			else
			{
				needAddGoldNum = pgsd.GetCountGoldNum();
			}
			if (curGoldNum + needAddGoldNum >= gold_maxNum)
			{
				needAddGoldNum = gold_maxNum - curGoldNum;
				isNeedGoldOutNum = 3;
				isNeedGoldInNum = 2;
			}
			else
			{
				isNeedGoldInNum = 5;
				isNeedGoldOutNum = 0;
			}
			addSpeed = needAddGoldNum;
			pgsd.AddGoldNum(needAddGoldNum);
			if (!pgsd.isFull() && pgsd.CurrentLevelScore != 0)
			{
				AutoCloseTime = 1.5f;
			}
			if (pgsd.isFull() && needAddGoldNum > 0)
			{
				pgsd.AddFullShowTime();
			}
			btnOk_go.SetActive(value: false);
			if (isLevelMapOpen)
			{
				btnOk_go.SetActive(value: true);
				isLevelMapOpen = false;
			}
		}
		endGoldNum = curGoldNum + needAddGoldNum;
		if (gold_script == null)
		{
			gold_script = WordGlobal.AddGoldUI(top_tran);
			gold_script.m_ShowBanner = true;
			gold_script._isAddGold = true;
			gold_script.gameObject.SetActive(value: false);
		}
		if (isNeedResult)
		{
			for (int i = 0; i < 5; i++)
			{
				GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI("UI/PiggyBank/PiggyBankGold", piggy_tran);
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.transform.localScale = Vector3.one;
				PiggyBankGold component = gameObject.GetComponent<PiggyBankGold>();
				if (i < isNeedGoldInNum)
				{
					component.SetShow(0, (float)i * 0.2f);
				}
				else
				{
					component.SetShow(1, (float)i * 0.2f);
				}
			}
		}
		//btnOpen_go.GetComponent<IAPButton>().productId = pgsd.GetOfferItem().Productid;
		//btnOpen_go.GetComponent<IAPButton>().enabled = true;
		SetJinDu();
		if (AdsManager.Instance.isShowBanner)
		{
			HideNeedShowBanner = true;
			AdsManager.Instance.HideBannerAD();
		}
	}

	public void FullShow()
	{
		Pig_img.AnimationState.SetAnimation(0, "end", loop: true);
		jinbi_up.SetActive(value: true);
		jinbi_down.SetActive(value: true);
		btnOpen_go.SetActive(value: true);
		if (pgsd.Discount_EndTimeSpame > 0)
		{
			Discount_go.SetActive(value: true);
			//Product product = CodelessIAPStoreListener.Instance.GetProduct(pgsd.GetMinOfferItem().Productid);
			string empty = string.Empty;
			//empty = ((product == null) ? pgsd.GetMinOfferItem().Price.ToString() : WordGlobal.GetPrice(product.metadata.localizedPriceString));
			Discount_txt.text = TextLibraryDataManagerCsv.instance.GetText(1283) + ": " + empty;
		}
		des_text.text = TextLibraryDataManagerCsv.instance.GetText(1226);
		des01_text.text = TextLibraryDataManagerCsv.instance.GetText(1227);
	}

	public void ShowNextBtn()
	{
		btnNext_go.SetActive(value: true);
		btnOpen_go.SetActive(value: false);
		Discount_go.SetActive(value: false);
		btnClose_go.SetActive(value: false);
	}

	public void SetJinDu()
	{
		if (curGoldNum > gold_maxNum)
		{
			curGoldNum = gold_maxNum;
		}
		jindu_txt.text = curGoldNum + "/" + gold_maxNum;
		curFillAmount = (float)curGoldNum / (float)gold_maxNum;
		jindu_img.fillAmount = curFillAmount;
	}

	private void Update()
	{
		if (curGoldNum < endGoldNum)
		{
			int num = (int)((float)addSpeed * Time.deltaTime);
			if (num <= 1)
			{
				num = 1;
			}
			curGoldNum += num;
			if (curGoldNum >= endGoldNum)
			{
				curGoldNum = endGoldNum;
			}
			if (curGoldNum >= gold_maxNum)
			{
				FullShow();
			}
			SetJinDu();
		}
		if (BuyComplete)
		{
			OpenAnim(0f);
			btnOpen_go.SetActive(value: false);
			Discount_go.SetActive(value: false);
			playBrokeTime = 0.7f;
			Debug.Log("播放砸碎动画");
			BuyComplete = false;
		}
		if (playBrokeTime > 0f)
		{
			playBrokeTime -= Time.deltaTime;
			if (playBrokeTime <= 0f)
			{
				AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_PiggyBankList[2]);
			}
		}
		if (levelScore > 0f)
		{
			levelScore -= Time.deltaTime * (float)pgsd.CurrentLevelScore;
			if (levelScore < 1f)
			{
				levelScore = 0f;
			}
			if (MainUIScript.Instance != null)
			{
				RectTransform tFScoreUI = MainUIScript.Instance.m_TFScoreUI;
				Vector2 anchoredPosition = MainUIScript.Instance.m_TFScoreUI.anchoredPosition;
				tFScoreUI.anchoredPosition = new Vector2(-39f, anchoredPosition.y);
				MainUIScript.Instance.m_TFScoreUI.gameObject.SetActive(value: true);
				MainUIScript.Instance.SetTxtStarCount((int)levelScore);
				if (levelScore <= 0f)
				{
					MainUIScript.Instance.ShowScroeUI(isShow: false);
				}
			}
		}
		if (levelScore == 0f && AutoCloseTime > 0f)
		{
			AutoCloseTime -= Time.deltaTime;
			if (AutoCloseTime <= 0f)
			{
				BtnCloseClick();
			}
		}
		UpdateDiscountTime();
		if (isCloseClick)
		{
			BtnCloseClick();
			isCloseClick = false;
		}
	}

	public void UpdateDiscountTime()
	{
		if (!Discount_go.activeInHierarchy)
		{
			return;
		}
		discountWaitTime += Time.deltaTime;
		if (discountWaitTime >= 1f)
		{
			int discountRestSecond = pgsd.GetDiscountRestSecond();
			if (discountRestSecond > 0)
			{
				string text = WordGlobal.ChangeTimeToNomral(discountRestSecond, isNeedDay: false);
				Discount_timeTxt.text = text;
			}
			else
			{
				Discount_go.SetActive(value: false);
				pgsd.ClearDiscount();
				//Product product = CodelessIAPStoreListener.Instance.GetProduct(pgsd.GetOfferItem().Productid);
				string empty = string.Empty;
				//empty = ((product == null) ? pgsd.GetOfferItem().Price.ToString() : WordGlobal.GetPrice(product.metadata.localizedPriceString));
				btnOpen_txt.text = TextLibraryDataManagerCsv.instance.GetText(1284) + "  " + empty;
				//btnOpen_go.GetComponent<IAPButton>().productId = pgsd.GetOfferItem().Productid;
			}
			discountWaitTime = 0f;
		}
	}

	public void OpenAnim(float delay_time)
	{
		TrackEntry trackEntry = Pig_img.AnimationState.SetAnimation(0, "zasui", loop: false);
		trackEntry.Delay = delay_time;
		trackEntry.Complete += OpenAnimEnd;
		isCanCloseUI = false;
		gold_script.gameObject.SetActive(value: true);
		if (MainUIScript.Instance != null)
		{
			MainUIScript.Instance.m_GoldItem.SetActive(value: false);
		}
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.GoldItem.gameObject.SetActive(value: false);
		}
	}

	public void OpenAnimEnd(TrackEntry trackEntry)
	{
		isCanCloseUI = true;
		gold_script._isAddGold = true;
		gold_script.AddGold(gold_maxNum, 1f, 1f, isAdd2PlayerInfo: false, 16);
		gold_script.CollectGoldNew(gold_flyPos.position, 1, 1f, null);
		ShowNextBtn();
		Pig_img.AnimationState.SetAnimation(0, "xunhuan", loop: true);
		Debug.Log("播放砸碎循环动画");
	}

	private void Hide()
	{
		if (isCanCloseUI)
		{
			Object.Destroy(base.gameObject);
		}
	}

	public void BtnCloseClick()
	{
		Hide();
	}

	public void BtnPlayClick()
	{
		PayManager.GetInstance().m_CurrentOfferID = pgsd.GetOfferItem().ID;
		PayManager.PaymentVerifyEvnet += PurchaseCompleted;
		PayManager.PaymentVerifyFailEvnet += PurchaseFaild;
		WordGlobal.DisableBackKey = true;
		if (m_ObjMask != null)
		{
			m_ObjMask.SetActive(value: true);
		}
	}

	public void PurchaseFaild()
	{
		PayManager.PaymentVerifyEvnet -= PurchaseCompleted;
		PayManager.PaymentVerifyFailEvnet -= PurchaseFaild;
		if (m_ObjMask != null)
		{
			m_ObjMask.SetActive(value: false);
		}
	}

	public void PurchaseCompleted(Product data)
	{
		WordGlobal.DisableBackKey = false;
		PayManager.PaymentVerifyEvnet -= PurchaseCompleted;
		PayManager.PaymentVerifyFailEvnet -= PurchaseFaild;
		if (m_ObjMask != null)
		{
			m_ObjMask.SetActive(value: false);
		}
	}

	private void OnDestroy()
	{
		if (isNeedResult)
		{
			GameObject gameObject = UIManager.Instance.LoadMainUI("UI/Main/Result", isPop: true, UIManager.MainUIType.Type_Common, 99);
			gameObject.GetComponent<MainResultScript>().SetContent(pgsd.CurrentLevelScore);
			isNeedResult = false;
			if (MainUIScript.Instance != null)
			{
				MainUIScript.Instance.ShowScroeUI(isShow: false);
			}
		}
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.GoldItem.gameObject.SetActive(value: true);
		}
	}
}
