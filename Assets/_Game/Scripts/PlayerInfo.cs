using LitJson;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;

public class PlayerInfo : MonoBehaviour
{
	public Sprite FBHeadIconSpr;

	private static PlayerInfo instance;

	public List<int> BuyInOffers = new List<int>();

	public int LastPlayLevel;

	public bool NeedTutorialFirstTips;

	public List<OfferItem> OfferList = new List<OfferItem>();

	public SystemConfig systemConfig;

	private Dictionary<string, int> TmpKeyValue = new Dictionary<string, int>();

	private int _curDailyDayId;

	private DailyPuzzleDataItem _curDailyPuzzleItem;

	private DailyPuzzleMonthDataItem _curDailyMonthItem;

	private int FULLPUZZLE_GETCOLD_NUM = 1000;

	private int[] openPuzzleStarNum = new int[9]
	{
		3,
		4,
		4,
		5,
		5,
		6,
		6,
		8,
		9
	};

	private LittleTipsSaveData _littleTipsSaveData;

	private HuoDongSaveData _HuoDongSaveData;

	private HuoDongGardonSaveData _HuoDongGardonSaveDataJson;

	private HuoDongSunSaveData _HuoDongSunSaveDataJson;

	private HuoDongWordFindSaveData _HuoDongWordFindSaveDataJson;

	private HuoDongTournamentSaveData _HuoDongTournamentSaveDataJson;

	private PigGoldSaveData _PigGoldSaveDataJson;

	private WordSearchSaveData _WordSearchSaveDataJson;

	private PlayerOtherInfo _PlayerOtherInfoSaveDataJson;

	private DailyPuzzleTimeMarkData _dailyTimeMark;

	private DailyPuzzleGetImageData _curDailyImageData;

	private DailyQuestSaveDataManager _dailyQuestData;

	public OfferItem CurrentOfferItem;

	public DateTime OfferEndTime;

	public Dictionary<string, WordInfo> WordInfoList = new Dictionary<string, WordInfo>();

	public static PlayerInfo Instance => GetInstance();

	public int IsAlreadyClickRateBtn
	{
		get
		{
			return PlayerPrefs.GetInt("Player.IsAlreadyClickRateBtn", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.IsAlreadyClickRateBtn", value);
		}
	}

	public int PopRateUINum
	{
		get
		{
			return PlayerPrefs.GetInt("Player.PopRateUINum", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.PopRateUINum", value);
		}
	}

	public int PopUpFBClickNextLvNum
	{
		get
		{
			return PlayerPrefs.GetInt("Player.PopUpFBClickNextLvNum", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.PopUpFBClickNextLvNum", value);
		}
	}

	public int PopUpFBOnceLoginNum
	{
		get
		{
			return PlayerPrefs.GetInt("Player.PopUpFBOnceLoginNum", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.PopUpFBOnceLoginNum", value);
		}
	}

	public int PopUpFBNum
	{
		get
		{
			return PlayerPrefs.GetInt("Player.PopUpFBNum", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.PopUpFBNum", value);
		}
	}

	public int LastPopUpFBUtcTime
	{
		get
		{
			return PlayerPrefs.GetInt("Player.LastPopUpFBUtcTime", 1);
		}
		set
		{
			PlayerPrefs.SetInt("Player.LastPopUpFBUtcTime", value);
		}
	}

	public int LoggedFaceBook
	{
		get
		{
			return PlayerPrefs.GetInt("Player.LoggedFaceBook", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.LoggedFaceBook", value);
		}
	}

	public string FaceBookName
	{
		get
		{
			return PlayerPrefs.GetString("Player.FaceBookName", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.FaceBookName", value);
		}
	}

	public string FaceBookID
	{
		get
		{
			return PlayerPrefs.GetString("Player.FaceBookID", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.FaceBookID", value);
		}
	}

	public int FirstGetDay3DailyReward
	{
		get
		{
			return PlayerPrefs.GetInt("Player.IsFirstGetDay3DailyReward", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.IsFirstGetDay3DailyReward", value);
		}
	}

	public int NotificationKey
	{
		get
		{
			return PlayerPrefs.GetInt("Player.NotificationKey", 1);
		}
		set
		{
			PlayerPrefs.SetInt("Player.NotificationKey", value);
		}
	}

	public long NotificationTime
	{
		get
		{
			if (PlayerPrefs.GetString("Player.NotificationTime") == string.Empty)
			{
				return 0L;
			}
			return long.Parse(PlayerPrefs.GetString("Player.NotificationTime"));
		}
		set
		{
			PlayerPrefs.SetString("Player.NotificationTime", value.ToString());
		}
	}

	public int IsGetDailyReward
	{
		get
		{
			return PlayerPrefs.GetInt("Player.IsCanGetDailyReward", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.IsCanGetDailyReward", value);
		}
	}

	public int DailyRewardTarget
	{
		get
		{
			return PlayerPrefs.GetInt("Player.DailyRewardTarget", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.DailyRewardTarget", value);
		}
	}

	public int ShowTutorialWatchVideo
	{
		get
		{
			return PlayerPrefs.GetInt("Player.ShowTutorialWatchVideo", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.ShowTutorialWatchVideo", value);
		}
	}

	public int ShowTutorialDailyPuzzleFinish
	{
		get
		{
			return PlayerPrefs.GetInt("Player.ShowTutorialDailyPuzzleFinish", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.ShowTutorialDailyPuzzleFinish", value);
		}
	}

	public int ShowTutorialDailyQuestFinish
	{
		get
		{
			return PlayerPrefs.GetInt("Player.ShowTutorialDailyQuestFinish", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.ShowTutorialDailyQuestFinish", value);
		}
	}

	public int DailyRewardDay
	{
		get
		{
			return PlayerPrefs.GetInt("Player.DailyRewardDay", 1);
		}
		set
		{
			PlayerPrefs.SetInt("Player.DailyRewardDay", value);
		}
	}

	public int DailyRewardDayNew
	{
		get
		{
			return PlayerPrefs.GetInt("Player.DailyRewardDayNew", 1);
		}
		set
		{
			PlayerPrefs.SetInt("Player.DailyRewardDayNew", value);
		}
	}

	public int LastDailyRewardUtcTime
	{
		get
		{
			return PlayerPrefs.GetInt("Player.LastDailyRewardUtcTime", 1);
		}
		set
		{
			PlayerPrefs.SetInt("Player.LastDailyRewardUtcTime", value);
		}
	}

	public string UUID
	{
		get
		{
			return PlayerPrefs.GetString("Player.UUID", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.UUID", value);
		}
	}

	public int HuoDongOpenTime
	{
		get
		{
			return PlayerPrefs.GetInt("Player.HuoDongOpenTime", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.HuoDongOpenTime", value);
		}
	}

	public int HuoDongGardonOpenTime
	{
		get
		{
			return PlayerPrefs.GetInt("Player.HuoDongGardonOpenTime", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.HuoDongGardonOpenTime", value);
		}
	}

	public int HuoDongSunOpenTime
	{
		get
		{
			return PlayerPrefs.GetInt("Player.HuoDongSunOpenTime", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.HuoDongSunOpenTime", value);
		}
	}

	public int HuoDongWordFindOpenTime
	{
		get
		{
			return PlayerPrefs.GetInt("Player.HuoDongWordFindOpenTime", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.HuoDongWordFindOpenTime", value);
		}
	}

	public int FirstOpen
	{
		get
		{
			return PlayerPrefs.GetInt("Player.FirstOpen", 1);
		}
		set
		{
			PlayerPrefs.SetInt("Player.FirstOpen", value);
		}
	}

	public long Gold => GetSaveLong("PlayerInfo.Gold", 0L);

	public int AdRemove
	{
		get
		{
			return (int)GetSaveLong("PlayerInfo.AdRemove", 0L);
		}
		set
		{
			SaveString("PlayerInfo.AdRemove", value.ToString());
		}
	}

	public string ClickTipsCountBuffDateTime
	{
		get
		{
			return PlayerPrefs.GetString("Player.ClickTipsCountBuffDateTime", WordGlobal.GetTimeStamp(DateTime.Now));
		}
		set
		{
			PlayerPrefs.SetString("Player.ClickTipsCountBuffDateTime", value);
		}
	}

	public int ClickTipsCount
	{
		get
		{
			return (int)GetSaveLong("PlayerInfo.ClickTipsCount", 0L);
		}
		set
		{
			SaveString("PlayerInfo.ClickTipsCount", value.ToString());
		}
	}

	public string TipsCountBuffDateTime
	{
		get
		{
			return PlayerPrefs.GetString("Player.TipsCountBuffDateTime", WordGlobal.GetTimeStamp(DateTime.Now));
		}
		set
		{
			PlayerPrefs.SetString("Player.TipsCountBuffDateTime", value);
		}
	}

	public int TipsCount
	{
		get
		{
			if (GetSaveString("PlayerInfo.TipsCount") == string.Empty)
			{
				return 0;
			}
			return int.Parse(GetSaveString("PlayerInfo.TipsCount"));
		}
		set
		{
			SaveString("PlayerInfo.TipsCount", value.ToString());
		}
	}

	public string FirstTipsCountBuffDateTime
	{
		get
		{
			return PlayerPrefs.GetString("Player.FirstTipsCountBuffDateTime", WordGlobal.GetTimeStamp(DateTime.Now));
		}
		set
		{
			PlayerPrefs.SetString("Player.FirstTipsCountBuffDateTime", value);
		}
	}

	public int FirstTipsCount
	{
		get
		{
			int result = 0;
			bool flag = int.TryParse(GetSaveString("PlayerInfo.FirstTipsCount"), out result);
			return result;
		}
		set
		{
			SaveString("PlayerInfo.FirstTipsCount", value.ToString());
		}
	}

	public long Score
	{
		get
		{
			return GetSaveLong("PlayerInfo.Score", 0L);
		}
		set
		{
			SaveString("PlayerInfo.Score", value.ToString());
		}
	}

	public int ShowTermsService
	{
		get
		{
			return PlayerPrefs.GetInt("PlayerInfo.ShowTermsService", 0);
		}
		set
		{
			PlayerPrefs.SetInt("PlayerInfo.ShowTermsService", value);
		}
	}

	public string GameSaveDatas
	{
		get
		{
			return GetSaveString("PlayerInfo.GameSaveDatas");
		}
		set
		{
			SaveString("PlayerInfo.GameSaveDatas", value);
		}
	}

	public string DailyGameSaveDatas
	{
		get
		{
			return GetSaveString("PlayerInfo.DailyGameSaveDatas");
		}
		set
		{
			SaveString("PlayerInfo.DailyGameSaveDatas", value);
		}
	}

	public string HuoDongSunGameSaveDatas
	{
		get
		{
			return GetSaveString("PlayerInfo.HuoDongSunGameSaveDatas");
		}
		set
		{
			SaveString("PlayerInfo.HuoDongSunGameSaveDatas", value);
		}
	}

	public string FirstTapAdStr
	{
		get
		{
			return GetSaveString("PlayerInfo.FirstTapAdStr");
		}
		set
		{
			SaveString("PlayerInfo.FirstTapAdStr", value);
		}
	}

	public int OTA_CompleteNum
	{
		get
		{
			int result = 0;
			bool flag = int.TryParse(GetSaveString("PlayerInfo.OTA_CompleteNum"), out result);
			return result;
		}
		set
		{
			SaveString("PlayerInfo.OTA_CompleteNum", value.ToString());
		}
	}

	public int MaxPlayLevel
	{
		get
		{
			return (int)GetSaveLong("PlayerInfo.MaxPlayLevel", 1L);
		}
		set
		{
			int num = value;
			SaveString("PlayerInfo.MaxPlayLevel", value.ToString());
			if (LevelMapUIScript.Instance != null && num > 1)
			{
				LevelMapUIScript.Instance.RefreshPointItemIcon(num - 1);
			}
			if (num >= 55 && Instance.FirstTipButtonUnlock == 0)
			{
				Instance.FirstTipButtonUnlock = 1;
				if (instance.FirstTipsCount == 0)
				{
					Instance.FirstTipsCount++;
				}
			}
		}
	}

	public int CurrentLevel
	{
		get
		{
			int num = (int)GetSaveLong("PlayerInfo.CurrentLevel", 1L);
			if (num > MaxPlayLevel)
			{
				CurrentLevel = MaxPlayLevel;
			}
			return (int)GetSaveLong("PlayerInfo.CurrentLevel", 1L);
		}
		set
		{
			if (value > MaxPlayLevel)
			{
				MaxPlayLevel = value;
				if (MaxPlayLevel == 55)
				{
					if (Instance.FirstTipButtonUnlock == 0)
					{
						NeedTutorialFirstTips = true;
						Instance.FirstTipButtonUnlock = 1;
						if (instance.FirstTipsCount == 0)
						{
							Instance.FirstTipsCount++;
						}
					}
				}
				else if (MaxPlayLevel == EventDataManagerCsv.instance.GetMinLevel(1))
				{
					WordFaceBook.Instance.GetHuoDongData();
				}
			}
			SaveString("PlayerInfo.CurrentLevel", value.ToString());
		}
	}

	public int BonusGoldNumber => 5;

	public int FirstTipButtonUnlock
	{
		get
		{
			return (int)GetSaveLong("PlayerInfo.FirstTipButtonUnlock", 0L);
		}
		set
		{
			SaveString("PlayerInfo.FirstTipButtonUnlock", value.ToString());
		}
	}

	public int GetBonusRewardCount
	{
		get
		{
			return (int)GetSaveLong("PlayerInfo.GetBonusRewardCount", 1L);
		}
		set
		{
			SaveString("PlayerInfo.GetBonusRewardCount", value.ToString());
		}
	}

	public int MaxBonusCount
	{
		get
		{
			return 5;
		}
		set
		{
			PlayerPrefs.SetInt("PlayerInfo.MaxBonusCount", value);
		}
	}

	public int CollectedBonusCount
	{
		get
		{
			return (int)GetSaveLong("PlayerInfo.CollectedBonusCount", 0L);
		}
		set
		{
			SaveString("PlayerInfo.CollectedBonusCount", value.ToString());
		}
	}

	public string CollectedBonusList
	{
		get
		{
			return GetSaveString("PlayerInfo.CollectedBonusList");
		}
		set
		{
			SaveString("PlayerInfo.CollectedBonusList", value.ToString());
		}
	}

	public int RemoveAdTips
	{
		get
		{
			return PlayerPrefs.GetInt("player.RemoveAdTips", 0);
		}
		set
		{
			PlayerPrefs.SetInt("player.RemoveAdTips", value);
		}
	}

	public int TutorialStep
	{
		get
		{
			return PlayerPrefs.GetInt("player.tutorial", 0);
		}
		set
		{
			PlayerPrefs.SetInt("player.tutorial", value);
		}
	}

	public int ResVersion
	{
		get
		{
			return PlayerPrefs.GetInt("PlayerResVersion", 0);
		}
		set
		{
			if (value < 0)
			{
				value = 0;
			}
			PlayerPrefs.SetInt("PlayerResVersion", value);
		}
	}

	public string OfferListJson
	{
		get
		{
			return GetSaveString("Player.OfferListJson");
		}
		set
		{
			SaveString("Player.OfferListJson", value);
		}
	}

	public string LastPayUUID
	{
		get
		{
			return PlayerPrefs.GetString("Player.LastPayUUID", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.LastPayUUID", value);
		}
	}

	public float LastOfferPayValue
	{
		get
		{
			float num = PlayerPrefs.GetFloat("Player.LastOfferPayValue", 0f);
			if (num > 0f && IsFristPayVip() ) //&& CodelessIAPStoreListener.Instance != null && string.IsNullOrEmpty(CodelessIAPStoreListener.Instance.SubscriptionEndDate_str))
			{
				num = 0f;
			}
			return num;
		}
		set
		{
			PlayerPrefs.SetFloat("Player.LastOfferPayValue", value);
			if (value > 0f)
			{
				if (GetDailyQuestSaveData().QuestList.Count > 0)
				{
					GetDailyQuestSaveData().RemoveADQuestInList();
					SetDailyQuestSaveData();
				}
				else
				{
					GetDailyQuestSaveData().Getlist(5);
				}
			}
		}
	}

	public string OfferCloseTime
	{
		get
		{
			return PlayerPrefs.GetString("Player.OfferCloseTime", "0");
		}
		set
		{
			PlayerPrefs.SetString("Player.OfferCloseTime", value);
		}
	}

	public int OfferCloseCount
	{
		get
		{
			DateTime dateTime = new DateTime(long.Parse(OfferCloseTime));
			if (DateTime.Now.Day != dateTime.Day)
			{
				OfferCloseCount = 0;
				OfferCloseTime = DateTime.Now.Ticks.ToString();
			}
			return PlayerPrefs.GetInt("Player.OfferCloseCount", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.OfferCloseCount", value);
		}
	}

	public int NoPayPlayLevel
	{
		get
		{
			return PlayerPrefs.GetInt("Player.NoPayPlayLevel", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.NoPayPlayLevel", value);
		}
	}

	public string LasyBuyOfferTime
	{
		get
		{
			return PlayerPrefs.GetString("Player.LasyBuyOfferTime", "0");
		}
		set
		{
			PlayerPrefs.SetString("Player.LasyBuyOfferTime", value);
		}
	}

	public int BuyOfferCount
	{
		get
		{
			DateTime dateTime = new DateTime(long.Parse(LasyBuyOfferTime));
			if (DateTime.Now.Day != dateTime.Day)
			{
				BuyOfferCount = 0;
				LasyBuyOfferTime = DateTime.Now.Ticks.ToString();
			}
			return PlayerPrefs.GetInt("Player.BuyOfferCount", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.BuyOfferCount", value);
		}
	}

	public string RewardAdShowCountTime
	{
		get
		{
			return PlayerPrefs.GetString("Player.RewardAdShowCountTime", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.RewardAdShowCountTime", value);
		}
	}

	public int RewardAdShowCount
	{
		get
		{
			return PlayerPrefs.GetInt("Player.RewardAdShowCount", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.RewardAdShowCount", value);
		}
	}

	public bool ShowRewardAD
	{
		get
		{
			DateTime now = DateTime.Now;
			DateTime d = new DateTime(now.Year, now.Month, now.Day);
			if (RewardAdShowCountTime != string.Empty)
			{
				DateTime d2 = new DateTime(long.Parse(RewardAdShowCountTime));
				if (d2 != d)
				{
					RewardAdShowCount = 0;
					RewardAdShowCountTime = d.Ticks.ToString();
					return true;
				}
				if (RewardAdShowCount < 30)
				{
					return true;
				}
				return false;
			}
			RewardAdShowCount = 0;
			RewardAdShowCountTime = d.Ticks.ToString();
			return true;
		}
	}

	public int FirstGetBonus
	{
		get
		{
			return PlayerPrefs.GetInt("Player.FirstGetBonus", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.FirstGetBonus", value);
		}
	}

	public int FirstGetMoneyWord
	{
		get
		{
			return PlayerPrefs.GetInt("Player.FirstGetMoneyWord", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.FirstGetMoneyWord", value);
		}
	}

	public int FirstLogin
	{
		get
		{
			return PlayerPrefs.GetInt("Player.FirstLogin", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.FirstLogin", value);
		}
	}

	public string FirstLoginDate
	{
		get
		{
			return PlayerPrefs.GetString("Player.FirstLoginDate", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.FirstLoginDate", value);
		}
	}

	public string DailyPuzzleListJson
	{
		get
		{
			return GetSaveString("Player.DailyPuzzleListJson");
		}
		set
		{
			SaveString("Player.DailyPuzzleListJson", value);
		}
	}

	public string DailyPuMonthListJson
	{
		get
		{
			return GetSaveString("Player.DailyPuMonthListJson");
		}
		set
		{
			SaveString("Player.DailyPuMonthListJson", value);
		}
	}

	public string DailyPuzzleTimeMarkJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.DailyPuzzleTimeMarkJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.DailyPuzzleTimeMarkJson", value);
		}
	}

	public string LittleTipsSaveDataJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.LittleTipsSaveDataJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.LittleTipsSaveDataJson", value);
		}
	}

	public string HuoDongSaveDataJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.HuoDongSaveDataJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.HuoDongSaveDataJson", value);
		}
	}

	public string HuoDongGardonSaveDataJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.HuoDongGardonSaveDataJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.HuoDongGardonSaveDataJson", value);
		}
	}

	public string HuoDongSunSaveDataJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.HuoDongSunSaveDataJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.HuoDongSunSaveDataJson", value);
		}
	}

	public string HuoDongWordFindSaveDataJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.HuoDongWordFindSaveDataJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.HuoDongWordFindSaveDataJson", value);
		}
	}

	public string HuoDongTournamentSaveDataJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.HuoDongTournamentSaveDataJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.HuoDongTournamentSaveDataJson", value);
		}
	}

	public string PigGoldSaveDataJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.PigGoldSaveDataJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.PigGoldSaveDataJson", value);
		}
	}

	public string WordSearchSaveDataJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.WordSearchSaveDataJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.WordSearchSaveDataJson", value);
		}
	}

	public string PlayerOtherInfoSaveDataJson
	{
		get
		{
			return PlayerPrefs.GetString("Player.PlayerOtherInfoSaveDataJson", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.PlayerOtherInfoSaveDataJson", value);
		}
	}

	public string DailyPuAllImageJson
	{
		get
		{
			return GetSaveString("Player.DailyPuAllImageJson");
		}
		set
		{
			SaveString("Player.DailyPuAllImageJson", value);
		}
	}

	public string DailyQuestDataJson
	{
		get
		{
			return GetSaveString("Player.DailyQuestDataJson");
		}
		set
		{
			SaveString("Player.DailyQuestDataJson", value);
		}
	}

	public int IsAlreadyShowDailyQuestTutorial
	{
		get
		{
			return PlayerPrefs.GetInt("Player.IsAlreadyShowDailyQuestTutorial", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.IsAlreadyShowDailyQuestTutorial", value);
		}
	}

	public int NewPlayerOffer
	{
		get
		{
			return PlayerPrefs.GetInt("Player.NewPlayerOffer", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.NewPlayerOffer", value);
		}
	}

	public int CsvFileVersion
	{
		get
		{
			return PlayerPrefs.GetInt("Player.CsvFileVersion", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.CsvFileVersion", value);
		}
	}

	public int FirstGetNoThreeBonus
	{
		get
		{
			return PlayerPrefs.GetInt("Player.FirstGetNoThreeBonus", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.FirstGetNoThreeBonus", value);
		}
	}

	public string PlayerName
	{
		get
		{
			return PlayerPrefs.GetString("Player.PlayerName", string.Empty);
		}
		set
		{
			PlayerPrefs.SetString("Player.PlayerName", value);
		}
	}

	public int Clear3Bonus
	{
		get
		{
			return PlayerPrefs.GetInt("Player.Clear3Bonus", 0);
		}
		set
		{
			PlayerPrefs.SetInt("Player.Clear3Bonus", value);
		}
	}

	public int showTestLog
	{
		get
		{
			return PlayerPrefs.GetInt("ShowTestLog", 0);
		}
		set
		{
			PlayerPrefs.SetInt("ShowTestLog", value);
		}
	}

	public static event UnityAction GetOfferEvent;

	private void Start()
	{
	}

	private void Update()
	{
	}

	private static PlayerInfo GetInstance()
	{
		if ((bool)instance)
		{
			return instance;
		}
		GameObject gameObject = new GameObject("PlayerInfo", typeof(PlayerInfo));
		return gameObject.GetComponent<PlayerInfo>();
	}

	private void Awake()
	{
		if (instance != null)
		{
			UnityEngine.Object.DestroyImmediate(base.gameObject);
			return;
		}
		instance = this;
		UnityEngine.Object.DontDestroyOnLoad(this);
	}

	public string CreateUUID(int len = 72)
	{
		System.Random random = new System.Random();
		string text = string.Empty;
		string text2 = "0123456789abcdefghijklmnopqrstuvwxyzQWERTYUIOPLKJHGFDSAZXCVBNM";
		for (int i = 0; i < len; i++)
		{
			text += text2.Substring(random.Next(36), 1);
		}
		return text;
	}

	public void AddGold(long value, int addIndex, bool isSetV = false)
	{
		if (isSetV)
		{
			SaveString("PlayerInfo.Gold", value.ToString());
		}
		else
		{
			SaveString("PlayerInfo.Gold", (Gold + value).ToString());
		}
		string text = "GoldEndV = " + Gold + "&value = " + value + "&addIndex = " + addIndex + "&isSetV = " + isSetV;
		Debug.Log("AddGold log_str:" + text);
		WordFaceBook.Instance.UpdateTrackTaskEvent("Gold", text);
	}

	public bool TryDecGold(long num)
	{
		if (Gold >= num)
		{
			return true;
		}
		if (GameObject.Find("ShopUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/ShopUI").name = "ShopUI";
		}
		return false;
	}

	public bool DecGold(long num)
	{
		if (Gold >= num)
		{
			Instance.AddGold(-num, 16);
			return true;
		}
		if (GameObject.Find("ShopUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/ShopUI").name = "ShopUI";
		}
		return false;
	}

	public long ClickTipsCountBuffTimeLeft()
	{
		return (long)(WordGlobal.ConvertStringToDateTime(ClickTipsCountBuffDateTime) - DateTime.Now).TotalSeconds;
	}

	public long TipsCountBuffTimeLeft()
	{
		return (long)(WordGlobal.ConvertStringToDateTime(TipsCountBuffDateTime) - DateTime.Now).TotalSeconds;
	}

	public long FirstTipsCountBuffTimeLeft()
	{
		return (long)(WordGlobal.ConvertStringToDateTime(FirstTipsCountBuffDateTime) - DateTime.Now).TotalSeconds;
	}

	public void SetFirstTapAdStr(string str)
	{
		if (string.IsNullOrEmpty(FirstTapAdStr))
		{
			FirstTapAdStr = str;
		}
	}

	public bool AddBonusWord(int level, string word)
	{
		bool result = true;
		if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
		{
			level = instance.GetDailyDayId();
		}
		if (UIManager.CurMainUIType != UIManager.MainUIType.Type_HDGardon)
		{
			CollectedBonus collectedBonus;
			if (CollectedBonusList == string.Empty)
			{
				collectedBonus = new CollectedBonus();
				collectedBonus.BonusList = new List<BonusWord>();
			}
			else
			{
				collectedBonus = JsonUtility.FromJson<CollectedBonus>(CollectedBonusList);
			}
			if (collectedBonus.BonusList.FindIndex((BonusWord x) => x.Level == level && x.Word == word) >= 0)
			{
				result = false;
			}
			else
			{
				BonusWord bonusWord = new BonusWord();
				bonusWord.Level = level;
				bonusWord.Word = word;
				collectedBonus.BonusList.Add(bonusWord);
			}
			CollectedBonusList = JsonUtility.ToJson(collectedBonus);
		}
		return result;
	}

	public List<string> GetLevelBonusList(int level)
	{
		List<string> list = new List<string>();
		if (CollectedBonusList != string.Empty)
		{
			CollectedBonus collectedBonus = JsonUtility.FromJson<CollectedBonus>(CollectedBonusList);
			if (collectedBonus == null)
			{
				return list;
			}
			List<BonusWord> list2 = collectedBonus.BonusList.FindAll((BonusWord x) => x.Level == level);
			for (int i = 0; i < list2.Count; i++)
			{
				list.Add(list2[i].Word);
			}
		}
		return list;
	}

	public void AddOfferToList(OfferItem item)
	{
		OfferList.Add(item);
	}

	private void OnDestroy()
	{
		PlayerPrefs.Save();
	}

	private void SaveString(string key, string str)
	{
		PlayerPrefs.SetString(key, PBEWithMD5AndDESEncryption.EncryptTxt(str));
	}

	private string GetSaveString(string id)
	{
		return PBEWithMD5AndDESEncryption.DecryptTxt(PlayerPrefs.GetString(id));
	}

	private long GetSaveLong(string id, long defaultvalue = 0L)
	{
		if (long.TryParse(GetSaveString(id), out long result))
		{
			return result;
		}
		return defaultvalue;
	}

	public void Restart()
	{
		GameSaveDatas = string.Empty;
		DailyGameSaveDatas = string.Empty;
		instance = null;
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void SetRepeatingNotification()
	{
		Debug.Log("Set Notify");
		//DateTime now = DateTime.Now;
		//DateTime d = new DateTime(now.Year, now.Month, now.Day, 12, 0, 0);
		//if (now.Hour >= 12)
		//{
		//	d = d.AddDays(1.0);
		//}
		//UnityEngine.iOS.LocalNotification.SendRepeatingNotification(10, (long)(d - now).TotalSeconds, NotificationDataManager.instance.Get12Data().title, NotificationDataManager.instance.Get12Data().message);
	}

	public void Set18HourNotification()
	{
		Debug.Log("Set Notify");
		//DateTime now = DateTime.Now;
		//DateTime dateTime = new DateTime(now.Year, now.Month, now.Day, 18, 0, 0);
		//if (now.Hour >= 18)
		//{
		//	dateTime = dateTime.AddDays(1.0);
		//}
		//DateTime dateTime2 = dateTime;
		//for (int i = 0; i < 10; i++)
		//{
		//	int index = UnityEngine.Random.Range(0, NotificationDataManager.instance.Get18DataList().Count);
		//	UnityEngine.iOS.LocalNotification.SendNotification(i, (long)(dateTime - now).TotalSeconds, NotificationDataManager.instance.Get18DataList()[index].title, NotificationDataManager.instance.Get18DataList()[index].message);
		//	dateTime = dateTime.AddDays(1.0);
		//}
	}

	public int GetTmpKeyValue(string key)
	{
		if (TmpKeyValue.ContainsKey(key))
		{
			return TmpKeyValue[key];
		}
		return 0;
	}

	public void SetTmpKeyValue(string key, int value)
	{
		if (TmpKeyValue.ContainsKey(key))
		{
			TmpKeyValue[key] = value;
		}
		else
		{
			TmpKeyValue.Add(key, value);
		}
	}

	public void GetBuyOffers(int type = 1)
	{
		JsonData jsonData = new JsonData();
		jsonData["uuid"] = Instance.UUID;
		jsonData["versionCode"] = Application.version;
		jsonData["promotion"] = type;
		jsonData["platform"] = 0;
		Debug.Log(jsonData.ToJson());
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["getofferlist"] = GetOfferListCallback;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "getofferlist", jsonData, dictionary);
	}

	private bool GetOfferListCallback(JsonData data)
	{
		Instance.BuyInOffers = new List<int>();
		if (data != null)
		{
			JsonData jsonData = data["offerlist"];
			for (int i = 0; i < jsonData.Count; i++)
			{
				int num = int.Parse(jsonData[i].ToString());
				BuyInOffers.Add(num);
				if (num == 1)
				{
					NewPlayerOffer = 1;
				}
			}
			JsonData jsonData2 = data["salelist"];
			OfferListJson = jsonData2.ToJson();
			PlayerPrefs.Save();
			Instance.OfferList = new List<OfferItem>();
			for (int j = 0; j < jsonData2.Count; j++)
			{
				OfferItem item = new OfferItem(jsonData2[j]);
				Instance.AddOfferToList(item);
			}
			if (Instance.UUID != Instance.LastPayUUID || Instance.LastOfferPayValue == 0f)
			{
				Instance.LastOfferPayValue = float.Parse(data["lastpay"].ToString());
			}
			OfferEndTime = DateTime.Now.AddSeconds(int.Parse(data["timeleft"].ToString()));
			CurrentOfferItem = OfferList.Find((OfferItem x) => x.Type == 1);
		}
		else
		{
			if (!string.IsNullOrEmpty(OfferListJson))
			{
				JsonData jsonData3 = JsonMapper.ToObject(OfferListJson);
				OfferList = new List<OfferItem>();
				for (int k = 0; k < jsonData3.Count; k++)
				{
					OfferItem item2 = new OfferItem(jsonData3[k]);
					AddOfferToList(item2);
				}
			}
			CurrentOfferItem = null;
		}
		if (PlayerInfo.GetOfferEvent != null)
		{
			PlayerInfo.GetOfferEvent();
		}
		return true;
	}

	public bool IsFristPayVip()
	{
		if (BuyInOffers.Count == 1)
		{
			int num = BuyInOffers[0];
			if (num == 28)
			{
				return true;
			}
		}
		return false;
	}

	public void SaveBuyOfferItem(int offerId)
	{
		string key = "Player.FirstGetBonus_" + offerId;
		string value = DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day;
		PlayerPrefs.SetString(key, value);
	}

	public string GetBuyOfferItemDate(int offerId)
	{
		string key = "Player.FirstGetBonus_" + offerId;
		return PlayerPrefs.GetString(key, string.Empty);
	}

	public List<OfferItem> GetNoPayOfferList()
	{
		return Instance.OfferList.FindAll((OfferItem x) => x.Type == ((!(Instance.LastOfferPayValue > 0f)) ? 1 : 2) && !GetOfferIsPayToday(x.ID));
	}

	public bool GetOfferIsPayToday(int offerId)
	{
		bool result = false;
		string buyOfferItemDate = GetBuyOfferItemDate(offerId);
		if (buyOfferItemDate != null && buyOfferItemDate != string.Empty)
		{
			DateTime now = DateTime.Now;
			string[] array = buyOfferItemDate.Split('_');
			if (int.Parse(array[0]) == now.Year && int.Parse(array[1]) == now.Month && int.Parse(array[2]) == now.Day)
			{
				result = true;
			}
		}
		return result;
	}

	public bool IsShowVip()
	{
		if (!WordGlobal.isHaveVIP)
		{
			return false;
		}
		if (instance.GetPlayerOtherInfoSaveData().isBoughtVip)
		{
			return false;
		}
		return true;
	}

	public int GetDailyDayId()
	{
		if (_curDailyDayId <= 0)
		{
            int num = DateTime.Now.Year % 2000;
            _curDailyDayId = num * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day;
			_curDailyDayId = ((_curDailyDayId >= GetDailyPuTimeMarkData().DailyPuzzleLevel) ? _curDailyDayId : GetDailyPuTimeMarkData().DailyPuzzleLevel);
		}
		return _curDailyDayId;
	}

	public int GetDailyMonthId()
	{
		int dailyDayId = GetDailyDayId();
		return dailyDayId / 100;
	}

	public DailyPuzzleDataItem GetDailyPuzzleItem()
	{
		if (_curDailyPuzzleItem == null && DailyPuzzleListJson != string.Empty)
		{
			DailyPuzzleData dailyPuzzleData = JsonUtility.FromJson<DailyPuzzleData>(DailyPuzzleListJson);
			_curDailyPuzzleItem = dailyPuzzleData.DailyPuzzleItemList.Find((DailyPuzzleDataItem x) => x.ID == GetDailyDayId());
		}
		if (_curDailyPuzzleItem == null)
		{
			_curDailyPuzzleItem = new DailyPuzzleDataItem();
			_curDailyPuzzleItem.ID = GetDailyDayId();
		}
		return _curDailyPuzzleItem;
	}

	public DailyPuzzleDataItem GetDailyPuzzleItemById(int id)
	{
		DailyPuzzleDataItem dailyPuzzleDataItem = null;
		if (DailyPuzzleListJson != string.Empty)
		{
			DailyPuzzleData dailyPuzzleData = JsonUtility.FromJson<DailyPuzzleData>(DailyPuzzleListJson);
			dailyPuzzleDataItem = dailyPuzzleData.DailyPuzzleItemList.Find((DailyPuzzleDataItem x) => x.ID == id);
		}
		if (dailyPuzzleDataItem == null)
		{
			dailyPuzzleDataItem = new DailyPuzzleDataItem();
			dailyPuzzleDataItem.ID = GetDailyDayId();
		}
		return dailyPuzzleDataItem;
	}

	public void ChangeDailyPuzzleItem(int starNum)
	{
		if (_curDailyPuzzleItem == null)
		{
			GetDailyPuzzleItem();
		}
		_curDailyPuzzleItem.Stars = starNum;
		_curDailyPuzzleItem.IsFinished = 1;
		DailyPuzzleData dailyPuzzleData;
		if (DailyPuzzleListJson == string.Empty)
		{
			dailyPuzzleData = new DailyPuzzleData();
			dailyPuzzleData.DailyPuzzleItemList = new List<DailyPuzzleDataItem>();
		}
		else
		{
			dailyPuzzleData = JsonUtility.FromJson<DailyPuzzleData>(DailyPuzzleListJson);
		}
		int num = dailyPuzzleData.DailyPuzzleItemList.FindIndex((DailyPuzzleDataItem x) => x.ID == GetDailyDayId());
		if (num >= 0)
		{
			dailyPuzzleData.DailyPuzzleItemList[num] = _curDailyPuzzleItem;
		}
		else
		{
			dailyPuzzleData.DailyPuzzleItemList.Add(_curDailyPuzzleItem);
		}
		DailyPuzzleListJson = JsonUtility.ToJson(dailyPuzzleData);
	}

	public DailyPuzzleMonthDataItem GetDailyPuzzleMonthItem()
	{
		if (_curDailyMonthItem == null && DailyPuMonthListJson != string.Empty)
		{
			DailyPuzzleMonthData dailyPuzzleMonthData = JsonUtility.FromJson<DailyPuzzleMonthData>(DailyPuMonthListJson);
			_curDailyMonthItem = dailyPuzzleMonthData.DailyPuzzleMonthList.Find((DailyPuzzleMonthDataItem x) => x.ID == GetDailyMonthId());
		}
		if (_curDailyMonthItem == null)
		{
			_curDailyMonthItem = new DailyPuzzleMonthDataItem();
			_curDailyMonthItem.ID = GetDailyMonthId();
		}
		return _curDailyMonthItem;
	}

	public void SavePuzzleResultData(int today_get_starNum)
	{
		ChangeDailyPuzzleItem(today_get_starNum);
		DailyPuzzleMonthDataItem dailyPuzzleMonthItem = GetDailyPuzzleMonthItem();
		int num = dailyPuzzleMonthItem.StarNum + today_get_starNum;
		int num2 = -1;
		bool flag = false;
		int num3 = num;
		int num4 = 0;
		for (int i = 0; i < openPuzzleStarNum.Length && num3 >= openPuzzleStarNum[i]; i++)
		{
			num4++;
			num3 -= openPuzzleStarNum[i];
		}
		if (num4 > dailyPuzzleMonthItem.PuzzleList.Count)
		{
			flag = true;
		}
		if (flag)
		{
			List<int> list = new List<int>();
			for (int j = 0; j < 9; j++)
			{
				bool flag2 = false;
				for (int k = 0; k < dailyPuzzleMonthItem.PuzzleList.Count; k++)
				{
					if (dailyPuzzleMonthItem.PuzzleList[k] == j)
					{
						flag2 = true;
					}
				}
				if (!flag2)
				{
					list.Add(j);
				}
			}
			if (list.Count > 0)
			{
				int index = UnityEngine.Random.Range(0, list.Count);
				num2 = list[index];
			}
			else
			{
				WordGlobal.DebugLog("图片拼图收集齐了~！");
			}
		}
		DPCollectPicUIScript.NEW_PUZZLEIDX = num2;
		if (num2 >= 0)
		{
			dailyPuzzleMonthItem.PuzzleList.Add(num2);
			if (dailyPuzzleMonthItem.PuzzleList.Count >= 9)
			{
				AddDailyImageData(_curDailyMonthItem.ID);
			}
		}
		ChangeDailyMonthItem(num, dailyPuzzleMonthItem.PuzzleList);
	}

	public void ChangeDailyMonthItem(int starNum, List<int> puzzleList)
	{
		if (_curDailyMonthItem == null)
		{
			GetDailyPuzzleMonthItem();
		}
		_curDailyMonthItem.StarNum = starNum;
		_curDailyMonthItem.PuzzleList = puzzleList;
		DailyPuzzleMonthData dailyPuzzleMonthData;
		if (DailyPuMonthListJson == string.Empty)
		{
			dailyPuzzleMonthData = new DailyPuzzleMonthData();
			dailyPuzzleMonthData.DailyPuzzleMonthList = new List<DailyPuzzleMonthDataItem>();
		}
		else
		{
			dailyPuzzleMonthData = JsonUtility.FromJson<DailyPuzzleMonthData>(DailyPuMonthListJson);
		}
		int num = dailyPuzzleMonthData.DailyPuzzleMonthList.FindIndex((DailyPuzzleMonthDataItem x) => x.ID == GetDailyMonthId());
		if (num >= 0)
		{
			dailyPuzzleMonthData.DailyPuzzleMonthList[num] = _curDailyMonthItem;
		}
		else
		{
			dailyPuzzleMonthData.DailyPuzzleMonthList.Add(_curDailyMonthItem);
		}
		DailyPuMonthListJson = JsonUtility.ToJson(dailyPuzzleMonthData);
	}

	public void CHangeDailyMonthItemReward(bool isGot)
	{
		if (_curDailyMonthItem == null)
		{
			GetDailyPuzzleMonthItem();
		}
		_curDailyMonthItem.isGotReward = isGot;
		DailyPuzzleMonthData dailyPuzzleMonthData;
		if (DailyPuMonthListJson == string.Empty)
		{
			dailyPuzzleMonthData = new DailyPuzzleMonthData();
			dailyPuzzleMonthData.DailyPuzzleMonthList = new List<DailyPuzzleMonthDataItem>();
		}
		else
		{
			dailyPuzzleMonthData = JsonUtility.FromJson<DailyPuzzleMonthData>(DailyPuMonthListJson);
		}
		int num = dailyPuzzleMonthData.DailyPuzzleMonthList.FindIndex((DailyPuzzleMonthDataItem x) => x.ID == GetDailyMonthId());
		if (num >= 0)
		{
			dailyPuzzleMonthData.DailyPuzzleMonthList[num] = _curDailyMonthItem;
		}
		else
		{
			dailyPuzzleMonthData.DailyPuzzleMonthList.Add(_curDailyMonthItem);
		}
		DailyPuMonthListJson = JsonUtility.ToJson(dailyPuzzleMonthData);
	}

	public LittleTipsSaveData GetLittleTipsData()
	{
		if (_littleTipsSaveData == null)
		{
			if (LittleTipsSaveDataJson != string.Empty)
			{
				_littleTipsSaveData = JsonUtility.FromJson<LittleTipsSaveData>(LittleTipsSaveDataJson);
			}
			else
			{
				_littleTipsSaveData = new LittleTipsSaveData();
			}
		}
		return _littleTipsSaveData;
	}

	public void SaveLittleTipsData()
	{
		LittleTipsSaveDataJson = JsonUtility.ToJson(GetLittleTipsData());
	}

	public HuoDongSaveData GetHuoDongSaveData()
	{
		if (_HuoDongSaveData == null)
		{
			if (HuoDongSaveDataJson != string.Empty)
			{
				_HuoDongSaveData = JsonUtility.FromJson<HuoDongSaveData>(HuoDongSaveDataJson);
			}
			else
			{
				_HuoDongSaveData = new HuoDongSaveData();
			}
		}
		return _HuoDongSaveData;
	}

	public void SaveHuoDongSaveData()
	{
		if (WordGlobal.isNeedHuoDong)
		{
			if (WordGlobal.GetHuoDongTimeLeft() > 0)
			{
				HuoDongSaveDataJson = JsonUtility.ToJson(GetHuoDongSaveData());
			}
			else
			{
				HuoDongSaveDataJson = string.Empty;
			}
		}
	}

	public void ClearHuoDongData()
	{
		if (!string.IsNullOrEmpty(HuoDongGardonSaveDataJson) && GetHuoDongSaveData().HuoDongItemAmount > 0)
		{
			string info = "ItemAmount=" + GetHuoDongSaveData().HuoDongItemAmount + "&GetItemMaxLevel=" + GetHuoDongSaveData().GetItemMaxLevel;
			WordFaceBook.Instance.SendHuoDongInfo("HuoDong", info);
		}
		HuoDongSaveDataJson = string.Empty;
	}

	public HuoDongGardonSaveData GetHuoDongGardonSaveData()
	{
		if (_HuoDongGardonSaveDataJson == null)
		{
			if (HuoDongGardonSaveDataJson != string.Empty)
			{
				_HuoDongGardonSaveDataJson = JsonUtility.FromJson<HuoDongGardonSaveData>(HuoDongGardonSaveDataJson);
			}
			else
			{
				_HuoDongGardonSaveDataJson = new HuoDongGardonSaveData();
			}
		}
		return _HuoDongGardonSaveDataJson;
	}

	public void SaveHuoDongGardonSaveData()
	{
		if (WordGlobal.isNeedHuoDong)
		{
			if (WordGlobal.GetHuoDongGardonTimeLeft() > 0)
			{
				HuoDongGardonSaveDataJson = JsonUtility.ToJson(GetHuoDongGardonSaveData());
			}
			else
			{
				HuoDongGardonSaveDataJson = string.Empty;
			}
		}
	}

	public void ClearHuoDongGardonData(bool isOnlyClear = false)
	{
		if (!isOnlyClear && !string.IsNullOrEmpty(HuoDongGardonSaveDataJson) && GetHuoDongGardonSaveData().HuoDongItemAmount > 0)
		{
			string text = string.Empty;
			for (int i = 0; i < GetHuoDongGardonSaveData().GetRewardNum.Count; i++)
			{
				text = text + GetHuoDongGardonSaveData().GetRewardNum[i] + "&";
			}
			string info = "ItemAmount=" + GetHuoDongGardonSaveData().HuoDongItemAmount + "&GetRewardNum=" + text + "&BuyPowerNum=" + GetHuoDongGardonSaveData().BuyPowerNum;
			WordFaceBook.Instance.SendHuoDongInfo("HuoDongGardon", info);
		}
		HuoDongGardonSaveDataJson = string.Empty;
		_HuoDongGardonSaveDataJson = null;
	}

	public HuoDongSunSaveData GetHuoDongSunSaveData()
	{
		if (_HuoDongSunSaveDataJson == null)
		{
			if (HuoDongSunSaveDataJson != string.Empty)
			{
				_HuoDongSunSaveDataJson = JsonUtility.FromJson<HuoDongSunSaveData>(HuoDongSunSaveDataJson);
				_HuoDongSunSaveDataJson.Init();
			}
			else
			{
				_HuoDongSunSaveDataJson = new HuoDongSunSaveData();
				_HuoDongSunSaveDataJson.Init();
			}
		}
		return _HuoDongSunSaveDataJson;
	}

	public void SaveHuoDongSunSaveData()
	{
		if (WordGlobal.isNeedHuoDong)
		{
			if (WordGlobal.GetHuoDongSunTimeLeft() > 0)
			{
				HuoDongSunSaveDataJson = JsonUtility.ToJson(GetHuoDongSunSaveData());
			}
			else
			{
				HuoDongSunSaveDataJson = string.Empty;
			}
		}
	}

	public void ClearHuoDongSunData(bool isOnlyClear = false)
	{
		if (!isOnlyClear && !string.IsNullOrEmpty(HuoDongSunSaveDataJson) && GetHuoDongSunSaveData().HuoDongItemAmount > 0)
		{
			string info = "SunItemAmount=" + GetHuoDongSunSaveData().HuoDongItemAmount;
			WordFaceBook.Instance.SendHuoDongInfo("HuoDongSun", info);
		}
		HuoDongSunSaveDataJson = string.Empty;
		_HuoDongSunSaveDataJson = null;
	}

	public HuoDongWordFindSaveData GetHuoDongWordFindSaveData()
	{
		if (_HuoDongWordFindSaveDataJson == null)
		{
			if (HuoDongWordFindSaveDataJson != string.Empty)
			{
				_HuoDongWordFindSaveDataJson = JsonUtility.FromJson<HuoDongWordFindSaveData>(HuoDongWordFindSaveDataJson);
				_HuoDongWordFindSaveDataJson.Init();
			}
			else
			{
				_HuoDongWordFindSaveDataJson = new HuoDongWordFindSaveData();
				_HuoDongWordFindSaveDataJson.Init();
			}
		}
		return _HuoDongWordFindSaveDataJson;
	}

	public void SaveHuoDongWordFindSaveData()
	{
		if (WordGlobal.isNeedHuoDong)
		{
			if (WordGlobal.GetHuoDongWordFindTimeLeft() > 0)
			{
				HuoDongWordFindSaveDataJson = JsonUtility.ToJson(GetHuoDongWordFindSaveData());
			}
			else
			{
				HuoDongWordFindSaveDataJson = string.Empty;
			}
		}
	}

	public void ClearHuoDongWordFindData(bool isOnlyClear = false)
	{
		if (!isOnlyClear && !string.IsNullOrEmpty(HuoDongWordFindSaveDataJson) && GetHuoDongWordFindSaveData().HuoDongItemAmount > 0)
		{
			string info = "WordFindItemAmount=" + GetHuoDongWordFindSaveData().HuoDongItemAmount;
			WordFaceBook.Instance.SendHuoDongInfo("HuoDongWordFind", info);
		}
		HuoDongWordFindSaveDataJson = string.Empty;
		_HuoDongWordFindSaveDataJson = null;
	}

	public HuoDongTournamentSaveData GetHuoDongTournamentSaveData()
	{
		if (_HuoDongTournamentSaveDataJson == null)
		{
			if (HuoDongTournamentSaveDataJson != string.Empty)
			{
				_HuoDongTournamentSaveDataJson = JsonUtility.FromJson<HuoDongTournamentSaveData>(HuoDongTournamentSaveDataJson);
				_HuoDongTournamentSaveDataJson.Init();
			}
			else
			{
				_HuoDongTournamentSaveDataJson = new HuoDongTournamentSaveData();
				_HuoDongTournamentSaveDataJson.Init();
			}
		}
		return _HuoDongTournamentSaveDataJson;
	}

	public void SaveHuoDongTournamentSaveData()
	{
		if (WordGlobal.isNeedHuoDong)
		{
			if (WordGlobal.GetHuoDongTournamentTimeLeft() > 0)
			{
				HuoDongTournamentSaveDataJson = JsonUtility.ToJson(GetHuoDongTournamentSaveData());
			}
			else
			{
				HuoDongTournamentSaveDataJson = string.Empty;
			}
		}
	}

	public void ClearHuoDongTournamentSaveData()
	{
		if (!string.IsNullOrEmpty(HuoDongTournamentSaveDataJson) && GetHuoDongTournamentSaveData().HuoDongItemAmount > 0)
		{
			string info = "TournamentItemAmount=" + GetHuoDongTournamentSaveData().HuoDongItemAmount;
			WordFaceBook.Instance.SendHuoDongInfo("HuoDongTournament", info);
		}
		HuoDongTournamentSaveDataJson = string.Empty;
		_HuoDongTournamentSaveDataJson = null;
	}

	public PigGoldSaveData GetPigGoldSaveData()
	{
		if (_PigGoldSaveDataJson == null)
		{
			if (PigGoldSaveDataJson != string.Empty)
			{
				_PigGoldSaveDataJson = JsonUtility.FromJson<PigGoldSaveData>(PigGoldSaveDataJson);
			}
			else
			{
				_PigGoldSaveDataJson = new PigGoldSaveData();
			}
		}
		return _PigGoldSaveDataJson;
	}

	public void SavePigGoldSaveData()
	{
		PigGoldSaveDataJson = JsonUtility.ToJson(GetPigGoldSaveData());
	}

	public WordSearchSaveData GetWordSearchSaveData()
	{
		if (_WordSearchSaveDataJson == null)
		{
			if (WordSearchSaveDataJson != string.Empty)
			{
				_WordSearchSaveDataJson = JsonUtility.FromJson<WordSearchSaveData>(WordSearchSaveDataJson);
			}
			else
			{
				_WordSearchSaveDataJson = new WordSearchSaveData();
			}
		}
		return _WordSearchSaveDataJson;
	}

	public void SaveWordSearchSaveData()
	{
		WordSearchSaveDataJson = JsonUtility.ToJson(GetWordSearchSaveData());
	}

	public PlayerOtherInfo GetPlayerOtherInfoSaveData()
	{
		if (_PlayerOtherInfoSaveDataJson == null)
		{
			if (PlayerOtherInfoSaveDataJson != string.Empty)
			{
				_PlayerOtherInfoSaveDataJson = JsonUtility.FromJson<PlayerOtherInfo>(PlayerOtherInfoSaveDataJson);
			}
			else
			{
				_PlayerOtherInfoSaveDataJson = new PlayerOtherInfo();
			}
		}
		return _PlayerOtherInfoSaveDataJson;
	}

	public void SavePlayerOtherInfoSaveData()
	{
		PlayerOtherInfoSaveDataJson = JsonUtility.ToJson(GetPlayerOtherInfoSaveData());
	}

	public DailyPuzzleTimeMarkData GetDailyPuTimeMarkData()
	{
		if (_dailyTimeMark == null)
		{
			if (DailyPuzzleTimeMarkJson != string.Empty)
			{
				_dailyTimeMark = JsonUtility.FromJson<DailyPuzzleTimeMarkData>(DailyPuzzleTimeMarkJson);
			}
			else
			{
				_dailyTimeMark = new DailyPuzzleTimeMarkData();
			}
		}
		return _dailyTimeMark;
	}

	public void SetDailyTimeMark()
	{
		if (_dailyTimeMark == null)
		{
			_dailyTimeMark = new DailyPuzzleTimeMarkData();
		}
		_dailyTimeMark.VersionCode = Application.version;
		_dailyTimeMark.DailyPuzzleLevel = GetDailyDayId();
		if (WordGlobal.IsShowDailyPuzzleBtn)
		{
			DailyPuzzleTimeMarkJson = JsonUtility.ToJson(_dailyTimeMark);
		}
	}

	public DailyPuzzleGetImageData GetDailyPuzzleImageData()
	{
		if (_curDailyImageData == null && DailyPuAllImageJson != string.Empty)
		{
			_curDailyImageData = JsonUtility.FromJson<DailyPuzzleGetImageData>(DailyPuAllImageJson);
		}
		if (_curDailyImageData == null)
		{
			_curDailyImageData = new DailyPuzzleGetImageData();
			_curDailyImageData.ImageList = new List<int>();
		}
		return _curDailyImageData;
	}

	public void AddDailyImageData(int imageId)
	{
		if (_curDailyImageData == null)
		{
			GetDailyPuzzleImageData();
		}
		if (!_curDailyImageData.ImageList.Contains(imageId))
		{
			_curDailyImageData.ImageList.Add(imageId);
		}
		DailyPuAllImageJson = JsonUtility.ToJson(_curDailyImageData);
	}

	public void SetDailyImageDataTutorial(int tutorial)
	{
		if (_curDailyImageData == null)
		{
			GetDailyPuzzleImageData();
		}
		_curDailyImageData.Tutorial = tutorial;
		DailyPuAllImageJson = JsonUtility.ToJson(_curDailyImageData);
	}

	public DailyQuestSaveDataManager GetDailyQuestSaveData()
	{
		if (_dailyQuestData == null)
		{
			if (DailyQuestDataJson != string.Empty)
			{
				_dailyQuestData = JsonUtility.FromJson<DailyQuestSaveDataManager>(DailyQuestDataJson);
			}
			else
			{
				_dailyQuestData = new DailyQuestSaveDataManager();
			}
		}
		return _dailyQuestData;
	}

	public void SetDailyQuestSaveData()
	{
		if (_dailyQuestData == null)
		{
			_dailyQuestData = new DailyQuestSaveDataManager();
		}
		DailyQuestDataJson = JsonUtility.ToJson(_dailyQuestData);
	}

	public void GetMailListData(NetMill.CommandCallback callBack)
	{
		JsonData jsonData = new JsonData();
		if (Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = Instance.CreateUUID();
			Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["getgiftlist"] = callBack;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "getgiftlist", jsonData, dictionary);
	}

	public void AddItemByStr(string itemStr, int idx)
	{
		Debug.Log("Playerinfo addItemByStr:" + itemStr + " idx:" + idx);
		string[] array = itemStr.Split('_');
		switch (int.Parse(array[0]))
		{
		case 1:
		{
			int num3 = int.Parse(array[1]);
			int num4 = int.Parse(array[2]);
			switch (num3)
			{
			case 1001:
				Instance.TipsCount += num4;
				break;
			case 1002:
				Instance.ClickTipsCount += num4;
				break;
			case 1003:
				Instance.FirstTipsCount += num4;
				break;
			case 2001:
				instance.GetPlayerOtherInfoSaveData().setgHalfPayFirstTipLastTime(num4);
				break;
			case 2003:
				instance.GetPlayerOtherInfoSaveData().SetHalfPayAllFirstLetterTimeByS(num4);
				break;
			}
			break;
		}
		case 3:
		{
			int num2 = int.Parse(array[1]);
			Instance.AddGold(num2, 14);
			break;
		}
		case 4:
		{
			int num = int.Parse(array[1]);
			Instance.GetWordSearchSaveData().AddGuideItemAmount(1);
			break;
		}
		}
	}

	public int GetApplicationVNum()
	{
		string[] array = Application.version.Split('.');
		int result = 0;
		if (array.Length >= 3)
		{
			int num = int.Parse(array[0]) * 1000;
			int num2 = int.Parse(array[1]) * 100;
			int num3 = int.Parse(array[2]);
			result = num + num2 + num3;
		}
		return result;
	}

	public void SetShowTestLog()
	{
		if (showTestLog == 0)
		{
			showTestLog = 1;
		}
		else
		{
			showTestLog = 0;
		}
		ShowTestLog();
	}

	public void ShowTestLog()
	{
		if (showTestLog > 0)
		{
			if (GameObject.Find("TestLog") == null)
			{
				GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI("UI/TestLog", null);
				gameObject.name = "TestLog";
			}
		}
		else
		{
			TestLog.isDestroy = true;
		}
	}

	public void GetMailData()
	{
		GetMailListData(GetMailListCallBack);
		Invoke("GetMailFailNext", 5f);
	}

	private void GetMailFailNext()
	{
		NetMill.Instance.ResetSendQueue();
	}

	private bool GetMailListCallBack(JsonData data)
	{
		if (data == null)
		{
			return false;
		}
		JsonData jsonData = data["giftlist"];
		WordGlobal.IsShowMailBtn = (jsonData.Count > 0);
		CancelInvoke("GetMailFailNext");
		return true;
	}
}
