using System;

[Serializable]
public class PlayerOtherInfo
{
	public int HuoDongTutorial02;

	public int HuoDongGardonTutorial01;

	public int HuoDongSunTutorial02;

	public int HuoDongTournamentTutorial01;

	public int HuoDongWordFindTutorial02;

	public double HalfPayFirstTipTimeStamp;

	private DateTime HalfPayFirstTipTimeStamp_dt;

	public int HalfPayFirstTipTutorial;

	public static double HalfPayAllFirstLettersTimeStamp;

	private DateTime HalfPayAllFirstLettersTimeStamp_dt;

	public int ShowButterFlyTVCommonLevel;

	public int HuoDongGardonOpenDayId;

	public int HuoDongSunOpenDayId;

	public bool[] isHuoDongTournamentTutorialShow = new bool[4];

	public int HuoDongTournamentDateID;

	public int ButterFlyDailyPuzzleTutorial;

	public bool isBoughtVip;

	public int LuckyDrawDateNum;

	public int LuckyDrawBuyTimes;

	public int ShowLuckyDrawTutorialDateNum;

	public bool ShowLuckyDrawTutorial()
	{
		if (!WordGlobal.isHaveLuckyDraw)
		{
			return false;
		}
		if (ShowLuckyDrawTutorialDateNum == WordGlobal.GetDateNum(DateTime.Now))
		{
			return false;
		}
		ShowLuckyDrawTutorialDateNum = WordGlobal.GetDateNum(DateTime.Now);
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
		return true;
	}

	public bool IsNeedLuckyDrawBtn()
	{
		if (!WordGlobal.isHaveLuckyDraw)
		{
			return false;
		}
		if (LuckyDrawDateNum == WordGlobal.GetDateNum(DateTime.Now))
		{
			return false;
		}
		return true;
	}

	public void SetBoughtLuckyDraw()
	{
		WordGlobal.isUpdateLevelMapLeftBtn = true;
		LuckyDrawDateNum = WordGlobal.GetDateNum(DateTime.Now);
		LuckyDrawBuyTimes++;
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void SetBoughtVip()
	{
		isBoughtVip = true;
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void SetHuoDongTournamentTutorialShow(int idx)
	{
		isHuoDongTournamentTutorialShow[idx] = true;
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void SetButterFlyDailyPuzzleTutorial()
	{
		ButterFlyDailyPuzzleTutorial = 1;
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public bool IsTodayOpenHuoDong(int type)
	{
		switch (type)
		{
		case 0:
		{
			int dateNum2 = WordGlobal.GetDateNum(DateTime.Now);
			if (dateNum2 > HuoDongGardonOpenDayId)
			{
				return false;
			}
			return true;
		}
		case 1:
		{
			int dateNum = WordGlobal.GetDateNum(DateTime.Now);
			if (dateNum > HuoDongSunOpenDayId)
			{
				return false;
			}
			return true;
		}
		default:
			return false;
		}
	}

	public void OpenHuoDongUI(int type)
	{
		if (type == 0)
		{
			int dateNum = WordGlobal.GetDateNum(DateTime.Now);
			if (dateNum > HuoDongGardonOpenDayId)
			{
				HuoDongGardonOpenDayId = dateNum;
				PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
			}
		}
		if (type == 1)
		{
			int dateNum2 = WordGlobal.GetDateNum(DateTime.Now);
			if (dateNum2 > HuoDongSunOpenDayId)
			{
				HuoDongSunOpenDayId = dateNum2;
				PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
			}
		}
	}

	public void SetShowButterFlyTVCommonLevel(int level)
	{
		ShowButterFlyTVCommonLevel = level;
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void SetHuoDongTournamentDateID(int valu)
	{
		HuoDongTournamentDateID = valu;
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void setHuoDongTutorial02()
	{
		HuoDongTutorial02++;
		PlayerInfo.Instance.GetHuoDongSaveData().SetTutorial02(1);
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void setHuoDongGardonTutorial01()
	{
		HuoDongGardonTutorial01++;
		PlayerInfo.Instance.GetHuoDongGardonSaveData().SetTutorial(1);
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void setHuoDongSunTutorial02(int idx)
	{
		HuoDongSunTutorial02++;
		PlayerInfo.Instance.GetHuoDongSunSaveData().SetTutorial02();
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void setHuoDongWordFindTutorial02(int idx)
	{
		HuoDongWordFindTutorial02++;
		PlayerInfo.Instance.GetHuoDongWordFindSaveData().SetTutorial02();
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void setHuoDongTournamentTutorial01(int idx)
	{
		HuoDongTournamentTutorial01++;
		PlayerInfo.Instance.GetHuoDongTournamentSaveData().SetTutorial();
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void setHalfPayFirstTipTutorial()
	{
		HalfPayFirstTipTutorial = 1;
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void setgHalfPayFirstTipLastTime(int seconds)
	{
		double halfPayTime = WordGlobal.ConvertDateTimeToSecond(DateTime.Now.AddSeconds(seconds));
		SetHalfPayTime(halfPayTime);
		PlayerInfo.Instance.SavePlayerOtherInfoSaveData();
	}

	public void SetHalfPayTime(double time)
	{
		HalfPayFirstTipTimeStamp = time;
		if (time != 0)
		{
			HalfPayFirstTipTimeStamp_dt = WordGlobal.ConvertTimeStampToDateTime(HalfPayFirstTipTimeStamp);
		}
	}

	public double getHalfPayFirstTipLastTime()
	{
		double num = 0.0;
		if (HalfPayFirstTipTimeStamp > 0)
		{
			if (HalfPayFirstTipTimeStamp_dt.Year < 2000)
			{
				HalfPayFirstTipTimeStamp_dt = WordGlobal.ConvertTimeStampToDateTime(HalfPayFirstTipTimeStamp);
			}
			num = (HalfPayFirstTipTimeStamp_dt - DateTime.Now).TotalSeconds;
			if (num <= 0.0)
			{
				SetHalfPayTime(0L);
			}
		}
		return num;
	}

	public void SetHalfPayAllFirstLetterTimeByS(int seconds)
	{
		double halfPayAllFirstLetterTime = WordGlobal.ConvertDateTimeToSecond(DateTime.Now.AddSeconds(seconds));
		SetHalfPayAllFirstLetterTime(halfPayAllFirstLetterTime);
	}

	public void SetHalfPayAllFirstLetterTime(double time)
	{
		HalfPayAllFirstLettersTimeStamp = time;
		if (time != 0)
		{
			HalfPayAllFirstLettersTimeStamp_dt = WordGlobal.ConvertTimeStampToDateTime(HalfPayAllFirstLettersTimeStamp);
		}
	}

	public double getHalfPayAllFirstLetterLastTime()
	{
		double num = 0.0;
		if (HalfPayAllFirstLettersTimeStamp > 0)
		{
			if (HalfPayAllFirstLettersTimeStamp_dt.Year < 2000)
			{
				HalfPayAllFirstLettersTimeStamp_dt = WordGlobal.ConvertTimeStampToDateTime(HalfPayAllFirstLettersTimeStamp);
			}
			num = (HalfPayAllFirstLettersTimeStamp_dt - DateTime.Now).TotalSeconds;
			if (num <= 0.0)
			{
				SetHalfPayAllFirstLetterTime(0L);
			}
		}
		return num;
	}
}
