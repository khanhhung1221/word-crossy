using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PointItem : MonoBehaviour, PointAnimInfo
{
	public GameObject UnLocked;

	public Image UnLock_img;

	public GameObject Locked;

	public Text Level;

	public GameObject CoinIcon;

	public Image FriendIcon;

	public Image hd_iconImg;

	private int curLevel;

	private bool isShowCoin;

	private int HD_refreshIndex = -1;

	private bool isTempLock;

	private float delaytoshowEff;

	private GameObject eff_go;

	private void Start()
	{
		UnLock_img = UnLocked.GetComponent<Image>();
	}

	private void Update()
	{
		if (HD_refreshIndex != WordGlobal.HuoDongRefreshLevelMapIconIndex)
		{
			if (PlayerInfo.Instance.GetHuoDongSaveData().isShowIconInLevelMapLevels(curLevel))
			{
				if (!hd_iconImg.gameObject.activeInHierarchy)
				{
					string path = "Image/HuoDong/Christmas_snowicon";
					hd_iconImg.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>(path);
					hd_iconImg.gameObject.SetActive(value: true);
					CoinIcon.gameObject.SetActive(value: false);
				}
			}
			else if (hd_iconImg.gameObject.activeInHierarchy)
			{
				hd_iconImg.gameObject.SetActive(value: false);
				CoinIcon.SetActive(isShowCoin);
			}
			HD_refreshIndex = WordGlobal.HuoDongRefreshLevelMapIconIndex;
		}
		if (delaytoshowEff > 0f)
		{
			delaytoshowEff -= Time.deltaTime;
			if (delaytoshowEff <= 0f)
			{
				eff_go.SetActive(value: true);
			}
		}
	}

	public void RefreshLockedIcon()
	{
		if (!isTempLock)
		{
			bool flag = curLevel < PlayerInfo.Instance.MaxPlayLevel;
			UnLocked.SetActive(flag);
			Locked.SetActive(!flag);
		}
	}

	public void LockedItem()
	{
		isTempLock = true;
		UnLocked.SetActive(value: false);
		Locked.SetActive(value: true);
		if (UnLock_img == null)
		{
			UnLock_img = UnLocked.GetComponent<Image>();
		}
		Image unLock_img = UnLock_img;
		Color color = UnLock_img.color;
		float r = color.r;
		Color color2 = UnLock_img.color;
		float g = color2.g;
		Color color3 = UnLock_img.color;
		unLock_img.color = new Color(r, g, color3.b, 0f);
	}

	public void UnLockItemAnim(float delay_time, float time)
	{
		isTempLock = false;
		UnLocked.SetActive(value: true);
		UnLock_img.DOFade(1f, time).SetDelay(delay_time);
		string empty = string.Empty;
		empty = "LinkIconItemEff_small";
		delaytoshowEff = delay_time;
		string path = "UI/LevelMap/" + empty;
		eff_go = ResourceLoadManager.GetInstance().LoadUI(path, base.transform);
		eff_go.transform.localPosition = Vector3.zero;
		eff_go.transform.localScale = Vector3.one;
		(eff_go.transform as RectTransform).anchoredPosition = Vector2.zero;
		if (delaytoshowEff > 0f)
		{
			eff_go.SetActive(value: false);
		}
		else
		{
			eff_go.SetActive(value: true);
		}
	}

	public void SetData(bool isSel, int level)
	{
		curLevel = level;
		Level.text = level.ToString();
		if (level.ToString().Length >= 2)
		{
			Level.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
		}
		RefreshLockedIcon();
		isShowCoin = false;
		MapDataCsv mapData = MapDataManager.instance.GetMapData(level);
		if (level == mapData.LevelEndNum && level > PlayerInfo.Instance.MaxPlayLevel)
		{
			isShowCoin = true;
		}
		CoinIcon.SetActive(isShowCoin);
		FriendIcon.gameObject.SetActive(value: false);
	}

	public void SetCoinShow(bool isShow)
	{
		CoinIcon.SetActive(isShowCoin && isShow);
	}

	public void OnItemClick()
	{
		if (curLevel <= PlayerInfo.Instance.MaxPlayLevel && LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.ShowMainUI(curLevel);
		}
	}

	public void UnLockItemNow()
	{
		isTempLock = false;
		if (!(UnLocked == null) && !(UnLock_img == null))
		{
			UnLocked.SetActive(value: true);
			UnLock_img.color = Color.white;
			UnLock_img.DOKill();
			delaytoshowEff = 0f;
			if (eff_go != null)
			{
				eff_go.SetActive(value: false);
			}
		}
	}
}
