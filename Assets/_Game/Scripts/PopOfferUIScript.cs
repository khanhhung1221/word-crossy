using System;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class PopOfferUIScript : MonoBehaviour
{
	public static bool isShow;

	public Text m_TxtPrice;

	public Text m_TxtValue;

	public Text m_TxtCoins;

	public Text m_TxtTitle;

	public GameObject m_objBuyBtn;

	public GameObject m_objOfferPack;

	public GameObject m_objOffer;

	private static bool _count;

	private int _offerid;

	private void Start()
	{
	}

	private void Awake()
	{
		WordGlobal.InitLabel(base.transform);
	}

	private void Update()
	{
	}

	public void BtnBuyClick()
	{
		PayManager.GetInstance().m_CurrentOfferID = _offerid;
		PayManager.PaymentVerifyEvnet += PurchaseCompleted;
	}

	public static void ShowUI()
	{
		Debug.Log("ShowPopOfferUI");
		if (PlayerInfo.Instance.CurrentOfferItem != null && PlayerInfo.Instance.TutorialStep == 999)
		{
			OfferItem currentOfferItem = PlayerInfo.Instance.CurrentOfferItem;
			GameObject x = GameObject.Find("PopOfferUI");
			if (x == null)
			{
				x = ((currentOfferItem.Coins <= 0 || currentOfferItem.BulbCount + currentOfferItem.FigerCount + currentOfferItem.LightingCount <= 0) ? UIManager.Instance.LoadMainUI("UI/Shop/OfferItem", isPop: true) : UIManager.Instance.LoadMainUI("UI/Shop/OfferItemPack", isPop: true));
				x.name = "PopOfferUI";
				x.GetComponent<ShopOfferScript>().SetContent(currentOfferItem, isPop: true);
				isShow = true;
			}
		}
	}

	public void PurchaseCompleted(Product data)
	{
		PayManager.PaymentVerifyEvnet -= PurchaseCompleted;
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void Init()
	{
	}

	public int GetOffIndexByWeight(bool is3Day)
	{
		int num = 1;
		int max = (!is3Day) ? 63 : 91;
		int num2 = UnityEngine.Random.Range(1, max);
		if (is3Day)
		{
			if (num2 >= 1 && num2 <= 10)
			{
				num = 1;
			}
			else if (num2 >= 11 && num2 <= 40)
			{
				num = 2;
			}
			else if (num2 >= 41 && num2 <= 60)
			{
				num = 3;
			}
			else if (num2 >= 61 && num2 <= 75)
			{
				num = 4;
			}
			else if (num2 >= 76 && num2 <= 90)
			{
				num = 5;
			}
		}
		else if (num2 >= 1 && num2 <= 30)
		{
			num = 1;
		}
		else if (num2 >= 31 && num2 <= 50)
		{
			num = 2;
		}
		else if (num2 >= 51 && num2 <= 60)
		{
			num = 3;
		}
		else
		{
			switch (num2)
			{
			case 61:
				num = 4;
				break;
			case 62:
				num = 5;
				break;
			}
		}
		return num - 1;
	}

	public bool IsFirst3Days()
	{
		bool result = false;
		DateTime now = DateTime.Now;
		string[] array = PlayerInfo.Instance.FirstLoginDate.Split('_');
		if (array.Length >= 3)
		{
			if (now.Year > int.Parse(array[0]))
			{
				result = false;
			}
			else if (now.Month > int.Parse(array[1]))
			{
				result = false;
			}
			else if (now.Day > int.Parse(array[2]) && now.Day - int.Parse(array[2]) < 3)
			{
				result = true;
			}
		}
		return result;
	}

	private void OnDestroy()
	{
		isShow = false;
	}
}
