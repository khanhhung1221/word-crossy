using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProbabilityCalcutionUI : MonoBehaviour
{
	private int ItemCount;

	private int CyclesNum;

	public InputField CountInput;

	public InputField CysNumInput;

	public GameObject ItemPrefab;

	public GameObject TextPrefab;

	public GameObject ItemsContent;

	public GameObject TextContent;

	public List<InputField> ItemInputList;

	public List<Text> ResultList;

	public List<ProbabilityItem> ItemList;

	private void Start()
	{
		ItemCount = 0;
		CyclesNum = 0;
		ItemInputList = new List<InputField>();
		ResultList = new List<Text>();
		ItemList = new List<ProbabilityItem>();
	}

	private void Update()
	{
	}

	public int GetIntForString(string str, int errType = 1)
	{
		int result = 0;
		try
		{
			result = int.Parse(str);
			return result;
		}
		catch
		{
			switch (errType)
			{
			case 1:
				WordGlobal.DebugLogError("请输入正确数量");
				return result;
			case 2:
				WordGlobal.DebugLogError("请输入正确格式");
				return result;
			default:
				return result;
			}
		}
	}

	public void OnConfirmClick()
	{
		ItemCount = GetIntForString(CountInput.text);
		CyclesNum = GetIntForString(CysNumInput.text);
		CleanItems();
		for (int i = 0; i < ItemCount; i++)
		{
			GameObject gameObject = ResourceLoadManager.GetInstance().CreateObjForPrefab(ItemPrefab);
			gameObject.transform.SetParent(ItemsContent.transform, worldPositionStays: false);
			gameObject.SetActive(value: true);
			InputField component = gameObject.GetComponent<InputField>();
			ItemInputList.Add(component);
		}
	}

	public void OnStartBtnClick()
	{
		ItemList.Clear();
		int num = 0;
		Dictionary<string, ProbabilityItem> dictionary = new Dictionary<string, ProbabilityItem>();
		for (int i = 0; i < ItemCount; i++)
		{
			string[] array = ItemInputList[i].text.Split('_');
			if (array.Length >= 2)
			{
				ProbabilityItem probabilityItem = new ProbabilityItem();
				int num2 = int.Parse(array[1]);
				probabilityItem.ItemId = int.Parse(array[0]);
				probabilityItem.WeightStartNum = num + 1;
				probabilityItem.WeightEndNum = num + num2;
				ItemList.Add(probabilityItem);
				dictionary.Add(probabilityItem.WeightStartNum + "_" + probabilityItem.WeightEndNum, probabilityItem);
				num += num2;
			}
		}
		for (int j = 0; j < CyclesNum; j++)
		{
			int num3 = Random.Range(1, num + 1);
			for (int k = 0; k < ItemList.Count; k++)
			{
				ProbabilityItem probabilityItem2 = ItemList[k];
				if (num3 >= probabilityItem2.WeightStartNum && num3 <= probabilityItem2.WeightEndNum)
				{
					probabilityItem2.RadomNums++;
				}
			}
		}
		CleanResultItems();
		for (int l = 0; l < ItemCount; l++)
		{
			GameObject gameObject = ResourceLoadManager.GetInstance().CreateObjForPrefab(TextPrefab);
			gameObject.transform.SetParent(TextContent.transform, worldPositionStays: false);
			gameObject.SetActive(value: true);
			Text component = gameObject.GetComponent<Text>();
			component.text = ItemList[l].ItemId + " : " + ItemList[l].RadomNums;
			ResultList.Add(component);
		}
	}

	public void CleanItems()
	{
		int num = 0;
		while (num < ItemInputList.Count)
		{
			InputField inputField = ItemInputList[num];
			ItemInputList.Remove(inputField);
			Object.Destroy(inputField.gameObject);
		}
		ItemInputList.Clear();
	}

	public void CleanResultItems()
	{
		int num = 0;
		while (num < ResultList.Count)
		{
			Text text = ResultList[num];
			ResultList.Remove(text);
			Object.Destroy(text.gameObject);
		}
		ResultList.Clear();
	}

	public void OnCyclesInputFinish()
	{
		CyclesNum = GetIntForString(CysNumInput.text);
	}
}
