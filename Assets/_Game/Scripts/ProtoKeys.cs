using UnityEngine;

public class ProtoKeys : MonoBehaviour
{
	public const string KEY_token = "token";

	public const string KEY_userid = "userid";

	public const string KEY_seq = "seq";

	public const string KEY_version = "version";

	public const string KEY_cmds = "cmds";

	public const string KEY_cmd = "cmd";

	public const string KEY_error = "error";

	public const string KEY_error_msg = "errmsg";

	public const string KEY_error_no = "errno";

	public const string KEY_PKG_ERR_www = "EWWW";

	public const string KEY_PKG_ERR_token = "ETOKEN";

	public const string KEY_PKG_ERR_seq = "ESEQ";

	public const string KEY_PKG_ERR_version = "EVER";

	public const string KEY_PKG_ERR_data = "EDATA";

	public const string KEY_PKG_ERR_internal = "EINT";

	public const string KEY_RegisterCmd = "RegisterCmd";

	public const string KEY_LoginASCmd = "LoginASCmd";

	public const string KEY_LoginGSCmd = "LoginGSCmd";

	public const string KEY_CreatePlayerCmd = "CreatePlayerCmd";

	public const string KEY_GuestRegisterCmd = "GuestRegisterCmd";

	public const string KEY_username = "username";

	public const string KEY_pwd = "pwd";

	public const string KEY_phone_id = "phone_id";

	public const string KEY_nickname = "nickname";
}
