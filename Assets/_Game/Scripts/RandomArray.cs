using System;

public class RandomArray<T>
{
	private T[] item;

	public RandomArray(T[] obj)
	{
		item = new T[obj.Length];
		for (int i = 0; i < obj.Length; i++)
		{
			item[i] = obj[i];
		}
	}

	public Type ShowType()
	{
		return typeof(T);
	}

	public T[] GetItems()
	{
		return item;
	}

	public T[] GetDisruptedItems()
	{
		if (item.Length == 1)
		{
			return item;
		}
		T[] array = new T[item.Length];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = item[i];
		}
		Random random = new Random(DateTime.Now.Millisecond);
		for (int j = 0; j < array.Length; j++)
		{
			int num = random.Next(0, array.Length);
			int num2;
			do
			{
				num2 = random.Next(0, array.Length);
			}
			while (num2 == num);
			T val = array[num];
			array[num] = array[num2];
			array[num2] = val;
		}
		return array;
	}
}
