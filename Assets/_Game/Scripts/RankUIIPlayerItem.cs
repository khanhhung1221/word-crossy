using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankUIIPlayerItem : MonoBehaviour
{
	public Text m_TextRankNum;

	public Text PlayerNameText;

	public Text PlayerScoreNumText;

	public Image HeadIconImg;

	public GameObject SendBtnObj;

	public GameObject SendCompleteBtnObj;

	private RankItemInfo rankii;

	public void SetInfo(int index, RankItemInfo ri, bool isFriend = false, bool isShow = true)
	{
		if (isFriend)
		{
			SendBtnObj.SetActive(value: true);
		}
		rankii = ri;
		m_TextRankNum.text = index.ToString();
		PlayerScoreNumText.text = ri.ScoreNum.ToString();
		if (ri.FaceBookID != string.Empty)
		{
			PlayerNameText.text = ri.FaceBookName;
			StartCoroutine(Load("https://graph.facebook.com/" + ri.FaceBookID + "/picture?type=large"));
		}
		else
		{
			PlayerNameText.text = "ID" + ri.Uuid;
		}
		if (!isShow)
		{
			base.gameObject.SetActive(value: false);
		}
	}

	public void DownloadImg()
	{
		if (rankii.FaceBookID != string.Empty)
		{
			StartCoroutine(Load("https://graph.facebook.com/" + rankii.FaceBookID + "/picture?type=large"));
		}
	}

	private IEnumerator Load(string path)
	{
		WWW www = new WWW(path);
		yield return www;
		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.Log(www.error);
		}
		else if (www.isDone)
		{
			Texture2D texture = new Texture2D(512, 512);
			www.LoadImageIntoTexture(texture);
			Sprite sprite = Sprite.Create(texture, new Rect(0f, 0f, 200f, 200f), Vector2.zero);
			HeadIconImg.GetComponent<Image>().sprite = sprite;
		}
	}

	public void BtnSendClick()
	{
		Sendgift();
	}

	private void Sendgift()
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		jsonData["frienduuid"] = rankii.Uuid;
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["sendgift"] = Callback;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "sendgift", jsonData, dictionary);
	}

	private bool Callback(JsonData data)
	{
		return true;
	}

	private void Start()
	{
	}

	private void Update()
	{
	}
}
