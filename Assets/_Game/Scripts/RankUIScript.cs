using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankUIScript : MonoBehaviour
{
	public Transform WordTransRoot;

	public Transform FriendTransRoot;

	public GameObject m_ObjRank;

	public GameObject m_ObjReward;

	public GameObject WorldRankBtn;

	public GameObject WorldRankBgImg;

	public GameObject FriendRankBtn;

	public GameObject FriendRankBgImg;

	public GameObject WoridRankTrans;

	public GameObject FriendRankTrans;

	public Text ScoreNumText;

	public GameObject TipsObj;

	public Text RemainingTimeText;

	public Text MyRankText;

	private int LeftTimes;

	public ArrayList UIList;

	public int CurrentNum;

	public static RankUIScript Instance;

	private int WorldClickNums;

	private int FriendClickNums;

	private float itemHeight = 143f;

	private void Awake()
	{
		Instance = this;
		UIList = new ArrayList();
	}

	public void CheckNetwork()
	{
		switch (Application.internetReachability)
		{
		case NetworkReachability.NotReachable:
			TipsObj.SetActive(value: true);
			WoridRankTrans.SetActive(value: false);
			FriendRankTrans.SetActive(value: false);
			break;
		case NetworkReachability.ReachableViaCarrierDataNetwork:
			TipsObj.SetActive(value: false);
			break;
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			TipsObj.SetActive(value: false);
			break;
		}
	}

	public void BtnWorldRankClick()
	{
	}

	public void BtnFriendRankClick()
	{
	}

	private void Start()
	{
		GetWorldRank();
		CheckNetwork();
	}

	private void GetWorldRank()
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		jsonData["ranktype"] = 1;
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["getrank"] = CallbackWorldRank;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "getrank", jsonData, dictionary);
		GameObject gameObject = UIManager.Instance.LoadMainUI("UI/WaitLoadingUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
		WaitLoadingUI component = gameObject.GetComponent<WaitLoadingUI>();
		component.SetUIBg(isShowBlackBg: false);
	}

	private bool CallbackWorldRank(JsonData data)
	{
		if ((bool)WaitLoadingUI.instance)
		{
			WaitLoadingUI.instance.CloseUI();
		}
		if (!Instance)
		{
			return true;
		}
		LeftTimes = Convert.ToInt32(data["world_rank"]["next_update_time"].ToString());
		int num = LeftTimes / 86400;
		if (num > 0)
		{
			string arg = num + " " + TextLibraryDataManagerCsv.instance.GetText(1110);
			RemainingTimeText.text = TextLibraryDataManagerCsv.instance.GetText(1109) + " " + $"<color=#FF0000>{arg}</color>";
		}
		else
		{
			int num2 = LeftTimes % 86400;
			int num3 = num2 / 3600;
			if (num3 == 0)
			{
				num3 = 1;
			}
			string arg2 = num3 + " " + TextLibraryDataManagerCsv.instance.GetText(1111);
			RemainingTimeText.text = TextLibraryDataManagerCsv.instance.GetText(1109) + " " + $"<color=#FF0000>{arg2}</color>";
		}
		int num4 = Convert.ToInt32(data["world_rank"]["myrank"].ToString());
		MyRankText.text = TextLibraryDataManagerCsv.instance.GetText(1035) + " " + num4;
		JsonData list = data["world_rank"]["ranklist"];
		StartCoroutine(LoadObj(list));
		return true;
	}

	private IEnumerator LoadObj(JsonData list)
	{
		for (int index = 0; index < list.Count; index++)
		{
			object[] sysobj = JsonUtility.FromJson<object[]>(list[index].ToJson());
			RankItemInfo ri = new RankItemInfo
			{
				Uuid = (string)sysobj[0],
				ScoreNum = (int)sysobj[1],
				FaceBookID = (string)sysobj[2],
				FaceBookName = (string)sysobj[3]
			};
			GameObject obj = UIManager.Instance.LoadMainUI("UI/RankUIPlayerItem");
			obj.name = index.ToString();
			obj.transform.SetParent(WordTransRoot);
			RankUIIPlayerItem ru = obj.GetComponent<RankUIIPlayerItem>();
			UIList.Add(obj);
			if (index < 80)
			{
				ru.SetInfo(index + 1, ri);
			}
			else
			{
				ru.SetInfo(index + 1, ri, isFriend: false, isShow: false);
			}
			if (index % 100 == 0)
			{
				yield return null;
			}
		}
		CurrentNum = 40;
	}

	private void Update()
	{
		if (!Input.GetMouseButtonUp(0))
		{
			return;
		}
		Vector3 localPosition = WordTransRoot.localPosition;
		if (localPosition.y > 65f * itemHeight && CurrentNum < UIList.Count)
		{
			if (CurrentNum + 40 <= UIList.Count)
			{
				for (int i = CurrentNum; i < CurrentNum + 40; i++)
				{
					GameObject gameObject = (GameObject)UIList[i];
					gameObject.SetActive(value: true);
					RankUIIPlayerItem component = gameObject.GetComponent<RankUIIPlayerItem>();
					component.DownloadImg();
				}
			}
			if (CurrentNum - 80 >= 0)
			{
				for (int j = CurrentNum - 80; j < CurrentNum - 40; j++)
				{
					GameObject gameObject2 = (GameObject)UIList[j];
					gameObject2.SetActive(value: false);
				}
			}
			if (CurrentNum != 40)
			{
				Transform wordTransRoot = WordTransRoot;
				Vector3 localPosition2 = WordTransRoot.localPosition;
				float x = localPosition2.x;
				Vector3 localPosition3 = WordTransRoot.localPosition;
				float y = localPosition3.y - 40f * itemHeight;
				Vector3 localPosition4 = WordTransRoot.localPosition;
				wordTransRoot.localPosition = new Vector3(x, y, localPosition4.z);
			}
			CurrentNum += 40;
			return;
		}
		Vector3 localPosition5 = WordTransRoot.localPosition;
		if (!(localPosition5.y < 15f * itemHeight) || (((GameObject)UIList[0]).activeSelf && (CurrentNum == 80 || CurrentNum == 40)))
		{
			return;
		}
		if (CurrentNum - 80 - 40 >= 0)
		{
			for (int k = CurrentNum - 80 - 40; k < CurrentNum - 40 - 40; k++)
			{
				GameObject gameObject3 = (GameObject)UIList[k];
				gameObject3.SetActive(value: true);
				RankUIIPlayerItem component2 = gameObject3.GetComponent<RankUIIPlayerItem>();
				component2.DownloadImg();
			}
		}
		if (CurrentNum + 40 - 40 < UIList.Count)
		{
			for (int l = CurrentNum - 40; l < CurrentNum + 40 - 40; l++)
			{
				GameObject gameObject4 = (GameObject)UIList[l];
				gameObject4.SetActive(value: false);
			}
		}
		Transform wordTransRoot2 = WordTransRoot;
		Vector3 localPosition6 = WordTransRoot.localPosition;
		float x2 = localPosition6.x;
		Vector3 localPosition7 = WordTransRoot.localPosition;
		float y2 = localPosition7.y + 40f * itemHeight;
		Vector3 localPosition8 = WordTransRoot.localPosition;
		wordTransRoot2.localPosition = new Vector3(x2, y2, localPosition8.z);
		CurrentNum -= 40;
	}

	public void BtnRankClose()
	{
		UnityEngine.Object.Destroy(base.gameObject);
		WaitLoadingUI.instance.CloseUI();
		NetMill.Instance.ResetSendQueue();
	}

	public void BtnRewardClose()
	{
		BtnRankClose();
	}

	public void BtnReward()
	{
		m_ObjReward.SetActive(value: true);
		m_ObjRank.SetActive(value: false);
	}
}
