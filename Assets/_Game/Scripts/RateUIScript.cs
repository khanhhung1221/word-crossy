using UnityEngine;
using UnityEngine.UI;

public class RateUIScript : MonoBehaviour
{
	public GameObject[] arraryStarObj;

	private int _CurrentIndx;

	public Button RateBtn;

	private void Start()
	{
		RateBtn.interactable = false;
	}

	private void Update()
	{
	}

	public void BtnRateClick()
	{
		if (_CurrentIndx == 4)
		{
			MS_Interface.Current.OnRate();
		}
		else
		{
			UIManager.Instance.LoadMainUI("UI/ContactUsUI", isPop: true);
		}
		Object.Destroy(base.gameObject);
		PlayerInfo.Instance.IsAlreadyClickRateBtn = 1;
	}

	public void BtnClickStar1()
	{
		_CurrentIndx = 0;
		SetStarStateByIndex();
	}

	public void BtnClickStar2()
	{
		_CurrentIndx = 1;
		SetStarStateByIndex();
	}

	public void BtnClickStar3()
	{
		_CurrentIndx = 2;
		SetStarStateByIndex();
	}

	public void BtnClickStar4()
	{
		_CurrentIndx = 3;
		SetStarStateByIndex();
	}

	public void BtnClickStar5()
	{
		_CurrentIndx = 4;
		SetStarStateByIndex();
	}

	private void SetStarStateByIndex()
	{
		RateBtn.interactable = true;
		for (int i = 0; i < arraryStarObj.Length; i++)
		{
			if (i <= _CurrentIndx)
			{
				arraryStarObj[i].SetActive(value: true);
			}
			else
			{
				arraryStarObj[i].SetActive(value: false);
			}
		}
	}

	public void BtnCloseClick()
	{
		Object.Destroy(base.gameObject);
	}
}
