using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ResourceLoadManager
{
	public enum SP_LEVELMAP_TYPE
	{
		CICON,
		LICON,
		BG
	}

	public GameObject RefreshResObj;

	private static ResourceLoadManager instance;

	private Dictionary<string, Sprite> SpritePrefebList = new Dictionary<string, Sprite>();

	public List<CacheObj> m_PreFabList = new List<CacheObj>();

	public static ResourceLoadManager GetInstance()
	{
		if (instance == null)
		{
			instance = new ResourceLoadManager();
		}
		return instance;
	}

	private void Update()
	{
	}

	public GameObject LoadUI(string path, Transform parent)
	{
		GameObject gameObject = null;
		gameObject = LoadObjByPath(path);
		gameObject.transform.SetParent(parent, worldPositionStays: false);
		gameObject.SetActive(value: true);
		return gameObject;
	}

	public T LoadObjByPath<T>(string path) where T : UnityEngine.Object
	{
		string resName = "Assets/Game/Resources/" + path;
		T val = AssetBundleResourseLoad.Instance.LoadRes<T>(resName);
		if ((UnityEngine.Object)val == (UnityEngine.Object)null)
		{
			val = Resources.Load<T>(path);
		}
		return val;
	}

	public IEnumerator LoadSpriteAsync(string path, Image image, SP_LEVELMAP_TYPE type = SP_LEVELMAP_TYPE.CICON)
	{
		string www_path = "file://" + path;
		Debug.Log("Load " + www_path);
		WWW www = new WWW(www_path);
		try
		{
			yield return www;
			if (www.isDone && www.error == null)
			{
				if (www.texture.width <= 10 && www.texture.height <= 10)
				{
					if (type == SP_LEVELMAP_TYPE.CICON)
					{
						if (image != null)
						{
							image.sprite = LoadObjByPath<Sprite>("Image/LevelMap/map_default");
						}
						ThreadManager.Instance.LoadSprite(path);
					}
				}
				else if (image != null)
				{
					image.sprite = Sprite.Create(www.texture, new Rect(0f, 0f, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));
				}
			}
			else
			{
				if (type == SP_LEVELMAP_TYPE.CICON && image != null)
				{
					image.sprite = LoadObjByPath<Sprite>("Image/LevelMap/map_default");
				}
				Debug.Log(www.error);
			}
		}
		finally
		{
			//base._003C_003E__Finally0();
		}
	}

	public Sprite LoadSpriteByPath(string path, SP_LEVELMAP_TYPE type)
	{
		Sprite sprite = null;
		sprite = LoadObjByPath<Sprite>(path);
		if (sprite == null && ThreadManager.Instance != null)
		{
			string str = ".png";
			if (type == SP_LEVELMAP_TYPE.BG)
			{
				str = ".jpg";
			}
			string path2 = WordGlobal.DataPath + path + str;
			sprite = ThreadManager.Instance.LoadSprite(path2);
		}
		if (sprite == null)
		{
			switch (type)
			{
			case SP_LEVELMAP_TYPE.BG:
				sprite = LoadObjByPath<Sprite>("Image/LevelMap/bg_default");
				break;
			case SP_LEVELMAP_TYPE.CICON:
				sprite = LoadObjByPath<Sprite>("Image/LevelMap/map_default");
				break;
			case SP_LEVELMAP_TYPE.LICON:
				sprite = LoadObjByPath<Sprite>("Image/LevelMap/icon_default");
				break;
			}
		}
		return sprite;
	}

	public void PreInitObj(string path)
	{
		LoadObjByPath(path, isInstantiate: false);
	}

	private GameObject GetObjByKey(string path, bool isInstantiate = true)
	{
		string key = path.ToString();
		CacheObj cacheObj = m_PreFabList.Find((CacheObj x) => x.key == key);
		if (cacheObj != null)
		{
			cacheObj.count++;
			cacheObj.tick = DateTime.Now.Ticks;
			if (!isInstantiate)
			{
				return cacheObj.obj;
			}
			return UnityEngine.Object.Instantiate(cacheObj.obj);
		}
		GameObject gameObject = LoadObjByPath<GameObject>(key);
		if (gameObject != null)
		{
			cacheObj = new CacheObj();
			cacheObj.key = key;
			cacheObj.count = 1;
			cacheObj.obj = gameObject;
			cacheObj.tick = DateTime.Now.Ticks;
			m_PreFabList.Add(cacheObj);
			if (m_PreFabList.Count > WordGlobal.G_MaxObjCacheCount)
			{
				m_PreFabList.Sort((CacheObj x, CacheObj y) => x.tick.CompareTo(y.tick));
				m_PreFabList.RemoveAt(m_PreFabList.Count - 1);
			}
			if (isInstantiate)
			{
				return UnityEngine.Object.Instantiate(gameObject);
			}
		}
		return gameObject;
	}

	public GameObject LoadObjByPath(string path, bool isInstantiate = true)
	{
		return GetObjByKey(path, isInstantiate);
	}

	public GameObject CreateObjForPrefab(GameObject prefab)
	{
		if ((bool)prefab)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(prefab);
			gameObject.SetActive(value: true);
			return gameObject;
		}
		return null;
	}

	public GameObject LoadPrefabObj(string path)
	{
		return LoadObjByPath(path, isInstantiate: false);
	}

	public void Open_ResultTipUI(int textId, ResultTipUIManager.TipsType type, string buyBtnText = "", int cost = 0, WordGlobal.OkBtnCall click = null, object parse = null, bool showCloseBtn = true, int sureBtnTextId = 1000006, WordGlobal.OkBtnCall closeBtnCall = null)
	{
		WordGlobal.DebugLog("Open result tip ui");
		string path = "UI/TipsUI_Normal";
		GameObject gameObject = GameObject.Find("TipsUI_Normal");
		if (gameObject == null)
		{
			gameObject = LoadUI(path, null);
			gameObject.name = "TipsUI_Normal";
		}
		ResultTipUIManager component = gameObject.GetComponent<ResultTipUIManager>();
		component.ShowUI(textId, type, buyBtnText, cost, click, parse, showCloseBtn, sureBtnTextId, closeBtnCall);
	}

	public void DestroyUIObj(GameObject obj)
	{
		if (!(obj == null))
		{
			UnityEngine.Object.Destroy(obj);
		}
	}

	public void ShowTips(int id)
	{
		ShowTips(WordGlobal.ReadText(id));
	}

	public void ShowTips(string txt)
	{
		GameObject gameObject = GameObject.Find("CommTips");
		if (gameObject == null)
		{
			gameObject = UIManager.Instance.LoadMainUI("UI/CommTips");
			gameObject.name = "CommTips";
		}
		gameObject.GetComponent<TipsScript>().ShowTips(txt);
	}

	public void ShowQuestTips(int id)
	{
		TipsQuestScript.Quest_id = id;
		GameObject gameObject = GameObject.Find("CommQuestTips");
		if (gameObject == null)
		{
			gameObject = UIManager.Instance.LoadMainUI("UI/CommQuestTips");
			gameObject.name = "CommQuestTips";
		}
		gameObject.GetComponent<TipsQuestScript>().ShowTips();
	}

	public void ShowNetWaiting(float hide_time = 0f)
	{
		if (!GameObject.Find("NetWaiting"))
		{
			GameObject gameObject = UIManager.Instance.LoadMainUI("UI/NetWaiting", isPop: true);
			gameObject.name = "NetWaiting";
			NetWaiting.HideTime = hide_time;
		}
	}

	public void HideNetWaiting()
	{
		GameObject gameObject = GameObject.Find("NetWaiting");
		if (gameObject != null)
		{
			DestroyUIObj(gameObject);
		}
	}

	public void ShowGotoTipsUI()
	{
		GameObject x = GameObject.Find("GotoTipsUI");
		if (x == null)
		{
			x = UIManager.Instance.LoadMainUI("UI/Tips_GotoUI");
			x.name = "GotoTipsUI";
		}
	}

	public void ShowConfirmTipsUI(ConfirmUIScript.ConfirmType type = ConfirmUIScript.ConfirmType.Type_Quit, int textId = 1044, int comfirmBtnTextId = 1043, UnityAction call = null)
	{
		GameObject gameObject = GameObject.Find("ConfirmUI");
		if (gameObject == null)
		{
			gameObject = UIManager.Instance.LoadMainUI("UI/ConfirmUI", isPop: true);
			gameObject.name = "ConfirmUI";
		}
		gameObject.GetComponent<ConfirmUIScript>().ShowUI(type, textId, comfirmBtnTextId, call);
	}

	public void ShowNoBtnTipsUI(int textId)
	{
		GameObject gameObject = GameObject.Find("TipsUI_NoBtn");
		if (gameObject == null)
		{
			gameObject = UIManager.Instance.LoadMainUI("UI/TipsUI_NoBtn", isPop: true);
			gameObject.name = "TipsUI_NoBtn";
		}
		gameObject.GetComponent<NoBtnTipsUIScript>().ShowUI(textId);
	}
}
