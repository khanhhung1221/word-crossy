using UnityEngine;
using UnityEngine.UI;

public class ResultTipUIManager : MonoBehaviour
{
	public enum TipsType
	{
		TYPE_NULL = 0,
		TYPE_1 = 1,
		TYPE_21 = 21
	}

	public Text ShowText;

	public GameObject SureBtn;

	public GameObject SureBuyBtn;

	public GameObject CloseBtn;

	public object ClickEventParas;

	private TipsType curType;

	public static event WordGlobal.OkBtnCall ClickBtnEvent;

	public static event WordGlobal.OkBtnCall CloseBtnEvent;

	public void ShowUI(string text, TipsType type, string buyBtnText, int costNum, WordGlobal.OkBtnCall click = null, object parse = null, bool showCloseBtn = true, int sureBtnTextId = 1000006, WordGlobal.OkBtnCall closeCallback = null)
	{
		ShowText.text = text;
		curType = type;
		SureBtn.transform.Find("Text").GetComponent<Text>().text = TextLibraryDataManagerCsv.instance.GetText(sureBtnTextId);
		if (curType == TipsType.TYPE_1)
		{
			SureBtn.SetActive(value: true);
			SureBuyBtn.SetActive(value: false);
		}
		else if (curType == TipsType.TYPE_21)
		{
			SureBuyBtn.SetActive(value: true);
			SureBtn.SetActive(value: false);
			SureBuyBtn.transform.Find("Text").GetComponent<Text>().text = buyBtnText;
			SureBuyBtn.transform.Find("Cost").GetComponent<Text>().text = costNum.ToString();
		}
		ResultTipUIManager.CloseBtnEvent = closeCallback;
		ResultTipUIManager.ClickBtnEvent = click;
		ClickEventParas = parse;
		base.gameObject.SetActive(value: true);
		CloseBtn.SetActive(showCloseBtn);
		if (!showCloseBtn)
		{
			WordGlobal.DisableBackKey = true;
		}
	}

	public void ShowUI(int textId, TipsType type, string buyBtnText, int costNum, WordGlobal.OkBtnCall click = null, object parse = null, bool showCloseBtn = true, int sureBtnTextId = 1000006, WordGlobal.OkBtnCall closeCallback = null)
	{
		string text = TextLibraryDataManagerCsv.instance.GetText(textId);
		ShowUI(text, type, buyBtnText, costNum, click, parse, showCloseBtn, sureBtnTextId, closeCallback);
	}

	public void OnClickBg()
	{
		if (ResultTipUIManager.CloseBtnEvent != null)
		{
			ResultTipUIManager.CloseBtnEvent(null);
		}
		HideUI();
	}

	public void OnClickSureBtn()
	{
		if (ResultTipUIManager.ClickBtnEvent != null)
		{
			ResultTipUIManager.ClickBtnEvent(ClickEventParas);
		}
		HideUI();
	}

	public void OnClickSureBuyBtn()
	{
		if (curType == TipsType.TYPE_21)
		{
			HideUI();
		}
	}

	public void HideUI()
	{
		WordGlobal.DisableBackKey = false;
		curType = TipsType.TYPE_NULL;
		ResultTipUIManager.ClickBtnEvent = null;
		ClickEventParas = null;
		ResourceLoadManager.GetInstance().DestroyUIObj(base.gameObject);
	}
}
