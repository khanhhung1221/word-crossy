using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScrollControlScript : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
{
	public static UnityAction<int> OnEndMove;

	public int m_ItemCount;

	public int m_CurrentIndex;

	private int _targetIndex;

	public float m_NextLimit = 0.2f;

	public float m_Sensitive;

	public Scrollbar m_ScrollBar;

	public GameObject m_ObjPreBtn;

	public GameObject m_ObjNextBtn;

	public GameObject m_ObjDot;

	public Transform m_TfDotRoot;

	private bool _canMove;

	private float _oldValue;

	private float _targetPosition = 1f;

	private float _targetPositionTmp = 1f;

	private Vector2 _oldPosition;

	private float _moveSpeed = 0.2f;

	private const float SMOOTH_TIME = 0.2f;

	private float _pageWidth;

	private float _dragParam;

	private bool _isInit;

	private bool _isLeft;

	private void Start()
	{
	}

	private void Awake()
	{
	}

	private void Update()
	{
	}

	public void Init()
	{
		_dragParam = 1f / (float)(m_ItemCount - 1) * m_Sensitive;
		_pageWidth = 1f / (float)(m_ItemCount - 1);
		m_ScrollBar.value = _pageWidth * (float)(m_CurrentIndex - 1);
		_targetIndex = m_CurrentIndex;
		_oldValue = m_ScrollBar.value;
		InitBtn();
		InitDots();
		_isInit = true;
		SetDotIndex(m_CurrentIndex - 1);
	}

	private void LateUpdate()
	{
		if (_isInit)
		{
			_isInit = false;
			GetComponent<ScrollRect>().content.localPosition = new Vector2(-(m_CurrentIndex - 1) * 608 - 304, 0f);
		}
		if (!_canMove)
		{
			return;
		}
		if (Mathf.Abs(m_ScrollBar.value - _targetPosition) < 0.01f)
		{
			m_ScrollBar.value = _targetPosition;
			m_CurrentIndex = _targetIndex;
			_canMove = false;
			_oldValue = m_ScrollBar.value;
			InitBtn();
			SetDotIndex(GetCurrentIndex(m_ScrollBar.value) - 1);
			if (OnEndMove != null)
			{
				OnEndMove(GetCurrentIndex(m_ScrollBar.value));
			}
		}
		else
		{
			m_ScrollBar.value = Mathf.SmoothDamp(m_ScrollBar.value, _targetPosition, ref _moveSpeed, 0.2f);
		}
	}

	private void InitBtn()
	{
		m_ObjPreBtn.SetActive(GetCurrentIndex(m_ScrollBar.value) > 1);
		m_ObjNextBtn.SetActive(GetCurrentIndex(m_ScrollBar.value) < m_ItemCount);
	}

	public void PrePage()
	{
		if (!_canMove && m_CurrentIndex > 0)
		{
			if (m_CurrentIndex == m_ItemCount)
			{
				_targetIndex--;
			}
			_targetIndex--;
			_targetPosition = (float)_targetIndex * _pageWidth;
			if (GetCurrentIndex(_targetPosition) == GetCurrentIndex(m_ScrollBar.value))
			{
				_targetIndex--;
				_targetPosition -= _pageWidth;
			}
			_canMove = true;
			InitBtn();
		}
	}

	public void NextPage()
	{
		if (!_canMove && m_CurrentIndex < m_ItemCount)
		{
			if (m_CurrentIndex == 0)
			{
				_targetIndex++;
			}
			_targetIndex++;
			_targetPosition = (float)(_targetIndex - 1) * _pageWidth;
			if (GetCurrentIndex(_targetPosition) == GetCurrentIndex(m_ScrollBar.value))
			{
				_targetIndex++;
				_targetPosition += _pageWidth;
			}
			_canMove = true;
			InitBtn();
		}
	}

	private void SetDotIndex(int index)
	{
		for (int i = 0; i < m_ItemCount; i++)
		{
			if (i == index)
			{
				m_TfDotRoot.GetChild(i).GetComponent<Image>().color = WordGlobal.ChangeStringToColor("#FFFFFF");
			}
			else
			{
				m_TfDotRoot.GetChild(i).GetComponent<Image>().color = WordGlobal.ChangeStringToColor("#9674F0");
			}
		}
	}

	private void InitDots()
	{
		for (int i = 0; i < m_ItemCount; i++)
		{
			GameObject gameObject = Object.Instantiate(m_ObjDot);
			gameObject.SetActive(value: true);
			gameObject.transform.SetParent(m_TfDotRoot, worldPositionStays: false);
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		_oldValue = m_ScrollBar.value;
		m_CurrentIndex = GetCurrentIndex(_oldValue);
		_targetIndex = m_CurrentIndex;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		float num = m_ScrollBar.value - _oldValue;
		if (Mathf.Abs(num / _pageWidth) > m_NextLimit)
		{
			_targetIndex += ((num > 0f) ? 1 : (-1));
			_targetPosition = (float)(_targetIndex - 1) * _pageWidth;
			if (_targetPosition < 0f)
			{
				_targetPosition = 0f;
			}
		}
		if (num != 0f)
		{
			_canMove = true;
		}
	}

	public void OnDrag(Vector2 pos)
	{
	}

	private int GetCurrentIndex(float pCurrentValue)
	{
		return Mathf.RoundToInt(pCurrentValue / _pageWidth + 1f);
	}

	private void DragScreen(Vector2 pDragVector)
	{
		float value = m_ScrollBar.value;
		m_ScrollBar.value -= pDragVector.x / (float)Screen.width * _dragParam;
		_moveSpeed = m_ScrollBar.value - value;
	}

	private void GotoIndex(int index)
	{
	}
}
