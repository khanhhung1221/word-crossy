using System;
using System.Collections.Generic;

[Serializable]
public class SectionItem
{
	public int SectionID;

	public string Title;

	public List<SetsItem> Sets;

	public List<SetsItem> HiddenSets;

	public SectionItem()
	{
		Sets = new List<SetsItem>();
		HiddenSets = new List<SetsItem>();
	}
}
