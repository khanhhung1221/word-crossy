using System;
using System.Collections.Generic;

[Serializable]
public class SectionsDataList
{
	public List<SectionItem> Sections;

	public List<SetsItem> DailyLevels;

	public SectionsDataList()
	{
		Sections = new List<SectionItem>();
		DailyLevels = new List<SetsItem>();
	}

	public List<string> GetAllDailyLevelDataList()
	{
		int num = 0;
		List<string> list = new List<string>();
		for (int i = 0; i < DailyLevels.Count; i++)
		{
			for (int j = 0; j < DailyLevels[i].Levels.Count; j++)
			{
				string levelString = DailyLevels[i].Levels[j].GetLevelString();
				list.Add(levelString);
				num = ((DailyLevels[i].Levels[j].LevelID <= num) ? num : DailyLevels[i].Levels[j].LevelID);
			}
		}
		return list;
	}

	public List<string> GetAllSetsDataList()
	{
		int num = 0;
		List<string> list = new List<string>();
		for (int i = 0; i < Sections.Count; i++)
		{
			for (int j = 0; j < Sections[i].Sets.Count; j++)
			{
				for (int k = 0; k < Sections[i].Sets[j].Levels.Count; k++)
				{
					string levelString = Sections[i].Sets[j].Levels[k].GetLevelString();
					list.Add(levelString);
					num = ((Sections[i].Sets[j].Levels[k].LevelID <= num) ? num : Sections[i].Sets[j].Levels[k].LevelID);
				}
			}
		}
		return list;
	}

	public List<string> GetAllHiddenSetsDataList()
	{
		int num = 0;
		List<string> list = new List<string>();
		for (int i = 0; i < Sections.Count; i++)
		{
			for (int j = 0; j < Sections[i].HiddenSets.Count; j++)
			{
				for (int k = 0; k < Sections[i].HiddenSets[j].Levels.Count; k++)
				{
					string levelString = Sections[i].HiddenSets[j].Levels[k].GetLevelString();
					list.Add(levelString);
					num = ((Sections[i].HiddenSets[j].Levels[k].LevelID <= num) ? num : Sections[i].HiddenSets[j].Levels[k].LevelID);
				}
			}
		}
		return list;
	}
}
