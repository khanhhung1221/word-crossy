using System;
using System.Collections.Generic;

[Serializable]
public class SetsItem
{
	public int SetID;

	public int SectionID;

	public string SetName;

	public string SetFullName;

	public string TopText;

	public string BottomText;

	public List<LevelItem> Levels;

	public SetsItem()
	{
		Levels = new List<LevelItem>();
	}
}
