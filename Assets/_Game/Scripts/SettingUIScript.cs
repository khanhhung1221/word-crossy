using System;
using UnityEngine;
using UnityEngine.UI;

public class SettingUIScript : MonoBehaviour
{
	public static int BackType;

	public Transform MusicTrans;

	public Transform SoundTrans;

	public Transform NotiTrans;

	public Text BackText;

	public Text FaceBookText;

	public Text VersionText;

	public GameObject restore_iosBtn;

	public RectTransform Down_rect;

	public RectTransform bg_rect;

	public Text PrivacyLinkBtn_txt;

	public Text TermsLinkBtn_txt;

	private int version_click_count;

	private void OnDestroy()
	{
		BackType = 0;
	}

	private void Start()
	{
		if ((bool)MainUIScript.Instance || DPCollectPicUIScript.isShow || BackType != 0)
		{
			BackText.text = TextLibraryDataManagerCsv.instance.GetText(1030);
		}
		else
		{
			BackText.text = TextLibraryDataManagerCsv.instance.GetText(1041);
		}
		PrivacyLinkBtn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1074);
		TermsLinkBtn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1073);
		if (WordFaceBook.Instance.CheckFBIsLoggedIn())
		{
			FaceBookText.text = TextLibraryDataManagerCsv.instance.GetText(1052);
		}
		else
		{
			FaceBookText.text = TextLibraryDataManagerCsv.instance.GetText(1054);
		}
		SetBtnState(MusicTrans, AudioControl.isMusicOpen);
		SetBtnState(SoundTrans, AudioControl.isSoundEffOpen);
		if (PlayerInfo.Instance.NotificationKey == 0)
		{
			SetBtnState(NotiTrans, isOpen: false);
		}
		else
		{
			SetBtnState(NotiTrans, isOpen: true);
		}
		VersionText.text = TextLibraryDataManagerCsv.instance.GetText(1121) + " : " + Application.version;
		IOSSet();
	}

	public void IOSSet()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
		{
			restore_iosBtn.SetActive(value: true);
			Down_rect.anchoredPosition = new Vector2(0f, -115f);
			RectTransform rectTransform = bg_rect;
			Vector2 anchoredPosition = bg_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, -52f);
			RectTransform rectTransform2 = bg_rect;
			Vector2 sizeDelta = bg_rect.sizeDelta;
			rectTransform2.sizeDelta = new Vector2(sizeDelta.x, 1090f);
		}
	}

	private void Update()
	{
	}

	public void BtnIOSRestore()
	{
		PayManager.GetInstance().RestoreIap();
	}

	public void BtnMusicClick()
	{
		BtnClickEffect(MusicTrans);
	}

	public void BtnSoundClick()
	{
		BtnClickEffect(SoundTrans);
	}

	public void BtnNotificationClick()
	{
		BtnClickEffect(NotiTrans);
	}

	private void SetBtnState(Transform trans, bool isOpen)
	{
		Transform component = trans.Find("OFFTrans").GetComponent<Transform>();
		Transform component2 = trans.Find("ONTrans").GetComponent<Transform>();
		if (isOpen)
		{
			trans.GetComponent<Image>().sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Setting/togle_on");
			component.gameObject.SetActive(value: false);
			component2.gameObject.SetActive(value: true);
		}
		else
		{
			trans.GetComponent<Image>().sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Setting/togle_off");
			component.gameObject.SetActive(value: true);
			component2.gameObject.SetActive(value: false);
		}
	}

	private void BtnClickEffect(Transform trans)
	{
		Transform component = trans.Find("OFFTrans").GetComponent<Transform>();
		Transform component2 = trans.Find("ONTrans").GetComponent<Transform>();
		if (component.gameObject.activeSelf)
		{
			trans.GetComponent<Image>().sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Setting/togle_on");
			component.gameObject.SetActive(value: false);
			component2.gameObject.SetActive(value: true);
			if (trans == MusicTrans)
			{
				AudioControl.GetInstance().SetBgVudio(isOpen: true);
			}
			else if (trans == SoundTrans)
			{
				AudioControl.GetInstance().SetEffVudio(isOpen: true);
			}
			else if (trans == NotiTrans)
			{
				PlayerInfo.Instance.SetRepeatingNotification();
				PlayerInfo.Instance.Set18HourNotification();
				PlayerInfo.Instance.NotificationKey = 1;
			}
		}
		else
		{
			trans.GetComponent<Image>().sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Setting/togle_off");
			component.gameObject.SetActive(value: true);
			component2.gameObject.SetActive(value: false);
			if (trans == MusicTrans)
			{
				AudioControl.GetInstance().SetBgVudio(isOpen: false);
			}
			else if (trans == SoundTrans)
			{
				AudioControl.GetInstance().SetEffVudio(isOpen: false);
			}
			else if (trans == NotiTrans)
			{
				//UnityEngine.iOS.LocalNotification.CancelAllNotification();
				Debug.Log("Tat notify");
				PlayerInfo.Instance.NotificationKey = 0;
			}
		}
	}

	public void BtnFaceBookCLick()
	{
		if (WordFaceBook.Instance.CheckFBIsLoggedIn())
		{
			WordFaceBook.Instance.FaceBookLogOut();
		}
		else
		{
			WordFaceBook.Instance.FaceBookLogin();
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void BtnHowtoplayClick()
	{
		UIManager.Instance.LoadMainUI("UI/HelpUI", isPop: true);
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void BtnContactUsClick()
	{
		MS_Interface.Current.OnRate();
		//string text = "wordslide@outlook.com";
		//string text2 = Escape("Word Slide - Free Word Games & Crossword Puzzle");
		//string text3 = Escape("\n\n\n\n\n===========================\nUTCTime: " + DateTime.UtcNow + "\nGame: Word Slide - Free Word Games & Crossword Puzzle\nGameVersion: " + Application.version + "\nPlayerID: " + PlayerInfo.Instance.UUID + "\nLanguage: EN\nDeviceType: " + SystemInfo.deviceModel + "\nOSVersion: " + SystemInfo.operatingSystem + "\n===========================");
		//Application.OpenURL("mailto:" + text + "?subject=" + text2 + "&body=" + text3);
	}

	private string Escape(string url)
	{
		return WWW.EscapeURL(url).Replace("+", "%20");
	}

	public void BtnCloseClick()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void BtnBackClick()
	{
		if ((bool)MainUIScript.Instance)
		{
			if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Common)
			{
				MainUIScript.Instance.BtnCloseClick();
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_Daily)
			{
				if (GameObject.Find("DPCollectPicUI") == null)
				{
					MainUIScript.Instance.BackToPuzzleCollect();
				}
				else
				{
					DPCollectPicUIScript.isCloseUI = true;
					if (MainUIScript.Instance != null)
					{
						MainUIScript.Instance.BtnCloseClick();
					}
				}
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDGardon)
			{
				MainUIScript.Instance.BtnCloseClick();
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDSun)
			{
				MainUIScript.Instance.BtnCloseClick();
			}
			else if (UIManager.CurMainUIType == UIManager.MainUIType.Type_HDTournament)
			{
				MainUIScript.Instance.BtnCloseClick();
			}
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else if (DPCollectPicUIScript.isShow)
		{
			DPCollectPicUIScript.isCloseUI = true;
			if (MainUIScript.Instance != null)
			{
				MainUIScript.Instance.BtnCloseClick();
			}
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else if (BackType == 1)
		{
			HuoDongWordSearch.isCloseClick = true;
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else if (BackType == 2)
		{
			HuoDongWordSearchUI.isCloseClick = true;
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else if (BackType == 3)
		{
			HuoDongWordFind.isBackKey = true;
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else
		{
			ResourceLoadManager.GetInstance().ShowConfirmTipsUI();
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void BtnPrivacyClick()
	{
		MS_Interface.Current.OnPrivacy();
	}

	public void BtnTermsClick()
	{
		MS_Interface.Current.OnPrivacy();
	}

	public void BtnVersionClick()
	{
		version_click_count++;
		if (version_click_count >= 7)
		{
			PlayerInfo.Instance.SetShowTestLog();
			version_click_count = 0;
		}
	}
}
