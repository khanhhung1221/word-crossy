using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopOfferItem : MonoBehaviour
{
	public Image m_ImgIcon;

	public Text m_TxtNumber;

	public List<Sprite> m_Icons;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void SetContent(int type, string number)
	{
		m_ImgIcon.sprite = m_Icons[type];
		m_TxtNumber.text = number;
	}
}
