using System;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class ShopOfferScript : MonoBehaviour
{
	public GameObject m_objBuyBtn;

	public GameObject more_go;

	public Text m_TxtMore;

	public Text m_TxtCoins;

	public GameObject m_objAds;

	public Image m_ImgIcon;

	public Image m_ImgBigIcon;

	public GameObject m_ObjMask;

	public Text m_TxtPrice;

	public Text m_TxtTitle;

	public Transform m_TfItemroot;

	public GameObject m_ObjItem;

	public Text m_TxtLeftTime;

	public GameObject m_ObjPopular;

	public GameObject m_ObjBest;

	private int _offerid;

	private bool _isPop;

	private void Start()
	{
	}

	private void Awake()
	{
		if (PlayerInfo.Instance.AdRemove == 1)
		{
			HideAds();
		}
		if (!(m_objBuyBtn == null))
		{
			return;
		}
        Product product = CodelessIAPStoreListener.Instance.GetProduct(base.transform.GetComponent<IAPButton>().productId);
        if (product != null)
        {
            OfferItem offerItem = PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.Productid == base.transform.GetComponent<IAPButton>().productId);
            if (product.availableToPurchase)
            {
                m_TxtPrice.text = WordGlobal.GetPrice(product.metadata.localizedPriceString);
            }
            else
            {
                m_TxtPrice.text = offerItem.Price.ToString();
            }
            if (offerItem == null)
            {
                Debug.LogError("item is null productId:" + base.transform.GetComponent<IAPButton>().productId);
            }
            int num = 0;
            if (offerItem.Coins > 0)
            {
                num = offerItem.Coins;
            }
            _offerid = offerItem.ID;
            if (m_TxtCoins != null)
            {
                m_TxtCoins.text = num.ToString();
            }
        }
    }

	private void Update()
	{
		if (m_TxtLeftTime != null)
		{
			if (PlayerInfo.Instance.OfferEndTime < DateTime.Now)
			{
				UnityEngine.Object.Destroy(base.gameObject);
			}
			m_TxtLeftTime.text = WordGlobal.ChangeTimeToNomral((long)(PlayerInfo.Instance.OfferEndTime - DateTime.Now).TotalSeconds);
		}
	}

	public void SetContent(OfferItem item, bool isPop = false)
	{
		_isPop = isPop;
		m_TxtCoins.text = item.Coins.ToString();
		if (m_TxtMore != null)
		{
			if (more_go != null)
			{
				more_go.SetActive(value: false);
			}
			int num = int.Parse(item.Value);
			if (num > 0)
			{
				m_TxtMore.text = item.Value + "% " + TextLibraryDataManagerCsv.instance.GetText(1116);
				if (more_go != null)
				{
					more_go.SetActive(value: true);
				}
			}
			else
			{
				m_TxtMore.text = string.Empty;
			}
		}
		if (m_objAds != null)
		{
			m_objAds.SetActive(item.AdsOff && PlayerInfo.Instance.AdRemove == 0);
		}
		//m_objBuyBtn.GetComponent<IAPButton>().productId = item.Productid;
		//m_objBuyBtn.GetComponent<IAPButton>().enabled = true;
		//Product product = CodelessIAPStoreListener.Instance.GetProduct(item.Productid);
		//if (product != null)
		//{
		//	m_TxtPrice.text = WordGlobal.GetPrice(product.metadata.localizedPriceString);
		//}
		//else
		{
			m_TxtPrice.text = item.Price.ToString();
		}
		if (m_TxtTitle != null)
		{
			m_TxtTitle.text = WordGlobal.ReadText(item.Title);
		}
		_offerid = item.ID;
		if (m_ImgBigIcon != null)
		{
			m_ImgBigIcon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/" + item.Icon);
			m_ImgBigIcon.SetNativeSize();
		}
		if ((item.Type == 2 || item.Type == 1) && item.Coins > 0)
		{
			if (item.BulbCount > 0)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate(m_ObjItem);
				gameObject.transform.SetParent(m_TfItemroot, worldPositionStays: false);
				gameObject.SetActive(value: true);
				gameObject.GetComponent<ShopOfferItem>().SetContent(0, item.BulbCount.ToString());
			}
			if (item.LightingCount > 0)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate(m_ObjItem);
				gameObject2.transform.SetParent(m_TfItemroot, worldPositionStays: false);
				gameObject2.SetActive(value: true);
				gameObject2.GetComponent<ShopOfferItem>().SetContent(1, item.LightingCount.ToString());
			}
			if (item.FigerCount > 0)
			{
				GameObject gameObject3 = UnityEngine.Object.Instantiate(m_ObjItem);
				gameObject3.transform.SetParent(m_TfItemroot, worldPositionStays: false);
				gameObject3.SetActive(value: true);
				gameObject3.GetComponent<ShopOfferItem>().SetContent(2, item.FigerCount.ToString());
			}
		}
		else if (item.BulbCount > 0)
		{
			m_ImgIcon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_lightbulb");
			m_TxtCoins.text = item.BulbCount.ToString();
		}
		else if (item.FigerCount > 0)
		{
			m_ImgIcon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_finger");
			m_TxtCoins.text = item.FigerCount.ToString();
		}
		else if (item.LightingCount > 0)
		{
			m_ImgIcon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/shop_icon_lightning");
			m_TxtCoins.text = item.LightingCount.ToString();
		}
		if (item.Type == 3)
		{
			m_ImgIcon.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/" + item.Icon);
			if (item.ID == 19)
			{
				m_ObjPopular.SetActive(value: true);
			}
			if (item.ID == 21)
			{
				m_ObjBest.SetActive(value: true);
			}
		}
	}

	public void HideAds()
	{
		if (m_objAds != null)
		{
			m_objAds.SetActive(value: false);
		}
	}

	public void BtnClick()
	{
		PayManager.GetInstance().m_CurrentOfferID = _offerid;
		PayManager.PaymentVerifyEvnet += PurchaseCompleted;
		PayManager.PaymentVerifyFailEvnet += PurchaseFaild;
		WordGlobal.DisableBackKey = true;
		if (m_ObjMask != null)
		{
			m_ObjMask.SetActive(value: true);
		}
	}

	public void PurchaseFaild()
	{
		PayManager.PaymentVerifyEvnet -= PurchaseCompleted;
		PayManager.PaymentVerifyFailEvnet -= PurchaseFaild;
		m_ObjMask.SetActive(value: false);
	}

	public void PurchaseCompleted(Product data)
	{
		WordGlobal.DisableBackKey = false;
		PayManager.PaymentVerifyEvnet -= PurchaseCompleted;
		PayManager.PaymentVerifyFailEvnet -= PurchaseFaild;
		if (m_ObjMask != null)
		{
			m_ObjMask.SetActive(value: false);
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	private void OnDestroy()
	{
		if (_isPop)
		{
		}
	}
}
