using UnityEngine;
using UnityEngine.UI;

public class ShopOfferVip : MonoBehaviour
{
	public Text more_txt;

	public Text des_txt;

	public Text btn_txt;


	public void Init(OfferItem item)
	{
        try
        {
			more_txt.text = item.Value + "% <size=36>more</size>";
		}
        catch
        {
			Debug.LogError("Error: null");
		}
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1312);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1313);
	}

	public void BtnClick()
	{
		ShopUIScript.isHide = true;
		WordGlobal.LogEvent("VIP_Tap_Shop");
		if (GameObject.Find("VipUI") == null)
		{
			UIManager.Instance.LoadMainUI("UI/Vip/VipUI");
		}
	}
}
