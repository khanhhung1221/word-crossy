using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShopUIScript : MonoBehaviour
{
	public static bool WaitToInit;

	public static bool isHide;

	public GameObject m_ObjWaiting;

	public GameObject m_ObjPack;

	public GameObject m_ObjPackOffer;

	public GameObject m_ObjOffer;

	public GameObject m_ObjOfferVIP;

	public Transform m_TfRoot;

	public Transform m_TfSampleItems;

	public GameObject m_ObjAll;

	public GameObject m_RemoveAdsItem;

	public GameObject m_FreeCoinsItem;

	public GameObject m_ObjBaseItem;

	public RectTransform m_RtfTop;

	public RectTransform m_RtfSV;

	public GameObject JiaZai_go;

	public Transform JiaZai_yTran;

	public GameObject[] RemoveAdsObj;

	private bool _isWatchVideo;

	private bool HideNeedShowBanner;

	private void Awake()
	{
		m_ObjWaiting.SetActive(value: false);
		WordGlobal.InitLabel(base.transform);
		WordGlobal.ShowUIAni(base.transform);
		m_RtfTop.localPosition = new Vector3(0f, 0f - WordGlobal.GetSaftTopSize(), 0f);
		RectTransform rtfSV = m_RtfSV;
		Vector2 offsetMax = m_RtfSV.offsetMax;
		float x = offsetMax.x;
		Vector2 offsetMax2 = m_RtfSV.offsetMax;
		rtfSV.offsetMax = new Vector2(x, offsetMax2.y - WordGlobal.GetSaftTopSize());
	}

	private void Start()
	{
		if (AdsManager.Instance.isShowBanner)
		{
			HideNeedShowBanner = true;
			AdsManager.Instance.HideBannerAD();
		}

        if (PlayerInfo.Instance.AdRemove == 1)
        {
			Debug.Log("Remove Ads");
			DisableRemoveAdsObj();
		}
		
	}

    private void DisableRemoveAdsObj()
    {
		foreach(GameObject obj in RemoveAdsObj)
        {
			obj.SetActive(false);
		}

	}

	private void InitBaseItem()
	{
		if (PlayerInfo.Instance.AdRemove == 0)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(m_RemoveAdsItem);
			gameObject.transform.SetParent(m_TfSampleItems, worldPositionStays: false);
		}
		List<OfferItem> list = PlayerInfo.Instance.OfferList.FindAll((OfferItem x) => x.Type == 3 && x.Productid != "wc.removeads");
		list.Sort((OfferItem x, OfferItem y) => x.Price.CompareTo(y.Price));
		for (int i = 0; i < list.Count; i++)
		{
			GameObject gameObject2 = UnityEngine.Object.Instantiate(m_ObjBaseItem);
			gameObject2.transform.SetParent(m_TfSampleItems, worldPositionStays: false);
			gameObject2.GetComponent<ShopOfferScript>().SetContent(list[i]);
		}
	}

	public void InitOffer()
	{
		JiaZai_go.SetActive(value: false);
		OfferItem currentOfferItem = PlayerInfo.Instance.CurrentOfferItem;
		if (currentOfferItem != null)
		{
			m_TfRoot.gameObject.SetActive(value: true);
			GameObject gameObject = (currentOfferItem.Coins <= 0 || currentOfferItem.BulbCount + currentOfferItem.FigerCount + currentOfferItem.LightingCount <= 0) ? UnityEngine.Object.Instantiate(m_ObjOffer) : UnityEngine.Object.Instantiate(m_ObjPackOffer);
			gameObject.transform.SetParent(m_TfRoot, worldPositionStays: false);
			gameObject.GetComponent<ShopOfferScript>().SetContent(currentOfferItem);
		}
		if (PlayerInfo.Instance.IsShowVip())
		{
			OfferItem item = PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.Type == 5);
			GameObject gameObject2 = UnityEngine.Object.Instantiate(m_ObjOfferVIP);
			gameObject2.transform.SetParent(m_TfRoot, worldPositionStays: false);
			gameObject2.GetComponent<ShopOfferVip>().Init(item);
		}
		List<OfferItem> list = PlayerInfo.Instance.OfferList.FindAll((OfferItem x) => x.Type == 2);
		list.Sort((OfferItem x, OfferItem y) => x.Price.CompareTo(y.Price));
		if (list.Count != 0)
		{
			m_TfRoot.gameObject.SetActive(value: true);
			for (int i = 0; i < list.Count; i++)
			{
				GameObject gameObject3 = UnityEngine.Object.Instantiate(m_ObjPack);
				gameObject3.transform.SetParent(m_TfRoot, worldPositionStays: false);
				gameObject3.GetComponent<ShopOfferScript>().SetContent(list[i]);
			}
		}
	}

	public void InitDeals()
	{
		m_RemoveAdsItem.SetActive(PlayerInfo.Instance.AdRemove == 0);
		bool flag = AdsManager.Instance.IncentivizedAdState && PlayerInfo.Instance.LastOfferPayValue == 0f;
		flag = false;
		m_FreeCoinsItem.SetActive(flag);
		if (flag)
		{
			WordGlobal.LogEvent("Shop_FreeCoin_Show");
		}
	}

	public void HideAds()
	{
		for (int i = 0; i < m_TfRoot.childCount; i++)
		{
			m_TfRoot.GetChild(i).GetComponent<ShopOfferScript>().HideAds();
		}
		for (int j = 0; j < m_TfSampleItems.childCount; j++)
		{
			if (m_TfSampleItems.GetChild(j).GetComponent<ShopOfferScript>() != null)
			{
				m_TfSampleItems.GetChild(j).GetComponent<ShopOfferScript>().HideAds();
			}
		}
	}

	public void PurchaseCompleted(string ProductId)
	{
		m_ObjWaiting.SetActive(value: false);
        switch (ProductId) {
			case "wc.vip":
				PlayerInfo.Instance.AdRemove = 1;
				DisableRemoveAdsObj();
				ResourceLoadManager.GetInstance().ShowTips(string.Format(WordGlobal.ReadText(1339), 400));
				break;
			case "wc.removeads":
				PlayerInfo.Instance.AdRemove = 1;
				DisableRemoveAdsObj();
				ResourceLoadManager.GetInstance().ShowTips(1058);
				break;
			case "wc.coinpack1":
				getCoin(450);
				break;
			case "wc.coinpack2":
				getCoin(1350);
				break;
			case "wc.coinpack3":
				getCoin(3150);
				break;
			case "wc.coinpack4":
				getCoin(7200);
				break;
			case "wc.coinpack5":
				getCoin(20250);
				DisableRemoveAdsObj();
				break;
			case "wc.coinpack6":
				getCoin(45000);
				DisableRemoveAdsObj();
				break;
		}
		PlayerInfo.Instance.LastOfferPayValue = 1;
		//WordFaceBook.Instance.UpdateUserdataMill();
	}

    private void getCoin(int value)
    {
		PlayerInfo.Instance.AddGold(value, 15);
		ResourceLoadManager.GetInstance().ShowTips(string.Format(WordGlobal.ReadText(1056), value));
	}

	public void PurchaseFaild()
	{
		m_ObjWaiting.SetActive(value: false);
	}

	public void BtnBuyClick()
	{
		WordGlobal.DisableBackKey = true;
		m_ObjWaiting.SetActive(value: true);
	}

	public void BtnGetFreeCoinsClick()
	{
		if (AdsManager.Instance.ShowIncentivizedAd("Shop_FreeCoin_Tap"))
		{
			AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
		}
	}

	private void RewardADEvent(bool ret)
	{
		AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Remove(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(RewardADEvent));
		if (ret)
		{
			CommonTipsScript.Instance.ShowUI(string.Format(WordGlobal.ReadText(1112), PlayerInfo.Instance.systemConfig.WatchAdGold), 0, WordGlobal.ReadText(1016), delegate(object obj)
			{
				Vector3 starPos = (Vector3)obj;
				GoldScript goldScript = UnityEngine.Object.FindObjectOfType<GoldScript>();
				goldScript.CollectGold(starPos, 5, 1f, null);
				goldScript.AddGold((int)PlayerInfo.Instance.systemConfig.WatchAdGold, 1f, 1f, isAdd2PlayerInfo: true, 17);
			}, showClose: false);
		}
		_isWatchVideo = true;
		BtnClose();
	}

	public void BtnClose()
	{
		WordGlobal.CloseUIAni(base.transform);
		WordGlobal.LogEvent("Tap_CloseShop");
	}

	private void OnDestroy()
	{
		if (PlayerInfo.Instance.LastOfferPayValue == 0f && AdsManager.Instance.IncentivizedAdState && !_isWatchVideo)
		{
			UIManager.Instance.isShowPopNow = true;
			CommonTipsScript.Instance.ShowUI(string.Format(WordGlobal.ReadText(1017), 25), 1, WordGlobal.ReadText(1018), delegate
			{
				if (AdsManager.Instance.ShowIncentivizedAd("Adpop_Watch"))
				{
					AdsManager.ShowRewardVideoEvent = (UnityAction<bool>)Delegate.Combine(AdsManager.ShowRewardVideoEvent, new UnityAction<bool>(AdsManager.Instance.WatchVideoEvent));
				}
			});
		}
		if (MainUIScript.Instance != null && WordGlobal.isMainUIShowBanner)
		{
			AdsManager.Instance.ShowBannerAD();
		}
		if (HideNeedShowBanner)
		{
			AdsManager.Instance.ShowBannerAD();
			HideNeedShowBanner = false;
		}
	}
}
