using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteAni : MonoBehaviour
{
	public delegate void delegateMovieEvent();

	public string movieName;

	public List<Sprite> lSprites;

	public int fps = 30;

	private bool isStop;

	private Image shower;

	private int curFrame;

	private float fDelta;

	private Dictionary<int, List<delegateMovieEvent>> dMovieEvents = new Dictionary<int, List<delegateMovieEvent>>();

	public float SpriteWidth
	{
		get
		{
			if (shower == null)
			{
				return 0f;
			}
			return shower.rectTransform.rect.width;
		}
	}

	public float SpriteHeight
	{
		get
		{
			if (shower == null)
			{
				return 0f;
			}
			return shower.rectTransform.rect.height;
		}
	}

	public int FrameCount => lSprites.Count;

	private void Awake()
	{
		shower = GetComponent<Image>();
		if (string.IsNullOrEmpty(movieName))
		{
			movieName = "movieName";
		}
	}

	private void Start()
	{
		Play(curFrame);
	}

	public void Play(int iFrame)
	{
		if (lSprites.Count == 0)
		{
			return;
		}
		if (iFrame >= FrameCount)
		{
			iFrame = 0;
		}
		if (!(lSprites[iFrame] == null))
		{
			shower.sprite = lSprites[iFrame];
			curFrame = iFrame;
			if (dMovieEvents.ContainsKey(iFrame))
			{
				foreach (delegateMovieEvent item in dMovieEvents[iFrame])
				{
					item();
				}
			}
		}
	}

	private void Update()
	{
		if (!isStop && lSprites.Count != 0)
		{
			fDelta += Time.deltaTime;
			if (fDelta > 1f / (float)fps)
			{
				fDelta = 0f;
				curFrame++;
				Play(curFrame);
			}
		}
	}

	public void StopAni()
	{
		isStop = true;
	}

	public void RestartAni()
	{
		isStop = false;
		curFrame = 0;
		Play(curFrame);
	}

	public void RegistMovieEvent(int frame, delegateMovieEvent delEvent)
	{
		if (!dMovieEvents.ContainsKey(frame))
		{
			dMovieEvents.Add(frame, new List<delegateMovieEvent>());
		}
		dMovieEvents[frame].Add(delEvent);
	}

	public void UnregistMovieEvent(int frame, delegateMovieEvent delEvent)
	{
		if (dMovieEvents.ContainsKey(frame) && dMovieEvents[frame].Contains(delEvent))
		{
			dMovieEvents[frame].Remove(delEvent);
		}
	}

	public void SetSprites(List<Sprite> list)
	{
		lSprites.Clear();
		lSprites = list;
	}
}
