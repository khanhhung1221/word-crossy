using System;

[Serializable]
public class SystemConfig
{
	public long TipsPrice = 100L;

	public long ClickTipsPrice = 200L;

	public long FirstLetterTipsPrice = 300L;

	public long WatchAdGold = 25L;

	public long DailyRewardWatchAdGlod = 50L;

	public int FirstLetterTipsCount = 5;

	public long GetTipsPrice()
	{
		if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().getHalfPayFirstTipLastTime() > 0.0)
		{
			return TipsPrice / 2;
		}
		return TipsPrice;
	}

	public long GetFirstLetterTipsPrice()
	{
		if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().getHalfPayAllFirstLetterLastTime() > 0.0)
		{
			return FirstLetterTipsPrice / 2;
		}
		return FirstLetterTipsPrice;
	}
}
