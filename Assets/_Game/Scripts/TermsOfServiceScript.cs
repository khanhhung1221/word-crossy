using UnityEngine;

public class TermsOfServiceScript : MonoBehaviour
{
	private void Start()
	{
	}

	private void Awake()
	{
		WordGlobal.InitLabel(base.transform);
	}

	private void Update()
	{
	}

	public void OpenUrl(string txt)
	{
		Application.OpenURL(txt);
	}

	public void BtnAgree()
	{
		Object.Destroy(base.gameObject);
	}
}
