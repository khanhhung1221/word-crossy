using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class Test : MonoBehaviour
{
	public class WordCsvTmp
	{
		public List<string> words;

		public string lvl;

		public List<string> bonus;

		public string moneyword;

		public WordCsvTmp(string[] values)
		{
			int num = 0;
			lvl = values[num++];
			words = WordGlobal.StrToList(values[num++]);
			moneyword = values[num++];
			bonus = WordGlobal.StrToList(values[num++]);
		}
	}

	private List<WordCsvTmp> ItemList = new List<WordCsvTmp>();

	private string _filePath = "/_Game/Resources/Data/";

	private void Start()
	{
		Init(Resources.Load<TextAsset>("Data/word").bytes);
	}

	public void TTTT()
	{
		string text = Resources.Load<TextAsset>("Data/incorrect words").text;
		List<string> list = WordGlobal.StrToList(text);
		Debug.Log("count:" + list.Count);
		for (int i = 0; i < list.Count; i++)
		{
			string tttt = list[i].ToUpper();
			List<WordCsvTmp> list2 = ItemList.FindAll((WordCsvTmp x) => x.bonus.FindIndex((string y) => y == tttt) >= 0);
			if (list2.Count > 0)
			{
				for (int j = 0; j < list2.Count; j++)
				{
					list2[j].bonus.Remove(tttt);
					Debug.LogError("Lvl:" + list2[j].lvl + ";bonus:" + tttt);
				}
			}
			List<WordCsvTmp> list3 = ItemList.FindAll((WordCsvTmp x) => x.words.FindIndex((string y) => y == tttt) >= 0);
			if (list3.Count > 0)
			{
				for (int k = 0; k < list3.Count; k++)
				{
					list3[k].words.Remove(tttt);
					Debug.LogError("Lvl:" + list3[k].lvl + ";words:" + tttt);
				}
			}
			List<WordCsvTmp> list4 = ItemList.FindAll((WordCsvTmp x) => x.moneyword == tttt);
			if (list4.Count > 0)
			{
				for (int l = 0; l < list4.Count; l++)
				{
					list4[l].moneyword = string.Empty;
					Debug.LogError("Lvl:" + list4[l].lvl + ";moneywords:" + tttt);
				}
			}
		}
		WriteWords();
	}

	private void WriteWords()
	{
		using (StreamWriter streamWriter = new StreamWriter(Application.dataPath + _filePath + "wordnew.csv", append: true))
		{
			foreach (WordCsvTmp item in ItemList)
			{
				string value = item.lvl + ",\"" + WordGlobal.ListToStr(item.words) + "\"," + item.moneyword + ",\"" + WordGlobal.ListToStr(item.bonus) + "\"";
				streamWriter.WriteLine(value);
			}
		}
	}

	public void Init(byte[] file, string source_encoding = "UTF-8")
	{
		MemoryStream stream = new MemoryStream(file);
		int num = 2;
		using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
		{
			IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					string[] values = (string[])enumerator.Current;
					if (num > 0)
					{
						num--;
					}
					else
					{
						WordCsvTmp item = new WordCsvTmp(values);
						ItemList.Add(item);
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}
	}
}
