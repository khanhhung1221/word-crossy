using UnityEngine;
using UnityEngine.UI;

public class TestDownload : MonoBehaviour
{
	private string url1 = "http://s3-us-west-2.amazonaws.com/lettuce-game/words/Image/LevelMap/bg_0_1.jpg";

	private string url2 = "http://s3-us-west-2.amazonaws.com/lettuce-game/words/Image/LevelMap/bg_0_2.jpg";

	public Text textUrl1;

	public Text textProgress1;

	public Text textUrl2;

	public Text textProgress2;

	public bool isTest;

	private void Start()
	{
	}

	public void Update()
	{
		if (isTest)
		{
			StartDownload1();
			StartDownload2();
			isTest = false;
		}
	}

	public void StartDownload1()
	{
		string[] array = url1.Split('/');
		string str = array[array.Length - 1];
		_DownloadHandler downloadHandler = DownlaodFile._Instance.StartDownload(url1, "d:/" + str);
		if (downloadHandler != null)
		{
			downloadHandler.RegisteProgressBack(Progress1);
			downloadHandler.RegisteReceiveTotalLengthBack(Total);
			downloadHandler.RegisteCompleteBack(Complete);
		}
	}

	public void StartDownload2()
	{
		string[] array = url2.Split('/');
		string str = array[array.Length - 1];
		_DownloadHandler downloadHandler = DownlaodFile._Instance.StartDownload(url2, Application.dataPath + "/../" + str);
		if (downloadHandler != null)
		{
			downloadHandler.RegisteProgressBack(Progress2);
			downloadHandler.RegisteReceiveTotalLengthBack(Total);
			downloadHandler.RegisteCompleteBack(Complete);
		}
	}

	public void StopDownload1()
	{
		DownlaodFile._Instance.StopDownload(url1);
	}

	public void StopDownload2()
	{
		DownlaodFile._Instance.StopDownload(url2);
	}

	private void Complete(string path)
	{
		Debug.Log("下载完成，文件路径=>" + path);
	}

	private void Total(int length)
	{
		Debug.Log("要下载的文件总大小=>" + length + "字节");
	}

	private void Progress1(float progress)
	{
		if (textProgress1 != null)
		{
			textProgress1.text = "进度:" + (progress * 100f).ToString("0.00") + "%";
		}
	}

	private void Progress2(float progress)
	{
		if (textProgress1 != null)
		{
			textProgress2.text = "进度:" + (progress * 100f).ToString("0.00") + "%";
		}
	}
}
