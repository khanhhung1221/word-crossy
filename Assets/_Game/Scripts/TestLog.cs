using System;
using System.Collections.Generic;
using UnityEngine;

public class TestLog : MonoBehaviour
{
	public static bool isDestroy;

	public bool AllowDebugging = true;

	public static int Count;

	public GUIStyle scroll_style;

	private DebugType _debugType;

	private List<LogData> _logInformations = new List<LogData>();

	private int _currentLogIndex = -1;

	private int _infoLogCount;

	private int _warningLogCount;

	private int _errorLogCount;

	private int _fatalLogCount;

	private bool _showInfoLog = true;

	private bool _showWarningLog;

	private bool _showErrorLog = true;

	private bool _showFatalLog = true;

	private Vector2 _scrollLogView = Vector2.zero;

	private Vector2 _scrollCurrentLogView = Vector2.zero;

	private Vector2 _scrollSystemView = Vector2.zero;

	private bool _expansion;

	private Rect _windowRect = new Rect(0f, 0f, 100f, 60f);

	private int _fps;

	private Color _fpsColor = Color.white;

	private int _frameNumber;

	private float _lastShowFPSTime;

	private float w_rate = 1f;

	private Rect Shrink_Rect;

	private int count = 1;

	private void Start()
	{
		isDestroy = false;
		Debug.unityLogger.logEnabled = true;
		if (Count == 0)
		{
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			AllowDebugging = true;
		}
		else
		{
			AllowDebugging = false;
		}
		Count++;
		if (AllowDebugging)
		{
			Application.logMessageReceived += LogHandler;
		}
		if (Screen.width > 600)
		{
			w_rate = (float)Screen.width / 600f;
		}
		Shrink_Rect = new Rect(0f, 0f, 140f * w_rate, 140f * w_rate);
		_windowRect = Shrink_Rect;
		Debug.Log("w_rate:" + w_rate);
	}

	private void Update()
	{
		if (isDestroy)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		if (AllowDebugging)
		{
			_frameNumber++;
			float num = Time.realtimeSinceStartup - _lastShowFPSTime;
			if (num >= 1f)
			{
				_fps = (int)((float)_frameNumber / num);
				_frameNumber = 0;
				_lastShowFPSTime = Time.realtimeSinceStartup;
			}
		}
	}

	private void OnDestory()
	{
		if (AllowDebugging)
		{
			Application.logMessageReceived -= LogHandler;
		}
	}

	private void LogHandler(string condition, string stackTrace, LogType type)
	{
		LogData item = default(LogData);
		item.time = DateTime.Now.ToString("HH:mm:ss");
		item.message = condition;
		item.stackTrace = stackTrace;
		switch (type)
		{
		case LogType.Assert:
			item.type = "Fatal";
			_fatalLogCount++;
			break;
		case LogType.Error:
		case LogType.Exception:
			item.type = "Error";
			_errorLogCount++;
			break;
		case LogType.Warning:
			item.type = "Warning";
			_warningLogCount++;
			break;
		case LogType.Log:
			item.type = "Info";
			_infoLogCount++;
			break;
		}
		_logInformations.Add(item);
		if (_warningLogCount > 0)
		{
			_fpsColor = Color.yellow;
		}
		if (_errorLogCount > 0)
		{
			_fpsColor = Color.red;
		}
	}

	private void OnGUI()
	{
		if (AllowDebugging)
		{
			int fontSize = (int)(20f * w_rate);
			GUI.skin.window.fontSize = fontSize;
			GUI.skin.button.fontSize = fontSize;
			GUI.skin.label.fontSize = fontSize;
			GUI.skin.textArea.fontSize = fontSize;
			GUI.skin.textField.fontSize = fontSize;
			GUI.skin.toggle.fontSize = fontSize;
			if (count == 1)
			{
				scroll_style = new GUIStyle(GUI.skin.verticalScrollbar);
				count = 0;
			}
			if (_expansion)
			{
				_windowRect = GUI.Window(0, _windowRect, ExpansionGUIWindow, "DEBUGGER");
			}
			else
			{
				_windowRect = GUI.Window(0, _windowRect, ShrinkGUIWindow, "DEBUGGER");
			}
		}
	}

	private void ExpansionGUIWindow(int windowId)
	{
		GUI.DragWindow(new Rect(0f, 0f, 10000f, 40f * w_rate));
		GUILayout.BeginArea(new Rect(0f, 40f * w_rate, 600f * w_rate, 600f * w_rate));
		GUILayout.BeginHorizontal();
		GUI.contentColor = _fpsColor;
		if (GUILayout.Button("FPS:" + _fps, GUILayout.Height(30f * w_rate)))
		{
			_expansion = false;
			_windowRect.width = 140f * w_rate;
			_windowRect.height = 140f * w_rate;
		}
		GUI.contentColor = ((_debugType != 0) ? Color.gray : Color.white);
		if (GUILayout.Button("Console", GUILayout.Height(30f * w_rate)))
		{
			_debugType = DebugType.Console;
		}
		GUI.contentColor = ((_debugType != DebugType.Memory) ? Color.gray : Color.white);
		if (GUILayout.Button("Memory", GUILayout.Height(30f * w_rate)))
		{
			_debugType = DebugType.Memory;
		}
		GUI.contentColor = ((_debugType != DebugType.System) ? Color.gray : Color.white);
		if (GUILayout.Button("System", GUILayout.Height(30f * w_rate)))
		{
			_debugType = DebugType.System;
		}
		GUI.contentColor = ((_debugType != DebugType.Screen) ? Color.gray : Color.white);
		if (GUILayout.Button("Screen", GUILayout.Height(30f * w_rate)))
		{
			_debugType = DebugType.Screen;
		}
		GUI.contentColor = ((_debugType != DebugType.Quality) ? Color.gray : Color.white);
		if (GUILayout.Button("Quality", GUILayout.Height(30f * w_rate)))
		{
			_debugType = DebugType.Quality;
		}
		GUI.contentColor = ((_debugType != DebugType.Environment) ? Color.gray : Color.white);
		if (GUILayout.Button("Environment", GUILayout.Height(30f * w_rate)))
		{
			_debugType = DebugType.Environment;
		}
		GUI.contentColor = Color.white;
		GUILayout.EndHorizontal();
		if (_debugType == DebugType.Console)
		{
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Clear"))
			{
				_logInformations.Clear();
				_fatalLogCount = 0;
				_warningLogCount = 0;
				_errorLogCount = 0;
				_infoLogCount = 0;
				_currentLogIndex = -1;
				_fpsColor = Color.white;
			}
			GUI.contentColor = ((!_showInfoLog) ? Color.gray : Color.white);
			_showInfoLog = GUILayout.Toggle(_showInfoLog, "Info [" + _infoLogCount + "]");
			GUI.contentColor = ((!_showWarningLog) ? Color.gray : Color.white);
			_showWarningLog = GUILayout.Toggle(_showWarningLog, "Warning [" + _warningLogCount + "]");
			GUI.contentColor = ((!_showErrorLog) ? Color.gray : Color.white);
			_showErrorLog = GUILayout.Toggle(_showErrorLog, "Error [" + _errorLogCount + "]");
			GUI.contentColor = ((!_showFatalLog) ? Color.gray : Color.white);
			_showFatalLog = GUILayout.Toggle(_showFatalLog, "Fatal [" + _fatalLogCount + "]");
			GUI.contentColor = Color.white;
			GUILayout.EndHorizontal();
			scroll_style.fixedWidth = 50f * w_rate;
			_scrollLogView = GUILayout.BeginScrollView(_scrollLogView, scroll_style, scroll_style, GUILayout.Height(430f * w_rate));
			for (int i = 0; i < _logInformations.Count; i++)
			{
				bool flag = false;
				Color contentColor = Color.white;
				LogData logData = _logInformations[i];
				switch (logData.type)
				{
				case "Fatal":
					flag = _showFatalLog;
					contentColor = Color.red;
					break;
				case "Error":
					flag = _showErrorLog;
					contentColor = Color.red;
					break;
				case "Info":
					flag = _showInfoLog;
					contentColor = Color.white;
					break;
				case "Warning":
					flag = _showWarningLog;
					contentColor = Color.yellow;
					break;
				}
				if (flag)
				{
					GUILayout.BeginHorizontal();
					GUI.contentColor = contentColor;
					bool value = _currentLogIndex == i;
					string[] obj = new string[5]
					{
						"[",
						null,
						null,
						null,
						null
					};
					LogData logData2 = _logInformations[i];
					obj[1] = logData2.type;
					obj[2] = "] \n[";
					LogData logData3 = _logInformations[i];
					obj[3] = logData3.time;
					obj[4] = "] ";
					if (GUILayout.Toggle(value, string.Concat(obj)))
					{
						_currentLogIndex = i;
					}
					LogData logData4 = _logInformations[i];
					GUILayout.Label(logData4.message);
					GUILayout.FlexibleSpace();
					GUI.contentColor = Color.white;
					GUILayout.EndHorizontal();
				}
			}
			GUILayout.EndScrollView();
			_scrollCurrentLogView = GUILayout.BeginScrollView(_scrollCurrentLogView, scroll_style, scroll_style, GUILayout.Height(50f * w_rate));
			if (_currentLogIndex != -1)
			{
				LogData logData5 = _logInformations[_currentLogIndex];
				string message = logData5.message;
				LogData logData6 = _logInformations[_currentLogIndex];
				GUILayout.Label(message + "\r\n\r\n" + logData6.stackTrace);
			}
			GUILayout.EndScrollView();
		}
		else if (_debugType == DebugType.Memory)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label("Memory Information");
			GUILayout.EndHorizontal();
			GUILayout.BeginVertical("Box");
			GUILayout.EndVertical();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("卸载未使用的资源"))
			{
				Resources.UnloadUnusedAssets();
			}
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("使用GC垃圾回收"))
			{
				GC.Collect();
			}
			GUILayout.EndHorizontal();
		}
		else if (_debugType == DebugType.System)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label("System Information");
			GUILayout.EndHorizontal();
			_scrollSystemView = GUILayout.BeginScrollView(_scrollSystemView, "Box");
			GUILayout.Label("操作系统：" + SystemInfo.operatingSystem);
			GUILayout.Label("系统内存：" + SystemInfo.systemMemorySize + "MB");
			GUILayout.Label("处理器：" + SystemInfo.processorType);
			GUILayout.Label("处理器数量：" + SystemInfo.processorCount);
			GUILayout.Label("显卡：" + SystemInfo.graphicsDeviceName);
			GUILayout.Label("显卡类型：" + SystemInfo.graphicsDeviceType);
			GUILayout.Label("显存：" + SystemInfo.graphicsMemorySize + "MB");
			GUILayout.Label("显卡标识：" + SystemInfo.graphicsDeviceID);
			GUILayout.Label("显卡供应商：" + SystemInfo.graphicsDeviceVendor);
			GUILayout.Label("显卡供应商标识码：" + SystemInfo.graphicsDeviceVendorID);
			GUILayout.Label("设备模式：" + SystemInfo.deviceModel);
			GUILayout.Label("设备名称：" + SystemInfo.deviceName);
			GUILayout.Label("设备类型：" + SystemInfo.deviceType);
			GUILayout.Label("设备标识：" + SystemInfo.deviceUniqueIdentifier);
			GUILayout.Label(string.Empty);
			GUILayout.Label(string.Empty);
			GUILayout.Label(string.Empty);
			GUILayout.EndScrollView();
		}
		else if (_debugType == DebugType.Screen)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label("Screen Information");
			GUILayout.EndHorizontal();
			GUILayout.BeginVertical("Box");
			GUILayout.Label("DPI：" + Screen.dpi);
			GUILayout.Label("分辨率：" + Screen.currentResolution.ToString());
			GUILayout.EndVertical();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("全屏"))
			{
				Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, !Screen.fullScreen);
			}
			GUILayout.EndHorizontal();
		}
		else if (_debugType == DebugType.Quality)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label("Quality Information");
			GUILayout.EndHorizontal();
			GUILayout.BeginVertical("Box");
			string str = string.Empty;
			if (QualitySettings.GetQualityLevel() == 0)
			{
				str = " [最低]";
			}
			else if (QualitySettings.GetQualityLevel() == QualitySettings.names.Length - 1)
			{
				str = " [最高]";
			}
			GUILayout.Label("图形质量：" + QualitySettings.names[QualitySettings.GetQualityLevel()] + str);
			GUILayout.EndVertical();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("降低一级图形质量"))
			{
				QualitySettings.DecreaseLevel();
			}
			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("提升一级图形质量"))
			{
				QualitySettings.IncreaseLevel();
			}
			GUILayout.EndHorizontal();
		}
		else if (_debugType == DebugType.Environment)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label("Environment Information");
			GUILayout.EndHorizontal();
			GUILayout.BeginVertical("Box");
			GUILayout.Label("项目名称：" + Application.productName);
			GUILayout.Label("项目版本：" + Application.version);
			GUILayout.Label("Unity版本：" + Application.unityVersion);
			GUILayout.Label("公司名称：" + Application.companyName);
			GUILayout.EndVertical();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("退出程序"))
			{
				Application.Quit();
			}
			GUILayout.EndHorizontal();
		}
		GUILayout.EndArea();
	}

	private void ShrinkGUIWindow(int windowId)
	{
		GUI.DragWindow(new Rect(0f, 0f, 10000f, 40f * w_rate));
		GUI.contentColor = _fpsColor;
		GUILayout.BeginArea(new Rect(40f * w_rate, 40f * w_rate, 90f * w_rate, 90f * w_rate));
		if (GUILayout.Button("FPS:" + _fps, GUILayout.Width(80f * w_rate), GUILayout.Height(80f * w_rate)))
		{
			_expansion = true;
			_windowRect.width = 600f * w_rate;
			_windowRect.height = 600f * w_rate;
		}
		GUILayout.EndArea();
		GUI.contentColor = Color.white;
	}
}
