using System;

[Serializable]
public class TextDataCsv
{
	public int ID;

	public string TextStr;

	public TextDataCsv(string[] values, int valueIndex)
	{
		int num = 0;
		ID = int.Parse(values[num++]);
		num += valueIndex;
		TextStr = values[num++].Replace("\\n", "\n");
		TextStr = TextStr.Replace("\"", string.Empty);
	}

	public TextDataCsv()
	{
		ID = 0;
	}
}
