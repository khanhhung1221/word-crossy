using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class TextLibraryDataManagerCsv : ManagerCsv
{
	private static TextLibraryDataManagerCsv inst;

	private Dictionary<int, TextDataCsv> TextDataList;

	public static TextLibraryDataManagerCsv instance => inst ?? (inst = new TextLibraryDataManagerCsv());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			int valueIndex = 0;
			MemoryStream stream = new MemoryStream(file);
			TextDataList = new Dictionary<int, TextDataCsv>();
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								TextDataCsv textDataCsv = new TextDataCsv(array, valueIndex);
								TextDataList[textDataCsv.ID] = textDataCsv;
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public string GetText(int textId)
	{
		if (TextDataList != null && TextDataList.ContainsKey(textId))
		{
			return TextDataList[textId].TextStr;
		}
		return textId.ToString();
	}

	public static void Renew()
	{
		inst = new TextLibraryDataManagerCsv();
	}
}
