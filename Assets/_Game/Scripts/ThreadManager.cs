using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using UnityEngine;

public class ThreadManager : MonoBehaviour
{
	private delegate void ThreadSyncEvent(NotiData data);

	public static ThreadManager Instance;

	public const string UPDATE_MESSAGE = "UpdateMessage";

	public const string UPDATE_EXTRACT = "UpdateExtract";

	public const string UPDATE_DOWNLOAD = "UpdateDownload";

	public const string UPDATE_PROGRESS = "UpdateProgress";

	private Thread thread;

	private Action<NotiData> func;

	private Stopwatch sw = new Stopwatch();

	private string currDownUrl = string.Empty;

	private string currDownFile = string.Empty;

	private static readonly object m_lockObj = new object();

	private static Queue<ThreadEvent> events = new Queue<ThreadEvent>();

	private ThreadSyncEvent m_SyncEvent;

	private bool isDownLoad;

	private float downLoadWaitTime;

	private bool isStopDL;

	private int temp_complete;

	internal static event Action OnDownloadCompleteEvent;

	private void Awake()
	{
		Instance = this;
		m_SyncEvent = OnSyncEvent;
		thread = new Thread(OnUpdate);
	}

	private void Start()
	{
		thread.Start();
	}

	public void AddEvent(ThreadEvent ev, Action<NotiData> func = null)
	{
		lock (m_lockObj)
		{
			this.func = func;
			events.Enqueue(ev);
		}
	}

	public void AddLevelMapImageDl()
	{
		if (WordGlobal.IsNeedDownLoadImage)
		{
			int num = 0;
			MapDataManager.instance.CheckDownLoadFile(PlayerInfo.Instance.MaxPlayLevel);
			if (MapDataManager.instance.NeedDownLoadFiles.Count == 0)
			{
				WordFaceBook.Instance.UpdateTrackTaskEvent("OTA", "Complete");
			}
			while (MapDataManager.instance.NeedDownLoadFiles.Count > 0)
			{
				string str = MapDataManager.instance.NeedDownLoadFiles.Dequeue();
				ThreadEvent threadEvent = new ThreadEvent();
				threadEvent.Key = "UpdateDownload";
				string item = WordGlobal.DownLoadFileUrl + str;
				string item2 = WordGlobal.DataPath + str;
				threadEvent.evParams.Add(item);
				threadEvent.evParams.Add(item2);
				AddEvent(threadEvent);
			}
		}
	}

	private void OnSyncEvent(NotiData data)
	{
		if (func != null)
		{
			func(data);
		}
	}

	private void OnUpdate()
	{
		if (!WordGlobal.IsNeedDownLoadImage)
		{
			return;
		}
		while (true)
		{
			if (!isStopDL)
			{
				lock (m_lockObj)
				{
					if (events.Count > 0 && !isDownLoad)
					{
						ThreadEvent threadEvent = events.Dequeue();
						try
						{
							string key = threadEvent.Key;
							if (key != null)
							{
								if (!(key == "UpdateExtract"))
								{
									if (key == "UpdateDownload")
									{
										OnDownloadFile(threadEvent.evParams);
									}
								}
								else
								{
									OnExtractFile(threadEvent.evParams);
								}
							}
						}
						catch (Exception ex)
						{
							UnityEngine.Debug.LogError(ex.Message);
						}
					}
				}
			}
			Thread.Sleep(1);
		}
	}

	public void Update()
	{
		if (Application.internetReachability != 0)
		{
			isStopDL = false;
			if (isDownLoad)
			{
				downLoadWaitTime -= Time.deltaTime;
				if (downLoadWaitTime < 0f)
				{
					isDownLoad = false;
				}
			}
		}
		else
		{
			isStopDL = true;
			isDownLoad = false;
		}
		if (temp_complete > 0)
		{
			PlayerInfo.Instance.OTA_CompleteNum += temp_complete;
			temp_complete = 0;
		}
	}

	public Sprite LoadSprite(string path)
	{
		if (File.Exists(path))
		{
			FileStream fileStream = null;
			try
			{
				float realtimeSinceStartup = Time.realtimeSinceStartup;
				fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);
				fileStream.Seek(0L, SeekOrigin.Begin);
				byte[] array = new byte[fileStream.Length];
				fileStream.Read(array, 0, (int)fileStream.Length);
				fileStream.Close();
				fileStream.Dispose();
				fileStream = null;
				Texture2D texture2D = new Texture2D(1, 1);
				texture2D.LoadImage(array);
				if (texture2D.width <= 10 && texture2D.height <= 10)
				{
					File.Delete(path);
					MapDataManager.instance.CheckDownLoadFile();
					return null;
				}
				return Sprite.Create(texture2D, new Rect(0f, 0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f));
			}
			catch (Exception arg)
			{
				UnityEngine.Debug.Log("LoadSprite:" + arg);
			}
			finally
			{
				fileStream?.Close();
			}
		}
		return null;
	}

	public static IEnumerator DownLoad(string downloadfileName1, string desFileName1)
	{
		string downloadfileName2 = "http://s3-us-west-2.amazonaws.com/lettuce-game/words/Image/LevelMap/bg_0_1.jpg";
		string desFileName2 = "d:/b.jpg";
		HttpWebRequest request = (HttpWebRequest)WebRequest.Create(downloadfileName2);
		request.Timeout = 5000;
		WebResponse response = request.GetResponse();
		FileStream fs = new FileStream(desFileName2, FileMode.Create);
		try
		{
			Stream netStream = response.GetResponseStream();
			try
			{
				int packLength = 20480;
				long countLength = response.ContentLength;
				byte[] nbytes = new byte[packLength];
				int nReadSize = netStream.Read(nbytes, 0, packLength);
				while (nReadSize > 0)
				{
					fs.Write(nbytes, 0, nReadSize);
					nReadSize = netStream.Read(nbytes, 0, packLength);
					double dDownloadedLength = (double)fs.Length * 1.0 / 1048576.0;
					string ss = string.Format(arg1: (double)countLength * 1.0 / 1048576.0, format: "已下载 {0:F}M / {1:F}M", arg0: dDownloadedLength);
					UnityEngine.Debug.Log(ss);
					yield return false;
				}
				netStream.Close();
				fs.Close();
				if (ThreadManager.OnDownloadCompleteEvent != null)
				{
					UnityEngine.Debug.Log("download  finished");
					ThreadManager.OnDownloadCompleteEvent();
				}
			}
			finally
			{
				//base._003C_003E__Finally0();
			}
		}
		finally
		{
			//base._003C_003E__Finally1();
		}
	}

	public IEnumerator setimg()
	{
		currDownFile = "d:/b.jpg";
		WWW www = new WWW(currDownFile);
		try
		{
			yield return www;
			try
			{
				if (www.error == null)
				{
					try
					{
					}
					catch (Exception exception)
					{
						UnityEngine.Debug.LogException(exception);
					}
				}
			}
			catch (Exception exception2)
			{
				UnityEngine.Debug.LogException(exception2);
			}
		}
		finally
		{
			//base._003C_003E__Finally0();
		}
	}

	public void OnDownloadFile(List<object> evParams)
	{
		currDownUrl = evParams[0].ToString();
		currDownFile = evParams[1].ToString();
		isDownLoad = true;
		downLoadWaitTime = 10f;
		string directoryName = Path.GetDirectoryName(currDownFile);
		if (!Directory.Exists(directoryName))
		{
			Directory.CreateDirectory(directoryName);
		}
		UnityEngine.Debug.Log("url:" + currDownUrl + " currDownFile:" + currDownFile + " fullPath:" + directoryName);
		using (WebClient webClient = new WebClient())
		{
			sw.Start();
			webClient.DownloadProgressChanged += ProgressChanged;
			webClient.DownloadFileAsync(new Uri(currDownUrl), currDownFile);
		}
	}

	private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
	{
		string text = currDownFile;
		string text2 = string.Format("{0} kb/s", ((double)e.BytesReceived / 1024.0 / sw.Elapsed.TotalSeconds).ToString("0.00"));
		string text3 = string.Format("{0} MB / {1} MB", ((double)e.BytesReceived / 1024.0 / 1024.0).ToString("0.00"), ((double)e.TotalBytesToReceive / 1024.0 / 1024.0).ToString("0.00"));
		float num = (float)e.ProgressPercentage / 100f;
		string text4 = text + "|" + text2 + "|" + text3 + "|" + num;
		NotiData data = new NotiData("UpdateProgress", text4);
		if (m_SyncEvent != null)
		{
			m_SyncEvent(data);
		}
		if (e.ProgressPercentage == 100 && e.BytesReceived == e.TotalBytesToReceive)
		{
			sw.Reset();
			isDownLoad = false;
			temp_complete++;
			WordGlobal.DebugLog("完成 " + text4);
			data = new NotiData("UpdateDownload", currDownFile);
			if (m_SyncEvent != null)
			{
				m_SyncEvent(data);
			}
		}
	}

	private void OnExtractFile(List<object> evParams)
	{
		NotiData data = new NotiData("UpdateDownload", null);
		if (m_SyncEvent != null)
		{
			m_SyncEvent(data);
		}
	}

	private void OnDestroy()
	{
		thread.Abort();
	}
}
