using System;

public class TimeEvent
{
	public string StrEvent;

	public object TimeEventParas;

	public bool IsRemove;

	public Action<object> TimeCallBack;

	public void OnRegistEvent(Action<object> dtAction, object para)
	{
		StrEvent = dtAction.Method.Name;
		TimeCallBack = dtAction;
		TimeEventParas = para;
		IsRemove = false;
	}
}
