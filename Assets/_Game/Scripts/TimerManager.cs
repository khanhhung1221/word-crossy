using System;
using System.Collections.Generic;
using UnityEngine;

public class TimerManager : MonoBehaviour
{
	public static TimerManager instance;

	private bool isSort;

	private Dictionary<DateTime, List<TimeEvent>> dTimeEvents = new Dictionary<DateTime, List<TimeEvent>>();

	private void Start()
	{
	}

	public void Go(object o)
	{
		string text = (string)o;
	}

	private void Update()
	{
		Dictionary<DateTime, List<TimeEvent>> dictionary = new Dictionary<DateTime, List<TimeEvent>>(dTimeEvents);
		foreach (KeyValuePair<DateTime, List<TimeEvent>> item in dictionary)
		{
			if (item.Key <= DateTime.UtcNow)
			{
				foreach (TimeEvent item2 in item.Value)
				{
					if (!item2.IsRemove)
					{
						item2.TimeCallBack(item2.TimeEventParas);
					}
				}
				dTimeEvents.Remove(item.Key);
			}
		}
	}

	private void LateUpdate()
	{
	}

	public static TimerManager CreatManager()
	{
		if (!instance)
		{
			GameObject gameObject = new GameObject();
			gameObject.name = "TimerManager";
			instance = gameObject.AddComponent<TimerManager>();
		}
		return instance;
	}

	private void Awake()
	{
		instance = this;
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
	}

	public void RegistTimerEvent(DateTime time, Action<object> action, object obj)
	{
		TimeEvent timeEvent = new TimeEvent();
		timeEvent.OnRegistEvent(action, obj);
		RegistTimerEvent(time, timeEvent);
	}

	public void RegistTimerEvent(DateTime time, TimeEvent te)
	{
		if (!dTimeEvents.ContainsKey(time))
		{
			dTimeEvents.Add(time, new List<TimeEvent>());
		}
		if (!dTimeEvents[time].Contains(te))
		{
			dTimeEvents[time].Add(te);
		}
		isSort = true;
	}

	public void UnRegistTimerEvent(DateTime time, TimeEvent delEvent)
	{
		if (dTimeEvents.ContainsKey(time) && dTimeEvents[time].Contains(delEvent))
		{
			if (time > DateTime.UtcNow)
			{
				dTimeEvents[time].Remove(delEvent);
			}
			else
			{
				dTimeEvents[time].Find((TimeEvent x) => x == delEvent).IsRemove = true;
			}
		}
	}

	public void UnRegistTimerEvent(Action<object> action)
	{
		foreach (KeyValuePair<DateTime, List<TimeEvent>> dTimeEvent in dTimeEvents)
		{
			if (dTimeEvent.Value.FindIndex((TimeEvent x) => x.StrEvent == action.Method.Name) >= 0)
			{
				dTimeEvent.Value.Find((TimeEvent x) => x.StrEvent == action.Method.Name).IsRemove = true;
			}
		}
	}

	public void Restart()
	{
		dTimeEvents.Clear();
	}
}
