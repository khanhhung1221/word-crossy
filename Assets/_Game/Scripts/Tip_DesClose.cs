using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Tip_DesClose : MonoBehaviour
{
	public Text text;

	public Text close_txt;

	private UnityAction call;

	private void Start()
	{
		close_txt.text = TextLibraryDataManagerCsv.instance.GetText(1279);
	}

	public void ShowUI(string str, UnityAction action = null)
	{
		text.text = str;
		if (action != null)
		{
			call = action;
		}
	}

	private void OnDestroy()
	{
		if (call != null)
		{
			call();
		}
	}
}
