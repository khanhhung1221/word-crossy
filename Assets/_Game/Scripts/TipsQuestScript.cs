using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TipsQuestScript : MonoBehaviour
{
	public static int Quest_id;

	public Text m_Txt;

	public Text des_txt;

	public Image icon_img;

	private float _startY = 130f;

	private float _showTime = 2f;

	private float _moveTime = 0.3f;

	private float _tmpTime;

	private void Start()
	{
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1164);
	}

	private void Update()
	{
		_tmpTime -= Time.deltaTime;
		if (_tmpTime < 0f)
		{
			_tmpTime = _showTime;
			GetComponent<RectTransform>().DOAnchorPosY(_startY, _moveTime).OnComplete(delegate
			{
				Object.Destroy(base.gameObject);
			});
		}
	}

	public void ShowTips()
	{
		DailyQuestDataCsv itemByID = DailyQuestDataManagerCsv.instance.GetItemByID(Quest_id);
		int num = itemByID.GetQuestRequireType();
		if (num == 41 || num == 42)
		{
			num = 40;
		}

        icon_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyQuest/daily_quest_icon" + num);
        DailyQuestSaveData dataByID = PlayerInfo.Instance.GetDailyQuestSaveData().GetDataByID(Quest_id);
		int questRequireNum = itemByID.GetQuestRequireNum();
		if (dataByID != null)
		{
			questRequireNum = dataByID.GetQuestRequireNum();
		}
		if (itemByID.GetQuestRequireType() == 30)
		{
			m_Txt.text = WordGlobal.ReadText(itemByID.DesID);
		}
		else
		{
			m_Txt.text = string.Format(WordGlobal.ReadText(itemByID.DesID), questRequireNum);
		}
		float endValue = -16f - WordGlobal.GetSaftTopSize();
		GetComponent<RectTransform>().DOAnchorPosY(endValue, _moveTime);
		_tmpTime = _showTime;
	}
}
