using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TipsScript : MonoBehaviour
{
	public Text m_Txt;

	private float _startY = 133f;

	private float _showTime = 1.5f;

	private float _moveTime = 0.3f;

	private float _tmpTime;

	private void Start()
	{
		float endValue = 0f - WordGlobal.GetSaftTopSize();
		GetComponent<RectTransform>().DOAnchorPosY(endValue, _moveTime);
		_tmpTime = _showTime;
	}

	private void Update()
	{
		_tmpTime -= Time.deltaTime;
		if (_tmpTime < 0f)
		{
			_tmpTime = _showTime;
			GetComponent<RectTransform>().DOAnchorPosY(_startY, _moveTime).OnComplete(delegate
			{
				Object.Destroy(base.gameObject);
			});
		}
	}

	public void ShowTips(string txt)
	{
		_tmpTime = _showTime;
		m_Txt.text = txt;
	}
}
