using LitJson;

public class TournamentInfoData
{
	public string uuid;

	public string fbid;

	public string name;

	public int openCurLevel;

	public int cupNum;

	public int rankNum;

	public int Vip;

	public TournamentInfoData(JsonData data)
	{
		int num = 0;
		uuid = data[num++].ToString();
		fbid = data[num++].ToString();
		name = data[num++].ToString();
		openCurLevel = int.Parse(data[num++].ToString());
		cupNum = int.Parse(data[num++].ToString());
		rankNum = int.Parse(data[num++].ToString());
		Vip = int.Parse(data[num++].ToString());
	}

	public TournamentInfoData()
	{
	}
}
