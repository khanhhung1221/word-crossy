using UnityEngine;
using UnityEngine.UI;

public class TutorialButterFlyVideo : MonoBehaviour
{
	public RectTransform pos_rect;

	public Text des_txt;

	public Text btn_txt;

	private Transform butterFly_tran;

	public static bool isShow;

	public static bool isHide;

	private void Start()
	{
		isHide = false;
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1281);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1279);
		if (MainUIScript.Instance != null)
		{
			butterFly_tran = MainUIScript.Instance.transform.Find("ButterFlyTV");
			butterFly_tran.GetComponent<Canvas>().sortingOrder = 19;
			Renderer[] componentsInChildren = butterFly_tran.GetComponentsInChildren<Renderer>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].sortingOrder = 19;
			}
		}
		if (WordGlobal.isMainUIShowBanner)
		{
			RectTransform rectTransform = pos_rect;
			Vector2 anchoredPosition = pos_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, WordGlobal.showBanner_bottomupy);
		}
		else
		{
			RectTransform rectTransform2 = pos_rect;
			Vector2 anchoredPosition2 = pos_rect.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, 0f);
		}
		isShow = true;
	}

	private void Update()
	{
		if (isHide)
		{
			Object.Destroy(base.gameObject);
			isHide = false;
		}
	}

	private void OnDestroy()
	{
		isShow = false;
		if (MainUIScript.Instance != null)
		{
			butterFly_tran.GetComponent<Canvas>().sortingOrder = 1;
			Renderer[] componentsInChildren = butterFly_tran.GetComponentsInChildren<Renderer>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].sortingOrder = 1;
			}
		}
	}
}
