using UnityEngine;
using UnityEngine.UI;

public class TutorialDailyPuzzleFinish : MonoBehaviour
{
	public Text des_txt;

	public Text btn_text;

	private void Start()
	{
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1201);
		btn_text.text = TextLibraryDataManagerCsv.instance.GetText(1007);
	}

	public void BtnCloseClick()
	{
		Object.Destroy(base.gameObject);
	}
}
