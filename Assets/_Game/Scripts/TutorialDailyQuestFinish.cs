using UnityEngine;
using UnityEngine.UI;

public class TutorialDailyQuestFinish : MonoBehaviour
{
	public static bool isHide;

	public RectTransform mainui_tran;

	public RectTransform levelmap_tran;

	public Text des_txt;

	public Text des_txt01;

	public Transform btn_tran;

	private bool isClickQuestUI = true;

	private void Start()
	{
		isHide = false;
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1197);
		des_txt01.text = TextLibraryDataManagerCsv.instance.GetText(1197);
		if (MainUIScript.Instance != null)
		{
			mainui_tran.gameObject.SetActive(value: true);
			if (WordGlobal.isMainUIShowBanner)
			{
				RectTransform rectTransform = mainui_tran;
				Vector2 anchoredPosition = mainui_tran.anchoredPosition;
				rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, -326f + WordGlobal.showBanner_bottomupy);
			}
			else
			{
				RectTransform rectTransform2 = mainui_tran;
				Vector2 anchoredPosition2 = mainui_tran.anchoredPosition;
				rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, -326f);
			}
			levelmap_tran.gameObject.SetActive(value: false);
			MainUIScript.Instance.DailyQuestBtn.transform.SetParent(btn_tran);
		}
		else if (BtnsCtrlUIScript.Instance != null)
		{
			mainui_tran.gameObject.SetActive(value: false);
			levelmap_tran.gameObject.SetActive(value: true);
			BtnsCtrlUIScript.Instance.DailyQuestBtnObj.transform.SetParent(btn_tran);
		}
		else
		{
			HIde();
		}
	}

	private void Update()
	{
		if (isHide)
		{
			isClickQuestUI = false;
			HIde();
			isHide = false;
		}
		if (MainUIScript.Instance != null && MainUIScript.Instance.isShowResult())
		{
			isHide = true;
		}
	}

	public void HIde()
	{
		Object.Destroy(base.gameObject);
	}

	private void OnDestroy()
	{
		if (MainUIScript.Instance != null)
		{
			MainUIScript.Instance.DailyQuestBtn.transform.SetParent(MainUIScript.Instance.transform);
			if (isClickQuestUI)
			{
				MainUIScript.Instance.ShowDailyQuest(0);
			}
		}
		else if (BtnsCtrlUIScript.Instance != null)
		{
			BtnsCtrlUIScript.Instance.DailyQuestBtnObj.transform.SetParent(LevelMapUIScript.Instance.DownBtnsObj.transform);
			if (isClickQuestUI)
			{
				LevelMapUIScript.Instance.BtnDailyQuestClick();
			}
		}
	}
}
