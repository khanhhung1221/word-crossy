using UnityEngine;
using UnityEngine.UI;

public class TutorialFirstTips : MonoBehaviour
{
	public Text des;

	public RectTransform bottom_pos;

	private void Start()
	{
		des.text = TextLibraryDataManagerCsv.instance.GetText(1200);
		if (WordGlobal.isMainUIShowBanner)
		{
			RectTransform rectTransform = bottom_pos;
			Vector2 anchoredPosition = bottom_pos.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, -333f + WordGlobal.showBanner_bottomupy);
		}
		else
		{
			RectTransform rectTransform2 = bottom_pos;
			Vector2 anchoredPosition2 = bottom_pos.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, -333f);
		}
	}

	public void Hide()
	{
		Object.Destroy(base.gameObject);
	}
}
