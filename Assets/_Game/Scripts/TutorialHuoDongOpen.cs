using UnityEngine;
using UnityEngine.UI;

public class TutorialHuoDongOpen : MonoBehaviour
{
	public Text des;

	public Text btn_txt;

	private void Start()
	{
		des.text = TextLibraryDataManagerCsv.instance.GetText(1234);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1043);
	}

	public void BtnOKClick()
	{
		if (MainUIScript.Instance != null)
		{
			MainUIScript.Instance.BtnCloseClick();
		}
		Object.Destroy(base.gameObject);
	}
}
