using UnityEngine;
using UnityEngine.UI;

public class TutorialHuoDongTournament03 : MonoBehaviour
{
	public Text des_txt;

	public Text btn_txt;

	private GameObject btn_go;

	public static bool isHide;

	private void Start()
	{
		isHide = false;
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1289);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1007);
		btn_go = GameObject.Find("HuoDongTournamentBtn");
		if (btn_go != null)
		{
			btn_go.GetComponent<Animator>().enabled = false;
			btn_go.GetComponent<Canvas>().sortingOrder = 19;
			btn_go.transform.SetParent(base.transform);
		}
	}

	private void Update()
	{
		if (isHide)
		{
			Object.Destroy(base.gameObject);
			isHide = false;
		}
	}

	private void OnDestroy()
	{
		if (btn_go != null)
		{
			GameObject gameObject = GameObject.Find("Result/Main/Btns");
			if (gameObject != null)
			{
				btn_go.GetComponent<Animator>().enabled = true;
				btn_go.GetComponent<Image>().enabled = true;
				btn_go.GetComponent<Button>().enabled = true;
				btn_go.GetComponent<Canvas>().enabled = true;
				btn_go.GetComponent<Canvas>().sortingOrder = 14;
				btn_go.GetComponent<GraphicRaycaster>().enabled = true;
				btn_go.transform.SetParent(gameObject.transform);
			}
		}
	}
}
