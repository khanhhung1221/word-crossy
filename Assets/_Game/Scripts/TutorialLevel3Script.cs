using UnityEngine;

public class TutorialLevel3Script : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
	}

	public void BtnGotItClick()
	{
		MainUIScript.Instance.m_ObjBtnTipsClick.transform.SetParent(MainUIScript.Instance.m_ObjBtnFirstLetter.transform.parent);
		MainUIScript.Instance.m_ObjBtnRandomLetter.transform.SetParent(MainUIScript.Instance.m_ObjBtnFirstLetter.transform.parent);
		Object.Destroy(base.gameObject);
	}
}
