using UnityEngine;
using UnityEngine.UI;

public class TutorialLevel4Script : MonoBehaviour
{
	public Text m_TxtDesc;

	public GameObject m_ObjBtn;

	public GameObject m_BtnTips;

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void Awake()
	{
		WordGlobal.InitLabel(base.transform);
	}

	public void BtnClick()
	{
		MainUIScript.Instance.BtnCellTipsClick();
		Object.Destroy(base.gameObject);
	}
}
