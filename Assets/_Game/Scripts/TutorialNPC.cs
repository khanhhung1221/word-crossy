using UnityEngine;
using UnityEngine.UI;

public class TutorialNPC : MonoBehaviour
{
	public static string TalkStr = string.Empty;

	public RectTransform talk_bg_rect;

	public Text talk_txt;

	private float wait_time;

	public bool isTest;

	public string test = string.Empty;

	private void Start()
	{
		SetTalkStr(TalkStr);
	}

	public void SetTalkStr(string str)
	{
		talk_txt.text = str;
		talk_txt.Rebuild(CanvasUpdate.PreRender);
		wait_time = 0.6f;
		talk_bg_rect.gameObject.SetActive(value: false);
		RectTransform rectTransform = talk_bg_rect;
		Vector2 sizeDelta = talk_bg_rect.sizeDelta;
		rectTransform.sizeDelta = new Vector2(sizeDelta.x, talk_txt.preferredHeight + 70f);
	}

	private void Update()
	{
		if (isTest)
		{
			SetTalkStr(test);
			isTest = false;
		}
		if (wait_time > 0f)
		{
			wait_time -= Time.deltaTime;
			if (wait_time <= 0f)
			{
				talk_bg_rect.gameObject.SetActive(value: true);
			}
		}
	}
}
