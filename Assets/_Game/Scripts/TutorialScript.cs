using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialScript : MonoBehaviour
{
	public LineRenderer[] m_LineRenderer;

	public Text m_TxtDesc;

	public GameObject m_ObjMask;

	public GameObject m_ObjHand;

	public GameObject m_ObjBG;

	private List<Vector3> _posList;

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void Awake()
	{
		_posList = new List<Vector3>();
	}

	public void SetLinePos(List<Vector3> poslist)
	{
		_posList = poslist;
		for (int i = 0; i < _posList.Count - 1; i++)
		{
			m_LineRenderer[i].positionCount = 2;
			m_LineRenderer[i].SetPosition(0, _posList[i]);
			m_LineRenderer[i].SetPosition(1, _posList[i + 1]);
		}
		SetHandMove();
	}

	public void SetText(string txt)
	{
		m_TxtDesc.text = txt;
		SetBgAni();
	}

	public void SetHandMove()
	{
		m_ObjHand.transform.DOKill();
		m_ObjHand.transform.position = _posList[0];
		m_ObjHand.SetActive(value: true);
		m_ObjHand.transform.DOPath(_posList.ToArray(), 4f).SetLoops(-1);
	}

	public void SetBgAni(bool isin = true)
	{
		if (isin)
		{
			m_ObjBG.transform.localScale = Vector3.zero;
			m_ObjBG.SetActive(value: true);
			m_ObjBG.transform.DOScale(1f, 1f);
		}
		else
		{
			m_ObjBG.transform.localScale = Vector3.one;
			m_ObjBG.SetActive(value: true);
			m_ObjBG.transform.DOScale(0f, 1f).OnComplete(OnAniStop);
		}
	}

	public void OnExitClick()
	{
		SetBgAni(isin: false);
	}

	public void OnAniStop()
	{
		base.gameObject.SetActive(value: false);
	}
}
