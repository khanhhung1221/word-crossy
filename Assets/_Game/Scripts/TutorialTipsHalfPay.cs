using UnityEngine;
using UnityEngine.UI;

public class TutorialTipsHalfPay : MonoBehaviour
{
	public RectTransform pos_rect;

	public Text des_txt;

	public Text btn_txt;

	public static bool isHide;

	private void Start()
	{
		isHide = false;
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1236);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1007);
		if (MainUIScript.Instance != null)
		{
			MainUIScript.Instance.m_ObjBtnTipsClick.transform.SetParent(base.transform);
		}
		if (WordGlobal.isMainUIShowBanner)
		{
			RectTransform rectTransform = pos_rect;
			Vector2 anchoredPosition = pos_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(anchoredPosition.x, WordGlobal.showBanner_bottomupy);
		}
		else
		{
			RectTransform rectTransform2 = pos_rect;
			Vector2 anchoredPosition2 = pos_rect.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(anchoredPosition2.x, 0f);
		}
	}

	private void Update()
	{
		if (isHide)
		{
			Object.Destroy(base.gameObject);
			isHide = false;
		}
	}

	private void OnDestroy()
	{
		if (MainUIScript.Instance != null)
		{
			MainUIScript.Instance.m_ObjBtnTipsClick.transform.SetParent(MainUIScript.Instance.transform.Find("Bottom"));
			MainUIScript.Instance.m_ObjBtnTipsClick.GetComponent<Image>().enabled = true;
			MainUIScript.Instance.m_ObjBtnTipsClick.GetComponent<Button>().enabled = true;
			MainUIScript.Instance.m_ObjBtnTipsClick.GetComponent<Animator>().enabled = true;
			MainUIScript.Instance.m_ObjBtnTipsClick.GetComponent<BtnNumberPriceSet>().enabled = true;
		}
	}
}
