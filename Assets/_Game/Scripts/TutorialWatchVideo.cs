using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TutorialWatchVideo : MonoBehaviour
{
	public static bool BtnClick;

	public Text des_txt;

	public Text btn_txt;

	public GameObject main_go;

	public GameObject flygold_go;

	public Transform[] golds;

	public Transform fly_tran;

	private int anim_idx;

	private float anim_t = 0.8f;

	private float delay_t = 0.2f;

	private int count = -1;

	private void Start()
	{
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1196);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1007);
		main_go.SetActive(value: true);
		flygold_go.SetActive(value: false);
		BtnVideoScript.isClearWaitTime = true;
	}

	private void Update()
	{
		if (BtnClick)
		{
			BtnGotItClick();
			BtnClick = false;
		}
		if (anim_idx != 1)
		{
			return;
		}
		if (MainUIScript.Instance != null)
		{
			RectTransform rectTransform = MainUIScript.Instance.m_ObjBtnWatchVideo.transform as RectTransform;
			Vector3 position = fly_tran.position;
			Vector2 anchoredPosition = rectTransform.anchoredPosition;
			if (anchoredPosition.x <= 10f)
			{
				position = fly_tran.position;
			}
			count = -1;
			for (int i = 0; i < golds.Length; i++)
			{
				int num = Random.Range(0, 5);
				float delay = Random.Range(0f, 0.3f);
				Vector3[] path = WordGlobal.CountCirclePoints(golds[i].position, position, num);
				golds[i].DOPath(path, anim_t).SetDelay(delay).OnComplete(delegate
				{
					AudioControl.GetInstance().PlaySoundEff(AudioControl.GetInstance().SF_GetCoin);
					count++;
					HideGold();
				});
			}
			anim_idx = 2;
		}
		else
		{
			Object.Destroy(base.gameObject);
		}
	}

	public void HideGold()
	{
		if (count >= golds.Length - 1)
		{
			Object.Destroy(base.gameObject);
		}
		else
		{
			golds[count].gameObject.SetActive(value: false);
		}
	}

	public void BtnGotItClick()
	{
		if (anim_idx != 2)
		{
			PlayerInfo.Instance.ShowTutorialWatchVideo = 1;
			main_go.SetActive(value: false);
			flygold_go.SetActive(value: true);
			BtnVideoScript.isClearWaitTime = true;
			anim_idx = 1;
		}
	}
}
