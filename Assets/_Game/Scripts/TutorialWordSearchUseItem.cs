using UnityEngine;
using UnityEngine.UI;

public class TutorialWordSearchUseItem : MonoBehaviour
{
	public Text des_txt;

	public Text btn_txt;

	private HuoDongWordSearch ws;

	public static bool isHide;

	private void Start()
	{
		isHide = false;
		des_txt.text = TextLibraryDataManagerCsv.instance.GetText(1254);
		btn_txt.text = TextLibraryDataManagerCsv.instance.GetText(1007);
		if (GameObject.Find("HuoDongWordSearch") != null)
		{
			ws = GameObject.Find("HuoDongWordSearch").GetComponent<HuoDongWordSearch>();
			ws.GuideBtn_tran.SetParent(base.transform);
		}
	}

	private void Update()
	{
		if (isHide)
		{
			Object.Destroy(base.gameObject);
			isHide = false;
		}
	}

	private void OnDestroy()
	{
		if (ws != null)
		{
			ws.GuideBtn_tran.SetParent(ws.hide_tran);
			ws.GuideBtn_tran.GetComponent<Image>().enabled = true;
			ws.GuideBtn_tran.GetComponent<Button>().enabled = true;
		}
	}
}
