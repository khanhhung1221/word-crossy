using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class TxtToXlsxManager : MonoBehaviour
{
	public string _filePath = "E:/WorkSpace/Csv/";

	public List<string> TxtList;

	private void Start()
	{
		if (TxtList != null)
		{
			for (int i = 0; i < TxtList.Count; i++)
			{
				string[] array = TxtList[i].Split('.');
				Init(TxtList[i], array[0] + "_");
			}
		}
	}

	private void Update()
	{
	}

	public void Init(string name, string title)
	{
		if (File.Exists(_filePath + name))
		{
			string json = File.ReadAllText(_filePath + name);
			SectionsDataList sectionsDataList = JsonUtility.FromJson<SectionsDataList>(json);
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("level,");
			stringBuilder.Append("word,");
			stringBuilder.Append("moneyword,");
			stringBuilder.Append("bonus");
			List<string> list = new List<string>();
			list = sectionsDataList.GetAllSetsDataList();
			list.Insert(0, stringBuilder.ToString());
			File.WriteAllLines(_filePath + title + "words.csv", list.ToArray(), Encoding.UTF8);
			list = sectionsDataList.GetAllHiddenSetsDataList();
			list.Insert(0, stringBuilder.ToString());
			File.WriteAllLines(_filePath + title + "hiddenwords.csv", list.ToArray(), Encoding.UTF8);
			list = sectionsDataList.GetAllDailyLevelDataList();
			list.Insert(0, stringBuilder.ToString());
			File.WriteAllLines(_filePath + title + "dailywords.csv", list.ToArray(), Encoding.UTF8);
		}
	}
}
