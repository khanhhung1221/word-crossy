using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public enum MainUIType
	{
		Type_Common = 1,
		Type_Daily,
		Type_HDGardon,
		Type_HDSun,
		Type_HDTournament
	}

	public enum LevelMapUIType
	{
		Type_Normal = 1,
		Type_OpenChapter
	}

	public Transform LevelMapBgCanvas;

	public ImageBlurEffect BlurEffect;

	public Transform PopRoot;

	public bool IsCameraBlurOpen;

	public string Path_LevelMapUI = "UI/LevelMap/LevelMapUI";

	public string Path_LevelMapBg = "UI/LevelMap/LevelMapBg";

	private static UIManager instance;

	private float _bannerADRefreshTime = 30f;

	private float _leftTime;

	private bool _showAD;

	private int _curShowEffectLevel;

	public static MainUIType CurMainUIType;

	public static LevelMapUIType CurLevelMapUIType;

	private RectTransform mainCanvasRect;

	private List<UIPopInfo> pop_list = new List<UIPopInfo>();

	private List<UIPopInfo> specialPop = new List<UIPopInfo>();

	public bool isShowPopNow;

	public static UIManager Instance => instance;

	private void Start()
	{
		IsCameraBlurOpen = false;
		AudioControl.GetInstance().PlayBG(AudioControl.GetInstance().Music_Main1);
		if (PlayerInfo.Instance.TutorialStep < 2 && PlayerInfo.Instance.MaxPlayLevel == 1)
		{
			WordGlobal.LevelMapMoveAnim_StartLevel = 1;
			LoadMainUI("UI/MainUI");
		}
		else
		{
			LoadLevelMapUI(Path_LevelMapUI);
		}
		Debug.Log("Start Download Server Bg");
		//if (WordGlobal.IsNeedDownLoadImage)
		//{
		//	ThreadManager.Instance.AddLevelMapImageDl();
		//}
		if (PlayerInfo.Instance.AdRemove == 0)
		{
			_showAD = true;
		}
		_leftTime = _bannerADRefreshTime;
	}

	private void Update()
	{
		UpdateShowSpecialPop();
	}

	private void Awake()
	{
		instance = this;
		CanvasScaler component = GetComponent<CanvasScaler>();
		if ((double)(Screen.height / Screen.width) > 1.77)
		{
			component.matchWidthOrHeight = 0f;
		}
		else
		{
			component.matchWidthOrHeight = 1f;
		}
	}

	public void SetCameraBlur(bool isBlur)
	{
		IsCameraBlurOpen = isBlur;
		if (IsCameraBlurOpen == isBlur)
		{
		}
	}

	public void CreateBgEffect(Transform parent, int level)
	{
		if (_curShowEffectLevel != level)
		{
			CleanBgEffect(parent);
			GameObject levelSpriteEffect = MapDataManager.instance.GetLevelSpriteEffect(level);
			if (levelSpriteEffect != null)
			{
				levelSpriteEffect.transform.SetParent(parent, worldPositionStays: false);
				_curShowEffectLevel = level;
			}
		}
	}

	public void CleanBgEffect(Transform parent)
	{
		WordGlobal.ClearChilds(parent);
		_curShowEffectLevel = 0;
	}

	public int GetPopCount()
	{
		return pop_list.Count + specialPop.Count;
	}

	public List<UIPopInfo> GetPopList()
	{
		return pop_list;
	}

	public void AddPop(UIPopInfo uipi, int index = -1)
	{
		if (index < 0)
		{
			pop_list.Add(uipi);
		}
		else
		{
			pop_list.Insert(index, uipi);
		}
	}

	public void UpdateShowSpecialPop()
	{
		if (specialPop.Count <= 0)
		{
			return;
		}
		UIPopInfo uIPopInfo = null;
		for (int i = 0; i < specialPop.Count; i++)
		{
			if (specialPop[i].showType == 1 && LevelMapUIScript.Instance != null && LevelMapUIScript.Instance.gameObject.activeInHierarchy)
			{
				uIPopInfo = specialPop[i];
				AddPop(specialPop[i]);
				ShowUIPop(specialPop[i]);
			}
		}
		if (uIPopInfo != null)
		{
			specialPop.Remove(uIPopInfo);
		}
	}

	public void ShowNextPop(GameObject go)
	{
		PopRemove(go);
		if (pop_list.Count > 0)
		{
			UIPopInfo uIPopInfo = pop_list[0];
			if (uIPopInfo.path.Contains("DailyRewardUI") || uIPopInfo.path.Contains("HuoDongGardonTutorial01"))
			{
				specialPop.Add(uIPopInfo);
				ShowNextPop(uIPopInfo.go);
			}
			else
			{
				ShowUIPop(uIPopInfo);
			}
		}
	}

	public void ShowUIPop(UIPopInfo uipi)
	{
		if (uipi.go != null)
		{
			if (uipi.go.transform.parent != null)
			{
				uipi.go.transform.SetAsFirstSibling();
			}
			uipi.go.SetActive(value: true);
		}
	}

	public void PopRemove(GameObject go)
	{
		UIPopInfo uIPopInfo = null;
		for (int i = 0; i < pop_list.Count; i++)
		{
			if (pop_list[i].go.Equals(go))
			{
				uIPopInfo = pop_list[i];
			}
		}
		if (uIPopInfo != null)
		{
			pop_list.Remove(uIPopInfo);
		}
	}

	public void ClearPop()
	{
		pop_list.Clear();
	}

	public void ShowLog()
	{
		if (!(PopRoot != null))
		{
			return;
		}
		foreach (UIPopInfo item in pop_list)
		{
			Debug.LogError("pop_info:" + item.path);
		}
		for (int i = 0; i < PopRoot.childCount; i++)
		{
			if (PopRoot.GetChild(i) != null)
			{
				Debug.LogError(PopRoot.GetChild(i).name + "---------active:" + PopRoot.GetChild(i).gameObject.activeInHierarchy);
			}
		}
	}

	public GameObject LoadMainUI(string path, bool isPop = false, MainUIType type = MainUIType.Type_Common, int popIndex = -1, int showType = 0)
	{
		Debug.Log("UImanager LoadMainUI path:" + path);
		if (path == "UI/ShopUI")
		{
			WordGlobal.LogEvent("Shop_Open");
		}
		else if (path == "UI/MainUI")
		{
			if (LevelMapUIScript.Instance != null && LevelMapUIScript.Instance.ClearImgBlackBg != null)
			{
				CleanBgEffect(LevelMapUIScript.Instance.ClearImgBlackBg.transform);
			}
			CurMainUIType = type;
		}
		string[] array = path.Split('/');
		Transform parent = base.transform;
		if (isPop)
		{
			parent = PopRoot;
		}
		GameObject gameObject = ResourceLoadManager.GetInstance().LoadUI(path, parent);
		gameObject.name = array[array.Length - 1];
		if (isShowPopNow)
		{
			isShowPopNow = false;
		}
		else if ((isPop || popIndex >= 100) && !path.Contains("NetWaiting") && !path.Contains("ContactUsConfirmUI") && popIndex != 99)
		{
			if (pop_list.Count > 0)
			{
				gameObject.SetActive(value: false);
			}
			gameObject.AddComponent<UIPopQueue>();
			UIPopInfo uIPopInfo = new UIPopInfo();
			uIPopInfo.go = gameObject;
			uIPopInfo.path = path;
			uIPopInfo.showType = showType;
			int index = popIndex;
			if (popIndex >= 100)
			{
				index = -1;
			}
			AddPop(uIPopInfo, index);
		}
		return gameObject;
	}

	public GameObject LoadLevelMapUI(string path, LevelMapUIType type = LevelMapUIType.Type_Normal)
	{
		CurLevelMapUIType = type;
		AudioControl.GetInstance().PlayBG(AudioControl.GetInstance().Music_Main2);
		if (LevelMapBgScript.Instance == null)
		{
			ResourceLoadManager.GetInstance().LoadUI(Path_LevelMapBg, LevelMapBgCanvas);
		}
		if (LevelMapUIScript.Instance != null)
		{
			LevelMapUIScript.Instance.ShowLevelMapUI();
			return LevelMapUIScript.Instance.gameObject;
		}
		return LoadMainUI(path);
	}

	public void ShowTip_DesClose(string str, UnityAction action = null)
	{
		if (GameObject.Find("Tip_DesClose") == null)
		{
			GameObject gameObject = Instance.LoadMainUI("UI/Tips/Tip_DesClose", isPop: true, MainUIType.Type_Common, 99);
			gameObject.GetComponent<Tip_DesClose>().ShowUI(str, action);
		}
	}

	public Vector2 GetCanvasSize()
	{
		if (mainCanvasRect == null)
		{
			mainCanvasRect = base.transform.GetComponent<RectTransform>();
		}
		return mainCanvasRect.sizeDelta;
	}
}
