using UnityEngine;
using UnityEngine.UI;

public class Underline : MonoBehaviour
{
	public int underlineStart;

	public int underlineEnd;

	private Text text;

	private RectTransform textRectTransform;

	private TextGenerator textGenerator;

	private GameObject lineGameObject;

	private Image lineImage;

	private RectTransform lineRectTransform;

	private void Start()
	{
		text = base.gameObject.GetComponent<Text>();
		textRectTransform = base.gameObject.GetComponent<RectTransform>();
		textGenerator = text.cachedTextGenerator;
		lineGameObject = new GameObject("Underline");
		lineImage = lineGameObject.AddComponent<Image>();
		lineImage.color = text.color;
		lineRectTransform = lineGameObject.GetComponent<RectTransform>();
		lineRectTransform.SetParent(base.transform, worldPositionStays: false);
		lineRectTransform.anchorMin = textRectTransform.pivot;
		lineRectTransform.anchorMax = textRectTransform.pivot;
	}

	private void Update()
	{
		if (textGenerator.characterCount < 0)
		{
			return;
		}
		UICharInfo[] charactersArray = textGenerator.GetCharactersArray();
		if (underlineEnd > underlineStart && underlineEnd < charactersArray.Length)
		{
			UILineInfo[] linesArray = textGenerator.GetLinesArray();
			if (linesArray.Length >= 1)
			{
				float num = linesArray[0].height;
				Canvas componentInParent = base.gameObject.GetComponentInParent<Canvas>();
				float num2 = 1f / componentInParent.scaleFactor;
				lineRectTransform.anchoredPosition = new Vector2(num2 * (charactersArray[underlineStart].cursorPos.x + charactersArray[underlineEnd].cursorPos.x) / 2f, num2 * (charactersArray[underlineStart].cursorPos.y - num / 1f));
				lineRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, num2 * Mathf.Abs(charactersArray[underlineStart].cursorPos.x - charactersArray[underlineEnd].cursorPos.x));
				lineRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, num / 10f);
			}
		}
	}
}
