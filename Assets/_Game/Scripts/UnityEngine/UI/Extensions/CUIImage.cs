using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
	[RequireComponent(typeof(RectTransform))]
	[RequireComponent(typeof(Image))]
	[AddComponentMenu("UI/Effects/Extensions/Curly UI Image")]
	public class CUIImage : CUIGraphic
	{
		public static int SlicedImageCornerRefVertexIdx = 2;

		public static int FilledImageCornerRefVertexIdx;

		[Tooltip("For changing the size of the corner for tiled or sliced Image")]
		[HideInInspector]
		[SerializeField]
		public Vector2 cornerPosRatio = Vector2.one * -1f;

		[HideInInspector]
		[SerializeField]
		protected Vector2 oriCornerPosRatio = Vector2.one * -1f;

		public Vector2 OriCornerPosRatio => oriCornerPosRatio;

		public Image UIImage => (Image)uiGraphic;

		public static int ImageTypeCornerRefVertexIdx(Image.Type _type)
		{
			if (_type == Image.Type.Sliced)
			{
				return SlicedImageCornerRefVertexIdx;
			}
			return FilledImageCornerRefVertexIdx;
		}

		public override void ReportSet()
		{
			if (uiGraphic == null)
			{
				uiGraphic = GetComponent<Image>();
			}
			base.ReportSet();
		}

		protected override void modifyVertices(List<UIVertex> _verts)
		{
			if (!IsActive())
			{
				return;
			}
			if (UIImage.type == Image.Type.Filled)
			{
				Debug.LogWarning("Might not work well Radial Filled at the moment!");
			}
			else if (UIImage.type == Image.Type.Sliced || UIImage.type == Image.Type.Tiled)
			{
				if (cornerPosRatio == Vector2.one * -1f)
				{
					UIVertex uIVertex = _verts[ImageTypeCornerRefVertexIdx(UIImage.type)];
					cornerPosRatio = uIVertex.position;
					ref Vector2 reference = ref cornerPosRatio;
					float x = cornerPosRatio.x;
					Vector2 pivot = rectTrans.pivot;
					reference.x = (x + pivot.x * rectTrans.rect.width) / rectTrans.rect.width;
					ref Vector2 reference2 = ref cornerPosRatio;
					float y = cornerPosRatio.y;
					Vector2 pivot2 = rectTrans.pivot;
					reference2.y = (y + pivot2.y * rectTrans.rect.height) / rectTrans.rect.height;
					oriCornerPosRatio = cornerPosRatio;
				}
				if (cornerPosRatio.x < 0f)
				{
					cornerPosRatio.x = 0f;
				}
				if (cornerPosRatio.x >= 0.5f)
				{
					cornerPosRatio.x = 0.5f;
				}
				if (cornerPosRatio.y < 0f)
				{
					cornerPosRatio.y = 0f;
				}
				if (cornerPosRatio.y >= 0.5f)
				{
					cornerPosRatio.y = 0.5f;
				}
				for (int i = 0; i < _verts.Count; i++)
				{
					UIVertex value = _verts[i];
					float x2 = value.position.x;
					float width = rectTrans.rect.width;
					Vector2 pivot3 = rectTrans.pivot;
					float num = (x2 + width * pivot3.x) / rectTrans.rect.width;
					float y2 = value.position.y;
					float height = rectTrans.rect.height;
					Vector2 pivot4 = rectTrans.pivot;
					float num2 = (y2 + height * pivot4.y) / rectTrans.rect.height;
					num = ((!(num < oriCornerPosRatio.x)) ? ((!(num > 1f - oriCornerPosRatio.x)) ? Mathf.Lerp(cornerPosRatio.x, 1f - cornerPosRatio.x, (num - oriCornerPosRatio.x) / (1f - oriCornerPosRatio.x * 2f)) : Mathf.Lerp(1f - cornerPosRatio.x, 1f, (num - (1f - oriCornerPosRatio.x)) / oriCornerPosRatio.x)) : Mathf.Lerp(0f, cornerPosRatio.x, num / oriCornerPosRatio.x));
					num2 = ((!(num2 < oriCornerPosRatio.y)) ? ((!(num2 > 1f - oriCornerPosRatio.y)) ? Mathf.Lerp(cornerPosRatio.y, 1f - cornerPosRatio.y, (num2 - oriCornerPosRatio.y) / (1f - oriCornerPosRatio.y * 2f)) : Mathf.Lerp(1f - cornerPosRatio.y, 1f, (num2 - (1f - oriCornerPosRatio.y)) / oriCornerPosRatio.y)) : Mathf.Lerp(0f, cornerPosRatio.y, num2 / oriCornerPosRatio.y));
					ref Vector3 position = ref value.position;
					float num3 = num * rectTrans.rect.width;
					float width2 = rectTrans.rect.width;
					Vector2 pivot5 = rectTrans.pivot;
					position.x = num3 - width2 * pivot5.x;
					ref Vector3 position2 = ref value.position;
					float num4 = num2 * rectTrans.rect.height;
					float height2 = rectTrans.rect.height;
					Vector2 pivot6 = rectTrans.pivot;
					position2.y = num4 - height2 * pivot6.y;
					_verts[i] = value;
				}
			}
			base.modifyVertices(_verts);
		}
	}
}
