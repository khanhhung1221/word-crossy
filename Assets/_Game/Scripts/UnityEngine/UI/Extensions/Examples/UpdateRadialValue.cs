namespace UnityEngine.UI.Extensions.Examples
{
	public class UpdateRadialValue : MonoBehaviour
	{
		public InputField input;

		public RadialSlider slider;

		private void Start()
		{
		}

		public void UpdateSliderValue()
		{
			float.TryParse(input.text, out float result);
			slider.Value = result;
		}

		public void UpdateSliderAndle()
		{
			int.TryParse(input.text, out int result);
			slider.Angle = result;
		}
	}
}
