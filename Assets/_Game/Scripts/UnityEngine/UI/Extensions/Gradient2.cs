using System;
using System.Collections.Generic;

namespace UnityEngine.UI.Extensions
{
	[AddComponentMenu("UI/Effects/Extensions/Gradient2")]
	public class Gradient2 : BaseMeshEffect
	{
		public enum Type
		{
			Horizontal,
			Vertical,
			Radial,
			Diamond
		}

		public enum Blend
		{
			Override,
			Add,
			Multiply
		}

		[SerializeField]
		private Type _gradientType;

		[SerializeField]
		private Blend _blendMode = Blend.Multiply;

		[SerializeField]
		[Range(-1f, 1f)]
		private float _offset;

		[SerializeField]
		private UnityEngine.Gradient _effectGradient = new UnityEngine.Gradient
		{
			colorKeys = new GradientColorKey[2]
			{
				new GradientColorKey(Color.black, 0f),
				new GradientColorKey(Color.white, 1f)
			}
		};

		public Blend BlendMode
		{
			get
			{
				return _blendMode;
			}
			set
			{
				_blendMode = value;
				base.graphic.SetVerticesDirty();
			}
		}

		public UnityEngine.Gradient EffectGradient
		{
			get
			{
				return _effectGradient;
			}
			set
			{
				_effectGradient = value;
				base.graphic.SetVerticesDirty();
			}
		}

		public Type GradientType
		{
			get
			{
				return _gradientType;
			}
			set
			{
				_gradientType = value;
				base.graphic.SetVerticesDirty();
			}
		}

		public float Offset
		{
			get
			{
				return _offset;
			}
			set
			{
				_offset = value;
				base.graphic.SetVerticesDirty();
			}
		}

		public override void ModifyMesh(VertexHelper helper)
		{
			if (!IsActive() || helper.currentVertCount == 0)
			{
				return;
			}
			List<UIVertex> list = new List<UIVertex>();
			helper.GetUIVertexStream(list);
			int count = list.Count;
			switch (GradientType)
			{
			case Type.Horizontal:
			{
				UIVertex uIVertex19 = list[0];
				float num27 = uIVertex19.position.x;
				UIVertex uIVertex20 = list[0];
				float num28 = uIVertex20.position.x;
				float num29 = 0f;
				for (int num30 = count - 1; num30 >= 1; num30--)
				{
					UIVertex uIVertex21 = list[num30];
					num29 = uIVertex21.position.x;
					if (num29 > num28)
					{
						num28 = num29;
					}
					else if (num29 < num27)
					{
						num27 = num29;
					}
				}
				float num31 = 1f / (num28 - num27);
				UIVertex vertex4 = default(UIVertex);
				for (int num32 = 0; num32 < helper.currentVertCount; num32++)
				{
					helper.PopulateUIVertex(ref vertex4, num32);
					vertex4.color = BlendColor(vertex4.color, EffectGradient.Evaluate((vertex4.position.x - num27) * num31 - Offset));
					helper.SetUIVertex(vertex4, num32);
				}
				break;
			}
			case Type.Vertical:
			{
				UIVertex uIVertex11 = list[0];
				float num16 = uIVertex11.position.y;
				UIVertex uIVertex12 = list[0];
				float num17 = uIVertex12.position.y;
				float num18 = 0f;
				for (int num19 = count - 1; num19 >= 1; num19--)
				{
					UIVertex uIVertex13 = list[num19];
					num18 = uIVertex13.position.y;
					if (num18 > num17)
					{
						num17 = num18;
					}
					else if (num18 < num16)
					{
						num16 = num18;
					}
				}
				float num20 = 1f / (num17 - num16);
				UIVertex vertex2 = default(UIVertex);
				for (int l = 0; l < helper.currentVertCount; l++)
				{
					helper.PopulateUIVertex(ref vertex2, l);
					vertex2.color = BlendColor(vertex2.color, EffectGradient.Evaluate((vertex2.position.y - num16) * num20 - Offset));
					helper.SetUIVertex(vertex2, l);
				}
				break;
			}
			case Type.Diamond:
			{
				UIVertex uIVertex14 = list[0];
				float num21 = uIVertex14.position.y;
				UIVertex uIVertex15 = list[0];
				float num22 = uIVertex15.position.y;
				float num23 = 0f;
				for (int num24 = count - 1; num24 >= 1; num24--)
				{
					UIVertex uIVertex16 = list[num24];
					num23 = uIVertex16.position.y;
					if (num23 > num22)
					{
						num22 = num23;
					}
					else if (num23 < num21)
					{
						num21 = num23;
					}
				}
				float num25 = 1f / (num22 - num21);
				helper.Clear();
				for (int m = 0; m < count; m++)
				{
					helper.AddVert(list[m]);
				}
				float d3 = (num21 + num22) / 2f;
				UIVertex v3 = default(UIVertex);
				Vector3 a3 = (Vector3.right + Vector3.up) * d3;
				Vector3 forward3 = Vector3.forward;
				UIVertex uIVertex17 = list[0];
				v3.position = a3 + forward3 * uIVertex17.position.z;
				UIVertex uIVertex18 = list[0];
				v3.normal = uIVertex18.normal;
				v3.color = Color.white;
				helper.AddVert(v3);
				for (int n = 1; n < count; n++)
				{
					helper.AddTriangle(n - 1, n, count);
				}
				helper.AddTriangle(0, count - 1, count);
				UIVertex vertex3 = default(UIVertex);
				for (int num26 = 0; num26 < helper.currentVertCount; num26++)
				{
					helper.PopulateUIVertex(ref vertex3, num26);
					vertex3.color = BlendColor(vertex3.color, EffectGradient.Evaluate(Vector3.Distance(vertex3.position, v3.position) * num25 - Offset));
					helper.SetUIVertex(vertex3, num26);
				}
				break;
			}
			case Type.Radial:
			{
				UIVertex uIVertex = list[0];
				float num = uIVertex.position.x;
				UIVertex uIVertex2 = list[0];
				float num2 = uIVertex2.position.x;
				UIVertex uIVertex3 = list[0];
				float num3 = uIVertex3.position.y;
				UIVertex uIVertex4 = list[0];
				float num4 = uIVertex4.position.y;
				float num5 = 0f;
				float num6 = 0f;
				for (int num7 = count - 1; num7 >= 1; num7--)
				{
					UIVertex uIVertex5 = list[num7];
					num5 = uIVertex5.position.x;
					if (num5 > num2)
					{
						num2 = num5;
					}
					else if (num5 < num)
					{
						num = num5;
					}
					UIVertex uIVertex6 = list[num7];
					num6 = uIVertex6.position.y;
					if (num6 > num4)
					{
						num4 = num6;
					}
					else if (num6 < num3)
					{
						num3 = num6;
					}
				}
				float num8 = 1f / (num2 - num);
				float num9 = 1f / (num4 - num3);
				helper.Clear();
				float num10 = (num2 + num) / 2f;
				float num11 = (num3 + num4) / 2f;
				float num12 = (num2 - num) / 2f;
				float num13 = (num4 - num3) / 2f;
				UIVertex v = default(UIVertex);
				Vector3 a = Vector3.right * num10 + Vector3.up * num11;
				Vector3 forward = Vector3.forward;
				UIVertex uIVertex7 = list[0];
				v.position = a + forward * uIVertex7.position.z;
				UIVertex uIVertex8 = list[0];
				v.normal = uIVertex8.normal;
				v.color = Color.white;
				int num14 = 64;
				for (int i = 0; i < num14; i++)
				{
					UIVertex v2 = default(UIVertex);
					float num15 = (float)i * 360f / (float)num14;
					float d = Mathf.Cos((float)Math.PI / 180f * num15) * num12;
					float d2 = Mathf.Sin((float)Math.PI / 180f * num15) * num13;
					Vector3 a2 = Vector3.right * d + Vector3.up * d2;
					Vector3 forward2 = Vector3.forward;
					UIVertex uIVertex9 = list[0];
					v2.position = a2 + forward2 * uIVertex9.position.z;
					UIVertex uIVertex10 = list[0];
					v2.normal = uIVertex10.normal;
					v2.color = Color.white;
					helper.AddVert(v2);
				}
				helper.AddVert(v);
				for (int j = 1; j < num14; j++)
				{
					helper.AddTriangle(j - 1, j, num14);
				}
				helper.AddTriangle(0, num14 - 1, num14);
				UIVertex vertex = default(UIVertex);
				for (int k = 0; k < helper.currentVertCount; k++)
				{
					helper.PopulateUIVertex(ref vertex, k);
					vertex.color = BlendColor(vertex.color, EffectGradient.Evaluate(Mathf.Sqrt(Mathf.Pow(Mathf.Abs(vertex.position.x - num10) * num8, 2f) + Mathf.Pow(Mathf.Abs(vertex.position.y - num11) * num9, 2f)) * 2f - Offset));
					helper.SetUIVertex(vertex, k);
				}
				break;
			}
			}
		}

		private Color BlendColor(Color colorA, Color colorB)
		{
			switch (BlendMode)
			{
			default:
				return colorB;
			case Blend.Add:
				return colorA + colorB;
			case Blend.Multiply:
				return colorA * colorB;
			}
		}
	}
}
