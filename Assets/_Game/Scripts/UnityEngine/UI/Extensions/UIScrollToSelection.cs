using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
	[RequireComponent(typeof(ScrollRect))]
	[AddComponentMenu("UI/Extensions/UIScrollToSelection")]
	public class UIScrollToSelection : MonoBehaviour
	{
		public enum ScrollType
		{
			VERTICAL,
			HORIZONTAL,
			BOTH
		}

		[Header("[ Settings ]")]
		[SerializeField]
		private ScrollType scrollDirection;

		[SerializeField]
		private float scrollSpeed = 10f;

		[Header("[ Input ]")]
		[SerializeField]
		private bool cancelScrollOnInput;

		[SerializeField]
		private List<KeyCode> cancelScrollKeycodes = new List<KeyCode>();

		protected RectTransform LayoutListGroup => (!(TargetScrollRect != null)) ? null : TargetScrollRect.content;

		protected ScrollType ScrollDirection => scrollDirection;

		protected float ScrollSpeed => scrollSpeed;

		protected bool CancelScrollOnInput => cancelScrollOnInput;

		protected List<KeyCode> CancelScrollKeycodes => cancelScrollKeycodes;

		protected RectTransform ScrollWindow
		{
			get;
			set;
		}

		protected ScrollRect TargetScrollRect
		{
			get;
			set;
		}

		protected EventSystem CurrentEventSystem => EventSystem.current;

		protected GameObject LastCheckedGameObject
		{
			get;
			set;
		}

		protected GameObject CurrentSelectedGameObject => EventSystem.current.currentSelectedGameObject;

		protected RectTransform CurrentTargetRectTransform
		{
			get;
			set;
		}

		protected bool IsManualScrollingAvailable
		{
			get;
			set;
		}

		protected virtual void Awake()
		{
			TargetScrollRect = GetComponent<ScrollRect>();
			ScrollWindow = TargetScrollRect.GetComponent<RectTransform>();
		}

		protected virtual void Start()
		{
		}

		protected virtual void Update()
		{
			UpdateReferences();
			CheckIfScrollingShouldBeLocked();
			ScrollRectToLevelSelection();
		}

		private void UpdateReferences()
		{
			if (CurrentSelectedGameObject != LastCheckedGameObject)
			{
				CurrentTargetRectTransform = ((!(CurrentSelectedGameObject != null)) ? null : CurrentSelectedGameObject.GetComponent<RectTransform>());
				if (CurrentSelectedGameObject != null && CurrentSelectedGameObject.transform.parent == LayoutListGroup.transform)
				{
					IsManualScrollingAvailable = false;
				}
			}
			LastCheckedGameObject = CurrentSelectedGameObject;
		}

		private void CheckIfScrollingShouldBeLocked()
		{
			if (!CancelScrollOnInput || IsManualScrollingAvailable)
			{
				return;
			}
			int num = 0;
			while (true)
			{
				if (num < CancelScrollKeycodes.Count)
				{
					if (Input.GetKeyDown(CancelScrollKeycodes[num]))
					{
						break;
					}
					num++;
					continue;
				}
				return;
			}
			IsManualScrollingAvailable = true;
		}

		private void ScrollRectToLevelSelection()
		{
			if (TargetScrollRect == null || LayoutListGroup == null || ScrollWindow == null || IsManualScrollingAvailable)
			{
				return;
			}
			RectTransform currentTargetRectTransform = CurrentTargetRectTransform;
			if (!(currentTargetRectTransform == null) && !(currentTargetRectTransform.transform.parent != LayoutListGroup.transform))
			{
				switch (ScrollDirection)
				{
				case ScrollType.VERTICAL:
					UpdateVerticalScrollPosition(currentTargetRectTransform);
					break;
				case ScrollType.HORIZONTAL:
					UpdateHorizontalScrollPosition(currentTargetRectTransform);
					break;
				case ScrollType.BOTH:
					UpdateVerticalScrollPosition(currentTargetRectTransform);
					UpdateHorizontalScrollPosition(currentTargetRectTransform);
					break;
				}
			}
		}

		private void UpdateVerticalScrollPosition(RectTransform selection)
		{
			Vector2 anchoredPosition = selection.anchoredPosition;
			float num = 0f - anchoredPosition.y;
			float height = selection.rect.height;
			Vector2 pivot = selection.pivot;
			float position = num - height * (1f - pivot.y);
			float height2 = selection.rect.height;
			float height3 = ScrollWindow.rect.height;
			Vector2 anchoredPosition2 = LayoutListGroup.anchoredPosition;
			float y = anchoredPosition2.y;
			float scrollOffset = GetScrollOffset(position, y, height2, height3);
			TargetScrollRect.verticalNormalizedPosition += scrollOffset / LayoutListGroup.rect.height * Time.unscaledDeltaTime * scrollSpeed;
		}

		private void UpdateHorizontalScrollPosition(RectTransform selection)
		{
			Vector2 anchoredPosition = selection.anchoredPosition;
			float num = 0f - anchoredPosition.x;
			float width = selection.rect.width;
			Vector2 pivot = selection.pivot;
			float position = num - width * (1f - pivot.x);
			float width2 = selection.rect.width;
			float width3 = ScrollWindow.rect.width;
			Vector2 anchoredPosition2 = LayoutListGroup.anchoredPosition;
			float listAnchorPosition = 0f - anchoredPosition2.x;
			float num2 = 0f - GetScrollOffset(position, listAnchorPosition, width2, width3);
			TargetScrollRect.horizontalNormalizedPosition += num2 / LayoutListGroup.rect.width * Time.unscaledDeltaTime * scrollSpeed;
		}

		private float GetScrollOffset(float position, float listAnchorPosition, float targetLength, float maskLength)
		{
			if (position < listAnchorPosition + targetLength / 2f)
			{
				return listAnchorPosition + maskLength - (position - targetLength);
			}
			if (position + targetLength > listAnchorPosition + maskLength)
			{
				return listAnchorPosition + maskLength - (position + targetLength);
			}
			return 0f;
		}
	}
}
