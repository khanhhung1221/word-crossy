using UnityEngine;
using UnityEngine.UI;

public class VipBenefitsItem : MonoBehaviour
{
	public Image icon_img;

	public Text des_txt;

	public GameObject other_go;

	private void Start()
	{
	}

	public void Init(string reward)
	{
		base.gameObject.SetActive(value: true);
		other_go.SetActive(value: false);
		string[] array = reward.Split('_');
		switch (int.Parse(array[0]))
		{
		case 1:
		{
			int num2 = int.Parse(array[1]);
			int num3 = int.Parse(array[2]);
			string str = num2 + string.Empty;
			switch (num2)
			{
			case 2003:
				str = "shop_icon_lightning";
				des_txt.text = "FLASH \nPRICE -50%";
				other_go.SetActive(value: true);
				break;
			case 1003:
				str = "shop_icon_lightning";
				des_txt.text = "FLASH \nITEM";
				break;
			}
			icon_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/" + str);
			icon_img.rectTransform.sizeDelta = new Vector2(94f, 94f);
			break;
		}
		case 3:
		{
			int num = int.Parse(array[1]);
			icon_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/jinbi_dr");
			icon_img.rectTransform.sizeDelta = new Vector2(70f, 70f);
			des_txt.text = num + "COINS";
			break;
		}
		}
	}
}
