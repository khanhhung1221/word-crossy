using UnityEngine;
using UnityEngine.UI;

public class VipInstantRewardItem : MonoBehaviour
{
	public Image icon_img;

	public Text des_txt;

	private void Start()
	{
	}

	public void Init(int item_id, int num)
	{
		if (item_id > 10)
		{
			string str = item_id + string.Empty;
			switch (item_id)
			{
			case 1001:
				str = "shop_icon_lightbulb";
				break;
			case 1003:
				str = "shop_icon_lightning";
				break;
			}
			des_txt.text = num.ToString();
			icon_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/Shop/" + str);
		}
		else
		{
			icon_img.sprite = ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>("Image/DailyReward/jinbi_dr");
			des_txt.text = num.ToString();
		}
	}
}
