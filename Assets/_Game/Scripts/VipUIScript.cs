using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class VipUIScript : MonoBehaviour
{
	public static bool isHide;

	public RectTransform top_rect;

	public Text title_vip_txt;

	public Text benefits_title_txt;

	public Text benefits_des_txt;

	public GameObject benefits_item_res;

	public RectTransform benefits_itemParent;

	public Text InstantReward_title_txt;

	public Text InstantReward_btnUp_txt;

	public Text InstantReward_BtnPrice_txt;

	public GameObject buyBtnGo;

	public VipInstantRewardItem[] InstantRewardItems;

	public Text Des_des_txt;

	private string vip_cancel_http = "https://support.google.com/googleplay/answer/7018481";

	public GameObject mask;

	private int offer_id;

	private void Start()
	{
		if (WordGlobal.GetSaftTopSize() > 1f)
		{
			WordGlobal.ResetTopPos(top_rect);
		}
		else
		{
			RectTransform rectTransform = top_rect;
			Vector2 anchoredPosition = top_rect.anchoredPosition;
			float x2 = anchoredPosition.x;
			Vector2 anchoredPosition2 = top_rect.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(x2, anchoredPosition2.y - 16f);
		}
		title_vip_txt.text = WordGlobal.ReadText(1307);
		benefits_title_txt.text = WordGlobal.ReadText(1310);
		benefits_des_txt.text = WordGlobal.ReadText(1314);
		InstantReward_title_txt.text = WordGlobal.ReadText(1311);
		InstantReward_btnUp_txt.text = WordGlobal.ReadText(1308);
		string productID = "wc.vip";
		string text = string.Empty;
		//Product product = CodelessIAPStoreListener.Instance.GetProduct(productID);
		//if (product != null)
		//{
		//	text = WordGlobal.GetPrice(product.metadata.localizedPriceString);
		//}
		InstantReward_BtnPrice_txt.text = text + WordGlobal.ReadText(1309);
		Des_des_txt.text = string.Format(WordGlobal.ReadText(1315), text, vip_cancel_http);
		OfferItem offerItem = PlayerInfo.Instance.OfferList.Find((OfferItem x) => x.Type == 5);
		if (offerItem != null)
		{
			InstantRewardItems[0].Init(1, offerItem.Coins);
			InstantRewardItems[1].Init(1001, offerItem.BulbCount);
			InstantRewardItems[2].Init(1003, offerItem.LightingCount);
			SetContent(offerItem);
		}
		string[] array = WordGlobal.VipReward_perDay.Split(';');
		if (array.Length > 0)
		{
			for (int i = 0; i < array.Length; i++)
			{
				GameObject gameObject = Object.Instantiate(benefits_item_res);
				gameObject.transform.SetParent(benefits_itemParent);
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.transform.localScale = Vector3.one;
				float x3 = -180 + i * 180;
				(gameObject.transform as RectTransform).anchoredPosition = new Vector2(x3, 0f);
				string reward = array[i];
				gameObject.GetComponent<VipBenefitsItem>().Init(reward);
			}
		}
	}

	public void BtnLink()
	{
		Application.OpenURL(vip_cancel_http);
	}

	private void Update()
	{
		if (isHide)
		{
			BtnCloseClick();
			isHide = false;
		}
	}

	public void BtnCloseClick()
	{
		Object.Destroy(base.gameObject);
	}

	public void BtnBuyClick()
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			ResourceLoadManager.GetInstance().ShowTips(1118);
			return;
		}
		WordGlobal.LogEvent("VIP_Purchase");
		PayManager.GetInstance().m_CurrentOfferID = offer_id;
		PayManager.PaymentVerifyEvnet += PurchaseCompleted;
		PayManager.PaymentVerifyFailEvnet += PurchaseFaild;
		WordGlobal.DisableBackKey = true;
		if (mask != null)
		{
			mask.SetActive(value: true);
		}
	}

	public void SetContent(OfferItem item)
	{
		//buyBtnGo.GetComponent<IAPButton>().productId = item.Productid;
		//buyBtnGo.GetComponent<IAPButton>().enabled = true;
		offer_id = item.ID;
	}

	public void PurchaseFaild()
	{
		PayManager.PaymentVerifyEvnet -= PurchaseCompleted;
		PayManager.PaymentVerifyFailEvnet -= PurchaseFaild;
		mask.SetActive(value: false);
	}

	public void PurchaseCompleted(Product data)
	{
		WordGlobal.DisableBackKey = false;
		PayManager.PaymentVerifyEvnet -= PurchaseCompleted;
		PayManager.PaymentVerifyFailEvnet -= PurchaseFaild;
		if (mask != null)
		{
			mask.SetActive(value: false);
		}
		Object.Destroy(base.gameObject);
	}
}
