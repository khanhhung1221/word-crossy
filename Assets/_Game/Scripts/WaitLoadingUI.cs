using UnityEngine;
using UnityEngine.UI;

public class WaitLoadingUI : MonoBehaviour
{
	public Image img;

	public Image BgImg;

	private float m_ReconnectTime;

	public static WaitLoadingUI instance;

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		Invoke("CloseUI", 5f);
	}

	public void SetUIBg(bool isShowBlackBg = true)
	{
		if (isShowBlackBg)
		{
			BgImg.gameObject.SetActive(value: true);
		}
		else
		{
			BgImg.gameObject.SetActive(value: false);
		}
	}

	private void Update()
	{
		img.transform.Rotate(new Vector3(0f, 0f, -14.4f));
	}

	public static void ShowUI()
	{
		GameObject x = GameObject.Find("WaitLoadingUI");
		if (x == null)
		{
			UIManager.Instance.LoadMainUI("UI/WaitLoadingUI", isPop: true, UIManager.MainUIType.Type_Common, 99).name = "WaitLoadingUI";
		}
	}

	public void CloseUI()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
		}
	}
}
