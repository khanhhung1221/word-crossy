using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class WordDataManager : MonoBehaviour
{
	private static WordDataManager inst;

	public Dictionary<int, WordListItem> ItemList;

	private bool inited;

	private int skipNumber = 2;

	public static WordDataManager instance => inst ?? (inst = new WordDataManager());

	public WordDataManager()
	{
		byte[] file = File.ReadAllBytes(Application.dataPath + "/_Game/Word/word.csv");
		Init(file);
	}

	public void Reload()
	{
	}

	public void Init(byte[] file, string source_encoding = "UTF-8")
	{
		if (inited)
		{
			return;
		}
		MemoryStream stream = new MemoryStream(file);
		ItemList = new Dictionary<int, WordListItem>();
		List<string> list = new List<string>();
		List<string> list2 = new List<string>();
		using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
		{
			IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					string[] array = (string[])enumerator.Current;
					if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
					{
						if (skipNumber > 0)
						{
							skipNumber--;
						}
						else
						{
							WordListItem wordListItem = new WordListItem(array);
							ItemList.Add(wordListItem.Level, wordListItem);
							if (WordGlobal.isDebugBuild && wordListItem.Level <= 30)
							{
								for (int i = 0; i < wordListItem.Words.Count; i++)
								{
									if (!string.IsNullOrEmpty(wordListItem.Words[i]))
									{
										if (list.Contains(wordListItem.Words[i]))
										{
											Debug.LogError("重复单词:" + wordListItem.Words[i] + " 关卡:" + wordListItem.Level);
										}
										else
										{
											list.Add(wordListItem.Words[i]);
										}
									}
								}
								if (!string.IsNullOrEmpty(wordListItem.MoneyWord))
								{
									if (list.Contains(wordListItem.MoneyWord))
									{
										Debug.LogError("重复MoneyWord单词:" + wordListItem.MoneyWord + " 关卡:" + wordListItem.Level);
									}
									else
									{
										list.Add(wordListItem.MoneyWord);
									}
								}
							}
						}
					}
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}
		if (list2.Count > 0)
		{
			string text = string.Empty;
			for (int j = 0; j < list2.Count; j++)
			{
				text = text + list2[j] + ",";
			}
			Debug.LogError(text);
		}
		inited = true;
	}

	public WordListItem GetItemByLevel(int level)
	{
		if (ItemList.ContainsKey(level))
		{
			return ItemList[level];
		}
		return new WordListItem();
	}
}
