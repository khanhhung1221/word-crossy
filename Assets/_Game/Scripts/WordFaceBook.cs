//using Facebook.Unity;
using LitJson;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class WordFaceBook : MonoBehaviour
{
	public delegate void OnFBInvitedSucceed(string resultJsonStr);

	private static WordFaceBook instance;

	public bool isChangeUser;

	private static string testUUID = string.Empty;

	private int huodong_minLv = -1;

	private static string appLinkUrl;

	[CompilerGenerated]
	private static OnFBInvitedSucceed _003C_003Ef__mg_0024cache0;

	public static WordFaceBook Instance => GetInstance();

	private static WordFaceBook GetInstance()
	{
		if ((bool)instance)
		{
			return instance;
		}
		GameObject gameObject = new GameObject("WordFaceBook", typeof(WordFaceBook));
		return gameObject.GetComponent<WordFaceBook>();
	}

	private void Awake()
	{
		if (instance != null)
		{
			UnityEngine.Object.DestroyImmediate(base.gameObject);
			return;
		}
		instance = this;
		UnityEngine.Object.DontDestroyOnLoad(this);
	}

	public bool CheckFBIsLoggedIn()
	{
        //if (FB.IsLoggedIn)
        //{
        //	return true;
        //}
        return false;
    }

	public void FaceBookLogin()
	{
		//if (!FB.IsInitialized)
		//{
		//	FB.Init(delegate
		//	{
		//		if (FB.IsInitialized)
		//		{
		//			SendFacebookLogin();
		//		}
		//		else
		//		{
		//			WordGlobal.DebugLog("Facebook init error!");
		//		}
		//	});
		//}
		//else
		//{
		//	SendFacebookLogin();
		//}
	}

	public void FaceBookLogOut()
	{
		//FB.LogOut();
	}

	public void SendFacebookLogin()
	{
		ResourceLoadManager.GetInstance().ShowNetWaiting(30f);
		List<string> list = new List<string>();
		list.Add("public_profile");
		list.Add("email");
		//FB.LogInWithReadPermissions(list, FacebookLoginResult);
	}

	//public void FacebookLoginResult(ILoginResult loginResult)
	//{
	//	Debug.Log("FacebookLoginResult:" + loginResult.Error);
	//	ResourceLoadManager.GetInstance().HideNetWaiting();
	//	if (FB.IsLoggedIn)
	//	{
	//		FB.API("/me?fields=name" + string.Empty, HttpMethod.GET, delegate(IGraphResult graphResult)
	//		{
	//			if (string.IsNullOrEmpty(graphResult.Error))
	//			{
	//				string faceBookID = graphResult.ResultDictionary["id"] as string;
	//				string faceBookName = graphResult.ResultDictionary["name"] as string;
	//				PlayerInfo.Instance.FaceBookID = faceBookID;
	//				PlayerInfo.Instance.FaceBookName = faceBookName;
	//				LoginfbMill();
	//			}
	//		});
	//		return;
	//	}
	//	WordGlobal.DebugLogError("fbLogin:" + loginResult.Error);
	//	ResourceLoadManager.GetInstance().ShowTips(1120);
	//}

	public void LoginfbMill()
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		jsonData["fbid"] = PlayerInfo.Instance.FaceBookID;
		jsonData["fbname"] = PlayerInfo.Instance.FaceBookName;
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["loginfb"] = LoginfbCallback;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "loginfb", jsonData, dictionary);
		GameObject gameObject = UIManager.Instance.LoadMainUI("UI/WaitLoadingUI", isPop: true, UIManager.MainUIType.Type_Common, 99);
		WaitLoadingUI component = gameObject.GetComponent<WaitLoadingUI>();
		component.SetUIBg();
		Invoke("LoginfbMillFailed", 3f);
	}

	public void LoginfbMillFailed()
	{
		ResourceLoadManager.GetInstance().ShowTips(1120);
		NetMill.Instance.ResetSendQueue();
	}

	public bool LoginfbCallback(JsonData data)
	{
		CancelInvoke("LoginfbMillFailed");
		if ((bool)WaitLoadingUI.instance)
		{
			WaitLoadingUI.instance.CloseUI();
		}
		string text = (string)data["uuid"];
		int num = Convert.ToInt32(data["newbind"].ToString());
		if (PlayerInfo.Instance.UUID == text && num == 1)
		{
			Vector3 starPos = new Vector3(0.8f, 0.3f, 90f);
			GoldScript goldScript = UnityEngine.Object.FindObjectOfType<GoldScript>();
			goldScript.CollectGold(starPos, 5, 1f, null);
			PlayerInfo.Instance.AddGold(200L, 1);
			if (PlayerInfo.Instance.MaxPlayLevel >= 5)
			{
				PlayerInfo.Instance.GetDailyQuestSaveData().ChangeFBState();
				PlayerInfo.Instance.SetDailyQuestSaveData();
			}
			PlayerInfo.Instance.LoggedFaceBook = 1;
			if ((bool)BtnsCtrlUIScript.Instance)
			{
				BtnsCtrlUIScript.Instance.SetFBBtn();
			}
			if ((bool)FaceBookLoginUIScript.Instance)
			{
				FaceBookLoginUIScript.Instance.BtnCloseClick();
			}
			UpdateUserdataMill();
		}
		else
		{
			isChangeUser = true;
			if (PlayerInfo.Instance.UUID == text)
			{
				isChangeUser = false;
			}
			testUUID = text;
			PlayerInfo.Instance.UUID = text;
			GetUserdataMill();
		}
		PlayerInfo.Instance.LoggedFaceBook = 1;
		return true;
	}

	public void UpdateUserdataMill()
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		jsonData["userdata"] = new JsonData();
		jsonData["userdata"]["Score"] = PlayerInfo.Instance.Score;
		jsonData["userdata"]["CurrentLevel"] = PlayerInfo.Instance.CurrentLevel;
		jsonData["userdata"]["MaxPlayLevel"] = PlayerInfo.Instance.MaxPlayLevel;
		jsonData["userdata"]["Gold"] = PlayerInfo.Instance.Gold;
		jsonData["userdata"]["TipsCount"] = PlayerInfo.Instance.TipsCount;
		jsonData["userdata"]["ClickTipsCount"] = PlayerInfo.Instance.ClickTipsCount;
		jsonData["userdata"]["FirstTipsCount"] = PlayerInfo.Instance.FirstTipsCount;
		jsonData["userdata"]["GetBonusRewardCount"] = PlayerInfo.Instance.GetBonusRewardCount;
		jsonData["userdata"]["MaxBonusCount"] = PlayerInfo.Instance.MaxBonusCount;
		jsonData["userdata"]["CollectedBonusCount"] = PlayerInfo.Instance.CollectedBonusCount;
		jsonData["userdata"]["CollectedBonusList"] = PlayerInfo.Instance.CollectedBonusList;
		jsonData["userdata"]["FirstGetBonus"] = PlayerInfo.Instance.FirstGetBonus;
		jsonData["userdata"]["DailyRewardDay"] = PlayerInfo.Instance.DailyRewardDay;
		jsonData["userdata"]["DailyRewardDayNew"] = PlayerInfo.Instance.DailyRewardDayNew;
		jsonData["userdata"]["IsGetDailyReward"] = PlayerInfo.Instance.IsGetDailyReward;
		jsonData["userdata"]["DailyRewardTarget"] = PlayerInfo.Instance.DailyRewardTarget;
		jsonData["userdata"]["LastDailyRewardUtcTime"] = PlayerInfo.Instance.LastDailyRewardUtcTime;
		jsonData["userdata"]["ShowTutorialWatchVideo"] = PlayerInfo.Instance.ShowTutorialWatchVideo;
		jsonData["userdata"]["ShowTutorialDailyQuestFinish"] = PlayerInfo.Instance.ShowTutorialDailyQuestFinish;
		jsonData["userdata"]["ShowTutorialDailyPuzzleFinish"] = PlayerInfo.Instance.ShowTutorialDailyPuzzleFinish;
		jsonData["userdata"]["FirstTipButtonUnlock"] = PlayerInfo.Instance.FirstTipButtonUnlock;
		jsonData["userdata"]["TutorialStep"] = PlayerInfo.Instance.TutorialStep;
		jsonData["userdata"]["LoggedFaceBook"] = PlayerInfo.Instance.LoggedFaceBook;
		jsonData["userdata"]["NoPayPopCountTime"] = PlayerInfo.Instance.LasyBuyOfferTime;
		jsonData["userdata"]["NoPayPopCount"] = PlayerInfo.Instance.BuyOfferCount;
		jsonData["userdata"]["NoPayPopCount "] = PlayerInfo.Instance.BuyOfferCount;
		jsonData["userdata"]["AdRemove"] = PlayerInfo.Instance.AdRemove;
		jsonData["userdata"]["IsAlreadyClickRateBtn"] = PlayerInfo.Instance.IsAlreadyClickRateBtn;
		jsonData["userdata"]["DailyPuzzleListJson"] = PlayerInfo.Instance.DailyPuzzleListJson;
		jsonData["userdata"]["DailyPuMonthListJson"] = PlayerInfo.Instance.DailyPuMonthListJson;
		jsonData["userdata"]["DailyPuAllImageJson"] = PlayerInfo.Instance.DailyPuAllImageJson;
		jsonData["userdata"]["DailyQuestDataJson"] = PlayerInfo.Instance.DailyQuestDataJson;
		jsonData["userdata"]["LittleTipsSaveDataJson"] = PlayerInfo.Instance.LittleTipsSaveDataJson;
		jsonData["userdata"]["HuoDongSaveDataJson"] = PlayerInfo.Instance.HuoDongSaveDataJson;
		jsonData["userdata"]["HuoDongGardonSaveDataJson"] = PlayerInfo.Instance.HuoDongGardonSaveDataJson;
		jsonData["userdata"]["PigGoldSaveDataJson"] = PlayerInfo.Instance.PigGoldSaveDataJson;
		jsonData["userdata"]["HuoDongSunSaveDataJson"] = PlayerInfo.Instance.HuoDongSunSaveDataJson;
		jsonData["userdata"]["HuoDongWordFindSaveDataJson"] = PlayerInfo.Instance.HuoDongWordFindSaveDataJson;
		jsonData["userdata"]["WordSearchSaveDataJson"] = PlayerInfo.Instance.WordSearchSaveDataJson;
		jsonData["userdata"]["PlayerOtherInfoSaveDataJson"] = PlayerInfo.Instance.PlayerOtherInfoSaveDataJson;
		jsonData["userdata"]["HuoDongTournamentSaveDataJson"] = PlayerInfo.Instance.HuoDongTournamentSaveDataJson;
		jsonData["userdata"]["HuoDongGardonOpenTime"] = PlayerInfo.Instance.HuoDongGardonOpenTime;
		jsonData["userdata"]["HuoDongSunOpenTime"] = PlayerInfo.Instance.HuoDongSunOpenTime;
		jsonData["userdata"]["HuoDongWordFindOpenTime"] = PlayerInfo.Instance.HuoDongWordFindOpenTime;
		jsonData["userdata"]["OTA_CompleteNum"] = PlayerInfo.Instance.OTA_CompleteNum;
		jsonData["userdata"]["FirstTapAdStr"] = PlayerInfo.Instance.FirstTapAdStr;
		jsonData["userdata"]["OTA_CompleteNum"] = PlayerInfo.Instance.OTA_CompleteNum;
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["updateuserdata"] = UpdateUserdataCallback;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "updateuserdata", jsonData, dictionary);
		SendDailyQuestInfo();
	}

	public bool UpdateUserdataCallback(JsonData data)
	{
		return true;
	}

	public void UpdateTrackTaskEvent(string type_str, string info)
	{
		if (!string.IsNullOrEmpty(info))
		{
			JsonData jsonData = new JsonData();
			if (PlayerInfo.Instance.UUID.Length > 0)
			{
				jsonData["uuid"] = PlayerInfo.Instance.UUID;
			}
			else
			{
				JsonData jsonData2 = jsonData;
				string text = PlayerInfo.Instance.CreateUUID();
				PlayerInfo.Instance.UUID = text;
				jsonData2["uuid"] = text;
			}
			jsonData["eventdata_time"] = PlayerInfo.Instance.GetDailyDayId() + string.Empty;
			jsonData["eventdata"] = type_str + ":" + info;
			Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
			dictionary["tracktaskevent"] = UpdateTrackTaskEventCallBack;
			NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "tracktaskevent", jsonData, dictionary);
		}
	}

	public bool UpdateTrackTaskEventCallBack(JsonData data)
	{
		if (data == null)
		{
			return false;
		}
		string b = (string)data["uuid"];
		string str = data["eventdata"].ToString();
		string str2 = string.Empty;
		if (data["eventdata_time"] != null)
		{
			str2 = data["eventdata_time"].ToString();
		}
		WordGlobal.DebugLog("Daily task data statistics callback:time_str" + str2 + " str:" + str);
		if (PlayerInfo.Instance.UUID == b)
		{
		}
		return true;
	}

	public void SendHuoDongInfo(string type, string info)
	{
		WordGlobal.DebugLog("SendHuoDongInfo " + type + ":" + info);
		if (!string.IsNullOrEmpty(info))
		{
			UpdateTrackTaskEvent(type, info);
		}
	}

	public void SendDailyQuestInfo()
	{
		List<DailyQuestSaveData> list = PlayerInfo.Instance.GetDailyQuestSaveData().Getlist(10);
		string text = string.Empty;
		for (int i = 0; i < list.Count; i++)
		{
			DailyQuestDataCsv itemByID = DailyQuestDataManagerCsv.instance.GetItemByID(list[i].ID);
			DailyQuestSaveData dataByID = PlayerInfo.Instance.GetDailyQuestSaveData().GetDataByID(list[i].ID);
			string text2 = string.Empty;
			if (list[i].State == 0)
			{
				text2 = "none";
			}
			else if (list[i].State == 1)
			{
				text2 = "canget";
			}
			else if (list[i].State == 2)
			{
				text2 = "got";
			}
			string text3 = text;
			text = text3 + "ID=" + list[i].ID + "&RequireStr=" + itemByID.RequireStr + "&State=" + text2 + "&FinishNum=" + dataByID.FinishQuestNum + ";";
		}
		WordGlobal.DebugLog("SendDailyQuestInfo:" + text);
		if (!string.IsNullOrEmpty(text))
		{
			UpdateTrackTaskEvent("DailyQuest", text);
		}
	}

	public void GetHuoDongData()
	{
		if (huodong_minLv < 0)
		{
			huodong_minLv = 6;
		}
		Debug.Log("GetHuoDongData MaxPlayLevel:" + PlayerInfo.Instance.MaxPlayLevel + " min_lv:" + huodong_minLv);
		if (PlayerInfo.Instance.MaxPlayLevel >= huodong_minLv)
		{
			JsonData jsonData = new JsonData();
			if (PlayerInfo.Instance.UUID.Length > 0)
			{
				jsonData["uuid"] = PlayerInfo.Instance.UUID;
			}
			else
			{
				JsonData jsonData2 = jsonData;
				string text = PlayerInfo.Instance.CreateUUID();
				PlayerInfo.Instance.UUID = text;
				jsonData2["uuid"] = text;
			}
			jsonData["appver"] = Application.version;
			jsonData["maxplaylevel"] = PlayerInfo.Instance.MaxPlayLevel;
			GetTournamentData(string.Empty);
			Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
			dictionary["checkxmasnew"] = GetHuoDongDataCallBack;
			NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "checkxmasnew", jsonData, dictionary);
		}
	}

	public void SendApplicationVersion()
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		jsonData["appVer"] = Application.version;
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["appVer"] = SendApplicationVersionCallBack;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "appVer", jsonData, dictionary);
	}

	public bool SendApplicationVersionCallBack(JsonData data)
	{
		return false;
	}

	public bool GetHuoDongDataCallBack(JsonData data)
	{
		if (data == null)
		{
			return false;
		}
		string empty = string.Empty;
		if (data.Contains("timeleft"))
		{
			int num = int.Parse(data["timeopen"].ToString());
			empty = data["timeleft"].ToString();
			WordGlobal.HuoDongTimeLeft = int.Parse(empty);
			WordGlobal.isUpdateLevelMapHuoDong = true;
			if (WordGlobal.HuoDongTempTime > 0)
			{
				Debug.Log("The festival event is set for debugging time");
				WordGlobal.HuoDongTimeLeft = WordGlobal.HuoDongTempTime;
			}
			if (num > PlayerInfo.Instance.HuoDongOpenTime)
			{
				PlayerInfo.Instance.ClearHuoDongData();
			}
			if (WordGlobal.HuoDongTimeLeft < 0)
			{
				PlayerInfo.Instance.ClearHuoDongData();
				WordGlobal.DebugLog("Holiday time remaining 1:timeleft" + WordGlobal.HuoDongTimeLeft);
			}
			else
			{
				WordGlobal.HuoDongTimeLeft_dt = DateTime.Now.AddSeconds(WordGlobal.HuoDongTimeLeft);
				WordGlobal.DebugLog("Holiday time remaining 2:timeleft" + WordGlobal.HuoDongTimeLeft + " end_dateTime:" + WordGlobal.HuoDongTimeLeft_dt);
			}
			PlayerInfo.Instance.HuoDongOpenTime = num;
		}
		else
		{
			WordGlobal.HuoDongTimeLeft = 0;
			PlayerInfo.Instance.ClearHuoDongData();
			Debug.Log("Holiday time remaining 3:timeleft" + WordGlobal.HuoDongTimeLeft);
		}
		string empty2 = string.Empty;
		if (data.Contains("gardentimeleft"))
		{
			long num2 = long.Parse(data["gardentimeopen"].ToString());
			Debug.Log("Garden opening time:" + num2 + " Last open time:" + PlayerInfo.Instance.HuoDongGardonOpenTime);
			empty2 = data["gardentimeleft"].ToString();
			WordGlobal.HuoDongGardonTimeLeft = int.Parse(empty2);
			WordGlobal.isUpdateLevelMapHuoDong = true;
			WordGlobal.isUpdateMainUIHuoDong = true;
			if (WordGlobal.HuoDongGardonTempTime > 0)
			{
				Debug.Log("Garden events set for commissioning time");
				WordGlobal.HuoDongGardonTimeLeft = WordGlobal.HuoDongGardonTempTime;
			}
			if (num2 > PlayerInfo.Instance.HuoDongGardonOpenTime)
			{
				PlayerInfo.Instance.ClearHuoDongGardonData();
			}
			if (WordGlobal.HuoDongGardonTimeLeft < 0)
			{
				PlayerInfo.Instance.ClearHuoDongGardonData();
				Debug.Log("Garden activities remaining 1:timeleft" + WordGlobal.HuoDongGardonTimeLeft);
			}
			else
			{
				WordGlobal.HuoDongGardonTimeLeft_dt = DateTime.Now.AddSeconds(WordGlobal.HuoDongGardonTimeLeft);
				Debug.Log("Garden activities remaining 2:timeleft" + WordGlobal.HuoDongGardonTimeLeft + " end_dateTime:" + WordGlobal.HuoDongGardonTimeLeft_dt);
			}
			PlayerInfo.Instance.HuoDongGardonOpenTime = (int)num2;
		}
		else
		{
			WordGlobal.HuoDongGardonTimeLeft = 0;
			PlayerInfo.Instance.ClearHuoDongGardonData();
			Debug.Log("Garden activities remaining 3:timeleft" + WordGlobal.HuoDongGardonTimeLeft);
		}
		string empty3 = string.Empty;
		int num3 = 0;
		if (data.Contains("suntimeleft"))
		{
			num3 = int.Parse(data["suntimeopen"].ToString());
			empty3 = data["suntimeleft"].ToString();
			WordGlobal.HuoDongSunTimeLeft = int.Parse(empty3);
			WordGlobal.isUpdateLevelMapHuoDong = true;
			WordGlobal.isUpdateMainUIHuoDong = true;
			if (WordGlobal.HuoDongSunTempTime > 0)
			{
				Debug.Log("Solar activity sets debug time");
				WordGlobal.HuoDongSunTimeLeft = WordGlobal.HuoDongSunTempTime;
			}
			if (num3 > PlayerInfo.Instance.HuoDongSunOpenTime)
			{
				PlayerInfo.Instance.ClearHuoDongSunData();
			}
			if (WordGlobal.HuoDongSunTimeLeft < 0)
			{
				PlayerInfo.Instance.ClearHuoDongSunData();
				WordGlobal.HuoDongSunTimeLeft_dt = DateTime.Now.AddSeconds(-1.0);
				WordGlobal.DebugLog("Time remaining for solar activity1:timeleft" + WordGlobal.HuoDongSunTimeLeft);
			}
			else
			{
				WordGlobal.HuoDongSunTimeLeft_dt = DateTime.Now.AddSeconds(WordGlobal.HuoDongSunTimeLeft);
				WordGlobal.DebugLog("Time remaining for solar activity2:timeleft" + WordGlobal.HuoDongSunTimeLeft + " end_dateTime:" + WordGlobal.HuoDongSunTimeLeft_dt);
			}
			PlayerInfo.Instance.HuoDongSunOpenTime = num3;
		}
		else
		{
			WordGlobal.HuoDongSunTimeLeft = 0;
			WordGlobal.HuoDongSunTimeLeft_dt = DateTime.Now.AddSeconds(-1.0);
			PlayerInfo.Instance.ClearHuoDongSunData();
			Debug.Log("Time remaining for solar activity3:timeleft" + WordGlobal.HuoDongSunTimeLeft);
		}
		string empty4 = string.Empty;
		int num4 = 0;
		if (data.Contains("wordfindtimeleft"))
		{
			num4 = int.Parse(data["wordfindtimeopen"].ToString());
			empty4 = data["wordfindtimeleft"].ToString();
			WordGlobal.HuoDongWordFindTimeLeft = int.Parse(empty4);
			WordGlobal.isUpdateLevelMapHuoDong = true;
			WordGlobal.isUpdateMainUIHuoDong = true;
			if (WordGlobal.HuoDongWordFindTempTime > 0)
			{
				Debug.Log("WordFind Activity sets debug time");
				WordGlobal.HuoDongWordFindTimeLeft = WordGlobal.HuoDongWordFindTempTime;
			}
			if (num4 > PlayerInfo.Instance.HuoDongWordFindOpenTime)
			{
				PlayerInfo.Instance.ClearHuoDongWordFindData();
			}
			if (WordGlobal.HuoDongWordFindTimeLeft < 0)
			{
				PlayerInfo.Instance.ClearHuoDongWordFindData();
				WordGlobal.HuoDongWordFindTimeLeft_dt = DateTime.Now.AddSeconds(-1.0);
				WordGlobal.DebugLog("WordFindTime remaining1:timeleft" + WordGlobal.HuoDongWordFindTimeLeft);
			}
			else
			{
				WordGlobal.HuoDongWordFindTimeLeft_dt = DateTime.Now.AddSeconds(WordGlobal.HuoDongWordFindTimeLeft);
				WordGlobal.DebugLog("WordFindTime remaining2:timeleft" + WordGlobal.HuoDongWordFindTimeLeft + " end_dateTime:" + WordGlobal.HuoDongWordFindTimeLeft_dt);
			}
			PlayerInfo.Instance.HuoDongWordFindOpenTime = num4;
		}
		else
		{
			WordGlobal.HuoDongWordFindTimeLeft = 0;
			WordGlobal.HuoDongWordFindTimeLeft_dt = DateTime.Now.AddSeconds(-1.0);
			PlayerInfo.Instance.ClearHuoDongWordFindData();
			Debug.Log("WordFindTime remaining3:timeleft" + WordGlobal.HuoDongWordFindTimeLeft);
		}
		if (data.Contains("popreview"))
		{
			int num5 = int.Parse(data["popreview"].ToString());
			if (num5 == 1)
			{
				WordGlobal.isShowRateUs = true;
			}
		}
		if (data.Contains("vipbonus"))
		{
			int num6 = int.Parse(data["vipbonus"].ToString());
			if (num6 == 1)
			{
				WordGlobal.isHaveVIP = true;
			}
			else
			{
				WordGlobal.isHaveVIP = false;
			}
		}
		if (data.Contains("luckywheel"))
		{
			int num7 = int.Parse(data["luckywheel"].ToString());
			if (num7 == 1)
			{
				WordGlobal.isHaveLuckyDraw = true;
			}
			else
			{
				WordGlobal.isHaveLuckyDraw = false;
			}
			WordGlobal.isUpdateLevelMapLeftBtn = true;
		}
		return true;
	}

	public void GetTournamentData(string currentname = "")
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		Debug.Log("GetTournamentData HuoDongTournamentDateID:" + PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HuoDongTournamentDateID + "  PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongItemAmount:" + PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongItemAmount + " PlayerInfo.Instance.UUID:" + PlayerInfo.Instance.UUID);
		jsonData["date"] = PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HuoDongTournamentDateID;
		jsonData["cups"] = PlayerInfo.Instance.GetHuoDongTournamentSaveData().HuoDongItemAmount;
		jsonData["rank"] = 1;
		jsonData["currentname"] = currentname;
		if (!string.IsNullOrEmpty(currentname))
		{
			WordGlobal.isChangePlayerName = true;
		}
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["checktournament"] = GetTournamentDataCallBack;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "checktournament", jsonData, dictionary);
	}

	public bool GetTournamentDataCallBack(JsonData data)
	{
		if (data == null)
		{
			return false;
		}
		Debug.Log("GetTournamentDataCallBack");
		HuoDongTournamentUI.isWaitNetInfo = false;
		if (WordGlobal.UpdateHuoDongTournamentInfoIdx == 100)
		{
			WordGlobal.UpdateHuoDongTournamentInfoIdx = 2;
		}
		string empty = string.Empty;
		int num = int.Parse(data["date"].ToString());
		int myRank = int.Parse(data["myrank"].ToString());
		int myCups = int.Parse(data["mycups"].ToString());
		int num2 = int.Parse(data["maxdate"].ToString());
		string playerName = data["currentname"].ToString();
		if (WordGlobal.isChangePlayerName)
		{
			PlayerInfo.Instance.PlayerName = playerName;
			HuoDongTournamentUIItem.isMyChangeName = true;
			WordGlobal.isChangePlayerName = false;
		}
		PlayerInfo.Instance.GetHuoDongTournamentSaveData().MyRank = myRank;
		PlayerInfo.Instance.GetHuoDongTournamentSaveData().MyCups = myCups;
		if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HuoDongTournamentDateID != num2)
		{
			PlayerInfo.Instance.ClearHuoDongTournamentSaveData();
			PlayerInfo.Instance.GetPlayerOtherInfoSaveData().SetHuoDongTournamentDateID(num2);
			if (PlayerInfo.Instance.GetPlayerOtherInfoSaveData().HuoDongTournamentDateID != 0)
			{
				GetTournamentData(string.Empty);
			}
		}
		if (data.Contains("timeleft"))
		{
			empty = data["timeleft"].ToString();
			WordGlobal.HuoDongTournamentTimeLeft = int.Parse(empty);
			WordGlobal.isUpdateLevelMapHuoDong = true;
			WordGlobal.isUpdateMainUIHuoDong = true;
			if (WordGlobal.HuoDongTournamentTempTime > 0)
			{
				Debug.Log("Tournament Activity sets debug time");
				WordGlobal.HuoDongTournamentTimeLeft = WordGlobal.HuoDongTournamentTempTime;
				WordGlobal.HuoDongTournamentTempTime = 0;
			}
			if (WordGlobal.HuoDongTournamentTimeLeft < 0)
			{
				PlayerInfo.Instance.ClearHuoDongTournamentSaveData();
			}
			else
			{
				if (WordGlobal.HuoDongTournamentTimeLeft > WordGlobal.HuoDongTournamentFinishtTime)
				{
					PlayerInfo.Instance.GetHuoDongTournamentSaveData().GetRewardState = 0;
				}
				WordGlobal.HuoDongTournamentTimeLeft_dt = DateTime.Now.AddSeconds(WordGlobal.HuoDongTournamentTimeLeft);
				WordGlobal.DebugLog("TournamentTime remaining:timeleft" + WordGlobal.HuoDongTournamentTimeLeft + " end_dateTime:" + WordGlobal.HuoDongTournamentTimeLeft_dt);
			}
			JsonData jsonData = data["ranklist"];
			WordGlobal.TournamentInfoList.Clear();
			for (int i = 0; i < jsonData.Count; i++)
			{
				JsonData data2 = jsonData[i];
				TournamentInfoData item = new TournamentInfoData(data2);
				WordGlobal.TournamentInfoList.Add(item);
			}
		}
		else
		{
			WordGlobal.HuoDongTournamentTimeLeft = 0;
			PlayerInfo.Instance.ClearHuoDongTournamentSaveData();
			Debug.Log("TournamentTime remaining2:timeleft" + WordGlobal.HuoDongTournamentTimeLeft);
		}
		return true;
	}

	public void CheckWordIsReal(List<object> words, NetMill.CommandCallback callBack = null)
	{
		JsonData jsonData = new JsonData();
		jsonData["wordlist"] = new JsonData(words);
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		if (callBack != null)
		{
			dictionary["checkwords"] = callBack;
		}
		else
		{
			dictionary["checkwords"] = CheckWordIsRealCallBack;
		}
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "checkwords", jsonData, dictionary);
	}

	public bool CheckWordIsRealCallBack(JsonData data)
	{
		if (data == null)
		{
			return false;
		}
		Debug.Log("CheckWordIsRealCallBack");
		WordFindTools.callBackInfos.Clear();
		WordFindTools.StepNum = 3;
		JsonData jsonData = data["wordlist"];
		for (int i = 0; i < jsonData.Count; i++)
		{
			JsonData jsonData2 = jsonData[i];
			WordFindTools.callBackInfos.Add(int.Parse(jsonData2.ToJson()));
		}
		return true;
	}

	public void SendVipInfo(string begin, string end, string transactionID)
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		jsonData["begin"] = begin;
		jsonData["end"] = end;
		jsonData["viporderid"] = transactionID;
		Debug.Log("SendVipInfo begin:" + begin + " end:" + end + " transactionID:" + transactionID);
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["checkvipbonus"] = VipInfoCallBack;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "checkvipbonus", jsonData, dictionary);
	}

	public bool VipInfoCallBack(JsonData data)
	{
		if (data == null)
		{
			return false;
		}
		int num = int.Parse(data["timeleft"].ToString());
		PlayerInfo.Instance.GetPlayerOtherInfoSaveData().SetHalfPayAllFirstLetterTimeByS(num);
		Debug.Log("vip timeleft:" + num);
		return true;
	}

	public void GetUserdataMill()
	{
		JsonData jsonData = new JsonData();
		if (PlayerInfo.Instance.UUID.Length > 0)
		{
			jsonData["uuid"] = PlayerInfo.Instance.UUID;
		}
		else
		{
			JsonData jsonData2 = jsonData;
			string text = PlayerInfo.Instance.CreateUUID();
			PlayerInfo.Instance.UUID = text;
			jsonData2["uuid"] = text;
		}
		jsonData["crc"] = "crc";
		Dictionary<string, NetMill.CommandCallback> dictionary = new Dictionary<string, NetMill.CommandCallback>();
		dictionary["getuserdata"] = GetUserdataMillCallback;
		NetMill.Instance.Send(NetMill.RequestType.AccountServerRequest, "getuserdata", jsonData, dictionary);
	}

	public bool GetUserdataMillCallback(JsonData data)
	{
		if (data["userdata"].ToJson().Length <= 10 || data["crc"].ToString() != WordGlobal.GetCrcHash(data["userdata"].ToString()))
		{
			PlayerPrefs.DeleteAll();
			PlayerInfo.Instance.UUID = testUUID;
			PlayerInfo.Instance.Score = 0L;
			PlayerInfo.Instance.CurrentLevel = 1;
			PlayerInfo.Instance.MaxPlayLevel = 1;
			PlayerInfo.Instance.AddGold(400L, 3, isSetV: true);
			PlayerInfo.Instance.FirstTipsCount = 0;
			PlayerInfo.Instance.ClickTipsCount = 1;
			PlayerInfo.Instance.TipsCount = 1;
			PlayerInfo.Instance.TutorialStep = 0;
			PlayerInfo.Instance.GetDailyQuestSaveData().ChangeFBState();
			PlayerInfo.Instance.SetDailyQuestSaveData();
			PlayerInfo.Instance.LoggedFaceBook = 1;
			PlayerInfo.Instance.FirstOpen = 0;
			PlayerInfo.Instance.AdRemove = 0;
		}
		else
		{
			JsonData jsonData = JsonMapper.ToObject(data["userdata"].ToString());
			bool flag = true;
			if (!isChangeUser)
			{
				int num = Convert.ToInt32(jsonData["MaxPlayLevel"].ToString());
				if (num < PlayerInfo.Instance.MaxPlayLevel)
				{
					flag = false;
					Debug.LogError("同步客户端数据到服务器上  PlayerInfo.Instance.MaxPlayLevel:" + PlayerInfo.Instance.MaxPlayLevel + "server_maxPlayLevel:" + num);
					UpdateUserdataMill();
				}
			}
			if (flag)
			{
				if (jsonData.ToJson().Contains("Score"))
				{
					PlayerInfo.Instance.Score = Convert.ToInt32(jsonData["Score"].ToString());
				}
				if (jsonData.ToJson().Contains("CurrentLevel"))
				{
					PlayerInfo.Instance.CurrentLevel = Convert.ToInt32(jsonData["CurrentLevel"].ToString());
				}
				if (jsonData.ToJson().Contains("MaxPlayLevel"))
				{
					PlayerInfo.Instance.MaxPlayLevel = Convert.ToInt32(jsonData["MaxPlayLevel"].ToString());
				}
				if (jsonData.ToJson().Contains("Gold"))
				{
					PlayerInfo.Instance.AddGold(Convert.ToInt64(jsonData["Gold"].ToString()), 2, isSetV: true);
				}
				if (jsonData.ToJson().Contains("FirstTapAdStr"))
				{
					PlayerInfo.Instance.FirstTapAdStr = jsonData["FirstTapAdStr"].ToString();
				}
				if (jsonData.ToJson().Contains("OTA_CompleteNum"))
				{
					PlayerInfo.Instance.OTA_CompleteNum = Convert.ToInt32(jsonData["OTA_CompleteNum"].ToString());
				}
				if (jsonData.ToJson().Contains("LoggedFaceBook"))
				{
					PlayerInfo.Instance.LoggedFaceBook = Convert.ToInt32(jsonData["LoggedFaceBook"].ToString());
				}
				if (jsonData.ToJson().Contains("TipsCount"))
				{
					PlayerInfo.Instance.TipsCount = Convert.ToInt32(jsonData["TipsCount"].ToString());
				}
				if (jsonData.ToJson().Contains("ClickTipsCount"))
				{
					PlayerInfo.Instance.ClickTipsCount = Convert.ToInt32(jsonData["ClickTipsCount"].ToString());
				}
				if (jsonData.ToJson().Contains("FirstTipsCount"))
				{
					PlayerInfo.Instance.FirstTipsCount = Convert.ToInt32(jsonData["FirstTipsCount"].ToString());
				}
				if (jsonData.ToJson().Contains("GetBonusRewardCount"))
				{
					PlayerInfo.Instance.GetBonusRewardCount = Convert.ToInt32(jsonData["GetBonusRewardCount"].ToString());
				}
				if (jsonData.ToJson().Contains("MaxBonusCount"))
				{
					PlayerInfo.Instance.MaxBonusCount = Convert.ToInt32(jsonData["MaxBonusCount"].ToString());
				}
				if (jsonData.ToJson().Contains("CollectedBonusCount"))
				{
					PlayerInfo.Instance.CollectedBonusCount = Convert.ToInt32(jsonData["CollectedBonusCount"].ToString());
				}
				if (jsonData.ToJson().Contains("CollectedBonusList"))
				{
					PlayerInfo.Instance.CollectedBonusList = jsonData["CollectedBonusList"].ToString();
				}
				if (jsonData.ToJson().Contains("FirstGetBonus"))
				{
					PlayerInfo.Instance.FirstGetBonus = Convert.ToInt32(jsonData["FirstGetBonus"].ToString());
				}
				if (jsonData.ToJson().Contains("DailyRewardDay"))
				{
					PlayerInfo.Instance.DailyRewardDay = Convert.ToInt32(jsonData["DailyRewardDay"].ToString());
				}
				if (jsonData.ToJson().Contains("IsGetDailyReward"))
				{
					PlayerInfo.Instance.IsGetDailyReward = Convert.ToInt32(jsonData["IsGetDailyReward"].ToString());
				}
				if (jsonData.ToJson().Contains("ShowTutorialWatchVideo"))
				{
					PlayerInfo.Instance.ShowTutorialWatchVideo = Convert.ToInt32(jsonData["ShowTutorialWatchVideo"].ToString());
				}
				if (jsonData.ToJson().Contains("ShowTutorialDailyQuestFinish"))
				{
					PlayerInfo.Instance.ShowTutorialDailyQuestFinish = Convert.ToInt32(jsonData["ShowTutorialDailyQuestFinish"].ToString());
				}
				if (jsonData.ToJson().Contains("ShowTutorialDailyPuzzleFinish"))
				{
					PlayerInfo.Instance.ShowTutorialDailyPuzzleFinish = Convert.ToInt32(jsonData["ShowTutorialDailyPuzzleFinish"].ToString());
				}
				if (jsonData.ToJson().Contains("DailyRewardDayNew"))
				{
					PlayerInfo.Instance.DailyRewardDayNew = Convert.ToInt32(jsonData["DailyRewardDayNew"].ToString());
				}
				else
				{
					PlayerInfo.Instance.DailyRewardDayNew = 1;
					PlayerInfo.Instance.IsGetDailyReward = 0;
				}
				if (jsonData.ToJson().Contains("DailyRewardTarget"))
				{
					PlayerInfo.Instance.DailyRewardTarget = Convert.ToInt32(jsonData["DailyRewardTarget"].ToString());
				}
				if (jsonData.ToJson().Contains("LastDailyRewardUtcTime"))
				{
					PlayerInfo.Instance.LastDailyRewardUtcTime = Convert.ToInt32(jsonData["LastDailyRewardUtcTime"].ToString());
				}
				if (jsonData.ToJson().Contains("FirstTipButtonUnlock"))
				{
					PlayerInfo.Instance.FirstTipButtonUnlock = Convert.ToInt32(jsonData["FirstTipButtonUnlock"].ToString());
				}
				if (jsonData.ToJson().Contains("TutorialStep"))
				{
					PlayerInfo.Instance.TutorialStep = Convert.ToInt32(jsonData["TutorialStep"].ToString());
				}
				if (jsonData.ToJson().Contains("NoPayPopCountTime"))
				{
					PlayerInfo.Instance.LasyBuyOfferTime = jsonData["NoPayPopCountTime"].ToString();
				}
				if (jsonData.ToJson().Contains("NoPayPopCount "))
				{
					PlayerInfo.Instance.BuyOfferCount = Convert.ToInt32(jsonData["NoPayPopCount "].ToString());
				}
				else if (jsonData.ToJson().Contains("NoPayPopCount"))
				{
					PlayerInfo.Instance.BuyOfferCount = Convert.ToInt32(jsonData["NoPayPopCount"].ToString());
				}
				if (jsonData.ToJson().Contains("AdRemove"))
				{
					PlayerInfo.Instance.AdRemove = Convert.ToInt32(jsonData["AdRemove"].ToString());
				}
				if (jsonData.ToJson().Contains("IsAlreadyClickRateBtn"))
				{
					PlayerInfo.Instance.IsAlreadyClickRateBtn = Convert.ToInt32(jsonData["IsAlreadyClickRateBtn"].ToString());
				}
				if (jsonData.ToJson().Contains("LittleTipsSaveDataJson"))
				{
					PlayerInfo.Instance.LittleTipsSaveDataJson = jsonData["LittleTipsSaveDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.LittleTipsSaveDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("HuoDongSaveDataJson"))
				{
					PlayerInfo.Instance.HuoDongSaveDataJson = jsonData["HuoDongSaveDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.HuoDongSaveDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("HuoDongGardonSaveDataJson"))
				{
					PlayerInfo.Instance.HuoDongGardonSaveDataJson = jsonData["HuoDongGardonSaveDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.HuoDongGardonSaveDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("HuoDongSunSaveDataJson"))
				{
					PlayerInfo.Instance.HuoDongSunSaveDataJson = jsonData["HuoDongSunSaveDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.HuoDongSunSaveDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("HuoDongWordFindSaveDataJson"))
				{
					PlayerInfo.Instance.HuoDongWordFindSaveDataJson = jsonData["HuoDongWordFindSaveDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.HuoDongWordFindSaveDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("HuoDongTournamentSaveDataJson"))
				{
					PlayerInfo.Instance.HuoDongTournamentSaveDataJson = jsonData["HuoDongTournamentSaveDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.HuoDongTournamentSaveDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("PigGoldSaveDataJson"))
				{
					PlayerInfo.Instance.PigGoldSaveDataJson = jsonData["PigGoldSaveDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.PigGoldSaveDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("WordSearchSaveDataJson"))
				{
					PlayerInfo.Instance.WordSearchSaveDataJson = jsonData["WordSearchSaveDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.WordSearchSaveDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("PlayerOtherInfoSaveDataJson"))
				{
					PlayerInfo.Instance.PlayerOtherInfoSaveDataJson = jsonData["PlayerOtherInfoSaveDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.PlayerOtherInfoSaveDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("DailyPuzzleListJson"))
				{
					PlayerInfo.Instance.DailyPuzzleListJson = jsonData["DailyPuzzleListJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.DailyPuzzleListJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("DailyPuMonthListJson"))
				{
					PlayerInfo.Instance.DailyPuMonthListJson = jsonData["DailyPuMonthListJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.DailyPuMonthListJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("DailyPuAllImageJson"))
				{
					PlayerInfo.Instance.DailyPuAllImageJson = jsonData["DailyPuAllImageJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.DailyPuAllImageJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("DailyQuestDataJson"))
				{
					PlayerInfo.Instance.DailyQuestDataJson = jsonData["DailyQuestDataJson"].ToString();
				}
				else
				{
					PlayerInfo.Instance.DailyQuestDataJson = string.Empty;
				}
				if (jsonData.ToJson().Contains("HuoDongGardonOpenTime"))
				{
					PlayerInfo.Instance.HuoDongGardonOpenTime = Convert.ToInt32(jsonData["HuoDongGardonOpenTime"].ToString());
				}
				if (jsonData.ToJson().Contains("HuoDongSunOpenTime"))
				{
					PlayerInfo.Instance.HuoDongSunOpenTime = Convert.ToInt32(jsonData["HuoDongSunOpenTime"].ToString());
				}
				if (jsonData.ToJson().Contains("HuoDongWordFindOpenTime"))
				{
					PlayerInfo.Instance.HuoDongWordFindOpenTime = Convert.ToInt32(jsonData["HuoDongWordFindOpenTime"].ToString());
				}
			}
		}
		PlayerInfo.Instance.LoggedFaceBook = 1;
		WordGlobal.isReGetHuoDongData = true;
		WordGlobal.RestartGame();
		return true;
	}

	public static void FBInvite()
	{
		Debug.Log("FBInvite");
		FBGetAPPLinkUrl();
		FBInvite(string.Empty, string.Empty, FBINviteSucceed);
	}

	public static void FBINviteSucceed(string resultJsonStr)
	{
		Debug.LogError("FBINviteSucceed resultJsonStr:" + resultJsonStr);
	}

	private static void FBInvite(string assignedLink, string previewImageUrl, OnFBInvitedSucceed onFBInvitedSucceed = null)
	{
		if (string.IsNullOrEmpty(assignedLink))
		{
			assignedLink = appLinkUrl;
		}
		Debug.Log("appLinkUrl: " + appLinkUrl);
		Debug.Log("assignedLink: " + assignedLink);
		FBInvite(new Uri(assignedLink), new Uri(previewImageUrl), onFBInvitedSucceed);
	}

	private static void FBInvite(Uri appLinkUrl, Uri previewImageUrl = null, OnFBInvitedSucceed onFBInvitedSucceed = null)
    {
        //	FB.Mobile.AppInvite(appLinkUrl, previewImageUrl, delegate(IAppInviteResult result)
        //	{
        //		Debug.Log("rawResult: " + result.RawResult);
        //	});
    }

	public static void FBGetAPPLinkUrl()
	{
		//FB.GetAppLink(delegate(IAppLinkResult result)
		//{
		//	Debug.Log(result.RawResult);
		//	Debug.Log("Ref: " + result.Ref);
		//	Debug.Log("TargetUrl: " + result.TargetUrl);
		//	Debug.Log("Url: " + result.Url);
		//	appLinkUrl = result.Url;
		//});
	}
}
