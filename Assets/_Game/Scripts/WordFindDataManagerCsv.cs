using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class WordFindDataManagerCsv : ManagerCsv
{
	private static WordFindDataManagerCsv inst;

	private List<WordFindDataCsv> all_datas = new List<WordFindDataCsv>();

	private List<int> use_idx_list = new List<int>();

	public static WordFindDataManagerCsv instance => inst ?? (inst = new WordFindDataManagerCsv());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			int valueIndex = 0;
			MemoryStream stream = new MemoryStream(file);
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								WordFindDataCsv item = new WordFindDataCsv(array, valueIndex);
								all_datas.Add(item);
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public List<WordFindDataCsv> GetDataRandom(int num)
	{
		List<WordFindDataCsv> list = new List<WordFindDataCsv>();
		HuoDongWordFindSaveData huoDongWordFindSaveData = PlayerInfo.Instance.GetHuoDongWordFindSaveData();
		use_idx_list.Clear();
		use_idx_list.AddRange(huoDongWordFindSaveData.lastLevelIds);
		int[] array = new int[num];
		for (int i = 0; i < num; i++)
		{
			int randomIdx = GetRandomIdx();
			list.Add(all_datas[randomIdx]);
			array[i] = randomIdx;
		}
		huoDongWordFindSaveData.SetLaseLevelIds(array);
		return list;
	}

	public int GetRandomIdx()
	{
		int num = UnityEngine.Random.Range(0, all_datas.Count);
		if (use_idx_list.Contains(num))
		{
			num = GetRandomIdx();
		}
		return num;
	}
}
