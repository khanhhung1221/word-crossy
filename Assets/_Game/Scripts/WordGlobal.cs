//using com.adjust.sdk;
using DG.Tweening;
//using Facebook.Unity;
//using Firebase.Analytics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WordGlobal
{
	public delegate void OkBtnCall(object obj);

	private static bool PrintLog = true;

	public static bool isDebugBuild = false;

	public static bool GameStop = false;

	public static int SmallChaptherLevelNum = 18;

	public static int OpenDailyQuestLevel = 5;

	public static int OpenDailyPuzzleLevel = 26;

	public static int OpenDailySearchLevel = 11;

	public static bool IsShowDailyPuzzleBtn = true;

	public static bool IAPInit = false;

	public static bool OpenWordSearch = true;

	public static int WordSearchMaxLevel = 50;

	public static int AD_Type = 0;

	public static int LevelMapImgDLMinNum = 5;

	public static bool IsShowMailBtn = false;

	public static bool isNeedBanner = true;

	public static bool IsNeedDownLoadImage = true;

	public static int DownEndFileVersion = 0;

	public static float UIMoveSpeed = 0.5f;

	public static int G_MaxObjCacheCount = 200;

	public static List<GameObject> BackKeyObjList = new List<GameObject>();

	public static List<TournamentInfoData> TournamentInfoList = new List<TournamentInfoData>();

	public static bool IsShowLevelMapEff = true;

	private static bool disableBackKey = false;

	public static int SCENE_LOADING = 0;

	public static string SCENE_MAIN = "main";

	public static string UI_MAIN = "MainUI";

	public static string UI_LevelMap = "LevelMapUI";

	public const string HashStr = "_lettuce-words_";

	public static int LevelEndAddGoldNum = 0;

	public static int HuoDongRefreshLevelMapIconIndex = -1;

	public static bool isCheckHuoDongTournamentReward = false;

	public static bool isChangePlayerName = false;

	public static bool isMainUIShowBanner = false;

	public static float showBanner_bottomupy = 50f;

	public static int WordSelectWrongCount = 0;

	public static bool isNeedWordSearch = true;

	public static bool isNeedWordFind = true;

	public static bool isNeedHuoDong = true;

	public static bool isNeedPigGold = true;

	public static int HuoDongTimeLeft = -1;

	public static DateTime HuoDongTimeLeft_dt;

	public static int HuoDongGardonTimeLeft = -1;

	public static DateTime HuoDongGardonTimeLeft_dt;

	private static int huoDongSunTimeLeft = -1;

	public static DateTime HuoDongSunTimeLeft_dt;

	public static int HuoDongWordFindTimeLeft = -1;

	public static DateTime HuoDongWordFindTimeLeft_dt;

	public static int HuoDongTournamentTimeLeft = -1;

	public static DateTime HuoDongTournamentTimeLeft_dt;

	public static bool isUpdateLevelMapHuoDong = false;

	public static bool isUpdateLevelMapPigBtn = false;

	public static bool isUpdateMainUIHuoDong = false;

	public static bool isUpdateLevelMapLeftBtn = false;

	public static bool isOpenHuoDongTournamentGetRewardUI = false;

	public static string LevelMapOpenShowUI = string.Empty;

	public static bool IsGuideCheck = false;

	public static int showButterFlyTV_per = 20;

	public static int LevelMapMoveAnim_StartLevel = -1;

	public static int LevelMapMoveAnim_EndLevel = -1;

	public static bool isWordSearchTutorialFromMainUI = false;

	public static bool isShowDailyReward = true;

	public static bool isShowRateUs = true;

	public static bool isHaveVIP = true;

	public static bool isHaveLuckyDraw = true;

	public static float GetMailTime = 0f;

	public const int WordFind_MaxNumPerLevel = 25;

	public static string VipReward_perDay = "1_2003_86400;3_150;1_1003_1";

	public static float[] KeepCalm_Medium2_UpperSize = new float[26]
	{
		48f,
		36.6f,
		43.6f,
		45f,
		35f,
		33.8f,
		46.5f,
		40.8f,
		17f,
		31f,
		39.5f,
		33.8f,
		50.7f,
		43.8f,
		50.7f,
		35.2f,
		49.3f,
		36.6f,
		33.8f,
		35.2f,
		45f,
		40.8f,
		59f,
		42.3f,
		43.7f,
		39.5f
	};

	public static int HuoDongTournamentFinishtTime = 172800;

	public static int UpdateHuoDongTournamentInfoIdx = 0;

	public static bool Restarting = false;

	public static bool isReGetHuoDongData = false;

	public static int HuoDongTempTime = 0;

	public static int HuoDongGardonTempTime = 0;

	public static int HuoDongSunTempTime = 0;

	public static int HuoDongWordFindTempTime = 0;

	public static int HuoDongTournamentTempTime = 0;

	public static int PiggyBankTempAddGoldNum = 0;

	public static int Today_DayNum = 0;

	public static string[] DailyReward_ItemArr = new string[7]
	{
		"3_10",
		"3_15",
		"3_20",
		"3_25",
		"3_30",
		"1_1001_1",
		"1_1002_1"
	};

	public static int[] DailyReward_WeightArr = new int[7]
	{
		20,
		20,
		20,
		20,
		10,
		15,
		15
	};

	public static string DailyReward_FirstItem = "1_2001_1800";

	public static string FileServerUrl => "https://s3-us-west-2.amazonaws.com/lettuce-game/slg/";

	public static string HttpAccountServer
	{
		get
		{
			if (isDebugBuild)
			{
				return "http://192.168.2.60:8089/";
			}
            return "http://words.lettucegames.com:8089/";
        }
	}

    public static string DownLoadFileUrl => "http://s3-us-west-2.amazonaws.com/lettuce-game/words/";

    public static string DataPath
	{
		get
		{
			string text = "WC".ToLower();
			if (Application.isMobilePlatform)
			{
				return Application.persistentDataPath + "/" + text + "/";
			}
			if (Application.platform == RuntimePlatform.WindowsPlayer)
			{
				return Application.dataPath + "/" + text + "/";
			}
			if (Application.isEditor)
			{
				Debug.Log("Data Path: " + "d:/" + text + "/");
				return "d:/" + text + "/";
			}
			if (Application.platform == RuntimePlatform.OSXEditor)
			{
				int num = Application.dataPath.LastIndexOf('/');
				return Application.dataPath.Substring(0, num + 1) + text + "/";
			}
			return Application.dataPath + "/" + text + "/";
		}
	}

	public static int HuoDongSunTimeLeft
	{
		get
		{
			return huoDongSunTimeLeft;
		}
		set
		{
			huoDongSunTimeLeft = value;
		}
	}

	public static string UI_CurrentUI
	{
		get
		{
			if (BackKeyObjList.Count > 0)
			{
				return BackKeyObjList[BackKeyObjList.Count - 1].name;
			}
			return string.Empty;
		}
	}

	public static int Platform => 0;

	public static bool IsEditorPlatform => Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor;

	public static bool DisableBackKey
	{
		get
		{
			return disableBackKey;
		}
		set
		{
			disableBackKey = value;
		}
	}

	public static bool isFileExist(string path)
	{
		return File.Exists(path);
	}

	public static Dictionary<char, float> GetKeepCalm_Medium2_UpperSizeDic()
	{
		Dictionary<char, float> dictionary = new Dictionary<char, float>();
		dictionary.Add('A', KeepCalm_Medium2_UpperSize[0]);
		dictionary.Add('B', KeepCalm_Medium2_UpperSize[1]);
		dictionary.Add('C', KeepCalm_Medium2_UpperSize[2]);
		dictionary.Add('D', KeepCalm_Medium2_UpperSize[3]);
		dictionary.Add('E', KeepCalm_Medium2_UpperSize[4]);
		dictionary.Add('F', KeepCalm_Medium2_UpperSize[5]);
		dictionary.Add('G', KeepCalm_Medium2_UpperSize[6]);
		dictionary.Add('H', KeepCalm_Medium2_UpperSize[7]);
		dictionary.Add('I', KeepCalm_Medium2_UpperSize[8]);
		dictionary.Add('J', KeepCalm_Medium2_UpperSize[9]);
		dictionary.Add('K', KeepCalm_Medium2_UpperSize[10]);
		dictionary.Add('L', KeepCalm_Medium2_UpperSize[11]);
		dictionary.Add('M', KeepCalm_Medium2_UpperSize[12]);
		dictionary.Add('N', KeepCalm_Medium2_UpperSize[13]);
		dictionary.Add('O', KeepCalm_Medium2_UpperSize[14]);
		dictionary.Add('P', KeepCalm_Medium2_UpperSize[15]);
		dictionary.Add('Q', KeepCalm_Medium2_UpperSize[16]);
		dictionary.Add('R', KeepCalm_Medium2_UpperSize[17]);
		dictionary.Add('S', KeepCalm_Medium2_UpperSize[18]);
		dictionary.Add('T', KeepCalm_Medium2_UpperSize[19]);
		dictionary.Add('U', KeepCalm_Medium2_UpperSize[20]);
		dictionary.Add('V', KeepCalm_Medium2_UpperSize[21]);
		dictionary.Add('W', KeepCalm_Medium2_UpperSize[22]);
		dictionary.Add('X', KeepCalm_Medium2_UpperSize[23]);
		dictionary.Add('Y', KeepCalm_Medium2_UpperSize[24]);
		dictionary.Add('Z', KeepCalm_Medium2_UpperSize[25]);
		return dictionary;
	}

	public static int GetHuoDongTimeLeft()
	{
		if (HuoDongTimeLeft_dt.Year < 2000 && HuoDongTimeLeft > 0)
		{
			HuoDongTimeLeft_dt = DateTime.Now.AddSeconds(HuoDongTimeLeft);
			return HuoDongTimeLeft;
		}
		if (HuoDongTimeLeft < 0)
		{
			return 0;
		}
		return (int)(HuoDongTimeLeft_dt - DateTime.Now).TotalSeconds;
	}

	public static int GetHuoDongGardonTimeLeft()
	{
		if (HuoDongGardonTimeLeft_dt.Year < 2000 && HuoDongGardonTimeLeft > 0)
		{
			HuoDongGardonTimeLeft_dt = DateTime.Now.AddSeconds(HuoDongGardonTimeLeft);
			return HuoDongGardonTimeLeft;
		}
		if (HuoDongGardonTimeLeft < 0)
		{
			return 0;
		}
		return (int)(HuoDongGardonTimeLeft_dt - DateTime.Now).TotalSeconds;
	}

	public static int GetHuoDongTournamentTimeLeft()
	{
		if (HuoDongTournamentTimeLeft_dt.Year < 2000 && HuoDongTournamentTimeLeft > 0)
		{
			HuoDongTournamentTimeLeft_dt = DateTime.Now.AddSeconds(HuoDongTournamentTimeLeft);
			return HuoDongTournamentTimeLeft;
		}
		if (HuoDongTournamentTimeLeft < 0)
		{
			return 0;
		}
		return (int)(HuoDongTournamentTimeLeft_dt - DateTime.Now).TotalSeconds;
	}

	public static int GetHuoDongSunTimeLeft()
	{
		if (HuoDongSunTimeLeft_dt.Year < 2000 && HuoDongSunTimeLeft > 0)
		{
			HuoDongSunTimeLeft_dt = DateTime.Now.AddSeconds(HuoDongSunTimeLeft);
			return HuoDongSunTimeLeft;
		}
		if (HuoDongSunTimeLeft < 0)
		{
			return 0;
		}
		return (int)(HuoDongSunTimeLeft_dt - DateTime.Now).TotalSeconds;
	}

	public static int GetHuoDongWordFindTimeLeft()
	{
		if (HuoDongWordFindTimeLeft_dt.Year < 2000 && HuoDongWordFindTimeLeft > 0)
		{
			HuoDongWordFindTimeLeft_dt = DateTime.Now.AddSeconds(HuoDongWordFindTimeLeft);
			return HuoDongWordFindTimeLeft;
		}
		if (HuoDongWordFindTimeLeft < 0)
		{
			return 0;
		}
		return (int)(HuoDongWordFindTimeLeft_dt - DateTime.Now).TotalSeconds;
	}

	public static GoldScript AddGoldUI(Transform parent, float x = -142f, float y = -45f)
	{
		GameObject gameObject = ResourceLoadManager.GetInstance().LoadObjByPath("UI/Main/Gold");
		gameObject.transform.SetParent(parent);
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		(gameObject.transform as RectTransform).anchoredPosition = new Vector2(x, y);
		return gameObject.GetComponent<GoldScript>();
	}

	public static void ShowTutorialNPC(string talk_str)
	{
		if (GameObject.Find("TutorialNPC") == null)
		{
			TutorialNPC.TalkStr = talk_str;
			UIManager.Instance.LoadMainUI("UI/Main/TutorialNPC", isPop: true, UIManager.MainUIType.Type_Common, 99);
		}
	}

	public static void ShowHuoDongGardonMainFullEff(int type)
	{
		HuoDongGardonFullEff.ShowType = type;
		if (GameObject.Find("HuoDongGardonFullEff") == null)
		{
			UIManager.Instance.LoadMainUI("UI/HuoDong/HuoDongGardonFullEff", isPop: true, UIManager.MainUIType.Type_Common, 99);
		}
	}

	public static List<T> RandomSortList<T>(List<T> ListT)
	{
		System.Random random = new System.Random(DateTime.Now.Millisecond);
		List<T> list = new List<T>();
		foreach (T item in ListT)
		{
			list.Insert(random.Next(list.Count + 1), item);
		}
		return list;
	}

	public static void ClearChilds(Transform trans, bool removeAll = false)
	{
		for (int num = trans.childCount - 1; num >= 0; num--)
		{
			if (removeAll)
			{
				UnityEngine.Object.DestroyImmediate(trans.GetChild(num).gameObject);
			}
			else if (trans.GetChild(num).gameObject.activeSelf)
			{
				UnityEngine.Object.DestroyImmediate(trans.GetChild(num).gameObject);
			}
		}
	}

	public static void DebugLog(object message)
	{
		Debug.Log(message);
	}

	public static void DebugLogError(object message)
	{
		Debug.LogError(message);
	}

	public static bool CheckVersionNew(string new_version, string old_version)
	{
		string[] array = new_version.Split('.');
		string[] array2 = old_version.Split('.');
		if (array.Length == array2.Length)
		{
			for (int i = 0; i < array.Length; i++)
			{
				int num = int.Parse(array[i]);
				int num2 = int.Parse(array2[i]);
				if (num > num2)
				{
					return true;
				}
			}
		}
		else if (array.Length > array2.Length)
		{
			return true;
		}
		return false;
	}

	public static List<T> ShuffleList<T>(List<T> inputList)
	{
		T[] array = new T[inputList.Count];
		inputList.CopyTo(array);
		List<T> list = new List<T>();
		list.AddRange(array);
		List<T> list2 = new List<T>();
		System.Random random = new System.Random(DateTime.Now.Millisecond);
		while (list.Count > 0)
		{
			int index = random.Next(0, list.Count);
			T item = list[index];
			list.Remove(item);
			list2.Add(item);
		}
		return list2;
	}

	public static void InitLabel(Transform tf)
	{
		Text[] componentsInChildren = tf.GetComponentsInChildren<Text>(includeInactive: true);
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			int result = 0;
			if (int.TryParse(componentsInChildren[i].text, out result))
			{
				componentsInChildren[i].text = ReadText(result).Replace("\\n", "\n");
			}
		}
	}

	public static string ReadText(string id)
	{
		int result = 0;
		if (!int.TryParse(id, out result))
		{
			return string.Empty;
		}
		return ReadText(int.Parse(id));
	}

	public static string ReadText(int id)
	{
		return TextLibraryDataManagerCsv.instance.GetText(id);
	}

	public static void RestartGame()
	{
		Restarting = true;
		DOTween.KillAll();
		DisableBackKey = false;
		BackKeyObjList.Clear();
		if (AdsManager.Instance != null)
		{
			AdsManager.Instance.DestroyBannerAD();
		}
		if ((bool)MainUIScript.Instance)
		{
			MainUIScript.Instance.Restart();
		}
		PlayerInfo.Instance.Restart();
		HuoDongSunTimeLeft_dt = DateTime.Now.AddSeconds(-1.0);
		HuoDongGardonTimeLeft_dt = DateTime.Now.AddSeconds(-1.0);
		HuoDongTimeLeft_dt = DateTime.Now.AddSeconds(-1.0);
		LevelMapMoveAnim_StartLevel = -1;
		SceneManager.LoadScene(SCENE_LOADING);
	}

	public static void ScaleObjAndChild(Transform tf, Vector3 scale)
	{
		tf.localScale = scale;
		for (int i = 0; i < tf.childCount; i++)
		{
			tf.GetChild(i).localScale = scale;
		}
	}

	public static Vector3 interpolatedPosition(Vector3 point0, Vector3 point1, Vector3 point2, Vector3 point3, float i)
	{
		float num = i * i * i;
		float num2 = i * i;
		float num3 = -0.5f * num + num2 - 0.5f * i;
		float num4 = 1.5f * num - 2.5f * num2 + 1f;
		float num5 = -1.5f * num + 2f * num2 + 0.5f * i;
		float num6 = 0.5f * num - 0.5f * num2;
		float x = point0.x * num3 + point1.x * num4 + point2.x * num5 + point3.x * num6;
		float y = point0.y * num3 + point1.y * num4 + point2.y * num5 + point3.y * num6;
		return new Vector3(x, y);
	}

	public static Vector3[] CountCirclePoints(Vector3 start_pos, Vector3 end_pos, float y_offset = 0f, int dir = 0)
	{
		List<Vector3> list = new List<Vector3>();
		Vector3 p = start_pos;
		Vector3 p2 = end_pos;
		Vector3 p3 = new Vector3(p.x, p2.y + y_offset, p.z);
		if (dir == 1)
		{
			p3 = new Vector3(p2.x, p.y + y_offset, p.z);
		}
		for (float num = 0f; num < 1f; num += 0.01f)
		{
			Vector3 item = Bezier(p, p3, p2, num);
			list.Add(item);
		}
		return list.ToArray();
	}

	private static Vector3 Bezier(Vector3 p0, Vector3 p1, Vector3 p2, float t)
	{
		Vector3 a = (1f - t) * p0 + t * p1;
		Vector3 a2 = (1f - t) * p1 + t * p2;
		return (1f - t) * a + t * a2;
	}

	public static string GetTimeStamp(DateTime time)
	{
		return ConvertDateTimeToInt(time).ToString();
	}

	public static long ConvertDateTimeToInt(DateTime time)
	{
		DateTime dateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1, 0, 0, 0, 0));
		return (time.Ticks - dateTime.Ticks) / 10000;
	}

	public static double ConvertDateTimeToSecond(DateTime time)
	{
		DateTime dateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1, 0, 0, 0, 0));
		return (time - dateTime).TotalSeconds;//  time.TotalSeconds() - dateTime.TotalSeconds();
	}

	public static int GetDateNum(DateTime dt)
	{
		return dt.Year * 10000 + dt.Month * 100 + dt.Day;
	}

	public static DateTime ConvertStringToDateTime(string timeStamp)
	{
		DateTime dateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
		long ticks = long.Parse(timeStamp + "0000");
		TimeSpan value = new TimeSpan(ticks);
		return dateTime.Add(value);
	}

	public static DateTime ConvertTimeStampToDateTime(double timeStamp)
	{
		return TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).AddSeconds(timeStamp);
	}

	public static bool IsInteger(string strIn)
	{
		bool result = true;
		if (strIn == string.Empty)
		{
			result = false;
		}
		else
		{
			foreach (char c in strIn)
			{
				if (!char.IsNumber(c))
				{
					result = false;
					break;
				}
			}
		}
		return result;
	}

	public static string ListToStr(List<string> list)
	{
		string text = string.Empty;
		if (list.Count == 0)
		{
			return text;
		}
		for (int i = 0; i < list.Count; i++)
		{
			text = text + list[i] + ",";
		}
		return text.Remove(text.Length - 1);
	}

	public static List<string> StrToList(string str)
	{
		List<string> list = new List<string>();
		string[] array = str.Split(',');
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i] != string.Empty)
			{
				list.Add(array[i]);
			}
		}
		return list;
	}

	public static bool isExists(string str)
	{
		return Regex.Matches(str, "[a-zA-Z]").Count > 0;
	}

	public static string GetPrice(string priceString)
	{
		string text = priceString;
		for (int i = 0; i < text.Length; i++)
		{
			if (!isExists(text.Substring(i, 1)))
			{
				text = text.Remove(0, i);
				break;
			}
		}
		return text;
	}

	public static string GetCrcHash(string str)
	{
		MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
		byte[] array = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(str + "_lettuce-words_"));
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < array.Length; i++)
		{
			stringBuilder.Append(array[i].ToString("x2"));
		}
		return stringBuilder.ToString();
	}

	public static int CalculateLengthOfText(Text text, string message)
	{
		int num = 0;
		Font font = text.font;
		font.RequestCharactersInTexture(message, text.fontSize, text.fontStyle);
		CharacterInfo info = default(CharacterInfo);
		char[] array = message.ToCharArray();
		char[] array2 = array;
		foreach (char ch in array2)
		{
			font.GetCharacterInfo(ch, out info, text.fontSize);
			num += info.advance;
		}
		return num;
	}

	public static void LogEvent(string eventName)
	{
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
		{
			Debug.Log("eventName:" + eventName);
			//AdjustEvent adjustEvent = new AdjustEvent(AdJustDataManager.instance.GetToken(eventName));
			//Adjust.trackEvent(adjustEvent);
			try
			{
				//FirebaseAnalytics.LogEvent(eventName);
			}
			catch (Exception)
			{
				throw;
			}
			//FB.LogAppEvent(eventName);
		}
	}

	public static Color ChangeStringToColor(string str)
	{
		ColorUtility.TryParseHtmlString(str, out Color color);
		return color;
	}

	public static string ChangeColorToString(Color color)
	{
		return ColorUtility.ToHtmlStringRGBA(color);
	}

	public static void ChangePSStartColor(ParticleSystem ps, Color color)
	{
		//ps.main.startColor = color;
	}

	public static void ShowUIAni(Transform tf)
	{
		Vector3 localScale = tf.localScale;
		float x = localScale.x;
		tf.transform.localScale = Vector3.zero;
		tf.DOScale(x, 0.3f);
	}

	public static void CloseUIAni(Transform tf, bool isDestory = true)
	{
		tf.DOScale(0f, 0.15f).OnComplete(delegate
		{
			if (isDestory)
			{
				UnityEngine.Object.Destroy(tf.gameObject);
			}
		});
	}

	public static string ChangeTimeToNormalSpecial(long time, int type = 0, bool isNeedDay = true)
	{
		if (time < 0)
		{
			time = 0L;
		}
		string text = ChangeTimeToNomral(time, isNeedDay);
		string text2 = text;
		switch (type)
		{
		case 0:
			if (text.Contains("D"))
			{
				text2 = text.Substring(0, text.Length - 3);
			}
			break;
		case 1:
			if (text.Contains("D"))
			{
				text2 = text.Substring(0, text.Length - 3);
			}
			text2 = text2.Replace("00:", string.Empty);
			break;
		}
		return text2;
	}

	public static string ChangeTimeToNomral(double time, bool isNeedDay = true)
	{
		double num = (int)time / 3600;
		double num2 = (int)time % 3600 / 60;
		double num3 = (int)time % 3600 % 60;
		int num4 = (int)num / 24;
		if (isNeedDay)
		{
			num %= 24;
		}
		StringBuilder stringBuilder = new StringBuilder(string.Empty);
		if (num >= 10)
		{
			stringBuilder.Append(num);
			stringBuilder.Append(":");
		}
		else if (num > 0)
		{
			stringBuilder.Append("0");
			stringBuilder.Append(num);
			stringBuilder.Append(":");
		}
		if (num2 >= 10)
		{
			stringBuilder.Append(num2);
			stringBuilder.Append(":");
		}
		else
		{
			stringBuilder.Append("0");
			stringBuilder.Append(num2);
			stringBuilder.Append(":");
		}
		if (num3 >= 10)
		{
			stringBuilder.Append(num3);
		}
		else
		{
			stringBuilder.Append("0");
			stringBuilder.Append(num3);
		}
		if (num4 > 0 && isNeedDay)
		{
			stringBuilder.Insert(0, "D ");
			stringBuilder.Insert(0, num4);
		}
		return stringBuilder.ToString();
	}

	public static IEnumerator DownloadFBHeadIcon(string path, Image img, bool isSelf = true)
	{
		WWW www = new WWW(path);
		Debug.Log(path);
		yield return www;
		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.Log(www.error);
		}
		else if (www.isDone)
		{
			Texture2D texture = new Texture2D(512, 512);
			www.LoadImageIntoTexture(texture);
			Sprite fBHeadIconSpr = img.sprite = Sprite.Create(texture, new Rect(0f, 0f, 200f, 200f), Vector2.zero);
			if (isSelf)
			{
				PlayerInfo.Instance.FBHeadIconSpr = fBHeadIconSpr;
			}
		}
	}

	public static IEnumerator LoadFBHeadIcon(string fbID, Image img, bool isSelf = true)
	{
		return DownloadFBHeadIcon("https://graph.facebook.com/" + fbID + "/picture?type=large", img, isSelf);
	}

	public static void ResetTopPos(RectTransform rect)
	{
		Vector2 anchoredPosition = rect.anchoredPosition;
		float x = anchoredPosition.x;
		Vector2 anchoredPosition2 = rect.anchoredPosition;
		rect.anchoredPosition = new Vector2(x, anchoredPosition2.y - GetSaftTopSize());
	}

	public static float GetSaftTopSize()
	{
		return Screen.safeArea.y * 1280f / (float)Screen.height;
	}

	public static string GetDailyRewardItem()
	{
		string empty = string.Empty;
		int num = 0;
		if (num <= 0)
		{
			for (int i = 0; i < DailyReward_WeightArr.Length; i++)
			{
				num += DailyReward_WeightArr[i];
			}
		}
		int num2 = UnityEngine.Random.Range(0, num);
		int num3 = 0;
		for (int j = 0; j < DailyReward_WeightArr.Length; j++)
		{
			if (num2 - DailyReward_WeightArr[j] < 0)
			{
				num3 = j;
				break;
			}
			num2 -= DailyReward_WeightArr[j];
		}
		empty = DailyReward_ItemArr[num3];
		Debug.Log("item_str:" + empty + " ran_weight_num:" + num2 + " weight_levelIdx:" + num3);
		return empty;
	}
}
