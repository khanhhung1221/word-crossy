using System.Collections.Generic;
using UnityEngine;
using System;

public class WordJsonDataManager : MonoBehaviour
{
	private static WordJsonDataManager inst;

	public Dictionary<int, WordListItem> ItemList;

	public Dictionary<int, WordListItem> DailyPuzzleItemList;

	private bool inited;

	private WordJsonList _wordJsonList;

	private WordJsonList _dailyWordJsonList;

	private int _dailyDataMinLevel = -1;

	private int _dailyDataMaxLevel = -1;

	public static WordJsonDataManager instance => inst ?? (inst = new WordJsonDataManager());

	public void Init()
	{
		if (!inited)
		{
			_wordJsonList = JsonUtility.FromJson<WordJsonList>(Resources.Load<TextAsset>("Data/words").text);
			_dailyWordJsonList = JsonUtility.FromJson<WordJsonList>(Resources.Load<TextAsset>("Data/dailypuzzlelevel").text);
			ItemList = new Dictionary<int, WordListItem>();
			DailyPuzzleItemList = new Dictionary<int, WordListItem>();
			InitDailyItemList();
			inited = true;
		}
	}

	public void InitDailyItemList()
	{
		Debug.Log("MIN-MAX-DAILY");
		for (int i = 0; i < _dailyWordJsonList.Words.Count; i++)
		{
			WordListItem wordListItem = _dailyWordJsonList.Words[i];
			wordListItem.DecryptTxt();
			//DailyPuzzleItemList.Add(wordListItem.Level, wordListItem);
			DailyPuzzleItemList.Add((DateTime.Now.Year % 2000) * 10000 + wordListItem.Level % 10000, wordListItem);
		}
	}

	public WordListItem GetItemByLevel(int level)
	{
		if (ItemList.ContainsKey(level))
		{
			return ItemList[level];
		}
		if (_wordJsonList.Words.FindIndex((WordListItem x) => x.Level == level) >= 0)
		{
			WordListItem wordListItem = _wordJsonList.Words.Find((WordListItem x) => x.Level == level);
			wordListItem.DecryptTxt();
			ItemList.Add(wordListItem.Level, wordListItem);
			return wordListItem;
		}
		return new WordListItem();
	}

	public WordListItem GetDailyItemByLevel(int level)
	{
		if (DailyPuzzleItemList.ContainsKey(level))
		{
			return DailyPuzzleItemList[level];
		}
		return new WordListItem();
	}

	public int GetDailyMinLevel()
	{
		if (_dailyDataMinLevel < 0)
		{
			List<int> list = new List<int>(DailyPuzzleItemList.Keys);
			for (int i = 0; i < list.Count; i++)
			{
				if (_dailyDataMinLevel <= 0 || _dailyDataMinLevel > list[i])
				{
					_dailyDataMinLevel = list[i];
				}
			}
		}
		return _dailyDataMinLevel;
	}

	public int GetDailyMaxLevel()
	{
		if (_dailyDataMaxLevel < 0)
		{
			List<int> list = new List<int>(DailyPuzzleItemList.Keys);
			for (int i = 0; i < list.Count; i++)
			{
				if (_dailyDataMaxLevel < list[i])
				{
					_dailyDataMaxLevel = list[i];
				}
			}
		}
		return _dailyDataMaxLevel;
	}

	public Sprite GetDailyPuzzleBg()
	{
		int dailyMonthId = PlayerInfo.Instance.GetDailyMonthId();
		dailyMonthId %= 100;
		string path = "Image/DailyPuzzle/puzzle_bg_" + dailyMonthId;
		return ResourceLoadManager.GetInstance().LoadObjByPath<Sprite>(path);
	}

	public bool IsOpenDailyPuzzle()
	{
		bool result = false;
		if (!WordGlobal.IsShowDailyPuzzleBtn)
		{
			result = false;
		}
		else
		{
			int dailyDayId = PlayerInfo.Instance.GetDailyDayId();
			if (dailyDayId >= GetDailyMinLevel())
			{
				result = true;
			}
		}
		return result;
	}
}
