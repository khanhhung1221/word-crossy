using System;
using System.Collections.Generic;

[Serializable]
public class WordJsonList
{
	public List<WordListItem> Words;

	public WordJsonList()
	{
		Words = new List<WordListItem>();
	}
}
