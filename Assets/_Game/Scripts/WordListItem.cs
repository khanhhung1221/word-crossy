using System;
using System.Collections.Generic;

[Serializable]
public class WordListItem
{
	public int Level;

	public List<CrossWordItem> WordList;

	public List<string> Words;

	public string StrWords;

	public string MoneyWord = string.Empty;

	public List<string> Bonus;

	public string StrBonus;

	public int MaxRow;

	public int MaxCol;

	public List<string> Letters;

	public string Date;

	public CrossWordItem MoneyWordItem => WordList.Find((CrossWordItem x) => x.Word == MoneyWord);

	public WordListItem()
	{
		Level = 0;
		WordList = new List<CrossWordItem>();
		Words = new List<string>();
	}

	public WordListItem(string[] values, bool isDaily = false)
	{
		int num = 0;
		if (isDaily)
		{
			Level = int.Parse(values[num++] + values[num++].PadLeft(2, '0'));
		}
		else
		{
			Level = int.Parse(values[num++]);
		}
		WordList = new List<CrossWordItem>();
		Words = new List<string>();
		Words.AddRange(values[num++].ToUpper().Split(','));
		if (!isDaily)
		{
			MoneyWord = values[num++].ToUpper();
		}
		else
		{
			MoneyWord = string.Empty;
		}
		Bonus = new List<string>();
		string[] collection = values[num++].ToUpper().Split(',');
		Bonus.AddRange(collection);
		Letters = new List<string>();
	}

	public void EncryptTxt()
	{
		StrBonus = PBEWithMD5AndDESEncryption.EncryptTxt(WordGlobal.ListToStr(Bonus));
		Words.Clear();
		Bonus.Clear();
		for (int i = 0; i < WordList.Count; i++)
		{
			WordList[i].Word = PBEWithMD5AndDESEncryption.EncryptTxt(WordList[i].Word);
		}
	}

	public void DecryptTxt()
	{
		if (WordList.Count != 0)
		{
			Words = new List<string>();
			if (StrBonus != string.Empty)
			{
				Bonus = WordGlobal.StrToList(PBEWithMD5AndDESEncryption.DecryptTxt(StrBonus));
			}
			StrWords = string.Empty;
			StrBonus = string.Empty;
			for (int i = 0; i < WordList.Count; i++)
			{
				try
				{
					WordList[i].Word = PBEWithMD5AndDESEncryption.DecryptTxt(WordList[i].Word);
				}
				catch (Exception)
				{
					WordList[i].Word = WordList[i].Word;
				}
				Words.Add(WordList[i].Word);
			}
		}
	}
}
