using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class WordSearchDataManagerCsv : ManagerCsv
{
	private static WordSearchDataManagerCsv inst;

	public List<string> word4_list = new List<string>();

	public List<string> word5_list = new List<string>();

	public List<string> word6_list = new List<string>();

	public List<string> word7_list = new List<string>();

	public static WordSearchDataManagerCsv instance => inst ?? (inst = new WordSearchDataManagerCsv());

	public override void Init(byte[] file, string source_encoding)
	{
		if (!inited)
		{
			int valueIndex = 0;
			MemoryStream stream = new MemoryStream(file);
			using (CsvReader csvReader = new CsvReader(stream, Encoding.GetEncoding(source_encoding)))
			{
				IEnumerator enumerator = csvReader.RowEnumerator.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						string[] array = (string[])enumerator.Current;
						if (!string.IsNullOrEmpty(array[0]) && !array[0].StartsWith("#") && !array[0].StartsWith("//"))
						{
							if (skipNumber > 0)
							{
								skipNumber--;
							}
							else
							{
								WordSearchDataCsv wordSearchDataCsv = new WordSearchDataCsv(array, valueIndex);
								if (wordSearchDataCsv.word.Length <= 4)
								{
									if (!word4_list.Contains(wordSearchDataCsv.word))
									{
										word4_list.Add(wordSearchDataCsv.word);
									}
								}
								else if (wordSearchDataCsv.word.Length == 5)
								{
									if (!word5_list.Contains(wordSearchDataCsv.word))
									{
										word5_list.Add(wordSearchDataCsv.word);
									}
								}
								else if (wordSearchDataCsv.word.Length >= 6 && wordSearchDataCsv.word.Length <= 7)
								{
									if (!word6_list.Contains(wordSearchDataCsv.word))
									{
										word6_list.Add(wordSearchDataCsv.word);
									}
								}
								else if (wordSearchDataCsv.word.Length >= 8 && wordSearchDataCsv.word.Length <= 10 && !word7_list.Contains(wordSearchDataCsv.word))
								{
									word7_list.Add(wordSearchDataCsv.word);
								}
							}
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
			inited = true;
		}
	}

	public string GetWordByType(int type)
	{
		WordSearchSaveData wordSearchSaveData = PlayerInfo.Instance.GetWordSearchSaveData();
		string result = string.Empty;
		switch (type)
		{
		case 4:
		{
			if (wordSearchSaveData.word4Index >= word4_list.Count)
			{
				wordSearchSaveData.word4Index = 0;
			}
			int index3 = UnityEngine.Random.Range(0, word4_list.Count);
			result = word4_list[index3];
			wordSearchSaveData.word4Index++;
			break;
		}
		case 5:
		{
			if (wordSearchSaveData.word5Index >= word5_list.Count)
			{
				wordSearchSaveData.word5Index = 0;
			}
			int index2 = UnityEngine.Random.Range(0, word5_list.Count);
			result = word5_list[index2];
			wordSearchSaveData.word5Index++;
			break;
		}
		case 6:
		{
			if (wordSearchSaveData.word6Index >= word6_list.Count)
			{
				wordSearchSaveData.word6Index = 0;
			}
			int index4 = UnityEngine.Random.Range(0, word6_list.Count);
			result = word6_list[index4];
			wordSearchSaveData.word6Index++;
			break;
		}
		case 7:
		{
			if (wordSearchSaveData.word7Index >= word7_list.Count)
			{
				wordSearchSaveData.word7Index = 0;
			}
			int index = UnityEngine.Random.Range(0, word7_list.Count);
			result = word7_list[index];
			wordSearchSaveData.word7Index++;
			break;
		}
		}
		return result;
	}
}
