using System;
using UnityEngine;

[Serializable]
public class WordSearchSaveData
{
	public int word4Index;

	public int word5Index;

	public int word6Index;

	public int word7Index;

	public int GuideItemAmount = 1;

	public int GuideItemGoldNum = 100;

	public int Timeout_addTime = 60;

	public int Timeout_needGoldNum = 100;

	public int LastPlayDay;

	public int CurPlayLevel;

	public int[] GetBoxArr;

	public static string box01 = "3_25";

	public static string box02 = "3_50&4_1";

	public int Tutorial01;

	public int Tutorial02;

	public void SetTutorial(int type)
	{
		Tutorial01 = type;
		PlayerInfo.Instance.SaveWordSearchSaveData();
	}

	public void SetTutorial02(int type)
	{
		Tutorial02 = type;
		PlayerInfo.Instance.SaveWordSearchSaveData();
	}

	public bool isAllComplete()
	{
		if (GetBoxArr != null && GetBoxArr.Length > 0 && GetBoxArr[GetBoxArr.Length - 1] == 3)
		{
			return true;
		}
		return false;
	}

	public int GetCurPlayLevel()
	{
		if (GetBoxArr == null || GetBoxArr.Length == 0 || GetBoxArr.Length < GetMaxChapterNum())
		{
			GetBoxArr = new int[WordGlobal.WordSearchMaxLevel / 5];
		}
		if (WordGlobal.Today_DayNum != LastPlayDay)
		{
			Debug.Log("today_dayNum:" + WordGlobal.Today_DayNum + " LastPlayDay:" + LastPlayDay);
			CurPlayLevel = 0;
			GetBoxArr = new int[WordGlobal.WordSearchMaxLevel / 5];
			SetLastPlayDay(WordGlobal.Today_DayNum);
		}
		return CurPlayLevel;
	}

	public int GetMaxChapterNum()
	{
		return WordGlobal.WordSearchMaxLevel / 5;
	}

	public int AddLevel()
	{
		int result = 0;
		CurPlayLevel++;
		if (CurPlayLevel >= WordGlobal.WordSearchMaxLevel)
		{
			CurPlayLevel = WordGlobal.WordSearchMaxLevel;
		}
		int maxChapterNum = GetMaxChapterNum();
		for (int i = 0; i < maxChapterNum; i++)
		{
			int num = i + 1;
			if (CurPlayLevel >= num * 5 && GetBoxArr[i] == 0)
			{
				SetBox(num, 1);
				string[] array = null;
				array = ((num % 2 != 0) ? box01.Split('&') : box02.Split('&'));
				Debug.Log("box01:" + box01 + " box02:" + box02);
				for (int j = 0; j < array.Length; j++)
				{
					Debug.Log("wordAddLevel items[j]:" + array[j]);
					PlayerInfo.Instance.AddItemByStr(array[j], 5);
				}
				SetBox(num, 2);
				result = num;
			}
		}
		PlayerInfo.Instance.GetDailyQuestSaveData().WordSearch_num++;
		HuoDongWordSearchUI.isUpdateUI = true;
		PlayerInfo.Instance.SaveWordSearchSaveData();
		return result;
	}

	public void SetBox(int type, int valu, bool isSave = false)
	{
		GetBoxArr[type - 1] = valu;
		if (isSave)
		{
			PlayerInfo.Instance.SaveWordSearchSaveData();
		}
	}

	public void SetBox3()
	{
		bool flag = false;
		for (int i = 0; i < GetBoxArr.Length; i++)
		{
			if (GetBoxArr[i] == 2)
			{
				flag = true;
				SetBox(i + 1, 3);
			}
		}
		if (flag)
		{
			PlayerInfo.Instance.SaveWordSearchSaveData();
		}
	}

	public void SetLastPlayDay(int day_num)
	{
		LastPlayDay = day_num;
		PlayerInfo.Instance.SaveWordSearchSaveData();
	}

	public void AddGuideItemAmount(int num)
	{
		Debug.Log("AddGuideItemAmount num:" + num);
		GuideItemAmount += num;
		PlayerInfo.Instance.SaveWordSearchSaveData();
	}
}
