using System;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class _DownloadHandler : DownloadHandlerScript
{
	private FileStream fs;

	private int _contentLength;

	private int _downedLength;

	private string _fileName;

	private string savePath;

	private int ContentLength => _contentLength;

	public int DownedLength => _downedLength;

	public string FileName => _fileName;

	public string FileNameTemp => _fileName + ".temp";

	public string DirectoryPath => savePath.Substring(0, savePath.LastIndexOf('/'));

	private event Action<int> _eventTotalLength;

	private event Action<float> _eventProgress;

	private event Action<string> _eventComplete;

	public _DownloadHandler(string filePath)
		: base(new byte[204800])
	{
		savePath = filePath.Replace('\\', '/');
		_fileName = savePath.Substring(savePath.LastIndexOf('/') + 1);
		fs = new FileStream(savePath + ".temp", FileMode.Append, FileAccess.Write);
		_downedLength = (int)fs.Length;
	}

	public void RegisteReceiveTotalLengthBack(Action<int> back)
	{
		if (back != null)
		{
			_eventTotalLength += back;
		}
	}

	public void RegisteProgressBack(Action<float> back)
	{
		if (back != null)
		{
			_eventProgress += back;
		}
	}

	public void RegisteCompleteBack(Action<string> back)
	{
		if (back != null)
		{
			_eventComplete += back;
		}
	}

	protected override bool ReceiveData(byte[] data, int dataLength)
	{
		if (data == null || data.Length == 0)
		{
			Debug.Log("没有获取到数据缓存！");
			return false;
		}
		fs.Write(data, 0, dataLength);
		_downedLength += dataLength;
		if (this._eventProgress != null)
		{
			this._eventProgress((float)_downedLength / (float)_contentLength);
		}
		return true;
	}

	protected override void CompleteContent()
	{
		string text = DirectoryPath + "/" + FileName;
		string name = fs.Name;
		OnDispose();
		if (File.Exists(name))
		{
			if (File.Exists(text))
			{
				File.Delete(text);
			}
			File.Move(name, text);
		}
		else
		{
			Debug.Log("生成文件失败=>下载的文件不存在！");
		}
		if (this._eventComplete != null)
		{
			this._eventComplete(text);
		}
	}

	public void OnDispose()
	{
		fs.Close();
		fs.Dispose();
	}

	protected override void ReceiveContentLength(int contentLength)
	{
		_contentLength = contentLength + _downedLength;
		if (this._eventTotalLength != null)
		{
			this._eventTotalLength(_contentLength);
		}
	}
}
