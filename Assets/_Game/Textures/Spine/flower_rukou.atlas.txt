
flower_rukou.png
size: 180,276
format: RGBA8888
filter: Linear,Linear
repeat: none
blue_hua
  rotate: true
  xy: 118, 81
  size: 62, 60
  orig: 62, 60
  offset: 0, 0
  index: -1
blue_ye
  rotate: false
  xy: 120, 192
  size: 47, 35
  orig: 47, 36
  offset: 0, 1
  index: -1
heidi
  rotate: false
  xy: 2, 117
  size: 114, 36
  orig: 116, 38
  offset: 1, 1
  index: -1
red_hua
  rotate: true
  xy: 111, 7
  size: 72, 64
  orig: 72, 64
  offset: 0, 0
  index: -1
red_ye
  rotate: false
  xy: 2, 2
  size: 61, 39
  orig: 61, 39
  offset: 0, 0
  index: -1
tudi
  rotate: false
  xy: 2, 43
  size: 107, 72
  orig: 109, 74
  offset: 1, 1
  index: -1
xingguang
  rotate: false
  xy: 2, 155
  size: 116, 116
  orig: 128, 128
  offset: 6, 3
  index: -1
yellow_hua
  rotate: false
  xy: 120, 145
  size: 46, 45
  orig: 46, 45
  offset: 0, 0
  index: -1
yellow_ye
  rotate: true
  xy: 120, 229
  size: 42, 50
  orig: 56, 51
  offset: 13, 1
  index: -1
